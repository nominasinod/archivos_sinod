/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mbn.sinod.model.entidades;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author José Antonio Zarco Robles, MBN
 */
@Entity
@Table(name = "tsgrhareas", schema = "sgrh")
@XmlRootElement
public class Tsgrhareas implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "cod_area")
    private Integer codArea;
    @Basic(optional = false)
    @Column(name = "des_nbarea")
    private String desNbarea;
    @Basic(optional = false)
    @Column(name = "cod_acronimo")
    private String codAcronimo;
    @Basic(optional = false)
    @Column(name = "cnu_activo")
    private boolean cnuActivo;
    @Basic(optional = false)
    @Column(name = "cod_sistemasuite")
    private int codSistemasuite;
    @Basic(optional = false)
    @Column(name = "fec_creacion")
    @Temporal(TemporalType.DATE)
    private Date fecCreacion;
    @Basic(optional = false)
    @Column(name = "fec_modificacion")
    @Temporal(TemporalType.DATE)
    private Date fecModificacion;
    @Basic(optional = false)
    @Column(name = "cod_creadopor")
    private int codCreadopor;
    @Basic(optional = false)
    @Column(name = "cod_modificadopor")
    private int codModificadopor;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "codArea", fetch = FetchType.LAZY)
    private List<Tsgrhempleados> tsgrhempleadosList;

    public Tsgrhareas() {
    }

    public Tsgrhareas(Integer codArea) {
        this.codArea = codArea;
    }

    public Tsgrhareas(Integer codArea, String desNbarea, String codAcronimo, boolean cnuActivo, int codSistemasuite, Date fecCreacion, Date fecModificacion, int codCreadopor, int codModificadopor) {
        this.codArea = codArea;
        this.desNbarea = desNbarea;
        this.codAcronimo = codAcronimo;
        this.cnuActivo = cnuActivo;
        this.codSistemasuite = codSistemasuite;
        this.fecCreacion = fecCreacion;
        this.fecModificacion = fecModificacion;
        this.codCreadopor = codCreadopor;
        this.codModificadopor = codModificadopor;
    }

    public Integer getCodArea() {
        return codArea;
    }

    public void setCodArea(Integer codArea) {
        this.codArea = codArea;
    }

    public String getDesNbarea() {
        return desNbarea;
    }

    public void setDesNbarea(String desNbarea) {
        this.desNbarea = desNbarea;
    }

    
    public String getCodAcronimo() {
        return codAcronimo;
    }

    public void setCodAcronimo(String codAcronimo) {
        this.codAcronimo = codAcronimo;
    }

    @JsonIgnore
    public boolean getCnuActivo() {
        return cnuActivo;
    }

    public void setCnuActivo(boolean cnuActivo) {
        this.cnuActivo = cnuActivo;
    }

    @JsonIgnore
    public int getCodSistemasuite() {
        return codSistemasuite;
    }

    public void setCodSistemasuite(int codSistemasuite) {
        this.codSistemasuite = codSistemasuite;
    }

    @JsonIgnore
    public Date getFecCreacion() {
        return fecCreacion;
    }

    public void setFecCreacion(Date fecCreacion) {
        this.fecCreacion = fecCreacion;
    }

    @JsonIgnore
    public Date getFecModificacion() {
        return fecModificacion;
    }

    public void setFecModificacion(Date fecModificacion) {
        this.fecModificacion = fecModificacion;
    }

    @JsonIgnore
    public int getCodCreadopor() {
        return codCreadopor;
    }

    public void setCodCreadopor(int codCreadopor) {
        this.codCreadopor = codCreadopor;
    }

    @JsonIgnore
    public int getCodModificadopor() {
        return codModificadopor;
    }

    public void setCodModificadopor(int codModificadopor) {
        this.codModificadopor = codModificadopor;
    }

    @JsonIgnore
    @XmlTransient
    public List<Tsgrhempleados> getTsgrhempleadosList() {
        return tsgrhempleadosList;
    }

    public void setTsgrhempleadosList(List<Tsgrhempleados> tsgrhempleadosList) {
        this.tsgrhempleadosList = tsgrhempleadosList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codArea != null ? codArea.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tsgrhareas)) {
            return false;
        }
        Tsgrhareas other = (Tsgrhareas) object;
        if ((this.codArea == null && other.codArea != null) || (this.codArea != null && !this.codArea.equals(other.codArea))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mbn.sinod.model.entidades.Tsgrhareas[ codArea=" + codArea + " ]";
    }
    
}
