/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mbn.sinod.model.entidades;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author ISRAEL-STARK
 */
@Entity
@Table(name = "tsgnomargumento", catalog = "sgnom", schema = "sgnom")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Tsgnomargumento.findAll", query = "SELECT t FROM Tsgnomargumento t")})
public class Tsgnomargumento implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "cod_argumentoid")
    private Integer codArgumentoid;
    @Basic(optional = false)
    @Column(name = "cod_nbargumento")
    private String codNbargumento;
    @Basic(optional = false)
    @Column(name = "cod_clavearg")
    private String codClavearg;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "imp_valorconst")
    private BigDecimal impValorconst;
    @Column(name = "des_funcionbd")
    private String desFuncionbd;
    @Basic(optional = false)
    @Column(name = "bol_estatus")
    private boolean bolEstatus;
    @Column(name = "txt_descripcion")
    private String txtDescripcion;
    @Basic(optional = false)
    @Column(name = "aud_ucrea")
    private int audUcrea;
    @Column(name = "aud_uactualizacion")
    private Integer audUactualizacion;
    @Basic(optional = false)
    @Column(name = "aud_feccrea")
    @Temporal(TemporalType.DATE)
    private Date audFeccrea;
    @Column(name = "aud_fecactualizacion")
    @Temporal(TemporalType.DATE)
    private Date audFecactualizacion;

    public Tsgnomargumento() {
    }

    public Tsgnomargumento(Integer codArgumentoid) {
        this.codArgumentoid = codArgumentoid;
    }

    public Tsgnomargumento(Integer codArgumentoid, String codNbargumento, String codClavearg, boolean bolEstatus, int audUcrea, Date audFeccrea) {
        this.codArgumentoid = codArgumentoid;
        this.codNbargumento = codNbargumento;
        this.codClavearg = codClavearg;
        this.bolEstatus = bolEstatus;
        this.audUcrea = audUcrea;
        this.audFeccrea = audFeccrea;
    }

    public Integer getCodArgumentoid() {
        return codArgumentoid;
    }

    public void setCodArgumentoid(Integer codArgumentoid) {
        this.codArgumentoid = codArgumentoid;
    }

    public String getCodNbargumento() {
        return codNbargumento;
    }

    public void setCodNbargumento(String codNbargumento) {
        this.codNbargumento = codNbargumento;
    }

    public String getCodClavearg() {
        return codClavearg;
    }

    public void setCodClavearg(String codClavearg) {
        this.codClavearg = codClavearg;
    }

    public BigDecimal getImpValorconst() {
        return impValorconst;
    }

    public void setImpValorconst(BigDecimal impValorconst) {
        this.impValorconst = impValorconst;
    }

    public String getDesFuncionbd() {
        return desFuncionbd;
    }

    public void setDesFuncionbd(String desFuncionbd) {
        this.desFuncionbd = desFuncionbd;
    }

    public boolean getBolEstatus() {
        return bolEstatus;
    }

    public void setBolEstatus(boolean bolEstatus) {
        this.bolEstatus = bolEstatus;
    }

    public String getTxtDescripcion() {
        return txtDescripcion;
    }

    public void setTxtDescripcion(String txtDescripcion) {
        this.txtDescripcion = txtDescripcion;
    }

    public int getAudUcrea() {
        return audUcrea;
    }

    public void setAudUcrea(int audUcrea) {
        this.audUcrea = audUcrea;
    }

    public Integer getAudUactualizacion() {
        return audUactualizacion;
    }

    public void setAudUactualizacion(Integer audUactualizacion) {
        this.audUactualizacion = audUactualizacion;
    }

    public Date getAudFeccrea() {
        return audFeccrea;
    }

    public void setAudFeccrea(Date audFeccrea) {
        this.audFeccrea = audFeccrea;
    }

    public Date getAudFecactualizacion() {
        return audFecactualizacion;
    }

    public void setAudFecactualizacion(Date audFecactualizacion) {
        this.audFecactualizacion = audFecactualizacion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codArgumentoid != null ? codArgumentoid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tsgnomargumento)) {
            return false;
        }
        Tsgnomargumento other = (Tsgnomargumento) object;
        if ((this.codArgumentoid == null && other.codArgumentoid != null) || (this.codArgumentoid != null && !this.codArgumentoid.equals(other.codArgumentoid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mbn.sinod.model.entidades.Tsgnomargumento[ codArgumentoid=" + codArgumentoid + " ]";
    }
    
}
