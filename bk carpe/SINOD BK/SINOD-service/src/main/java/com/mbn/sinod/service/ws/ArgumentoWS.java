/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mbn.sinod.service.ws;

import com.mbn.sinod.model.dto.ArgumentoDTO;
import com.mbn.sinod.model.entidades.Tsgnomargumento;
import com.mbn.sinod.model.negocio.ArgumentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author mariana
 */
@RestController //Incluye las anotaciones @Controller y @ResponseBody,simplifica la implementación del controlador
@RequestMapping("/ws") 
public class ArgumentoWS {
   @Autowired
    ArgumentoService argumento;
    
    @RequestMapping(value = "/argumento/obtenerArgumento/",method = RequestMethod.GET)
    @ResponseBody
    public ArgumentoDTO obtenerArgumento(){
        return argumento.listarArgumentos();
    }

    @RequestMapping(value = "/argumento/guardar/", method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ArgumentoDTO guardar(@RequestBody Tsgnomargumento argu){ 
        System.out.println("WS guardar");
        return argumento.guardarArgumento(argu);
    }

    
    
    @RequestMapping(method = RequestMethod.POST, value = "/argumento/eliminar/",produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ArgumentoDTO eliminar(@RequestParam Integer argumentoid) {
        return argumento.eliminarArgumento(argumentoid);
    }
   
}
