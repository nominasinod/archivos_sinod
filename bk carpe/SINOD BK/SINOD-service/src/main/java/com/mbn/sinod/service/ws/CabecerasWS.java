/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mbn.sinod.service.ws;

import com.mbn.sinod.model.dto.CabeceraDTO;
import com.mbn.sinod.model.negocio.CabeceraService;
import java.io.Serializable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Mi Pe
 */
@RestController
@RequestMapping("/ws")
public class CabecerasWS implements Serializable{
    
    @Autowired
    CabeceraService cabeceraService;

    @RequestMapping(value = "/cabeceras", method = RequestMethod.GET,
            produces= MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public CabeceraDTO listCabeceras() {
        System.out.println("ws " + cabeceraService.listCabeceras());
        System.out.println("ws idcabecera " + cabeceraService.listCabeceras().getListarCabecera().get(0).getCodCabeceraid());
        return cabeceraService.listCabeceras();
    }
    
    @RequestMapping(method = RequestMethod.GET, value = "/listarCabeceras",
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody// anotacion que nos permite retornar un json
    public CabeceraDTO listarCatIncidencias() {
        return cabeceraService.listCabeceras();
    }

}
