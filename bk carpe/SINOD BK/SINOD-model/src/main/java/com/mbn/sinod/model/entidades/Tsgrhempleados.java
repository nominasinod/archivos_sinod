/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mbn.sinod.model.entidades;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GenerationType;
import javax.persistence.Id;
	import javax.persistence.GeneratedValue;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author José Antonio Zarco Robles, MBN
 */
@Entity
@Table(name = "tsgrhempleados", schema = "sgrh")
@XmlRootElement
public class Tsgrhempleados implements Serializable {

    private static final long serialVersionUID = 1L;
    	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "cod_empleado")
    private Integer codEmpleado;
    @Basic(optional = false)
    @Column(name = "des_nombre")
    private String desNombre;
    @Column(name = "des_nombres")
    private String desNombres;
    @Basic(optional = false)
    @Column(name = "des_apepaterno")
    private String desApepaterno;
    @Column(name = "des_apematerno")
    private String desApematerno;
//    @Basic(optional = false)
//    @Column(name = "des_direccion")
//    private String desDireccion;
//    @Basic(optional = false)
//    @Column(name = "fec_nacimiento")
//    @Temporal(TemporalType.DATE)
//    private Date fecNacimiento;
//    @Basic(optional = false)
//    @Column(name = "des_lugarnacimiento")
//    private String desLugarnacimiento;
//    @Basic(optional = false)
//    @Column(name = "cod_tiposangre")
//    private String codTiposangre;
//    @Basic(optional = false)
//    @Column(name = "fec_ingreso")
//    @Temporal(TemporalType.DATE)
//    private Date fecIngreso;
//    @Basic(optional = false)
//    @Column(name = "cod_curp")
//    private String codCurp;
//    @Basic(optional = false)
//    @Lob
    @Column(name = "bin_foto")
    private byte[] binFoto;
//    @Basic(optional = false)
//    @Column(name = "cod_tipofoto")
//    private String codTipofoto;
//    @Basic(optional = false)
//    @Column(name = "cod_extensionfoto")
//    private String codExtensionfoto;
//    @Basic(optional = false)
//    @Column(name = "cod_estatusempleado")
//    private int codEstatusempleado;
//    @Basic(optional = false)
//    @Column(name = "cod_estadocivil")
//    private int codEstadocivil;
//    @Basic(optional = false)
//    @Column(name = "cod_rol")
//    private int codRol;
//    @Basic(optional = false)
//    @Column(name = "cod_diasvacaciones")
//    private int codDiasvacaciones;
//    @Basic(optional = false)
//    @Column(name = "cod_sistemasuite")
//    private int codSistemasuite;
//    @Basic(optional = false)
//    @Column(name = "fec_creacion")
//    @Temporal(TemporalType.DATE)
//    private Date fecCreacion;
//    @Basic(optional = false)
//    @Column(name = "fec_modificacion")
//    @Temporal(TemporalType.DATE)
//    private Date fecModificacion;
//    @Basic(optional = false)
//    @Column(name = "cod_creadopor")
//    private int codCreadopor;
//    @Basic(optional = false)
//    @Column(name = "cod_modificadopor")
//    private int codModificadopor;
    @Column(name = "des_correo")
    private String desCorreo;
    @JoinColumn(name = "cod_area", referencedColumnName = "cod_area")
    @ManyToOne(optional = false)
    private Tsgrhareas codArea;

    public Tsgrhempleados() {
    }

    public Tsgrhempleados(Integer codEmpleado) {
        this.codEmpleado = codEmpleado;
    }

    public Tsgrhempleados(Integer codEmpleado, String desNombre, String desApepaterno, String desDireccion, Date fecNacimiento, String desLugarnacimiento, String codTiposangre, Date fecIngreso, String codCurp, byte[] binFoto, String codTipofoto, String codExtensionfoto, int codEstatusempleado, int codEstadocivil, int codRol, int codDiasvacaciones, int codSistemasuite, Date fecCreacion, Date fecModificacion, int codCreadopor, int codModificadopor, String desCorreo) {
        this.codEmpleado = codEmpleado;
        this.desNombre = desNombre;
        this.desApepaterno = desApepaterno;
//        this.desDireccion = desDireccion;
//        this.fecNacimiento = fecNacimiento;
//        this.desLugarnacimiento = desLugarnacimiento;
//        this.codTiposangre = codTiposangre;
//        this.fecIngreso = fecIngreso;
//        this.codCurp = codCurp;
//        this.binFoto = binFoto;
//        this.codTipofoto = codTipofoto;
//        this.codExtensionfoto = codExtensionfoto;
//        this.codEstatusempleado = codEstatusempleado;
//        this.codEstadocivil = codEstadocivil;
//        this.codRol = codRol;
//        this.codDiasvacaciones = codDiasvacaciones;
//        this.codSistemasuite = codSistemasuite;
//        this.fecCreacion = fecCreacion;
//        this.fecModificacion = fecModificacion;
//        this.codCreadopor = codCreadopor;
//        this.codModificadopor = codModificadopor;
        this.desCorreo = desCorreo;
    }

    public Integer getCodEmpleado() {
        return codEmpleado;
    }

    public void setCodEmpleado(Integer codEmpleado) {
        this.codEmpleado = codEmpleado;
    }

    public String getDesNombre() {
        return desNombre;
    }

    public void setDesNombre(String desNombre) {
        this.desNombre = desNombre;
    }

    public String getDesNombres() {
        return desNombres;
    }

    public void setDesNombres(String desNombres) {
        this.desNombres = desNombres;
    }

    public String getDesApepaterno() {
        return desApepaterno;
    }

    public void setDesApepaterno(String desApepaterno) {
        this.desApepaterno = desApepaterno;
    }

    public String getDesApematerno() {
        return desApematerno;
    }

    public void setDesApematerno(String desApematerno) {
        this.desApematerno = desApematerno;
    }

//    public String getDesDireccion() {
//        return desDireccion;
//    }
//
//    public void setDesDireccion(String desDireccion) {
//        this.desDireccion = desDireccion;
//    }
//
//    public Date getFecNacimiento() {
//        return fecNacimiento;
//    }
//
//    public void setFecNacimiento(Date fecNacimiento) {
//        this.fecNacimiento = fecNacimiento;
//    }
//
//    public String getDesLugarnacimiento() {
//        return desLugarnacimiento;
//    }
//
//    public void setDesLugarnacimiento(String desLugarnacimiento) {
//        this.desLugarnacimiento = desLugarnacimiento;
//    }
//
//    public String getCodTiposangre() {
//        return codTiposangre;
//    }
//
//    public void setCodTiposangre(String codTiposangre) {
//        this.codTiposangre = codTiposangre;
//    }
//
//    public Date getFecIngreso() {
//        return fecIngreso;
//    }
//
//    public void setFecIngreso(Date fecIngreso) {
//        this.fecIngreso = fecIngreso;
//    }
//
//    public String getCodCurp() {
//        return codCurp;
//    }
//
//    public void setCodCurp(String codCurp) {
//        this.codCurp = codCurp;
//    }
//
    public byte[] getBinFoto() {
        return binFoto;
    }

    public void setBinFoto(byte[] binFoto) {
        this.binFoto = binFoto;
    }
//
//    public String getCodTipofoto() {
//        return codTipofoto;
//    }
//
//    public void setCodTipofoto(String codTipofoto) {
//        this.codTipofoto = codTipofoto;
//    }
//
//    public String getCodExtensionfoto() {
//        return codExtensionfoto;
//    }
//
//    public void setCodExtensionfoto(String codExtensionfoto) {
//        this.codExtensionfoto = codExtensionfoto;
//    }
//
//    public int getCodEstatusempleado() {
//        return codEstatusempleado;
//    }
//
//    public void setCodEstatusempleado(int codEstatusempleado) {
//        this.codEstatusempleado = codEstatusempleado;
//    }
//
//    public int getCodEstadocivil() {
//        return codEstadocivil;
//    }
//
//    public void setCodEstadocivil(int codEstadocivil) {
//        this.codEstadocivil = codEstadocivil;
//    }
//
//    public int getCodRol() {
//        return codRol;
//    }
//
//    public void setCodRol(int codRol) {
//        this.codRol = codRol;
//    }
//
//    public int getCodDiasvacaciones() {
//        return codDiasvacaciones;
//    }
//
//    public void setCodDiasvacaciones(int codDiasvacaciones) {
//        this.codDiasvacaciones = codDiasvacaciones;
//    }
//
//    public int getCodSistemasuite() {
//        return codSistemasuite;
//    }
//
//    public void setCodSistemasuite(int codSistemasuite) {
//        this.codSistemasuite = codSistemasuite;
//    }
//
//    public Date getFecCreacion() {
//        return fecCreacion;
//    }
//
//    public void setFecCreacion(Date fecCreacion) {
//        this.fecCreacion = fecCreacion;
//    }
//
//    public Date getFecModificacion() {
//        return fecModificacion;
//    }
//
//    public void setFecModificacion(Date fecModificacion) {
//        this.fecModificacion = fecModificacion;
//    }
//
//    public int getCodCreadopor() {
//        return codCreadopor;
//    }
//
//    public void setCodCreadopor(int codCreadopor) {
//        this.codCreadopor = codCreadopor;
//    }
//
//    public int getCodModificadopor() {
//        return codModificadopor;
//    }
//
//    public void setCodModificadopor(int codModificadopor) {
//        this.codModificadopor = codModificadopor;
//    }

    public String getDesCorreo() {
        return desCorreo;
    }

    public void setDesCorreo(String desCorreo) {
        this.desCorreo = desCorreo;
    }

    public Tsgrhareas getCodArea() {
        return codArea;
    }

    public void setCodArea(Tsgrhareas codArea) {
        this.codArea = codArea;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codEmpleado != null ? codEmpleado.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tsgrhempleados)) {
            return false;
        }
        Tsgrhempleados other = (Tsgrhempleados) object;
        if ((this.codEmpleado == null && other.codEmpleado != null) || (this.codEmpleado != null && !this.codEmpleado.equals(other.codEmpleado))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mbn.sinod.model.entidades.Tsgrhempleados[ codEmpleado=" + codEmpleado + " ]";
    }

}
