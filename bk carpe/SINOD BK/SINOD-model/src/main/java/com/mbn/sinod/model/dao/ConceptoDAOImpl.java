/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mbn.sinod.model.dao;

import com.googlecode.genericdao.dao.hibernate.GenericDAOImpl;
import com.mbn.sinod.model.entidades.Tsgnomconcepto;
import java.util.List;


public class ConceptoDAOImpl extends GenericDAOImpl<Tsgnomconcepto, Integer> implements ConceptoDAO {

    @Override
    public List<Tsgnomconcepto> listarConcepto() {
//        return findAll();
        List<Tsgnomconcepto> conceptos = (List<Tsgnomconcepto>)
                getSession().createQuery("FROM Tsgnomconcepto WHERE bolEstatus = :parametro")
                        .setParameter("parametro", true).list();
        return conceptos;
    }

}
