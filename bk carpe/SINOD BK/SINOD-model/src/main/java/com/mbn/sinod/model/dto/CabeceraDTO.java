/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mbn.sinod.model.dto;

import com.mbn.sinod.model.entidades.Tsgnomcabecera;
import java.util.List;

/**
 *
 * @author Mi Pe
 */
public class CabeceraDTO extends GenericDTO{
    private Tsgnomcabecera CatCabecera;
        private List<Tsgnomcabecera> listarCabecera;


    /**
     * @return the CatCabecera
     */
    public Tsgnomcabecera getCatCabecera() {
        return CatCabecera;
    }

    /**
     * @param CatCabecera the CatCabecera to set
     */
    public void setCatCabecera(Tsgnomcabecera CatCabecera) {
        this.CatCabecera = CatCabecera;
    }

    /**
     * @return the listarCabecera
     */
    public List<Tsgnomcabecera> getListarCabecera() {
        return listarCabecera;
    }

    /**
     * @param listarCabecera the listarCabecera to set
     */
    public void setListarCabecera(List<Tsgnomcabecera> listarCabecera) {
        this.listarCabecera = listarCabecera;
    }
}
