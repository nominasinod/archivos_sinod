/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mbn.sinod.model.negocio;

import com.mbn.sinod.model.comun.StaticConstantes;
import com.mbn.sinod.model.dao.ConceptoDAO;
import com.mbn.sinod.model.dto.ConceptoDTO;
import com.mbn.sinod.model.dto.ConceptoDTO;
import com.mbn.sinod.model.entidades.Tsgnomconcepto;
import com.mbn.sinod.model.entidades.Tsgnomconcepto;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;


public class ConceptoServiceImpl extends BaseServiceImpl<Tsgnomconcepto, Integer> implements ConceptoService {
    
    private static final Logger logger = Logger.getLogger(ConceptoService.class.getName());
    
    @Override
    public ConceptoDTO listarConceptos() {
        ConceptoDTO respuesta = new ConceptoDTO();

        try {
            List<Tsgnomconcepto> listCatInci
                    = ((ConceptoDAO) getGenericDAO()).listarConcepto();

            if (listCatInci != null) {
                respuesta.setListConceptos(listCatInci);
//                respuesta.setCodigoMensaje(StaticConstantes.EXITO_OBTENER_LISTA);
                respuesta.setTipoMensaje(StaticConstantes.MENSAJE_CORRECTO);
            } else {
                listCatInci = new ArrayList();
                respuesta.setListConceptos(listCatInci);
//                respuesta.setCodigoMensaje(StaticConstantes.ERROR_OBTENER_LISTA);
                respuesta.setTipoMensaje(StaticConstantes.MENSAJE_ERROR);
            }
        } catch (Exception e) {
            respuesta = new ConceptoDTO();
            respuesta.setCodigoMensaje(e.getMessage());
            respuesta.setTipoMensaje(StaticConstantes.MENSAJE_ERROR);
            logger.getLogger(Tsgnomconcepto.class.getName()).log(Level.SEVERE, null, e);
        }

        return respuesta;
    }

    
}
