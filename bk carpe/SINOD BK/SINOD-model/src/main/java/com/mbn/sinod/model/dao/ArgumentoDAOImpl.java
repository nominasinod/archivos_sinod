/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mbn.sinod.model.dao;


import com.mbn.sinod.model.entidades.Tsgnomargumento;
import org.hibernate.Query;
import java.util.List;
import com.googlecode.genericdao.dao.hibernate.GenericDAOImpl;


/**
 *
 * @author mariana
 */
public class ArgumentoDAOImpl  extends GenericDAOImpl<Tsgnomargumento, Integer> implements ArgumentoDAO {

    @Override
    public List<Tsgnomargumento> obtenerListaArgumentos() {
       List<Tsgnomargumento> argumentos
                = (List<Tsgnomargumento>) getSession().createQuery("from Tsgnomargumento where bolEstatus = :parametro").setParameter("parametro", true).list();
        return argumentos;
    }

    @Override
    public boolean guardarActualizarArgumento(Tsgnomargumento argumento) {
        try {
            _saveOrUpdate(argumento);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public boolean eliminarArgumento(Integer id) {
        try
        {
            StringBuilder sb = new StringBuilder("UPDATE sgnom.tsgnomargumento SET bol_estatus = false WHERE cod_argumentoid = :argumentoId");       
            Query sql = getSession().createSQLQuery(sb.toString());
            sql.setParameter("argumentoId",id);
            
            if(sql.executeUpdate()>0)
            return true;
        }catch(Exception e)
        {
            return false;
        }
        
        return false;
    }
    
}
