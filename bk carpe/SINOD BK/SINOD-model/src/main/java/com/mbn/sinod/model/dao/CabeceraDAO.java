/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mbn.sinod.model.dao;

import com.googlecode.genericdao.dao.hibernate.GenericDAO;
import com.mbn.sinod.model.entidades.Tsgnomcabecera;
import java.util.List;

/**
 *
 * @author Mi Pe
 */
public interface CabeceraDAO extends GenericDAO<Tsgnomcabecera, Integer>{

    /**
     *
     * @return
     */
    List<Tsgnomcabecera> listarCabeceras();
    
}
