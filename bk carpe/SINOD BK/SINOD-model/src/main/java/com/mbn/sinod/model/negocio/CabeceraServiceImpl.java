/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mbn.sinod.model.negocio;

import com.mbn.sinod.model.comun.StaticConstantes;
import com.mbn.sinod.model.dao.CabeceraDAO;
import com.mbn.sinod.model.dto.CabeceraDTO;
import com.mbn.sinod.model.entidades.Tsgnomcabecera;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CabeceraServiceImpl extends BaseServiceImpl<Tsgnomcabecera, Integer>
        implements CabeceraService {

    private static final Logger logger = Logger.getLogger(CabeceraService.class.getName());

    @Override
    public CabeceraDTO listCabeceras() {
        CabeceraDTO respuesta = new CabeceraDTO();

        try {
            List<Tsgnomcabecera> listCatInci
                    = ((CabeceraDAO) getGenericDAO()).listarCabeceras();
            System.out.println(listCatInci);
            System.out.println(listCatInci);
            
            if (listCatInci != null) {
                respuesta.setListarCabecera(listCatInci);
                respuesta.getListarCabecera().get(0).getCodNbnomina();
//                respuesta.setCodigoMensaje(StaticConstantes.EXITO_OBTENER_LISTA);
                respuesta.setTipoMensaje(StaticConstantes.MENSAJE_CORRECTO);
            } else {
                listCatInci = new ArrayList();
                respuesta.setListarCabecera(listCatInci);
//                respuesta.setCodigoMensaje(StaticConstantes.ERROR_OBTENER_LISTA);
                respuesta.setTipoMensaje(StaticConstantes.MENSAJE_ERROR);
            }
        } catch (Exception e) {
            respuesta = new CabeceraDTO();
            respuesta.setCodigoMensaje(e.getMessage());
            respuesta.setTipoMensaje(StaticConstantes.MENSAJE_ERROR);
            logger.getLogger(Tsgnomcabecera.class.getName()).log(Level.SEVERE, null, e);
        }

        return respuesta;
    }
}
