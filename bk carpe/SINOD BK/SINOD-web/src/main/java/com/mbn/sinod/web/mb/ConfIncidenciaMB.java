/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mbn.sinod.web.mb;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.mbn.sinod.model.dto.CatIncidenciaDTO;
import com.mbn.sinod.model.entidades.Tsgnomcatincidencia;
import com.mbn.sinod.web.client.CatIncidenciaWSClient;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.inject.Named;
import org.apache.deltaspike.core.api.scope.ViewAccessScoped;
import org.primefaces.context.RequestContext;

/**
 *
 * @author Ambrosio Muñoz Flores
 */
@Named(value = "confIncidenciaMB") 
@ViewAccessScoped
public class ConfIncidenciaMB implements Serializable{
    private String titulo;
        
    private List<Tsgnomcatincidencia> listaCatIncidencias;
    private Tsgnomcatincidencia catIncidenciaSeleccionado;
    
    @PostConstruct 
    public void iniciarVariables() {
        listaCatIncidencias = new ArrayList();
        catIncidenciaSeleccionado = new Tsgnomcatincidencia();
        titulo = "";
        setListaCatIncidencias(CatIncidenciaWSClient.listarCatIncidencias());
        

    }
     
    public void dialogoCrearConfig(){
        setTitulo("Nueva");
        System.out.println("entra aqui");
        catIncidenciaSeleccionado = new Tsgnomcatincidencia();
        catIncidenciaSeleccionado.setBolEstatus(true); 
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('detalleConfIncidencia').show();");
    }
    
    public void dialogoEditarConfig(Tsgnomcatincidencia catIncidencia){
        setTitulo("Modificar");
        setCatIncidenciaSeleccionado(catIncidencia);
        System.out.println("editando " + getCatIncidenciaSeleccionado().getCodCatincidenciaid());
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('detalleConfIncidencia').show();");
    }
    
    public void dialogoEliminar(Tsgnomcatincidencia catIncidencia){
        setCatIncidenciaSeleccionado(catIncidencia);
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('eliminarConfIncidencia').show();");
    }
    
    public void guardar() throws JsonProcessingException{
        System.out.println("valor " + getCatIncidenciaSeleccionado().getCodNbincidencia());
        CatIncidenciaDTO dtoRespuesta = new CatIncidenciaDTO();
        dtoRespuesta.setCatIncidencia(getCatIncidenciaSeleccionado());
        dtoRespuesta = CatIncidenciaWSClient.guardarActualizarCatInci(dtoRespuesta);
    }
    
    public void eliminar() throws JsonProcessingException{
        CatIncidenciaWSClient.eliminarCatIncidenciasId(getCatIncidenciaSeleccionado().getCodCatincidenciaid());
    }
    
    /**
     * @return the titulo
     */
    public String getTitulo() {
        return titulo;
    }

    /**
     * @param titulo the titulo to set
     */
    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    /**
     * @return the listaCatIncidencias
     */
    public List<Tsgnomcatincidencia> getListaCatIncidencias() {
        return listaCatIncidencias;
    }

    /**
     * @param listaCatIncidencias the listaCatIncidencias to set
     */
    public void setListaCatIncidencias(List<Tsgnomcatincidencia> listaCatIncidencias) {
        this.listaCatIncidencias = listaCatIncidencias;
    }

    /**
     * @return the catIncidenciaSeleccionado
     */
    public Tsgnomcatincidencia getCatIncidenciaSeleccionado() {
        return catIncidenciaSeleccionado;
    }

    /**
     * @param catIncidenciaSeleccionado the catIncidenciaSeleccionado to set
     */
    public void setCatIncidenciaSeleccionado(Tsgnomcatincidencia catIncidenciaSeleccionado) {
        this.catIncidenciaSeleccionado = catIncidenciaSeleccionado;
    }
}
