/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mbn.sinod.web.mb;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.mbn.sinod.model.dto.ArgumentoDTO;
import com.mbn.sinod.model.entidades.Tsgnomargumento;
import com.mbn.sinod.web.client.ArgumentoWSClient;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import org.apache.deltaspike.core.api.scope.ViewAccessScoped;
import org.primefaces.context.RequestContext;

/**
 *
 * @author mariana
 */
@Named(value = "ConfArgumentosMB")
@ViewAccessScoped
public class ConfArgumentosMB implements Serializable{
   private String clave;
   private String descripcion;
   private String titulo;
   private Tsgnomargumento argumentoSeleccionado;
   private List<Tsgnomargumento> listaArgumentos;
   private List<ArgumentoDTO> listaArgumentosDTO;
   

   /*@PostConstruct
   public void iniciarVariables()
   {
       titulo = "";
       listaArgumentos = new ArrayList();
       argumentoSeleccionado  = new Tsgnomargumento();
       setListaArgumentos(ArgumentoWSClient.listarArgumentos());
   }*/
    @PostConstruct
    public void iniciarVariables() {
//        listaArgumentosDTO =  new ArrayList<>();
        setListaArgumentos(ArgumentoWSClient.listarArgumentos());
    }
   
    public void eliminarArgumento() throws JsonProcessingException
    {
        System.out.println("Holisss!!!!" );

        ArgumentoWSClient.eliminarArgumento(getArgumentoSeleccionado().getCodArgumentoid());
        System.out.println("clienteeeeeeeee!!!!" + getArgumentoSeleccionado().getCodArgumentoid());
        setListaArgumentos(ArgumentoWSClient.listarArgumentos());
    }
    public void guardar() throws JsonProcessingException{
       ArgumentoDTO dtoRespuesta = new ArgumentoDTO();
       argumentoSeleccionado.setBolEstatus(true);
       dtoRespuesta.setArgumento(getArgumentoSeleccionado());
       dtoRespuesta = ArgumentoWSClient.guardarActualizarArgumento(dtoRespuesta);
   }
   
    public void dialogoCrear() {
        argumentoSeleccionado = new Tsgnomargumento();
        argumentoSeleccionado.setCodArgumentoid(71);
        setTitulo("Nuevo argumento");
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('ArgumentoDialogo').show();");
    }
    
    public void dialogoEliminar(Tsgnomargumento argumento) {
        setArgumentoSeleccionado(argumento);
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('dialogEliminar').show();");
    }
    
     public void dialogoEditar() {
        setTitulo("Modificar argumento");
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('ArgumentoDialogo').show();");
    }
     
     public void limpiarDialogo()
     {
         setClave("");
         setDescripcion("");
         setTitulo("");
     }

     
      public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getClave() {
        return clave;
    }
    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Tsgnomargumento getArgumentoSeleccionado() {
        return argumentoSeleccionado;
    }

    public void setArgumentoSeleccionado(Tsgnomargumento argumentoSeleccionado) {
        this.argumentoSeleccionado = argumentoSeleccionado;
    }

    public List<Tsgnomargumento> getListaArgumentos() {
        return listaArgumentos;
    }

    public void setListaArgumentos(List<Tsgnomargumento> listaArgumentos) {
        this.listaArgumentos = listaArgumentos;
    }

    public List<ArgumentoDTO> getListaArgumentosDTO() {
        return listaArgumentosDTO;
    }

    public void setListaArgumentosDTO(List<ArgumentoDTO> listaArgumentosDTO) {
        this.listaArgumentosDTO = listaArgumentosDTO;
    }
    
     
}
