/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mbn.sinod.web.mb;

import com.mbn.sinod.model.entidades.Tsgnomcalculo;
import com.mbn.sinod.model.entidades.Tsgnomclasificador;
import com.mbn.sinod.model.entidades.Tsgnomconcepto;
import com.mbn.sinod.model.entidades.Tsgnomconceptosat;
import com.mbn.sinod.model.entidades.Tsgnomtipoconcepto;
import com.mbn.sinod.web.client.CatClasificadorWSClient;
import com.mbn.sinod.web.client.CatConceptosSATWSClient;
import com.mbn.sinod.web.client.ConceptoWSClient;
import com.mbn.sinod.web.client.TipoCalculoWSClient;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import javax.servlet.http.HttpServletRequest;
import org.apache.deltaspike.core.api.scope.ViewAccessScoped;
import org.primefaces.context.RequestContext;
import com.fasterxml.jackson.core.JsonProcessingException;


/**
 *
 * @author mariana
 */
@Named(value = "confConceptoMB")
@ViewAccessScoped
public class ConfConceptoMB implements Serializable {
    private Boolean visible;
    private String FormulaDOC;
    private String formula;
    private String expReg;
    private ArrayList<String> partes;
    private String OperadoresDOC;
    private String FuncionesDOC;
    private String ConstantesDOC;
    private String RetroactividadDOC;
    private String RetroAct;
    private Integer TipoCalculoDOC;
    private List<Tsgnomconcepto> listaConceptos;
    private List<Tsgnomclasificador> listaclasificadores;
    private List<Tsgnomconceptosat> listaconceptosat;
    private List<Tsgnomcalculo> listatipocalculo;
    private Tsgnomconcepto conceptoSeleccionado;
    private List<Tsgnomtipoconcepto> listaTipoConcepto;
    private Tsgnomcalculo calculoSeleccinado;
    private Double valor;
   // private Object operation;
    
    private List <Double> valores;
    
    @PostConstruct
    public void iniciarVariables() {
        setTipoCalculoDOC(0);
        setRetroAct("");
        setRetroactividadDOC("");
        setVisible(false);
        setFormulaDOC("");
        setExpReg("");
        setPartes(new ArrayList<String>());
        setListaConceptos(ConceptoWSClient.listarConceptos());
        setListaclasificadores(CatClasificadorWSClient.listarConceptos());
        setListaconceptosat(CatConceptosSATWSClient.listarConceptosSAT());
        setListatipocalculo(TipoCalculoWSClient.listarConceptosSAT());
        //setListaTipoConcepto(ConceptoWSClient.listarTipoConcepto());
        conceptoSeleccionado = new Tsgnomconcepto();
        valores = new ArrayList<Double>();
    }
      
    public void operadores(){
        if(getFormula() == null){
            setFormula(getOperadoresDOC());
            setExpReg(":".concat(getOperadoresDOC()));
            
        }else{
            setFormula(getFormula().concat(getOperadoresDOC()));
            setExpReg(getExpReg().concat(":".concat(getOperadoresDOC())));
        }
        setFormulaDOC(getFormula());
        setOperadoresDOC("");
    }
    
    public void funciones(){
        //System.out.println(getFuncionesDOC());
        if(getFormula() == null){
            setFormula(getFuncionesDOC());
            setExpReg(":".concat(getFuncionesDOC()));
            getPartes().add(getFuncionesDOC());
        }else{
            setFormula(getFormula().concat(getFuncionesDOC()));
            setExpReg(getExpReg().concat(":".concat(getFuncionesDOC())));
            getPartes().add(getFuncionesDOC());
        }
        setFormulaDOC(getFormula());
//        System.out.println(getExpReg());
        setFuncionesDOC("");
    }
    
    public void constantes(){
        //System.out.println(getConstantesDOC());
        if(getFormula() == null){
            setFormula(getConstantesDOC());
            setExpReg(":".concat(getConstantesDOC()));
            getPartes().add(getConstantesDOC());
        }else{
            setFormula(getFormula().concat(getConstantesDOC()));
            setExpReg(getExpReg().concat(":".concat(getConstantesDOC())));
            getPartes().add(getConstantesDOC());
        }
        setFormulaDOC(getFormula());
        setConstantesDOC("");
    }
    
    public void limpiarFormula(){
        setFormulaDOC("");
        setFormula("");
        setExpReg("");
        getPartes().clear();
        getValores().clear(); 
        setValor(0.0);
        valores.clear();
    }
    
    public void refrescarValores(int index)
    {
        System.out.println("indice" + index);
        //valores.set(index, getValor());
       
    }
    
    public void agregarValores() {

        valores.add(getValor());

        //<--!p:ajax process="@this"  listener="{confConceptoMB.agregarValores()}"/-->
    }
    
    public void calcularFormula()  {
        System.out.println("Valores " + valores.size());
        System.out.println("Partes " + partes.size());
        try {
            ScriptEngineManager manager = new ScriptEngineManager();
            //cargamos el codigo para la maquina que evalua
            ScriptEngine engine = manager.getEngineByName("js");
            for (int i = 0; i < getPartes().size(); i++) {
                engine.put(partes.get(i), valores.get(i));
            }
           Object resultado=engine.eval(getFormula());
            //Object operation2 = engine.eval("(X+3)");
           System.out.println("Evaluado operacion 1: " + resultado);
           
            //System.out.println("Evaluado operacion 2: " + operation2);
        }catch (ScriptException e) {
            System.out.println("Error en el script" + e.getMessage());
        }
    }
    
    public void eliminarConcepto() throws JsonProcessingException
    {
        System.out.println("Conceptossss!!!!" );

//        ConceptoWSClient.eliminarConcepto(getConceptoSeleccionado().getCodConceptoid());
        System.out.println("clienteeeeeeeee!!!!" + getConceptoSeleccionado().getCodConceptoid());
        setListaConceptos(ConceptoWSClient.listarConceptos());
    }
    
    
    
    
    public void tipoCalculo(){
        setTipoCalculoDOC(calculoSeleccinado.getCodCalculoid());
        //System.out.println(getTipoCalculoDOC());
        //getTipoCalculoDOC();
    }
    
    public void retroactividad(){
        System.out.println("retro  " + getRetroactividadDOC());
        getRetroactividadDOC();
    }
    
    public void retroAct(){
        System.out.println("gravdo  " + getRetroAct());
        getRetroAct();
    }
    
    public void dialogoCrear(){
        setVisible(true);
        conceptoSeleccionado = new Tsgnomconcepto();
    }
    
    public void dialogoEditar(Tsgnomconcepto seleccionado){
        setVisible(true);
        setConceptoSeleccionado(seleccionado);
    }
    
    public void dialogoEliminar(Tsgnomconcepto seleccionado){
        setConceptoSeleccionado(seleccionado);
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('eliminarConfConcepto').show();");
    }
    
    private void reload() throws IOException {
        ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
        ec.redirect(((HttpServletRequest) ec.getRequest()).getRequestURI());
    }

    /**
     * @return the FormulaDOC
     */
    public String getFormulaDOC() {
        return FormulaDOC;
    }

    /**
     * @param FormulaDOC the FormulaDOC to set
     */
    public void setFormulaDOC(String FormulaDOC) {
        this.FormulaDOC = FormulaDOC;
    }

    /**
     * @return the OperadoresDOC
     */
    public String getOperadoresDOC() {
        return OperadoresDOC;
    }

    /**
     * @param OperadoresDOC the OperadoresDOC to set
     */
    public void setOperadoresDOC(String OperadoresDOC) {
        this.OperadoresDOC = OperadoresDOC;
    }

    /**
     * @return the formula
     */
    public String getFormula() {
        return formula;
    }

    /**
     * @param formula the formula to set
     */
    public void setFormula(String formula) {
        this.formula = formula;
    }

    /**
     * @return the FuncionesDOC
     */
    public String getFuncionesDOC() {
        return FuncionesDOC;
    }

    /**
     * @param FuncionesDOC the FuncionesDOC to set
     */
    public void setFuncionesDOC(String FuncionesDOC) {
        this.FuncionesDOC = FuncionesDOC;
    }

    /**
     * @return the ConstantesDOC
     */
    public String getConstantesDOC() {
        return ConstantesDOC;
    }

    /**
     * @param ConstantesDOC the ConstantesDOC to set
     */
    public void setConstantesDOC(String ConstantesDOC) {
        this.ConstantesDOC = ConstantesDOC;
    }

    /**
     * @return the TipoCalculoDOC
     */
    public Integer getTipoCalculoDOC() {
        return TipoCalculoDOC;
    }

    /**
     * @param TipoCalculoDOC the TipoCalculoDOC to set
     */
    public void setTipoCalculoDOC(Integer TipoCalculoDOC) {
        this.TipoCalculoDOC = TipoCalculoDOC;
    }

    /**
     * @return the visible
     */
    public Boolean getVisible() {
        return visible;
    }

    /**
     * @param visible the visible to set
     */
    public void setVisible(Boolean visible) {
        this.visible = visible;
    }

    /**
     * @return the RetroactividadDOC
     */
    public String getRetroactividadDOC() {
        return RetroactividadDOC;
    }

    /**
     * @param RetroactividadDOC the RetroactividadDOC to set
     */
    public void setRetroactividadDOC(String RetroactividadDOC) {
        this.RetroactividadDOC = RetroactividadDOC;
    }

    /**
     * @return the RetroAct
     */
    public String getRetroAct() {
        return RetroAct;
    }

    /**
     * @param RetroAct the RetroAct to set
     */
    public void setRetroAct(String RetroAct) {
        this.RetroAct = RetroAct;
    }

    /**
     * @return the expReg
     */
    public String getExpReg() {
        return expReg;
    }

    /**
     * @param expReg the expReg to set
     */
    public void setExpReg(String expReg) {
        this.expReg = expReg;
    }

    /**
     * @return the partes
     */
    public ArrayList<String> getPartes() {
        return partes;
    }

    /**
     * @param partes the partes to set
     */
    public void setPartes(ArrayList<String> partes) {
        this.partes = partes;
    }

    /**
     * @return the listaConceptos
     */
    public List<Tsgnomconcepto> getListaConceptos() {
        return listaConceptos;
    }

    /**
     * @param listaConceptos the listaConceptos to set
     */
    public void setListaConceptos(List<Tsgnomconcepto> listaConceptos) {
        this.listaConceptos = listaConceptos;
    }

    /**
     * @return the conceptoSeleccionado
     */
    public Tsgnomconcepto getConceptoSeleccionado() {
        return conceptoSeleccionado;
    }

    /**
     * @param conceptoSeleccionado the conceptoSeleccionado to set
     */
    public void setConceptoSeleccionado(Tsgnomconcepto conceptoSeleccionado) {
        this.conceptoSeleccionado = conceptoSeleccionado;
    }

    /**
     * @return the listaclasificadores
     */
    public List<Tsgnomclasificador> getListaclasificadores() {
        return listaclasificadores;
    }

    /**
     * @param listaclasificadores the listaclasificadores to set
     */
    public void setListaclasificadores(List<Tsgnomclasificador> listaclasificadores) {
        this.listaclasificadores = listaclasificadores;
    }

    /**
     * @return the listaconceptosat
     */
    public List<Tsgnomconceptosat> getListaconceptosat() {
        return listaconceptosat;
    }

    /**
     * @param listaconceptosat the listaconceptosat to set
     */
    public void setListaconceptosat(List<Tsgnomconceptosat> listaconceptosat) {
        this.listaconceptosat = listaconceptosat;
    }

    /**
     * @return the listatipocalculo
     */
    public List<Tsgnomcalculo> getListatipocalculo() {
        return listatipocalculo;
    }

    /**
     * @param listatipocalculo the listatipocalculo to set
     */
    public void setListatipocalculo(List<Tsgnomcalculo> listatipocalculo) {
        this.listatipocalculo = listatipocalculo;
    }

    public List<Tsgnomtipoconcepto> getListaTipoConcepto() {
        return listaTipoConcepto;
    }

    public void setListaTipoConcepto(List<Tsgnomtipoconcepto> listaTipoConcepto) {
        this.listaTipoConcepto = listaTipoConcepto;
    }

    public Tsgnomcalculo getCalculoSeleccinado() {
        return calculoSeleccinado;
    }

    public void setCalculoSeleccinado(Tsgnomcalculo calculoSeleccinado) {
        this.calculoSeleccinado = calculoSeleccinado;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public List<Double> getValores() {
        return valores;
    }

    public void setValores(List<Double> valores) {
        this.valores = valores;
    }

   
    
    
    

    
    
    
}
