package com.mbn.sinod.web.mb;

import java.io.Serializable;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import org.apache.deltaspike.core.api.scope.ViewAccessScoped;
import org.apache.log4j.Logger;
import javax.inject.Named;
import org.primefaces.context.RequestContext;

@Named(value = "SelectBooleanView")
@ViewAccessScoped
public class SelectBooleanView implements Serializable {

    private final static Logger logger = Logger.getLogger(SelectBooleanView.class);
    private boolean value1;
    private boolean value2;

    public boolean isValue1() {
        return value1;
    }

    public void setValue1(boolean value1) {
        this.value1 = value1;
    }

    public boolean isValue2() {
        return value2;
    }

    public void setValue2(boolean value2) {
        this.value2 = value2;
    }
    
    public void mostrar(){
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('detalleIncidencia').show();");
    }
    
    public void addMessage() {
        String summary = value2 ? "Checked" : "Validado";
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(summary));
    }
}
