/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mbn.sinod.web.mb;

import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import org.apache.deltaspike.core.api.scope.ViewAccessScoped;
import org.primefaces.event.CloseEvent;
import org.primefaces.event.MoveEvent;

/**
 *
 * @author Luis
 */
@Named(value = "manualesTercerosMB")
@ViewAccessScoped
public class ManualesTercerosMB implements Serializable{
    private Integer Mov;
    
    @PostConstruct
    public void iniciarVariables(){
        Mov=0;
    }
    public void manuales(){
        getMov();
        System.out.println(getMov());
    }
    
    /**
     * @return the Mov
     */
    public Integer getMov() {
        return Mov;
    }

    /**
     * @param Mov the Mov to set
     */
    public void setMov(Integer Mov) {
        this.Mov = Mov;
    }
    
//    public void handleClose(CloseEvent event) {
//        addMessage(event.getComponent().getId() + " cerrar", "So you don't like nature?");
//    }
//     
//    public void handleMove(MoveEvent event) {
//        addMessage(event.getComponent().getId() + " Moved", "Left: " + event.getLeft() + ", Top: " + event.getTop());
//    }
//     
//    public void destroyWorld() {
//        addMessage("Error", "Intentelo mas tarde.");
//    }
//     
//    public void addMessage(String summary, String detail) {
//        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary, detail);
//        FacesContext.getCurrentInstance().addMessage(null, message);
//    }
    
  
}