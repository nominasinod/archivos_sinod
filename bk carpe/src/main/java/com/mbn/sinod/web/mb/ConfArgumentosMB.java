/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mbn.sinod.web.mb;
import java.io.Serializable;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import org.apache.deltaspike.core.api.scope.ViewAccessScoped;
import org.primefaces.context.RequestContext;
/**
 *
 * @author HP
 */

@Named(value = "ConfArgumentosMB")
@ViewAccessScoped
public class ConfArgumentosMB implements Serializable {
   private String clave;
   private String descripcion;
   
   private String titulo;

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getClave() {
        return clave;
    }
    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    public void dialogoCrear() {
        setTitulo("Nuevo argumento");
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('ArgumentoDialogo').show();");
    }
    
     public void dialogoEditar() {
        setTitulo("Modificar argumento");
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('ArgumentoDialogo').show();");
    }
     
     public void limpiarDialogo()
     {
         setClave("");
         setDescripcion("");
         setTitulo("");
     }


    public void dialogoEliminar() {
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('DialogoEliminar').show();");
    }
    
     public void guardar() {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Los datos han sido guardados")); 
    }
}
