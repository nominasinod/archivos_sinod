/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mbn.sinod.web.mb;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import javax.annotation.PostConstruct;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import org.apache.deltaspike.core.api.scope.ViewAccessScoped;
import org.primefaces.context.RequestContext;

/**
 *
 * @author Ambrosio Muñoz Flores
 */
@Named(value = "confConceptoMB")
@ViewAccessScoped
public class ConfConceptoMB implements Serializable{
    private Boolean visible;
    private String FormulaDOC;
    private String formula;
    private String expReg;
    private ArrayList<String> partes;
    private String OperadoresDOC;
    private String FuncionesDOC;
    private String ConstantesDOC;
    private String RetroactividadDOC;
    private String RetroAct;
    private Integer TipoCalculoDOC;
    
    @PostConstruct
    public void iniciarVariables() {
        setTipoCalculoDOC(0);
        setRetroAct("");
        setRetroactividadDOC("");
        
        setVisible(false);
        setFormulaDOC("");
        setExpReg("");
        setPartes(new ArrayList<String>());
    }
    
    public void operadores(){
        if(getFormula() == null){
            setFormula(getOperadoresDOC());
            setExpReg(":".concat(getOperadoresDOC()));
        }else{
            setFormula(getFormula().concat(getOperadoresDOC()));
            setExpReg(getExpReg().concat(":".concat(getOperadoresDOC())));
        }
        setFormulaDOC(getFormula());
        setOperadoresDOC("");
    }
    
    public void funciones(){
        //System.out.println(getFuncionesDOC());
        if(getFormula() == null){
            setFormula(getFuncionesDOC());
            setExpReg(":".concat(getFuncionesDOC()));
            getPartes().add(getFuncionesDOC());
        }else{
            setFormula(getFormula().concat(getFuncionesDOC()));
            setExpReg(getExpReg().concat(":".concat(getFuncionesDOC())));
            getPartes().add(getFuncionesDOC());
        }
        setFormulaDOC(getFormula());
//        System.out.println(getExpReg());
        setFuncionesDOC("");
    }
    
    public void constantes(){
        //System.out.println(getConstantesDOC());
        if(getFormula() == null){
            setFormula(getConstantesDOC());
            setExpReg(":".concat(getConstantesDOC()));
            getPartes().add(getConstantesDOC());
        }else{
            setFormula(getFormula().concat(getConstantesDOC()));
            setExpReg(getExpReg().concat(":".concat(getConstantesDOC())));
            getPartes().add(getConstantesDOC());
        }
        setFormulaDOC(getFormula());
        setConstantesDOC("");
    }
    
    public void limpiarFormula(){
        setFormulaDOC("");
        setFormula("");
        setExpReg("");
        getPartes().clear();
    }
    
    public void tipoCalculo(){
        //System.out.println(getTipoCalculoDOC());
        getTipoCalculoDOC();
    }
    
    public void retroactividad(){
        System.out.println("retro  " + getRetroactividadDOC());
        getRetroactividadDOC();
    }
    
    public void retroAct(){
        System.out.println("gravdo  " + getRetroAct());
        getRetroAct();
    }
    
    public void dialogoEliminar(){
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('eliminarConfConcepto').show();");
    }
    
    private void reload() throws IOException {
        ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
        ec.redirect(((HttpServletRequest) ec.getRequest()).getRequestURI());
    }

    /**
     * @return the FormulaDOC
     */
    public String getFormulaDOC() {
        return FormulaDOC;
    }

    /**
     * @param FormulaDOC the FormulaDOC to set
     */
    public void setFormulaDOC(String FormulaDOC) {
        this.FormulaDOC = FormulaDOC;
    }

    /**
     * @return the OperadoresDOC
     */
    public String getOperadoresDOC() {
        return OperadoresDOC;
    }

    /**
     * @param OperadoresDOC the OperadoresDOC to set
     */
    public void setOperadoresDOC(String OperadoresDOC) {
        this.OperadoresDOC = OperadoresDOC;
    }

    /**
     * @return the formula
     */
    public String getFormula() {
        return formula;
    }

    /**
     * @param formula the formula to set
     */
    public void setFormula(String formula) {
        this.formula = formula;
    }

    /**
     * @return the FuncionesDOC
     */
    public String getFuncionesDOC() {
        return FuncionesDOC;
    }

    /**
     * @param FuncionesDOC the FuncionesDOC to set
     */
    public void setFuncionesDOC(String FuncionesDOC) {
        this.FuncionesDOC = FuncionesDOC;
    }

    /**
     * @return the ConstantesDOC
     */
    public String getConstantesDOC() {
        return ConstantesDOC;
    }

    /**
     * @param ConstantesDOC the ConstantesDOC to set
     */
    public void setConstantesDOC(String ConstantesDOC) {
        this.ConstantesDOC = ConstantesDOC;
    }

    /**
     * @return the TipoCalculoDOC
     */
    public Integer getTipoCalculoDOC() {
        return TipoCalculoDOC;
    }

    /**
     * @param TipoCalculoDOC the TipoCalculoDOC to set
     */
    public void setTipoCalculoDOC(Integer TipoCalculoDOC) {
        this.TipoCalculoDOC = TipoCalculoDOC;
    }

    /**
     * @return the visible
     */
    public Boolean getVisible() {
        return visible;
    }

    /**
     * @param visible the visible to set
     */
    public void setVisible(Boolean visible) {
        this.visible = visible;
    }

    /**
     * @return the RetroactividadDOC
     */
    public String getRetroactividadDOC() {
        return RetroactividadDOC;
    }

    /**
     * @param RetroactividadDOC the RetroactividadDOC to set
     */
    public void setRetroactividadDOC(String RetroactividadDOC) {
        this.RetroactividadDOC = RetroactividadDOC;
    }

    /**
     * @return the RetroAct
     */
    public String getRetroAct() {
        return RetroAct;
    }

    /**
     * @param RetroAct the RetroAct to set
     */
    public void setRetroAct(String RetroAct) {
        this.RetroAct = RetroAct;
    }

    /**
     * @return the expReg
     */
    public String getExpReg() {
        return expReg;
    }

    /**
     * @param expReg the expReg to set
     */
    public void setExpReg(String expReg) {
        this.expReg = expReg;
    }

    /**
     * @return the partes
     */
    public ArrayList<String> getPartes() {
        return partes;
    }

    /**
     * @param partes the partes to set
     */
    public void setPartes(ArrayList<String> partes) {
        this.partes = partes;
    }

    
}
