/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mbn.sinod.web.mb;

import java.io.Serializable;
import java.util.Date;
import javax.inject.Named;
import org.apache.deltaspike.core.api.scope.ViewAccessScoped;

/**
 *
 * @author mariana
 */
@Named(value = "bean")
@ViewAccessScoped
public class bean implements Serializable{
    private Date date1;
    private Integer numberOfGuests; // +getter+setter
    private String[] guests; // +getter

    public void submit() {
        guests = new String[numberOfGuests];
    }

    public Integer getNumberOfGuests() {
        return numberOfGuests;
    }

    public void setNumberOfGuests(Integer numberOfGuests) {
        this.numberOfGuests = numberOfGuests;
    }

    public String[] getGuests() {
        return guests;
    }

    public void setGuests(String[] guests) {
        this.guests = guests;
    }
    
    
    public void save()
    {
        
    }

    /**
     * @return the date1
     */
    public Date getDate1() {
        return date1;
    }

    /**
     * @param date1 the date1 to set
     */
    public void setDate1(Date date1) {
        this.date1 = date1;
    }
    
    
    
    
    
}
