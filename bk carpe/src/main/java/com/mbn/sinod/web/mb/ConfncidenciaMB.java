/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mbn.sinod.web.mb;

import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import org.apache.deltaspike.core.api.scope.ViewAccessScoped;
import org.primefaces.context.RequestContext;

/**
 *
 * @author Ambrosio Muñoz Flores
 */
@Named(value = "confIncidenciaMB")
@ViewAccessScoped
public class ConfncidenciaMB implements Serializable{
    private String titulo;
    
    @PostConstruct
    public void iniciarVariables() {
        titulo = "";
    }
     
    public void dialogoCrearConfig(){
        setTitulo("Nueva");
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('detalleConfIncidencia').show();");
    }
    
    public void dialogoEditarConfig(){
        setTitulo("Modificar");
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('detalleConfIncidencia').show();");
    }
    
    public void dialogoEliminar(){
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('eliminarConfIncidencia').show();");
    }
    
    public void dialogoSiEliminar(){
        FacesContext.getCurrentInstance().addMessage("mensajes", new FacesMessage(FacesMessage.SEVERITY_INFO, "Operación exitosa", "Se ha aumentado la cantidad del pedido  " ));
    }

    /**
     * @return the titulo
     */
    public String getTitulo() {
        return titulo;
    }

    /**
     * @param titulo the titulo to set
     */
    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }
}
