--
-- PostgreSQL database dump
--

-- Dumped from database version 11.3 (Debian 11.3-1.pgdg90+1)
-- Dumped by pg_dump version 11.3 (Debian 11.3-1.pgdg90+1)

-- Started on 2019-05-18 13:55:48 CDT

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 10 (class 2615 OID 46838)
-- Name: sgnom; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA sgnom;


ALTER SCHEMA sgnom OWNER TO postgres;

--
-- TOC entry 3652 (class 0 OID 0)
-- Dependencies: 10
-- Name: SCHEMA sgnom; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON SCHEMA sgnom IS 'Sistema de Gestion de Nomina.';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 382 (class 1259 OID 46843)
-- Name: tsgnomalguinaldo; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnomalguinaldo (
    cod_aguinaldoid integer NOT NULL,
    imp_aguinaldo numeric(10,2) NOT NULL,
    cod_tipoaguinaldo character(1) NOT NULL,
    cod_empquincenaid_fk integer NOT NULL,
    bol_estatus boolean NOT NULL,
    xml_desgloce xml
);


ALTER TABLE sgnom.tsgnomalguinaldo OWNER TO suite;

--
-- TOC entry 3654 (class 0 OID 0)
-- Dependencies: 382
-- Name: TABLE tsgnomalguinaldo; Type: COMMENT; Schema: sgnom; Owner: suite
--

COMMENT ON TABLE sgnom.tsgnomalguinaldo IS 'cod_tipoaguinaldo

(

i = imss,

h = honorarios

)';


--
-- TOC entry 383 (class 1259 OID 46851)
-- Name: tsgnomargumento; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnomargumento (
    cod_argumentoid integer NOT NULL,
    cod_nbargumento character varying(30) NOT NULL,
    cod_clavearg character varying(5) NOT NULL,
    imp_valorconst numeric(10,2),
    des_funcionbd character varying(60),
    cod_tipoargumento character(1),
    bol_estatus boolean NOT NULL,
    txt_descripcion text NOT NULL,
    aud_codcreadopor integer NOT NULL,
    aud_codmodificadopor integer,
    aud_feccreacion date NOT NULL,
    aud_fecmodificacion date
);


ALTER TABLE sgnom.tsgnomargumento OWNER TO suite;

--
-- TOC entry 384 (class 1259 OID 46859)
-- Name: tsgnombitacora; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnombitacora (
    cod_bitacoraid integer NOT NULL,
    xml_bitacora xml NOT NULL,
    cod_tablaid_fk integer NOT NULL
);


ALTER TABLE sgnom.tsgnombitacora OWNER TO suite;

--
-- TOC entry 385 (class 1259 OID 46867)
-- Name: tsgnomcabecera; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnomcabecera (
    cod_cabeceraid integer NOT NULL,
    cod_nbnomina character varying(40) NOT NULL,
    fec_creacion date NOT NULL,
    fec_ejecucion date,
    fec_cierre date,
    imp_totpercepcion numeric(10,2),
    imp_totdeduccion numeric(10,2),
    imp_totalemp numeric(10,2),
    cod_quincenaid_fk integer NOT NULL,
    cod_tiponominaid_fk integer NOT NULL,
    cod_estatusnomid_fk integer NOT NULL,
    cnu_totalemp integer,
    aud_codcreadopor integer NOT NULL,
    aud_codmodificadopor integer,
    aud_feccreacion date NOT NULL,
    aud_fecmodificacion date
);


ALTER TABLE sgnom.tsgnomcabecera OWNER TO suite;

--
-- TOC entry 386 (class 1259 OID 46872)
-- Name: tsgnomcabeceraht; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnomcabeceraht (
    cod_cabeceraid integer NOT NULL,
    cod_nbnomina character varying(40) NOT NULL,
    fec_creacion date NOT NULL,
    fec_ejecucion date,
    fec_cierre date,
    imp_totpercepcion numeric(10,2),
    imp_totdeduccion numeric(10,2),
    imp_totalemp numeric(10,2),
    cod_quincenaid_fk integer NOT NULL,
    cod_tiponominaid_fk integer NOT NULL,
    cod_estatusnomid_fk integer NOT NULL,
    cnu_totalemp integer,
    aud_codcreadopor integer NOT NULL,
    aud_codmodificadopor integer,
    aud_feccreacion date NOT NULL,
    aud_fecmodificacion date
);


ALTER TABLE sgnom.tsgnomcabeceraht OWNER TO suite;

--
-- TOC entry 387 (class 1259 OID 46877)
-- Name: tsgnomcalculo; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnomcalculo (
    cod_calculoid integer NOT NULL,
    cod_tpcalculo character varying(25) NOT NULL,
    bol_estatus boolean NOT NULL
);


ALTER TABLE sgnom.tsgnomcalculo OWNER TO suite;

--
-- TOC entry 3660 (class 0 OID 0)
-- Dependencies: 387
-- Name: TABLE tsgnomcalculo; Type: COMMENT; Schema: sgnom; Owner: suite
--

COMMENT ON TABLE sgnom.tsgnomcalculo IS 'bol_estatus

(

activo, inactivo

)';


--
-- TOC entry 388 (class 1259 OID 46882)
-- Name: tsgnomcatincidencia; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnomcatincidencia (
    cod_catincidenciaid integer NOT NULL,
    cod_claveincidencia character varying(5),
    cod_nbincidencia character varying(20),
    cod_perfilincidencia character varying(25),
    bol_estatus boolean,
    cod_tipoincidencia character(1),
    imp_monto numeric(10,2)
);


ALTER TABLE sgnom.tsgnomcatincidencia OWNER TO suite;

--
-- TOC entry 3662 (class 0 OID 0)
-- Dependencies: 388
-- Name: TABLE tsgnomcatincidencia; Type: COMMENT; Schema: sgnom; Owner: suite
--

COMMENT ON TABLE sgnom.tsgnomcatincidencia IS 'cod_tipoincidencia

(

1 = horas

2 = dias

3 = actividad

)';


--
-- TOC entry 389 (class 1259 OID 46887)
-- Name: tsgnomclasificador; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnomclasificador (
    cod_clasificadorid integer NOT NULL,
    cod_tpclasificador character varying(20),
    bol_estatus boolean
);


ALTER TABLE sgnom.tsgnomclasificador OWNER TO suite;

--
-- TOC entry 390 (class 1259 OID 46892)
-- Name: tsgnomcncptoquinc; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnomcncptoquinc (
    cod_cncptoquincid integer NOT NULL,
    cod_empquincenaid_fk integer NOT NULL,
    cod_conceptoid_fk integer NOT NULL,
    imp_concepto numeric(10,2) NOT NULL,
    imp_gravado numeric(10,2),
    imp_exento numeric(10,2),
    xml_desgloce xml
);


ALTER TABLE sgnom.tsgnomcncptoquinc OWNER TO suite;

--
-- TOC entry 391 (class 1259 OID 46900)
-- Name: tsgnomcncptoquincht; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnomcncptoquincht (
    cod_cncptoquinchtid integer NOT NULL,
    cod_empquincenaid_fk integer NOT NULL,
    cod_conceptoid_fk integer NOT NULL,
    imp_concepto numeric(10,2) NOT NULL,
    imp_gravado numeric(10,2),
    imp_exento numeric(10,2),
    xml_desgloce xml
);


ALTER TABLE sgnom.tsgnomcncptoquincht OWNER TO suite;

--
-- TOC entry 392 (class 1259 OID 46908)
-- Name: tsgnomconcepto; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnomconcepto (
    cod_conceptoid integer NOT NULL,
    cod_nbconcepto character varying(20) NOT NULL,
    cod_claveconcepto character varying(4) NOT NULL,
    cnu_prioricalculo integer NOT NULL,
    cnu_articulo integer NOT NULL,
    bol_estatus boolean NOT NULL,
    cod_formulaid_fk integer,
    cod_tipoconceptoid_fk integer NOT NULL,
    cod_calculoid_fk integer NOT NULL,
    cod_conceptosatid_fk integer NOT NULL,
    cod_frecuenciapago character varying(20) NOT NULL,
    cod_partidaprep integer NOT NULL,
    cnu_cuentacontable integer NOT NULL,
    cod_gravado character(1),
    cod_excento character(1),
    bol_aplicaisn boolean,
    bol_retroactividad boolean NOT NULL,
    cnu_topeex integer,
    cod_clasificadorid_fk integer NOT NULL,
    cod_tiponominaid_fk integer,
    aud_codcreadopor integer NOT NULL,
    aud_codmodificadopor integer,
    aud_feccreacion date NOT NULL,
    aud_fecmodificacion date
);


ALTER TABLE sgnom.tsgnomconcepto OWNER TO suite;

--
-- TOC entry 3667 (class 0 OID 0)
-- Dependencies: 392
-- Name: TABLE tsgnomconcepto; Type: COMMENT; Schema: sgnom; Owner: suite
--

COMMENT ON TABLE sgnom.tsgnomconcepto IS 'bol_estatus

(

activo, inactivo

)



gravado

(

1 = dias, 

2 = porcentaje, 

3 = no aplica

)



excento

(

activo, inactivo

)





validar tope_ex';


--
-- TOC entry 393 (class 1259 OID 46913)
-- Name: tsgnomconceptosat; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnomconceptosat (
    cod_conceptosatid integer NOT NULL,
    des_conceptosat character varying(51) NOT NULL,
    des_descconcepto character varying(51) NOT NULL,
    bol_estatus boolean NOT NULL
);


ALTER TABLE sgnom.tsgnomconceptosat OWNER TO suite;

--
-- TOC entry 3669 (class 0 OID 0)
-- Dependencies: 393
-- Name: TABLE tsgnomconceptosat; Type: COMMENT; Schema: sgnom; Owner: suite
--

COMMENT ON TABLE sgnom.tsgnomconceptosat IS 'bol_estatus

(

activo, inactivo

)';


--
-- TOC entry 394 (class 1259 OID 46918)
-- Name: tsgnomconfpago; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnomconfpago (
    cod_confpagoid integer NOT NULL,
    bol_pagoempleado boolean,
    bol_pagorh boolean,
    bol_pagofinanzas boolean,
    cod_empquincenaid_fk integer
);


ALTER TABLE sgnom.tsgnomconfpago OWNER TO suite;

--
-- TOC entry 3671 (class 0 OID 0)
-- Dependencies: 394
-- Name: TABLE tsgnomconfpago; Type: COMMENT; Schema: sgnom; Owner: suite
--

COMMENT ON TABLE sgnom.tsgnomconfpago IS 'bol_pagoempleado

(

confirmado, pendiente 

)



pago_rh

(

autorizado, pendiente

)



pago_fnzas

(

autorizado, pendiente

)';


--
-- TOC entry 395 (class 1259 OID 46923)
-- Name: tsgnomejercicio; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnomejercicio (
    cod_ejercicioid integer NOT NULL,
    cnu_valorejercicio integer NOT NULL,
    bol_estatus boolean NOT NULL
);


ALTER TABLE sgnom.tsgnomejercicio OWNER TO suite;

--
-- TOC entry 396 (class 1259 OID 46928)
-- Name: tsgnomempleados; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnomempleados (
    cod_empleadoid integer NOT NULL,
    fec_ingreso date NOT NULL,
    fec_salida date,
    bol_estatus boolean NOT NULL,
    cod_empleado_fk integer NOT NULL,
    imp_sueldoimss numeric(10,2),
    imp_honorarios numeric(10,2),
    imp_finiquito numeric(10,2),
    cod_tipoimss character(1),
    cod_tipohonorarios character(1),
    cod_banco character varying(50),
    cod_sucursal integer,
    cod_cuenta character varying(20),
    txt_descripcionbaja text,
    aud_codcreadopor integer NOT NULL,
    aud_feccreacion date NOT NULL,
    aud_codmodificadopor integer,
    aud_fecmodificacion date,
    cod_clave character varying(18)
);


ALTER TABLE sgnom.tsgnomempleados OWNER TO suite;

--
-- TOC entry 3674 (class 0 OID 0)
-- Dependencies: 396
-- Name: TABLE tsgnomempleados; Type: COMMENT; Schema: sgnom; Owner: suite
--

COMMENT ON TABLE sgnom.tsgnomempleados IS 'cod_empleado_fk hace referencia al schema sgrh en la tabla tsgrhempleados';


--
-- TOC entry 397 (class 1259 OID 46936)
-- Name: tsgnomempquincena; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnomempquincena (
    cod_empquincenaid integer NOT NULL,
    cod_empleadoid_fk integer NOT NULL,
    cod_cabeceraid_fk integer NOT NULL,
    imp_totpercepcion numeric(10,2) NOT NULL,
    imp_totdeduccion numeric(10,2) NOT NULL,
    imp_totalemp numeric(10,2) NOT NULL,
    bol_estatusemp boolean NOT NULL
);


ALTER TABLE sgnom.tsgnomempquincena OWNER TO suite;

--
-- TOC entry 3676 (class 0 OID 0)
-- Dependencies: 397
-- Name: TABLE tsgnomempquincena; Type: COMMENT; Schema: sgnom; Owner: suite
--

COMMENT ON TABLE sgnom.tsgnomempquincena IS 'bol_estatusemp

(

activo, inactivo

)';


--
-- TOC entry 398 (class 1259 OID 46941)
-- Name: tsgnomempquincenaht; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnomempquincenaht (
    cod_empquincenahtid integer NOT NULL,
    cod_empleadoid_fk integer NOT NULL,
    cod_cabeceraid_fk integer NOT NULL,
    imp_totpercepcion numeric(10,2) NOT NULL,
    imp_totdeduccion numeric(10,2) NOT NULL,
    imp_totalemp numeric(10,2) NOT NULL,
    bol_estatusemp boolean NOT NULL
);


ALTER TABLE sgnom.tsgnomempquincenaht OWNER TO suite;

--
-- TOC entry 399 (class 1259 OID 46946)
-- Name: tsgnomestatusnom; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnomestatusnom (
    cod_estatusnomid integer NOT NULL,
    cod_estatusnomina character varying(15) NOT NULL,
    bol_estatus boolean NOT NULL
);


ALTER TABLE sgnom.tsgnomestatusnom OWNER TO suite;

--
-- TOC entry 3679 (class 0 OID 0)
-- Dependencies: 399
-- Name: TABLE tsgnomestatusnom; Type: COMMENT; Schema: sgnom; Owner: suite
--

COMMENT ON TABLE sgnom.tsgnomestatusnom IS 'cod_estatusnomid 

estatus

(

abierta,

calculada,

revision,

validada,

cerrada

)



nota: en caso de no ser validada entra en revision y posteriormente calcular nuevamente 

hasta ser validada';


--
-- TOC entry 400 (class 1259 OID 46951)
-- Name: tsgnomformula; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnomformula (
    cod_formulaid integer NOT NULL,
    des_nbformula character varying(60) NOT NULL,
    des_formula character varying(250) NOT NULL,
    bol_estatus boolean NOT NULL
);


ALTER TABLE sgnom.tsgnomformula OWNER TO suite;

--
-- TOC entry 3681 (class 0 OID 0)
-- Dependencies: 400
-- Name: TABLE tsgnomformula; Type: COMMENT; Schema: sgnom; Owner: suite
--

COMMENT ON TABLE sgnom.tsgnomformula IS 'bol_estatus

(

activo, inactivo

)';


--
-- TOC entry 401 (class 1259 OID 46956)
-- Name: tsgnomfuncion; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnomfuncion (
    cod_funcionid integer NOT NULL,
    cod_nbfuncion character varying(15) NOT NULL,
    bol_estatus boolean NOT NULL
);


ALTER TABLE sgnom.tsgnomfuncion OWNER TO suite;

--
-- TOC entry 402 (class 1259 OID 46961)
-- Name: tsgnomhisttabla; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnomhisttabla (
    cod_tablaid integer NOT NULL,
    cod_nbtabla character varying(18) NOT NULL,
    bol_estatus boolean NOT NULL
);


ALTER TABLE sgnom.tsgnomhisttabla OWNER TO suite;

--
-- TOC entry 403 (class 1259 OID 46966)
-- Name: tsgnomincidencia; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnomincidencia (
    cod_incidenciaid integer NOT NULL,
    cod_catincidenciaid_fk integer NOT NULL,
    cnu_cantidad smallint,
    des_actividad character varying(100),
    txt_comentarios text,
    cod_empreporta_fk integer,
    cod_empautoriza_fk integer,
    imp_monto numeric(10,2),
    xml_detcantidad xml,
    bol_estatus boolean,
    cod_quincenaid_fk integer NOT NULL,
    bol_validacion boolean,
    fec_validacion date,
    aud_codcreadopor integer NOT NULL,
    aud_feccreacion date NOT NULL,
    aud_codmodificadopor integer,
    aud_fecmodificacion date
);


ALTER TABLE sgnom.tsgnomincidencia OWNER TO suite;

--
-- TOC entry 3685 (class 0 OID 0)
-- Dependencies: 403
-- Name: TABLE tsgnomincidencia; Type: COMMENT; Schema: sgnom; Owner: suite
--

COMMENT ON TABLE sgnom.tsgnomincidencia IS 'bol_estatus: (activo, inactivo) (1, 0)

validacion: (validar, denegar) (1, 0) 



';


--
-- TOC entry 404 (class 1259 OID 46974)
-- Name: tsgnommanterceros; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnommanterceros (
    cod_mantercerosid integer NOT NULL,
    cod_conceptoid_fk integer,
    imp_monto numeric(10,2),
    cod_quincenainicio_fk integer,
    cod_quincenafin_fk integer,
    cod_empleadoid_fk integer,
    cod_frecuenciapago character varying(20),
    bol_estatus boolean,
    aud_codcreadopor integer NOT NULL,
    aud_codmodificadopor integer,
    aud_fecreacion date NOT NULL,
    aud_fecmodificacion date
);


ALTER TABLE sgnom.tsgnommanterceros OWNER TO suite;

--
-- TOC entry 405 (class 1259 OID 46979)
-- Name: tsgnomquincena; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnomquincena (
    cod_quincenaid integer NOT NULL,
    des_quincena character varying(70) NOT NULL,
    fec_inicio date NOT NULL,
    fec_fin date NOT NULL,
    fec_pago date NOT NULL,
    fec_dispersion date NOT NULL,
    cnu_numquincena integer NOT NULL,
    cod_ejercicioid_fk integer NOT NULL,
    bol_estatus boolean NOT NULL
);


ALTER TABLE sgnom.tsgnomquincena OWNER TO suite;

--
-- TOC entry 406 (class 1259 OID 46984)
-- Name: tsgnomtipoconcepto; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnomtipoconcepto (
    cod_tipoconceptoid integer NOT NULL,
    cod_tipoconcepto character varying(25) NOT NULL,
    bol_estatus boolean NOT NULL
);


ALTER TABLE sgnom.tsgnomtipoconcepto OWNER TO suite;

--
-- TOC entry 3689 (class 0 OID 0)
-- Dependencies: 406
-- Name: TABLE tsgnomtipoconcepto; Type: COMMENT; Schema: sgnom; Owner: suite
--

COMMENT ON TABLE sgnom.tsgnomtipoconcepto IS 'bol_estatus

(

activo, inactivo

)';


--
-- TOC entry 407 (class 1259 OID 46989)
-- Name: tsgnomtiponomina; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnomtiponomina (
    cod_tiponominaid integer NOT NULL,
    cod_nomina character varying(30) NOT NULL,
    bol_estatus boolean NOT NULL
);


ALTER TABLE sgnom.tsgnomtiponomina OWNER TO suite;

--
-- TOC entry 3691 (class 0 OID 0)
-- Dependencies: 407
-- Name: TABLE tsgnomtiponomina; Type: COMMENT; Schema: sgnom; Owner: suite
--

COMMENT ON TABLE sgnom.tsgnomtiponomina IS 'bol_estatus (activa, inactiva)';


--
-- TOC entry 3621 (class 0 OID 46843)
-- Dependencies: 382
-- Data for Name: tsgnomalguinaldo; Type: TABLE DATA; Schema: sgnom; Owner: suite
--



--
-- TOC entry 3622 (class 0 OID 46851)
-- Dependencies: 383
-- Data for Name: tsgnomargumento; Type: TABLE DATA; Schema: sgnom; Owner: suite
--



--
-- TOC entry 3623 (class 0 OID 46859)
-- Dependencies: 384
-- Data for Name: tsgnombitacora; Type: TABLE DATA; Schema: sgnom; Owner: suite
--



--
-- TOC entry 3624 (class 0 OID 46867)
-- Dependencies: 385
-- Data for Name: tsgnomcabecera; Type: TABLE DATA; Schema: sgnom; Owner: suite
--



--
-- TOC entry 3625 (class 0 OID 46872)
-- Dependencies: 386
-- Data for Name: tsgnomcabeceraht; Type: TABLE DATA; Schema: sgnom; Owner: suite
--



--
-- TOC entry 3626 (class 0 OID 46877)
-- Dependencies: 387
-- Data for Name: tsgnomcalculo; Type: TABLE DATA; Schema: sgnom; Owner: suite
--

INSERT INTO sgnom.tsgnomcalculo (cod_calculoid, cod_tpcalculo, bol_estatus) VALUES (1, 'Importe', true);
INSERT INTO sgnom.tsgnomcalculo (cod_calculoid, cod_tpcalculo, bol_estatus) VALUES (2, 'Calculo', true);


--
-- TOC entry 3627 (class 0 OID 46882)
-- Dependencies: 388
-- Data for Name: tsgnomcatincidencia; Type: TABLE DATA; Schema: sgnom; Owner: suite
--

INSERT INTO sgnom.tsgnomcatincidencia (cod_catincidenciaid, cod_claveincidencia, cod_nbincidencia, cod_perfilincidencia, bol_estatus, cod_tipoincidencia, imp_monto) VALUES (2, NULL, NULL, NULL, true, NULL, NULL);
INSERT INTO sgnom.tsgnomcatincidencia (cod_catincidenciaid, cod_claveincidencia, cod_nbincidencia, cod_perfilincidencia, bol_estatus, cod_tipoincidencia, imp_monto) VALUES (1, 'gngmg', 'gngn', 'xxxx', true, NULL, 100.00);
INSERT INTO sgnom.tsgnomcatincidencia (cod_catincidenciaid, cod_claveincidencia, cod_nbincidencia, cod_perfilincidencia, bol_estatus, cod_tipoincidencia, imp_monto) VALUES (10, 'post', 'postman', 'por', true, 'a', NULL);
INSERT INTO sgnom.tsgnomcatincidencia (cod_catincidenciaid, cod_claveincidencia, cod_nbincidencia, cod_perfilincidencia, bol_estatus, cod_tipoincidencia, imp_monto) VALUES (15, 'post', 'postman', 'por', true, 'a', NULL);
INSERT INTO sgnom.tsgnomcatincidencia (cod_catincidenciaid, cod_claveincidencia, cod_nbincidencia, cod_perfilincidencia, bol_estatus, cod_tipoincidencia, imp_monto) VALUES (16, 'post', 'postman', 'por', true, 'a', NULL);
INSERT INTO sgnom.tsgnomcatincidencia (cod_catincidenciaid, cod_claveincidencia, cod_nbincidencia, cod_perfilincidencia, bol_estatus, cod_tipoincidencia, imp_monto) VALUES (17, 'post', 'postman', 'por', true, 'a', NULL);
INSERT INTO sgnom.tsgnomcatincidencia (cod_catincidenciaid, cod_claveincidencia, cod_nbincidencia, cod_perfilincidencia, bol_estatus, cod_tipoincidencia, imp_monto) VALUES (18, 'post', 'postman', 'por', true, 'a', NULL);
INSERT INTO sgnom.tsgnomcatincidencia (cod_catincidenciaid, cod_claveincidencia, cod_nbincidencia, cod_perfilincidencia, bol_estatus, cod_tipoincidencia, imp_monto) VALUES (19, 'post', 'postman', 'por', true, 'a', NULL);
INSERT INTO sgnom.tsgnomcatincidencia (cod_catincidenciaid, cod_claveincidencia, cod_nbincidencia, cod_perfilincidencia, bol_estatus, cod_tipoincidencia, imp_monto) VALUES (20, 'post', 'postman', 'por', true, 'a', NULL);
INSERT INTO sgnom.tsgnomcatincidencia (cod_catincidenciaid, cod_claveincidencia, cod_nbincidencia, cod_perfilincidencia, bol_estatus, cod_tipoincidencia, imp_monto) VALUES (21, 'post', 'postman', 'por', true, 'a', NULL);
INSERT INTO sgnom.tsgnomcatincidencia (cod_catincidenciaid, cod_claveincidencia, cod_nbincidencia, cod_perfilincidencia, bol_estatus, cod_tipoincidencia, imp_monto) VALUES (22, 'post', 'postman', 'por', true, 'a', NULL);
INSERT INTO sgnom.tsgnomcatincidencia (cod_catincidenciaid, cod_claveincidencia, cod_nbincidencia, cod_perfilincidencia, bol_estatus, cod_tipoincidencia, imp_monto) VALUES (23, 'post', 'postman', 'por', true, 'a', NULL);
INSERT INTO sgnom.tsgnomcatincidencia (cod_catincidenciaid, cod_claveincidencia, cod_nbincidencia, cod_perfilincidencia, bol_estatus, cod_tipoincidencia, imp_monto) VALUES (24, 'post', 'postman', 'por', true, 'a', NULL);
INSERT INTO sgnom.tsgnomcatincidencia (cod_catincidenciaid, cod_claveincidencia, cod_nbincidencia, cod_perfilincidencia, bol_estatus, cod_tipoincidencia, imp_monto) VALUES (25, 'post', 'postman', 'por', true, 'a', NULL);
INSERT INTO sgnom.tsgnomcatincidencia (cod_catincidenciaid, cod_claveincidencia, cod_nbincidencia, cod_perfilincidencia, bol_estatus, cod_tipoincidencia, imp_monto) VALUES (26, 'post', 'postman', 'por', true, 'a', NULL);
INSERT INTO sgnom.tsgnomcatincidencia (cod_catincidenciaid, cod_claveincidencia, cod_nbincidencia, cod_perfilincidencia, bol_estatus, cod_tipoincidencia, imp_monto) VALUES (27, 'post', 'postman', 'por', true, 'a', NULL);
INSERT INTO sgnom.tsgnomcatincidencia (cod_catincidenciaid, cod_claveincidencia, cod_nbincidencia, cod_perfilincidencia, bol_estatus, cod_tipoincidencia, imp_monto) VALUES (28, 'post', 'postman', 'por', true, 'a', NULL);
INSERT INTO sgnom.tsgnomcatincidencia (cod_catincidenciaid, cod_claveincidencia, cod_nbincidencia, cod_perfilincidencia, bol_estatus, cod_tipoincidencia, imp_monto) VALUES (29, 'post', 'postman', 'por', true, 'a', NULL);


--
-- TOC entry 3628 (class 0 OID 46887)
-- Dependencies: 389
-- Data for Name: tsgnomclasificador; Type: TABLE DATA; Schema: sgnom; Owner: suite
--



--
-- TOC entry 3629 (class 0 OID 46892)
-- Dependencies: 390
-- Data for Name: tsgnomcncptoquinc; Type: TABLE DATA; Schema: sgnom; Owner: suite
--



--
-- TOC entry 3630 (class 0 OID 46900)
-- Dependencies: 391
-- Data for Name: tsgnomcncptoquincht; Type: TABLE DATA; Schema: sgnom; Owner: suite
--



--
-- TOC entry 3631 (class 0 OID 46908)
-- Dependencies: 392
-- Data for Name: tsgnomconcepto; Type: TABLE DATA; Schema: sgnom; Owner: suite
--



--
-- TOC entry 3632 (class 0 OID 46913)
-- Dependencies: 393
-- Data for Name: tsgnomconceptosat; Type: TABLE DATA; Schema: sgnom; Owner: suite
--



--
-- TOC entry 3633 (class 0 OID 46918)
-- Dependencies: 394
-- Data for Name: tsgnomconfpago; Type: TABLE DATA; Schema: sgnom; Owner: suite
--



--
-- TOC entry 3634 (class 0 OID 46923)
-- Dependencies: 395
-- Data for Name: tsgnomejercicio; Type: TABLE DATA; Schema: sgnom; Owner: suite
--



--
-- TOC entry 3635 (class 0 OID 46928)
-- Dependencies: 396
-- Data for Name: tsgnomempleados; Type: TABLE DATA; Schema: sgnom; Owner: suite
--



--
-- TOC entry 3636 (class 0 OID 46936)
-- Dependencies: 397
-- Data for Name: tsgnomempquincena; Type: TABLE DATA; Schema: sgnom; Owner: suite
--



--
-- TOC entry 3637 (class 0 OID 46941)
-- Dependencies: 398
-- Data for Name: tsgnomempquincenaht; Type: TABLE DATA; Schema: sgnom; Owner: suite
--



--
-- TOC entry 3638 (class 0 OID 46946)
-- Dependencies: 399
-- Data for Name: tsgnomestatusnom; Type: TABLE DATA; Schema: sgnom; Owner: suite
--



--
-- TOC entry 3639 (class 0 OID 46951)
-- Dependencies: 400
-- Data for Name: tsgnomformula; Type: TABLE DATA; Schema: sgnom; Owner: suite
--



--
-- TOC entry 3640 (class 0 OID 46956)
-- Dependencies: 401
-- Data for Name: tsgnomfuncion; Type: TABLE DATA; Schema: sgnom; Owner: suite
--



--
-- TOC entry 3641 (class 0 OID 46961)
-- Dependencies: 402
-- Data for Name: tsgnomhisttabla; Type: TABLE DATA; Schema: sgnom; Owner: suite
--



--
-- TOC entry 3642 (class 0 OID 46966)
-- Dependencies: 403
-- Data for Name: tsgnomincidencia; Type: TABLE DATA; Schema: sgnom; Owner: suite
--



--
-- TOC entry 3643 (class 0 OID 46974)
-- Dependencies: 404
-- Data for Name: tsgnommanterceros; Type: TABLE DATA; Schema: sgnom; Owner: suite
--



--
-- TOC entry 3644 (class 0 OID 46979)
-- Dependencies: 405
-- Data for Name: tsgnomquincena; Type: TABLE DATA; Schema: sgnom; Owner: suite
--



--
-- TOC entry 3645 (class 0 OID 46984)
-- Dependencies: 406
-- Data for Name: tsgnomtipoconcepto; Type: TABLE DATA; Schema: sgnom; Owner: suite
--



--
-- TOC entry 3646 (class 0 OID 46989)
-- Dependencies: 407
-- Data for Name: tsgnomtiponomina; Type: TABLE DATA; Schema: sgnom; Owner: suite
--



--
-- TOC entry 3414 (class 2606 OID 46886)
-- Name: tsgnomcatincidencia cat_incidencia_id; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomcatincidencia
    ADD CONSTRAINT cat_incidencia_id PRIMARY KEY (cod_catincidenciaid);


--
-- TOC entry 3402 (class 2606 OID 46850)
-- Name: tsgnomalguinaldo nom_aguinaldo_id; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomalguinaldo
    ADD CONSTRAINT nom_aguinaldo_id PRIMARY KEY (cod_aguinaldoid);


--
-- TOC entry 3406 (class 2606 OID 46866)
-- Name: tsgnombitacora nom_bitacora_id; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnombitacora
    ADD CONSTRAINT nom_bitacora_id PRIMARY KEY (cod_bitacoraid);


--
-- TOC entry 3410 (class 2606 OID 46876)
-- Name: tsgnomcabeceraht nom_cabecera_copia_id; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomcabeceraht
    ADD CONSTRAINT nom_cabecera_copia_id PRIMARY KEY (cod_cabeceraid);


--
-- TOC entry 3408 (class 2606 OID 46871)
-- Name: tsgnomcabecera nom_cabecera_id; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomcabecera
    ADD CONSTRAINT nom_cabecera_id PRIMARY KEY (cod_cabeceraid);


--
-- TOC entry 3404 (class 2606 OID 46858)
-- Name: tsgnomargumento nom_cat_argumento_id; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomargumento
    ADD CONSTRAINT nom_cat_argumento_id PRIMARY KEY (cod_argumentoid);


--
-- TOC entry 3422 (class 2606 OID 46912)
-- Name: tsgnomconcepto nom_cat_concepto_id; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomconcepto
    ADD CONSTRAINT nom_cat_concepto_id PRIMARY KEY (cod_conceptoid);


--
-- TOC entry 3424 (class 2606 OID 46917)
-- Name: tsgnomconceptosat nom_cat_concepto_sat_id; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomconceptosat
    ADD CONSTRAINT nom_cat_concepto_sat_id PRIMARY KEY (cod_conceptosatid);


--
-- TOC entry 3428 (class 2606 OID 46927)
-- Name: tsgnomejercicio nom_cat_ejercicio_id; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomejercicio
    ADD CONSTRAINT nom_cat_ejercicio_id PRIMARY KEY (cod_ejercicioid);


--
-- TOC entry 3436 (class 2606 OID 46950)
-- Name: tsgnomestatusnom nom_cat_estatus_nomina_id; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomestatusnom
    ADD CONSTRAINT nom_cat_estatus_nomina_id PRIMARY KEY (cod_estatusnomid);


--
-- TOC entry 3438 (class 2606 OID 46955)
-- Name: tsgnomformula nom_cat_formula_id; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomformula
    ADD CONSTRAINT nom_cat_formula_id PRIMARY KEY (cod_formulaid);


--
-- TOC entry 3440 (class 2606 OID 46960)
-- Name: tsgnomfuncion nom_cat_funcion_id; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomfuncion
    ADD CONSTRAINT nom_cat_funcion_id PRIMARY KEY (cod_funcionid);


--
-- TOC entry 3448 (class 2606 OID 46983)
-- Name: tsgnomquincena nom_cat_quincena_id; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomquincena
    ADD CONSTRAINT nom_cat_quincena_id PRIMARY KEY (cod_quincenaid);


--
-- TOC entry 3442 (class 2606 OID 46965)
-- Name: tsgnomhisttabla nom_cat_tabla_id; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomhisttabla
    ADD CONSTRAINT nom_cat_tabla_id PRIMARY KEY (cod_tablaid);


--
-- TOC entry 3412 (class 2606 OID 46881)
-- Name: tsgnomcalculo nom_cat_tipo_calculo_id; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomcalculo
    ADD CONSTRAINT nom_cat_tipo_calculo_id PRIMARY KEY (cod_calculoid);


--
-- TOC entry 3416 (class 2606 OID 46891)
-- Name: tsgnomclasificador nom_cat_tipo_clasificador_id; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomclasificador
    ADD CONSTRAINT nom_cat_tipo_clasificador_id PRIMARY KEY (cod_clasificadorid);


--
-- TOC entry 3450 (class 2606 OID 46988)
-- Name: tsgnomtipoconcepto nom_cat_tipo_conepto_id; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomtipoconcepto
    ADD CONSTRAINT nom_cat_tipo_conepto_id PRIMARY KEY (cod_tipoconceptoid);


--
-- TOC entry 3452 (class 2606 OID 46993)
-- Name: tsgnomtiponomina nom_cat_tipo_nomina_id; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomtiponomina
    ADD CONSTRAINT nom_cat_tipo_nomina_id PRIMARY KEY (cod_tiponominaid);


--
-- TOC entry 3420 (class 2606 OID 46907)
-- Name: tsgnomcncptoquincht nom_conceptos_quincena_copia_id; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomcncptoquincht
    ADD CONSTRAINT nom_conceptos_quincena_copia_id PRIMARY KEY (cod_cncptoquinchtid);


--
-- TOC entry 3418 (class 2606 OID 46899)
-- Name: tsgnomcncptoquinc nom_conceptos_quincena_id; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomcncptoquinc
    ADD CONSTRAINT nom_conceptos_quincena_id PRIMARY KEY (cod_cncptoquincid);


--
-- TOC entry 3426 (class 2606 OID 46922)
-- Name: tsgnomconfpago nom_conf_pago_pkey; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomconfpago
    ADD CONSTRAINT nom_conf_pago_pkey PRIMARY KEY (cod_confpagoid);


--
-- TOC entry 3430 (class 2606 OID 46935)
-- Name: tsgnomempleados nom_empleado_id; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomempleados
    ADD CONSTRAINT nom_empleado_id PRIMARY KEY (cod_empleadoid);


--
-- TOC entry 3434 (class 2606 OID 46945)
-- Name: tsgnomempquincenaht nom_empleado_quincena_copia_id; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomempquincenaht
    ADD CONSTRAINT nom_empleado_quincena_copia_id PRIMARY KEY (cod_empquincenahtid);


--
-- TOC entry 3432 (class 2606 OID 46940)
-- Name: tsgnomempquincena nom_empleado_quincena_id; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomempquincena
    ADD CONSTRAINT nom_empleado_quincena_id PRIMARY KEY (cod_empquincenaid);


--
-- TOC entry 3444 (class 2606 OID 46973)
-- Name: tsgnomincidencia nom_incidencia_id; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomincidencia
    ADD CONSTRAINT nom_incidencia_id PRIMARY KEY (cod_incidenciaid);


--
-- TOC entry 3446 (class 2606 OID 46978)
-- Name: tsgnommanterceros nom_manuales_terceros_pkey; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnommanterceros
    ADD CONSTRAINT nom_manuales_terceros_pkey PRIMARY KEY (cod_mantercerosid);


--
-- TOC entry 3469 (class 2606 OID 47044)
-- Name: tsgnomcncptoquincht concepto_quincena_id_copia_fk_conceptos_quincena; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomcncptoquincht
    ADD CONSTRAINT concepto_quincena_id_copia_fk_conceptos_quincena FOREIGN KEY (cod_conceptoid_fk) REFERENCES sgnom.tsgnomconcepto(cod_conceptoid) ON UPDATE CASCADE;


--
-- TOC entry 3467 (class 2606 OID 47034)
-- Name: tsgnomcncptoquinc concepto_quincena_id_fk_conceptos_quincena; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomcncptoquinc
    ADD CONSTRAINT concepto_quincena_id_fk_conceptos_quincena FOREIGN KEY (cod_conceptoid_fk) REFERENCES sgnom.tsgnomconcepto(cod_conceptoid) ON UPDATE CASCADE;


--
-- TOC entry 3470 (class 2606 OID 47049)
-- Name: tsgnomcncptoquincht empleado_concepto_id_copia_fk_conceptos_quincena; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomcncptoquincht
    ADD CONSTRAINT empleado_concepto_id_copia_fk_conceptos_quincena FOREIGN KEY (cod_empquincenaid_fk) REFERENCES sgnom.tsgnomempquincena(cod_empquincenaid) ON UPDATE CASCADE;


--
-- TOC entry 3468 (class 2606 OID 47039)
-- Name: tsgnomcncptoquinc empleado_concepto_id_fk_conceptos_quincena; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomcncptoquinc
    ADD CONSTRAINT empleado_concepto_id_fk_conceptos_quincena FOREIGN KEY (cod_empquincenaid_fk) REFERENCES sgnom.tsgnomempquincena(cod_empquincenaid) ON UPDATE CASCADE;


--
-- TOC entry 3483 (class 2606 OID 47084)
-- Name: tsgnomempquincena nom_cabecera_id_fk_empleados_quincena; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomempquincena
    ADD CONSTRAINT nom_cabecera_id_fk_empleados_quincena FOREIGN KEY (cod_cabeceraid_fk) REFERENCES sgnom.tsgnomcabecera(cod_cabeceraid) ON UPDATE CASCADE;


--
-- TOC entry 3485 (class 2606 OID 47094)
-- Name: tsgnomempquincenaht nom_cabecera_id_fk_empleados_quincena_copia; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomempquincenaht
    ADD CONSTRAINT nom_cabecera_id_fk_empleados_quincena_copia FOREIGN KEY (cod_cabeceraid_fk) REFERENCES sgnom.tsgnomcabecera(cod_cabeceraid) ON UPDATE CASCADE;


--
-- TOC entry 3471 (class 2606 OID 47054)
-- Name: tsgnomconcepto nom_cat_clasificador_id_fk_cat_conceptos; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomconcepto
    ADD CONSTRAINT nom_cat_clasificador_id_fk_cat_conceptos FOREIGN KEY (cod_clasificadorid_fk) REFERENCES sgnom.tsgnomclasificador(cod_clasificadorid) ON UPDATE CASCADE;


--
-- TOC entry 3495 (class 2606 OID 47227)
-- Name: tsgnommanterceros nom_cat_conceptos_fk_manuales_terceros; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnommanterceros
    ADD CONSTRAINT nom_cat_conceptos_fk_manuales_terceros FOREIGN KEY (cod_conceptoid_fk) REFERENCES sgnom.tsgnomconcepto(cod_conceptoid) ON UPDATE CASCADE;


--
-- TOC entry 3499 (class 2606 OID 47144)
-- Name: tsgnomquincena nom_cat_ejercicio_id_fk_cat_quincenas; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomquincena
    ADD CONSTRAINT nom_cat_ejercicio_id_fk_cat_quincenas FOREIGN KEY (cod_ejercicioid_fk) REFERENCES sgnom.tsgnomejercicio(cod_ejercicioid) ON UPDATE CASCADE;


--
-- TOC entry 3462 (class 2606 OID 47019)
-- Name: tsgnomcabeceraht nom_cat_estatus_nomina_id_copia_fk_cabeceras; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomcabeceraht
    ADD CONSTRAINT nom_cat_estatus_nomina_id_copia_fk_cabeceras FOREIGN KEY (cod_estatusnomid_fk) REFERENCES sgnom.tsgnomestatusnom(cod_estatusnomid) ON UPDATE CASCADE;


--
-- TOC entry 3457 (class 2606 OID 47004)
-- Name: tsgnomcabecera nom_cat_estatus_nomina_id_fk_cabeceras; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomcabecera
    ADD CONSTRAINT nom_cat_estatus_nomina_id_fk_cabeceras FOREIGN KEY (cod_estatusnomid_fk) REFERENCES sgnom.tsgnomestatusnom(cod_estatusnomid) ON UPDATE CASCADE;


--
-- TOC entry 3487 (class 2606 OID 47104)
-- Name: tsgnomincidencia nom_cat_incidencia_id_fk_incidencias; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomincidencia
    ADD CONSTRAINT nom_cat_incidencia_id_fk_incidencias FOREIGN KEY (cod_catincidenciaid_fk) REFERENCES sgnom.tsgnomcatincidencia(cod_catincidenciaid) ON UPDATE CASCADE;


--
-- TOC entry 3463 (class 2606 OID 47024)
-- Name: tsgnomcabeceraht nom_cat_quincena_id_copia_fk_cabeceras; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomcabeceraht
    ADD CONSTRAINT nom_cat_quincena_id_copia_fk_cabeceras FOREIGN KEY (cod_quincenaid_fk) REFERENCES sgnom.tsgnomquincena(cod_quincenaid) ON UPDATE CASCADE;


--
-- TOC entry 3458 (class 2606 OID 47009)
-- Name: tsgnomcabecera nom_cat_quincena_id_fk_cabeceras; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomcabecera
    ADD CONSTRAINT nom_cat_quincena_id_fk_cabeceras FOREIGN KEY (cod_quincenaid_fk) REFERENCES sgnom.tsgnomquincena(cod_quincenaid) ON UPDATE CASCADE;


--
-- TOC entry 3488 (class 2606 OID 47109)
-- Name: tsgnomincidencia nom_cat_quincena_id_fk_incidencias; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomincidencia
    ADD CONSTRAINT nom_cat_quincena_id_fk_incidencias FOREIGN KEY (cod_quincenaid_fk) REFERENCES sgnom.tsgnomquincena(cod_quincenaid) ON UPDATE CASCADE;


--
-- TOC entry 3496 (class 2606 OID 47232)
-- Name: tsgnommanterceros nom_cat_quincenas_fk_manuales_terceros_fin; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnommanterceros
    ADD CONSTRAINT nom_cat_quincenas_fk_manuales_terceros_fin FOREIGN KEY (cod_quincenafin_fk) REFERENCES sgnom.tsgnomquincena(cod_quincenaid) ON UPDATE CASCADE;


--
-- TOC entry 3497 (class 2606 OID 47237)
-- Name: tsgnommanterceros nom_cat_quincenas_fk_manuales_terceros_inicio; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnommanterceros
    ADD CONSTRAINT nom_cat_quincenas_fk_manuales_terceros_inicio FOREIGN KEY (cod_quincenainicio_fk) REFERENCES sgnom.tsgnomquincena(cod_quincenaid) ON UPDATE CASCADE;


--
-- TOC entry 3456 (class 2606 OID 46999)
-- Name: tsgnombitacora nom_cat_tabla_id_fk_nom_cat_tablas; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnombitacora
    ADD CONSTRAINT nom_cat_tabla_id_fk_nom_cat_tablas FOREIGN KEY (cod_tablaid_fk) REFERENCES sgnom.tsgnomhisttabla(cod_tablaid) ON UPDATE CASCADE;


--
-- TOC entry 3464 (class 2606 OID 47029)
-- Name: tsgnomcabeceraht nom_cat_tipo_nomina_id_copia_fk_cabeceras; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomcabeceraht
    ADD CONSTRAINT nom_cat_tipo_nomina_id_copia_fk_cabeceras FOREIGN KEY (cod_tiponominaid_fk) REFERENCES sgnom.tsgnomtiponomina(cod_tiponominaid) ON UPDATE CASCADE;


--
-- TOC entry 3459 (class 2606 OID 47014)
-- Name: tsgnomcabecera nom_cat_tipo_nomina_id_fk_cabeceras; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomcabecera
    ADD CONSTRAINT nom_cat_tipo_nomina_id_fk_cabeceras FOREIGN KEY (cod_tiponominaid_fk) REFERENCES sgnom.tsgnomtiponomina(cod_tiponominaid) ON UPDATE CASCADE;


--
-- TOC entry 3472 (class 2606 OID 47059)
-- Name: tsgnomconcepto nom_concepto_sat_id_fk_cat_conceptos; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomconcepto
    ADD CONSTRAINT nom_concepto_sat_id_fk_cat_conceptos FOREIGN KEY (cod_conceptosatid_fk) REFERENCES sgnom.tsgnomconceptosat(cod_conceptosatid) ON UPDATE CASCADE;


--
-- TOC entry 3489 (class 2606 OID 47114)
-- Name: tsgnomincidencia nom_emp_autoriza_fk_incidencias; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomincidencia
    ADD CONSTRAINT nom_emp_autoriza_fk_incidencias FOREIGN KEY (cod_empautoriza_fk) REFERENCES sgnom.tsgnomempleados(cod_empleadoid) ON UPDATE CASCADE;


--
-- TOC entry 3490 (class 2606 OID 47119)
-- Name: tsgnomincidencia nom_emp_reporta_fk_incidencias; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomincidencia
    ADD CONSTRAINT nom_emp_reporta_fk_incidencias FOREIGN KEY (cod_empreporta_fk) REFERENCES sgnom.tsgnomempleados(cod_empleadoid) ON UPDATE CASCADE;


--
-- TOC entry 3484 (class 2606 OID 47089)
-- Name: tsgnomempquincena nom_empleado_quincena_id_fk_empleados_quincena; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomempquincena
    ADD CONSTRAINT nom_empleado_quincena_id_fk_empleados_quincena FOREIGN KEY (cod_empleadoid_fk) REFERENCES sgnom.tsgnomempleados(cod_empleadoid) ON UPDATE CASCADE;


--
-- TOC entry 3486 (class 2606 OID 47099)
-- Name: tsgnomempquincenaht nom_empleado_quincena_id_fk_empleados_quincena_copia; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomempquincenaht
    ADD CONSTRAINT nom_empleado_quincena_id_fk_empleados_quincena_copia FOREIGN KEY (cod_empleadoid_fk) REFERENCES sgnom.tsgnomempleados(cod_empleadoid) ON UPDATE CASCADE;


--
-- TOC entry 3498 (class 2606 OID 47242)
-- Name: tsgnommanterceros nom_empleados_fk_manuales_terceros; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnommanterceros
    ADD CONSTRAINT nom_empleados_fk_manuales_terceros FOREIGN KEY (cod_empleadoid_fk) REFERENCES sgnom.tsgnomempleados(cod_empleadoid) ON UPDATE CASCADE;


--
-- TOC entry 3453 (class 2606 OID 46994)
-- Name: tsgnomalguinaldo nom_empleados_quincena_fk_aguinaldos; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomalguinaldo
    ADD CONSTRAINT nom_empleados_quincena_fk_aguinaldos FOREIGN KEY (cod_empquincenaid_fk) REFERENCES sgnom.tsgnomempquincena(cod_empquincenaid) ON UPDATE CASCADE;


--
-- TOC entry 3479 (class 2606 OID 47079)
-- Name: tsgnomconfpago nom_empleados_quincena_fk_conf_pago; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomconfpago
    ADD CONSTRAINT nom_empleados_quincena_fk_conf_pago FOREIGN KEY (cod_empquincenaid_fk) REFERENCES sgnom.tsgnomempquincena(cod_empquincenaid) ON UPDATE CASCADE;


--
-- TOC entry 3473 (class 2606 OID 47064)
-- Name: tsgnomconcepto nom_formula_id_fk_cat_conceptos; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomconcepto
    ADD CONSTRAINT nom_formula_id_fk_cat_conceptos FOREIGN KEY (cod_formulaid_fk) REFERENCES sgnom.tsgnomformula(cod_formulaid) ON UPDATE CASCADE;


--
-- TOC entry 3474 (class 2606 OID 47069)
-- Name: tsgnomconcepto nom_tipo_calculo_id_fk_cat_conceptos; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomconcepto
    ADD CONSTRAINT nom_tipo_calculo_id_fk_cat_conceptos FOREIGN KEY (cod_calculoid_fk) REFERENCES sgnom.tsgnomcalculo(cod_calculoid) ON UPDATE CASCADE;


--
-- TOC entry 3475 (class 2606 OID 47074)
-- Name: tsgnomconcepto nom_tipo_concepto_id_fk_cat_conceptos; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomconcepto
    ADD CONSTRAINT nom_tipo_concepto_id_fk_cat_conceptos FOREIGN KEY (cod_tipoconceptoid_fk) REFERENCES sgnom.tsgnomtipoconcepto(cod_tipoconceptoid) ON UPDATE CASCADE;


--
-- TOC entry 3476 (class 2606 OID 47149)
-- Name: tsgnomconcepto nom_tipo_nomina_id_fk_cat_conceptos; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomconcepto
    ADD CONSTRAINT nom_tipo_nomina_id_fk_cat_conceptos FOREIGN KEY (cod_tiponominaid_fk) REFERENCES sgnom.tsgnomtiponomina(cod_tiponominaid) ON UPDATE CASCADE;


--
-- TOC entry 3465 (class 2606 OID 47165)
-- Name: tsgnomcabeceraht tsgrh_empleados_id_copia_fk_cabeceras_c; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomcabeceraht
    ADD CONSTRAINT tsgrh_empleados_id_copia_fk_cabeceras_c FOREIGN KEY (aud_codcreadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 3466 (class 2606 OID 47170)
-- Name: tsgnomcabeceraht tsgrh_empleados_id_copia_fk_cabeceras_m; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomcabeceraht
    ADD CONSTRAINT tsgrh_empleados_id_copia_fk_cabeceras_m FOREIGN KEY (aud_codmodificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 3454 (class 2606 OID 47247)
-- Name: tsgnomargumento tsgrh_empleados_id_fk_argumento_c; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomargumento
    ADD CONSTRAINT tsgrh_empleados_id_fk_argumento_c FOREIGN KEY (aud_codcreadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 3455 (class 2606 OID 47252)
-- Name: tsgnomargumento tsgrh_empleados_id_fk_argumento_m; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomargumento
    ADD CONSTRAINT tsgrh_empleados_id_fk_argumento_m FOREIGN KEY (aud_codmodificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 3460 (class 2606 OID 47155)
-- Name: tsgnomcabecera tsgrh_empleados_id_fk_cabeceras_c; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomcabecera
    ADD CONSTRAINT tsgrh_empleados_id_fk_cabeceras_c FOREIGN KEY (aud_codcreadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 3461 (class 2606 OID 47160)
-- Name: tsgnomcabecera tsgrh_empleados_id_fk_cabeceras_m; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomcabecera
    ADD CONSTRAINT tsgrh_empleados_id_fk_cabeceras_m FOREIGN KEY (aud_codmodificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 3480 (class 2606 OID 47195)
-- Name: tsgnomempleados tsgrh_empleados_id_fk_cabeceras_m; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomempleados
    ADD CONSTRAINT tsgrh_empleados_id_fk_cabeceras_m FOREIGN KEY (aud_codmodificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 3477 (class 2606 OID 47175)
-- Name: tsgnomconcepto tsgrh_empleados_id_fk_cat_conceptos_c; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomconcepto
    ADD CONSTRAINT tsgrh_empleados_id_fk_cat_conceptos_c FOREIGN KEY (aud_codcreadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 3478 (class 2606 OID 47180)
-- Name: tsgnomconcepto tsgrh_empleados_id_fk_cat_conceptos_m; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomconcepto
    ADD CONSTRAINT tsgrh_empleados_id_fk_cat_conceptos_m FOREIGN KEY (aud_codmodificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 3481 (class 2606 OID 47185)
-- Name: tsgnomempleados tsgrh_empleados_id_fk_empleados; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomempleados
    ADD CONSTRAINT tsgrh_empleados_id_fk_empleados FOREIGN KEY (cod_empleado_fk) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 3482 (class 2606 OID 47190)
-- Name: tsgnomempleados tsgrh_empleados_id_fk_empleados_c; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomempleados
    ADD CONSTRAINT tsgrh_empleados_id_fk_empleados_c FOREIGN KEY (aud_codcreadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 3492 (class 2606 OID 47205)
-- Name: tsgnomincidencia tsgrh_empleados_id_fk_incidencias_c; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomincidencia
    ADD CONSTRAINT tsgrh_empleados_id_fk_incidencias_c FOREIGN KEY (aud_codcreadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 3491 (class 2606 OID 47200)
-- Name: tsgnomincidencia tsgrh_empleados_id_fk_incidencias_m; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomincidencia
    ADD CONSTRAINT tsgrh_empleados_id_fk_incidencias_m FOREIGN KEY (aud_codmodificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 3493 (class 2606 OID 47210)
-- Name: tsgnommanterceros tsgrh_empleados_id_fk_manuales_terceros_c; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnommanterceros
    ADD CONSTRAINT tsgrh_empleados_id_fk_manuales_terceros_c FOREIGN KEY (aud_codcreadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 3494 (class 2606 OID 47215)
-- Name: tsgnommanterceros tsgrh_empleados_id_fk_manuales_terceros_m; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnommanterceros
    ADD CONSTRAINT tsgrh_empleados_id_fk_manuales_terceros_m FOREIGN KEY (aud_codmodificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 3653 (class 0 OID 0)
-- Dependencies: 10
-- Name: SCHEMA sgnom; Type: ACL; Schema: -; Owner: postgres
--

GRANT ALL ON SCHEMA sgnom TO suite WITH GRANT OPTION;


--
-- TOC entry 3655 (class 0 OID 0)
-- Dependencies: 382
-- Name: TABLE tsgnomalguinaldo; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnomalguinaldo FROM suite;
GRANT ALL ON TABLE sgnom.tsgnomalguinaldo TO suite WITH GRANT OPTION;


--
-- TOC entry 3656 (class 0 OID 0)
-- Dependencies: 383
-- Name: TABLE tsgnomargumento; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnomargumento FROM suite;
GRANT ALL ON TABLE sgnom.tsgnomargumento TO suite WITH GRANT OPTION;


--
-- TOC entry 3657 (class 0 OID 0)
-- Dependencies: 384
-- Name: TABLE tsgnombitacora; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnombitacora FROM suite;
GRANT ALL ON TABLE sgnom.tsgnombitacora TO suite WITH GRANT OPTION;


--
-- TOC entry 3658 (class 0 OID 0)
-- Dependencies: 385
-- Name: TABLE tsgnomcabecera; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnomcabecera FROM suite;
GRANT ALL ON TABLE sgnom.tsgnomcabecera TO suite WITH GRANT OPTION;


--
-- TOC entry 3659 (class 0 OID 0)
-- Dependencies: 386
-- Name: TABLE tsgnomcabeceraht; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnomcabeceraht FROM suite;
GRANT ALL ON TABLE sgnom.tsgnomcabeceraht TO suite WITH GRANT OPTION;


--
-- TOC entry 3661 (class 0 OID 0)
-- Dependencies: 387
-- Name: TABLE tsgnomcalculo; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnomcalculo FROM suite;
GRANT ALL ON TABLE sgnom.tsgnomcalculo TO suite WITH GRANT OPTION;


--
-- TOC entry 3663 (class 0 OID 0)
-- Dependencies: 388
-- Name: TABLE tsgnomcatincidencia; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnomcatincidencia FROM suite;
GRANT ALL ON TABLE sgnom.tsgnomcatincidencia TO suite WITH GRANT OPTION;


--
-- TOC entry 3664 (class 0 OID 0)
-- Dependencies: 389
-- Name: TABLE tsgnomclasificador; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnomclasificador FROM suite;
GRANT ALL ON TABLE sgnom.tsgnomclasificador TO suite WITH GRANT OPTION;


--
-- TOC entry 3665 (class 0 OID 0)
-- Dependencies: 390
-- Name: TABLE tsgnomcncptoquinc; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnomcncptoquinc FROM suite;
GRANT ALL ON TABLE sgnom.tsgnomcncptoquinc TO suite WITH GRANT OPTION;


--
-- TOC entry 3666 (class 0 OID 0)
-- Dependencies: 391
-- Name: TABLE tsgnomcncptoquincht; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnomcncptoquincht FROM suite;
GRANT ALL ON TABLE sgnom.tsgnomcncptoquincht TO suite WITH GRANT OPTION;


--
-- TOC entry 3668 (class 0 OID 0)
-- Dependencies: 392
-- Name: TABLE tsgnomconcepto; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnomconcepto FROM suite;
GRANT ALL ON TABLE sgnom.tsgnomconcepto TO suite WITH GRANT OPTION;


--
-- TOC entry 3670 (class 0 OID 0)
-- Dependencies: 393
-- Name: TABLE tsgnomconceptosat; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnomconceptosat FROM suite;
GRANT ALL ON TABLE sgnom.tsgnomconceptosat TO suite WITH GRANT OPTION;


--
-- TOC entry 3672 (class 0 OID 0)
-- Dependencies: 394
-- Name: TABLE tsgnomconfpago; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnomconfpago FROM suite;
GRANT ALL ON TABLE sgnom.tsgnomconfpago TO suite WITH GRANT OPTION;


--
-- TOC entry 3673 (class 0 OID 0)
-- Dependencies: 395
-- Name: TABLE tsgnomejercicio; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnomejercicio FROM suite;
GRANT ALL ON TABLE sgnom.tsgnomejercicio TO suite WITH GRANT OPTION;


--
-- TOC entry 3675 (class 0 OID 0)
-- Dependencies: 396
-- Name: TABLE tsgnomempleados; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnomempleados FROM suite;
GRANT ALL ON TABLE sgnom.tsgnomempleados TO suite WITH GRANT OPTION;


--
-- TOC entry 3677 (class 0 OID 0)
-- Dependencies: 397
-- Name: TABLE tsgnomempquincena; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnomempquincena FROM suite;
GRANT ALL ON TABLE sgnom.tsgnomempquincena TO suite WITH GRANT OPTION;


--
-- TOC entry 3678 (class 0 OID 0)
-- Dependencies: 398
-- Name: TABLE tsgnomempquincenaht; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnomempquincenaht FROM suite;
GRANT ALL ON TABLE sgnom.tsgnomempquincenaht TO suite WITH GRANT OPTION;


--
-- TOC entry 3680 (class 0 OID 0)
-- Dependencies: 399
-- Name: TABLE tsgnomestatusnom; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnomestatusnom FROM suite;
GRANT ALL ON TABLE sgnom.tsgnomestatusnom TO suite WITH GRANT OPTION;


--
-- TOC entry 3682 (class 0 OID 0)
-- Dependencies: 400
-- Name: TABLE tsgnomformula; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnomformula FROM suite;
GRANT ALL ON TABLE sgnom.tsgnomformula TO suite WITH GRANT OPTION;


--
-- TOC entry 3683 (class 0 OID 0)
-- Dependencies: 401
-- Name: TABLE tsgnomfuncion; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnomfuncion FROM suite;
GRANT ALL ON TABLE sgnom.tsgnomfuncion TO suite WITH GRANT OPTION;


--
-- TOC entry 3684 (class 0 OID 0)
-- Dependencies: 402
-- Name: TABLE tsgnomhisttabla; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnomhisttabla FROM suite;
GRANT ALL ON TABLE sgnom.tsgnomhisttabla TO suite WITH GRANT OPTION;


--
-- TOC entry 3686 (class 0 OID 0)
-- Dependencies: 403
-- Name: TABLE tsgnomincidencia; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnomincidencia FROM suite;
GRANT ALL ON TABLE sgnom.tsgnomincidencia TO suite WITH GRANT OPTION;


--
-- TOC entry 3687 (class 0 OID 0)
-- Dependencies: 404
-- Name: TABLE tsgnommanterceros; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnommanterceros FROM suite;
GRANT ALL ON TABLE sgnom.tsgnommanterceros TO suite WITH GRANT OPTION;


--
-- TOC entry 3688 (class 0 OID 0)
-- Dependencies: 405
-- Name: TABLE tsgnomquincena; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnomquincena FROM suite;
GRANT ALL ON TABLE sgnom.tsgnomquincena TO suite WITH GRANT OPTION;


--
-- TOC entry 3690 (class 0 OID 0)
-- Dependencies: 406
-- Name: TABLE tsgnomtipoconcepto; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnomtipoconcepto FROM suite;
GRANT ALL ON TABLE sgnom.tsgnomtipoconcepto TO suite WITH GRANT OPTION;


--
-- TOC entry 3692 (class 0 OID 0)
-- Dependencies: 407
-- Name: TABLE tsgnomtiponomina; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnomtiponomina FROM suite;
GRANT ALL ON TABLE sgnom.tsgnomtiponomina TO suite WITH GRANT OPTION;


--
-- TOC entry 2335 (class 826 OID 46840)
-- Name: DEFAULT PRIVILEGES FOR SEQUENCES; Type: DEFAULT ACL; Schema: sgnom; Owner: postgres
--

ALTER DEFAULT PRIVILEGES FOR ROLE postgres IN SCHEMA sgnom REVOKE ALL ON SEQUENCES  FROM postgres;
ALTER DEFAULT PRIVILEGES FOR ROLE postgres IN SCHEMA sgnom GRANT SELECT,USAGE ON SEQUENCES  TO suite WITH GRANT OPTION;


--
-- TOC entry 2337 (class 826 OID 46842)
-- Name: DEFAULT PRIVILEGES FOR TYPES; Type: DEFAULT ACL; Schema: sgnom; Owner: postgres
--

ALTER DEFAULT PRIVILEGES FOR ROLE postgres IN SCHEMA sgnom REVOKE ALL ON TYPES  FROM PUBLIC;
ALTER DEFAULT PRIVILEGES FOR ROLE postgres IN SCHEMA sgnom REVOKE ALL ON TYPES  FROM postgres;
ALTER DEFAULT PRIVILEGES FOR ROLE postgres IN SCHEMA sgnom GRANT ALL ON TYPES  TO suite WITH GRANT OPTION;


--
-- TOC entry 2336 (class 826 OID 46841)
-- Name: DEFAULT PRIVILEGES FOR FUNCTIONS; Type: DEFAULT ACL; Schema: sgnom; Owner: postgres
--

ALTER DEFAULT PRIVILEGES FOR ROLE postgres IN SCHEMA sgnom REVOKE ALL ON FUNCTIONS  FROM PUBLIC;
ALTER DEFAULT PRIVILEGES FOR ROLE postgres IN SCHEMA sgnom REVOKE ALL ON FUNCTIONS  FROM postgres;
ALTER DEFAULT PRIVILEGES FOR ROLE postgres IN SCHEMA sgnom GRANT ALL ON FUNCTIONS  TO suite WITH GRANT OPTION;


--
-- TOC entry 2334 (class 826 OID 46839)
-- Name: DEFAULT PRIVILEGES FOR TABLES; Type: DEFAULT ACL; Schema: sgnom; Owner: postgres
--

ALTER DEFAULT PRIVILEGES FOR ROLE postgres IN SCHEMA sgnom REVOKE ALL ON TABLES  FROM postgres;
ALTER DEFAULT PRIVILEGES FOR ROLE postgres IN SCHEMA sgnom GRANT ALL ON TABLES  TO suite WITH GRANT OPTION;


-- Completed on 2019-05-18 13:55:49 CDT

--
-- PostgreSQL database dump complete
--

