--
-- PostgreSQL database dump
--

-- Dumped from database version 11.5
-- Dumped by pg_dump version 11.5

-- Started on 2019-11-21 11:34:15

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 11 (class 2615 OID 97139)
-- Name: sgnom; Type: SCHEMA; Schema: -; Owner: suite
--

CREATE SCHEMA sgnom;


ALTER SCHEMA sgnom OWNER TO suite;

--
-- TOC entry 3774 (class 0 OID 0)
-- Dependencies: 11
-- Name: SCHEMA sgnom; Type: COMMENT; Schema: -; Owner: suite
--

COMMENT ON SCHEMA sgnom IS 'Sistema de Gestion de Nomina.';


--
-- TOC entry 479 (class 1255 OID 97141)
-- Name: actualizar_comentarios_incidencia(integer, text, integer); Type: FUNCTION; Schema: sgnom; Owner: suite
--

CREATE FUNCTION sgnom.actualizar_comentarios_incidencia(incidenciaid integer, comentarios text, reporta integer) RETURNS boolean
    LANGUAGE sql
    AS $$
		UPDATE sgnom.tsgnomincidencia 
			SET txt_comentarios = comentarios,
			aud_codmodificadopor = reporta,
			aud_fecmodificacion = CURRENT_DATE
		WHERE cod_incidenciaid = incidenciaid RETURNING TRUE; 
$$;


ALTER FUNCTION sgnom.actualizar_comentarios_incidencia(incidenciaid integer, comentarios text, reporta integer) OWNER TO suite;

--
-- TOC entry 480 (class 1255 OID 97142)
-- Name: actualizar_importe_incidencia(integer, numeric, integer); Type: FUNCTION; Schema: sgnom; Owner: suite
--

CREATE FUNCTION sgnom.actualizar_importe_incidencia(incidenciaid integer, importe numeric, reporta integer) RETURNS boolean
    LANGUAGE sql
    AS $$
		UPDATE sgnom.tsgnomincidencia 
			SET imp_monto = importe,
			aud_codmodificadopor = reporta,
			aud_fecmodificacion = CURRENT_DATE
		WHERE "cod_incidenciaid" = incidenciaid RETURNING TRUE; 
$$;


ALTER FUNCTION sgnom.actualizar_importe_incidencia(incidenciaid integer, importe numeric, reporta integer) OWNER TO suite;

--
-- TOC entry 481 (class 1255 OID 97143)
-- Name: actualizar_incidencia(integer, text, numeric, integer); Type: FUNCTION; Schema: sgnom; Owner: suite
--

CREATE FUNCTION sgnom.actualizar_incidencia(incidenciaid integer, comentarios text, importe numeric, reporta integer) RETURNS boolean
    LANGUAGE sql
    AS $$
		UPDATE sgnom.tsgnomincidencia 
			SET txt_comentarios = comentarios,
			imp_monto = importe,
			aud_codmodificadopor = reporta,
			aud_fecmodificacion = CURRENT_DATE
		WHERE cod_incidenciaid = incidenciaid RETURNING TRUE; 
$$;


ALTER FUNCTION sgnom.actualizar_incidencia(incidenciaid integer, comentarios text, importe numeric, reporta integer) OWNER TO suite;

--
-- TOC entry 516 (class 1255 OID 97144)
-- Name: altasvalidadas(); Type: FUNCTION; Schema: sgnom; Owner: suite
--

CREATE FUNCTION sgnom.altasvalidadas() RETURNS TABLE(cod_empleadoid integer, nom_empleado text, cod_rfc character varying, cod_curp character varying, des_nbarea character varying, des_puesto character varying, validar boolean, fecha_validacion date)
    LANGUAGE plpgsql COST 10
    AS $$
BEGIN 
RETURN QUERY 
 SELECT nomempleados.cod_empleadoid,
    (((rhempleados.des_nombre::text || ' '::text) || rhempleados.des_apepaterno::text) || ' '::text) || rhempleados.des_apematerno::text AS nom_empleado,
    rhempleados.cod_rfc,
	rhempleados.cod_curp,
    rharea.des_nbarea,
    rhpuestos.des_puesto,
	nomempleados.des_validacion,
	nomempleados.aud_fecmodificacion
   FROM sgnom.tsgnomempleados nomempleados
     JOIN sgrh.tsgrhempleados rhempleados ON nomempleados.cod_empleado_fk = rhempleados.cod_empleado
     JOIN sgrh.tsgrhareas rharea ON rhempleados.cod_area = rharea.cod_area
     JOIN sgrh.tsgrhpuestos rhpuestos ON rhempleados.cod_puesto = rhpuestos.cod_puesto 
  WHERE  nomempleados.des_validacion is not null AND nomempleados.cod_validaciones='a' OR nomempleados.cod_validaciones='b'
  GROUP BY nomempleados.cod_empleadoid, ((((rhempleados.des_nombre::text || ' '::text) || rhempleados.des_apepaterno::text) || ' '::text) || rhempleados.des_apematerno::text), rhempleados.cod_rfc, 
  rhempleados.cod_curp,rharea.des_nbarea, rhpuestos.des_puesto
  ORDER BY nomempleados.cod_empleadoid;
END;
$$;


ALTER FUNCTION sgnom.altasvalidadas() OWNER TO suite;

--
-- TOC entry 488 (class 1255 OID 97145)
-- Name: bajasvalidadas(); Type: FUNCTION; Schema: sgnom; Owner: suite
--

CREATE FUNCTION sgnom.bajasvalidadas() RETURNS TABLE(cod_empleadoid integer, nom_empleado text, cod_rfc character varying, des_nbarea character varying, des_puesto character varying, validar boolean, fecha_validacion date)
    LANGUAGE plpgsql COST 10
    AS $$
BEGIN 
RETURN QUERY 
 SELECT nomempleados.cod_empleadoid,
    (((rhempleados.des_nombre::text || ' '::text) || rhempleados.des_apepaterno::text) || ' '::text) || rhempleados.des_apematerno::text AS nom_empleado,
    rhempleados.cod_rfc,
    rharea.des_nbarea,
    rhpuestos.des_puesto,
	nomempleados.des_validacion,
	nomempleados.aud_fecmodificacion
   FROM sgnom.tsgnomempleados nomempleados
     JOIN sgrh.tsgrhempleados rhempleados ON nomempleados.cod_empleado_fk = rhempleados.cod_empleado
     JOIN sgrh.tsgrhareas rharea ON rhempleados.cod_area = rharea.cod_area
     JOIN sgrh.tsgrhpuestos rhpuestos ON rhempleados.cod_puesto = rhpuestos.cod_puesto 
 WHERE nomempleados.des_validacion is not null AND  nomempleados.cod_validaciones='c' OR nomempleados.cod_validaciones='d' 
  GROUP BY nomempleados.cod_empleadoid, ((((rhempleados.des_nombre::text || ' '::text) || rhempleados.des_apepaterno::text) || ' '::text) || rhempleados.des_apematerno::text), rhempleados.cod_rfc, rharea.des_nbarea, rhpuestos.des_puesto
  ORDER BY nomempleados.cod_empleadoid;
END;
$$;


ALTER FUNCTION sgnom.bajasvalidadas() OWNER TO suite;

--
-- TOC entry 517 (class 1255 OID 97146)
-- Name: buscar_detalle_emp(integer); Type: FUNCTION; Schema: sgnom; Owner: suite
--

CREATE FUNCTION sgnom.buscar_detalle_emp(cabecera integer) RETURNS TABLE(codempleado integer, despuesto character varying, nomcompleto text, desnbarea character varying, codempqui integer, bol_pagorh boolean, bol_pagofinanzas boolean, bol_pagoempleado boolean, monto numeric)
    LANGUAGE plpgsql
    AS $$
BEGIN 
RETURN QUERY 

SELECT rh_emp.cod_empleado, puesto.des_puesto, 
    CONCAT(rh_emp.des_nombre, ' ', rh_emp.des_nombres, ' ', rh_emp.des_apepaterno, ' ', rh_emp.des_apematerno) AS nom_completo,
    area.des_nbarea,
    empQui.cod_empquincenaid cod_empqui,
    confPago.bol_pagorh rh,
    confPago.bol_pagofinanzas,
    confPago.bol_pagoempleado,
	(SELECT imp_totalemp FROM sgnom.tsgnomempquincena AS pago 
	 WHERE pago.cod_empquincenaid = empQui.cod_empquincenaid) AS monto
	
	FROM sgrh.tsgrhareas AS area, 
		 sgrh.tsgrhempleados AS rh_emp, 
		 sgrh.tsgrhpuestos AS puesto, 
		 sgnom.tsgnomempleados AS nom_emp,
	     sgnom.tsgnomempquincena AS empQui, 
		 sgnom.tsgnomconfpago AS confPago

	WHERE area.cod_area = puesto.cod_area
		--AND rh_emp.cod_area = area.cod_area
		AND rh_emp.cod_puesto = puesto.cod_puesto
		AND rh_emp.cod_empleadoactivo = 't'
		AND rh_emp.cod_empleado = nom_emp.cod_empleadoid
		AND rh_emp.cod_empleado = empQui.cod_empleadoid_fk
		AND empQui.cod_empquincenaid = confPago.cod_empquincenaid_fk
		AND empQui.cod_cabeceraid_fk = cabecera
		AND nom_emp.bol_estatus = 't'
	ORDER BY 3, 1, 2;

END;
$$;


ALTER FUNCTION sgnom.buscar_detalle_emp(cabecera integer) OWNER TO suite;

--
-- TOC entry 487 (class 1255 OID 97147)
-- Name: buscar_detalle_empleados(); Type: FUNCTION; Schema: sgnom; Owner: suite
--

CREATE FUNCTION sgnom.buscar_detalle_empleados() RETURNS TABLE(codempleado integer, despuesto character varying, nomcompleto text, desnbarea character varying)
    LANGUAGE plpgsql
    AS $$
BEGIN 
RETURN QUERY 

SELECT rh_emp.cod_empleado, puesto.des_puesto, 
	CONCAT(rh_emp.des_nombre, ' ', rh_emp.des_nombres, ' ', rh_emp.des_apepaterno, ' ', rh_emp.des_apematerno) AS nom_completo,
	area.des_nbarea
FROM sgrh.tsgrhareas AS area, sgrh.tsgrhempleados AS rh_emp, sgrh.tsgrhpuestos AS puesto, sgnom.tsgnomempleados AS nom_emp
WHERE area.cod_area = puesto.cod_area
AND rh_emp.cod_area = area.cod_area
AND rh_emp.cod_puesto = puesto.cod_puesto
AND rh_emp.cod_empleadoactivo = 't'
AND rh_emp.cod_empleado = nom_emp.cod_empleadoid
AND nom_emp.bol_estatus = 't'
ORDER BY 3, 1, 2;

END;
$$;


ALTER FUNCTION sgnom.buscar_detalle_empleados() OWNER TO suite;

--
-- TOC entry 489 (class 1255 OID 97148)
-- Name: buscar_detalle_empleados(integer); Type: FUNCTION; Schema: sgnom; Owner: suite
--

CREATE FUNCTION sgnom.buscar_detalle_empleados(idempleado integer) RETURNS TABLE(codempleado integer, despuesto character varying, nomcompleto text, desnbarea character varying)
    LANGUAGE plpgsql
    AS $$
BEGIN 
RETURN QUERY 

SELECT rh_emp.cod_empleado, puesto.des_puesto, 
	CONCAT(rh_emp.des_nombre, ' ', rh_emp.des_nombres, ' ', rh_emp.des_apepaterno, ' ', rh_emp.des_apematerno) AS nom_completo,
	area.des_nbarea
FROM sgrh.tsgrhareas AS area, sgrh.tsgrhempleados AS rh_emp, sgrh.tsgrhpuestos AS puesto, sgnom.tsgnomempleados AS nom_emp
WHERE area.cod_area = puesto.cod_area
AND rh_emp.cod_area = area.cod_area
AND rh_emp.cod_puesto = puesto.cod_puesto
AND rh_emp.cod_empleadoactivo = 't'
AND rh_emp.cod_empleado = idempleado
AND nom_emp.bol_estatus = 't'
ORDER BY 3, 1, 2;

END;
$$;


ALTER FUNCTION sgnom.buscar_detalle_empleados(idempleado integer) OWNER TO suite;

--
-- TOC entry 490 (class 1255 OID 97149)
-- Name: buscar_detalle_todos_empleados(); Type: FUNCTION; Schema: sgnom; Owner: suite
--

CREATE FUNCTION sgnom.buscar_detalle_todos_empleados() RETURNS TABLE(codempleado integer, despuesto character varying, nomcompleto text, desnbarea character varying)
    LANGUAGE plpgsql
    AS $$
BEGIN 
RETURN QUERY 

SELECT rh_emp.cod_empleado, puesto.des_puesto, 
	CONCAT(rh_emp.des_nombre, ' ', rh_emp.des_nombres, ' ', rh_emp.des_apepaterno, ' ', rh_emp.des_apematerno) AS nom_completo,
	area.des_nbarea
FROM sgrh.tsgrhareas AS area, sgrh.tsgrhempleados AS rh_emp, sgrh.tsgrhpuestos AS puesto, sgnom.tsgnomempleados AS nom_emp
WHERE area.cod_area = puesto.cod_area
AND rh_emp.cod_area = area.cod_area
AND rh_emp.cod_puesto = puesto.cod_puesto
AND rh_emp.cod_empleadoactivo = 't'
AND rh_emp.cod_empleado = nom_emp.cod_empleadoid
AND nom_emp.bol_estatus = 't'
ORDER BY 3, 1, 2;

END;
$$;


ALTER FUNCTION sgnom.buscar_detalle_todos_empleados() OWNER TO suite;

--
-- TOC entry 491 (class 1255 OID 97150)
-- Name: buscar_incidencias_por_empleado(integer); Type: FUNCTION; Schema: sgnom; Owner: suite
--

CREATE FUNCTION sgnom.buscar_incidencias_por_empleado(idempleado integer) RETURNS TABLE(idincidencia integer, fechaalta date, clave character varying, incidencia character varying, idtipo character, desctipo text, cantidad smallint, actividad character varying, comentarios text, reportaid integer, reportanb text, autorizaid integer, autorizanb text, perfil character varying, detallefechas text, montoincidencia numeric, montopagado numeric, aceptacion boolean, validacion boolean, quincenaid integer, desquincena character varying, creaid integer, creanb text)
    LANGUAGE plpgsql
    AS $$

BEGIN 
RETURN QUERY 

	SELECT incidencias.cod_incidenciaid idincidencia,
		incidencias.aud_feccreacion fechaalta,
		catincidencias.cod_claveincidencia clave,
		catincidencias.cod_nbincidencia incidencia,
		catincidencias.cod_tipoincidencia idtipo,
		CASE 
			WHEN catincidencias.cod_tipoincidencia='1' THEN
			 'HORAS'
			WHEN catincidencias.cod_tipoincidencia='2' THEN
			 'DIAS' 
		  WHEN catincidencias.cod_tipoincidencia='3' THEN
			 'ACTIVIDAD' 
			ELSE
			 'NO DATA'
		END desc_tipo,
		incidencias.cnu_cantidad cantidad,
		incidencias.des_actividad actividad,
		incidencias.txt_comentarios comentarios,
		incidencias.cod_empreporta_fk reportaid,
		(SELECT CONCAT(rhempleados.des_nombre ||' ', rhempleados.des_nombres ,' ' || rhempleados.des_apepaterno || ' ' || rhempleados.des_apematerno) 
		FROM sgrh.tsgrhempleados rhempleados
		WHERE rhempleados.cod_empleado = (SELECT nom.cod_empleado_fk FROM sgnom.tsgnomempleados nom WHERE nom.cod_empleadoid = idempleado)
		) reportanb,
		incidencias.cod_empautoriza_fk autorizaid,
		(SELECT CONCAT(rhempleados.des_nombre ||' ', rhempleados.des_nombres ,' ' || rhempleados.des_apepaterno || ' ' || rhempleados.des_apematerno) 
		FROM sgrh.tsgrhempleados rhempleados
		WHERE rhempleados.cod_empleado = incidencias.cod_empautoriza_fk
		) autorizanb,
		catincidencias.cod_perfilincidencia perfil,
		CAST(incidencias.xml_detcantidad AS TEXT) detallefechas,
		catincidencias.imp_monto montoincidencia,
		incidencias.imp_monto montopagado,
		incidencias.bol_validacion validacion, --validacion rh
		incidencias.bol_aceptacion aceptacion, --aceptacion lider de celula
		incidencias.cod_quincenaid_fk quincena,
		(SELECT qui.des_quincena FROM sgnom.tsgnomquincena qui WHERE qui.cod_quincenaid = incidencias.cod_quincenaid_fk) desquincena,
		incidencias.aud_codcreadopor creaid,
		(SELECT CONCAT(rhempleados.des_nombre ||' ', rhempleados.des_nombres ,' ' || rhempleados.des_apepaterno || ' ' || rhempleados.des_apematerno) 
		FROM sgrh.tsgrhempleados rhempleados
		WHERE rhempleados.cod_empleado = incidencias.aud_codcreadopor
		) creanb
FROM sgnom.tsgnomincidencia incidencias
JOIN sgnom.tsgnomcatincidencia catincidencias 
ON incidencias.cod_catincidenciaid_fk = catincidencias.cod_catincidenciaid
WHERE incidencias.cod_empreporta_fk = idempleado 
	AND incidencias.cod_quincenaid_fk = (SELECT nomquincena.cod_quincenaid 
											FROM sgnom.tsgnomquincena nomquincena
											WHERE nomquincena.fec_inicio <= CURRENT_DATE AND nomquincena.fec_fin >= CURRENT_DATE)
	AND incidencias.bol_estatus = true 
ORDER BY 1 DESC, 2;

END;
$$;


ALTER FUNCTION sgnom.buscar_incidencias_por_empleado(idempleado integer) OWNER TO suite;

--
-- TOC entry 492 (class 1255 OID 97151)
-- Name: detalle_desglose(integer, integer); Type: FUNCTION; Schema: sgnom; Owner: suite
--

CREATE FUNCTION sgnom.detalle_desglose(cod_empleado integer, cod_cabecera integer) RETURNS TABLE(imp_concepto numeric, imp_exento numeric, imp_gravado numeric, nombre character varying, clave character varying, cod_conceptoid_fk integer)
    LANGUAGE plpgsql
    AS $$
BEGIN 
RETURN QUERY 

SELECT CQ.imp_concepto, CQ.imp_exento, CQ.imp_gravado, 
							(SELECT cod_nbconcepto
                            FROM sgnom.tsgnomconcepto CO
                            WHERE cod_conceptoid = CQ.cod_conceptoid_fk) nombre, 
							(SELECT cod_claveconcepto
                            FROM sgnom.tsgnomconcepto CO
                            WHERE cod_conceptoid = CQ.cod_conceptoid_fk) clave, 
							CQ.cod_conceptoid_fk
FROM sgnom.tsgnomcncptoquinc CQ
WHERE CQ.cod_empquincenaid_fk = (
                            SELECT cod_empquincenaid 
                            FROM sgnom.tsgnomempquincena EQ
                            WHERE EQ.cod_cabeceraid_fk  = cod_cabecera --cabecera a seleccionar
                            AND EQ.cod_empleadoid_fk = (SELECT cod_empleadoid
                                                       FROM sgnom.tsgnomempleados
                                                       WHERE cod_empleado_fk = cod_empleado )); --empleado elegido
 --tipo concepto 1=deduccion, 2=percepcion

END;
$$;


ALTER FUNCTION sgnom.detalle_desglose(cod_empleado integer, cod_cabecera integer) OWNER TO suite;

--
-- TOC entry 493 (class 1255 OID 97152)
-- Name: detalle_desglose_deduccion(integer, integer); Type: FUNCTION; Schema: sgnom; Owner: suite
--

CREATE FUNCTION sgnom.detalle_desglose_deduccion(cod_empleado integer, cod_cabecera integer) RETURNS TABLE(imp_concepto numeric, imp_exento numeric, imp_gravado numeric, nombre character varying, clave character varying, cod_conceptoid_fk integer)
    LANGUAGE plpgsql
    AS $$
BEGIN 
RETURN QUERY 

SELECT CQ.imp_concepto, CQ.imp_exento, CQ.imp_gravado, 
							(SELECT cod_nbconcepto
                            FROM sgnom.tsgnomconcepto CO
                            WHERE cod_conceptoid = CQ.cod_conceptoid_fk) nombre, 
							(SELECT cod_claveconcepto
                            FROM sgnom.tsgnomconcepto CO
                            WHERE cod_conceptoid = CQ.cod_conceptoid_fk) clave, 
							CQ.cod_conceptoid_fk
FROM sgnom.tsgnomcncptoquinc CQ
WHERE CQ.cod_empquincenaid_fk = (
                            SELECT cod_empquincenaid 
                            FROM sgnom.tsgnomempquincena EQ
                            WHERE EQ.cod_cabeceraid_fk  = cod_cabecera --cabecera a seleccionar
                            AND EQ.cod_empleadoid_fk = (SELECT cod_empleadoid
                                                       FROM sgnom.tsgnomempleados
                                                       WHERE cod_empleado_fk = cod_empleado )) --empleado elegido
 AND CQ.cod_conceptoid_fk IN (SELECT cod_conceptoid
                            FROM sgnom.tsgnomconcepto CO
                            WHERE CO.cod_tipoconceptoid_fk = 1);--tipo concepto 1=deduccion, 2=percepcion

END;
$$;


ALTER FUNCTION sgnom.detalle_desglose_deduccion(cod_empleado integer, cod_cabecera integer) OWNER TO suite;

--
-- TOC entry 518 (class 1255 OID 97153)
-- Name: detalle_desglose_percepcion(integer, integer); Type: FUNCTION; Schema: sgnom; Owner: suite
--

CREATE FUNCTION sgnom.detalle_desglose_percepcion(cod_empleado integer, cod_cabecera integer) RETURNS TABLE(imp_concepto numeric, imp_exento numeric, imp_gravado numeric, nombre character varying, clave character varying, cod_conceptoid_fk integer)
    LANGUAGE plpgsql
    AS $$
BEGIN 
RETURN QUERY 

SELECT CQ.imp_concepto, CQ.imp_exento, CQ.imp_gravado, 
							(SELECT cod_nbconcepto
                            FROM sgnom.tsgnomconcepto CO
                            WHERE cod_conceptoid = CQ.cod_conceptoid_fk) nombre, 
							(SELECT cod_claveconcepto
                            FROM sgnom.tsgnomconcepto CO
                            WHERE cod_conceptoid = CQ.cod_conceptoid_fk) clave, 
							CQ.cod_conceptoid_fk
FROM sgnom.tsgnomcncptoquinc CQ
WHERE CQ.cod_empquincenaid_fk = (
                            SELECT cod_empquincenaid 
                            FROM sgnom.tsgnomempquincena EQ
                            WHERE EQ.cod_cabeceraid_fk  = cod_cabecera --cabecera a seleccionar
                            AND EQ.cod_empleadoid_fk = (SELECT cod_empleadoid
                                                       FROM sgnom.tsgnomempleados
                                                       WHERE cod_empleado_fk = cod_empleado )) --empleado elegido
 AND CQ.cod_conceptoid_fk IN (SELECT cod_conceptoid
                            FROM sgnom.tsgnomconcepto CO
                            WHERE CO.cod_tipoconceptoid_fk = 2);--tipo concepto 1=deduccion, 2=percepcion

END;
$$;


ALTER FUNCTION sgnom.detalle_desglose_percepcion(cod_empleado integer, cod_cabecera integer) OWNER TO suite;

--
-- TOC entry 494 (class 1255 OID 97154)
-- Name: detalle_desglose_persepcion(integer, integer, integer); Type: FUNCTION; Schema: sgnom; Owner: suite
--

CREATE FUNCTION sgnom.detalle_desglose_persepcion(cod_empleado integer, cod_cabecera integer, tipo_concepto integer) RETURNS TABLE(imp_concepto numeric, imp_exento numeric, imp_gravado numeric, nombre character varying, clave character varying, cod_conceptoid_fk integer)
    LANGUAGE plpgsql
    AS $$
BEGIN 
RETURN QUERY 

SELECT CQ.imp_concepto, CQ.imp_exento, CQ.imp_gravado, 
							(SELECT cod_nbconcepto
                            FROM sgnom.tsgnomconcepto CO
                            WHERE cod_conceptoid = CQ.cod_conceptoid_fk) nombre, 
							(SELECT cod_claveconcepto
                            FROM sgnom.tsgnomconcepto CO
                            WHERE cod_conceptoid = CQ.cod_conceptoid_fk) clave, 
							CQ.cod_conceptoid_fk
FROM sgnom.tsgnomcncptoquinc CQ
WHERE CQ.cod_empquincenaid_fk = (
                            SELECT cod_empquincenaid 
                            FROM sgnom.tsgnomempquincena EQ
                            WHERE EQ.cod_cabeceraid_fk  = cod_cabecera --cabecera a seleccionar
                            AND EQ.cod_empleadoid_fk = (SELECT cod_empleadoid
                                                       FROM sgnom.tsgnomempleados
                                                       WHERE cod_empleado_fk = cod_empleado )) --empleado elegido
 AND CQ.cod_conceptoid_fk IN (SELECT cod_conceptoid
                            FROM sgnom.tsgnomconcepto CO
                            WHERE CO.cod_tipoconceptoid_fk = tipo_concepto);--tipo concepto 1=deduccion, 2=percepcion

END;
$$;


ALTER FUNCTION sgnom.detalle_desglose_persepcion(cod_empleado integer, cod_cabecera integer, tipo_concepto integer) OWNER TO suite;

--
-- TOC entry 495 (class 1255 OID 97155)
-- Name: detallespersonal(); Type: FUNCTION; Schema: sgnom; Owner: suite
--

CREATE FUNCTION sgnom.detallespersonal() RETURNS TABLE(codempleado integer, nomcompleto text, desrfc character varying, descurp character varying, desnbarea character varying, despuesto character varying)
    LANGUAGE plpgsql COST 10
    AS $$
BEGIN
RETURN QUERY
SELECT nomempleados.cod_empleadoid,
   (((rhempleados.des_nombre::text || ' '::text) || rhempleados.des_apepaterno::text) || ' '::text) || rhempleados.des_apematerno::text AS nom_empleado,
   rhempleados.cod_rfc,
    rhempleados.cod_curp,
   rharea.des_nbarea,
   rhpuestos.des_puesto
  FROM sgnom.tsgnomempleados nomempleados
    JOIN sgrh.tsgrhempleados rhempleados ON nomempleados.cod_empleado_fk = rhempleados.cod_empleado
    JOIN sgrh.tsgrhareas rharea ON rhempleados.cod_area = rharea.cod_area
    JOIN sgrh.tsgrhpuestos rhpuestos ON rhempleados.cod_puesto = rhpuestos.cod_puesto
 WHERE nomempleados.des_validacion=true
 and nomempleados.cod_validaciones='a' OR nomempleados.cod_validaciones='d'
 GROUP BY nomempleados.cod_empleadoid, ((((rhempleados.des_nombre::text || ' '::text) || rhempleados.des_apepaterno::text) || ' '::text) || rhempleados.des_apematerno::text), rhempleados.cod_rfc, rhempleados.cod_curp,rharea.des_nbarea, rhpuestos.des_puesto
 ORDER BY nomempleados.cod_empleadoid;
END;
$$;


ALTER FUNCTION sgnom.detallespersonal() OWNER TO suite;

--
-- TOC entry 519 (class 1255 OID 97156)
-- Name: eliminar_incidencia_por_empleado(integer, integer); Type: FUNCTION; Schema: sgnom; Owner: suite
--

CREATE FUNCTION sgnom.eliminar_incidencia_por_empleado(incidenciaid integer, reporta integer) RETURNS boolean
    LANGUAGE sql
    AS $$
	--No es eliminacion fisica, actualiza el estatus a 'false'
	UPDATE sgnom.tsgnomincidencia 
		SET bol_estatus = 'f',
		aud_codmodificadopor = reporta,
		aud_fecmodificacion = CURRENT_DATE
	WHERE "cod_incidenciaid" = incidenciaid	RETURNING TRUE;

$$;


ALTER FUNCTION sgnom.eliminar_incidencia_por_empleado(incidenciaid integer, reporta integer) OWNER TO suite;

--
-- TOC entry 496 (class 1255 OID 97157)
-- Name: empleado_confpago(integer, integer); Type: FUNCTION; Schema: sgnom; Owner: suite
--

CREATE FUNCTION sgnom.empleado_confpago(cabecera integer, empleado integer) RETURNS TABLE(codempleado integer, despuesto character varying, nomcompleto text, desnbarea character varying, codempqui integer, bol_pagorh boolean, bol_pagofinanzas boolean, bol_pagoempleado boolean, monto numeric)
    LANGUAGE plpgsql
    AS $$
BEGIN 
RETURN QUERY 

SELECT rh_emp.cod_empleado, puesto.des_puesto, 
    CONCAT(rh_emp.des_nombre, ' ', rh_emp.des_nombres, ' ', rh_emp.des_apepaterno, ' ', rh_emp.des_apematerno) AS nom_completo,
    area.des_nbarea,
    empQui.cod_empquincenaid cod_empqui,
    confPago.bol_pagorh rh,
    confPago.bol_pagofinanzas,
    confPago.bol_pagoempleado,
	(SELECT imp_totalemp FROM sgnom.tsgnomempquincena AS pago WHERE pago.cod_empquincenaid = empQui.cod_empquincenaid) AS monto
	
FROM sgrh.tsgrhareas AS area, 
	 sgrh.tsgrhempleados AS rh_emp, 
	 sgrh.tsgrhpuestos AS puesto, 
	 sgnom.tsgnomempleados AS nom_emp,
     sgnom.tsgnomempquincena AS empQui, 
	 sgnom.tsgnomconfpago AS confPago

WHERE area.cod_area = puesto.cod_area
--AND rh_emp.cod_area = area.cod_area
AND rh_emp.cod_puesto = puesto.cod_puesto
AND rh_emp.cod_empleadoactivo = 't'
AND rh_emp.cod_empleado = nom_emp.cod_empleadoid
AND rh_emp.cod_empleado = empQui.cod_empleadoid_fk
AND empQui.cod_empquincenaid = confPago.cod_empquincenaid_fk
AND empQui.cod_cabeceraid_fk = cabecera
AND empQui.cod_empleadoid_fk = empleado
AND nom_emp.bol_estatus = 't'
ORDER BY 3, 1, 2;

END;
$$;


ALTER FUNCTION sgnom.empleado_confpago(cabecera integer, empleado integer) OWNER TO suite;

--
-- TOC entry 520 (class 1255 OID 97158)
-- Name: empleados_por_cabecera(integer); Type: FUNCTION; Schema: sgnom; Owner: suite
--

CREATE FUNCTION sgnom.empleados_por_cabecera(idcabecera integer) RETURNS TABLE(nom_empleado text, rol character varying, area character varying, cod_empleado integer, cod_empquincenaid integer)
    LANGUAGE plpgsql
    AS $$
BEGIN 
RETURN QUERY 
SELECT 
(((rhempleados.des_nombre::text || ' '::text) || rhempleados.des_apepaterno::text) || ' '::text) || rhempleados.des_apematerno::text AS nom_empleado,
rhpuestos.des_puesto,
rharea.des_nbarea,
emp.cod_empleadoid,
empquin.cod_empquincenaid
FROM sgrh.tsgrhempleados rhempleados
JOIN sgnom.tsgnomempleados emp ON emp.cod_empleadoid = rhempleados.cod_empleado
JOIN sgnom.tsgnomempquincena empquin ON emp.cod_empleadoid =  empquin.cod_empleadoid_fk
JOIN sgrh.tsgrhpuestos rhpuestos ON rhempleados.cod_puesto = rhpuestos.cod_puesto
JOIN sgrh.tsgrhareas rharea ON rhempleados.cod_area = rharea.cod_area
WHERE empquin.cod_cabeceraid_fk = idcabecera;	
END;
$$;


ALTER FUNCTION sgnom.empleados_por_cabecera(idcabecera integer) OWNER TO suite;

--
-- TOC entry 508 (class 1255 OID 99000)
-- Name: fn_bono_asistencia_imss(integer); Type: FUNCTION; Schema: sgnom; Owner: postgres
--

CREATE FUNCTION sgnom.fn_bono_asistencia_imss(idempleado integer) RETURNS numeric
    LANGUAGE plpgsql
    AS $$
    DECLARE
		valor DECIMAL;
		--rec RECORD;
    BEGIN
		SELECT imss.premio_asistencia
        FROM sgnom.tsgnomnominaimss imss
        WHERE (imss.curp, imss.nss, imss.rfc) =        
            (SELECT cod_curp, cod_nss, cod_rfc 
            FROM sgrh.tsgrhempleados 
            WHERE cod_empleado  = idEmpleado) INTO valor;
		RETURN valor;
    END;
$$;


ALTER FUNCTION sgnom.fn_bono_asistencia_imss(idempleado integer) OWNER TO postgres;

--
-- TOC entry 509 (class 1255 OID 99001)
-- Name: fn_bono_puntualidad_imss(integer); Type: FUNCTION; Schema: sgnom; Owner: postgres
--

CREATE FUNCTION sgnom.fn_bono_puntualidad_imss(idempleado integer) RETURNS numeric
    LANGUAGE plpgsql
    AS $$
    DECLARE
		valor DECIMAL;
		--rec RECORD;
    BEGIN
		SELECT imss.premio_puntualidad
        FROM sgnom.tsgnomnominaimss imss
        WHERE (imss.curp, imss.nss, imss.rfc) =        
            (SELECT cod_curp, cod_nss, cod_rfc 
            FROM sgrh.tsgrhempleados 
            WHERE cod_empleado  = idEmpleado) INTO valor;
		RETURN valor;
    END;
$$;


ALTER FUNCTION sgnom.fn_bono_puntualidad_imss(idempleado integer) OWNER TO postgres;

--
-- TOC entry 497 (class 1255 OID 97159)
-- Name: fn_calcula_importes_nomina(integer, character varying, numeric); Type: FUNCTION; Schema: sgnom; Owner: suite
--

CREATE FUNCTION sgnom.fn_calcula_importes_nomina(idnomina integer, vempleados character varying, todos numeric) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
    DECLARE
        vTotalPercepcion          NUMERIC(10,2):=0;
        vTotalDeduccion           NUMERIC(10,2):=0;
        vImporteTotal             NUMERIC(10,2):=0;
        vNumRegistros             INTEGER:=0;
        vNumRegistros2            INTEGER:=0;
        vBandera                  INTEGER;
        vIdEmpleadoNomina         INTEGER;
        vTotalPercepcionC         NUMERIC(10,2):=0;
        vTotalDeduccionC          NUMERIC(10,2):=0;
        vImporteTotalC            NUMERIC(10,2):=0;
        vNumeroEmpleados          INTEGER;
        vNomEmpleado              NUMERIC(10,2);
        vTotalEmpleadosNomina     INTEGER;
        vTotalEmpleadosCalculados INTEGER;
        ERR_CODE       INTEGER;
        ERR_MSG        VARCHAR(250);
        a_count INTEGER;
        b_count INTEGER;
        --CURSORES

        C5 CURSOR FOR
            SELECT EQ.cod_empquincenaid
            FROM sgnom.tsgnomempquincena EQ
            WHERE EQ.cod_cabeceraid_fk = idNomina
            ORDER BY EQ.cod_empquincenaid;

        C6 CURSOR FOR
            SELECT EQ.cod_empquincenaid
            FROM sgnom.tsgnomempquincena EQ
            WHERE EQ.cod_cabeceraid_fk = idNomina
            AND EQ.bol_estatusemp = 'f'
            ORDER BY EQ.cod_empquincenaid;

    BEGIN
        OPEN C5;
        RAISE INFO 'abre cursor C1';
        LOOP 
            FETCH C5 INTO vIdEmpleadoNomina;
            EXIT WHEN not found;
            RAISE INFO 'empleado %, idNomina %', vIdEmpleadoNomina,idNomina;
            --Se realiza la suma de los conceptos que son percepciones
            SELECT SUM(CQ.imp_concepto) INTO vTotalPercepcion
            FROM sgnom.tsgnomcncptoquinc CQ, sgnom.tsgnomconcepto CO, sgnom.tsgnomempquincena EQ
            WHERE CO.cod_tipoconceptoid_fk = 2
            AND EQ.cod_cabeceraid_fk = idNomina
            AND EQ.cod_empquincenaid = vIdEmpleadoNomina
            AND EQ.cod_empquincenaid = CQ.cod_empquincenaid_fk
            AND CO.cod_conceptoid = CQ.cod_conceptoid_fk;
            RAISE INFO 'vTotalPercepcion %', vTotalPercepcion;
            IF(vTotalPercepcion        IS NOT NULL) THEN
                vTotalPercepcionC        :=vTotalPercepcionC+vTotalPercepcion;
            ELSIF(vTotalPercepcion     IS NULL) THEN
                vTotalPercepcion         :=0;
                vTotalPercepcionC        :=vTotalPercepcionC+vTotalPercepcion;
            END IF;
            RAISE INFO 'vTotalPercepcionC %', vTotalPercepcionC;
            --Se realiza la suma de los conceptos que son deducciones
            SELECT SUM(CQ.imp_concepto) INTO vTotalDeduccion
            FROM sgnom.tsgnomcncptoquinc CQ, sgnom.tsgnomconcepto CO, sgnom.tsgnomempquincena EQ
            WHERE CO.cod_tipoconceptoid_fk = 2
            AND EQ.cod_cabeceraid_fk = idNomina
            AND EQ.cod_empleadoid_fk = vIdEmpleadoNomina
            AND EQ.cod_empquincenaid = CQ.cod_empquincenaid_fk
            AND CO.cod_conceptoid = CQ.cod_conceptoid_fk;
            RAISE INFO 'vTotalDeduccion %', vTotalDeduccion;
            IF(vTotalDeduccion         IS NOT NULL) THEN
                vTotalDeduccionC         :=vTotalDeduccionC+vTotalDeduccion;
            ELSIF (vTotalDeduccion     IS NULL) THEN
                vTotalDeduccion          :=0;
                vTotalDeduccionC         :=vTotalDeduccionC+vTotalDeduccion;
            END IF;
            RAISE INFO 'vTotalDeduccionC %', vTotalDeduccionC;
            IF(vTotalPercepcion IS NOT NULL AND vTotalDeduccion IS NOT NULL) THEN
                SELECT (vTotalPercepcion - vTotalDeduccion) INTO vImporteTotal ;
            ELSIF(vTotalPercepcion IS NULL AND vTotalDeduccion IS NOT NULL) THEN
                vImporteTotal        :=vTotalDeduccion;
            ELSIF(vTotalPercepcion IS NOT NULL AND vTotalDeduccion IS NULL) THEN
                vImporteTotal        :=vTotalPercepcion;
            END IF;
            
            IF(vImporteTotal IS NOT NULL) THEN
                vImporteTotalC :=vImporteTotalC+vImporteTotal;
            ELSE
                vImporteTotalC:=0;
            END IF;
            --Se actualizan los importes de cada empleado asi como el estatus
            UPDATE sgnom.tsgnomempquincena
            SET imp_totpercepcion     =vTotalPercepcion,
                imp_totdeduccion        =vTotalDeduccion,
                imp_totalemp          =vImporteTotal
                --bol_estatusemp ='B'
            WHERE cod_empquincenaid = vIdEmpleadoNomina
            AND cod_cabeceraid_fk = idNomina;
            GET DIAGNOSTICS a_count = ROW_COUNT;
			RAISE INFO 'a_count %', a_count;
            IF (a_count>0) THEN 
                vNumRegistros := vNumRegistros + 1 ;
				RAISE INFO 'vNumRegistros %', vNumRegistros;
            END IF;
            --COMMIT;
        END LOOP;
        CLOSE C5;
        UPDATE sgnom.tsgnomcabecera
        SET imp_totpercepcion = vTotalPercepcionC,
        imp_totdeduccion = vTotalDeduccionC,
        imp_totalemp = vImporteTotalC,
        cod_estatusnomid_fk = 2
        WHERE cod_cabeceraid = idNomina
        AND (cod_estatusnomid_fk=1 OR cod_estatusnomid_fk=2);
        GET DIAGNOSTICS b_count = ROW_COUNT;
        IF (b_count >0) THEN
            vNumRegistros2      := vNumRegistros2 + 1 ;
        END IF; 

        IF (vNumRegistros > 0 AND vNumRegistros2 > 0) THEN
            vBandera := 1;
        ELSIF (vNumRegistros = 0 OR vNumRegistros2 = 0) THEN
            vBandera := 0;
        ELSE
            vBandera:=2;
        END IF;
    RETURN vBandera;
		EXCEPTION
		WHEN NO_DATA_FOUND THEN
			RAISE EXCEPTION '% exp no data found', vIdEmpleado;
			RETURN NULL;
    END;
$$;


ALTER FUNCTION sgnom.fn_calcula_importes_nomina(idnomina integer, vempleados character varying, todos numeric) OWNER TO suite;

--
-- TOC entry 521 (class 1255 OID 97160)
-- Name: fn_calcula_nomina(integer); Type: FUNCTION; Schema: sgnom; Owner: suite
--

CREATE FUNCTION sgnom.fn_calcula_nomina(vidnomina integer) RETURNS integer
    LANGUAGE plpgsql
    AS $_$
    DECLARE
    --rtSueldoBase INTEGER;
    --vFormula CHARACTER VARYING; --valor de la formula
    --vArgumento CHARACTER VARYING; --valor del argumento
    --vNombreArgumento CHARACTER VARYING; --valor del argumento
    --vNumeroArgumentos INTEGER; --numero de argumentos
    --VARIABLES
    total INTEGER;
    vIdNomEmpPla01 INTEGER;
    vIdConEmpPla01 INTEGER;
    vIdConcepto01fk INTEGER;
    vNumRegistros INTEGER:=0;
    vBandera INTEGER;
    vFormula VARCHAR(150);
    vArgumento VARCHAR(100);
    vNumeroArgumentos INTEGER;
    vFuncionOracle VARCHAR(100);
    vDiasLaborados INTEGER;
    vCadenaEjecutar VARCHAR(500);
    vFormulaEjecutar VARCHAR(500);
    vTabulador INTEGER:=0;
    vIdEmpleado INTEGER;
    vIdPlaza INTEGER;
    vNumeroQuincena INTEGER;
    vSueldoBase NUMERIC(10,2):=0.0;
    vConstante NUMERIC(10,2);
    vFormula2 VARCHAR(150);
    vImporteActualizado NUMERIC(10,2);
    vCadenaImporte VARCHAR(250);
    vXML VARCHAR(4000);
    vNombreConcepto VARCHAR(150);
    vValorArgumento NUMERIC(10,2);
    vNombreArgumento VARCHAR(100);
    vCompensasionG INTEGER:=0;
    vNumeroEmpleados INTEGER;
    vNomEmpleado INTEGER;
    vBanderaImporte INTEGER:=0;
    vIdManTer INTEGER;
    vPorcentajeManTer INTEGER;
    vClaveConcepto VARCHAR(20);
    vClaveConcepto2 VARCHAR(20);
    vApoyoVehicular INTEGER;
    vAportacionSSIGF INTEGER;
    vPrimaVacacional INTEGER;
    vFormulaCICAS VARCHAR(150);
    vFormulaCICAC VARCHAR(150);
    vNumeroArgumentosCAS INTEGER;
    vSueldoDiario INTEGER:=0;
    vExisteAguinaldo INTEGER;
    vIdAguinaldo INTEGER;
    vIdQuincena INTEGER;
    vFaltasSueldoEst INTEGER:=0;
    vFaltasSueldoEv INTEGER:=0;
    vFaltasSueldoEstAA INTEGER:=0;
    vFaltasCompEst INTEGER:=0;
    vFaltasCompEv INTEGER:=0;
    vFaltasCompEstAA INTEGER:=0;
    vISRSueldo INTEGER;
    vISRCompGaran INTEGER;
    vISRSSI INTEGER;
    vISRApoyoVehic INTEGER;
    vISRPrimaVac INTEGER;
    vValorQuinquenio INTEGER:=0;
    vSueldoCompactado INTEGER:=0;
    vSueldoBimestral INTEGER:=0;
    vQuinquenioBimestral INTEGER:=0;
    vSumaSRCEAYV INTEGER;
    vSueldoBimestralEV INTEGER;
    vQuinquenioBimestralEV INTEGER;
    vSumaSRCEAYVEV INTEGER;
    vSueldoCompactadoEV INTEGER;
    vPensionAlimenticia INTEGER;
    vDespensa INTEGER:=0;
    xmlDesglose VARCHAR(3000);
    --cursorPrimaVacacional SYS_REFCURSOR;
    --xmlApoyoVehicular VARCHAR(3000);
    --cursorApoyoVehicular SYS_REFCURSOR;
    -- xmlISRSSI VARCHAR(3000);
    --cursorISRSSI SYS_REFCURSOR;
    vSumaSARP INTEGER;
    vSumaFOVP INTEGER;
    vSumaSRCEAYVPAT INTEGER;
    vPorcentajePension INTEGER;
    vISREstDesemp INTEGER;
    vISRHonorarios INTEGER;
    vValorEstimulo INTEGER;
    vNumeroFaltas INTEGER;
    vDiasLicencia INTEGER;
    ERR_CODE INTEGER;
    ERR_MSG VARCHAR(250);
    vAhorroSolidario INTEGER;
    DecimaImpar INTEGER;
    vNumeroQuincenaBIM INTEGER;
    vTipoNominaBIM INTEGER;
    vEjercicioBIM INTEGER;
    vBanderaSegBaja INTEGER;
    vValISREstDesempA INTEGER;
    vExisteAnt INTEGER;
    vSueldoAnt INTEGER;
    vCompAnt INTEGER;
    vDiasLabAnterior INTEGER;
    vFechaNomina INTEGER;
    vImpLicMedSueldo INTEGER;
    vBanderaEmpNeg INTEGER;
	a_count INTEGER;
    cncptoquincid INTEGER;
    ---- CURSORES
    C1 CURSOR FOR
        SELECT  EQ.cod_empquincenaid,EQ.cod_empleadoid_fk,Q.cnu_numquincena,Q.cod_quincenaid, --CQ.cod_conceptoid_fk
				CQ.cod_cncptoquincid, CQ.cod_empquincenaid_fk, CQ.cod_conceptoid_fk, CO.cod_nbconcepto, CO.cod_claveconcepto
        FROM sgnom.tsgnomempquincena EQ
        INNER JOIN sgnom.tsgnomcabecera C 
            ON C.cod_cabeceraid = EQ.cod_cabeceraid_fk
        INNER JOIN sgnom.tsgnomquincena Q
            ON Q.cod_quincenaid = C.cod_quincenaid_fk
		INNER JOIN sgnom.tsgnomcncptoquinc CQ 
			ON CQ.cod_empquincenaid_fk = EQ.cod_empquincenaid
		INNER JOIN sgnom.tsgnomconcepto CO 
		    ON CO.cod_conceptoid = CQ.cod_conceptoid_fk
        WHERE C.cod_cabeceraid = vIdNomina
		AND CO.cod_calculoid_fk = 2
        AND bol_estatusemp != 'f'
        ORDER BY CO.cod_tipoconceptoid_fk, CO.cnu_prioricalculo, EQ.cod_empquincenaid;
    
    C3 CURSOR FOR
        SELECT CQ.cod_cncptoquincid, CQ.cod_empquincenaid_fk, CQ.cod_conceptoid_fk, CO.cod_nbconcepto, CO.cod_claveconcepto
        FROM sgnom.tsgnomcncptoquinc CQ
        INNER JOIN sgnom.tsgnomconcepto CO ON CO.cod_conceptoid = CQ.cod_conceptoid_fk
        WHERE CO.cod_calculoid_fk = 2
        ORDER BY CO.cod_tipoconceptoid_fk, CO.cnu_prioricalculo;

    C4 CURSOR FOR
        SELECT des_formula
        FROM sgnom.tsgnomformula
        WHERE cod_formulaid =
                (SELECT CO.cod_formulaid_fk 
                FROM sgnom.tsgnomconcepto CO
                WHERE CO.cod_conceptoid = vIdConcepto01fk );

    C10 CURSOR FOR
        SELECT des_formula
        FROM sgnom.tsgnomformula
        WHERE cod_formulaid = 9;

    BEGIN
        OPEN C1;
        RAISE INFO 'abre cursor C1';
        LOOP 
            FETCH C1 INTO vIdNomEmpPla01, vIdEmpleado,vNumeroQuincena,vIdQuincena,cncptoquincid, vIdConEmpPla01,vIdConcepto01fk,vNombreConcepto,vClaveConcepto;
            EXIT WHEN not found;
            OPEN C4;
            RAISE INFO 'abre cursor C10 -> C4';
            LOOP
                FETCH C4 INTO vFormula;
                EXIT WHEN not found;
                vFormula2 :=vFormula;
                vNumeroArgumentos:=(SELECT COUNT(*)
                                    FROM regexp_matches(vFormula, ':[^#0-9/*+$%-:]+', 'g'));
                RAISE INFO 'vformula %', vFormula;
                RAISE INFO 'vNumeroArgumentos %', vNumeroArgumentos;
                FOR i IN 1 .. vNumeroArgumentos
                LOOP
                    SELECT regexp_replace(
                        (SELECT contenido
                        FROM
                        (SELECT contenido, row_number() OVER () AS indice --COUNT(*)
                        FROM 
                        regexp_matches(vFormula, ':[^#0-9/*+$%-:]+', 'g') AS contenido
                        ) AS todo
                        WHERE indice = i)::varchar, '[{:}"]', '', 'g') 
                        --WHERE indice = )::"varchar", '[[:punct:]]', '', 'g')
                        INTO vArgumento;
                    --Se obtiene el nombre del argumento
                    SELECT cod_nbargumento INTO vNombreArgumento
                    FROM sgnom.tsgnomargumento
                    WHERE cod_nbargumento = vArgumento;
                    RAISE INFO 'nomARG: % %', vArgumento, vNombreArgumento;
                    --Se obtiene la funci�n del argumento
                    SELECT des_funcionbd INTO vFuncionOracle
                    FROM sgnom.tsgnomargumento
                   	WHERE cod_nbargumento = vArgumento;
                    RAISE INFO 'fnARG: %, %', vArgumento,vFuncionOracle;
                    IF (vFuncionOracle IS NOT NULL) THEN
                        IF (vFuncionOracle = 'fn_dias_laborados') THEN 
                            vFormulaEjecutar :='sgnom.'||vFuncionOracle||'('||vIdEmpleado||','||vIdQuincena||')';
                            vCadenaEjecutar :='select ' || vFormulaEjecutar;
                            RAISE INFO 'cadena %', vCadenaEjecutar;
                            EXECUTE vCadenaEjecutar INTO vDiasLaborados;
                            vValorArgumento :=vDiasLaborados;
                            RAISE INFO 'RES laborados %', vValorArgumento;
						ELSIF (vFuncionOracle = 'fn_sueldo_base') THEN 
                            vFormulaEjecutar :='sgnom.'||vFuncionOracle||'('||vIdEmpleado||')';
                            vCadenaEjecutar :='select ' || vFormulaEjecutar;
                            RAISE INFO 'cadena %', vCadenaEjecutar;
                            EXECUTE vCadenaEjecutar INTO vSueldoBase;
                            vValorArgumento :=vSueldoBase;
                            RAISE INFO 'RES SB %', vValorArgumento;
                        END IF;
                    ELSE
                    	SELECT imp_valorconst INTO vConstante
						FROM sgnom.tsgnomargumento
                   		WHERE cod_nbargumento = vArgumento;
                    	vValorArgumento :=vConstante;
						RAISE INFO 'RES constante %', vConstante;
					END IF;
                    SELECT regexp_replace(vFormula2, vArgumento, vValorArgumento::"varchar", 'g') INTO vFormula2;
                    RAISE INFO 'RES vFormula2 %, vArgumento %, vValorArgumento % ', 
					vFormula2,vArgumento,vValorArgumento;
                    
                END LOOP;
				SELECT regexp_replace(vFormula2, '[:]', '', 'g') INTO vFormula2;
				vCadenaImporte:='SELECT '|| vFormula2 || '';
				RAISE INFO 'RES vCadenaImporte %',  vCadenaImporte;
                -- DBMS_OUTPUT.PUT_LINE(vFormula2);
                EXECUTE vCadenaImporte INTO vImporteActualizado;
                RAISE INFO 'RES vImporteActualizado %', vImporteActualizado;
                --COMMIT;
            END LOOP;
            CLOSE C4;
            OPEN C4;
            LOOP 
                FETCH C4 INTO vFormula;
                EXIT WHEN not found;
                vFormula2 :=vFormula;
                vXML :='<?xml version="1.0" encoding="ISO8859_1"?>                
                <mx.org.ift.serp.conceptoBindingXml.DetallesConcepto>                  
                <claveConcepto>' || vClaveConcepto || '</claveConcepto>                  
                <nombreConcepto>' || vNombreConcepto || '</nombreConcepto>';
                vXML :=vXML ||'<formula>' || vFormula || '</formula>                  
                <argumentos>';
                --Se obtiene el numero de argumentos que contiene la formula
                vNumeroArgumentos:=(SELECT COUNT(*)
                                    FROM regexp_matches(vFormula, ':[^#0-9/*+$%-:]+', 'g'));
                FOR i IN 1 .. vNumeroArgumentos
                LOOP
                    SELECT regexp_replace(
                        (SELECT contenido
                        FROM
                            (SELECT contenido, row_number() OVER () AS indice --COUNT(*)
                            FROM 
                            regexp_matches(vFormula, ':[^#0-9/*+$%-:]+', 'g') AS contenido
                            ) AS todo
                        WHERE indice = i)::varchar, '[{:}"]', '', 'g') 
                        --WHERE indice = )::"varchar", '[[:punct:]]', '', 'g')
                        INTO vArgumento;
                    --Se obtiene el nombre del argumento
                    SELECT cod_nbargumento INTO vNombreArgumento
                    FROM sgnom.tsgnomargumento
                    WHERE cod_nbargumento = vArgumento;
                    RAISE INFO 'nomARG: % %', vArgumento, vNombreArgumento;
                    --Se obtiene la funci�n del argumento
                    SELECT des_funcionbd INTO vFuncionOracle
                    FROM sgnom.tsgnomargumento
                    WHERE cod_nbargumento = vArgumento;
                    RAISE INFO 'fnARG: %, %', vArgumento,vFuncionOracle;
                    --Se calcula la funci�n del argumento
                    IF (vFuncionOracle IS NOT NULL) THEN
                        IF (vFuncionOracle = 'fn_dias_laborados') THEN 
                            vFormulaEjecutar :='sgnom.'||vFuncionOracle||'('||vIdEmpleado||','||vIdQuincena||')';
                            vCadenaEjecutar :='select ' || vFormulaEjecutar;
                            RAISE INFO 'cadena %', vCadenaEjecutar;
                            EXECUTE vCadenaEjecutar INTO vDiasLaborados;
                            vValorArgumento :=vDiasLaborados;
                            RAISE INFO 'RES laborados %', vValorArgumento;
                        ELSIF (vFuncionOracle = 'fn_sueldo_base') THEN 
                            vFormulaEjecutar :='sgnom.'||vFuncionOracle||'('||vIdEmpleado||')';
                            vCadenaEjecutar :='select ' || vFormulaEjecutar;
                            RAISE INFO 'cadena %', vCadenaEjecutar;
                            EXECUTE vCadenaEjecutar INTO vSueldoBase;
                            vValorArgumento :=vSueldoBase;
                            RAISE INFO 'RES SB %', vValorArgumento;
                        END IF;
                    ELSE
                        SELECT imp_valorconst INTO vConstante
                        FROM sgnom.tsgnomargumento
                        WHERE cod_nbargumento = vArgumento;
                        vValorArgumento :=vConstante;
                        RAISE INFO 'RES constante %', vConstante;
                    END IF;
                    IF(vValorArgumento IS NULL) THEN 
                        vValorArgumento :=0;
                    END IF;
                    SELECT regexp_replace(vFormula2, '[:]', '', 'g') INTO vFormula2
                    FROM (SELECT des_formula
                            FROM sgnom.tsgnomformula
                            WHERE cod_formulaid =
                                    (SELECT CO.cod_formulaid_fk 
                                    FROM sgnom.tsgnomconcepto CO
                                    WHERE CO.cod_conceptoid = vIdConcepto01fk)) form;
                    --Se continua la construccion del XML agregando cada argumento
                    vXML:= vXML || '<mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>' || vArgumento || '</nombre>                            
                    <valor>' || vValorArgumento || '</valor> <descripcion>' || vNombreArgumento || '</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento>';
                END LOOP;
                EXECUTE vCadenaImporte INTO vImporteActualizado;
                vXML :=vXML || '</argumentos>';
                IF(xmlDesglose IS NOT NULL) THEN 
                    vXML := vXML || xmlDesglose;
                    xmlDesglose :=NULL;
                END IF;
                --Se agrega el importe total al XML
                vXML:=vXML || '<importe>' || vImporteActualizado || '</importe> </mx.org.ift.serp.conceptoBindingXml.DetallesConcepto>  ';
                --Se guarda el importe del concepto y el XML generado
				RAISE INFO 'dentro C4 ACTUALIZANDO EL EQ %, id %', vIdConEmpPla01, vIdEmpleado;
                UPDATE sgnom.tsgnomcncptoquinc
				SET imp_concepto = vImporteActualizado, xml_desgloce = vXML::xml
				WHERE cod_empquincenaid_fk = vIdConEmpPla01;
                --AND cod_conceptoid_fk = vIdConcepto01fk
                --AND cod_cncptoquincid = cncptoquincid;
				GET DIAGNOSTICS a_count = ROW_COUNT;
				RAISE INFO 'a_count %', a_count;
                IF (a_count>0) THEN 
                    vNumRegistros := vNumRegistros + 1 ;
					RAISE INFO 'vNumRegistros %', vNumRegistros;
                END IF;
                --COMMIT; 
            END LOOP;
            CLOSE C4;
			RAISE INFO 'fuera C4 ACTUALIZANDO EL EQ %, id %', vIdConEmpPla01, vIdEmpleado;
            vSueldoBase :=0.0;
            vDiasLaborados :=0;
            vCompensasionG :=0;
            vDespensa :=0;
            vPorcentajeManTer :=0;
            vApoyoVehicular :=0;
            vAportacionSSIGF :=0;
            vPrimaVacacional :=0;
            vFaltasSueldoEst :=0;
            vFaltasSueldoEv :=0;
            vFaltasSueldoEstAA :=0;
            vFaltasCompEst :=0;
            vFaltasCompEv :=0;
            vFaltasCompEstAA :=0;
            vISRSueldo :=0;
            vISRCompGaran :=0;
            vISRSSI :=0;
            vISRApoyoVehic :=0;
            vISRPrimaVac :=0;
            vValorQuinquenio :=0;
            vSueldoCompactado :=0;
            vSueldoBimestral :=0;
            vQuinquenioBimestral :=0;
            vSumaSRCEAYV :=0;
            vSueldoBimestralEV :=0;
            vQuinquenioBimestralEV:=0;
            vSumaSRCEAYVEV :=0;
            vPensionAlimenticia :=0;
            vSueldoCompactado :=0;
            vSumaSARP :=0;
            vSumaFOVP :=0;
            vSumaSRCEAYVPAT :=0;
            vSumaSRCEAYVPAT :=0;
            vPorcentajePension :=0;
            vISREstDesemp :=0;
            vISRHonorarios :=0;
            vValorEstimulo :=0;
            vAhorroSolidario :=0;
            vConstante :=0;
            vImpLicMedSueldo :=0; 
        END LOOP;
		CLOSE C1;
        --Se llama la funcion que calcula los importes de cada concepto
        SELECT sgnom.fn_calcula_importes_nomina(vIdNomina,NULL,0) INTO vBanderaImporte;
		--RETURN vNumRegistros::varchar;
        IF vNumRegistros > 0 THEN 
            vBandera := 1;
        ELSIF vNumRegistros = 0 THEN 
            vBandera := 0;
        ELSE 
            vBandera:=2;
        END IF;
        RETURN vBandera;
		EXCEPTION
		WHEN NO_DATA_FOUND THEN
			RAISE EXCEPTION '% exp no data found', vIdEmpleado;
			RETURN NULL;
    END;
$_$;


ALTER FUNCTION sgnom.fn_calcula_nomina(vidnomina integer) OWNER TO suite;

--
-- TOC entry 538 (class 1255 OID 98894)
-- Name: fn_cargar_nom_imss(character varying, integer); Type: FUNCTION; Schema: sgnom; Owner: postgres
--

CREATE FUNCTION sgnom.fn_cargar_nom_imss(archivo character varying, cabecera integer) RETURNS boolean
    LANGUAGE plpgsql
    AS $_$
    DECLARE
		art text;
		rec RECORD;
    BEGIN
		CREATE TEMPORARY TABLE temporal
            (
            clave integer,
            nombre_trabajador text,
            "NSS" varchar,
            "RFC" text,
            "CURP" text,
            fecha_alta date,
            departamento integer,
            tipo_salario text,
            salario_diario numeric,
            "SDI" numeric,
            dias_trabajados numeric,
            faltas numeric,
            "Sueldo" numeric,
            "SUELDO" numeric,
            premio_asistencia numeric,
            premio_puntualidad numeric,
            total_percepciones numeric,
            total_gravable numeric,
            total_imss numeric,
            "total_ISR" numeric,
            subsido_empleo numeric,
            "ISR" numeric,
            "IMSS" numeric,
            credito_infonavit numeric,
            subsidio_empleo numeric,
            subsidio_empleo_aplicado numeric,
            total_deducciones numeric,
            neto_pagado numeric
            );
		--SELECT regexp_replace(archivo , '''', '', 'g') into art;
		--SELECT quote_ident(archivo) into art;
		--SELECT regexp_matches(archivo, ':[^#0-9/*+$%-:]+', 'g') into art;
		--RAISE INFO 'RES tipo %', art;
        --COPY sgnom.tsgnomnominaimss FROM '/tmp/'.concat(archivo) DELIMITER ',' CSV HEADER;
		--execute format('copy sgnom.tsgnomnominaimss from ''/tmp/%L with delimiter '','' quote ''"'' csv ', archivo);
		execute 'copy temporal from ''C:/tmp/' || archivo || ''' with delimiter '',''  CSV HEADER';
		----copy temporal FROM 'C:\tmp\copia2.csv' DELIMITER ',' CSV HEADER;
		--execute 'copy sgnom.tsgnomnominaimss from ''/tmp/$1'' with delimiter '','' quote ''"'' csv ' USING art;
        --SELECT * FROM temporal;
		INSERT INTO sgnom.tsgnomnominaimss SELECT nextval('sgnom.seq_nom_imss'::regclass), *, cabecera FROM temporal;
		--Raise 'x % ', (SELECT nombre_trabajador FROM temporal);
		RETURN true;
    END;
$_$;


ALTER FUNCTION sgnom.fn_cargar_nom_imss(archivo character varying, cabecera integer) OWNER TO postgres;

--
-- TOC entry 512 (class 1255 OID 99005)
-- Name: fn_cuota_quin_imss(integer); Type: FUNCTION; Schema: sgnom; Owner: postgres
--

CREATE FUNCTION sgnom.fn_cuota_quin_imss(idempleado integer) RETURNS numeric
    LANGUAGE plpgsql
    AS $$
    DECLARE
		valor DECIMAL;
		--rec RECORD;
    BEGIN
		SELECT imss.imss
        FROM sgnom.tsgnomnominaimss imss
        WHERE (imss.curp, imss.nss, imss.rfc) =        
            (SELECT cod_curp, cod_nss, cod_rfc 
            FROM sgrh.tsgrhempleados 
            WHERE cod_empleado  = idEmpleado) INTO valor;
		RETURN valor;
    END;
$$;


ALTER FUNCTION sgnom.fn_cuota_quin_imss(idempleado integer) OWNER TO postgres;

--
-- TOC entry 523 (class 1255 OID 97169)
-- Name: fn_dias_laborados(integer, integer); Type: FUNCTION; Schema: sgnom; Owner: suite
--

CREATE FUNCTION sgnom.fn_dias_laborados(empid integer, prmnumquincenacalculo integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$

    DECLARE
    ----variables
    vFechaIngreso DATE;
    vFechaBaja DATE;
    vFechaIQuincena DATE;
    vFechaFQuincena DATE;
    vDiasLaborados INTEGER;
    vFecha1 INTEGER;
    vFecha2 INTEGER;
    vFecha3 INTEGER;
    vFecha1Dia INTEGER;
    vCadenaResultado VARCHAR(500);
    ERR_CODE INTEGER;
    ERR_MSG VARCHAR(250);

    ---cursores
    C11 CURSOR FOR 
        SELECT EMP.fec_ingreso, EMP.fec_salida
        FROM sgnom.tsgnomempleados EMP
        WHERE EMP.cod_empleadoid = empid;

    C12 CURSOR FOR 
        SELECT fec_inicio, fec_fin
        FROM sgnom.tsgnomquincena
        WHERE cod_quincenaid =
                    (SELECT cod_quincenaid
                    FROM sgnom.tsgnomquincena Q
                    INNER JOIN sgnom.tsgnomejercicio E ON Q.cod_ejercicioid_fk = E.cod_ejercicioid
                    WHERE cod_quincenaid = prmNumQuincenaCalculo
                    AND E.cnu_valorejercicio = (
                        SELECT MAX(cnu_valorejercicio)
                        FROM sgnom.tsgnomejercicio EJC
                        INNER JOIN sgnom.tsgnomquincena CQ ON EJC.cod_ejercicioid = CQ.cod_ejercicioid_fk
                        INNER JOIN sgnom.tsgnomcabecera CAB ON CQ.cod_quincenaid = CAB.cod_quincenaid_fk));

    BEGIN
		RAISE INFO 'inicia funcion laborales';
        OPEN C11; RAISE INFO 'inicia C11 laboral';
        LOOP
        FETCH C11 INTO vFechaIngreso, vFechaBaja;
			RAISE INFO 'contenido C11 % , %', vFechaIngreso,vFechaBaja;
            EXIT WHEN not found;
            OPEN C12; RAISE INFO 'inicia C12 laboral';
            LOOP
            FETCH C12 INTO vFechaIQuincena, vFechaFQuincena;
                RAISE INFO 'contenido C12 % , %', vFechaIQuincena,vFechaFQuincena;
                EXIT WHEN not found;
                -- DBMS_OUTPUT.PUT_LINE('vFechaIngreso '||vFechaIngreso||' vFechaBaja '||vFechaBaja||' vFechaIQuincena '||vFechaIQuincena||' vFechaFQuincena '||vFechaFQuincena );
                --Escenario1:trabajo la quincena completa,
                --ya sea porque ya llevaba tiempo trabajando o ingreso en inicio de quiincena
                IF(vFechaIngreso<=vFechaIQuincena AND vFechaBaja IS NULL) THEN
                    vDiasLaborados:= 15;
                --Escenario2:ingreso despues del inicio de la quincena,
                --se toma la fecha fin de la quincea y se le resta la fecha en la que entro,
                ELSIF(vFechaIngreso >vFechaIQuincena AND vFechaBaja IS NULL) THEN
                    vFecha1 :=TO_CHAR(vFechaFQuincena,'YYYYMMDD');
                    vFecha1Dia :=TO_CHAR(vFechaFQuincena,'DD');
                    vFecha2 :=TO_CHAR(vFechaIngreso,'YYYYMMDD');
                    IF(vFecha1!=vFecha2) THEN
                        IF(vFecha1Dia =28) THEN
                            vFecha1 :=vFecha1+2;
                        ELSIF(vFecha1Dia=29) THEN
                            vFecha1 :=vFecha1+1;
                        ELSIF(vFecha1Dia=31) THEN
                            vFecha1 :=vFecha1-1;
                        END IF;
                    END IF;
                    vDiasLaborados:= (vFecha1-vFecha2)+1;
                --Escenario3:se dio de baja antes de que termine la quincena, se toma la fecha de baja y se le resta la fecha de inicio de quincena
                ELSIF(vFechaIngreso<=vFechaIQuincena AND vFechaBaja IS NOT NULL) THEN
                    vFecha1 :=TO_CHAR(vFechaBaja,'YYYYMMDD');
                    vFecha2 :=TO_CHAR(vFechaIQuincena,'YYYYMMDD');
                    vDiasLaborados :=(vFecha1-vFecha2)+1;
                    IF(vDiasLaborados >15) THEN
                        vDiasLaborados :=15;
                    END IF;
                --Escenario4:se dio de alta despues del inicio de la quincena y se dio de baja antes que terminar la quincena,
                --se toma la fecha de baja y se le resta la fecha de ingreso
                ELSIF(vFechaIngreso>vFechaIQuincena AND vFechaBaja IS NOT NULL) THEN
                    vFecha1 :=TO_CHAR(vFechaBaja,'YYYYMMDD');
                    vFecha2 :=TO_CHAR(vFechaIngreso,'YYYYMMDD');
                    vFecha3 :=TO_CHAR(vFechaFQuincena,'YYYYMMDD');
                    vFecha1Dia :=TO_CHAR(vFechaFQuincena,'DD');
                    IF(vFecha1=vFecha3) THEN
                        IF(vFecha1Dia =28) THEN
                            vFecha1 :=vFecha1+2;
                        ELSIF(vFecha1Dia=29) THEN
                            vFecha1 :=vFecha1+1;
                        ELSIF(vFecha1Dia=31) THEN
                            vFecha1 :=vFecha1-1;
                        END IF;
                    END IF;
                    vDiasLaborados := (vFecha1-vFecha2)+1;
                END IF;
                RAISE INFO 'dentro loop laborados %', vDiasLaborados;
            END LOOP;
			RAISE INFO 'fin loop 2';
            CLOSE C12;
        END LOOP;
        RAISE INFO 'fin loop 1';
        CLOSE C11;
        --vCadenaResultado:=TO_CHAR(vDiasLaborados);
        RAISE INFO 'final laborados %', vDiasLaborados;
        --DBMS_OUTPUT.PUT_LINE(vCadenaResultado);
        RETURN vDiasLaborados;
		EXCEPTION
		WHEN NO_DATA_FOUND THEN
			RAISE EXCEPTION '% exp no data found', empid;
			RETURN NULL;
		WHEN OTHERS THEN
			--err_code := SQLCODE;
			--err_msg := SUBSTR(SQLERRM, 1, 200);
			RAISE EXCEPTION '% exp others', empid;
			--SPDERRORES('FN_DIAS_LABORADOS',err_code,err_msg,prmIdEmpleado);
			RETURN NULL;
    END;
$$;


ALTER FUNCTION sgnom.fn_dias_laborados(empid integer, prmnumquincenacalculo integer) OWNER TO suite;

--
-- TOC entry 498 (class 1255 OID 97170)
-- Name: fn_empleadosquincena(integer); Type: FUNCTION; Schema: sgnom; Owner: postgres
--

CREATE FUNCTION sgnom.fn_empleadosquincena(idcabecera integer) RETURNS TABLE(idemp_nom integer, idemp_quincena integer, nombre text, puesto character varying, area character varying)
    LANGUAGE plpgsql
    AS $$
BEGIN 
RETURN QUERY 
	SELECT empq.cod_empleadoid_fk idemp_nom,
		empq.cod_empquincenaid idemp_quincena,
		--rhemp.cod_empleado idemp_rh,
		CONCAT(rhemp.des_nombre , ' ', rhemp.des_nombres,  ' ', rhemp.des_apepaterno, ' ', rhemp.des_apematerno)::text nombre,
		(SELECT ps.des_puesto FROM sgrh.tsgrhpuestos ps WHERE ps.cod_puesto = rhemp.cod_puesto ) puesto,
		(SELECT ar.des_nbarea FROM sgrh.tsgrhareas ar WHERE ar.cod_area = (SELECT ps.cod_puesto FROM sgrh.tsgrhpuestos ps WHERE ps.cod_puesto = rhemp.cod_puesto )) area
	FROM sgnom.tsgnomempleados nomemp 
	JOIN sgrh.tsgrhempleados rhemp ON nomemp.cod_empleado_fk = rhemp.cod_empleado
	JOIN sgnom.tsgnomempquincena empq ON nomemp.cod_empleadoid = empq.cod_empleadoid_fk
	WHERE empq.cod_cabeceraid_fk = idcabecera
	ORDER BY 1;
END;
$$;


ALTER FUNCTION sgnom.fn_empleadosquincena(idcabecera integer) OWNER TO postgres;

--
-- TOC entry 524 (class 1255 OID 97171)
-- Name: fn_incidencias_por_quincena(integer); Type: FUNCTION; Schema: sgnom; Owner: suite
--

CREATE FUNCTION sgnom.fn_incidencias_por_quincena(quincena integer) RETURNS TABLE(idincidencia integer, fechaalta date, clave character varying, incidencia character varying, idtipo character, desctipo text, cantidad smallint, actividad character varying, comentarios text, reportaid integer, reportanb text, autorizaid integer, autorizanb text, perfil character varying, detallefechas text, montoincidencia numeric, montopagado numeric, validacion boolean, aceptacion boolean, quincenaid integer, desquincena character varying, creaid integer, creanb text)
    LANGUAGE plpgsql
    AS $$
BEGIN 
RETURN QUERY 
SELECT incidencias.cod_incidenciaid idincidencia,
		incidencias.aud_feccreacion fechaalta,
		catincidencias.cod_claveincidencia clave,
		catincidencias.cod_nbincidencia incidencia,
		catincidencias.cod_tipoincidencia idtipo,
		CASE 
			WHEN catincidencias.cod_tipoincidencia='1' THEN
			 'HORAS'
			WHEN catincidencias.cod_tipoincidencia='2' THEN
			 'DIAS' 
		  WHEN catincidencias.cod_tipoincidencia='3' THEN
			 'ACTIVIDAD' 
			ELSE
			 'NO DATA'
		END desc_tipo,
		incidencias.cnu_cantidad cantidad,
		incidencias.des_actividad actividad,
		incidencias.txt_comentarios comentarios,
		incidencias.cod_empreporta_fk reportaid,
		(SELECT CONCAT(rhempleados.des_nombre ||' ', rhempleados.des_nombres ,' ' || rhempleados.des_apepaterno || ' ' || rhempleados.des_apematerno) 
		FROM sgrh.tsgrhempleados rhempleados
		WHERE rhempleados.cod_empleado = (SELECT nom.cod_empleado_fk FROM sgnom.tsgnomempleados nom WHERE nom.cod_empleadoid = incidencias.cod_empreporta_fk)
		) reportanb,
		incidencias.cod_empautoriza_fk autorizaid,
		(SELECT CONCAT(rhempleados.des_nombre ||' ', rhempleados.des_nombres ,' ' || rhempleados.des_apepaterno || ' ' || rhempleados.des_apematerno) 
		FROM sgrh.tsgrhempleados rhempleados
		WHERE rhempleados.cod_empleado = incidencias.cod_empautoriza_fk
		) autorizanb,
		catincidencias.cod_perfilincidencia perfil,
		CAST(incidencias.xml_detcantidad AS TEXT) detallefechas,
		catincidencias.imp_monto montoincidencia,
		incidencias.imp_monto montopagado,
		incidencias.bol_validacion validacion, --validacion rh
		incidencias.bol_aceptacion aceptacion, --aceptacion lider de celula
		incidencias.cod_quincenaid_fk quincena,
		(SELECT qui.des_quincena FROM sgnom.tsgnomquincena qui WHERE qui.cod_quincenaid = incidencias.cod_quincenaid_fk) desquincena,
		incidencias.aud_codcreadopor creaid,
		(SELECT CONCAT(rhempleados.des_nombre ||' ', rhempleados.des_nombres ,' ' || rhempleados.des_apepaterno || ' ' || rhempleados.des_apematerno) 
		FROM sgrh.tsgrhempleados rhempleados
		WHERE rhempleados.cod_empleado = incidencias.aud_codcreadopor
		) creanb
FROM sgnom.tsgnomincidencia incidencias
JOIN sgnom.tsgnomcatincidencia catincidencias 
ON incidencias.cod_catincidenciaid_fk = catincidencias.cod_catincidenciaid
--WHERE incidencias.cod_empreporta_fk = idempleado 
	AND incidencias.cod_quincenaid_fk = quincena
	AND incidencias.bol_estatus = true 
ORDER BY 1 DESC, 2;

END;
$$;


ALTER FUNCTION sgnom.fn_incidencias_por_quincena(quincena integer) OWNER TO suite;

--
-- TOC entry 513 (class 1255 OID 99006)
-- Name: fn_infonavit_imss(integer); Type: FUNCTION; Schema: sgnom; Owner: postgres
--

CREATE FUNCTION sgnom.fn_infonavit_imss(idempleado integer) RETURNS numeric
    LANGUAGE plpgsql
    AS $$
    DECLARE
		valor DECIMAL;
		--rec RECORD;
    BEGIN
		SELECT imss.credito_infonavit
        FROM sgnom.tsgnomnominaimss imss
        WHERE (imss.curp, imss.nss, imss.rfc) =        
            (SELECT cod_curp, cod_nss, cod_rfc 
            FROM sgrh.tsgrhempleados 
            WHERE cod_empleado  = idEmpleado) INTO valor;
		RETURN valor;
    END;
$$;


ALTER FUNCTION sgnom.fn_infonavit_imss(idempleado integer) OWNER TO postgres;

--
-- TOC entry 539 (class 1255 OID 98951)
-- Name: fn_insertacabecera(integer, integer, character varying, integer); Type: FUNCTION; Schema: sgnom; Owner: suite
--

CREATE FUNCTION sgnom.fn_insertacabecera(quincena integer, tipo integer, nombre character varying, credopor integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$
    DECLARE
    banderaRepetido BOOLEAN;
    rec1 RECORD;
    contador integer := 0;
    bandera boolean := false;
    rec RECORD;
    rec2 RECORD;
    cabecera INTEGER;
    BEGIN

        FOR rec1 IN SELECT cod_quincenaid_fk, cod_tiponominaid_fk, cod_cabeceraid
        FROM sgnom.tsgnomcabecera
		WHERE cod_estatusnomid_fk != 6
        ORDER BY 3 DESC
        LOOP
            RAISE INFO 'RES for quincena %', rec1.cod_quincenaid_fk;
            RAISE INFO 'RES for tipo %', rec1.cod_tiponominaid_fk;
            IF((rec1.cod_quincenaid_fk = quincena) AND (rec1.cod_tiponominaid_fk = tipo)) THEN
                RAISE INFO 'RES if quincena %', rec1.cod_quincenaid_fk;
                RAISE INFO 'RES if tipo %', rec1.cod_tiponominaid_fk;
                RAISE INFO 'repetido ';
                banderaRepetido = TRUE;
            END IF;
        END LOOP; 

        IF (banderaRepetido) THEN
            RAISE INFO '\\\\\\\\\\';
        ELSE
            RAISE INFO 'crea nomina';
            
            INSERT INTO sgnom.tsgnomcabecera(
                cod_nbnomina, fec_creacion, 
                imp_totpercepcion, imp_totdeduccion, imp_totalemp, 
                cod_quincenaid_fk, cod_tiponominaid_fk, cod_estatusnomid_fk, 
                aud_codcreadopor, aud_feccreacion)
                VALUES (nombre, CURRENT_DATE, 
                0.0, 0.0, 0.0, 
                quincena, tipo, 1,
                credopor, CURRENT_DATE)  RETURNING cod_cabeceraid INTO cabecera;

            FOR rec IN SELECT cod_empleadoid 
                        FROM sgnom.tsgnomempleados 
                        WHERE tsgnomempleados.fec_ingreso <= 
                                (SELECT tsgnomquincena.fec_fin
                                FROM sgnom.tsgnomquincena, sgnom.tsgnomcabecera
                                WHERE tsgnomquincena.cod_quincenaid = tsgnomcabecera.cod_quincenaid_fk
                                AND tsgnomcabecera.cod_cabeceraid = cabecera)
                        AND (tsgnomempleados.fec_salida >= 
                                (SELECT tsgnomquincena.fec_inicio
                                FROM sgnom.tsgnomquincena, sgnom.tsgnomcabecera
                                WHERE tsgnomquincena.cod_quincenaid = tsgnomcabecera.cod_quincenaid_fk
                                AND tsgnomcabecera.cod_cabeceraid = cabecera)
                        OR tsgnomempleados.fec_salida IS NULL)
                        AND tsgnomempleados.bol_estatus = 't'
                        ORDER BY 1
            LOOP
                INSERT INTO sgnom.tsgnomempquincena(cod_empleadoid_fk, cod_cabeceraid_fk, bol_estatusemp,
												   imp_totpercepcion, imp_totdeduccion, imp_totalemp)
                VALUES (rec.cod_empleadoid, cabecera, 't',
					   0.0, 0.0, 0.0);
                
                contador := contador + 1;
            END LOOP;
            FOR rec2 IN SELECT cod_empquincenaid 
                        FROM sgnom.tsgnomempquincena 
                        WHERE cod_cabeceraid_fk = cabecera
            LOOP
                INSERT INTO sgnom.tsgnomconfpago(
                    cod_empquincenaid_fk)
                    VALUES (rec2.cod_empquincenaid);
				INSERT INTO sgnom.tsgnomcncptoquinc(
	 				cod_empquincenaid_fk, cod_conceptoid_fk, imp_concepto, 
					imp_gravado, imp_exento)
				VALUES (rec2.cod_empquincenaid, 1, 0.0, 
						0.0, 0.0);
            END LOOP;
            IF (contador > 0) THEN
				UPDATE sgnom.tsgnomcabecera SET cnu_totalemp = contador WHERE cod_cabeceraid = cabecera;

                bandera := true;

            ELSIF (contador = 0) THEN
                --ROLLBACK;
                bandera := false;
            END IF;
            
        END IF;
        IF bandera = false THEN
            --devuelve esta bandera
            --RAISE 'entra';
            DELETE FROM sgnom.tsgnomcabecera WHERE cod_cabeceraid = cabecera;
            return 0;
        END IF;
        --EXCEPTION 
        --WHEN OTHERS THEN 
            --ROLLBACK;
        RETURN cabecera;
    END;
$$;


ALTER FUNCTION sgnom.fn_insertacabecera(quincena integer, tipo integer, nombre character varying, credopor integer) OWNER TO suite;

--
-- TOC entry 527 (class 1255 OID 97173)
-- Name: fn_insertacabecera_buena(integer, integer, character varying, integer); Type: FUNCTION; Schema: sgnom; Owner: suite
--

CREATE FUNCTION sgnom.fn_insertacabecera_buena(quincena integer, tipo integer, nombre character varying, credopor integer) RETURNS boolean
    LANGUAGE plpgsql
    AS $$
    DECLARE
    banderaRepetido BOOLEAN;
    rec1 RECORD;
    contador integer := 0;
    bandera boolean := false;
    rec RECORD;
    rec2 RECORD;
    cabecera INTEGER;
    BEGIN

        FOR rec1 IN SELECT cod_quincenaid_fk, cod_tiponominaid_fk, cod_cabeceraid
        FROM sgnom.tsgnomcabecera
		WHERE cod_estatusnomid_fk != 6
        ORDER BY 3 DESC
        LOOP
            RAISE INFO 'RES for quincena %', rec1.cod_quincenaid_fk;
            RAISE INFO 'RES for tipo %', rec1.cod_tiponominaid_fk;
            IF((rec1.cod_quincenaid_fk = quincena) AND (rec1.cod_tiponominaid_fk = tipo)) THEN
                RAISE INFO 'RES if quincena %', rec1.cod_quincenaid_fk;
                RAISE INFO 'RES if tipo %', rec1.cod_tiponominaid_fk;
                RAISE INFO 'repetido ';
                banderaRepetido = TRUE;
            END IF;
        END LOOP; 

        IF (banderaRepetido) THEN
            RAISE INFO '\\\\\\\\\\';
        ELSE
            RAISE INFO 'crea nomina';
            
            INSERT INTO sgnom.tsgnomcabecera(
                cod_nbnomina, fec_creacion, 
                imp_totpercepcion, imp_totdeduccion, imp_totalemp, 
                cod_quincenaid_fk, cod_tiponominaid_fk, cod_estatusnomid_fk, 
                aud_codcreadopor, aud_feccreacion)
                VALUES (nombre, CURRENT_DATE, 
                0.0, 0.0, 0.0, 
                quincena, tipo, 1,
                credopor, CURRENT_DATE)  RETURNING cod_cabeceraid INTO cabecera;

            FOR rec IN SELECT cod_empleadoid 
                        FROM sgnom.tsgnomempleados 
                        WHERE tsgnomempleados.fec_ingreso <= 
                                (SELECT tsgnomquincena.fec_fin
                                FROM sgnom.tsgnomquincena, sgnom.tsgnomcabecera
                                WHERE tsgnomquincena.cod_quincenaid = tsgnomcabecera.cod_quincenaid_fk
                                AND tsgnomcabecera.cod_cabeceraid = cabecera)
                        AND (tsgnomempleados.fec_salida >= 
                                (SELECT tsgnomquincena.fec_inicio
                                FROM sgnom.tsgnomquincena, sgnom.tsgnomcabecera
                                WHERE tsgnomquincena.cod_quincenaid = tsgnomcabecera.cod_quincenaid_fk
                                AND tsgnomcabecera.cod_cabeceraid = cabecera)
                        OR tsgnomempleados.fec_salida IS NULL)
                        AND tsgnomempleados.bol_estatus = 't'
                        ORDER BY 1
            LOOP
                INSERT INTO sgnom.tsgnomempquincena(cod_empleadoid_fk, cod_cabeceraid_fk, bol_estatusemp,
												   imp_totpercepcion, imp_totdeduccion, imp_totalemp)
                VALUES (rec.cod_empleadoid, cabecera, 't',
					   0.0, 0.0, 0.0);
                
                contador := contador + 1;
            END LOOP;
            FOR rec2 IN SELECT cod_empquincenaid 
                        FROM sgnom.tsgnomempquincena 
                        WHERE cod_cabeceraid_fk = cabecera
            LOOP
                INSERT INTO sgnom.tsgnomconfpago(
                    cod_empquincenaid_fk)
                    VALUES (rec2.cod_empquincenaid);
				INSERT INTO sgnom.tsgnomcncptoquinc(
	 				cod_empquincenaid_fk, cod_conceptoid_fk, imp_concepto, 
					imp_gravado, imp_exento)
				VALUES (rec2.cod_empquincenaid, 1, 0.0, 
						0.0, 0.0);
            END LOOP;
            IF (contador > 0) THEN
				UPDATE sgnom.tsgnomcabecera SET cnu_totalemp = contador WHERE cod_cabeceraid = cabecera;

                bandera := true;

            ELSIF (contador = 0) THEN
                --ROLLBACK;
                bandera := false;
            END IF;
            
        END IF;
        IF bandera = false THEN
            --devuelve esta bandera
            --RAISE 'entra';
            DELETE FROM sgnom.tsgnomcabecera WHERE cod_cabeceraid = cabecera;
            return bandera;
        END IF;
        --EXCEPTION 
        --WHEN OTHERS THEN 
            --ROLLBACK;
        RETURN bandera;
    END;
$$;


ALTER FUNCTION sgnom.fn_insertacabecera_buena(quincena integer, tipo integer, nombre character varying, credopor integer) OWNER TO suite;

--
-- TOC entry 514 (class 1255 OID 99007)
-- Name: fn_isr_imss(integer); Type: FUNCTION; Schema: sgnom; Owner: postgres
--

CREATE FUNCTION sgnom.fn_isr_imss(idempleado integer) RETURNS numeric
    LANGUAGE plpgsql
    AS $$
    DECLARE
		valor DECIMAL;
		--rec RECORD;
    BEGIN
		SELECT imss.isr
        FROM sgnom.tsgnomnominaimss imss
        WHERE (imss.curp, imss.nss, imss.rfc) =        
            (SELECT cod_curp, cod_nss, cod_rfc 
            FROM sgrh.tsgrhempleados 
            WHERE cod_empleado  = idEmpleado) INTO valor;
		RETURN valor;
    END;
$$;


ALTER FUNCTION sgnom.fn_isr_imss(idempleado integer) OWNER TO postgres;

--
-- TOC entry 511 (class 1255 OID 99004)
-- Name: fn_prima_vacacional_imss(integer); Type: FUNCTION; Schema: sgnom; Owner: postgres
--

CREATE FUNCTION sgnom.fn_prima_vacacional_imss(idempleado integer) RETURNS numeric
    LANGUAGE plpgsql
    AS $$
    DECLARE
		valor DECIMAL;
		--rec RECORD;
    BEGIN
		SELECT imss.prima_vacacional
        FROM sgnom.tsgnomnominaimss imss
        WHERE (imss.curp, imss.nss, imss.rfc) =        
            (SELECT cod_curp, cod_nss, cod_rfc 
            FROM sgrh.tsgrhempleados 
            WHERE cod_empleado  = idEmpleado) INTO valor;
		RETURN valor;
    END;
$$;


ALTER FUNCTION sgnom.fn_prima_vacacional_imss(idempleado integer) OWNER TO postgres;

--
-- TOC entry 515 (class 1255 OID 99008)
-- Name: fn_subsidio_empleo_imss(integer); Type: FUNCTION; Schema: sgnom; Owner: postgres
--

CREATE FUNCTION sgnom.fn_subsidio_empleo_imss(idempleado integer) RETURNS numeric
    LANGUAGE plpgsql
    AS $$
    DECLARE
		valor DECIMAL;
		--rec RECORD;
    BEGIN
		SELECT imss.subsidio_empleo
        FROM sgnom.tsgnomnominaimss imss
        WHERE (imss.curp, imss.nss, imss.rfc) =        
            (SELECT cod_curp, cod_nss, cod_rfc 
            FROM sgrh.tsgrhempleados 
            WHERE cod_empleado  = idEmpleado) INTO valor;
		RETURN valor;
    END;
$$;


ALTER FUNCTION sgnom.fn_subsidio_empleo_imss(idempleado integer) OWNER TO postgres;

--
-- TOC entry 525 (class 1255 OID 97174)
-- Name: fn_sueldo_base(integer); Type: FUNCTION; Schema: sgnom; Owner: suite
--

CREATE FUNCTION sgnom.fn_sueldo_base(empid integer) RETURNS numeric
    LANGUAGE plpgsql
    AS $$
    DECLARE
    rtSueldoBase numeric;
    BEGIN
        SELECT imp_honorarios 
        INTO rtSueldoBase
        FROM sgnom.tsgnomempleados
        WHERE cod_empleadoid = empid;
        RETURN rtSueldoBase;
    END;
$$;


ALTER FUNCTION sgnom.fn_sueldo_base(empid integer) OWNER TO suite;

--
-- TOC entry 510 (class 1255 OID 99003)
-- Name: fn_vacaciones_imss(integer); Type: FUNCTION; Schema: sgnom; Owner: postgres
--

CREATE FUNCTION sgnom.fn_vacaciones_imss(idempleado integer) RETURNS numeric
    LANGUAGE plpgsql
    AS $$
    DECLARE
		valor DECIMAL;
		--rec RECORD;
    BEGIN
		SELECT imss.vacaciones
        FROM sgnom.tsgnomnominaimss imss
        WHERE (imss.curp, imss.nss, imss.rfc) =        
            (SELECT cod_curp, cod_nss, cod_rfc 
            FROM sgrh.tsgrhempleados 
            WHERE cod_empleado  = idEmpleado) INTO valor;
		RETURN valor;
    END;
$$;


ALTER FUNCTION sgnom.fn_vacaciones_imss(idempleado integer) OWNER TO postgres;

--
-- TOC entry 499 (class 1255 OID 97175)
-- Name: fn_validapagosnomina(integer); Type: FUNCTION; Schema: sgnom; Owner: suite
--

CREATE FUNCTION sgnom.fn_validapagosnomina(cabecera integer) RETURNS boolean
    LANGUAGE plpgsql
    AS $$
    DECLARE
    contador integer := 0;
    bandera boolean := false;
    rec RECORD;
    BEGIN
		FOR rec IN SELECT bol_pagofinanzas, bol_pagorh
                    FROM sgnom.Tsgnomconfpago 
                    WHERE cod_empquincenaid_fk IN ( SELECT cod_empquincenaid  
                                                    FROM sgnom.tsgnomempquincena 
                                                    WHERE cod_cabeceraid_fk = cabecera )
            LOOP
                RAISE INFO 'finanzas %, rh % ', rec.bol_pagofinanzas, rec.bol_pagorh;
                IF (rec.bol_pagofinanzas = rec.bol_pagorh) THEN
                    RAISE INFO 'acuerdo ';
                ELSE 
                    RAISE INFO 'desacuerdo ';
                    contador := contador + 1;
                END IF;
            END LOOP;

            IF (contador > 0) THEN
				RAISE INFO 'error ';
                bandera := false;
            ELSIF (contador = 0) THEN
                --ROLLBACK;
                RAISE INFO 'bueno ';
                bandera := true;
            END IF;
        RETURN bandera;
    END;
$$;


ALTER FUNCTION sgnom.fn_validapagosnomina(cabecera integer) OWNER TO suite;

--
-- TOC entry 526 (class 1255 OID 97176)
-- Name: historialquincenasemp(integer); Type: FUNCTION; Schema: sgnom; Owner: suite
--

CREATE FUNCTION sgnom.historialquincenasemp(empleado integer) RETURNS TABLE(cod_empleado integer, fecha_ingreso text, nom_empleado text, des_nbarea character varying, des_puesto character varying, des_quincena character varying, fec_inicio date, fec_fin date, fec_pago date, imp_concepto numeric, quincena integer)
    LANGUAGE plpgsql
    AS $$
BEGIN 
RETURN QUERY 
select empquincena.cod_empleadoid_fk,to_char(nomempl.fec_ingreso,'dd/MM/yyyy'),
(((rhempleados.des_nombre::text || ' '::text) || rhempleados.des_apepaterno::text) || ' '::text) || rhempleados.des_apematerno::text AS nom_empleado,
rharea.des_nbarea,
rhpuestos.des_puesto,
nomquincenas.des_quincena,
nomquincenas.fec_inicio,
nomquincenas.fec_fin,
nomquincenas.fec_pago,
cncptoquincenas.imp_concepto,
cncptoquincenas.cod_empquincenaid_fk
from sgnom.tsgnomcncptoquinc cncptoquincenas
JOIN sgnom.tsgnomempquincena as empquincena
ON empquincena.cod_empquincenaid = cncptoquincenas.cod_empquincenaid_fk
JOIN sgnom.tsgnomcabecera nomcabecera
ON empquincena.cod_cabeceraid_fk = nomcabecera.cod_cabeceraid
JOIN sgnom.tsgnomquincena nomquincenas
ON nomcabecera.cod_quincenaid_fk = nomquincenas.cod_quincenaid
JOIN sgnom.tsgnomempleados as nomempl
ON nomempl.cod_empleadoid = empquincena.cod_empleadoid_fk
JOIN sgrh.tsgrhempleados rhempleados
ON nomempl.cod_empleadoid = rhempleados.cod_empleado
JOIN sgrh.tsgrhpuestos rhpuestos ON rhempleados.cod_puesto = rhpuestos.cod_puesto
JOIN sgrh.tsgrhareas rharea ON rhempleados.cod_area = rharea.cod_area
WHERE nomempl.cod_empleadoid =(empleado);
END;
$$;


ALTER FUNCTION sgnom.historialquincenasemp(empleado integer) OWNER TO suite;

--
-- TOC entry 522 (class 1255 OID 97177)
-- Name: incidencias_por_area(integer); Type: FUNCTION; Schema: sgnom; Owner: suite
--

CREATE FUNCTION sgnom.incidencias_por_area(area integer) RETURNS TABLE(incidenciaid integer)
    LANGUAGE plpgsql
    AS $$
BEGIN 
RETURN QUERY 
	SELECT inc.cod_incidenciaid incidenciaid FROM sgnom.tsgnomincidencia inc WHERE inc.cod_empreporta_fk IN 
		(SELECT nom.cod_empleadoid FROM sgnom.tsgnomempleados nom WHERE nom.cod_empleado_fk IN 
			(SELECT rhe.cod_empleado FROM sgrh.tsgrhempleados rhe WHERE rhe.cod_puesto IN 
				(SELECT pst.cod_puesto FROM sgrh.tsgrhpuestos pst WHERE pst.cod_area = area)
			) 
		);
END;
$$;


ALTER FUNCTION sgnom.incidencias_por_area(area integer) OWNER TO suite;

--
-- TOC entry 501 (class 1255 OID 97178)
-- Name: incidencias_quincena(); Type: FUNCTION; Schema: sgnom; Owner: suite
--

CREATE FUNCTION sgnom.incidencias_quincena() RETURNS TABLE(idincidencia integer, clave character varying, incidencia character varying, idtipo character, desctipo text, cantidad smallint, actividad character varying, detallefechas text, comentarios text, importe numeric, reporta integer, autoriza integer, nombre text, perfil character varying, aceptacion boolean, validacion boolean, modifica integer)
    LANGUAGE plpgsql
    AS $$
BEGIN 
RETURN QUERY 
SELECT incidencias.cod_incidenciaid idincidencia,
        catincidencias.cod_claveincidencia clave,
        catincidencias.cod_nbincidencia incidencia,
        catincidencias.cod_tipoincidencia idtipo,
        CASE 
            WHEN catincidencias.cod_tipoincidencia='1' THEN
             'HORAS'
            WHEN catincidencias.cod_tipoincidencia='2' THEN
             'DIAS' 
          WHEN catincidencias.cod_tipoincidencia='3' THEN
             'ACTIVIDAD' 
            ELSE
             'NO DATA'
        END desctipo,
        incidencias.cnu_cantidad cantidad,
        incidencias.des_actividad actividad,
        CAST(incidencias.xml_detcantidad AS TEXT) detallefechas,
        incidencias.txt_comentarios comentarios,
        incidencias.imp_monto importe,
        incidencias.cod_empreporta_fk reporta, --Codigo del empleado que reporta (sgnom)
        incidencias.cod_empautoriza_fk autoriza, --Codigo del empleado que autoriza (sgnom)
        CONCAT(rhempleados.des_nombre ||' ', rhempleados.des_nombres ,' ' || rhempleados.des_apepaterno || ' ' || rhempleados.des_apematerno), --Nombre del empleado (RH)
        catincidencias.cod_perfilincidencia perfil, --Perfil del empleado que reporta (Incidencia)
        incidencias.bol_aceptacion aceptacion,
        incidencias.bol_validacion validacion,
        incidencias.aud_codmodificadopor modifica
FROM sgnom.tsgnomincidencia incidencias
JOIN sgnom.tsgnomcatincidencia catincidencias ON incidencias.cod_catincidenciaid_fk = catincidencias.cod_catincidenciaid
JOIN sgnom.tsgnomempleados nomemp ON incidencias.cod_empreporta_fk = nomemp.cod_empleadoid
JOIN sgrh.tsgrhempleados rhempleados ON rhempleados.cod_empleado = nomemp.cod_empleado_fk
WHERE incidencias.cod_quincenaid_fk = (SELECT nomquincena.cod_quincenaid FROM sgnom.tsgnomquincena nomquincena
WHERE nomquincena.fec_inicio <= CURRENT_DATE AND nomquincena.fec_fin >= CURRENT_DATE)AND incidencias.bol_estatus = true 
ORDER BY idincidencia;
END;
$$;


ALTER FUNCTION sgnom.incidencias_quincena() OWNER TO suite;

--
-- TOC entry 500 (class 1255 OID 97179)
-- Name: infohistorial(integer); Type: FUNCTION; Schema: sgnom; Owner: suite
--

CREATE FUNCTION sgnom.infohistorial(empleado integer) RETURNS TABLE(cod_empleadoid integer, nombre_completo text, area character varying, puesto character varying, sueldo numeric, fec_ingreso date)
    LANGUAGE plpgsql
    AS $$
BEGIN 
RETURN QUERY 
SELECT nomempleados.cod_empleadoid,
    (((rhempleados.des_nombre::text || ' '::text) || rhempleados.des_apepaterno::text) || ' '::text) || rhempleados.des_apematerno::text AS nombre_completo,
    rharea.des_nbarea,
    rhpuestos.des_puesto,
    sum(nomempleados.imp_sueldoimss + nomempleados.imp_honorarios) AS sueldo,
	nomempleados.fec_ingreso
   FROM sgnom.tsgnomempleados nomempleados
     JOIN sgrh.tsgrhempleados rhempleados ON nomempleados.cod_empleado_fk = rhempleados.cod_empleado
     JOIN sgrh.tsgrhareas rharea ON rhempleados.cod_area = rharea.cod_area
     JOIN sgrh.tsgrhpuestos rhpuestos ON rhempleados.cod_puesto = rhpuestos.cod_puesto
  where nomempleados.cod_empleadoid=(empleado)
  GROUP BY nomempleados.cod_empleadoid, ((((rhempleados.des_nombre::text || ' '::text) || rhempleados.des_apepaterno::text) || ' '::text) || rhempleados.des_apematerno::text), rharea.des_nbarea, rhpuestos.des_puesto
  ORDER BY nomempleados.cod_empleadoid;
  END;
$$;


ALTER FUNCTION sgnom.infohistorial(empleado integer) OWNER TO suite;

--
-- TOC entry 528 (class 1255 OID 97180)
-- Name: insertar_incidencia_por_empleado(integer, integer, character varying, text, integer, integer, numeric, character varying); Type: FUNCTION; Schema: sgnom; Owner: suite
--

CREATE FUNCTION sgnom.insertar_incidencia_por_empleado(incidenciaid integer, cantidad integer, actividad character varying, comentarios text, reporta integer, crea integer, monto numeric, fechas character varying) RETURNS boolean
    LANGUAGE plpgsql
    AS $$

DECLARE vXML varchar(4000);
arregloFechas DATE[];

BEGIN 
arregloFechas = fechas::DATE[];
vXML :='<?xml version="1.0" encoding="UTF-8"?>                
		<DetalleFechas>';
FOR i IN 1 .. array_upper(arregloFechas, 1)
   LOOP
      vXML := vXML ||'<fecha>' || TO_CHAR(arregloFechas[i],'dd-MM-yyyy') || '</fecha>';
   END LOOP;
vXML := vXML || '</DetalleFechas>';

	INSERT INTO sgnom.tsgnomincidencia(
											"cod_incidenciaid",
											"cod_catincidenciaid_fk", 
											"cnu_cantidad", 
											"des_actividad", 
											"txt_comentarios", 
											"cod_empreporta_fk",
										  "imp_monto",
										  "xml_detcantidad",
											"bol_estatus", 
											"cod_quincenaid_fk",
											"aud_codcreadopor",
											"aud_feccreacion")
			VALUES (
											NEXTVAL('sgnom.seq_incidencia'::regclass), 
											incidenciaid, 
											cantidad, 
											actividad, 
											comentarios, 
											reporta,--nom
											monto,
											vXML :: XML,
											't',
											(SELECT nomquincena.cod_quincenaid 
												FROM sgnom.tsgnomquincena nomquincena
												WHERE nomquincena.fec_inicio <= CURRENT_DATE AND nomquincena.fec_fin >= CURRENT_DATE),
											crea,
											CURRENT_DATE
								);RETURN TRUE;
END;
$$;


ALTER FUNCTION sgnom.insertar_incidencia_por_empleado(incidenciaid integer, cantidad integer, actividad character varying, comentarios text, reporta integer, crea integer, monto numeric, fechas character varying) OWNER TO suite;

--
-- TOC entry 507 (class 1255 OID 98996)
-- Name: obtener_correos(); Type: FUNCTION; Schema: sgnom; Owner: postgres
--

CREATE FUNCTION sgnom.obtener_correos() RETURNS TABLE(nomempleado integer, correo character varying)
    LANGUAGE plpgsql
    AS $$
BEGIN
RETURN QUERY
	SELECT nomemp.cod_empleadoid nomempleado,
			rhemp.des_correo correo
	FROM sgnom.tsgnomempleados nomemp
		JOIN sgrh.tsgrhempleados rhemp
		ON nomemp.cod_empleado_fk = rhemp.cod_empleado
	ORDER BY 1;
END;
$$;


ALTER FUNCTION sgnom.obtener_correos() OWNER TO postgres;

--
-- TOC entry 529 (class 1255 OID 97181)
-- Name: totalimpcab(integer); Type: FUNCTION; Schema: sgnom; Owner: suite
--

CREATE FUNCTION sgnom.totalimpcab(id integer) RETURNS boolean
    LANGUAGE plpgsql
    AS $$
DECLARE
 total numeric(10,2);
 BEGIN
   SELECT (imp_totpercepcion - imp_totdeduccion)
   INTO total
   FROM sgnom.tsgnomcabecera
   WHERE cod_cabeceraid = id;
  UPDATE sgnom.tsgnomcabecera SET imp_totalemp=total
   WHERE cod_cabeceraid = id;
RETURN true;
END;
$$;


ALTER FUNCTION sgnom.totalimpcab(id integer) OWNER TO suite;

--
-- TOC entry 502 (class 1255 OID 97182)
-- Name: validaraltas(); Type: FUNCTION; Schema: sgnom; Owner: suite
--

CREATE FUNCTION sgnom.validaraltas() RETURNS TABLE(cod_empleadoid integer, nom_empleado text, cod_rfc character varying, cod_curp character varying, des_nbarea character varying, des_puesto character varying, validar boolean)
    LANGUAGE plpgsql COST 10
    AS $$
BEGIN 
RETURN QUERY 
SELECT nomempleados.cod_empleadoid,
    (((rhempleados.des_nombre::text || ' '::text) || rhempleados.des_apepaterno::text) || ' '::text) || rhempleados.des_apematerno::text AS nom_empleado,
    rhempleados.cod_rfc,
	rhempleados.cod_curp,
    rharea.des_nbarea,
    rhpuestos.des_puesto,
	nomempleados.des_validacion
   FROM sgnom.tsgnomempleados nomempleados
     JOIN sgrh.tsgrhempleados rhempleados ON nomempleados.cod_empleado_fk = rhempleados.cod_empleado
     JOIN sgrh.tsgrhareas rharea ON rhempleados.cod_area = rharea.cod_area
     JOIN sgrh.tsgrhpuestos rhpuestos ON rhempleados.cod_puesto = rhpuestos.cod_puesto 
 WHERE nomempleados.bol_estatus = true
 AND nomempleados.des_validacion is null
  GROUP BY nomempleados.cod_empleadoid, ((((rhempleados.des_nombre::text || ' '::text) || rhempleados.des_apepaterno::text) || ' '::text) || rhempleados.des_apematerno::text), rhempleados.cod_rfc, 
  rhempleados.cod_curp,rharea.des_nbarea, rhpuestos.des_puesto
  ORDER BY nomempleados.cod_empleadoid;
  END;
$$;


ALTER FUNCTION sgnom.validaraltas() OWNER TO suite;

--
-- TOC entry 530 (class 1255 OID 97183)
-- Name: validarbajas(); Type: FUNCTION; Schema: sgnom; Owner: suite
--

CREATE FUNCTION sgnom.validarbajas() RETURNS TABLE(cod_empleadoid integer, nom_empleado text, cod_rfc character varying, cod_curp character varying, des_nbarea character varying, des_puesto character varying, validar boolean)
    LANGUAGE plpgsql COST 10
    AS $$
BEGIN 
RETURN QUERY 
 SELECT nomempleados.cod_empleadoid,
    (((rhempleados.des_nombre::text || ' '::text) || rhempleados.des_apepaterno::text) || ' '::text) || rhempleados.des_apematerno::text AS nom_empleado,
    rhempleados.cod_rfc,
	rhempleados.cod_curp,
    rharea.des_nbarea,
    rhpuestos.des_puesto,
	nomempleados.des_validacion
   FROM sgnom.tsgnomempleados nomempleados
     JOIN sgrh.tsgrhempleados rhempleados ON nomempleados.cod_empleado_fk = rhempleados.cod_empleado
     JOIN sgrh.tsgrhareas rharea ON rhempleados.cod_area = rharea.cod_area
     JOIN sgrh.tsgrhpuestos rhpuestos ON rhempleados.cod_puesto = rhpuestos.cod_puesto
  WHERE nomempleados.bol_estatus = false
  AND nomempleados.des_validacion is null
  GROUP BY nomempleados.cod_empleadoid, ((((rhempleados.des_nombre::text || ' '::text) || rhempleados.des_apepaterno::text) || ' '::text) || rhempleados.des_apematerno::text), rhempleados.cod_rfc, rhempleados.cod_curp,rharea.des_nbarea, rhpuestos.des_puesto
  ORDER BY nomempleados.cod_empleadoid;
END;
$$;


ALTER FUNCTION sgnom.validarbajas() OWNER TO suite;

--
-- TOC entry 531 (class 1255 OID 97184)
-- Name: verinformaciondepersonal(integer); Type: FUNCTION; Schema: sgnom; Owner: suite
--

CREATE FUNCTION sgnom.verinformaciondepersonal(empleado integer) RETURNS TABLE(cod_empleado integer, fec_ingreso date, fec_modificacion date, des_nombre character varying, des_apepaterno character varying, des_apematerno character varying, des_nbarea character varying, des_puesto character varying, des_rol character varying, cod_rfc character varying, cod_curp character varying, cod_nss character varying, fec_nacimiento date, des_direccion character varying, des_correo character varying, correo_personal character varying, cod_tipoaguinaldo character, imp_aguinaldo numeric, quincena integer)
    LANGUAGE plpgsql
    AS $$
BEGIN 
RETURN QUERY 
select nomEmpleados.cod_empleadoid,
                nomEmpleados.fec_ingreso,
				nomEmpleados.aud_fecmodificacion,
                rhEmpleados.des_nombre, 
                rhEmpleados.des_apepaterno,
                rhEmpleados.des_apematerno,
                rhArea.des_nbarea,
                rhPuestos.des_puesto,
				rhRoles.des_nbrol,
                rhEmpleados.cod_rfc, 
                rhEmpleados.cod_curp, 
                rhEmpleados.cod_nss,  
                rhEmpleados.fec_nacimiento,
                rhEmpleados.des_direccion, 
                rhEmpleados.des_correo,
				rhEmpleados.des_correopersonal,
    (select nomAguinaldo.cod_tipoaguinaldo from sgnom.tsgnomaguinaldo as nomAguinaldo
        join sgnom.tsgnomempquincena nomEmpQuincena on nomAguinaldo.cod_empquincenaid_fk=nomEmpQuincena.cod_empquincenaid
        where nomEmpQuincena.cod_empleadoid_fk=(empleado)),
    (select nomAguinaldo.imp_aguinaldo from sgnom.tsgnomaguinaldo as nomAguinaldo
        join sgnom.tsgnomempquincena nomEmpQuincena on nomAguinaldo.cod_empquincenaid_fk=nomEmpQuincena.cod_empquincenaid
        where nomEmpQuincena.cod_empleadoid_fk=(empleado)),
    (SELECT nomquincena.cod_quincenaid 
            FROM sgnom.tsgnomquincena nomquincena
            WHERE nomquincena.fec_inicio < CURRENT_DATE AND nomquincena.fec_fin > CURRENT_DATE) AS "Quincena actual"
from sgnom.tsgnomempleados as nomEmpleados
    join sgrh.tsgrhempleados rhEmpleados on nomEmpleados.cod_empleado_fk = rhEmpleados.cod_empleado
    join sgrh.tsgrhareas rhArea 
        using (cod_area)
    join sgrh.tsgrhpuestos rhPuestos
        using (cod_puesto)
	join sgrh.tsgrhroles rhRoles
		using (cod_rol)										
where nomEmpleados.cod_empleadoid=(empleado)
group by       nomEmpleados.cod_empleadoid,
				nomEmpleados.aud_fecmodificacion,								
				rhEmpleados.fec_ingreso,
                rhEmpleados.des_nombre, 
                rhEmpleados.des_apepaterno,
                rhEmpleados.des_apematerno,
                rhArea.des_nbarea,
                rhPuestos.des_puesto,
                rhRoles.des_nbrol,
                rhEmpleados.cod_rfc, 
                rhEmpleados.cod_curp, 
                rhEmpleados.cod_nss, 
                rhEmpleados.cod_diasvacaciones, 
                rhEmpleados.fec_nacimiento,
                rhEmpleados.des_direccion, 
                rhEmpleados.des_correo,
		       rhEmpleados.des_correopersonal;											
END;
$$;


ALTER FUNCTION sgnom.verinformaciondepersonal(empleado integer) OWNER TO suite;

--
-- TOC entry 358 (class 1259 OID 97207)
-- Name: seq_cabecera; Type: SEQUENCE; Schema: sgnom; Owner: suite
--

CREATE SEQUENCE sgnom.seq_cabecera
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgnom.seq_cabecera OWNER TO suite;

--
-- TOC entry 359 (class 1259 OID 97209)
-- Name: seq_cncptoquinc; Type: SEQUENCE; Schema: sgnom; Owner: suite
--

CREATE SEQUENCE sgnom.seq_cncptoquinc
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgnom.seq_cncptoquinc OWNER TO suite;

--
-- TOC entry 360 (class 1259 OID 97211)
-- Name: seq_confpago; Type: SEQUENCE; Schema: sgnom; Owner: suite
--

CREATE SEQUENCE sgnom.seq_confpago
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgnom.seq_confpago OWNER TO suite;

--
-- TOC entry 361 (class 1259 OID 97213)
-- Name: seq_empquincena; Type: SEQUENCE; Schema: sgnom; Owner: suite
--

CREATE SEQUENCE sgnom.seq_empquincena
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgnom.seq_empquincena OWNER TO suite;

--
-- TOC entry 362 (class 1259 OID 97215)
-- Name: seq_incidencia; Type: SEQUENCE; Schema: sgnom; Owner: suite
--

CREATE SEQUENCE sgnom.seq_incidencia
    START WITH 60
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgnom.seq_incidencia OWNER TO suite;

--
-- TOC entry 455 (class 1259 OID 98892)
-- Name: seq_nom_imss; Type: SEQUENCE; Schema: sgnom; Owner: suite
--

CREATE SEQUENCE sgnom.seq_nom_imss
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgnom.seq_nom_imss OWNER TO suite;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 363 (class 1259 OID 97217)
-- Name: tsgnomaguinaldo; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnomaguinaldo (
    cod_aguinaldoid integer NOT NULL,
    imp_aguinaldo numeric(10,2) NOT NULL,
    cod_tipoaguinaldo character(1) NOT NULL,
    cod_empquincenaid_fk integer NOT NULL,
    bol_estatus boolean NOT NULL,
    xml_desgloce xml
);


ALTER TABLE sgnom.tsgnomaguinaldo OWNER TO suite;

--
-- TOC entry 3810 (class 0 OID 0)
-- Dependencies: 363
-- Name: TABLE tsgnomaguinaldo; Type: COMMENT; Schema: sgnom; Owner: suite
--

COMMENT ON TABLE sgnom.tsgnomaguinaldo IS 'cod_tipoaguinaldo

(

i = imss,

h = honorarios

)';


--
-- TOC entry 364 (class 1259 OID 97223)
-- Name: tsgnomargumento; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnomargumento (
    cod_argumentoid integer NOT NULL,
    cod_nbargumento character varying(30) NOT NULL,
    cod_clavearg character varying(5) NOT NULL,
    imp_valorconst numeric(10,2),
    des_funcionbd character varying(60),
    cod_tipoargumento character(1),
    bol_estatus boolean NOT NULL,
    txt_descripcion text NOT NULL,
    aud_codcreadopor integer NOT NULL,
    aud_codmodificadopor integer,
    aud_feccreacion date NOT NULL,
    aud_fecmodificacion date
);


ALTER TABLE sgnom.tsgnomargumento OWNER TO suite;

--
-- TOC entry 365 (class 1259 OID 97229)
-- Name: tsgnombitacora; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnombitacora (
    cod_bitacoraid integer NOT NULL,
    xml_bitacora xml NOT NULL,
    cod_tablaid_fk integer NOT NULL
);


ALTER TABLE sgnom.tsgnombitacora OWNER TO suite;

--
-- TOC entry 366 (class 1259 OID 97235)
-- Name: tsgnomcabecera; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnomcabecera (
    cod_cabeceraid integer DEFAULT nextval('sgnom.seq_cabecera'::regclass) NOT NULL,
    cod_nbnomina character varying(40) NOT NULL,
    fec_creacion date NOT NULL,
    fec_ejecucion date,
    fec_cierre date,
    imp_totpercepcion numeric(10,2) NOT NULL,
    imp_totdeduccion numeric(10,2) NOT NULL,
    imp_totalemp numeric(10,2) NOT NULL,
    cod_quincenaid_fk integer NOT NULL,
    cod_tiponominaid_fk integer NOT NULL,
    cod_estatusnomid_fk integer NOT NULL,
    cnu_totalemp integer,
    aud_codcreadopor integer NOT NULL,
    aud_codmodificadopor integer,
    aud_feccreacion date NOT NULL,
    aud_fecmodificacion date
);


ALTER TABLE sgnom.tsgnomcabecera OWNER TO suite;

--
-- TOC entry 367 (class 1259 OID 97239)
-- Name: tsgnomcabeceraht; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnomcabeceraht (
    cod_cabeceraid integer NOT NULL,
    cod_nbnomina character varying(40) NOT NULL,
    fec_creacion date NOT NULL,
    fec_ejecucion date,
    fec_cierre date,
    imp_totpercepcion numeric(10,2),
    imp_totdeduccion numeric(10,2),
    imp_totalemp numeric(10,2),
    cod_quincenaid_fk integer NOT NULL,
    cod_tiponominaid_fk integer NOT NULL,
    cod_estatusnomid_fk integer NOT NULL,
    cnu_totalemp integer,
    aud_codcreadopor integer NOT NULL,
    aud_codmodificadopor integer,
    aud_feccreacion date NOT NULL,
    aud_fecmodificacion date
);


ALTER TABLE sgnom.tsgnomcabeceraht OWNER TO suite;

--
-- TOC entry 368 (class 1259 OID 97242)
-- Name: tsgnomcalculo; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnomcalculo (
    cod_calculoid integer NOT NULL,
    cod_tpcalculo character varying(25) NOT NULL,
    bol_estatus boolean NOT NULL
);


ALTER TABLE sgnom.tsgnomcalculo OWNER TO suite;

--
-- TOC entry 3816 (class 0 OID 0)
-- Dependencies: 368
-- Name: TABLE tsgnomcalculo; Type: COMMENT; Schema: sgnom; Owner: suite
--

COMMENT ON TABLE sgnom.tsgnomcalculo IS 'bol_estatus

(

activo, inactivo

)';


--
-- TOC entry 369 (class 1259 OID 97245)
-- Name: tsgnomcatincidencia; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnomcatincidencia (
    cod_catincidenciaid integer NOT NULL,
    cod_claveincidencia character varying(5) NOT NULL,
    cod_nbincidencia character varying(20),
    cod_perfilincidencia character varying(25),
    bol_estatus boolean,
    cod_tipoincidencia character(1),
    imp_monto numeric(10,2)
);


ALTER TABLE sgnom.tsgnomcatincidencia OWNER TO suite;

--
-- TOC entry 3818 (class 0 OID 0)
-- Dependencies: 369
-- Name: TABLE tsgnomcatincidencia; Type: COMMENT; Schema: sgnom; Owner: suite
--

COMMENT ON TABLE sgnom.tsgnomcatincidencia IS 'cod_tipoincidencia

(

1 = horas

2 = dias

3 = actividad

)';


--
-- TOC entry 370 (class 1259 OID 97248)
-- Name: tsgnomclasificador; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnomclasificador (
    cod_clasificadorid integer NOT NULL,
    cod_tpclasificador character varying(20),
    bol_estatus boolean
);


ALTER TABLE sgnom.tsgnomclasificador OWNER TO suite;

--
-- TOC entry 371 (class 1259 OID 97251)
-- Name: tsgnomcncptoquinc; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnomcncptoquinc (
    cod_cncptoquincid integer DEFAULT nextval('sgnom.seq_cncptoquinc'::regclass) NOT NULL,
    cod_empquincenaid_fk integer NOT NULL,
    cod_conceptoid_fk integer NOT NULL,
    imp_concepto numeric(10,2) NOT NULL,
    imp_gravado numeric(10,2),
    imp_exento numeric(10,2),
    xml_desgloce xml
);


ALTER TABLE sgnom.tsgnomcncptoquinc OWNER TO suite;

--
-- TOC entry 372 (class 1259 OID 97258)
-- Name: tsgnomcncptoquincht; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnomcncptoquincht (
    cod_cncptoquinchtid integer NOT NULL,
    cod_empquincenaid_fk integer NOT NULL,
    cod_conceptoid_fk integer NOT NULL,
    imp_concepto numeric(10,2) NOT NULL,
    imp_gravado numeric(10,2),
    imp_exento numeric(10,2),
    xml_desgloce xml
);


ALTER TABLE sgnom.tsgnomcncptoquincht OWNER TO suite;

--
-- TOC entry 373 (class 1259 OID 97264)
-- Name: tsgnomconcepto; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnomconcepto (
    cod_conceptoid integer NOT NULL,
    cod_nbconcepto character varying(20) NOT NULL,
    cod_claveconcepto character varying(4) NOT NULL,
    cnu_prioricalculo integer NOT NULL,
    cnu_articulo integer NOT NULL,
    bol_estatus boolean NOT NULL,
    cod_formulaid_fk integer,
    cod_tipoconceptoid_fk integer NOT NULL,
    cod_calculoid_fk integer NOT NULL,
    cod_conceptosatid_fk integer NOT NULL,
    cod_frecuenciapago character varying(20) NOT NULL,
    cod_partidaprep integer NOT NULL,
    cnu_cuentacontable character varying(18) NOT NULL,
    cod_gravado character(1),
    cod_excento character(1),
    bol_aplicaisn boolean,
    bol_retroactividad boolean NOT NULL,
    cnu_topeex integer,
    cod_clasificadorid_fk integer NOT NULL,
    cod_tiponominaid_fk integer,
    aud_codcreadopor integer NOT NULL,
    aud_codmodificadopor integer,
    aud_feccreacion date NOT NULL,
    aud_fecmodificacion date,
    imp_monto double precision
);


ALTER TABLE sgnom.tsgnomconcepto OWNER TO suite;

--
-- TOC entry 3823 (class 0 OID 0)
-- Dependencies: 373
-- Name: TABLE tsgnomconcepto; Type: COMMENT; Schema: sgnom; Owner: suite
--

COMMENT ON TABLE sgnom.tsgnomconcepto IS 'bol_estatus

(

activo, inactivo

)



gravado

(

1 = dias, 

2 = porcentaje, 

3 = no aplica

)



excento

(

activo, inactivo

)





validar tope_ex';


--
-- TOC entry 374 (class 1259 OID 97267)
-- Name: tsgnomconceptosat; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnomconceptosat (
    cod_conceptosatid integer NOT NULL,
    des_conceptosat character varying(51) NOT NULL,
    des_descconcepto character varying(51) NOT NULL,
    bol_estatus boolean NOT NULL
);


ALTER TABLE sgnom.tsgnomconceptosat OWNER TO suite;

--
-- TOC entry 3825 (class 0 OID 0)
-- Dependencies: 374
-- Name: TABLE tsgnomconceptosat; Type: COMMENT; Schema: sgnom; Owner: suite
--

COMMENT ON TABLE sgnom.tsgnomconceptosat IS 'bol_estatus

(

activo, inactivo

)';


--
-- TOC entry 375 (class 1259 OID 97270)
-- Name: tsgnomconfpago; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnomconfpago (
    cod_confpagoid integer DEFAULT nextval('sgnom.seq_confpago'::regclass) NOT NULL,
    bol_pagoempleado boolean,
    bol_pagorh boolean,
    bol_pagofinanzas boolean,
    cod_empquincenaid_fk integer
);


ALTER TABLE sgnom.tsgnomconfpago OWNER TO suite;

--
-- TOC entry 3827 (class 0 OID 0)
-- Dependencies: 375
-- Name: TABLE tsgnomconfpago; Type: COMMENT; Schema: sgnom; Owner: suite
--

COMMENT ON TABLE sgnom.tsgnomconfpago IS 'bol_pagoempleado

(

confirmado, pendiente 

)



pago_rh

(

autorizado, pendiente

)



pago_fnzas

(

autorizado, pendiente

)';


--
-- TOC entry 376 (class 1259 OID 97274)
-- Name: tsgnomejercicio; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnomejercicio (
    cod_ejercicioid integer NOT NULL,
    cnu_valorejercicio integer NOT NULL,
    bol_estatus boolean NOT NULL
);


ALTER TABLE sgnom.tsgnomejercicio OWNER TO suite;

--
-- TOC entry 377 (class 1259 OID 97277)
-- Name: tsgnomempleados; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnomempleados (
    cod_empleadoid integer NOT NULL,
    fec_ingreso date NOT NULL,
    fec_salida date,
    bol_estatus boolean NOT NULL,
    cod_empleado_fk integer NOT NULL,
    imp_sueldoimss numeric(10,2),
    imp_honorarios numeric(10,2),
    imp_finiquito numeric(10,2),
    cod_tipoimss character(1),
    cod_tipohonorarios character(1),
    cod_banco character varying(50),
    cod_sucursal integer,
    cod_cuenta character varying(20),
    txt_descripcionbaja text,
    aud_codcreadopor integer NOT NULL,
    aud_feccreacion date NOT NULL,
    aud_codmodificadopor integer,
    aud_fecmodificacion date,
    cod_clabe character varying(18),
    des_validacion boolean,
    cod_validaciones "char",
    cod_bancoh character varying,
    cod_sucursalh integer,
    cod_cuentah character varying,
    cod_clabeh character varying
);


ALTER TABLE sgnom.tsgnomempleados OWNER TO suite;

--
-- TOC entry 3830 (class 0 OID 0)
-- Dependencies: 377
-- Name: TABLE tsgnomempleados; Type: COMMENT; Schema: sgnom; Owner: suite
--

COMMENT ON TABLE sgnom.tsgnomempleados IS 'cod_empleado_fk hace referencia al schema sgrh en la tabla tsgrhempleados';


--
-- TOC entry 378 (class 1259 OID 97283)
-- Name: tsgnomempquincena; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnomempquincena (
    cod_empquincenaid integer DEFAULT nextval('sgnom.seq_empquincena'::regclass) NOT NULL,
    cod_empleadoid_fk integer NOT NULL,
    cod_cabeceraid_fk integer NOT NULL,
    imp_totpercepcion numeric(10,2) NOT NULL,
    imp_totdeduccion numeric(10,2) NOT NULL,
    imp_totalemp numeric(10,2) NOT NULL,
    bol_estatusemp boolean NOT NULL
);


ALTER TABLE sgnom.tsgnomempquincena OWNER TO suite;

--
-- TOC entry 3832 (class 0 OID 0)
-- Dependencies: 378
-- Name: TABLE tsgnomempquincena; Type: COMMENT; Schema: sgnom; Owner: suite
--

COMMENT ON TABLE sgnom.tsgnomempquincena IS 'bol_estatusemp

(

activo, inactivo

)';


--
-- TOC entry 379 (class 1259 OID 97287)
-- Name: tsgnomempquincenaht; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnomempquincenaht (
    cod_empquincenahtid integer NOT NULL,
    cod_empleadoid_fk integer NOT NULL,
    cod_cabeceraid_fk integer NOT NULL,
    imp_totpercepcion numeric(10,2) NOT NULL,
    imp_totdeduccion numeric(10,2) NOT NULL,
    imp_totalemp numeric(10,2) NOT NULL,
    bol_estatusemp boolean NOT NULL
);


ALTER TABLE sgnom.tsgnomempquincenaht OWNER TO suite;

--
-- TOC entry 380 (class 1259 OID 97290)
-- Name: tsgnomestatusnom; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnomestatusnom (
    cod_estatusnomid integer NOT NULL,
    cod_estatusnomina character varying(15) NOT NULL,
    bol_estatus boolean NOT NULL
);


ALTER TABLE sgnom.tsgnomestatusnom OWNER TO suite;

--
-- TOC entry 3835 (class 0 OID 0)
-- Dependencies: 380
-- Name: TABLE tsgnomestatusnom; Type: COMMENT; Schema: sgnom; Owner: suite
--

COMMENT ON TABLE sgnom.tsgnomestatusnom IS 'cod_estatusnomid 

estatus

(

abierta,

calculada,

revision,

validada,

cerrada

)



nota: en caso de no ser validada entra en revision y posteriormente calcular nuevamente 

hasta ser validada';


--
-- TOC entry 381 (class 1259 OID 97293)
-- Name: tsgnomformula; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnomformula (
    cod_formulaid integer NOT NULL,
    des_nbformula character varying(60) NOT NULL,
    des_formula character varying(250) NOT NULL,
    bol_estatus boolean NOT NULL
);


ALTER TABLE sgnom.tsgnomformula OWNER TO suite;

--
-- TOC entry 3837 (class 0 OID 0)
-- Dependencies: 381
-- Name: TABLE tsgnomformula; Type: COMMENT; Schema: sgnom; Owner: suite
--

COMMENT ON TABLE sgnom.tsgnomformula IS 'bol_estatus

(

activo, inactivo

)';


--
-- TOC entry 382 (class 1259 OID 97296)
-- Name: tsgnomfuncion; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnomfuncion (
    cod_funcionid integer NOT NULL,
    cod_nbfuncion character varying(15) NOT NULL,
    bol_estatus boolean NOT NULL
);


ALTER TABLE sgnom.tsgnomfuncion OWNER TO suite;

--
-- TOC entry 383 (class 1259 OID 97299)
-- Name: tsgnomhisttabla; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnomhisttabla (
    cod_tablaid integer NOT NULL,
    cod_nbtabla character varying(18) NOT NULL,
    bol_estatus boolean NOT NULL
);


ALTER TABLE sgnom.tsgnomhisttabla OWNER TO suite;

--
-- TOC entry 384 (class 1259 OID 97302)
-- Name: tsgnomincidencia; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnomincidencia (
    cod_incidenciaid integer NOT NULL,
    cod_catincidenciaid_fk integer NOT NULL,
    cnu_cantidad smallint,
    des_actividad character varying(100),
    txt_comentarios text,
    cod_empreporta_fk integer,
    cod_empautoriza_fk integer,
    imp_monto numeric(10,2),
    xml_detcantidad xml,
    bol_estatus boolean,
    cod_quincenaid_fk integer NOT NULL,
    bol_validacion boolean,
    fec_validacion date,
    aud_codcreadopor integer NOT NULL,
    aud_feccreacion date NOT NULL,
    aud_codmodificadopor integer,
    aud_fecmodificacion date,
    bol_aceptacion boolean,
    bol_pago boolean
);


ALTER TABLE sgnom.tsgnomincidencia OWNER TO suite;

--
-- TOC entry 3841 (class 0 OID 0)
-- Dependencies: 384
-- Name: TABLE tsgnomincidencia; Type: COMMENT; Schema: sgnom; Owner: suite
--

COMMENT ON TABLE sgnom.tsgnomincidencia IS 'bol_estatus: (activo, inactivo) (1, 0)

validacion: (validar, denegar) (1, 0) 



';


--
-- TOC entry 3842 (class 0 OID 0)
-- Dependencies: 384
-- Name: COLUMN tsgnomincidencia.bol_validacion; Type: COMMENT; Schema: sgnom; Owner: suite
--

COMMENT ON COLUMN sgnom.tsgnomincidencia.bol_validacion IS 'Validacion de la incidencia por parte de RH (pasa a finanzas)';


--
-- TOC entry 3843 (class 0 OID 0)
-- Dependencies: 384
-- Name: COLUMN tsgnomincidencia.bol_aceptacion; Type: COMMENT; Schema: sgnom; Owner: suite
--

COMMENT ON COLUMN sgnom.tsgnomincidencia.bol_aceptacion IS 'Validacion de la incidencia por parte del lider de celula (pasa a RH)';


--
-- TOC entry 385 (class 1259 OID 97308)
-- Name: tsgnommanterceros; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnommanterceros (
    cod_mantercerosid integer NOT NULL,
    cod_conceptoid_fk integer,
    imp_monto numeric(10,2),
    cod_quincenainicio_fk integer,
    cod_quincenafin_fk integer,
    cod_empleadoid_fk integer,
    cod_frecuenciapago character varying(20),
    bol_estatus boolean,
    aud_codcreadopor integer NOT NULL,
    aud_codmodificadopor integer,
    aud_fecreacion date NOT NULL,
    aud_fecmodificacion date
);


ALTER TABLE sgnom.tsgnommanterceros OWNER TO suite;

--
-- TOC entry 386 (class 1259 OID 97311)
-- Name: tsgnomnominaimss; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnomnominaimss (
    cod_nominaimssid integer NOT NULL,
    clave integer,
    nss text,
    rfc text,
    curp text,
    fecha_alta date,
    tipo_salario text,
    salario_diario numeric,
    "SDI" numeric,
    dias_trabajados numeric,
    faltas numeric,
    sueldo numeric,
    premio_asistencia numeric,
    premio_puntualidad numeric,
    total_percepciones numeric,
    total_gravable numeric,
    total_imss numeric,
    total_isr numeric,
    isr numeric,
    imss numeric,
    credito_infonavit numeric,
    subsidio_empleo numeric,
    subsidio_empleo_aplicado numeric,
    total_deducciones numeric,
    neto_pagado numeric,
    cod_cabeceraid_fk integer NOT NULL,
    vacaciones numeric,
    prima_vacacional numeric
);


ALTER TABLE sgnom.tsgnomnominaimss OWNER TO suite;

--
-- TOC entry 387 (class 1259 OID 97317)
-- Name: tsgnomquincena; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnomquincena (
    cod_quincenaid integer NOT NULL,
    des_quincena character varying(70) NOT NULL,
    fec_inicio date NOT NULL,
    fec_fin date NOT NULL,
    fec_pago date NOT NULL,
    fec_dispersion date NOT NULL,
    cnu_numquincena integer NOT NULL,
    cod_ejercicioid_fk integer NOT NULL,
    bol_estatus boolean NOT NULL
);


ALTER TABLE sgnom.tsgnomquincena OWNER TO suite;

--
-- TOC entry 388 (class 1259 OID 97320)
-- Name: tsgnomtipoconcepto; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnomtipoconcepto (
    cod_tipoconceptoid integer NOT NULL,
    cod_tipoconcepto character varying(25) NOT NULL,
    bol_estatus boolean NOT NULL
);


ALTER TABLE sgnom.tsgnomtipoconcepto OWNER TO suite;

--
-- TOC entry 3848 (class 0 OID 0)
-- Dependencies: 388
-- Name: TABLE tsgnomtipoconcepto; Type: COMMENT; Schema: sgnom; Owner: suite
--

COMMENT ON TABLE sgnom.tsgnomtipoconcepto IS 'bol_estatus

(

activo, inactivo

)';


--
-- TOC entry 389 (class 1259 OID 97323)
-- Name: tsgnomtiponomina; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnomtiponomina (
    cod_tiponominaid integer NOT NULL,
    cod_nomina character varying(30) NOT NULL,
    bol_estatus boolean NOT NULL
);


ALTER TABLE sgnom.tsgnomtiponomina OWNER TO suite;

--
-- TOC entry 3850 (class 0 OID 0)
-- Dependencies: 389
-- Name: TABLE tsgnomtiponomina; Type: COMMENT; Schema: sgnom; Owner: suite
--

COMMENT ON TABLE sgnom.tsgnomtiponomina IS 'bol_estatus (activa, inactiva)';


--
-- TOC entry 3741 (class 0 OID 97217)
-- Dependencies: 363
-- Data for Name: tsgnomaguinaldo; Type: TABLE DATA; Schema: sgnom; Owner: suite
--



--
-- TOC entry 3742 (class 0 OID 97223)
-- Dependencies: 364
-- Data for Name: tsgnomargumento; Type: TABLE DATA; Schema: sgnom; Owner: suite
--

INSERT INTO sgnom.tsgnomargumento (cod_argumentoid, cod_nbargumento, cod_clavearg, imp_valorconst, des_funcionbd, cod_tipoargumento, bol_estatus, txt_descripcion, aud_codcreadopor, aud_codmodificadopor, aud_feccreacion, aud_fecmodificacion) VALUES (1, 'SUELDOBASE', 'SB', NULL, 'fn_sueldo_base', '0', true, 'obtiene sueldo base asd', 10, 13, '2019-07-01', '2019-08-26');
INSERT INTO sgnom.tsgnomargumento (cod_argumentoid, cod_nbargumento, cod_clavearg, imp_valorconst, des_funcionbd, cod_tipoargumento, bol_estatus, txt_descripcion, aud_codcreadopor, aud_codmodificadopor, aud_feccreacion, aud_fecmodificacion) VALUES (3, 'SueldoXHora', 'SH', NULL, 'fn_sueldo_hora', '0', true, 'consulta el sueldo por hora segun su sueldo base', 10, NULL, '2019-09-20', NULL);
INSERT INTO sgnom.tsgnomargumento (cod_argumentoid, cod_nbargumento, cod_clavearg, imp_valorconst, des_funcionbd, cod_tipoargumento, bol_estatus, txt_descripcion, aud_codcreadopor, aud_codmodificadopor, aud_feccreacion, aud_fecmodificacion) VALUES (5, 'SueldoXDia', 'SD', NULL, 'fn_sueldo_dia', '0', true, 'calcula el sueldo por dia segun su sueldo base', 10, NULL, '2019-09-20', NULL);
INSERT INTO sgnom.tsgnomargumento (cod_argumentoid, cod_nbargumento, cod_clavearg, imp_valorconst, des_funcionbd, cod_tipoargumento, bol_estatus, txt_descripcion, aud_codcreadopor, aud_codmodificadopor, aud_feccreacion, aud_fecmodificacion) VALUES (6, 'Horas trabajadas', 'HT', NULL, 'fn_horas_trabajadas', '0', true, 'consulta el numero de horas registradas en la incidencia', 10, NULL, '2019-09-20', NULL);
INSERT INTO sgnom.tsgnomargumento (cod_argumentoid, cod_nbargumento, cod_clavearg, imp_valorconst, des_funcionbd, cod_tipoargumento, bol_estatus, txt_descripcion, aud_codcreadopor, aud_codmodificadopor, aud_feccreacion, aud_fecmodificacion) VALUES (7, 'Dias trabajados', 'DT', NULL, 'fn_dias_trabajados', '0', true, 'consulta el numero de dias segun la incidencia', 10, NULL, '2019-09-20', NULL);
INSERT INTO sgnom.tsgnomargumento (cod_argumentoid, cod_nbargumento, cod_clavearg, imp_valorconst, des_funcionbd, cod_tipoargumento, bol_estatus, txt_descripcion, aud_codcreadopor, aud_codmodificadopor, aud_feccreacion, aud_fecmodificacion) VALUES (8, 'con proyecto', 'CP', 100.00, NULL, '1', true, 'sueldo con proyecto', 10, NULL, '2019-09-20', NULL);
INSERT INTO sgnom.tsgnomargumento (cod_argumentoid, cod_nbargumento, cod_clavearg, imp_valorconst, des_funcionbd, cod_tipoargumento, bol_estatus, txt_descripcion, aud_codcreadopor, aud_codmodificadopor, aud_feccreacion, aud_fecmodificacion) VALUES (9, 'sin proyecto', 'SP', 50.00, NULL, '1', true, 'sueldo sin proyecto', 10, NULL, '2019-09-20', NULL);
INSERT INTO sgnom.tsgnomargumento (cod_argumentoid, cod_nbargumento, cod_clavearg, imp_valorconst, des_funcionbd, cod_tipoargumento, bol_estatus, txt_descripcion, aud_codcreadopor, aud_codmodificadopor, aud_feccreacion, aud_fecmodificacion) VALUES (10, 'sin proyecto acuerdo', 'SPA', NULL, 'fn_sueldo_porcentaje', '0', true, 'calcula el sueldo segun el porcentaje acordado si no se cuenta con proyecto', 10, NULL, '2019-09-21', NULL);
INSERT INTO sgnom.tsgnomargumento (cod_argumentoid, cod_nbargumento, cod_clavearg, imp_valorconst, des_funcionbd, cod_tipoargumento, bol_estatus, txt_descripcion, aud_codcreadopor, aud_codmodificadopor, aud_feccreacion, aud_fecmodificacion) VALUES (4, 'DIASLABORADOS', 'DL', NULL, 'fn_dias_laborados', '0', true, 'dias laborados', 13, 13, '2019-07-15', '2019-09-09');
INSERT INTO sgnom.tsgnomargumento (cod_argumentoid, cod_nbargumento, cod_clavearg, imp_valorconst, des_funcionbd, cod_tipoargumento, bol_estatus, txt_descripcion, aud_codcreadopor, aud_codmodificadopor, aud_feccreacion, aud_fecmodificacion) VALUES (2, 'DIASDELMES', 'DM', 30.00, NULL, '1', true, 'dias del mes p', 10, 13, '2019-07-01', '2019-08-26');
INSERT INTO sgnom.tsgnomargumento (cod_argumentoid, cod_nbargumento, cod_clavearg, imp_valorconst, des_funcionbd, cod_tipoargumento, bol_estatus, txt_descripcion, aud_codcreadopor, aud_codmodificadopor, aud_feccreacion, aud_fecmodificacion) VALUES (11, 'AMBROSIO', 'P001', NULL, 'fn_sueldo_base', '0', false, 'ambro', 13, 13, '2019-09-30', '2019-09-30');
INSERT INTO sgnom.tsgnomargumento (cod_argumentoid, cod_nbargumento, cod_clavearg, imp_valorconst, des_funcionbd, cod_tipoargumento, bol_estatus, txt_descripcion, aud_codcreadopor, aud_codmodificadopor, aud_feccreacion, aud_fecmodificacion) VALUES (12, 'TORRES', 'TORRE', 45.00, NULL, '1', false, 'torres', 13, NULL, '2019-09-30', NULL);
INSERT INTO sgnom.tsgnomargumento (cod_argumentoid, cod_nbargumento, cod_clavearg, imp_valorconst, des_funcionbd, cod_tipoargumento, bol_estatus, txt_descripcion, aud_codcreadopor, aud_codmodificadopor, aud_feccreacion, aud_fecmodificacion) VALUES (13, 'DGDFGDFGSDG', 'FSD', 5.00, NULL, '1', false, 'dgsdgds', 13, NULL, '2019-09-30', NULL);
INSERT INTO sgnom.tsgnomargumento (cod_argumentoid, cod_nbargumento, cod_clavearg, imp_valorconst, des_funcionbd, cod_tipoargumento, bol_estatus, txt_descripcion, aud_codcreadopor, aud_codmodificadopor, aud_feccreacion, aud_fecmodificacion) VALUES (14, 'SFSA', 'SFS', 5.00, NULL, '1', false, 'sfafas', 13, NULL, '2019-09-30', NULL);
INSERT INTO sgnom.tsgnomargumento (cod_argumentoid, cod_nbargumento, cod_clavearg, imp_valorconst, des_funcionbd, cod_tipoargumento, bol_estatus, txt_descripcion, aud_codcreadopor, aud_codmodificadopor, aud_feccreacion, aud_fecmodificacion) VALUES (15, '1', '1', 1.00, NULL, '1', false, '1', 13, NULL, '2019-09-30', NULL);
INSERT INTO sgnom.tsgnomargumento (cod_argumentoid, cod_nbargumento, cod_clavearg, imp_valorconst, des_funcionbd, cod_tipoargumento, bol_estatus, txt_descripcion, aud_codcreadopor, aud_codmodificadopor, aud_feccreacion, aud_fecmodificacion) VALUES (16, '2', '2', 2.00, NULL, '1', false, '2', 13, NULL, '2019-09-30', NULL);
INSERT INTO sgnom.tsgnomargumento (cod_argumentoid, cod_nbargumento, cod_clavearg, imp_valorconst, des_funcionbd, cod_tipoargumento, bol_estatus, txt_descripcion, aud_codcreadopor, aud_codmodificadopor, aud_feccreacion, aud_fecmodificacion) VALUES (17, '3', '3', 3.00, NULL, '1', false, '3', 13, NULL, '2019-09-30', NULL);
INSERT INTO sgnom.tsgnomargumento (cod_argumentoid, cod_nbargumento, cod_clavearg, imp_valorconst, des_funcionbd, cod_tipoargumento, bol_estatus, txt_descripcion, aud_codcreadopor, aud_codmodificadopor, aud_feccreacion, aud_fecmodificacion) VALUES (26, '', '', NULL, NULL, NULL, false, '', 13, NULL, '2019-10-28', NULL);
INSERT INTO sgnom.tsgnomargumento (cod_argumentoid, cod_nbargumento, cod_clavearg, imp_valorconst, des_funcionbd, cod_tipoargumento, bol_estatus, txt_descripcion, aud_codcreadopor, aud_codmodificadopor, aud_feccreacion, aud_fecmodificacion) VALUES (25, '', '', NULL, NULL, NULL, false, '', 13, NULL, '2019-10-28', NULL);
INSERT INTO sgnom.tsgnomargumento (cod_argumentoid, cod_nbargumento, cod_clavearg, imp_valorconst, des_funcionbd, cod_tipoargumento, bol_estatus, txt_descripcion, aud_codcreadopor, aud_codmodificadopor, aud_feccreacion, aud_fecmodificacion) VALUES (24, '', '', NULL, NULL, NULL, false, '', 13, NULL, '2019-10-28', NULL);
INSERT INTO sgnom.tsgnomargumento (cod_argumentoid, cod_nbargumento, cod_clavearg, imp_valorconst, des_funcionbd, cod_tipoargumento, bol_estatus, txt_descripcion, aud_codcreadopor, aud_codmodificadopor, aud_feccreacion, aud_fecmodificacion) VALUES (23, '', '', NULL, NULL, NULL, false, '', 13, NULL, '2019-10-28', NULL);
INSERT INTO sgnom.tsgnomargumento (cod_argumentoid, cod_nbargumento, cod_clavearg, imp_valorconst, des_funcionbd, cod_tipoargumento, bol_estatus, txt_descripcion, aud_codcreadopor, aud_codmodificadopor, aud_feccreacion, aud_fecmodificacion) VALUES (22, '', '', NULL, NULL, NULL, false, '', 13, NULL, '2019-10-28', NULL);
INSERT INTO sgnom.tsgnomargumento (cod_argumentoid, cod_nbargumento, cod_clavearg, imp_valorconst, des_funcionbd, cod_tipoargumento, bol_estatus, txt_descripcion, aud_codcreadopor, aud_codmodificadopor, aud_feccreacion, aud_fecmodificacion) VALUES (21, '', '', NULL, NULL, NULL, false, '', 13, NULL, '2019-10-28', NULL);
INSERT INTO sgnom.tsgnomargumento (cod_argumentoid, cod_nbargumento, cod_clavearg, imp_valorconst, des_funcionbd, cod_tipoargumento, bol_estatus, txt_descripcion, aud_codcreadopor, aud_codmodificadopor, aud_feccreacion, aud_fecmodificacion) VALUES (29, 'SUELDODIARIO', 'A001', 123.00, NULL, '1', true, 'sueldo que se le paga por una ', 13, NULL, '2019-10-28', NULL);
INSERT INTO sgnom.tsgnomargumento (cod_argumentoid, cod_nbargumento, cod_clavearg, imp_valorconst, des_funcionbd, cod_tipoargumento, bol_estatus, txt_descripcion, aud_codcreadopor, aud_codmodificadopor, aud_feccreacion, aud_fecmodificacion) VALUES (32, 'Q', 'A', NULL, NULL, NULL, true, '', 13, NULL, '2019-10-28', NULL);
INSERT INTO sgnom.tsgnomargumento (cod_argumentoid, cod_nbargumento, cod_clavearg, imp_valorconst, des_funcionbd, cod_tipoargumento, bol_estatus, txt_descripcion, aud_codcreadopor, aud_codmodificadopor, aud_feccreacion, aud_fecmodificacion) VALUES (20, '', '', NULL, NULL, NULL, false, '', 13, NULL, '2019-10-28', NULL);
INSERT INTO sgnom.tsgnomargumento (cod_argumentoid, cod_nbargumento, cod_clavearg, imp_valorconst, des_funcionbd, cod_tipoargumento, bol_estatus, txt_descripcion, aud_codcreadopor, aud_codmodificadopor, aud_feccreacion, aud_fecmodificacion) VALUES (37, 'PRUEBA1', 'SD2', 50.00, NULL, '1', true, 'esto es una prueba', 10, NULL, '2019-11-12', NULL);
INSERT INTO sgnom.tsgnomargumento (cod_argumentoid, cod_nbargumento, cod_clavearg, imp_valorconst, des_funcionbd, cod_tipoargumento, bol_estatus, txt_descripcion, aud_codcreadopor, aud_codmodificadopor, aud_feccreacion, aud_fecmodificacion) VALUES (36, '1', '1', NULL, NULL, NULL, false, '1', 13, NULL, '2019-10-28', NULL);
INSERT INTO sgnom.tsgnomargumento (cod_argumentoid, cod_nbargumento, cod_clavearg, imp_valorconst, des_funcionbd, cod_tipoargumento, bol_estatus, txt_descripcion, aud_codcreadopor, aud_codmodificadopor, aud_feccreacion, aud_fecmodificacion) VALUES (19, '', '', NULL, NULL, NULL, false, '', 13, NULL, '2019-10-28', NULL);
INSERT INTO sgnom.tsgnomargumento (cod_argumentoid, cod_nbargumento, cod_clavearg, imp_valorconst, des_funcionbd, cod_tipoargumento, bol_estatus, txt_descripcion, aud_codcreadopor, aud_codmodificadopor, aud_feccreacion, aud_fecmodificacion) VALUES (18, 'EDITAR', 'EDT', NULL, 'fn_sueldo_base', '0', true, 'esto lo estoy editando', 13, 10, '2019-10-28', '2019-11-12');


--
-- TOC entry 3743 (class 0 OID 97229)
-- Dependencies: 365
-- Data for Name: tsgnombitacora; Type: TABLE DATA; Schema: sgnom; Owner: suite
--



--
-- TOC entry 3744 (class 0 OID 97235)
-- Dependencies: 366
-- Data for Name: tsgnomcabecera; Type: TABLE DATA; Schema: sgnom; Owner: suite
--

INSERT INTO sgnom.tsgnomcabecera (cod_cabeceraid, cod_nbnomina, fec_creacion, fec_ejecucion, fec_cierre, imp_totpercepcion, imp_totdeduccion, imp_totalemp, cod_quincenaid_fk, cod_tiponominaid_fk, cod_estatusnomid_fk, cnu_totalemp, aud_codcreadopor, aud_codmodificadopor, aud_feccreacion, aud_fecmodificacion) VALUES (17, 'General / 2da de octubre / 2019', '2019-10-17', NULL, NULL, 63.90, 0.00, 63.90, 20, 1, 2, 5, 13, NULL, '2019-10-17', NULL);
INSERT INTO sgnom.tsgnomcabecera (cod_cabeceraid, cod_nbnomina, fec_creacion, fec_ejecucion, fec_cierre, imp_totpercepcion, imp_totdeduccion, imp_totalemp, cod_quincenaid_fk, cod_tiponominaid_fk, cod_estatusnomid_fk, cnu_totalemp, aud_codcreadopor, aud_codmodificadopor, aud_feccreacion, aud_fecmodificacion) VALUES (21, 'Aguinaldo / 1era de diciembre / 2019', '2019-11-07', NULL, NULL, 0.00, 0.00, 0.00, 23, 2, 6, 5, 13, NULL, '2019-11-07', NULL);
INSERT INTO sgnom.tsgnomcabecera (cod_cabeceraid, cod_nbnomina, fec_creacion, fec_ejecucion, fec_cierre, imp_totpercepcion, imp_totdeduccion, imp_totalemp, cod_quincenaid_fk, cod_tiponominaid_fk, cod_estatusnomid_fk, cnu_totalemp, aud_codcreadopor, aud_codmodificadopor, aud_feccreacion, aud_fecmodificacion) VALUES (23, 'mp / 1era de noviembre / 2019', '2019-11-12', NULL, NULL, 0.00, 0.00, 0.00, 21, 4, 6, 6, 10, NULL, '2019-11-12', NULL);
INSERT INTO sgnom.tsgnomcabecera (cod_cabeceraid, cod_nbnomina, fec_creacion, fec_ejecucion, fec_cierre, imp_totpercepcion, imp_totdeduccion, imp_totalemp, cod_quincenaid_fk, cod_tiponominaid_fk, cod_estatusnomid_fk, cnu_totalemp, aud_codcreadopor, aud_codmodificadopor, aud_feccreacion, aud_fecmodificacion) VALUES (22, 'Aguinaldo / 1era de diciembre / 2019', '2019-11-08', NULL, NULL, 0.00, 0.00, 0.00, 23, 2, 6, 5, 13, NULL, '2019-11-08', NULL);
INSERT INTO sgnom.tsgnomcabecera (cod_cabeceraid, cod_nbnomina, fec_creacion, fec_ejecucion, fec_cierre, imp_totpercepcion, imp_totdeduccion, imp_totalemp, cod_quincenaid_fk, cod_tiponominaid_fk, cod_estatusnomid_fk, cnu_totalemp, aud_codcreadopor, aud_codmodificadopor, aud_feccreacion, aud_fecmodificacion) VALUES (20, 'Aguinaldo / 1era de noviembre / 2019', '2019-11-07', NULL, NULL, 0.00, 0.00, 0.00, 21, 2, 6, 5, 13, NULL, '2019-11-07', NULL);
INSERT INTO sgnom.tsgnomcabecera (cod_cabeceraid, cod_nbnomina, fec_creacion, fec_ejecucion, fec_cierre, imp_totpercepcion, imp_totdeduccion, imp_totalemp, cod_quincenaid_fk, cod_tiponominaid_fk, cod_estatusnomid_fk, cnu_totalemp, aud_codcreadopor, aud_codmodificadopor, aud_feccreacion, aud_fecmodificacion) VALUES (19, 'General / 2da de noviembre / 2019', '2019-10-31', NULL, NULL, 999999.99, 0.00, 0.00, 22, 1, 6, 5, 13, NULL, '2019-10-31', NULL);
INSERT INTO sgnom.tsgnomcabecera (cod_cabeceraid, cod_nbnomina, fec_creacion, fec_ejecucion, fec_cierre, imp_totpercepcion, imp_totdeduccion, imp_totalemp, cod_quincenaid_fk, cod_tiponominaid_fk, cod_estatusnomid_fk, cnu_totalemp, aud_codcreadopor, aud_codmodificadopor, aud_feccreacion, aud_fecmodificacion) VALUES (30, 'General / 2da de noviembre / 2019', '2019-11-15', NULL, NULL, 4852.50, 0.00, 4852.50, 22, 1, 2, 8, 10, NULL, '2019-11-15', NULL);
INSERT INTO sgnom.tsgnomcabecera (cod_cabeceraid, cod_nbnomina, fec_creacion, fec_ejecucion, fec_cierre, imp_totpercepcion, imp_totdeduccion, imp_totalemp, cod_quincenaid_fk, cod_tiponominaid_fk, cod_estatusnomid_fk, cnu_totalemp, aud_codcreadopor, aud_codmodificadopor, aud_feccreacion, aud_fecmodificacion) VALUES (18, 'General / 1era de noviembre / 2019', '2019-10-21', NULL, NULL, 0.00, 999999.99, 999999.99, 21, 1, 1, 5, 13, NULL, '2019-10-21', NULL);
INSERT INTO sgnom.tsgnomcabecera (cod_cabeceraid, cod_nbnomina, fec_creacion, fec_ejecucion, fec_cierre, imp_totpercepcion, imp_totdeduccion, imp_totalemp, cod_quincenaid_fk, cod_tiponominaid_fk, cod_estatusnomid_fk, cnu_totalemp, aud_codcreadopor, aud_codmodificadopor, aud_feccreacion, aud_fecmodificacion) VALUES (32, 'test / 2da de noviembre / 2019', '2019-11-18', NULL, NULL, 0.00, 0.00, 0.00, 22, 3, 6, 8, 10, NULL, '2019-11-18', NULL);
INSERT INTO sgnom.tsgnomcabecera (cod_cabeceraid, cod_nbnomina, fec_creacion, fec_ejecucion, fec_cierre, imp_totpercepcion, imp_totdeduccion, imp_totalemp, cod_quincenaid_fk, cod_tiponominaid_fk, cod_estatusnomid_fk, cnu_totalemp, aud_codcreadopor, aud_codmodificadopor, aud_feccreacion, aud_fecmodificacion) VALUES (34, 'mp / 2da de noviembre / 2019', '2019-11-18', NULL, NULL, 0.00, 0.00, 0.00, 22, 4, 6, 8, 10, NULL, '2019-11-18', NULL);
INSERT INTO sgnom.tsgnomcabecera (cod_cabeceraid, cod_nbnomina, fec_creacion, fec_ejecucion, fec_cierre, imp_totpercepcion, imp_totdeduccion, imp_totalemp, cod_quincenaid_fk, cod_tiponominaid_fk, cod_estatusnomid_fk, cnu_totalemp, aud_codcreadopor, aud_codmodificadopor, aud_feccreacion, aud_fecmodificacion) VALUES (33, 'test / 2da de noviembre / 2019', '2019-11-18', NULL, NULL, 0.00, 0.00, 0.00, 22, 3, 6, 8, 10, NULL, '2019-11-18', NULL);
INSERT INTO sgnom.tsgnomcabecera (cod_cabeceraid, cod_nbnomina, fec_creacion, fec_ejecucion, fec_cierre, imp_totpercepcion, imp_totdeduccion, imp_totalemp, cod_quincenaid_fk, cod_tiponominaid_fk, cod_estatusnomid_fk, cnu_totalemp, aud_codcreadopor, aud_codmodificadopor, aud_feccreacion, aud_fecmodificacion) VALUES (31, 'Aguinaldo / 2da de noviembre / 2019', '2019-11-18', NULL, NULL, 0.00, 0.00, 0.00, 22, 2, 6, 8, 10, NULL, '2019-11-18', NULL);
INSERT INTO sgnom.tsgnomcabecera (cod_cabeceraid, cod_nbnomina, fec_creacion, fec_ejecucion, fec_cierre, imp_totpercepcion, imp_totdeduccion, imp_totalemp, cod_quincenaid_fk, cod_tiponominaid_fk, cod_estatusnomid_fk, cnu_totalemp, aud_codcreadopor, aud_codmodificadopor, aud_feccreacion, aud_fecmodificacion) VALUES (35, 'Aguinaldo / 2da de noviembre / 2019', '2019-11-18', NULL, NULL, 0.00, 0.00, 0.00, 22, 2, 6, 8, 10, NULL, '2019-11-18', NULL);
INSERT INTO sgnom.tsgnomcabecera (cod_cabeceraid, cod_nbnomina, fec_creacion, fec_ejecucion, fec_cierre, imp_totpercepcion, imp_totdeduccion, imp_totalemp, cod_quincenaid_fk, cod_tiponominaid_fk, cod_estatusnomid_fk, cnu_totalemp, aud_codcreadopor, aud_codmodificadopor, aud_feccreacion, aud_fecmodificacion) VALUES (36, 'Aguinaldo / 2da de noviembre / 2019', '2019-11-18', NULL, NULL, 0.00, 0.00, 0.00, 22, 2, 6, 8, 10, NULL, '2019-11-18', NULL);
INSERT INTO sgnom.tsgnomcabecera (cod_cabeceraid, cod_nbnomina, fec_creacion, fec_ejecucion, fec_cierre, imp_totpercepcion, imp_totdeduccion, imp_totalemp, cod_quincenaid_fk, cod_tiponominaid_fk, cod_estatusnomid_fk, cnu_totalemp, aud_codcreadopor, aud_codmodificadopor, aud_feccreacion, aud_fecmodificacion) VALUES (38, 'test / 2da de noviembre / 2019', '2019-11-18', NULL, NULL, 0.00, 0.00, 0.00, 22, 3, 6, 8, 10, NULL, '2019-11-18', NULL);
INSERT INTO sgnom.tsgnomcabecera (cod_cabeceraid, cod_nbnomina, fec_creacion, fec_ejecucion, fec_cierre, imp_totpercepcion, imp_totdeduccion, imp_totalemp, cod_quincenaid_fk, cod_tiponominaid_fk, cod_estatusnomid_fk, cnu_totalemp, aud_codcreadopor, aud_codmodificadopor, aud_feccreacion, aud_fecmodificacion) VALUES (39, 'test / 2da de noviembre / 2019', '2019-11-18', NULL, NULL, 0.00, 0.00, 0.00, 22, 3, 1, 8, 10, NULL, '2019-11-18', NULL);
INSERT INTO sgnom.tsgnomcabecera (cod_cabeceraid, cod_nbnomina, fec_creacion, fec_ejecucion, fec_cierre, imp_totpercepcion, imp_totdeduccion, imp_totalemp, cod_quincenaid_fk, cod_tiponominaid_fk, cod_estatusnomid_fk, cnu_totalemp, aud_codcreadopor, aud_codmodificadopor, aud_feccreacion, aud_fecmodificacion) VALUES (37, 'Aguinaldo / 2da de noviembre / 2019', '2019-11-18', NULL, NULL, 4852.50, 0.00, 4852.50, 22, 2, 2, 8, 10, NULL, '2019-11-18', NULL);


--
-- TOC entry 3745 (class 0 OID 97239)
-- Dependencies: 367
-- Data for Name: tsgnomcabeceraht; Type: TABLE DATA; Schema: sgnom; Owner: suite
--



--
-- TOC entry 3746 (class 0 OID 97242)
-- Dependencies: 368
-- Data for Name: tsgnomcalculo; Type: TABLE DATA; Schema: sgnom; Owner: suite
--

INSERT INTO sgnom.tsgnomcalculo (cod_calculoid, cod_tpcalculo, bol_estatus) VALUES (1, 'Importe', true);
INSERT INTO sgnom.tsgnomcalculo (cod_calculoid, cod_tpcalculo, bol_estatus) VALUES (2, 'Calculo', true);


--
-- TOC entry 3747 (class 0 OID 97245)
-- Dependencies: 369
-- Data for Name: tsgnomcatincidencia; Type: TABLE DATA; Schema: sgnom; Owner: suite
--

INSERT INTO sgnom.tsgnomcatincidencia (cod_catincidenciaid, cod_claveincidencia, cod_nbincidencia, cod_perfilincidencia, bol_estatus, cod_tipoincidencia, imp_monto) VALUES (13, 'PRUE', 'panda 21', 'ANALISTA', false, '2', 34.00);
INSERT INTO sgnom.tsgnomcatincidencia (cod_catincidenciaid, cod_claveincidencia, cod_nbincidencia, cod_perfilincidencia, bol_estatus, cod_tipoincidencia, imp_monto) VALUES (4, 'M', 'M', 'M', false, '1', 1.00);
INSERT INTO sgnom.tsgnomcatincidencia (cod_catincidenciaid, cod_claveincidencia, cod_nbincidencia, cod_perfilincidencia, bol_estatus, cod_tipoincidencia, imp_monto) VALUES (1, 'HE', 'horas extra 2', 'horas extra 2', true, '1', 1000.00);
INSERT INTO sgnom.tsgnomcatincidencia (cod_catincidenciaid, cod_claveincidencia, cod_nbincidencia, cod_perfilincidencia, bol_estatus, cod_tipoincidencia, imp_monto) VALUES (3, 'PAND', 'PANDA', 'PANDA', false, '1', 121.00);
INSERT INTO sgnom.tsgnomcatincidencia (cod_catincidenciaid, cod_claveincidencia, cod_nbincidencia, cod_perfilincidencia, bol_estatus, cod_tipoincidencia, imp_monto) VALUES (12, 'B001', 'BONIFICACION CLASE', 'ENCARGADO', true, '2', 789.00);
INSERT INTO sgnom.tsgnomcatincidencia (cod_catincidenciaid, cod_claveincidencia, cod_nbincidencia, cod_perfilincidencia, bol_estatus, cod_tipoincidencia, imp_monto) VALUES (2, 'JAJA', 'jajajajajaajajaj', 'fdv', false, '1', 3.00);
INSERT INTO sgnom.tsgnomcatincidencia (cod_catincidenciaid, cod_claveincidencia, cod_nbincidencia, cod_perfilincidencia, bol_estatus, cod_tipoincidencia, imp_monto) VALUES (5, 'FGGD', 'SDFW', 'Z', false, '1', 1.00);
INSERT INTO sgnom.tsgnomcatincidencia (cod_catincidenciaid, cod_claveincidencia, cod_nbincidencia, cod_perfilincidencia, bol_estatus, cod_tipoincidencia, imp_monto) VALUES (16, 'DNSF', 'assddd', 'qdq', false, '2', 12.00);
INSERT INTO sgnom.tsgnomcatincidencia (cod_catincidenciaid, cod_claveincidencia, cod_nbincidencia, cod_perfilincidencia, bol_estatus, cod_tipoincidencia, imp_monto) VALUES (17, 'DNSF', 'assddd', 'qdq', false, '2', 12.00);
INSERT INTO sgnom.tsgnomcatincidencia (cod_catincidenciaid, cod_claveincidencia, cod_nbincidencia, cod_perfilincidencia, bol_estatus, cod_tipoincidencia, imp_monto) VALUES (19, 'DNSF', 'assddd', 'qdq', false, '2', 12.00);
INSERT INTO sgnom.tsgnomcatincidencia (cod_catincidenciaid, cod_claveincidencia, cod_nbincidencia, cod_perfilincidencia, bol_estatus, cod_tipoincidencia, imp_monto) VALUES (21, 'DNSF', 'assddd', 'qdq', false, '2', 12.00);
INSERT INTO sgnom.tsgnomcatincidencia (cod_catincidenciaid, cod_claveincidencia, cod_nbincidencia, cod_perfilincidencia, bol_estatus, cod_tipoincidencia, imp_monto) VALUES (14, 'DNSF', 'assddd', 'qdq', false, '2', 12.00);
INSERT INTO sgnom.tsgnomcatincidencia (cod_catincidenciaid, cod_claveincidencia, cod_nbincidencia, cod_perfilincidencia, bol_estatus, cod_tipoincidencia, imp_monto) VALUES (20, 'DNSF', 'assddd', 'qdq', false, '2', 12.00);
INSERT INTO sgnom.tsgnomcatincidencia (cod_catincidenciaid, cod_claveincidencia, cod_nbincidencia, cod_perfilincidencia, bol_estatus, cod_tipoincidencia, imp_monto) VALUES (18, 'DNSF', 'assddd', 'qdq', false, '2', 12.00);
INSERT INTO sgnom.tsgnomcatincidencia (cod_catincidenciaid, cod_claveincidencia, cod_nbincidencia, cod_perfilincidencia, bol_estatus, cod_tipoincidencia, imp_monto) VALUES (15, 'DNSF', 'assddd', 'qdq', false, '2', 12.00);
INSERT INTO sgnom.tsgnomcatincidencia (cod_catincidenciaid, cod_claveincidencia, cod_nbincidencia, cod_perfilincidencia, bol_estatus, cod_tipoincidencia, imp_monto) VALUES (6, 'HE2', 'horas extra 2', 'horas extra 2', false, '1', 1000.00);
INSERT INTO sgnom.tsgnomcatincidencia (cod_catincidenciaid, cod_claveincidencia, cod_nbincidencia, cod_perfilincidencia, bol_estatus, cod_tipoincidencia, imp_monto) VALUES (8, 'C003', 'CELULA 3X3', 'ENCARGADO', false, '2', 45.00);
INSERT INTO sgnom.tsgnomcatincidencia (cod_catincidenciaid, cod_claveincidencia, cod_nbincidencia, cod_perfilincidencia, bol_estatus, cod_tipoincidencia, imp_monto) VALUES (9, 'H001', 'HORAS EXTRA', 'CONSULTOR', false, '1', 445.00);
INSERT INTO sgnom.tsgnomcatincidencia (cod_catincidenciaid, cod_claveincidencia, cod_nbincidencia, cod_perfilincidencia, bol_estatus, cod_tipoincidencia, imp_monto) VALUES (7, 'R002', 'REUNION CON CLIENTE', 'ANALISTA', false, '1', 12.00);
INSERT INTO sgnom.tsgnomcatincidencia (cod_catincidenciaid, cod_claveincidencia, cod_nbincidencia, cod_perfilincidencia, bol_estatus, cod_tipoincidencia, imp_monto) VALUES (22, 'GFD', 'gdfgdfg', 'dfg', true, '1', 54.00);
INSERT INTO sgnom.tsgnomcatincidencia (cod_catincidenciaid, cod_claveincidencia, cod_nbincidencia, cod_perfilincidencia, bol_estatus, cod_tipoincidencia, imp_monto) VALUES (23, 'NJMK', 'fghjg', 'iiojijoijoijo', true, '1', 456.00);
INSERT INTO sgnom.tsgnomcatincidencia (cod_catincidenciaid, cod_claveincidencia, cod_nbincidencia, cod_perfilincidencia, bol_estatus, cod_tipoincidencia, imp_monto) VALUES (24, '1', '1', '1', true, '1', 1.00);
INSERT INTO sgnom.tsgnomcatincidencia (cod_catincidenciaid, cod_claveincidencia, cod_nbincidencia, cod_perfilincidencia, bol_estatus, cod_tipoincidencia, imp_monto) VALUES (25, 'DSD', 'dsas', 'knsd', true, '1', 5.00);
INSERT INTO sgnom.tsgnomcatincidencia (cod_catincidenciaid, cod_claveincidencia, cod_nbincidencia, cod_perfilincidencia, bol_estatus, cod_tipoincidencia, imp_monto) VALUES (27, '', '', '', false, NULL, NULL);
INSERT INTO sgnom.tsgnomcatincidencia (cod_catincidenciaid, cod_claveincidencia, cod_nbincidencia, cod_perfilincidencia, bol_estatus, cod_tipoincidencia, imp_monto) VALUES (10, 'P005', 'PRESENTAR PROYECTO', 'CONSULTOR', true, '3', 8.00);
INSERT INTO sgnom.tsgnomcatincidencia (cod_catincidenciaid, cod_claveincidencia, cod_nbincidencia, cod_perfilincidencia, bol_estatus, cod_tipoincidencia, imp_monto) VALUES (11, 'P006', 'PROPUESTA CLIENTE', 'CONSULTOR', true, '3', 9.00);
INSERT INTO sgnom.tsgnomcatincidencia (cod_catincidenciaid, cod_claveincidencia, cod_nbincidencia, cod_perfilincidencia, bol_estatus, cod_tipoincidencia, imp_monto) VALUES (32, '5', '5', '5', true, '1', 5.50);
INSERT INTO sgnom.tsgnomcatincidencia (cod_catincidenciaid, cod_claveincidencia, cod_nbincidencia, cod_perfilincidencia, bol_estatus, cod_tipoincidencia, imp_monto) VALUES (26, '', '', '', false, NULL, NULL);
INSERT INTO sgnom.tsgnomcatincidencia (cod_catincidenciaid, cod_claveincidencia, cod_nbincidencia, cod_perfilincidencia, bol_estatus, cod_tipoincidencia, imp_monto) VALUES (29, '12', '1', '', false, NULL, NULL);
INSERT INTO sgnom.tsgnomcatincidencia (cod_catincidenciaid, cod_claveincidencia, cod_nbincidencia, cod_perfilincidencia, bol_estatus, cod_tipoincidencia, imp_monto) VALUES (30, '7', '7', '', false, NULL, 7.00);


--
-- TOC entry 3748 (class 0 OID 97248)
-- Dependencies: 370
-- Data for Name: tsgnomclasificador; Type: TABLE DATA; Schema: sgnom; Owner: suite
--

INSERT INTO sgnom.tsgnomclasificador (cod_clasificadorid, cod_tpclasificador, bol_estatus) VALUES (1, 'clas1', true);
INSERT INTO sgnom.tsgnomclasificador (cod_clasificadorid, cod_tpclasificador, bol_estatus) VALUES (2, 'clas2', true);


--
-- TOC entry 3749 (class 0 OID 97251)
-- Dependencies: 371
-- Data for Name: tsgnomcncptoquinc; Type: TABLE DATA; Schema: sgnom; Owner: suite
--

INSERT INTO sgnom.tsgnomcncptoquinc (cod_cncptoquincid, cod_empquincenaid_fk, cod_conceptoid_fk, imp_concepto, imp_gravado, imp_exento, xml_desgloce) VALUES (1, 64, 1, 31.95, 0.00, 0.00, '                
                <mx.org.ift.serp.conceptoBindingXml.DetallesConcepto>                  
                <claveConcepto>QW1</claveConcepto>                  
                <nombreConcepto>QW1</nombreConcepto><formula>:(:SUELDOBASE:/:con proyecto:):*:DIASLABORADOS</formula>                  
                <argumentos><mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>SUELDOBASE</nombre>                            
                    <valor>213.00</valor> <descripcion>SUELDOBASE</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento><mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>con proyecto</nombre>                            
                    <valor>100.00</valor> <descripcion>con proyecto</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento><mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>DIASLABORADOS</nombre>                            
                    <valor>15.00</valor> <descripcion>DIASLABORADOS</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento></argumentos><importe>31.95</importe> </mx.org.ift.serp.conceptoBindingXml.DetallesConcepto>  ');
INSERT INTO sgnom.tsgnomcncptoquinc (cod_cncptoquincid, cod_empquincenaid_fk, cod_conceptoid_fk, imp_concepto, imp_gravado, imp_exento, xml_desgloce) VALUES (2, 64, 2, 31.95, 0.00, 0.00, '                
                <mx.org.ift.serp.conceptoBindingXml.DetallesConcepto>                  
                <claveConcepto>QW2</claveConcepto>                  
                <nombreConcepto>QW2</nombreConcepto><formula>:(:SUELDOBASE:/:con proyecto:):*:DIASLABORADOS</formula>                  
                <argumentos><mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>SUELDOBASE</nombre>                            
                    <valor>213.00</valor> <descripcion>SUELDOBASE</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento><mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>con proyecto</nombre>                            
                    <valor>100.00</valor> <descripcion>con proyecto</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento><mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>DIASLABORADOS</nombre>                            
                    <valor>15.00</valor> <descripcion>DIASLABORADOS</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento></argumentos><importe>31.95</importe> </mx.org.ift.serp.conceptoBindingXml.DetallesConcepto>  ');
INSERT INTO sgnom.tsgnomcncptoquinc (cod_cncptoquincid, cod_empquincenaid_fk, cod_conceptoid_fk, imp_concepto, imp_gravado, imp_exento, xml_desgloce) VALUES (3, 64, 3, 31.95, 0.00, 0.00, '                
                <mx.org.ift.serp.conceptoBindingXml.DetallesConcepto>                  
                <claveConcepto>QW3</claveConcepto>                  
                <nombreConcepto>QW3</nombreConcepto><formula>:(:SUELDOBASE:/:con proyecto:):*:DIASLABORADOS</formula>                  
                <argumentos><mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>SUELDOBASE</nombre>                            
                    <valor>213.00</valor> <descripcion>SUELDOBASE</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento><mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>con proyecto</nombre>                            
                    <valor>100.00</valor> <descripcion>con proyecto</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento><mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>DIASLABORADOS</nombre>                            
                    <valor>15.00</valor> <descripcion>DIASLABORADOS</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento></argumentos><importe>31.95</importe> </mx.org.ift.serp.conceptoBindingXml.DetallesConcepto>  ');
INSERT INTO sgnom.tsgnomcncptoquinc (cod_cncptoquincid, cod_empquincenaid_fk, cod_conceptoid_fk, imp_concepto, imp_gravado, imp_exento, xml_desgloce) VALUES (4, 64, 4, 31.95, 0.00, 0.00, '                
                <mx.org.ift.serp.conceptoBindingXml.DetallesConcepto>                  
                <claveConcepto>QW4</claveConcepto>                  
                <nombreConcepto>QW4</nombreConcepto><formula>:(:SUELDOBASE:/:con proyecto:):*:DIASLABORADOS</formula>                  
                <argumentos><mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>SUELDOBASE</nombre>                            
                    <valor>213.00</valor> <descripcion>SUELDOBASE</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento><mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>con proyecto</nombre>                            
                    <valor>100.00</valor> <descripcion>con proyecto</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento><mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>DIASLABORADOS</nombre>                            
                    <valor>15.00</valor> <descripcion>DIASLABORADOS</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento></argumentos><importe>31.95</importe> </mx.org.ift.serp.conceptoBindingXml.DetallesConcepto>  ');
INSERT INTO sgnom.tsgnomcncptoquinc (cod_cncptoquincid, cod_empquincenaid_fk, cod_conceptoid_fk, imp_concepto, imp_gravado, imp_exento, xml_desgloce) VALUES (6, 146, 1, 2500.00, 0.00, 0.00, '                
                <mx.org.ift.serp.conceptoBindingXml.DetallesConcepto>                  
                <claveConcepto>SQ</claveConcepto>                  
                <nombreConcepto>SUELDO QUINCENA</nombreConcepto><formula>:(:SUELDOBASE:/:DIASDELMES:):*:DIASLABORADOS</formula>                  
                <argumentos><mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>SUELDOBASE</nombre>                            
                    <valor>5000.00</valor> <descripcion>SUELDOBASE</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento><mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>DIASDELMES</nombre>                            
                    <valor>30.00</valor> <descripcion>DIASDELMES</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento><mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>DIASLABORADOS</nombre>                            
                    <valor>15.00</valor> <descripcion>DIASLABORADOS</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento></argumentos><importe>2500.00</importe> </mx.org.ift.serp.conceptoBindingXml.DetallesConcepto>  ');
INSERT INTO sgnom.tsgnomcncptoquinc (cod_cncptoquincid, cod_empquincenaid_fk, cod_conceptoid_fk, imp_concepto, imp_gravado, imp_exento, xml_desgloce) VALUES (7, 147, 1, 106.50, 0.00, 0.00, '                
                <mx.org.ift.serp.conceptoBindingXml.DetallesConcepto>                  
                <claveConcepto>SQ</claveConcepto>                  
                <nombreConcepto>SUELDO QUINCENA</nombreConcepto><formula>:(:SUELDOBASE:/:DIASDELMES:):*:DIASLABORADOS</formula>                  
                <argumentos><mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>SUELDOBASE</nombre>                            
                    <valor>213.00</valor> <descripcion>SUELDOBASE</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento><mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>DIASDELMES</nombre>                            
                    <valor>30.00</valor> <descripcion>DIASDELMES</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento><mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>DIASLABORADOS</nombre>                            
                    <valor>15.00</valor> <descripcion>DIASLABORADOS</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento></argumentos><importe>106.50</importe> </mx.org.ift.serp.conceptoBindingXml.DetallesConcepto>  ');
INSERT INTO sgnom.tsgnomcncptoquinc (cod_cncptoquincid, cod_empquincenaid_fk, cod_conceptoid_fk, imp_concepto, imp_gravado, imp_exento, xml_desgloce) VALUES (33, 173, 1, 0.00, 0.00, 0.00, NULL);
INSERT INTO sgnom.tsgnomcncptoquinc (cod_cncptoquincid, cod_empquincenaid_fk, cod_conceptoid_fk, imp_concepto, imp_gravado, imp_exento, xml_desgloce) VALUES (34, 174, 1, 0.00, 0.00, 0.00, NULL);
INSERT INTO sgnom.tsgnomcncptoquinc (cod_cncptoquincid, cod_empquincenaid_fk, cod_conceptoid_fk, imp_concepto, imp_gravado, imp_exento, xml_desgloce) VALUES (35, 175, 1, 0.00, 0.00, 0.00, NULL);
INSERT INTO sgnom.tsgnomcncptoquinc (cod_cncptoquincid, cod_empquincenaid_fk, cod_conceptoid_fk, imp_concepto, imp_gravado, imp_exento, xml_desgloce) VALUES (36, 176, 1, 0.00, 0.00, 0.00, NULL);
INSERT INTO sgnom.tsgnomcncptoquinc (cod_cncptoquincid, cod_empquincenaid_fk, cod_conceptoid_fk, imp_concepto, imp_gravado, imp_exento, xml_desgloce) VALUES (37, 177, 1, 0.00, 0.00, 0.00, NULL);
INSERT INTO sgnom.tsgnomcncptoquinc (cod_cncptoquincid, cod_empquincenaid_fk, cod_conceptoid_fk, imp_concepto, imp_gravado, imp_exento, xml_desgloce) VALUES (38, 178, 1, 0.00, 0.00, 0.00, NULL);
INSERT INTO sgnom.tsgnomcncptoquinc (cod_cncptoquincid, cod_empquincenaid_fk, cod_conceptoid_fk, imp_concepto, imp_gravado, imp_exento, xml_desgloce) VALUES (39, 179, 1, 0.00, 0.00, 0.00, NULL);
INSERT INTO sgnom.tsgnomcncptoquinc (cod_cncptoquincid, cod_empquincenaid_fk, cod_conceptoid_fk, imp_concepto, imp_gravado, imp_exento, xml_desgloce) VALUES (40, 180, 1, 0.00, 0.00, 0.00, NULL);
INSERT INTO sgnom.tsgnomcncptoquinc (cod_cncptoquincid, cod_empquincenaid_fk, cod_conceptoid_fk, imp_concepto, imp_gravado, imp_exento, xml_desgloce) VALUES (41, 181, 1, 0.00, 0.00, 0.00, NULL);
INSERT INTO sgnom.tsgnomcncptoquinc (cod_cncptoquincid, cod_empquincenaid_fk, cod_conceptoid_fk, imp_concepto, imp_gravado, imp_exento, xml_desgloce) VALUES (42, 182, 1, 0.00, 0.00, 0.00, NULL);
INSERT INTO sgnom.tsgnomcncptoquinc (cod_cncptoquincid, cod_empquincenaid_fk, cod_conceptoid_fk, imp_concepto, imp_gravado, imp_exento, xml_desgloce) VALUES (43, 183, 1, 0.00, 0.00, 0.00, NULL);
INSERT INTO sgnom.tsgnomcncptoquinc (cod_cncptoquincid, cod_empquincenaid_fk, cod_conceptoid_fk, imp_concepto, imp_gravado, imp_exento, xml_desgloce) VALUES (44, 184, 1, 0.00, 0.00, 0.00, NULL);
INSERT INTO sgnom.tsgnomcncptoquinc (cod_cncptoquincid, cod_empquincenaid_fk, cod_conceptoid_fk, imp_concepto, imp_gravado, imp_exento, xml_desgloce) VALUES (45, 185, 1, 0.00, 0.00, 0.00, NULL);
INSERT INTO sgnom.tsgnomcncptoquinc (cod_cncptoquincid, cod_empquincenaid_fk, cod_conceptoid_fk, imp_concepto, imp_gravado, imp_exento, xml_desgloce) VALUES (46, 186, 1, 0.00, 0.00, 0.00, NULL);
INSERT INTO sgnom.tsgnomcncptoquinc (cod_cncptoquincid, cod_empquincenaid_fk, cod_conceptoid_fk, imp_concepto, imp_gravado, imp_exento, xml_desgloce) VALUES (47, 187, 1, 0.00, 0.00, 0.00, NULL);
INSERT INTO sgnom.tsgnomcncptoquinc (cod_cncptoquincid, cod_empquincenaid_fk, cod_conceptoid_fk, imp_concepto, imp_gravado, imp_exento, xml_desgloce) VALUES (48, 188, 1, 0.00, 0.00, 0.00, NULL);
INSERT INTO sgnom.tsgnomcncptoquinc (cod_cncptoquincid, cod_empquincenaid_fk, cod_conceptoid_fk, imp_concepto, imp_gravado, imp_exento, xml_desgloce) VALUES (49, 189, 1, 0.00, 0.00, 0.00, NULL);
INSERT INTO sgnom.tsgnomcncptoquinc (cod_cncptoquincid, cod_empquincenaid_fk, cod_conceptoid_fk, imp_concepto, imp_gravado, imp_exento, xml_desgloce) VALUES (50, 190, 1, 0.00, 0.00, 0.00, NULL);
INSERT INTO sgnom.tsgnomcncptoquinc (cod_cncptoquincid, cod_empquincenaid_fk, cod_conceptoid_fk, imp_concepto, imp_gravado, imp_exento, xml_desgloce) VALUES (51, 191, 1, 0.00, 0.00, 0.00, NULL);
INSERT INTO sgnom.tsgnomcncptoquinc (cod_cncptoquincid, cod_empquincenaid_fk, cod_conceptoid_fk, imp_concepto, imp_gravado, imp_exento, xml_desgloce) VALUES (8, 148, 1, 106.50, 0.00, 0.00, '                
                <mx.org.ift.serp.conceptoBindingXml.DetallesConcepto>                  
                <claveConcepto>SQ</claveConcepto>                  
                <nombreConcepto>SUELDO QUINCENA</nombreConcepto><formula>:(:SUELDOBASE:/:DIASDELMES:):*:DIASLABORADOS</formula>                  
                <argumentos><mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>SUELDOBASE</nombre>                            
                    <valor>213.00</valor> <descripcion>SUELDOBASE</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento><mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>DIASDELMES</nombre>                            
                    <valor>30.00</valor> <descripcion>DIASDELMES</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento><mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>DIASLABORADOS</nombre>                            
                    <valor>15.00</valor> <descripcion>DIASLABORADOS</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento></argumentos><importe>106.50</importe> </mx.org.ift.serp.conceptoBindingXml.DetallesConcepto>  ');
INSERT INTO sgnom.tsgnomcncptoquinc (cod_cncptoquincid, cod_empquincenaid_fk, cod_conceptoid_fk, imp_concepto, imp_gravado, imp_exento, xml_desgloce) VALUES (9, 149, 1, 106.50, 0.00, 0.00, '                
                <mx.org.ift.serp.conceptoBindingXml.DetallesConcepto>                  
                <claveConcepto>SQ</claveConcepto>                  
                <nombreConcepto>SUELDO QUINCENA</nombreConcepto><formula>:(:SUELDOBASE:/:DIASDELMES:):*:DIASLABORADOS</formula>                  
                <argumentos><mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>SUELDOBASE</nombre>                            
                    <valor>213.00</valor> <descripcion>SUELDOBASE</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento><mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>DIASDELMES</nombre>                            
                    <valor>30.00</valor> <descripcion>DIASDELMES</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento><mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>DIASLABORADOS</nombre>                            
                    <valor>15.00</valor> <descripcion>DIASLABORADOS</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento></argumentos><importe>106.50</importe> </mx.org.ift.serp.conceptoBindingXml.DetallesConcepto>  ');
INSERT INTO sgnom.tsgnomcncptoquinc (cod_cncptoquincid, cod_empquincenaid_fk, cod_conceptoid_fk, imp_concepto, imp_gravado, imp_exento, xml_desgloce) VALUES (10, 150, 1, 106.50, 0.00, 0.00, '                
                <mx.org.ift.serp.conceptoBindingXml.DetallesConcepto>                  
                <claveConcepto>SQ</claveConcepto>                  
                <nombreConcepto>SUELDO QUINCENA</nombreConcepto><formula>:(:SUELDOBASE:/:DIASDELMES:):*:DIASLABORADOS</formula>                  
                <argumentos><mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>SUELDOBASE</nombre>                            
                    <valor>213.00</valor> <descripcion>SUELDOBASE</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento><mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>DIASDELMES</nombre>                            
                    <valor>30.00</valor> <descripcion>DIASDELMES</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento><mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>DIASLABORADOS</nombre>                            
                    <valor>15.00</valor> <descripcion>DIASLABORADOS</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento></argumentos><importe>106.50</importe> </mx.org.ift.serp.conceptoBindingXml.DetallesConcepto>  ');
INSERT INTO sgnom.tsgnomcncptoquinc (cod_cncptoquincid, cod_empquincenaid_fk, cod_conceptoid_fk, imp_concepto, imp_gravado, imp_exento, xml_desgloce) VALUES (11, 151, 1, 4.50, 0.00, 0.00, '                
                <mx.org.ift.serp.conceptoBindingXml.DetallesConcepto>                  
                <claveConcepto>SQ</claveConcepto>                  
                <nombreConcepto>SUELDO QUINCENA</nombreConcepto><formula>:(:SUELDOBASE:/:DIASDELMES:):*:DIASLABORADOS</formula>                  
                <argumentos><mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>SUELDOBASE</nombre>                            
                    <valor>9.00</valor> <descripcion>SUELDOBASE</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento><mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>DIASDELMES</nombre>                            
                    <valor>30.00</valor> <descripcion>DIASDELMES</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento><mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>DIASLABORADOS</nombre>                            
                    <valor>15.00</valor> <descripcion>DIASLABORADOS</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento></argumentos><importe>4.50</importe> </mx.org.ift.serp.conceptoBindingXml.DetallesConcepto>  ');
INSERT INTO sgnom.tsgnomcncptoquinc (cod_cncptoquincid, cod_empquincenaid_fk, cod_conceptoid_fk, imp_concepto, imp_gravado, imp_exento, xml_desgloce) VALUES (12, 152, 1, 1750.00, 0.00, 0.00, '                
                <mx.org.ift.serp.conceptoBindingXml.DetallesConcepto>                  
                <claveConcepto>SQ</claveConcepto>                  
                <nombreConcepto>SUELDO QUINCENA</nombreConcepto><formula>:(:SUELDOBASE:/:DIASDELMES:):*:DIASLABORADOS</formula>                  
                <argumentos><mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>SUELDOBASE</nombre>                            
                    <valor>3500.00</valor> <descripcion>SUELDOBASE</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento><mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>DIASDELMES</nombre>                            
                    <valor>30.00</valor> <descripcion>DIASDELMES</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento><mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>DIASLABORADOS</nombre>                            
                    <valor>15.00</valor> <descripcion>DIASLABORADOS</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento></argumentos><importe>1750.00</importe> </mx.org.ift.serp.conceptoBindingXml.DetallesConcepto>  ');
INSERT INTO sgnom.tsgnomcncptoquinc (cod_cncptoquincid, cod_empquincenaid_fk, cod_conceptoid_fk, imp_concepto, imp_gravado, imp_exento, xml_desgloce) VALUES (13, 153, 1, 172.00, 0.00, 0.00, '                
                <mx.org.ift.serp.conceptoBindingXml.DetallesConcepto>                  
                <claveConcepto>SQ</claveConcepto>                  
                <nombreConcepto>SUELDO QUINCENA</nombreConcepto><formula>:(:SUELDOBASE:/:DIASDELMES:):*:DIASLABORADOS</formula>                  
                <argumentos><mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>SUELDOBASE</nombre>                            
                    <valor>344.00</valor> <descripcion>SUELDOBASE</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento><mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>DIASDELMES</nombre>                            
                    <valor>30.00</valor> <descripcion>DIASDELMES</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento><mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>DIASLABORADOS</nombre>                            
                    <valor>15.00</valor> <descripcion>DIASLABORADOS</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento></argumentos><importe>172.00</importe> </mx.org.ift.serp.conceptoBindingXml.DetallesConcepto>  ');
INSERT INTO sgnom.tsgnomcncptoquinc (cod_cncptoquincid, cod_empquincenaid_fk, cod_conceptoid_fk, imp_concepto, imp_gravado, imp_exento, xml_desgloce) VALUES (14, 154, 1, 0.00, 0.00, 0.00, NULL);
INSERT INTO sgnom.tsgnomcncptoquinc (cod_cncptoquincid, cod_empquincenaid_fk, cod_conceptoid_fk, imp_concepto, imp_gravado, imp_exento, xml_desgloce) VALUES (15, 155, 1, 0.00, 0.00, 0.00, NULL);
INSERT INTO sgnom.tsgnomcncptoquinc (cod_cncptoquincid, cod_empquincenaid_fk, cod_conceptoid_fk, imp_concepto, imp_gravado, imp_exento, xml_desgloce) VALUES (16, 156, 1, 0.00, 0.00, 0.00, NULL);
INSERT INTO sgnom.tsgnomcncptoquinc (cod_cncptoquincid, cod_empquincenaid_fk, cod_conceptoid_fk, imp_concepto, imp_gravado, imp_exento, xml_desgloce) VALUES (17, 157, 1, 0.00, 0.00, 0.00, NULL);
INSERT INTO sgnom.tsgnomcncptoquinc (cod_cncptoquincid, cod_empquincenaid_fk, cod_conceptoid_fk, imp_concepto, imp_gravado, imp_exento, xml_desgloce) VALUES (18, 158, 1, 0.00, 0.00, 0.00, NULL);
INSERT INTO sgnom.tsgnomcncptoquinc (cod_cncptoquincid, cod_empquincenaid_fk, cod_conceptoid_fk, imp_concepto, imp_gravado, imp_exento, xml_desgloce) VALUES (19, 159, 1, 0.00, 0.00, 0.00, NULL);
INSERT INTO sgnom.tsgnomcncptoquinc (cod_cncptoquincid, cod_empquincenaid_fk, cod_conceptoid_fk, imp_concepto, imp_gravado, imp_exento, xml_desgloce) VALUES (20, 160, 1, 0.00, 0.00, 0.00, NULL);
INSERT INTO sgnom.tsgnomcncptoquinc (cod_cncptoquincid, cod_empquincenaid_fk, cod_conceptoid_fk, imp_concepto, imp_gravado, imp_exento, xml_desgloce) VALUES (21, 161, 1, 0.00, 0.00, 0.00, NULL);
INSERT INTO sgnom.tsgnomcncptoquinc (cod_cncptoquincid, cod_empquincenaid_fk, cod_conceptoid_fk, imp_concepto, imp_gravado, imp_exento, xml_desgloce) VALUES (22, 162, 1, 0.00, 0.00, 0.00, NULL);
INSERT INTO sgnom.tsgnomcncptoquinc (cod_cncptoquincid, cod_empquincenaid_fk, cod_conceptoid_fk, imp_concepto, imp_gravado, imp_exento, xml_desgloce) VALUES (23, 163, 1, 0.00, 0.00, 0.00, NULL);
INSERT INTO sgnom.tsgnomcncptoquinc (cod_cncptoquincid, cod_empquincenaid_fk, cod_conceptoid_fk, imp_concepto, imp_gravado, imp_exento, xml_desgloce) VALUES (24, 164, 1, 0.00, 0.00, 0.00, NULL);
INSERT INTO sgnom.tsgnomcncptoquinc (cod_cncptoquincid, cod_empquincenaid_fk, cod_conceptoid_fk, imp_concepto, imp_gravado, imp_exento, xml_desgloce) VALUES (25, 165, 1, 0.00, 0.00, 0.00, NULL);
INSERT INTO sgnom.tsgnomcncptoquinc (cod_cncptoquincid, cod_empquincenaid_fk, cod_conceptoid_fk, imp_concepto, imp_gravado, imp_exento, xml_desgloce) VALUES (26, 166, 1, 0.00, 0.00, 0.00, NULL);
INSERT INTO sgnom.tsgnomcncptoquinc (cod_cncptoquincid, cod_empquincenaid_fk, cod_conceptoid_fk, imp_concepto, imp_gravado, imp_exento, xml_desgloce) VALUES (27, 167, 1, 0.00, 0.00, 0.00, NULL);
INSERT INTO sgnom.tsgnomcncptoquinc (cod_cncptoquincid, cod_empquincenaid_fk, cod_conceptoid_fk, imp_concepto, imp_gravado, imp_exento, xml_desgloce) VALUES (28, 168, 1, 0.00, 0.00, 0.00, NULL);
INSERT INTO sgnom.tsgnomcncptoquinc (cod_cncptoquincid, cod_empquincenaid_fk, cod_conceptoid_fk, imp_concepto, imp_gravado, imp_exento, xml_desgloce) VALUES (29, 169, 1, 0.00, 0.00, 0.00, NULL);
INSERT INTO sgnom.tsgnomcncptoquinc (cod_cncptoquincid, cod_empquincenaid_fk, cod_conceptoid_fk, imp_concepto, imp_gravado, imp_exento, xml_desgloce) VALUES (30, 170, 1, 0.00, 0.00, 0.00, NULL);
INSERT INTO sgnom.tsgnomcncptoquinc (cod_cncptoquincid, cod_empquincenaid_fk, cod_conceptoid_fk, imp_concepto, imp_gravado, imp_exento, xml_desgloce) VALUES (31, 171, 1, 0.00, 0.00, 0.00, NULL);
INSERT INTO sgnom.tsgnomcncptoquinc (cod_cncptoquincid, cod_empquincenaid_fk, cod_conceptoid_fk, imp_concepto, imp_gravado, imp_exento, xml_desgloce) VALUES (32, 172, 1, 0.00, 0.00, 0.00, NULL);
INSERT INTO sgnom.tsgnomcncptoquinc (cod_cncptoquincid, cod_empquincenaid_fk, cod_conceptoid_fk, imp_concepto, imp_gravado, imp_exento, xml_desgloce) VALUES (52, 192, 1, 0.00, 0.00, 0.00, NULL);
INSERT INTO sgnom.tsgnomcncptoquinc (cod_cncptoquincid, cod_empquincenaid_fk, cod_conceptoid_fk, imp_concepto, imp_gravado, imp_exento, xml_desgloce) VALUES (53, 193, 1, 0.00, 0.00, 0.00, NULL);
INSERT INTO sgnom.tsgnomcncptoquinc (cod_cncptoquincid, cod_empquincenaid_fk, cod_conceptoid_fk, imp_concepto, imp_gravado, imp_exento, xml_desgloce) VALUES (54, 194, 1, 0.00, 0.00, 0.00, NULL);
INSERT INTO sgnom.tsgnomcncptoquinc (cod_cncptoquincid, cod_empquincenaid_fk, cod_conceptoid_fk, imp_concepto, imp_gravado, imp_exento, xml_desgloce) VALUES (55, 195, 1, 0.00, 0.00, 0.00, NULL);
INSERT INTO sgnom.tsgnomcncptoquinc (cod_cncptoquincid, cod_empquincenaid_fk, cod_conceptoid_fk, imp_concepto, imp_gravado, imp_exento, xml_desgloce) VALUES (56, 196, 1, 0.00, 0.00, 0.00, NULL);
INSERT INTO sgnom.tsgnomcncptoquinc (cod_cncptoquincid, cod_empquincenaid_fk, cod_conceptoid_fk, imp_concepto, imp_gravado, imp_exento, xml_desgloce) VALUES (57, 197, 1, 0.00, 0.00, 0.00, NULL);
INSERT INTO sgnom.tsgnomcncptoquinc (cod_cncptoquincid, cod_empquincenaid_fk, cod_conceptoid_fk, imp_concepto, imp_gravado, imp_exento, xml_desgloce) VALUES (58, 198, 1, 0.00, 0.00, 0.00, NULL);
INSERT INTO sgnom.tsgnomcncptoquinc (cod_cncptoquincid, cod_empquincenaid_fk, cod_conceptoid_fk, imp_concepto, imp_gravado, imp_exento, xml_desgloce) VALUES (59, 199, 1, 0.00, 0.00, 0.00, NULL);
INSERT INTO sgnom.tsgnomcncptoquinc (cod_cncptoquincid, cod_empquincenaid_fk, cod_conceptoid_fk, imp_concepto, imp_gravado, imp_exento, xml_desgloce) VALUES (60, 200, 1, 0.00, 0.00, 0.00, NULL);
INSERT INTO sgnom.tsgnomcncptoquinc (cod_cncptoquincid, cod_empquincenaid_fk, cod_conceptoid_fk, imp_concepto, imp_gravado, imp_exento, xml_desgloce) VALUES (61, 201, 1, 0.00, 0.00, 0.00, NULL);
INSERT INTO sgnom.tsgnomcncptoquinc (cod_cncptoquincid, cod_empquincenaid_fk, cod_conceptoid_fk, imp_concepto, imp_gravado, imp_exento, xml_desgloce) VALUES (62, 202, 1, 2500.00, 0.00, 0.00, '                
                <mx.org.ift.serp.conceptoBindingXml.DetallesConcepto>                  
                <claveConcepto>SQ</claveConcepto>                  
                <nombreConcepto>SUELDO QUINCENA</nombreConcepto><formula>:(:SUELDOBASE:/:DIASDELMES:):*:DIASLABORADOS</formula>                  
                <argumentos><mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>SUELDOBASE</nombre>                            
                    <valor>5000.00</valor> <descripcion>SUELDOBASE</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento><mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>DIASDELMES</nombre>                            
                    <valor>30.00</valor> <descripcion>DIASDELMES</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento><mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>DIASLABORADOS</nombre>                            
                    <valor>15.00</valor> <descripcion>DIASLABORADOS</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento></argumentos><importe>2500.00</importe> </mx.org.ift.serp.conceptoBindingXml.DetallesConcepto>  ');
INSERT INTO sgnom.tsgnomcncptoquinc (cod_cncptoquincid, cod_empquincenaid_fk, cod_conceptoid_fk, imp_concepto, imp_gravado, imp_exento, xml_desgloce) VALUES (70, 210, 1, 0.00, 0.00, 0.00, NULL);
INSERT INTO sgnom.tsgnomcncptoquinc (cod_cncptoquincid, cod_empquincenaid_fk, cod_conceptoid_fk, imp_concepto, imp_gravado, imp_exento, xml_desgloce) VALUES (71, 211, 1, 0.00, 0.00, 0.00, NULL);
INSERT INTO sgnom.tsgnomcncptoquinc (cod_cncptoquincid, cod_empquincenaid_fk, cod_conceptoid_fk, imp_concepto, imp_gravado, imp_exento, xml_desgloce) VALUES (72, 212, 1, 0.00, 0.00, 0.00, NULL);
INSERT INTO sgnom.tsgnomcncptoquinc (cod_cncptoquincid, cod_empquincenaid_fk, cod_conceptoid_fk, imp_concepto, imp_gravado, imp_exento, xml_desgloce) VALUES (73, 213, 1, 0.00, 0.00, 0.00, NULL);
INSERT INTO sgnom.tsgnomcncptoquinc (cod_cncptoquincid, cod_empquincenaid_fk, cod_conceptoid_fk, imp_concepto, imp_gravado, imp_exento, xml_desgloce) VALUES (74, 214, 1, 0.00, 0.00, 0.00, NULL);
INSERT INTO sgnom.tsgnomcncptoquinc (cod_cncptoquincid, cod_empquincenaid_fk, cod_conceptoid_fk, imp_concepto, imp_gravado, imp_exento, xml_desgloce) VALUES (75, 215, 1, 0.00, 0.00, 0.00, NULL);
INSERT INTO sgnom.tsgnomcncptoquinc (cod_cncptoquincid, cod_empquincenaid_fk, cod_conceptoid_fk, imp_concepto, imp_gravado, imp_exento, xml_desgloce) VALUES (76, 216, 1, 0.00, 0.00, 0.00, NULL);
INSERT INTO sgnom.tsgnomcncptoquinc (cod_cncptoquincid, cod_empquincenaid_fk, cod_conceptoid_fk, imp_concepto, imp_gravado, imp_exento, xml_desgloce) VALUES (77, 217, 1, 0.00, 0.00, 0.00, NULL);
INSERT INTO sgnom.tsgnomcncptoquinc (cod_cncptoquincid, cod_empquincenaid_fk, cod_conceptoid_fk, imp_concepto, imp_gravado, imp_exento, xml_desgloce) VALUES (78, 218, 1, 0.00, 0.00, 0.00, NULL);
INSERT INTO sgnom.tsgnomcncptoquinc (cod_cncptoquincid, cod_empquincenaid_fk, cod_conceptoid_fk, imp_concepto, imp_gravado, imp_exento, xml_desgloce) VALUES (79, 219, 1, 0.00, 0.00, 0.00, NULL);
INSERT INTO sgnom.tsgnomcncptoquinc (cod_cncptoquincid, cod_empquincenaid_fk, cod_conceptoid_fk, imp_concepto, imp_gravado, imp_exento, xml_desgloce) VALUES (80, 220, 1, 0.00, 0.00, 0.00, NULL);
INSERT INTO sgnom.tsgnomcncptoquinc (cod_cncptoquincid, cod_empquincenaid_fk, cod_conceptoid_fk, imp_concepto, imp_gravado, imp_exento, xml_desgloce) VALUES (81, 221, 1, 0.00, 0.00, 0.00, NULL);
INSERT INTO sgnom.tsgnomcncptoquinc (cod_cncptoquincid, cod_empquincenaid_fk, cod_conceptoid_fk, imp_concepto, imp_gravado, imp_exento, xml_desgloce) VALUES (82, 222, 1, 0.00, 0.00, 0.00, NULL);
INSERT INTO sgnom.tsgnomcncptoquinc (cod_cncptoquincid, cod_empquincenaid_fk, cod_conceptoid_fk, imp_concepto, imp_gravado, imp_exento, xml_desgloce) VALUES (83, 223, 1, 0.00, 0.00, 0.00, NULL);
INSERT INTO sgnom.tsgnomcncptoquinc (cod_cncptoquincid, cod_empquincenaid_fk, cod_conceptoid_fk, imp_concepto, imp_gravado, imp_exento, xml_desgloce) VALUES (84, 224, 1, 0.00, 0.00, 0.00, NULL);
INSERT INTO sgnom.tsgnomcncptoquinc (cod_cncptoquincid, cod_empquincenaid_fk, cod_conceptoid_fk, imp_concepto, imp_gravado, imp_exento, xml_desgloce) VALUES (85, 225, 1, 0.00, 0.00, 0.00, NULL);
INSERT INTO sgnom.tsgnomcncptoquinc (cod_cncptoquincid, cod_empquincenaid_fk, cod_conceptoid_fk, imp_concepto, imp_gravado, imp_exento, xml_desgloce) VALUES (63, 203, 1, 106.50, 0.00, 0.00, '                
                <mx.org.ift.serp.conceptoBindingXml.DetallesConcepto>                  
                <claveConcepto>SQ</claveConcepto>                  
                <nombreConcepto>SUELDO QUINCENA</nombreConcepto><formula>:(:SUELDOBASE:/:DIASDELMES:):*:DIASLABORADOS</formula>                  
                <argumentos><mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>SUELDOBASE</nombre>                            
                    <valor>213.00</valor> <descripcion>SUELDOBASE</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento><mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>DIASDELMES</nombre>                            
                    <valor>30.00</valor> <descripcion>DIASDELMES</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento><mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>DIASLABORADOS</nombre>                            
                    <valor>15.00</valor> <descripcion>DIASLABORADOS</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento></argumentos><importe>106.50</importe> </mx.org.ift.serp.conceptoBindingXml.DetallesConcepto>  ');
INSERT INTO sgnom.tsgnomcncptoquinc (cod_cncptoquincid, cod_empquincenaid_fk, cod_conceptoid_fk, imp_concepto, imp_gravado, imp_exento, xml_desgloce) VALUES (64, 204, 1, 106.50, 0.00, 0.00, '                
                <mx.org.ift.serp.conceptoBindingXml.DetallesConcepto>                  
                <claveConcepto>SQ</claveConcepto>                  
                <nombreConcepto>SUELDO QUINCENA</nombreConcepto><formula>:(:SUELDOBASE:/:DIASDELMES:):*:DIASLABORADOS</formula>                  
                <argumentos><mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>SUELDOBASE</nombre>                            
                    <valor>213.00</valor> <descripcion>SUELDOBASE</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento><mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>DIASDELMES</nombre>                            
                    <valor>30.00</valor> <descripcion>DIASDELMES</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento><mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>DIASLABORADOS</nombre>                            
                    <valor>15.00</valor> <descripcion>DIASLABORADOS</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento></argumentos><importe>106.50</importe> </mx.org.ift.serp.conceptoBindingXml.DetallesConcepto>  ');
INSERT INTO sgnom.tsgnomcncptoquinc (cod_cncptoquincid, cod_empquincenaid_fk, cod_conceptoid_fk, imp_concepto, imp_gravado, imp_exento, xml_desgloce) VALUES (65, 205, 1, 106.50, 0.00, 0.00, '                
                <mx.org.ift.serp.conceptoBindingXml.DetallesConcepto>                  
                <claveConcepto>SQ</claveConcepto>                  
                <nombreConcepto>SUELDO QUINCENA</nombreConcepto><formula>:(:SUELDOBASE:/:DIASDELMES:):*:DIASLABORADOS</formula>                  
                <argumentos><mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>SUELDOBASE</nombre>                            
                    <valor>213.00</valor> <descripcion>SUELDOBASE</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento><mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>DIASDELMES</nombre>                            
                    <valor>30.00</valor> <descripcion>DIASDELMES</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento><mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>DIASLABORADOS</nombre>                            
                    <valor>15.00</valor> <descripcion>DIASLABORADOS</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento></argumentos><importe>106.50</importe> </mx.org.ift.serp.conceptoBindingXml.DetallesConcepto>  ');
INSERT INTO sgnom.tsgnomcncptoquinc (cod_cncptoquincid, cod_empquincenaid_fk, cod_conceptoid_fk, imp_concepto, imp_gravado, imp_exento, xml_desgloce) VALUES (66, 206, 1, 106.50, 0.00, 0.00, '                
                <mx.org.ift.serp.conceptoBindingXml.DetallesConcepto>                  
                <claveConcepto>SQ</claveConcepto>                  
                <nombreConcepto>SUELDO QUINCENA</nombreConcepto><formula>:(:SUELDOBASE:/:DIASDELMES:):*:DIASLABORADOS</formula>                  
                <argumentos><mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>SUELDOBASE</nombre>                            
                    <valor>213.00</valor> <descripcion>SUELDOBASE</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento><mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>DIASDELMES</nombre>                            
                    <valor>30.00</valor> <descripcion>DIASDELMES</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento><mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>DIASLABORADOS</nombre>                            
                    <valor>15.00</valor> <descripcion>DIASLABORADOS</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento></argumentos><importe>106.50</importe> </mx.org.ift.serp.conceptoBindingXml.DetallesConcepto>  ');
INSERT INTO sgnom.tsgnomcncptoquinc (cod_cncptoquincid, cod_empquincenaid_fk, cod_conceptoid_fk, imp_concepto, imp_gravado, imp_exento, xml_desgloce) VALUES (67, 207, 1, 4.50, 0.00, 0.00, '                
                <mx.org.ift.serp.conceptoBindingXml.DetallesConcepto>                  
                <claveConcepto>SQ</claveConcepto>                  
                <nombreConcepto>SUELDO QUINCENA</nombreConcepto><formula>:(:SUELDOBASE:/:DIASDELMES:):*:DIASLABORADOS</formula>                  
                <argumentos><mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>SUELDOBASE</nombre>                            
                    <valor>9.00</valor> <descripcion>SUELDOBASE</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento><mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>DIASDELMES</nombre>                            
                    <valor>30.00</valor> <descripcion>DIASDELMES</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento><mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>DIASLABORADOS</nombre>                            
                    <valor>15.00</valor> <descripcion>DIASLABORADOS</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento></argumentos><importe>4.50</importe> </mx.org.ift.serp.conceptoBindingXml.DetallesConcepto>  ');
INSERT INTO sgnom.tsgnomcncptoquinc (cod_cncptoquincid, cod_empquincenaid_fk, cod_conceptoid_fk, imp_concepto, imp_gravado, imp_exento, xml_desgloce) VALUES (68, 208, 1, 1750.00, 0.00, 0.00, '                
                <mx.org.ift.serp.conceptoBindingXml.DetallesConcepto>                  
                <claveConcepto>SQ</claveConcepto>                  
                <nombreConcepto>SUELDO QUINCENA</nombreConcepto><formula>:(:SUELDOBASE:/:DIASDELMES:):*:DIASLABORADOS</formula>                  
                <argumentos><mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>SUELDOBASE</nombre>                            
                    <valor>3500.00</valor> <descripcion>SUELDOBASE</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento><mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>DIASDELMES</nombre>                            
                    <valor>30.00</valor> <descripcion>DIASDELMES</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento><mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>DIASLABORADOS</nombre>                            
                    <valor>15.00</valor> <descripcion>DIASLABORADOS</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento></argumentos><importe>1750.00</importe> </mx.org.ift.serp.conceptoBindingXml.DetallesConcepto>  ');
INSERT INTO sgnom.tsgnomcncptoquinc (cod_cncptoquincid, cod_empquincenaid_fk, cod_conceptoid_fk, imp_concepto, imp_gravado, imp_exento, xml_desgloce) VALUES (69, 209, 1, 172.00, 0.00, 0.00, '                
                <mx.org.ift.serp.conceptoBindingXml.DetallesConcepto>                  
                <claveConcepto>SQ</claveConcepto>                  
                <nombreConcepto>SUELDO QUINCENA</nombreConcepto><formula>:(:SUELDOBASE:/:DIASDELMES:):*:DIASLABORADOS</formula>                  
                <argumentos><mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>SUELDOBASE</nombre>                            
                    <valor>344.00</valor> <descripcion>SUELDOBASE</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento><mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>DIASDELMES</nombre>                            
                    <valor>30.00</valor> <descripcion>DIASDELMES</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento><mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>DIASLABORADOS</nombre>                            
                    <valor>15.00</valor> <descripcion>DIASLABORADOS</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento></argumentos><importe>172.00</importe> </mx.org.ift.serp.conceptoBindingXml.DetallesConcepto>  ');


--
-- TOC entry 3750 (class 0 OID 97258)
-- Dependencies: 372
-- Data for Name: tsgnomcncptoquincht; Type: TABLE DATA; Schema: sgnom; Owner: suite
--



--
-- TOC entry 3751 (class 0 OID 97264)
-- Dependencies: 373
-- Data for Name: tsgnomconcepto; Type: TABLE DATA; Schema: sgnom; Owner: suite
--

INSERT INTO sgnom.tsgnomconcepto (cod_conceptoid, cod_nbconcepto, cod_claveconcepto, cnu_prioricalculo, cnu_articulo, bol_estatus, cod_formulaid_fk, cod_tipoconceptoid_fk, cod_calculoid_fk, cod_conceptosatid_fk, cod_frecuenciapago, cod_partidaprep, cnu_cuentacontable, cod_gravado, cod_excento, bol_aplicaisn, bol_retroactividad, cnu_topeex, cod_clasificadorid_fk, cod_tiponominaid_fk, aud_codcreadopor, aud_codmodificadopor, aud_feccreacion, aud_fecmodificacion, imp_monto) VALUES (3, 'T', 'T', 1, 5, true, 11, 1, 2, 1, 'Q', 5, '5', NULL, NULL, false, false, NULL, 1, 2, 13, NULL, '2019-10-07', NULL, NULL);
INSERT INTO sgnom.tsgnomconcepto (cod_conceptoid, cod_nbconcepto, cod_claveconcepto, cnu_prioricalculo, cnu_articulo, bol_estatus, cod_formulaid_fk, cod_tipoconceptoid_fk, cod_calculoid_fk, cod_conceptosatid_fk, cod_frecuenciapago, cod_partidaprep, cnu_cuentacontable, cod_gravado, cod_excento, bol_aplicaisn, bol_retroactividad, cnu_topeex, cod_clasificadorid_fk, cod_tiponominaid_fk, aud_codcreadopor, aud_codmodificadopor, aud_feccreacion, aud_fecmodificacion, imp_monto) VALUES (8, 'QWE', 'QWE', 2, 45, true, NULL, 1, 1, 1, 'Q', 4536, '345346463646436345', NULL, NULL, false, false, NULL, 1, 2, 13, NULL, '2019-11-05', NULL, 123456);
INSERT INTO sgnom.tsgnomconcepto (cod_conceptoid, cod_nbconcepto, cod_claveconcepto, cnu_prioricalculo, cnu_articulo, bol_estatus, cod_formulaid_fk, cod_tipoconceptoid_fk, cod_calculoid_fk, cod_conceptosatid_fk, cod_frecuenciapago, cod_partidaprep, cnu_cuentacontable, cod_gravado, cod_excento, bol_aplicaisn, bol_retroactividad, cnu_topeex, cod_clasificadorid_fk, cod_tiponominaid_fk, aud_codcreadopor, aud_codmodificadopor, aud_feccreacion, aud_fecmodificacion, imp_monto) VALUES (6, 'RENTA', 'RENT', 3, 989, true, NULL, 1, 1, 1, 'M', 31321, '12321312321', '1', '1', true, true, 546, 1, 1, 13, NULL, '2019-11-05', NULL, 456);
INSERT INTO sgnom.tsgnomconcepto (cod_conceptoid, cod_nbconcepto, cod_claveconcepto, cnu_prioricalculo, cnu_articulo, bol_estatus, cod_formulaid_fk, cod_tipoconceptoid_fk, cod_calculoid_fk, cod_conceptosatid_fk, cod_frecuenciapago, cod_partidaprep, cnu_cuentacontable, cod_gravado, cod_excento, bol_aplicaisn, bol_retroactividad, cnu_topeex, cod_clasificadorid_fk, cod_tiponominaid_fk, aud_codcreadopor, aud_codmodificadopor, aud_feccreacion, aud_fecmodificacion, imp_monto) VALUES (4, 'U', 'U', 4, 0, true, 13, 1, 2, 1, 'Q', 0, '123456789012345678', '0', '0', false, false, NULL, 1, 2, 13, 13, '2019-10-07', '2019-11-05', NULL);
INSERT INTO sgnom.tsgnomconcepto (cod_conceptoid, cod_nbconcepto, cod_claveconcepto, cnu_prioricalculo, cnu_articulo, bol_estatus, cod_formulaid_fk, cod_tipoconceptoid_fk, cod_calculoid_fk, cod_conceptosatid_fk, cod_frecuenciapago, cod_partidaprep, cnu_cuentacontable, cod_gravado, cod_excento, bol_aplicaisn, bol_retroactividad, cnu_topeex, cod_clasificadorid_fk, cod_tiponominaid_fk, aud_codcreadopor, aud_codmodificadopor, aud_feccreacion, aud_fecmodificacion, imp_monto) VALUES (1, 'SUELDO QUINCENA', 'SQ', 1, 1, true, 9, 2, 2, 1, 'Q', 1, '1', '0', '0', false, false, NULL, 1, 1, 13, 13, '2019-07-08', '2019-08-19', NULL);
INSERT INTO sgnom.tsgnomconcepto (cod_conceptoid, cod_nbconcepto, cod_claveconcepto, cnu_prioricalculo, cnu_articulo, bol_estatus, cod_formulaid_fk, cod_tipoconceptoid_fk, cod_calculoid_fk, cod_conceptosatid_fk, cod_frecuenciapago, cod_partidaprep, cnu_cuentacontable, cod_gravado, cod_excento, bol_aplicaisn, bol_retroactividad, cnu_topeex, cod_clasificadorid_fk, cod_tiponominaid_fk, aud_codcreadopor, aud_codmodificadopor, aud_feccreacion, aud_fecmodificacion, imp_monto) VALUES (9, 'CONCEPTO NUEVO', 'CON', 2, 25, true, NULL, 2, 1, 2, 'Q', 10, '454648646843513478', NULL, NULL, false, false, NULL, 1, 1, 10, NULL, '2019-11-12', NULL, 500);
INSERT INTO sgnom.tsgnomconcepto (cod_conceptoid, cod_nbconcepto, cod_claveconcepto, cnu_prioricalculo, cnu_articulo, bol_estatus, cod_formulaid_fk, cod_tipoconceptoid_fk, cod_calculoid_fk, cod_conceptosatid_fk, cod_frecuenciapago, cod_partidaprep, cnu_cuentacontable, cod_gravado, cod_excento, bol_aplicaisn, bol_retroactividad, cnu_topeex, cod_clasificadorid_fk, cod_tiponominaid_fk, aud_codcreadopor, aud_codmodificadopor, aud_feccreacion, aud_fecmodificacion, imp_monto) VALUES (2, 'QW', 'QW', 3, 2, true, 10, 2, 2, 1, 'Q', 20, '2', '0', '0', false, false, NULL, 1, 1, 13, 13, '2019-09-09', '2019-11-05', NULL);


--
-- TOC entry 3752 (class 0 OID 97267)
-- Dependencies: 374
-- Data for Name: tsgnomconceptosat; Type: TABLE DATA; Schema: sgnom; Owner: suite
--

INSERT INTO sgnom.tsgnomconceptosat (cod_conceptosatid, des_conceptosat, des_descconcepto, bol_estatus) VALUES (1, 'ss1', 'sat1', true);
INSERT INTO sgnom.tsgnomconceptosat (cod_conceptosatid, des_conceptosat, des_descconcepto, bol_estatus) VALUES (2, 'ss2', 'sat2', true);


--
-- TOC entry 3753 (class 0 OID 97270)
-- Dependencies: 375
-- Data for Name: tsgnomconfpago; Type: TABLE DATA; Schema: sgnom; Owner: suite
--

INSERT INTO sgnom.tsgnomconfpago (cod_confpagoid, bol_pagoempleado, bol_pagorh, bol_pagofinanzas, cod_empquincenaid_fk) VALUES (17, NULL, NULL, NULL, 67);
INSERT INTO sgnom.tsgnomconfpago (cod_confpagoid, bol_pagoempleado, bol_pagorh, bol_pagofinanzas, cod_empquincenaid_fk) VALUES (18, NULL, NULL, NULL, 68);
INSERT INTO sgnom.tsgnomconfpago (cod_confpagoid, bol_pagoempleado, bol_pagorh, bol_pagofinanzas, cod_empquincenaid_fk) VALUES (19, NULL, NULL, NULL, 69);
INSERT INTO sgnom.tsgnomconfpago (cod_confpagoid, bol_pagoempleado, bol_pagorh, bol_pagofinanzas, cod_empquincenaid_fk) VALUES (20, NULL, NULL, NULL, 70);
INSERT INTO sgnom.tsgnomconfpago (cod_confpagoid, bol_pagoempleado, bol_pagorh, bol_pagofinanzas, cod_empquincenaid_fk) VALUES (21, NULL, NULL, NULL, 71);
INSERT INTO sgnom.tsgnomconfpago (cod_confpagoid, bol_pagoempleado, bol_pagorh, bol_pagofinanzas, cod_empquincenaid_fk) VALUES (22, NULL, NULL, NULL, 72);
INSERT INTO sgnom.tsgnomconfpago (cod_confpagoid, bol_pagoempleado, bol_pagorh, bol_pagofinanzas, cod_empquincenaid_fk) VALUES (23, NULL, NULL, NULL, 73);
INSERT INTO sgnom.tsgnomconfpago (cod_confpagoid, bol_pagoempleado, bol_pagorh, bol_pagofinanzas, cod_empquincenaid_fk) VALUES (24, NULL, NULL, NULL, 74);
INSERT INTO sgnom.tsgnomconfpago (cod_confpagoid, bol_pagoempleado, bol_pagorh, bol_pagofinanzas, cod_empquincenaid_fk) VALUES (25, NULL, NULL, NULL, 75);
INSERT INTO sgnom.tsgnomconfpago (cod_confpagoid, bol_pagoempleado, bol_pagorh, bol_pagofinanzas, cod_empquincenaid_fk) VALUES (26, NULL, NULL, NULL, 76);
INSERT INTO sgnom.tsgnomconfpago (cod_confpagoid, bol_pagoempleado, bol_pagorh, bol_pagofinanzas, cod_empquincenaid_fk) VALUES (27, NULL, NULL, NULL, 77);
INSERT INTO sgnom.tsgnomconfpago (cod_confpagoid, bol_pagoempleado, bol_pagorh, bol_pagofinanzas, cod_empquincenaid_fk) VALUES (28, NULL, NULL, NULL, 78);
INSERT INTO sgnom.tsgnomconfpago (cod_confpagoid, bol_pagoempleado, bol_pagorh, bol_pagofinanzas, cod_empquincenaid_fk) VALUES (29, NULL, NULL, NULL, 79);
INSERT INTO sgnom.tsgnomconfpago (cod_confpagoid, bol_pagoempleado, bol_pagorh, bol_pagofinanzas, cod_empquincenaid_fk) VALUES (30, NULL, NULL, NULL, 80);
INSERT INTO sgnom.tsgnomconfpago (cod_confpagoid, bol_pagoempleado, bol_pagorh, bol_pagofinanzas, cod_empquincenaid_fk) VALUES (31, NULL, NULL, NULL, 81);
INSERT INTO sgnom.tsgnomconfpago (cod_confpagoid, bol_pagoempleado, bol_pagorh, bol_pagofinanzas, cod_empquincenaid_fk) VALUES (32, NULL, NULL, NULL, 82);
INSERT INTO sgnom.tsgnomconfpago (cod_confpagoid, bol_pagoempleado, bol_pagorh, bol_pagofinanzas, cod_empquincenaid_fk) VALUES (33, NULL, NULL, NULL, 83);
INSERT INTO sgnom.tsgnomconfpago (cod_confpagoid, bol_pagoempleado, bol_pagorh, bol_pagofinanzas, cod_empquincenaid_fk) VALUES (34, NULL, NULL, NULL, 84);
INSERT INTO sgnom.tsgnomconfpago (cod_confpagoid, bol_pagoempleado, bol_pagorh, bol_pagofinanzas, cod_empquincenaid_fk) VALUES (35, NULL, NULL, NULL, 85);
INSERT INTO sgnom.tsgnomconfpago (cod_confpagoid, bol_pagoempleado, bol_pagorh, bol_pagofinanzas, cod_empquincenaid_fk) VALUES (36, NULL, NULL, NULL, 86);
INSERT INTO sgnom.tsgnomconfpago (cod_confpagoid, bol_pagoempleado, bol_pagorh, bol_pagofinanzas, cod_empquincenaid_fk) VALUES (37, NULL, NULL, NULL, 87);
INSERT INTO sgnom.tsgnomconfpago (cod_confpagoid, bol_pagoempleado, bol_pagorh, bol_pagofinanzas, cod_empquincenaid_fk) VALUES (38, NULL, NULL, NULL, 88);
INSERT INTO sgnom.tsgnomconfpago (cod_confpagoid, bol_pagoempleado, bol_pagorh, bol_pagofinanzas, cod_empquincenaid_fk) VALUES (39, NULL, NULL, NULL, 89);
INSERT INTO sgnom.tsgnomconfpago (cod_confpagoid, bol_pagoempleado, bol_pagorh, bol_pagofinanzas, cod_empquincenaid_fk) VALUES (40, NULL, NULL, NULL, 90);
INSERT INTO sgnom.tsgnomconfpago (cod_confpagoid, bol_pagoempleado, bol_pagorh, bol_pagofinanzas, cod_empquincenaid_fk) VALUES (41, NULL, NULL, NULL, 91);
INSERT INTO sgnom.tsgnomconfpago (cod_confpagoid, bol_pagoempleado, bol_pagorh, bol_pagofinanzas, cod_empquincenaid_fk) VALUES (13, NULL, NULL, true, 63);
INSERT INTO sgnom.tsgnomconfpago (cod_confpagoid, bol_pagoempleado, bol_pagorh, bol_pagofinanzas, cod_empquincenaid_fk) VALUES (12, NULL, true, true, 62);
INSERT INTO sgnom.tsgnomconfpago (cod_confpagoid, bol_pagoempleado, bol_pagorh, bol_pagofinanzas, cod_empquincenaid_fk) VALUES (14, NULL, NULL, true, 64);
INSERT INTO sgnom.tsgnomconfpago (cod_confpagoid, bol_pagoempleado, bol_pagorh, bol_pagofinanzas, cod_empquincenaid_fk) VALUES (16, NULL, NULL, false, 66);
INSERT INTO sgnom.tsgnomconfpago (cod_confpagoid, bol_pagoempleado, bol_pagorh, bol_pagofinanzas, cod_empquincenaid_fk) VALUES (15, NULL, NULL, false, 65);
INSERT INTO sgnom.tsgnomconfpago (cod_confpagoid, bol_pagoempleado, bol_pagorh, bol_pagofinanzas, cod_empquincenaid_fk) VALUES (42, NULL, true, NULL, 92);
INSERT INTO sgnom.tsgnomconfpago (cod_confpagoid, bol_pagoempleado, bol_pagorh, bol_pagofinanzas, cod_empquincenaid_fk) VALUES (43, NULL, true, NULL, 93);
INSERT INTO sgnom.tsgnomconfpago (cod_confpagoid, bol_pagoempleado, bol_pagorh, bol_pagofinanzas, cod_empquincenaid_fk) VALUES (44, NULL, true, NULL, 94);
INSERT INTO sgnom.tsgnomconfpago (cod_confpagoid, bol_pagoempleado, bol_pagorh, bol_pagofinanzas, cod_empquincenaid_fk) VALUES (45, NULL, true, NULL, 95);
INSERT INTO sgnom.tsgnomconfpago (cod_confpagoid, bol_pagoempleado, bol_pagorh, bol_pagofinanzas, cod_empquincenaid_fk) VALUES (46, NULL, true, NULL, 96);
INSERT INTO sgnom.tsgnomconfpago (cod_confpagoid, bol_pagoempleado, bol_pagorh, bol_pagofinanzas, cod_empquincenaid_fk) VALUES (47, NULL, true, NULL, 97);
INSERT INTO sgnom.tsgnomconfpago (cod_confpagoid, bol_pagoempleado, bol_pagorh, bol_pagofinanzas, cod_empquincenaid_fk) VALUES (54, NULL, NULL, NULL, 146);
INSERT INTO sgnom.tsgnomconfpago (cod_confpagoid, bol_pagoempleado, bol_pagorh, bol_pagofinanzas, cod_empquincenaid_fk) VALUES (55, NULL, NULL, NULL, 147);
INSERT INTO sgnom.tsgnomconfpago (cod_confpagoid, bol_pagoempleado, bol_pagorh, bol_pagofinanzas, cod_empquincenaid_fk) VALUES (56, NULL, NULL, NULL, 148);
INSERT INTO sgnom.tsgnomconfpago (cod_confpagoid, bol_pagoempleado, bol_pagorh, bol_pagofinanzas, cod_empquincenaid_fk) VALUES (57, NULL, NULL, NULL, 149);
INSERT INTO sgnom.tsgnomconfpago (cod_confpagoid, bol_pagoempleado, bol_pagorh, bol_pagofinanzas, cod_empquincenaid_fk) VALUES (58, NULL, NULL, NULL, 150);
INSERT INTO sgnom.tsgnomconfpago (cod_confpagoid, bol_pagoempleado, bol_pagorh, bol_pagofinanzas, cod_empquincenaid_fk) VALUES (59, NULL, NULL, NULL, 151);
INSERT INTO sgnom.tsgnomconfpago (cod_confpagoid, bol_pagoempleado, bol_pagorh, bol_pagofinanzas, cod_empquincenaid_fk) VALUES (60, NULL, NULL, NULL, 152);
INSERT INTO sgnom.tsgnomconfpago (cod_confpagoid, bol_pagoempleado, bol_pagorh, bol_pagofinanzas, cod_empquincenaid_fk) VALUES (61, NULL, NULL, NULL, 153);
INSERT INTO sgnom.tsgnomconfpago (cod_confpagoid, bol_pagoempleado, bol_pagorh, bol_pagofinanzas, cod_empquincenaid_fk) VALUES (62, NULL, NULL, NULL, 154);
INSERT INTO sgnom.tsgnomconfpago (cod_confpagoid, bol_pagoempleado, bol_pagorh, bol_pagofinanzas, cod_empquincenaid_fk) VALUES (63, NULL, NULL, NULL, 155);
INSERT INTO sgnom.tsgnomconfpago (cod_confpagoid, bol_pagoempleado, bol_pagorh, bol_pagofinanzas, cod_empquincenaid_fk) VALUES (64, NULL, NULL, NULL, 156);
INSERT INTO sgnom.tsgnomconfpago (cod_confpagoid, bol_pagoempleado, bol_pagorh, bol_pagofinanzas, cod_empquincenaid_fk) VALUES (65, NULL, NULL, NULL, 157);
INSERT INTO sgnom.tsgnomconfpago (cod_confpagoid, bol_pagoempleado, bol_pagorh, bol_pagofinanzas, cod_empquincenaid_fk) VALUES (66, NULL, NULL, NULL, 158);
INSERT INTO sgnom.tsgnomconfpago (cod_confpagoid, bol_pagoempleado, bol_pagorh, bol_pagofinanzas, cod_empquincenaid_fk) VALUES (67, NULL, NULL, NULL, 159);
INSERT INTO sgnom.tsgnomconfpago (cod_confpagoid, bol_pagoempleado, bol_pagorh, bol_pagofinanzas, cod_empquincenaid_fk) VALUES (68, NULL, NULL, NULL, 160);
INSERT INTO sgnom.tsgnomconfpago (cod_confpagoid, bol_pagoempleado, bol_pagorh, bol_pagofinanzas, cod_empquincenaid_fk) VALUES (69, NULL, NULL, NULL, 161);
INSERT INTO sgnom.tsgnomconfpago (cod_confpagoid, bol_pagoempleado, bol_pagorh, bol_pagofinanzas, cod_empquincenaid_fk) VALUES (70, NULL, NULL, NULL, 162);
INSERT INTO sgnom.tsgnomconfpago (cod_confpagoid, bol_pagoempleado, bol_pagorh, bol_pagofinanzas, cod_empquincenaid_fk) VALUES (71, NULL, NULL, NULL, 163);
INSERT INTO sgnom.tsgnomconfpago (cod_confpagoid, bol_pagoempleado, bol_pagorh, bol_pagofinanzas, cod_empquincenaid_fk) VALUES (72, NULL, NULL, NULL, 164);
INSERT INTO sgnom.tsgnomconfpago (cod_confpagoid, bol_pagoempleado, bol_pagorh, bol_pagofinanzas, cod_empquincenaid_fk) VALUES (73, NULL, NULL, NULL, 165);
INSERT INTO sgnom.tsgnomconfpago (cod_confpagoid, bol_pagoempleado, bol_pagorh, bol_pagofinanzas, cod_empquincenaid_fk) VALUES (74, NULL, NULL, NULL, 166);
INSERT INTO sgnom.tsgnomconfpago (cod_confpagoid, bol_pagoempleado, bol_pagorh, bol_pagofinanzas, cod_empquincenaid_fk) VALUES (75, NULL, NULL, NULL, 167);
INSERT INTO sgnom.tsgnomconfpago (cod_confpagoid, bol_pagoempleado, bol_pagorh, bol_pagofinanzas, cod_empquincenaid_fk) VALUES (76, NULL, NULL, NULL, 168);
INSERT INTO sgnom.tsgnomconfpago (cod_confpagoid, bol_pagoempleado, bol_pagorh, bol_pagofinanzas, cod_empquincenaid_fk) VALUES (77, NULL, NULL, NULL, 169);
INSERT INTO sgnom.tsgnomconfpago (cod_confpagoid, bol_pagoempleado, bol_pagorh, bol_pagofinanzas, cod_empquincenaid_fk) VALUES (78, NULL, NULL, NULL, 170);
INSERT INTO sgnom.tsgnomconfpago (cod_confpagoid, bol_pagoempleado, bol_pagorh, bol_pagofinanzas, cod_empquincenaid_fk) VALUES (79, NULL, NULL, NULL, 171);
INSERT INTO sgnom.tsgnomconfpago (cod_confpagoid, bol_pagoempleado, bol_pagorh, bol_pagofinanzas, cod_empquincenaid_fk) VALUES (80, NULL, NULL, NULL, 172);
INSERT INTO sgnom.tsgnomconfpago (cod_confpagoid, bol_pagoempleado, bol_pagorh, bol_pagofinanzas, cod_empquincenaid_fk) VALUES (81, NULL, NULL, NULL, 173);
INSERT INTO sgnom.tsgnomconfpago (cod_confpagoid, bol_pagoempleado, bol_pagorh, bol_pagofinanzas, cod_empquincenaid_fk) VALUES (82, NULL, NULL, NULL, 174);
INSERT INTO sgnom.tsgnomconfpago (cod_confpagoid, bol_pagoempleado, bol_pagorh, bol_pagofinanzas, cod_empquincenaid_fk) VALUES (83, NULL, NULL, NULL, 175);
INSERT INTO sgnom.tsgnomconfpago (cod_confpagoid, bol_pagoempleado, bol_pagorh, bol_pagofinanzas, cod_empquincenaid_fk) VALUES (84, NULL, NULL, NULL, 176);
INSERT INTO sgnom.tsgnomconfpago (cod_confpagoid, bol_pagoempleado, bol_pagorh, bol_pagofinanzas, cod_empquincenaid_fk) VALUES (85, NULL, NULL, NULL, 177);
INSERT INTO sgnom.tsgnomconfpago (cod_confpagoid, bol_pagoempleado, bol_pagorh, bol_pagofinanzas, cod_empquincenaid_fk) VALUES (86, NULL, NULL, NULL, 178);
INSERT INTO sgnom.tsgnomconfpago (cod_confpagoid, bol_pagoempleado, bol_pagorh, bol_pagofinanzas, cod_empquincenaid_fk) VALUES (87, NULL, NULL, NULL, 179);
INSERT INTO sgnom.tsgnomconfpago (cod_confpagoid, bol_pagoempleado, bol_pagorh, bol_pagofinanzas, cod_empquincenaid_fk) VALUES (88, NULL, NULL, NULL, 180);
INSERT INTO sgnom.tsgnomconfpago (cod_confpagoid, bol_pagoempleado, bol_pagorh, bol_pagofinanzas, cod_empquincenaid_fk) VALUES (89, NULL, NULL, NULL, 181);
INSERT INTO sgnom.tsgnomconfpago (cod_confpagoid, bol_pagoempleado, bol_pagorh, bol_pagofinanzas, cod_empquincenaid_fk) VALUES (90, NULL, NULL, NULL, 182);
INSERT INTO sgnom.tsgnomconfpago (cod_confpagoid, bol_pagoempleado, bol_pagorh, bol_pagofinanzas, cod_empquincenaid_fk) VALUES (91, NULL, NULL, NULL, 183);
INSERT INTO sgnom.tsgnomconfpago (cod_confpagoid, bol_pagoempleado, bol_pagorh, bol_pagofinanzas, cod_empquincenaid_fk) VALUES (92, NULL, NULL, NULL, 184);
INSERT INTO sgnom.tsgnomconfpago (cod_confpagoid, bol_pagoempleado, bol_pagorh, bol_pagofinanzas, cod_empquincenaid_fk) VALUES (93, NULL, NULL, NULL, 185);
INSERT INTO sgnom.tsgnomconfpago (cod_confpagoid, bol_pagoempleado, bol_pagorh, bol_pagofinanzas, cod_empquincenaid_fk) VALUES (94, NULL, NULL, NULL, 186);
INSERT INTO sgnom.tsgnomconfpago (cod_confpagoid, bol_pagoempleado, bol_pagorh, bol_pagofinanzas, cod_empquincenaid_fk) VALUES (95, NULL, NULL, NULL, 187);
INSERT INTO sgnom.tsgnomconfpago (cod_confpagoid, bol_pagoempleado, bol_pagorh, bol_pagofinanzas, cod_empquincenaid_fk) VALUES (96, NULL, NULL, NULL, 188);
INSERT INTO sgnom.tsgnomconfpago (cod_confpagoid, bol_pagoempleado, bol_pagorh, bol_pagofinanzas, cod_empquincenaid_fk) VALUES (97, NULL, NULL, NULL, 189);
INSERT INTO sgnom.tsgnomconfpago (cod_confpagoid, bol_pagoempleado, bol_pagorh, bol_pagofinanzas, cod_empquincenaid_fk) VALUES (98, NULL, NULL, NULL, 190);
INSERT INTO sgnom.tsgnomconfpago (cod_confpagoid, bol_pagoempleado, bol_pagorh, bol_pagofinanzas, cod_empquincenaid_fk) VALUES (99, NULL, NULL, NULL, 191);
INSERT INTO sgnom.tsgnomconfpago (cod_confpagoid, bol_pagoempleado, bol_pagorh, bol_pagofinanzas, cod_empquincenaid_fk) VALUES (100, NULL, NULL, NULL, 192);
INSERT INTO sgnom.tsgnomconfpago (cod_confpagoid, bol_pagoempleado, bol_pagorh, bol_pagofinanzas, cod_empquincenaid_fk) VALUES (101, NULL, NULL, NULL, 193);
INSERT INTO sgnom.tsgnomconfpago (cod_confpagoid, bol_pagoempleado, bol_pagorh, bol_pagofinanzas, cod_empquincenaid_fk) VALUES (102, NULL, NULL, NULL, 194);
INSERT INTO sgnom.tsgnomconfpago (cod_confpagoid, bol_pagoempleado, bol_pagorh, bol_pagofinanzas, cod_empquincenaid_fk) VALUES (103, NULL, NULL, NULL, 195);
INSERT INTO sgnom.tsgnomconfpago (cod_confpagoid, bol_pagoempleado, bol_pagorh, bol_pagofinanzas, cod_empquincenaid_fk) VALUES (104, NULL, NULL, NULL, 196);
INSERT INTO sgnom.tsgnomconfpago (cod_confpagoid, bol_pagoempleado, bol_pagorh, bol_pagofinanzas, cod_empquincenaid_fk) VALUES (105, NULL, NULL, NULL, 197);
INSERT INTO sgnom.tsgnomconfpago (cod_confpagoid, bol_pagoempleado, bol_pagorh, bol_pagofinanzas, cod_empquincenaid_fk) VALUES (106, NULL, NULL, NULL, 198);
INSERT INTO sgnom.tsgnomconfpago (cod_confpagoid, bol_pagoempleado, bol_pagorh, bol_pagofinanzas, cod_empquincenaid_fk) VALUES (107, NULL, NULL, NULL, 199);
INSERT INTO sgnom.tsgnomconfpago (cod_confpagoid, bol_pagoempleado, bol_pagorh, bol_pagofinanzas, cod_empquincenaid_fk) VALUES (108, NULL, NULL, NULL, 200);
INSERT INTO sgnom.tsgnomconfpago (cod_confpagoid, bol_pagoempleado, bol_pagorh, bol_pagofinanzas, cod_empquincenaid_fk) VALUES (109, NULL, NULL, NULL, 201);
INSERT INTO sgnom.tsgnomconfpago (cod_confpagoid, bol_pagoempleado, bol_pagorh, bol_pagofinanzas, cod_empquincenaid_fk) VALUES (110, NULL, NULL, NULL, 202);
INSERT INTO sgnom.tsgnomconfpago (cod_confpagoid, bol_pagoempleado, bol_pagorh, bol_pagofinanzas, cod_empquincenaid_fk) VALUES (111, NULL, NULL, NULL, 203);
INSERT INTO sgnom.tsgnomconfpago (cod_confpagoid, bol_pagoempleado, bol_pagorh, bol_pagofinanzas, cod_empquincenaid_fk) VALUES (112, NULL, NULL, NULL, 204);
INSERT INTO sgnom.tsgnomconfpago (cod_confpagoid, bol_pagoempleado, bol_pagorh, bol_pagofinanzas, cod_empquincenaid_fk) VALUES (113, NULL, NULL, NULL, 205);
INSERT INTO sgnom.tsgnomconfpago (cod_confpagoid, bol_pagoempleado, bol_pagorh, bol_pagofinanzas, cod_empquincenaid_fk) VALUES (114, NULL, NULL, NULL, 206);
INSERT INTO sgnom.tsgnomconfpago (cod_confpagoid, bol_pagoempleado, bol_pagorh, bol_pagofinanzas, cod_empquincenaid_fk) VALUES (115, NULL, NULL, NULL, 207);
INSERT INTO sgnom.tsgnomconfpago (cod_confpagoid, bol_pagoempleado, bol_pagorh, bol_pagofinanzas, cod_empquincenaid_fk) VALUES (116, NULL, NULL, NULL, 208);
INSERT INTO sgnom.tsgnomconfpago (cod_confpagoid, bol_pagoempleado, bol_pagorh, bol_pagofinanzas, cod_empquincenaid_fk) VALUES (117, NULL, NULL, NULL, 209);
INSERT INTO sgnom.tsgnomconfpago (cod_confpagoid, bol_pagoempleado, bol_pagorh, bol_pagofinanzas, cod_empquincenaid_fk) VALUES (118, NULL, NULL, NULL, 210);
INSERT INTO sgnom.tsgnomconfpago (cod_confpagoid, bol_pagoempleado, bol_pagorh, bol_pagofinanzas, cod_empquincenaid_fk) VALUES (119, NULL, NULL, NULL, 211);
INSERT INTO sgnom.tsgnomconfpago (cod_confpagoid, bol_pagoempleado, bol_pagorh, bol_pagofinanzas, cod_empquincenaid_fk) VALUES (120, NULL, NULL, NULL, 212);
INSERT INTO sgnom.tsgnomconfpago (cod_confpagoid, bol_pagoempleado, bol_pagorh, bol_pagofinanzas, cod_empquincenaid_fk) VALUES (121, NULL, NULL, NULL, 213);
INSERT INTO sgnom.tsgnomconfpago (cod_confpagoid, bol_pagoempleado, bol_pagorh, bol_pagofinanzas, cod_empquincenaid_fk) VALUES (122, NULL, NULL, NULL, 214);
INSERT INTO sgnom.tsgnomconfpago (cod_confpagoid, bol_pagoempleado, bol_pagorh, bol_pagofinanzas, cod_empquincenaid_fk) VALUES (123, NULL, NULL, NULL, 215);
INSERT INTO sgnom.tsgnomconfpago (cod_confpagoid, bol_pagoempleado, bol_pagorh, bol_pagofinanzas, cod_empquincenaid_fk) VALUES (124, NULL, NULL, NULL, 216);
INSERT INTO sgnom.tsgnomconfpago (cod_confpagoid, bol_pagoempleado, bol_pagorh, bol_pagofinanzas, cod_empquincenaid_fk) VALUES (125, NULL, NULL, NULL, 217);
INSERT INTO sgnom.tsgnomconfpago (cod_confpagoid, bol_pagoempleado, bol_pagorh, bol_pagofinanzas, cod_empquincenaid_fk) VALUES (126, NULL, NULL, NULL, 218);
INSERT INTO sgnom.tsgnomconfpago (cod_confpagoid, bol_pagoempleado, bol_pagorh, bol_pagofinanzas, cod_empquincenaid_fk) VALUES (127, NULL, NULL, NULL, 219);
INSERT INTO sgnom.tsgnomconfpago (cod_confpagoid, bol_pagoempleado, bol_pagorh, bol_pagofinanzas, cod_empquincenaid_fk) VALUES (128, NULL, NULL, NULL, 220);
INSERT INTO sgnom.tsgnomconfpago (cod_confpagoid, bol_pagoempleado, bol_pagorh, bol_pagofinanzas, cod_empquincenaid_fk) VALUES (129, NULL, NULL, NULL, 221);
INSERT INTO sgnom.tsgnomconfpago (cod_confpagoid, bol_pagoempleado, bol_pagorh, bol_pagofinanzas, cod_empquincenaid_fk) VALUES (130, NULL, NULL, NULL, 222);
INSERT INTO sgnom.tsgnomconfpago (cod_confpagoid, bol_pagoempleado, bol_pagorh, bol_pagofinanzas, cod_empquincenaid_fk) VALUES (131, NULL, NULL, NULL, 223);
INSERT INTO sgnom.tsgnomconfpago (cod_confpagoid, bol_pagoempleado, bol_pagorh, bol_pagofinanzas, cod_empquincenaid_fk) VALUES (132, NULL, NULL, NULL, 224);
INSERT INTO sgnom.tsgnomconfpago (cod_confpagoid, bol_pagoempleado, bol_pagorh, bol_pagofinanzas, cod_empquincenaid_fk) VALUES (133, NULL, NULL, NULL, 225);


--
-- TOC entry 3754 (class 0 OID 97274)
-- Dependencies: 376
-- Data for Name: tsgnomejercicio; Type: TABLE DATA; Schema: sgnom; Owner: suite
--

INSERT INTO sgnom.tsgnomejercicio (cod_ejercicioid, cnu_valorejercicio, bol_estatus) VALUES (1, 2014, true);
INSERT INTO sgnom.tsgnomejercicio (cod_ejercicioid, cnu_valorejercicio, bol_estatus) VALUES (2, 2015, true);
INSERT INTO sgnom.tsgnomejercicio (cod_ejercicioid, cnu_valorejercicio, bol_estatus) VALUES (3, 2016, true);
INSERT INTO sgnom.tsgnomejercicio (cod_ejercicioid, cnu_valorejercicio, bol_estatus) VALUES (4, 2017, true);
INSERT INTO sgnom.tsgnomejercicio (cod_ejercicioid, cnu_valorejercicio, bol_estatus) VALUES (5, 2018, true);
INSERT INTO sgnom.tsgnomejercicio (cod_ejercicioid, cnu_valorejercicio, bol_estatus) VALUES (6, 2019, true);
INSERT INTO sgnom.tsgnomejercicio (cod_ejercicioid, cnu_valorejercicio, bol_estatus) VALUES (7, 2020, true);
INSERT INTO sgnom.tsgnomejercicio (cod_ejercicioid, cnu_valorejercicio, bol_estatus) VALUES (8, 2021, true);


--
-- TOC entry 3755 (class 0 OID 97277)
-- Dependencies: 377
-- Data for Name: tsgnomempleados; Type: TABLE DATA; Schema: sgnom; Owner: suite
--

INSERT INTO sgnom.tsgnomempleados (cod_empleadoid, fec_ingreso, fec_salida, bol_estatus, cod_empleado_fk, imp_sueldoimss, imp_honorarios, imp_finiquito, cod_tipoimss, cod_tipohonorarios, cod_banco, cod_sucursal, cod_cuenta, txt_descripcionbaja, aud_codcreadopor, aud_feccreacion, aud_codmodificadopor, aud_fecmodificacion, cod_clabe, des_validacion, cod_validaciones, cod_bancoh, cod_sucursalh, cod_cuentah, cod_clabeh) VALUES (11, '2019-05-07', '2019-08-10', false, 11, 123.00, 8200.00, 123.00, '1', 'o', '123', 321, '12345', 'baja', 1, '2190-12-21', 1, '2019-09-30', '12345', true, 'a', NULL, NULL, NULL, NULL);
INSERT INTO sgnom.tsgnomempleados (cod_empleadoid, fec_ingreso, fec_salida, bol_estatus, cod_empleado_fk, imp_sueldoimss, imp_honorarios, imp_finiquito, cod_tipoimss, cod_tipohonorarios, cod_banco, cod_sucursal, cod_cuenta, txt_descripcionbaja, aud_codcreadopor, aud_feccreacion, aud_codmodificadopor, aud_fecmodificacion, cod_clabe, des_validacion, cod_validaciones, cod_bancoh, cod_sucursalh, cod_cuentah, cod_clabeh) VALUES (12, '2019-05-07', NULL, true, 12, 123.00, 213.00, 123.00, '1', 'o', '123', 321, '12345', 'baja', 1, '2190-12-21', 1, '2019-09-30', '12345', true, NULL, NULL, NULL, NULL, NULL);
INSERT INTO sgnom.tsgnomempleados (cod_empleadoid, fec_ingreso, fec_salida, bol_estatus, cod_empleado_fk, imp_sueldoimss, imp_honorarios, imp_finiquito, cod_tipoimss, cod_tipohonorarios, cod_banco, cod_sucursal, cod_cuenta, txt_descripcionbaja, aud_codcreadopor, aud_feccreacion, aud_codmodificadopor, aud_fecmodificacion, cod_clabe, des_validacion, cod_validaciones, cod_bancoh, cod_sucursalh, cod_cuentah, cod_clabeh) VALUES (13, '2019-05-07', '2020-05-10', true, 13, 123.00, 213.00, 123.00, '1', 'o', '123', 321, '12345', 'baja', 1, '2190-12-21', 1, '2019-09-30', '12345', true, NULL, NULL, NULL, NULL, NULL);
INSERT INTO sgnom.tsgnomempleados (cod_empleadoid, fec_ingreso, fec_salida, bol_estatus, cod_empleado_fk, imp_sueldoimss, imp_honorarios, imp_finiquito, cod_tipoimss, cod_tipohonorarios, cod_banco, cod_sucursal, cod_cuenta, txt_descripcionbaja, aud_codcreadopor, aud_feccreacion, aud_codmodificadopor, aud_fecmodificacion, cod_clabe, des_validacion, cod_validaciones, cod_bancoh, cod_sucursalh, cod_cuentah, cod_clabeh) VALUES (14, '2019-05-07', NULL, true, 14, 123.00, 213.00, 123.00, '1', 'o', '123', 321, '12345', 'baja', 1, '2190-12-21', 1, '2019-09-30', '12345', true, NULL, NULL, NULL, NULL, NULL);
INSERT INTO sgnom.tsgnomempleados (cod_empleadoid, fec_ingreso, fec_salida, bol_estatus, cod_empleado_fk, imp_sueldoimss, imp_honorarios, imp_finiquito, cod_tipoimss, cod_tipohonorarios, cod_banco, cod_sucursal, cod_cuenta, txt_descripcionbaja, aud_codcreadopor, aud_feccreacion, aud_codmodificadopor, aud_fecmodificacion, cod_clabe, des_validacion, cod_validaciones, cod_bancoh, cod_sucursalh, cod_cuentah, cod_clabeh) VALUES (15, '2019-05-07', '2019-05-10', true, 15, 123.00, 1000.00, 123.00, '1', 'o', '123', 321, '12345', 'baja', 1, '2190-12-21', 1, '2019-09-30', '12345', true, NULL, NULL, NULL, NULL, NULL);
INSERT INTO sgnom.tsgnomempleados (cod_empleadoid, fec_ingreso, fec_salida, bol_estatus, cod_empleado_fk, imp_sueldoimss, imp_honorarios, imp_finiquito, cod_tipoimss, cod_tipohonorarios, cod_banco, cod_sucursal, cod_cuenta, txt_descripcionbaja, aud_codcreadopor, aud_feccreacion, aud_codmodificadopor, aud_fecmodificacion, cod_clabe, des_validacion, cod_validaciones, cod_bancoh, cod_sucursalh, cod_cuentah, cod_clabeh) VALUES (16, '2019-05-07', NULL, true, 16, 123.00, 213.00, 123.00, '1', 'o', '123', 321, '12345', 'baja', 1, '2190-12-21', 1, '2019-09-30', '12345', true, NULL, NULL, NULL, NULL, NULL);
INSERT INTO sgnom.tsgnomempleados (cod_empleadoid, fec_ingreso, fec_salida, bol_estatus, cod_empleado_fk, imp_sueldoimss, imp_honorarios, imp_finiquito, cod_tipoimss, cod_tipohonorarios, cod_banco, cod_sucursal, cod_cuenta, txt_descripcionbaja, aud_codcreadopor, aud_feccreacion, aud_codmodificadopor, aud_fecmodificacion, cod_clabe, des_validacion, cod_validaciones, cod_bancoh, cod_sucursalh, cod_cuentah, cod_clabeh) VALUES (21, '2019-05-07', '2019-05-10', true, 21, 123.00, 2000.00, 123.00, '1', '0', '123', 321, '12345', 'baja', 1, '2190-12-21', 1, '2019-09-30', '12345', true, 'a', NULL, NULL, NULL, NULL);
INSERT INTO sgnom.tsgnomempleados (cod_empleadoid, fec_ingreso, fec_salida, bol_estatus, cod_empleado_fk, imp_sueldoimss, imp_honorarios, imp_finiquito, cod_tipoimss, cod_tipohonorarios, cod_banco, cod_sucursal, cod_cuenta, txt_descripcionbaja, aud_codcreadopor, aud_feccreacion, aud_codmodificadopor, aud_fecmodificacion, cod_clabe, des_validacion, cod_validaciones, cod_bancoh, cod_sucursalh, cod_cuentah, cod_clabeh) VALUES (19, '2019-05-07', '2019-07-12', true, 19, 10000.00, 1000.00, 123.00, '1', '0', '123', 321, '12345', 'baja', 1, '2190-12-21', 1, '2019-09-30', '12345', false, 'b', NULL, NULL, NULL, NULL);
INSERT INTO sgnom.tsgnomempleados (cod_empleadoid, fec_ingreso, fec_salida, bol_estatus, cod_empleado_fk, imp_sueldoimss, imp_honorarios, imp_finiquito, cod_tipoimss, cod_tipohonorarios, cod_banco, cod_sucursal, cod_cuenta, txt_descripcionbaja, aud_codcreadopor, aud_feccreacion, aud_codmodificadopor, aud_fecmodificacion, cod_clabe, des_validacion, cod_validaciones, cod_bancoh, cod_sucursalh, cod_cuentah, cod_clabeh) VALUES (17, '2019-05-07', '2019-05-10', true, 17, 123.00, 2213.00, 123.00, '1', '0', '123', 321, '12345', 'baja', 1, '2190-12-21', 1, '2019-09-30', '12345', false, 'b', NULL, NULL, NULL, NULL);
INSERT INTO sgnom.tsgnomempleados (cod_empleadoid, fec_ingreso, fec_salida, bol_estatus, cod_empleado_fk, imp_sueldoimss, imp_honorarios, imp_finiquito, cod_tipoimss, cod_tipohonorarios, cod_banco, cod_sucursal, cod_cuenta, txt_descripcionbaja, aud_codcreadopor, aud_feccreacion, aud_codmodificadopor, aud_fecmodificacion, cod_clabe, des_validacion, cod_validaciones, cod_bancoh, cod_sucursalh, cod_cuentah, cod_clabeh) VALUES (18, '2019-05-07', '2019-06-10', false, 18, 123.00, 213.00, 123.00, '1', '0', '123', 321, '12345', 'baja', 1, '2190-12-21', 1, '2019-11-01', '12345', true, 'd', NULL, NULL, NULL, NULL);
INSERT INTO sgnom.tsgnomempleados (cod_empleadoid, fec_ingreso, fec_salida, bol_estatus, cod_empleado_fk, imp_sueldoimss, imp_honorarios, imp_finiquito, cod_tipoimss, cod_tipohonorarios, cod_banco, cod_sucursal, cod_cuenta, txt_descripcionbaja, aud_codcreadopor, aud_feccreacion, aud_codmodificadopor, aud_fecmodificacion, cod_clabe, des_validacion, cod_validaciones, cod_bancoh, cod_sucursalh, cod_cuentah, cod_clabeh) VALUES (1, '2019-05-07', NULL, true, 1, 12333.00, 5000.00, 123.00, '1', '1', '123', 321, '12345', 'baja', 1, '2190-12-21', 1, '2019-09-30', '12345', true, 'a', NULL, NULL, NULL, NULL);
INSERT INTO sgnom.tsgnomempleados (cod_empleadoid, fec_ingreso, fec_salida, bol_estatus, cod_empleado_fk, imp_sueldoimss, imp_honorarios, imp_finiquito, cod_tipoimss, cod_tipohonorarios, cod_banco, cod_sucursal, cod_cuenta, txt_descripcionbaja, aud_codcreadopor, aud_feccreacion, aud_codmodificadopor, aud_fecmodificacion, cod_clabe, des_validacion, cod_validaciones, cod_bancoh, cod_sucursalh, cod_cuentah, cod_clabeh) VALUES (20, '2019-05-07', '2019-07-15', true, 20, 123.00, 2000.00, 123.00, '0', '0', '123', 321, '12345', 'baja', 1, '2190-12-21', 1, '2019-09-30', '12345', true, 'a', NULL, NULL, NULL, NULL);
INSERT INTO sgnom.tsgnomempleados (cod_empleadoid, fec_ingreso, fec_salida, bol_estatus, cod_empleado_fk, imp_sueldoimss, imp_honorarios, imp_finiquito, cod_tipoimss, cod_tipohonorarios, cod_banco, cod_sucursal, cod_cuenta, txt_descripcionbaja, aud_codcreadopor, aud_feccreacion, aud_codmodificadopor, aud_fecmodificacion, cod_clabe, des_validacion, cod_validaciones, cod_bancoh, cod_sucursalh, cod_cuentah, cod_clabeh) VALUES (56, '2019-11-13', NULL, true, 34, 7.00, 9.00, NULL, '0', '0', NULL, NULL, NULL, NULL, 1, '2019-11-12', NULL, '2019-11-12', NULL, true, 'a', NULL, NULL, NULL, NULL);
INSERT INTO sgnom.tsgnomempleados (cod_empleadoid, fec_ingreso, fec_salida, bol_estatus, cod_empleado_fk, imp_sueldoimss, imp_honorarios, imp_finiquito, cod_tipoimss, cod_tipohonorarios, cod_banco, cod_sucursal, cod_cuenta, txt_descripcionbaja, aud_codcreadopor, aud_feccreacion, aud_codmodificadopor, aud_fecmodificacion, cod_clabe, des_validacion, cod_validaciones, cod_bancoh, cod_sucursalh, cod_cuentah, cod_clabeh) VALUES (58, '2019-11-16', NULL, true, 34, 3745.00, 3500.00, NULL, '0', '0', 'HSBC', 94, '1025311523', NULL, 1, '2019-11-12', NULL, '2019-11-12', '012345678901234567', true, 'a', NULL, NULL, NULL, NULL);
INSERT INTO sgnom.tsgnomempleados (cod_empleadoid, fec_ingreso, fec_salida, bol_estatus, cod_empleado_fk, imp_sueldoimss, imp_honorarios, imp_finiquito, cod_tipoimss, cod_tipohonorarios, cod_banco, cod_sucursal, cod_cuenta, txt_descripcionbaja, aud_codcreadopor, aud_feccreacion, aud_codmodificadopor, aud_fecmodificacion, cod_clabe, des_validacion, cod_validaciones, cod_bancoh, cod_sucursalh, cod_cuentah, cod_clabeh) VALUES (10, '2019-05-07', '2019-07-10', false, 10, 1.00, 520000.00, 123.00, '1', '0', '123', 321, '12345', 'baja', 1, '2190-12-21', 1, '2019-11-13', '12345', true, 'd', NULL, NULL, NULL, NULL);
INSERT INTO sgnom.tsgnomempleados (cod_empleadoid, fec_ingreso, fec_salida, bol_estatus, cod_empleado_fk, imp_sueldoimss, imp_honorarios, imp_finiquito, cod_tipoimss, cod_tipohonorarios, cod_banco, cod_sucursal, cod_cuenta, txt_descripcionbaja, aud_codcreadopor, aud_feccreacion, aud_codmodificadopor, aud_fecmodificacion, cod_clabe, des_validacion, cod_validaciones, cod_bancoh, cod_sucursalh, cod_cuentah, cod_clabeh) VALUES (60, '2019-05-07', NULL, true, 34, 43.00, 344.00, NULL, '0', '1', NULL, NULL, NULL, NULL, 1, '2019-11-13', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);


--
-- TOC entry 3756 (class 0 OID 97283)
-- Dependencies: 378
-- Data for Name: tsgnomempquincena; Type: TABLE DATA; Schema: sgnom; Owner: suite
--

INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (62, 1, 17, 0.00, 0.00, 0.00, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (63, 12, 17, 0.00, 0.00, 0.00, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (64, 13, 17, 63.90, 0.00, 63.90, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (65, 14, 17, 0.00, 0.00, 0.00, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (66, 16, 17, 0.00, 0.00, 0.00, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (67, 1, 18, 0.00, 0.00, 0.00, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (68, 12, 18, 0.00, 0.00, 0.00, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (69, 13, 18, 0.00, 0.00, 0.00, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (70, 14, 18, 0.00, 0.00, 0.00, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (71, 16, 18, 0.00, 0.00, 0.00, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (72, 1, 19, 0.00, 0.00, 0.00, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (73, 12, 19, 0.00, 0.00, 0.00, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (74, 13, 19, 0.00, 0.00, 0.00, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (75, 14, 19, 0.00, 0.00, 0.00, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (76, 16, 19, 0.00, 0.00, 0.00, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (77, 1, 20, 0.00, 0.00, 0.00, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (78, 12, 20, 0.00, 0.00, 0.00, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (79, 13, 20, 0.00, 0.00, 0.00, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (80, 14, 20, 0.00, 0.00, 0.00, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (81, 16, 20, 0.00, 0.00, 0.00, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (82, 1, 21, 0.00, 0.00, 0.00, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (83, 12, 21, 0.00, 0.00, 0.00, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (84, 13, 21, 0.00, 0.00, 0.00, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (85, 14, 21, 0.00, 0.00, 0.00, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (86, 16, 21, 0.00, 0.00, 0.00, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (87, 1, 22, 0.00, 0.00, 0.00, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (88, 12, 22, 0.00, 0.00, 0.00, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (89, 13, 22, 0.00, 0.00, 0.00, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (90, 14, 22, 0.00, 0.00, 0.00, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (91, 16, 22, 0.00, 0.00, 0.00, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (92, 1, 23, 0.00, 0.00, 0.00, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (93, 12, 23, 0.00, 0.00, 0.00, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (94, 13, 23, 0.00, 0.00, 0.00, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (95, 14, 23, 0.00, 0.00, 0.00, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (96, 16, 23, 0.00, 0.00, 0.00, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (97, 56, 23, 0.00, 0.00, 0.00, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (146, 1, 30, 2500.00, 0.00, 2500.00, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (147, 12, 30, 106.50, 0.00, 106.50, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (148, 13, 30, 106.50, 0.00, 106.50, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (149, 14, 30, 106.50, 0.00, 106.50, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (150, 16, 30, 106.50, 0.00, 106.50, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (151, 56, 30, 4.50, 0.00, 4.50, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (152, 58, 30, 1750.00, 0.00, 1750.00, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (153, 60, 30, 172.00, 0.00, 172.00, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (154, 1, 31, 0.00, 0.00, 0.00, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (155, 12, 31, 0.00, 0.00, 0.00, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (156, 13, 31, 0.00, 0.00, 0.00, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (157, 14, 31, 0.00, 0.00, 0.00, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (158, 16, 31, 0.00, 0.00, 0.00, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (159, 56, 31, 0.00, 0.00, 0.00, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (160, 58, 31, 0.00, 0.00, 0.00, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (161, 60, 31, 0.00, 0.00, 0.00, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (162, 1, 32, 0.00, 0.00, 0.00, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (163, 12, 32, 0.00, 0.00, 0.00, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (164, 13, 32, 0.00, 0.00, 0.00, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (165, 14, 32, 0.00, 0.00, 0.00, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (166, 16, 32, 0.00, 0.00, 0.00, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (167, 56, 32, 0.00, 0.00, 0.00, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (168, 58, 32, 0.00, 0.00, 0.00, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (169, 60, 32, 0.00, 0.00, 0.00, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (170, 1, 33, 0.00, 0.00, 0.00, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (171, 12, 33, 0.00, 0.00, 0.00, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (172, 13, 33, 0.00, 0.00, 0.00, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (173, 14, 33, 0.00, 0.00, 0.00, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (174, 16, 33, 0.00, 0.00, 0.00, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (175, 56, 33, 0.00, 0.00, 0.00, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (176, 58, 33, 0.00, 0.00, 0.00, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (177, 60, 33, 0.00, 0.00, 0.00, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (178, 1, 34, 0.00, 0.00, 0.00, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (179, 12, 34, 0.00, 0.00, 0.00, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (180, 13, 34, 0.00, 0.00, 0.00, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (181, 14, 34, 0.00, 0.00, 0.00, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (182, 16, 34, 0.00, 0.00, 0.00, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (183, 56, 34, 0.00, 0.00, 0.00, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (184, 58, 34, 0.00, 0.00, 0.00, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (185, 60, 34, 0.00, 0.00, 0.00, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (186, 1, 35, 0.00, 0.00, 0.00, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (187, 12, 35, 0.00, 0.00, 0.00, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (188, 13, 35, 0.00, 0.00, 0.00, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (189, 14, 35, 0.00, 0.00, 0.00, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (190, 16, 35, 0.00, 0.00, 0.00, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (191, 56, 35, 0.00, 0.00, 0.00, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (192, 58, 35, 0.00, 0.00, 0.00, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (193, 60, 35, 0.00, 0.00, 0.00, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (194, 1, 36, 0.00, 0.00, 0.00, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (195, 12, 36, 0.00, 0.00, 0.00, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (196, 13, 36, 0.00, 0.00, 0.00, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (197, 14, 36, 0.00, 0.00, 0.00, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (198, 16, 36, 0.00, 0.00, 0.00, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (199, 56, 36, 0.00, 0.00, 0.00, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (200, 58, 36, 0.00, 0.00, 0.00, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (201, 60, 36, 0.00, 0.00, 0.00, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (210, 1, 38, 0.00, 0.00, 0.00, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (211, 12, 38, 0.00, 0.00, 0.00, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (212, 13, 38, 0.00, 0.00, 0.00, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (213, 14, 38, 0.00, 0.00, 0.00, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (214, 16, 38, 0.00, 0.00, 0.00, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (215, 56, 38, 0.00, 0.00, 0.00, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (216, 58, 38, 0.00, 0.00, 0.00, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (217, 60, 38, 0.00, 0.00, 0.00, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (218, 1, 39, 0.00, 0.00, 0.00, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (219, 12, 39, 0.00, 0.00, 0.00, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (220, 13, 39, 0.00, 0.00, 0.00, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (221, 14, 39, 0.00, 0.00, 0.00, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (222, 16, 39, 0.00, 0.00, 0.00, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (223, 56, 39, 0.00, 0.00, 0.00, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (224, 58, 39, 0.00, 0.00, 0.00, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (225, 60, 39, 0.00, 0.00, 0.00, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (202, 1, 37, 2500.00, 0.00, 2500.00, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (203, 12, 37, 106.50, 0.00, 106.50, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (204, 13, 37, 106.50, 0.00, 106.50, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (205, 14, 37, 106.50, 0.00, 106.50, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (206, 16, 37, 106.50, 0.00, 106.50, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (207, 56, 37, 4.50, 0.00, 4.50, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (208, 58, 37, 1750.00, 0.00, 1750.00, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (209, 60, 37, 172.00, 0.00, 172.00, true);


--
-- TOC entry 3757 (class 0 OID 97287)
-- Dependencies: 379
-- Data for Name: tsgnomempquincenaht; Type: TABLE DATA; Schema: sgnom; Owner: suite
--



--
-- TOC entry 3758 (class 0 OID 97290)
-- Dependencies: 380
-- Data for Name: tsgnomestatusnom; Type: TABLE DATA; Schema: sgnom; Owner: suite
--

INSERT INTO sgnom.tsgnomestatusnom (cod_estatusnomid, cod_estatusnomina, bol_estatus) VALUES (2, 'calculada', true);
INSERT INTO sgnom.tsgnomestatusnom (cod_estatusnomid, cod_estatusnomina, bol_estatus) VALUES (1, 'abierta', true);
INSERT INTO sgnom.tsgnomestatusnom (cod_estatusnomid, cod_estatusnomina, bol_estatus) VALUES (3, 'revision', true);
INSERT INTO sgnom.tsgnomestatusnom (cod_estatusnomid, cod_estatusnomina, bol_estatus) VALUES (4, 'validada', true);
INSERT INTO sgnom.tsgnomestatusnom (cod_estatusnomid, cod_estatusnomina, bol_estatus) VALUES (5, 'cerrada', true);
INSERT INTO sgnom.tsgnomestatusnom (cod_estatusnomid, cod_estatusnomina, bol_estatus) VALUES (6, 'eliminada', true);


--
-- TOC entry 3759 (class 0 OID 97293)
-- Dependencies: 381
-- Data for Name: tsgnomformula; Type: TABLE DATA; Schema: sgnom; Owner: suite
--

INSERT INTO sgnom.tsgnomformula (cod_formulaid, des_nbformula, des_formula, bol_estatus) VALUES (9, 'SUELDO QUINCENA', ':(:SUELDOBASE:/:DIASDELMES:):*:DIASLABORADOS', true);
INSERT INTO sgnom.tsgnomformula (cod_formulaid, des_nbformula, des_formula, bol_estatus) VALUES (11, 'YY', ':DIASDELMES:+:SUELDOBASE', true);
INSERT INTO sgnom.tsgnomformula (cod_formulaid, des_nbformula, des_formula, bol_estatus) VALUES (12, 'HHHHH', ':DIASDELMES', true);
INSERT INTO sgnom.tsgnomformula (cod_formulaid, des_nbformula, des_formula, bol_estatus) VALUES (13, 'DGSDG', ':DIASDELMES:*:SUELDOBASE', true);
INSERT INTO sgnom.tsgnomformula (cod_formulaid, des_nbformula, des_formula, bol_estatus) VALUES (10, 'QW', ':(:SUELDOBASE:/:con proyecto:):*:DIASLABORADOS', true);


--
-- TOC entry 3760 (class 0 OID 97296)
-- Dependencies: 382
-- Data for Name: tsgnomfuncion; Type: TABLE DATA; Schema: sgnom; Owner: suite
--

INSERT INTO sgnom.tsgnomfuncion (cod_funcionid, cod_nbfuncion, bol_estatus) VALUES (1, 'fn_sueldo_base', true);
INSERT INTO sgnom.tsgnomfuncion (cod_funcionid, cod_nbfuncion, bol_estatus) VALUES (2, 'fn_dias_laborad', true);


--
-- TOC entry 3761 (class 0 OID 97299)
-- Dependencies: 383
-- Data for Name: tsgnomhisttabla; Type: TABLE DATA; Schema: sgnom; Owner: suite
--



--
-- TOC entry 3762 (class 0 OID 97302)
-- Dependencies: 384
-- Data for Name: tsgnomincidencia; Type: TABLE DATA; Schema: sgnom; Owner: suite
--

INSERT INTO sgnom.tsgnomincidencia (cod_incidenciaid, cod_catincidenciaid_fk, cnu_cantidad, des_actividad, txt_comentarios, cod_empreporta_fk, cod_empautoriza_fk, imp_monto, xml_detcantidad, bol_estatus, cod_quincenaid_fk, bol_validacion, fec_validacion, aud_codcreadopor, aud_feccreacion, aud_codmodificadopor, aud_fecmodificacion, bol_aceptacion, bol_pago) VALUES (41, 4, NULL, NULL, NULL, 13, NULL, NULL, '                
		<DetalleFechas><fecha>2019-08-20</fecha><fecha>2019-08-20</fecha><fecha>2019-08-21</fecha></DetalleFechas>', false, 7, NULL, NULL, 13, '2019-09-02', 13, '2019-09-04', NULL, NULL);
INSERT INTO sgnom.tsgnomincidencia (cod_incidenciaid, cod_catincidenciaid_fk, cnu_cantidad, des_actividad, txt_comentarios, cod_empreporta_fk, cod_empautoriza_fk, imp_monto, xml_detcantidad, bol_estatus, cod_quincenaid_fk, bol_validacion, fec_validacion, aud_codcreadopor, aud_feccreacion, aud_codmodificadopor, aud_fecmodificacion, bol_aceptacion, bol_pago) VALUES (1, 7, 1, NULL, 'CURSO ALUMNOS', 13, NULL, NULL, NULL, false, 7, NULL, NULL, 16, '2019-05-25', 16, '2019-07-16', NULL, NULL);
INSERT INTO sgnom.tsgnomincidencia (cod_incidenciaid, cod_catincidenciaid_fk, cnu_cantidad, des_actividad, txt_comentarios, cod_empreporta_fk, cod_empautoriza_fk, imp_monto, xml_detcantidad, bol_estatus, cod_quincenaid_fk, bol_validacion, fec_validacion, aud_codcreadopor, aud_feccreacion, aud_codmodificadopor, aud_fecmodificacion, bol_aceptacion, bol_pago) VALUES (4, 6, 0, '', '', 13, NULL, NULL, NULL, false, 7, NULL, NULL, 16, '2019-07-17', NULL, NULL, NULL, NULL);
INSERT INTO sgnom.tsgnomincidencia (cod_incidenciaid, cod_catincidenciaid_fk, cnu_cantidad, des_actividad, txt_comentarios, cod_empreporta_fk, cod_empautoriza_fk, imp_monto, xml_detcantidad, bol_estatus, cod_quincenaid_fk, bol_validacion, fec_validacion, aud_codcreadopor, aud_feccreacion, aud_codmodificadopor, aud_fecmodificacion, bol_aceptacion, bol_pago) VALUES (5, 9, 0, '', '', 13, NULL, NULL, NULL, false, 7, NULL, NULL, 16, '2019-07-17', NULL, NULL, NULL, NULL);
INSERT INTO sgnom.tsgnomincidencia (cod_incidenciaid, cod_catincidenciaid_fk, cnu_cantidad, des_actividad, txt_comentarios, cod_empreporta_fk, cod_empautoriza_fk, imp_monto, xml_detcantidad, bol_estatus, cod_quincenaid_fk, bol_validacion, fec_validacion, aud_codcreadopor, aud_feccreacion, aud_codmodificadopor, aud_fecmodificacion, bol_aceptacion, bol_pago) VALUES (6, 6, 0, '', '', 13, NULL, NULL, NULL, false, 7, false, NULL, 16, '2019-07-17', 16, '2019-08-22', NULL, NULL);
INSERT INTO sgnom.tsgnomincidencia (cod_incidenciaid, cod_catincidenciaid_fk, cnu_cantidad, des_actividad, txt_comentarios, cod_empreporta_fk, cod_empautoriza_fk, imp_monto, xml_detcantidad, bol_estatus, cod_quincenaid_fk, bol_validacion, fec_validacion, aud_codcreadopor, aud_feccreacion, aud_codmodificadopor, aud_fecmodificacion, bol_aceptacion, bol_pago) VALUES (10, 7, 1, NULL, 'curso 5 alumnos', 13, NULL, NULL, NULL, false, 7, true, NULL, 17, '2019-05-25', 13, '2019-08-26', NULL, NULL);
INSERT INTO sgnom.tsgnomincidencia (cod_incidenciaid, cod_catincidenciaid_fk, cnu_cantidad, des_actividad, txt_comentarios, cod_empreporta_fk, cod_empautoriza_fk, imp_monto, xml_detcantidad, bol_estatus, cod_quincenaid_fk, bol_validacion, fec_validacion, aud_codcreadopor, aud_feccreacion, aud_codmodificadopor, aud_fecmodificacion, bol_aceptacion, bol_pago) VALUES (11, 7, 1, NULL, 'curso 12 alumnos', 13, NULL, NULL, NULL, false, 7, true, NULL, 17, '2019-05-25', 13, '2019-08-26', NULL, NULL);
INSERT INTO sgnom.tsgnomincidencia (cod_incidenciaid, cod_catincidenciaid_fk, cnu_cantidad, des_actividad, txt_comentarios, cod_empreporta_fk, cod_empautoriza_fk, imp_monto, xml_detcantidad, bol_estatus, cod_quincenaid_fk, bol_validacion, fec_validacion, aud_codcreadopor, aud_feccreacion, aud_codmodificadopor, aud_fecmodificacion, bol_aceptacion, bol_pago) VALUES (16, 10, 0, '3X3 CURSO', 'integrar', 13, 12, NULL, NULL, true, 7, false, NULL, 13, '2019-08-26', NULL, NULL, NULL, NULL);
INSERT INTO sgnom.tsgnomincidencia (cod_incidenciaid, cod_catincidenciaid_fk, cnu_cantidad, des_actividad, txt_comentarios, cod_empreporta_fk, cod_empautoriza_fk, imp_monto, xml_detcantidad, bol_estatus, cod_quincenaid_fk, bol_validacion, fec_validacion, aud_codcreadopor, aud_feccreacion, aud_codmodificadopor, aud_fecmodificacion, bol_aceptacion, bol_pago) VALUES (31, 11, 7, '', '', 13, NULL, NULL, NULL, true, 7, false, NULL, 13, '2019-08-27', NULL, NULL, NULL, NULL);
INSERT INTO sgnom.tsgnomincidencia (cod_incidenciaid, cod_catincidenciaid_fk, cnu_cantidad, des_actividad, txt_comentarios, cod_empreporta_fk, cod_empautoriza_fk, imp_monto, xml_detcantidad, bol_estatus, cod_quincenaid_fk, bol_validacion, fec_validacion, aud_codcreadopor, aud_feccreacion, aud_codmodificadopor, aud_fecmodificacion, bol_aceptacion, bol_pago) VALUES (46, 4, 1, '', 'ejemplo', 13, NULL, 0.00, '                
		<DetalleFechas><fecha>2019-08-18</fecha><fecha>2019-08-19</fecha><fecha>2019-08-20</fecha></DetalleFechas>', false, 7, NULL, NULL, 13, '2019-09-06', 13, '2019-09-09', NULL, NULL);
INSERT INTO sgnom.tsgnomincidencia (cod_incidenciaid, cod_catincidenciaid_fk, cnu_cantidad, des_actividad, txt_comentarios, cod_empreporta_fk, cod_empautoriza_fk, imp_monto, xml_detcantidad, bol_estatus, cod_quincenaid_fk, bol_validacion, fec_validacion, aud_codcreadopor, aud_feccreacion, aud_codmodificadopor, aud_fecmodificacion, bol_aceptacion, bol_pago) VALUES (20, 1, 1, '', 'agosto horas extra', 13, NULL, NULL, NULL, true, 7, false, NULL, 13, '2019-08-26', NULL, NULL, NULL, NULL);
INSERT INTO sgnom.tsgnomincidencia (cod_incidenciaid, cod_catincidenciaid_fk, cnu_cantidad, des_actividad, txt_comentarios, cod_empreporta_fk, cod_empautoriza_fk, imp_monto, xml_detcantidad, bol_estatus, cod_quincenaid_fk, bol_validacion, fec_validacion, aud_codcreadopor, aud_feccreacion, aud_codmodificadopor, aud_fecmodificacion, bol_aceptacion, bol_pago) VALUES (45, 4, 1, '', 'ejemplo', 13, NULL, 0.00, '                
		<DetalleFechas><fecha>2019-08-18</fecha><fecha>2019-08-19</fecha><fecha>2019-08-20</fecha></DetalleFechas>', false, 18, true, NULL, 13, '2019-09-06', 13, '2019-09-30', NULL, NULL);
INSERT INTO sgnom.tsgnomincidencia (cod_incidenciaid, cod_catincidenciaid_fk, cnu_cantidad, des_actividad, txt_comentarios, cod_empreporta_fk, cod_empautoriza_fk, imp_monto, xml_detcantidad, bol_estatus, cod_quincenaid_fk, bol_validacion, fec_validacion, aud_codcreadopor, aud_feccreacion, aud_codmodificadopor, aud_fecmodificacion, bol_aceptacion, bol_pago) VALUES (36, 4, 1, '', 'ejemplo', 13, NULL, 0.00, '                
		<DetalleFechas><fecha>2019-08-18</fecha><fecha>2019-08-19</fecha><fecha>2019-08-20</fecha></DetalleFechas>', false, 18, false, NULL, 13, '2019-08-28', 13, '2019-09-30', NULL, NULL);
INSERT INTO sgnom.tsgnomincidencia (cod_incidenciaid, cod_catincidenciaid_fk, cnu_cantidad, des_actividad, txt_comentarios, cod_empreporta_fk, cod_empautoriza_fk, imp_monto, xml_detcantidad, bol_estatus, cod_quincenaid_fk, bol_validacion, fec_validacion, aud_codcreadopor, aud_feccreacion, aud_codmodificadopor, aud_fecmodificacion, bol_aceptacion, bol_pago) VALUES (8, 6, 2, NULL, 'EXTRAS', 13, NULL, NULL, NULL, true, 18, true, NULL, 16, '2019-05-25', NULL, NULL, NULL, NULL);
INSERT INTO sgnom.tsgnomincidencia (cod_incidenciaid, cod_catincidenciaid_fk, cnu_cantidad, des_actividad, txt_comentarios, cod_empreporta_fk, cod_empautoriza_fk, imp_monto, xml_detcantidad, bol_estatus, cod_quincenaid_fk, bol_validacion, fec_validacion, aud_codcreadopor, aud_feccreacion, aud_codmodificadopor, aud_fecmodificacion, bol_aceptacion, bol_pago) VALUES (28, 1, 0, '', ' coments coments coments  coments coments coments  coments coments coments  coments coments coments  coments coments coments  coments coments coments  coments coments coments  coments coments coments ', 13, NULL, NULL, NULL, true, 18, false, NULL, 13, '2019-08-27', NULL, NULL, NULL, NULL);
INSERT INTO sgnom.tsgnomincidencia (cod_incidenciaid, cod_catincidenciaid_fk, cnu_cantidad, des_actividad, txt_comentarios, cod_empreporta_fk, cod_empautoriza_fk, imp_monto, xml_detcantidad, bol_estatus, cod_quincenaid_fk, bol_validacion, fec_validacion, aud_codcreadopor, aud_feccreacion, aud_codmodificadopor, aud_fecmodificacion, bol_aceptacion, bol_pago) VALUES (2, 7, 1, NULL, 'CURSO ALUMNOS', 13, NULL, NULL, NULL, true, 19, NULL, NULL, 16, '2019-05-25', 16, '2019-07-16', NULL, NULL);
INSERT INTO sgnom.tsgnomincidencia (cod_incidenciaid, cod_catincidenciaid_fk, cnu_cantidad, des_actividad, txt_comentarios, cod_empreporta_fk, cod_empautoriza_fk, imp_monto, xml_detcantidad, bol_estatus, cod_quincenaid_fk, bol_validacion, fec_validacion, aud_codcreadopor, aud_feccreacion, aud_codmodificadopor, aud_fecmodificacion, bol_aceptacion, bol_pago) VALUES (3, 9, 0, '', '', 13, NULL, NULL, NULL, true, 20, NULL, NULL, 16, '2019-07-17', NULL, NULL, NULL, NULL);
INSERT INTO sgnom.tsgnomincidencia (cod_incidenciaid, cod_catincidenciaid_fk, cnu_cantidad, des_actividad, txt_comentarios, cod_empreporta_fk, cod_empautoriza_fk, imp_monto, xml_detcantidad, bol_estatus, cod_quincenaid_fk, bol_validacion, fec_validacion, aud_codcreadopor, aud_feccreacion, aud_codmodificadopor, aud_fecmodificacion, bol_aceptacion, bol_pago) VALUES (37, 4, 2, 'actividad', 'comentarios', 19, NULL, 0.00, '                
		<DetalleFechas><fecha>2019-08-20</fecha><fecha>2019-08-20</fecha><fecha>2019-08-21</fecha></DetalleFechas>', true, 18, NULL, NULL, 13, '2019-09-02', 13, '2019-09-05', NULL, NULL);
INSERT INTO sgnom.tsgnomincidencia (cod_incidenciaid, cod_catincidenciaid_fk, cnu_cantidad, des_actividad, txt_comentarios, cod_empreporta_fk, cod_empautoriza_fk, imp_monto, xml_detcantidad, bol_estatus, cod_quincenaid_fk, bol_validacion, fec_validacion, aud_codcreadopor, aud_feccreacion, aud_codmodificadopor, aud_fecmodificacion, bol_aceptacion, bol_pago) VALUES (39, 4, 2, NULL, NULL, 20, NULL, 0.00, '                
		<DetalleFechas><fecha>2019-08-20</fecha><fecha>2019-08-20</fecha><fecha>2019-08-21</fecha></DetalleFechas>', true, 18, NULL, NULL, 13, '2019-09-02', 13, '2019-09-05', NULL, NULL);
INSERT INTO sgnom.tsgnomincidencia (cod_incidenciaid, cod_catincidenciaid_fk, cnu_cantidad, des_actividad, txt_comentarios, cod_empreporta_fk, cod_empautoriza_fk, imp_monto, xml_detcantidad, bol_estatus, cod_quincenaid_fk, bol_validacion, fec_validacion, aud_codcreadopor, aud_feccreacion, aud_codmodificadopor, aud_fecmodificacion, bol_aceptacion, bol_pago) VALUES (47, 4, 1, '', 'ejemplo', 19, NULL, 0.00, '                
		<DetalleFechas><fecha>2019-08-18</fecha><fecha>2019-08-19</fecha><fecha>2019-08-20</fecha></DetalleFechas>', true, 18, NULL, NULL, 13, '2019-09-06', 13, '2019-09-10', NULL, NULL);
INSERT INTO sgnom.tsgnomincidencia (cod_incidenciaid, cod_catincidenciaid_fk, cnu_cantidad, des_actividad, txt_comentarios, cod_empreporta_fk, cod_empautoriza_fk, imp_monto, xml_detcantidad, bol_estatus, cod_quincenaid_fk, bol_validacion, fec_validacion, aud_codcreadopor, aud_feccreacion, aud_codmodificadopor, aud_fecmodificacion, bol_aceptacion, bol_pago) VALUES (9, 6, 1, NULL, 'extras', 20, NULL, NULL, NULL, true, 7, true, NULL, 16, '2019-05-25', 13, '2019-08-26', NULL, NULL);
INSERT INTO sgnom.tsgnomincidencia (cod_incidenciaid, cod_catincidenciaid_fk, cnu_cantidad, des_actividad, txt_comentarios, cod_empreporta_fk, cod_empautoriza_fk, imp_monto, xml_detcantidad, bol_estatus, cod_quincenaid_fk, bol_validacion, fec_validacion, aud_codcreadopor, aud_feccreacion, aud_codmodificadopor, aud_fecmodificacion, bol_aceptacion, bol_pago) VALUES (15, 5, 3, '', 'ej. agosto', 19, NULL, NULL, NULL, true, 7, NULL, NULL, 13, '2019-08-26', 13, '2019-08-26', NULL, NULL);
INSERT INTO sgnom.tsgnomincidencia (cod_incidenciaid, cod_catincidenciaid_fk, cnu_cantidad, des_actividad, txt_comentarios, cod_empreporta_fk, cod_empautoriza_fk, imp_monto, xml_detcantidad, bol_estatus, cod_quincenaid_fk, bol_validacion, fec_validacion, aud_codcreadopor, aud_feccreacion, aud_codmodificadopor, aud_fecmodificacion, bol_aceptacion, bol_pago) VALUES (23, 10, 0, 'prueba agosto prueba agosto prueba agosto prueba agosto prueba agosto prueba agosto prueba agosto 00', 'entrega', 13, NULL, NULL, NULL, true, 7, NULL, NULL, 13, '2019-08-26', 13, '2019-08-27', NULL, NULL);
INSERT INTO sgnom.tsgnomincidencia (cod_incidenciaid, cod_catincidenciaid_fk, cnu_cantidad, des_actividad, txt_comentarios, cod_empreporta_fk, cod_empautoriza_fk, imp_monto, xml_detcantidad, bol_estatus, cod_quincenaid_fk, bol_validacion, fec_validacion, aud_codcreadopor, aud_feccreacion, aud_codmodificadopor, aud_fecmodificacion, bol_aceptacion, bol_pago) VALUES (43, 4, 1, '', 'fechas ARREGLO', 13, NULL, 0.00, '                
		<DetalleFechas><fecha>2019-08-18</fecha><fecha>2019-08-19</fecha><fecha>2019-08-20</fecha></DetalleFechas>', true, 7, NULL, NULL, 13, '2019-09-05', 13, '2019-09-05', NULL, NULL);
INSERT INTO sgnom.tsgnomincidencia (cod_incidenciaid, cod_catincidenciaid_fk, cnu_cantidad, des_actividad, txt_comentarios, cod_empreporta_fk, cod_empautoriza_fk, imp_monto, xml_detcantidad, bol_estatus, cod_quincenaid_fk, bol_validacion, fec_validacion, aud_codcreadopor, aud_feccreacion, aud_codmodificadopor, aud_fecmodificacion, bol_aceptacion, bol_pago) VALUES (48, 4, 1, '', 'INSERTAR CON STRING', 13, NULL, 0.00, '                
		<DetalleFechas><fecha>2019-08-18</fecha><fecha>2019-08-19</fecha><fecha>2019-08-20</fecha></DetalleFechas>', false, 7, NULL, NULL, 13, '2019-09-06', 13, '2019-09-09', NULL, NULL);
INSERT INTO sgnom.tsgnomincidencia (cod_incidenciaid, cod_catincidenciaid_fk, cnu_cantidad, des_actividad, txt_comentarios, cod_empreporta_fk, cod_empautoriza_fk, imp_monto, xml_detcantidad, bol_estatus, cod_quincenaid_fk, bol_validacion, fec_validacion, aud_codcreadopor, aud_feccreacion, aud_codmodificadopor, aud_fecmodificacion, bol_aceptacion, bol_pago) VALUES (29, 1, 1, '', ' coments coments coments  coments coments coments  coments coments coments  coments coments coments  coments coments coments  coments coments coments  coments coments coments  coments coments coments ', 13, NULL, NULL, NULL, true, 7, false, NULL, 13, '2019-08-27', NULL, NULL, NULL, NULL);
INSERT INTO sgnom.tsgnomincidencia (cod_incidenciaid, cod_catincidenciaid_fk, cnu_cantidad, des_actividad, txt_comentarios, cod_empreporta_fk, cod_empautoriza_fk, imp_monto, xml_detcantidad, bol_estatus, cod_quincenaid_fk, bol_validacion, fec_validacion, aud_codcreadopor, aud_feccreacion, aud_codmodificadopor, aud_fecmodificacion, bol_aceptacion, bol_pago) VALUES (42, 4, 2, '', NULL, 13, NULL, NULL, '                
		<DetalleFechas><fecha>2019-08-20</fecha><fecha>2019-08-20</fecha><fecha>2019-08-21</fecha></DetalleFechas>', true, 7, false, NULL, 13, '2019-09-02', 13, '2019-09-06', NULL, NULL);
INSERT INTO sgnom.tsgnomincidencia (cod_incidenciaid, cod_catincidenciaid_fk, cnu_cantidad, des_actividad, txt_comentarios, cod_empreporta_fk, cod_empautoriza_fk, imp_monto, xml_detcantidad, bol_estatus, cod_quincenaid_fk, bol_validacion, fec_validacion, aud_codcreadopor, aud_feccreacion, aud_codmodificadopor, aud_fecmodificacion, bol_aceptacion, bol_pago) VALUES (44, 4, 1, '', 'fechas ARREGLO', 13, NULL, 0.00, '                
		<DetalleFechas><fecha>2019-08-18</fecha><fecha>2019-08-19</fecha><fecha>2019-08-20</fecha></DetalleFechas>', true, 7, false, NULL, 13, '2019-09-05', NULL, NULL, NULL, NULL);
INSERT INTO sgnom.tsgnomincidencia (cod_incidenciaid, cod_catincidenciaid_fk, cnu_cantidad, des_actividad, txt_comentarios, cod_empreporta_fk, cod_empautoriza_fk, imp_monto, xml_detcantidad, bol_estatus, cod_quincenaid_fk, bol_validacion, fec_validacion, aud_codcreadopor, aud_feccreacion, aud_codmodificadopor, aud_fecmodificacion, bol_aceptacion, bol_pago) VALUES (49, 1, 2, '.', 'example', 13, NULL, 0.00, '                
		<DetalleFechas><fecha>2019-08-18</fecha><fecha>2019-08-19</fecha><fecha>2019-08-20</fecha></DetalleFechas>', true, 7, false, NULL, 13, '2019-09-06', NULL, NULL, NULL, NULL);
INSERT INTO sgnom.tsgnomincidencia (cod_incidenciaid, cod_catincidenciaid_fk, cnu_cantidad, des_actividad, txt_comentarios, cod_empreporta_fk, cod_empautoriza_fk, imp_monto, xml_detcantidad, bol_estatus, cod_quincenaid_fk, bol_validacion, fec_validacion, aud_codcreadopor, aud_feccreacion, aud_codmodificadopor, aud_fecmodificacion, bol_aceptacion, bol_pago) VALUES (50, 8, 1, 'k', 'll', 13, NULL, 0.00, '                
		<DetalleFechas><fecha>2019-09-05</fecha><fecha>2019-09-03</fecha></DetalleFechas>', true, 7, false, NULL, 13, '2019-09-06', NULL, NULL, NULL, NULL);
INSERT INTO sgnom.tsgnomincidencia (cod_incidenciaid, cod_catincidenciaid_fk, cnu_cantidad, des_actividad, txt_comentarios, cod_empreporta_fk, cod_empautoriza_fk, imp_monto, xml_detcantidad, bol_estatus, cod_quincenaid_fk, bol_validacion, fec_validacion, aud_codcreadopor, aud_feccreacion, aud_codmodificadopor, aud_fecmodificacion, bol_aceptacion, bol_pago) VALUES (52, 10, 0, 'prueba', 'l', 13, NULL, 0.00, '                
		<DetalleFechas><fecha>2019-09-04</fecha></DetalleFechas>', true, 7, false, NULL, 13, '2019-09-09', NULL, NULL, NULL, NULL);
INSERT INTO sgnom.tsgnomincidencia (cod_incidenciaid, cod_catincidenciaid_fk, cnu_cantidad, des_actividad, txt_comentarios, cod_empreporta_fk, cod_empautoriza_fk, imp_monto, xml_detcantidad, bol_estatus, cod_quincenaid_fk, bol_validacion, fec_validacion, aud_codcreadopor, aud_feccreacion, aud_codmodificadopor, aud_fecmodificacion, bol_aceptacion, bol_pago) VALUES (38, 4, 2, 'actividad', NULL, 13, NULL, 0.00, '                
		<DetalleFechas><fecha>2019-08-20</fecha><fecha>2019-08-20</fecha><fecha>2019-08-21</fecha></DetalleFechas>', false, 7, NULL, NULL, 13, '2019-09-02', 13, '2019-09-09', NULL, NULL);
INSERT INTO sgnom.tsgnomincidencia (cod_incidenciaid, cod_catincidenciaid_fk, cnu_cantidad, des_actividad, txt_comentarios, cod_empreporta_fk, cod_empautoriza_fk, imp_monto, xml_detcantidad, bol_estatus, cod_quincenaid_fk, bol_validacion, fec_validacion, aud_codcreadopor, aud_feccreacion, aud_codmodificadopor, aud_fecmodificacion, bol_aceptacion, bol_pago) VALUES (54, 8, 3, '', 'prueba cantidad', 13, NULL, 0.00, '                
		<DetalleFechas><fecha>2019-09-06</fecha></DetalleFechas>', true, 7, false, NULL, 13, '2019-09-09', NULL, NULL, NULL, NULL);
INSERT INTO sgnom.tsgnomincidencia (cod_incidenciaid, cod_catincidenciaid_fk, cnu_cantidad, des_actividad, txt_comentarios, cod_empreporta_fk, cod_empautoriza_fk, imp_monto, xml_detcantidad, bol_estatus, cod_quincenaid_fk, bol_validacion, fec_validacion, aud_codcreadopor, aud_feccreacion, aud_codmodificadopor, aud_fecmodificacion, bol_aceptacion, bol_pago) VALUES (22, 10, 0, 'entrega parcial de proyecto', 'actualizaci�n de requerimientos aprobada por el cliente', 13, NULL, NULL, NULL, true, 7, false, NULL, 13, '2019-08-26', NULL, NULL, NULL, NULL);
INSERT INTO sgnom.tsgnomincidencia (cod_incidenciaid, cod_catincidenciaid_fk, cnu_cantidad, des_actividad, txt_comentarios, cod_empreporta_fk, cod_empautoriza_fk, imp_monto, xml_detcantidad, bol_estatus, cod_quincenaid_fk, bol_validacion, fec_validacion, aud_codcreadopor, aud_feccreacion, aud_codmodificadopor, aud_fecmodificacion, bol_aceptacion, bol_pago) VALUES (26, 1, 1, '', 'ejemplo', 13, NULL, NULL, NULL, true, 7, false, NULL, 13, '2019-08-27', NULL, NULL, NULL, NULL);
INSERT INTO sgnom.tsgnomincidencia (cod_incidenciaid, cod_catincidenciaid_fk, cnu_cantidad, des_actividad, txt_comentarios, cod_empreporta_fk, cod_empautoriza_fk, imp_monto, xml_detcantidad, bol_estatus, cod_quincenaid_fk, bol_validacion, fec_validacion, aud_codcreadopor, aud_feccreacion, aud_codmodificadopor, aud_fecmodificacion, bol_aceptacion, bol_pago) VALUES (27, 1, 2, '', ' coments coments coments  coments coments coments  coments coments coments  coments coments coments  coments coments coments  coments coments coments  coments coments coments  coments coments coments ', 13, NULL, NULL, NULL, true, 7, false, NULL, 13, '2019-08-27', NULL, NULL, NULL, NULL);
INSERT INTO sgnom.tsgnomincidencia (cod_incidenciaid, cod_catincidenciaid_fk, cnu_cantidad, des_actividad, txt_comentarios, cod_empreporta_fk, cod_empautoriza_fk, imp_monto, xml_detcantidad, bol_estatus, cod_quincenaid_fk, bol_validacion, fec_validacion, aud_codcreadopor, aud_feccreacion, aud_codmodificadopor, aud_fecmodificacion, bol_aceptacion, bol_pago) VALUES (19, 12, 2, '', 'clase de mvc', 13, 14, NULL, NULL, true, 7, false, NULL, 13, '2019-08-26', NULL, NULL, NULL, NULL);
INSERT INTO sgnom.tsgnomincidencia (cod_incidenciaid, cod_catincidenciaid_fk, cnu_cantidad, des_actividad, txt_comentarios, cod_empreporta_fk, cod_empautoriza_fk, imp_monto, xml_detcantidad, bol_estatus, cod_quincenaid_fk, bol_validacion, fec_validacion, aud_codcreadopor, aud_feccreacion, aud_codmodificadopor, aud_fecmodificacion, bol_aceptacion, bol_pago) VALUES (30, 8, 2, '', 'horario completo', 13, NULL, NULL, NULL, true, 7, false, NULL, 13, '2019-08-27', NULL, NULL, NULL, NULL);
INSERT INTO sgnom.tsgnomincidencia (cod_incidenciaid, cod_catincidenciaid_fk, cnu_cantidad, des_actividad, txt_comentarios, cod_empreporta_fk, cod_empautoriza_fk, imp_monto, xml_detcantidad, bol_estatus, cod_quincenaid_fk, bol_validacion, fec_validacion, aud_codcreadopor, aud_feccreacion, aud_codmodificadopor, aud_fecmodificacion, bol_aceptacion, bol_pago) VALUES (60, 1, 1, '', '164546464', 13, NULL, 0.00, '                
		<DetalleFechas><fecha>19-09-2019</fecha></DetalleFechas>', true, 7, NULL, NULL, 13, '2019-09-19', NULL, NULL, NULL, NULL);
INSERT INTO sgnom.tsgnomincidencia (cod_incidenciaid, cod_catincidenciaid_fk, cnu_cantidad, des_actividad, txt_comentarios, cod_empreporta_fk, cod_empautoriza_fk, imp_monto, xml_detcantidad, bol_estatus, cod_quincenaid_fk, bol_validacion, fec_validacion, aud_codcreadopor, aud_feccreacion, aud_codmodificadopor, aud_fecmodificacion, bol_aceptacion, bol_pago) VALUES (61, 10, 0, 'karla', 'patricia', 13, NULL, 0.00, '                
		<DetalleFechas><fecha>11-09-2019</fecha><fecha>20-09-2019</fecha></DetalleFechas>', true, 7, NULL, NULL, 13, '2019-09-30', NULL, NULL, NULL, NULL);
INSERT INTO sgnom.tsgnomincidencia (cod_incidenciaid, cod_catincidenciaid_fk, cnu_cantidad, des_actividad, txt_comentarios, cod_empreporta_fk, cod_empautoriza_fk, imp_monto, xml_detcantidad, bol_estatus, cod_quincenaid_fk, bol_validacion, fec_validacion, aud_codcreadopor, aud_feccreacion, aud_codmodificadopor, aud_fecmodificacion, bol_aceptacion, bol_pago) VALUES (62, 12, 9, '', 'yo mero', 13, NULL, 0.00, '                
		<DetalleFechas><fecha>03-09-2019</fecha><fecha>04-09-2019</fecha></DetalleFechas>', true, 18, true, NULL, 13, '2019-09-30', NULL, NULL, NULL, NULL);
INSERT INTO sgnom.tsgnomincidencia (cod_incidenciaid, cod_catincidenciaid_fk, cnu_cantidad, des_actividad, txt_comentarios, cod_empreporta_fk, cod_empautoriza_fk, imp_monto, xml_detcantidad, bol_estatus, cod_quincenaid_fk, bol_validacion, fec_validacion, aud_codcreadopor, aud_feccreacion, aud_codmodificadopor, aud_fecmodificacion, bol_aceptacion, bol_pago) VALUES (63, 1, 8, '', 'paty', 13, NULL, 0.00, '                
		<DetalleFechas><fecha>17-10-2019</fecha><fecha>19-10-2019</fecha></DetalleFechas>', true, 20, NULL, NULL, 13, '2019-10-23', 13, '2019-10-23', NULL, NULL);
INSERT INTO sgnom.tsgnomincidencia (cod_incidenciaid, cod_catincidenciaid_fk, cnu_cantidad, des_actividad, txt_comentarios, cod_empreporta_fk, cod_empautoriza_fk, imp_monto, xml_detcantidad, bol_estatus, cod_quincenaid_fk, bol_validacion, fec_validacion, aud_codcreadopor, aud_feccreacion, aud_codmodificadopor, aud_fecmodificacion, bol_aceptacion, bol_pago) VALUES (64, 12, 2, '', 'se pagara la bonificacion', 10, 11, 0.00, '                
		<DetalleFechas><fecha>08-11-2019</fecha><fecha>09-11-2019</fecha></DetalleFechas>', true, 21, true, '2019-11-12', 10, '2019-11-12', 11, '2019-11-12', NULL, NULL);


--
-- TOC entry 3763 (class 0 OID 97308)
-- Dependencies: 385
-- Data for Name: tsgnommanterceros; Type: TABLE DATA; Schema: sgnom; Owner: suite
--



--
-- TOC entry 3764 (class 0 OID 97311)
-- Dependencies: 386
-- Data for Name: tsgnomnominaimss; Type: TABLE DATA; Schema: sgnom; Owner: suite
--

INSERT INTO sgnom.tsgnomnominaimss (cod_nominaimssid, clave, nss, rfc, curp, fecha_alta, tipo_salario, salario_diario, "SDI", dias_trabajados, faltas, sueldo, premio_asistencia, premio_puntualidad, total_percepciones, total_gravable, total_imss, total_isr, isr, imss, credito_infonavit, subsidio_empleo, subsidio_empleo_aplicado, total_deducciones, neto_pagado, cod_cabeceraid_fk, vacaciones, prima_vacacional) VALUES (98, 10, NULL, NULL, 'ROJM960409HPZDAT22', '2010-03-04', 'Fijo', 200, 210.137, 15.2083, 0, 3041.66, 152.08, 152.08, 3345.82, 3345.82, 75.9, 114.17, 114.17, 75.9, 825.93, -0.01, 126.8392, 1142.8292, 2329.83, 39, NULL, NULL);
INSERT INTO sgnom.tsgnomnominaimss (cod_nominaimssid, clave, nss, rfc, curp, fecha_alta, tipo_salario, salario_diario, "SDI", dias_trabajados, faltas, sueldo, premio_asistencia, premio_puntualidad, total_percepciones, total_gravable, total_imss, total_isr, isr, imss, credito_infonavit, subsidio_empleo, subsidio_empleo_aplicado, total_deducciones, neto_pagado, cod_cabeceraid_fk, vacaciones, prima_vacacional) VALUES (99, 11, NULL, NULL, 'SDCA960614HPZDBT23', '2014-08-01', 'Fijo', 130, 136.5891, 15.2083, 0, 1977.08, 98.85, 98.85, 2174.78, 2174.78, 49.21, 0, 0, 49.21, 0, -65.12, 191.3344, 175.4244, 2190.69, 39, NULL, NULL);
INSERT INTO sgnom.tsgnomnominaimss (cod_nominaimssid, clave, nss, rfc, curp, fecha_alta, tipo_salario, salario_diario, "SDI", dias_trabajados, faltas, sueldo, premio_asistencia, premio_puntualidad, total_percepciones, total_gravable, total_imss, total_isr, isr, imss, credito_infonavit, subsidio_empleo, subsidio_empleo_aplicado, total_deducciones, neto_pagado, cod_cabeceraid_fk, vacaciones, prima_vacacional) VALUES (100, 12, NULL, NULL, 'ANTC960312HPZDCT24', '2011-01-16', 'Fijo', 170, 178.6164, 15.2083, 0, 2585.41, 129.27, 129.27, 2843.95, 2843.95, 64.52, 39.01, 39.01, 64.52, 0, -0.01, 147.3954, 250.9154, 2740.43, 39, NULL, NULL);
INSERT INTO sgnom.tsgnomnominaimss (cod_nominaimssid, clave, nss, rfc, curp, fecha_alta, tipo_salario, salario_diario, "SDI", dias_trabajados, faltas, sueldo, premio_asistencia, premio_puntualidad, total_percepciones, total_gravable, total_imss, total_isr, isr, imss, credito_infonavit, subsidio_empleo, subsidio_empleo_aplicado, total_deducciones, neto_pagado, cod_cabeceraid_fk, vacaciones, prima_vacacional) VALUES (101, 13, NULL, NULL, 'ROAA950219HPZDDT25', '2011-11-01', 'Fijo', 560, 588.3837, 15.2083, 0, 8516.65, 425.83, 425.83, 9368.31, 9368.31, 232.9, 1353.99, 1353.99, 232.9, 0, 0, 0, 1586.89, 7781.42, 39, NULL, NULL);
INSERT INTO sgnom.tsgnomnominaimss (cod_nominaimssid, clave, nss, rfc, curp, fecha_alta, tipo_salario, salario_diario, "SDI", dias_trabajados, faltas, sueldo, premio_asistencia, premio_puntualidad, total_percepciones, total_gravable, total_imss, total_isr, isr, imss, credito_infonavit, subsidio_empleo, subsidio_empleo_aplicado, total_deducciones, neto_pagado, cod_cabeceraid_fk, vacaciones, prima_vacacional) VALUES (102, 14, NULL, NULL, 'MASJ950212HPZDET26', '2009-08-16', 'Fijo', 300, 315.6164, 15.2083, 0, 4562.49, 228.12, 228.12, 5018.73, 5018.73, 117.61, 458.96, 458.96, 117.61, 0, 0, 0, 576.57, 4442.16, 39, NULL, NULL);
INSERT INTO sgnom.tsgnomnominaimss (cod_nominaimssid, clave, nss, rfc, curp, fecha_alta, tipo_salario, salario_diario, "SDI", dias_trabajados, faltas, sueldo, premio_asistencia, premio_puntualidad, total_percepciones, total_gravable, total_imss, total_isr, isr, imss, credito_infonavit, subsidio_empleo, subsidio_empleo_aplicado, total_deducciones, neto_pagado, cod_cabeceraid_fk, vacaciones, prima_vacacional) VALUES (103, 15, NULL, NULL, 'HECA950220HPZDFT27', '2013-01-01', 'Fijo', 535.1, 562.2214, 15.2083, 0, 8137.96, 406.9, 406.9, 8951.76, 8951.76, 221.85, 1265.02, 1265.02, 221.85, 0, 0, 0, 1486.87, 7464.89, 39, NULL, NULL);
INSERT INTO sgnom.tsgnomnominaimss (cod_nominaimssid, clave, nss, rfc, curp, fecha_alta, tipo_salario, salario_diario, "SDI", dias_trabajados, faltas, sueldo, premio_asistencia, premio_puntualidad, total_percepciones, total_gravable, total_imss, total_isr, isr, imss, credito_infonavit, subsidio_empleo, subsidio_empleo_aplicado, total_deducciones, neto_pagado, cod_cabeceraid_fk, vacaciones, prima_vacacional) VALUES (104, 16, NULL, NULL, 'CHHJ950221HPZDGT28', '2014-01-02', 'Fijo', 130, 136.5891, 15.2083, 0, 1977.08, 98.85, 98.85, 2174.78, 2174.78, 49.34, 0, 0, 49.34, 0, -65.12, 191.3344, 175.5544, 2190.56, 39, NULL, NULL);
INSERT INTO sgnom.tsgnomnominaimss (cod_nominaimssid, clave, nss, rfc, curp, fecha_alta, tipo_salario, salario_diario, "SDI", dias_trabajados, faltas, sueldo, premio_asistencia, premio_puntualidad, total_percepciones, total_gravable, total_imss, total_isr, isr, imss, credito_infonavit, subsidio_empleo, subsidio_empleo_aplicado, total_deducciones, neto_pagado, cod_cabeceraid_fk, vacaciones, prima_vacacional) VALUES (105, 17, NULL, NULL, 'GOPM950222HPZDHT29', '2015-04-16', 'Fijo', 105, 110.178, 15.2083, 0, 1596.87, 79.84, 79.84, 1756.55, 1756.55, 35.96, 0, 0, 35.96, 0, -97.04, 196.4922, 135.4122, 1817.63, 39, NULL, NULL);
INSERT INTO sgnom.tsgnomnominaimss (cod_nominaimssid, clave, nss, rfc, curp, fecha_alta, tipo_salario, salario_diario, "SDI", dias_trabajados, faltas, sueldo, premio_asistencia, premio_puntualidad, total_percepciones, total_gravable, total_imss, total_isr, isr, imss, credito_infonavit, subsidio_empleo, subsidio_empleo_aplicado, total_deducciones, neto_pagado, cod_cabeceraid_fk, vacaciones, prima_vacacional) VALUES (106, 19, NULL, NULL, 'BEAC950302HPZDJT31', '2016-03-28', 'Fijo', 110, 115.2738, 15.2083, 0, 1672.91, 83.65, 83.65, 1840.21, 1840.21, 41.69, 0, 0, 41.69, 0, -86.53, 191.3344, 146.4944, 1885.05, 39, NULL, NULL);
INSERT INTO sgnom.tsgnomnominaimss (cod_nominaimssid, clave, nss, rfc, curp, fecha_alta, tipo_salario, salario_diario, "SDI", dias_trabajados, faltas, sueldo, premio_asistencia, premio_puntualidad, total_percepciones, total_gravable, total_imss, total_isr, isr, imss, credito_infonavit, subsidio_empleo, subsidio_empleo_aplicado, total_deducciones, neto_pagado, cod_cabeceraid_fk, vacaciones, prima_vacacional) VALUES (107, 20, NULL, NULL, 'JIVC950303HPZDKT32', '2016-03-28', 'Fijo', 110, 115.2738, 15.2083, 0, 1672.91, 83.65, 83.65, 1840.21, 1840.21, 41.69, 0, 0, 41.69, 0, -86.53, 191.3344, 146.4944, 1885.05, 39, NULL, NULL);
INSERT INTO sgnom.tsgnomnominaimss (cod_nominaimssid, clave, nss, rfc, curp, fecha_alta, tipo_salario, salario_diario, "SDI", dias_trabajados, faltas, sueldo, premio_asistencia, premio_puntualidad, total_percepciones, total_gravable, total_imss, total_isr, isr, imss, credito_infonavit, subsidio_empleo, subsidio_empleo_aplicado, total_deducciones, neto_pagado, cod_cabeceraid_fk, vacaciones, prima_vacacional) VALUES (108, 21, NULL, NULL, 'SATV950304HPZDLT33', '2017-04-05', 'Fijo', 131.58, 137.7085, 15.2083, 0, 2001.11, 100.06, 100.06, 2201.23, 2201.23, 49.67, 0, 0, 49.67, 0, -63.42, 191.3344, 177.5844, 2214.98, 39, NULL, NULL);
INSERT INTO sgnom.tsgnomnominaimss (cod_nominaimssid, clave, nss, rfc, curp, fecha_alta, tipo_salario, salario_diario, "SDI", dias_trabajados, faltas, sueldo, premio_asistencia, premio_puntualidad, total_percepciones, total_gravable, total_imss, total_isr, isr, imss, credito_infonavit, subsidio_empleo, subsidio_empleo_aplicado, total_deducciones, neto_pagado, cod_cabeceraid_fk, vacaciones, prima_vacacional) VALUES (109, 22, NULL, NULL, 'TRMH950305HPZDMT34', '2017-04-16', 'Fijo', 131.58, 137.7085, 15.2083, 0, 2001.11, 100.06, 100.06, 2201.23, 2201.23, 49.67, 0, 0, 49.67, 0, -63.42, 191.3344, 177.5844, 2214.98, 39, NULL, NULL);
INSERT INTO sgnom.tsgnomnominaimss (cod_nominaimssid, clave, nss, rfc, curp, fecha_alta, tipo_salario, salario_diario, "SDI", dias_trabajados, faltas, sueldo, premio_asistencia, premio_puntualidad, total_percepciones, total_gravable, total_imss, total_isr, isr, imss, credito_infonavit, subsidio_empleo, subsidio_empleo_aplicado, total_deducciones, neto_pagado, cod_cabeceraid_fk, vacaciones, prima_vacacional) VALUES (110, 23, NULL, NULL, 'MVMJ950306HPZDNT35', '2017-09-01', 'Fijo', 150, 156.986, 15.2083, 0, 2281.24, 114.06, 114.06, 2509.36, 2509.36, 56.63, 0, 0, 56.63, 0, -12.52, 162.5237, 206.6337, 2465.25, 39, NULL, NULL);
INSERT INTO sgnom.tsgnomnominaimss (cod_nominaimssid, clave, nss, rfc, curp, fecha_alta, tipo_salario, salario_diario, "SDI", dias_trabajados, faltas, sueldo, premio_asistencia, premio_puntualidad, total_percepciones, total_gravable, total_imss, total_isr, isr, imss, credito_infonavit, subsidio_empleo, subsidio_empleo_aplicado, total_deducciones, neto_pagado, cod_cabeceraid_fk, vacaciones, prima_vacacional) VALUES (111, 24, NULL, NULL, 'ESMR950307HPZDOT36', '2017-09-01', 'Fijo', 110, 115.1231, 15.2083, 0, 1672.91, 83.65, 83.65, 1840.21, 1840.21, 41.64, 0, 0, 41.64, 0, -86.53, 191.3344, 146.4444, 1885.1, 39, NULL, NULL);
INSERT INTO sgnom.tsgnomnominaimss (cod_nominaimssid, clave, nss, rfc, curp, fecha_alta, tipo_salario, salario_diario, "SDI", dias_trabajados, faltas, sueldo, premio_asistencia, premio_puntualidad, total_percepciones, total_gravable, total_imss, total_isr, isr, imss, credito_infonavit, subsidio_empleo, subsidio_empleo_aplicado, total_deducciones, neto_pagado, cod_cabeceraid_fk, vacaciones, prima_vacacional) VALUES (112, 18, NULL, NULL, 'RAFS950301HPZDIT30', '2018-02-01', 'Fijo', 110, 114.9724, 15.2083, 0, 1672.91, 83.65, 83.65, 1840.21, 1840.21, 41.58, 0, 0, 41.58, 0, -86.53, 191.3344, 146.3844, 1885.16, 39, NULL, NULL);
INSERT INTO sgnom.tsgnomnominaimss (cod_nominaimssid, clave, nss, rfc, curp, fecha_alta, tipo_salario, salario_diario, "SDI", dias_trabajados, faltas, sueldo, premio_asistencia, premio_puntualidad, total_percepciones, total_gravable, total_imss, total_isr, isr, imss, credito_infonavit, subsidio_empleo, subsidio_empleo_aplicado, total_deducciones, neto_pagado, cod_cabeceraid_fk, vacaciones, prima_vacacional) VALUES (113, 25, NULL, NULL, 'ETMJ950308HPZDPT37', '2018-02-16', 'Fijo', 110, 114.9724, 15.2083, 0, 1672.91, 83.65, 83.65, 1840.21, 1840.21, 41.58, 0, 0, 41.58, 0, -86.53, 191.3344, 146.3844, 1885.16, 39, NULL, NULL);
INSERT INTO sgnom.tsgnomnominaimss (cod_nominaimssid, clave, nss, rfc, curp, fecha_alta, tipo_salario, salario_diario, "SDI", dias_trabajados, faltas, sueldo, premio_asistencia, premio_puntualidad, total_percepciones, total_gravable, total_imss, total_isr, isr, imss, credito_infonavit, subsidio_empleo, subsidio_empleo_aplicado, total_deducciones, neto_pagado, cod_cabeceraid_fk, vacaciones, prima_vacacional) VALUES (114, 26, NULL, NULL, 'GOSS950309HPZDQT38', '2018-02-16', 'Fijo', 110, 114.9724, 15.2083, 0, 1672.91, 83.65, 83.65, 1840.21, 1840.21, 41.58, 0, 0, 41.58, 0, -86.53, 191.3344, 146.3844, 1885.16, 39, NULL, NULL);
INSERT INTO sgnom.tsgnomnominaimss (cod_nominaimssid, clave, nss, rfc, curp, fecha_alta, tipo_salario, salario_diario, "SDI", dias_trabajados, faltas, sueldo, premio_asistencia, premio_puntualidad, total_percepciones, total_gravable, total_imss, total_isr, isr, imss, credito_infonavit, subsidio_empleo, subsidio_empleo_aplicado, total_deducciones, neto_pagado, cod_cabeceraid_fk, vacaciones, prima_vacacional) VALUES (115, 27, NULL, NULL, 'GUTF950310HPZDRT39', '2018-09-10', 'Fijo', 105, 109.7465, 15.2083, 0, 1596.87, 79.84, 79.84, 1756.55, 1756.55, 39.64, 0, 0, 39.64, 0, -97.04, 196.4922, 139.0922, 1813.95, 39, NULL, NULL);
INSERT INTO sgnom.tsgnomnominaimss (cod_nominaimssid, clave, nss, rfc, curp, fecha_alta, tipo_salario, salario_diario, "SDI", dias_trabajados, faltas, sueldo, premio_asistencia, premio_puntualidad, total_percepciones, total_gravable, total_imss, total_isr, isr, imss, credito_infonavit, subsidio_empleo, subsidio_empleo_aplicado, total_deducciones, neto_pagado, cod_cabeceraid_fk, vacaciones, prima_vacacional) VALUES (116, 28, NULL, NULL, 'IVSJ950311HPZDTT40', '2019-03-04', 'Fijo', 110, 114.9724, 15.2083, 0, 1672.91, 83.65, 83.65, 1840.21, 1840.21, 41.53, 0, 0, 41.53, 0, -86.53, 191.3344, 146.3344, 1885.21, 39, NULL, NULL);
INSERT INTO sgnom.tsgnomnominaimss (cod_nominaimssid, clave, nss, rfc, curp, fecha_alta, tipo_salario, salario_diario, "SDI", dias_trabajados, faltas, sueldo, premio_asistencia, premio_puntualidad, total_percepciones, total_gravable, total_imss, total_isr, isr, imss, credito_infonavit, subsidio_empleo, subsidio_empleo_aplicado, total_deducciones, neto_pagado, cod_cabeceraid_fk, vacaciones, prima_vacacional) VALUES (117, 29, NULL, NULL, 'ESCE950312HPZDUT41', '2019-03-05', 'Fijo', 110, 114.9724, 15.2083, 0, 1672.91, 83.65, 83.65, 1840.21, 1840.21, 41.53, 0, 0, 41.53, 0, -86.53, 191.3344, 146.3344, 1885.21, 39, NULL, NULL);
INSERT INTO sgnom.tsgnomnominaimss (cod_nominaimssid, clave, nss, rfc, curp, fecha_alta, tipo_salario, salario_diario, "SDI", dias_trabajados, faltas, sueldo, premio_asistencia, premio_puntualidad, total_percepciones, total_gravable, total_imss, total_isr, isr, imss, credito_infonavit, subsidio_empleo, subsidio_empleo_aplicado, total_deducciones, neto_pagado, cod_cabeceraid_fk, vacaciones, prima_vacacional) VALUES (118, 30, NULL, NULL, 'ORAR950313HPZDVT42', '2019-03-25', 'Fijo', 105.55, 110.3217, 15.2083, 0, 1605.24, 80.26, 80.26, 1765.76, 1765.76, 39.85, 0, 0, 39.85, 1062.81, -96.45, 196.4922, 1202.7022, 759.55, 39, NULL, NULL);
INSERT INTO sgnom.tsgnomnominaimss (cod_nominaimssid, clave, nss, rfc, curp, fecha_alta, tipo_salario, salario_diario, "SDI", dias_trabajados, faltas, sueldo, premio_asistencia, premio_puntualidad, total_percepciones, total_gravable, total_imss, total_isr, isr, imss, credito_infonavit, subsidio_empleo, subsidio_empleo_aplicado, total_deducciones, neto_pagado, cod_cabeceraid_fk, vacaciones, prima_vacacional) VALUES (119, 31, NULL, NULL, 'TERC950314HPZDWT43', '2019-05-27', 'Fijo', 110, 114.9724, 15.2083, 0, 1672.91, 83.65, 83.65, 1840.21, 1840.21, 41.53, 0, 0, 41.53, 0, -86.53, 191.3344, 146.3344, 1885.21, 39, NULL, NULL);
INSERT INTO sgnom.tsgnomnominaimss (cod_nominaimssid, clave, nss, rfc, curp, fecha_alta, tipo_salario, salario_diario, "SDI", dias_trabajados, faltas, sueldo, premio_asistencia, premio_puntualidad, total_percepciones, total_gravable, total_imss, total_isr, isr, imss, credito_infonavit, subsidio_empleo, subsidio_empleo_aplicado, total_deducciones, neto_pagado, cod_cabeceraid_fk, vacaciones, prima_vacacional) VALUES (120, 32, NULL, NULL, 'ESJC950315HPZDXT44', '2019-05-27', 'Fijo', 110, 114.9724, 15.2083, 0, 1672.91, 83.65, 83.65, 1840.21, 1840.21, 41.53, 0, 0, 41.53, 0, -86.53, 191.3344, 146.3344, 1885.21, 39, NULL, NULL);
INSERT INTO sgnom.tsgnomnominaimss (cod_nominaimssid, clave, nss, rfc, curp, fecha_alta, tipo_salario, salario_diario, "SDI", dias_trabajados, faltas, sueldo, premio_asistencia, premio_puntualidad, total_percepciones, total_gravable, total_imss, total_isr, isr, imss, credito_infonavit, subsidio_empleo, subsidio_empleo_aplicado, total_deducciones, neto_pagado, cod_cabeceraid_fk, vacaciones, prima_vacacional) VALUES (121, 33, NULL, NULL, 'DETJ950316HPZDYT45', '2019-10-02', 'Fijo', 1363.15, 1424.7716, 14.2083, 0, 19368.04, 968.4, 968.4, 21304.84, 21304.84, 547.35, 4245, 4245, 547.35, 0, 0, 0, 4792.35, 16512.49, 39, NULL, NULL);
INSERT INTO sgnom.tsgnomnominaimss (cod_nominaimssid, clave, nss, rfc, curp, fecha_alta, tipo_salario, salario_diario, "SDI", dias_trabajados, faltas, sueldo, premio_asistencia, premio_puntualidad, total_percepciones, total_gravable, total_imss, total_isr, isr, imss, credito_infonavit, subsidio_empleo, subsidio_empleo_aplicado, total_deducciones, neto_pagado, cod_cabeceraid_fk, vacaciones, prima_vacacional) VALUES (122, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 39, NULL, NULL);


--
-- TOC entry 3765 (class 0 OID 97317)
-- Dependencies: 387
-- Data for Name: tsgnomquincena; Type: TABLE DATA; Schema: sgnom; Owner: suite
--

INSERT INTO sgnom.tsgnomquincena (cod_quincenaid, des_quincena, fec_inicio, fec_fin, fec_pago, fec_dispersion, cnu_numquincena, cod_ejercicioid_fk, bol_estatus) VALUES (1, '1era de enero', '2019-01-01', '2019-01-15', '2019-01-01', '2019-01-15', 1, 6, true);
INSERT INTO sgnom.tsgnomquincena (cod_quincenaid, des_quincena, fec_inicio, fec_fin, fec_pago, fec_dispersion, cnu_numquincena, cod_ejercicioid_fk, bol_estatus) VALUES (2, '2da de enero', '2019-01-16', '2019-01-31', '2019-01-16', '2019-01-31', 2, 6, true);
INSERT INTO sgnom.tsgnomquincena (cod_quincenaid, des_quincena, fec_inicio, fec_fin, fec_pago, fec_dispersion, cnu_numquincena, cod_ejercicioid_fk, bol_estatus) VALUES (3, '1era de febrero', '2019-02-01', '2019-02-15', '2019-02-01', '2019-02-15', 3, 6, true);
INSERT INTO sgnom.tsgnomquincena (cod_quincenaid, des_quincena, fec_inicio, fec_fin, fec_pago, fec_dispersion, cnu_numquincena, cod_ejercicioid_fk, bol_estatus) VALUES (4, '2da de febrero', '2019-02-16', '2019-02-28', '2019-02-16', '2019-02-28', 4, 6, true);
INSERT INTO sgnom.tsgnomquincena (cod_quincenaid, des_quincena, fec_inicio, fec_fin, fec_pago, fec_dispersion, cnu_numquincena, cod_ejercicioid_fk, bol_estatus) VALUES (5, '1era de marzo', '2019-03-01', '2019-03-15', '2019-03-01', '2019-03-15', 5, 6, true);
INSERT INTO sgnom.tsgnomquincena (cod_quincenaid, des_quincena, fec_inicio, fec_fin, fec_pago, fec_dispersion, cnu_numquincena, cod_ejercicioid_fk, bol_estatus) VALUES (6, '2da de marzo', '2019-03-16', '2019-03-31', '2019-03-16', '2019-03-31', 6, 6, true);
INSERT INTO sgnom.tsgnomquincena (cod_quincenaid, des_quincena, fec_inicio, fec_fin, fec_pago, fec_dispersion, cnu_numquincena, cod_ejercicioid_fk, bol_estatus) VALUES (7, '1era de abril', '2019-04-01', '2019-04-15', '2019-04-01', '2019-04-15', 7, 6, true);
INSERT INTO sgnom.tsgnomquincena (cod_quincenaid, des_quincena, fec_inicio, fec_fin, fec_pago, fec_dispersion, cnu_numquincena, cod_ejercicioid_fk, bol_estatus) VALUES (8, '2da de abril', '2019-04-16', '2019-04-30', '2019-04-16', '2019-04-30', 8, 6, true);
INSERT INTO sgnom.tsgnomquincena (cod_quincenaid, des_quincena, fec_inicio, fec_fin, fec_pago, fec_dispersion, cnu_numquincena, cod_ejercicioid_fk, bol_estatus) VALUES (9, '1era de mayo', '2019-05-01', '2019-05-15', '2019-05-01', '2019-05-15', 9, 6, true);
INSERT INTO sgnom.tsgnomquincena (cod_quincenaid, des_quincena, fec_inicio, fec_fin, fec_pago, fec_dispersion, cnu_numquincena, cod_ejercicioid_fk, bol_estatus) VALUES (10, '2da de mayo', '2019-05-16', '2019-05-31', '2019-05-16', '2019-05-31', 10, 6, true);
INSERT INTO sgnom.tsgnomquincena (cod_quincenaid, des_quincena, fec_inicio, fec_fin, fec_pago, fec_dispersion, cnu_numquincena, cod_ejercicioid_fk, bol_estatus) VALUES (11, '1era de junio', '2019-06-01', '2019-06-15', '2019-06-01', '2019-06-15', 11, 6, true);
INSERT INTO sgnom.tsgnomquincena (cod_quincenaid, des_quincena, fec_inicio, fec_fin, fec_pago, fec_dispersion, cnu_numquincena, cod_ejercicioid_fk, bol_estatus) VALUES (12, '2da de junio', '2019-06-16', '2019-06-30', '2019-06-16', '2019-06-30', 12, 6, true);
INSERT INTO sgnom.tsgnomquincena (cod_quincenaid, des_quincena, fec_inicio, fec_fin, fec_pago, fec_dispersion, cnu_numquincena, cod_ejercicioid_fk, bol_estatus) VALUES (13, '1era de julio', '2019-07-01', '2019-07-15', '2019-07-01', '2019-07-15', 13, 6, true);
INSERT INTO sgnom.tsgnomquincena (cod_quincenaid, des_quincena, fec_inicio, fec_fin, fec_pago, fec_dispersion, cnu_numquincena, cod_ejercicioid_fk, bol_estatus) VALUES (14, '2da de julio', '2019-07-16', '2019-07-31', '2019-07-16', '2019-07-31', 14, 6, true);
INSERT INTO sgnom.tsgnomquincena (cod_quincenaid, des_quincena, fec_inicio, fec_fin, fec_pago, fec_dispersion, cnu_numquincena, cod_ejercicioid_fk, bol_estatus) VALUES (15, '1era de agosto', '2019-08-01', '2019-08-15', '2019-08-01', '2019-08-15', 15, 6, true);
INSERT INTO sgnom.tsgnomquincena (cod_quincenaid, des_quincena, fec_inicio, fec_fin, fec_pago, fec_dispersion, cnu_numquincena, cod_ejercicioid_fk, bol_estatus) VALUES (16, '2da de agosto', '2019-08-16', '2019-08-31', '2019-08-16', '2019-08-31', 16, 6, true);
INSERT INTO sgnom.tsgnomquincena (cod_quincenaid, des_quincena, fec_inicio, fec_fin, fec_pago, fec_dispersion, cnu_numquincena, cod_ejercicioid_fk, bol_estatus) VALUES (17, '1era de septiembre', '2019-09-01', '2019-09-15', '2019-09-01', '2019-09-15', 17, 6, true);
INSERT INTO sgnom.tsgnomquincena (cod_quincenaid, des_quincena, fec_inicio, fec_fin, fec_pago, fec_dispersion, cnu_numquincena, cod_ejercicioid_fk, bol_estatus) VALUES (18, '2da de septiembre', '2019-09-16', '2019-09-30', '2019-09-16', '2019-09-30', 18, 6, true);
INSERT INTO sgnom.tsgnomquincena (cod_quincenaid, des_quincena, fec_inicio, fec_fin, fec_pago, fec_dispersion, cnu_numquincena, cod_ejercicioid_fk, bol_estatus) VALUES (19, '1era de octubre', '2019-10-01', '2019-10-15', '2019-10-01', '2019-10-15', 19, 6, true);
INSERT INTO sgnom.tsgnomquincena (cod_quincenaid, des_quincena, fec_inicio, fec_fin, fec_pago, fec_dispersion, cnu_numquincena, cod_ejercicioid_fk, bol_estatus) VALUES (20, '2da de octubre', '2019-10-16', '2019-10-31', '2019-10-16', '2019-10-31', 20, 6, true);
INSERT INTO sgnom.tsgnomquincena (cod_quincenaid, des_quincena, fec_inicio, fec_fin, fec_pago, fec_dispersion, cnu_numquincena, cod_ejercicioid_fk, bol_estatus) VALUES (21, '1era de noviembre', '2019-11-01', '2019-11-15', '2019-11-01', '2019-11-15', 21, 6, true);
INSERT INTO sgnom.tsgnomquincena (cod_quincenaid, des_quincena, fec_inicio, fec_fin, fec_pago, fec_dispersion, cnu_numquincena, cod_ejercicioid_fk, bol_estatus) VALUES (22, '2da de noviembre', '2019-11-16', '2019-11-30', '2019-11-16', '2019-11-30', 22, 6, true);
INSERT INTO sgnom.tsgnomquincena (cod_quincenaid, des_quincena, fec_inicio, fec_fin, fec_pago, fec_dispersion, cnu_numquincena, cod_ejercicioid_fk, bol_estatus) VALUES (23, '1era de diciembre', '2019-12-01', '2019-12-15', '2019-12-01', '2019-12-15', 23, 6, true);
INSERT INTO sgnom.tsgnomquincena (cod_quincenaid, des_quincena, fec_inicio, fec_fin, fec_pago, fec_dispersion, cnu_numquincena, cod_ejercicioid_fk, bol_estatus) VALUES (24, '2da de diciembre', '2019-12-16', '2019-12-31', '2019-12-16', '2019-12-31', 24, 6, true);


--
-- TOC entry 3766 (class 0 OID 97320)
-- Dependencies: 388
-- Data for Name: tsgnomtipoconcepto; Type: TABLE DATA; Schema: sgnom; Owner: suite
--

INSERT INTO sgnom.tsgnomtipoconcepto (cod_tipoconceptoid, cod_tipoconcepto, bol_estatus) VALUES (1, 'Deducción', true);
INSERT INTO sgnom.tsgnomtipoconcepto (cod_tipoconceptoid, cod_tipoconcepto, bol_estatus) VALUES (2, 'Percepción', true);


--
-- TOC entry 3767 (class 0 OID 97323)
-- Dependencies: 389
-- Data for Name: tsgnomtiponomina; Type: TABLE DATA; Schema: sgnom; Owner: suite
--

INSERT INTO sgnom.tsgnomtiponomina (cod_tiponominaid, cod_nomina, bol_estatus) VALUES (2, 'Aguinaldo', true);
INSERT INTO sgnom.tsgnomtiponomina (cod_tiponominaid, cod_nomina, bol_estatus) VALUES (1, 'General', true);
INSERT INTO sgnom.tsgnomtiponomina (cod_tiponominaid, cod_nomina, bol_estatus) VALUES (3, 'test', true);
INSERT INTO sgnom.tsgnomtiponomina (cod_tiponominaid, cod_nomina, bol_estatus) VALUES (4, 'mp', true);


--
-- TOC entry 3852 (class 0 OID 0)
-- Dependencies: 358
-- Name: seq_cabecera; Type: SEQUENCE SET; Schema: sgnom; Owner: suite
--

SELECT pg_catalog.setval('sgnom.seq_cabecera', 39, true);


--
-- TOC entry 3853 (class 0 OID 0)
-- Dependencies: 359
-- Name: seq_cncptoquinc; Type: SEQUENCE SET; Schema: sgnom; Owner: suite
--

SELECT pg_catalog.setval('sgnom.seq_cncptoquinc', 85, true);


--
-- TOC entry 3854 (class 0 OID 0)
-- Dependencies: 360
-- Name: seq_confpago; Type: SEQUENCE SET; Schema: sgnom; Owner: suite
--

SELECT pg_catalog.setval('sgnom.seq_confpago', 133, true);


--
-- TOC entry 3855 (class 0 OID 0)
-- Dependencies: 361
-- Name: seq_empquincena; Type: SEQUENCE SET; Schema: sgnom; Owner: suite
--

SELECT pg_catalog.setval('sgnom.seq_empquincena', 225, true);


--
-- TOC entry 3856 (class 0 OID 0)
-- Dependencies: 362
-- Name: seq_incidencia; Type: SEQUENCE SET; Schema: sgnom; Owner: suite
--

SELECT pg_catalog.setval('sgnom.seq_incidencia', 64, true);


--
-- TOC entry 3857 (class 0 OID 0)
-- Dependencies: 455
-- Name: seq_nom_imss; Type: SEQUENCE SET; Schema: sgnom; Owner: suite
--

SELECT pg_catalog.setval('sgnom.seq_nom_imss', 122, true);


--
-- TOC entry 3541 (class 2606 OID 97579)
-- Name: tsgnomcatincidencia cat_incidencia_id; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomcatincidencia
    ADD CONSTRAINT cat_incidencia_id PRIMARY KEY (cod_catincidenciaid);


--
-- TOC entry 3529 (class 2606 OID 97581)
-- Name: tsgnomaguinaldo nom_aguinaldo_id; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomaguinaldo
    ADD CONSTRAINT nom_aguinaldo_id PRIMARY KEY (cod_aguinaldoid);


--
-- TOC entry 3533 (class 2606 OID 97583)
-- Name: tsgnombitacora nom_bitacora_id; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnombitacora
    ADD CONSTRAINT nom_bitacora_id PRIMARY KEY (cod_bitacoraid);


--
-- TOC entry 3537 (class 2606 OID 97585)
-- Name: tsgnomcabeceraht nom_cabecera_copia_id; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomcabeceraht
    ADD CONSTRAINT nom_cabecera_copia_id PRIMARY KEY (cod_cabeceraid);


--
-- TOC entry 3535 (class 2606 OID 97587)
-- Name: tsgnomcabecera nom_cabecera_id; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomcabecera
    ADD CONSTRAINT nom_cabecera_id PRIMARY KEY (cod_cabeceraid);


--
-- TOC entry 3531 (class 2606 OID 97589)
-- Name: tsgnomargumento nom_cat_argumento_id; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomargumento
    ADD CONSTRAINT nom_cat_argumento_id PRIMARY KEY (cod_argumentoid);


--
-- TOC entry 3549 (class 2606 OID 97591)
-- Name: tsgnomconcepto nom_cat_concepto_id; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomconcepto
    ADD CONSTRAINT nom_cat_concepto_id PRIMARY KEY (cod_conceptoid);


--
-- TOC entry 3551 (class 2606 OID 97593)
-- Name: tsgnomconceptosat nom_cat_concepto_sat_id; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomconceptosat
    ADD CONSTRAINT nom_cat_concepto_sat_id PRIMARY KEY (cod_conceptosatid);


--
-- TOC entry 3555 (class 2606 OID 97595)
-- Name: tsgnomejercicio nom_cat_ejercicio_id; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomejercicio
    ADD CONSTRAINT nom_cat_ejercicio_id PRIMARY KEY (cod_ejercicioid);


--
-- TOC entry 3563 (class 2606 OID 97597)
-- Name: tsgnomestatusnom nom_cat_estatus_nomina_id; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomestatusnom
    ADD CONSTRAINT nom_cat_estatus_nomina_id PRIMARY KEY (cod_estatusnomid);


--
-- TOC entry 3565 (class 2606 OID 97599)
-- Name: tsgnomformula nom_cat_formula_id; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomformula
    ADD CONSTRAINT nom_cat_formula_id PRIMARY KEY (cod_formulaid);


--
-- TOC entry 3567 (class 2606 OID 97601)
-- Name: tsgnomfuncion nom_cat_funcion_id; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomfuncion
    ADD CONSTRAINT nom_cat_funcion_id PRIMARY KEY (cod_funcionid);


--
-- TOC entry 3577 (class 2606 OID 97603)
-- Name: tsgnomquincena nom_cat_quincena_id; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomquincena
    ADD CONSTRAINT nom_cat_quincena_id PRIMARY KEY (cod_quincenaid);


--
-- TOC entry 3569 (class 2606 OID 97605)
-- Name: tsgnomhisttabla nom_cat_tabla_id; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomhisttabla
    ADD CONSTRAINT nom_cat_tabla_id PRIMARY KEY (cod_tablaid);


--
-- TOC entry 3539 (class 2606 OID 97607)
-- Name: tsgnomcalculo nom_cat_tipo_calculo_id; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomcalculo
    ADD CONSTRAINT nom_cat_tipo_calculo_id PRIMARY KEY (cod_calculoid);


--
-- TOC entry 3543 (class 2606 OID 97609)
-- Name: tsgnomclasificador nom_cat_tipo_clasificador_id; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomclasificador
    ADD CONSTRAINT nom_cat_tipo_clasificador_id PRIMARY KEY (cod_clasificadorid);


--
-- TOC entry 3579 (class 2606 OID 97611)
-- Name: tsgnomtipoconcepto nom_cat_tipo_conepto_id; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomtipoconcepto
    ADD CONSTRAINT nom_cat_tipo_conepto_id PRIMARY KEY (cod_tipoconceptoid);


--
-- TOC entry 3581 (class 2606 OID 97613)
-- Name: tsgnomtiponomina nom_cat_tipo_nomina_id; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomtiponomina
    ADD CONSTRAINT nom_cat_tipo_nomina_id PRIMARY KEY (cod_tiponominaid);


--
-- TOC entry 3547 (class 2606 OID 97615)
-- Name: tsgnomcncptoquincht nom_conceptos_quincena_copia_id; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomcncptoquincht
    ADD CONSTRAINT nom_conceptos_quincena_copia_id PRIMARY KEY (cod_cncptoquinchtid);


--
-- TOC entry 3545 (class 2606 OID 97617)
-- Name: tsgnomcncptoquinc nom_conceptos_quincena_id; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomcncptoquinc
    ADD CONSTRAINT nom_conceptos_quincena_id PRIMARY KEY (cod_cncptoquincid);


--
-- TOC entry 3553 (class 2606 OID 97619)
-- Name: tsgnomconfpago nom_conf_pago_pkey; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomconfpago
    ADD CONSTRAINT nom_conf_pago_pkey PRIMARY KEY (cod_confpagoid);


--
-- TOC entry 3557 (class 2606 OID 97621)
-- Name: tsgnomempleados nom_empleado_id; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomempleados
    ADD CONSTRAINT nom_empleado_id PRIMARY KEY (cod_empleadoid);


--
-- TOC entry 3561 (class 2606 OID 97623)
-- Name: tsgnomempquincenaht nom_empleado_quincena_copia_id; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomempquincenaht
    ADD CONSTRAINT nom_empleado_quincena_copia_id PRIMARY KEY (cod_empquincenahtid);


--
-- TOC entry 3559 (class 2606 OID 97625)
-- Name: tsgnomempquincena nom_empleado_quincena_id; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomempquincena
    ADD CONSTRAINT nom_empleado_quincena_id PRIMARY KEY (cod_empquincenaid);


--
-- TOC entry 3571 (class 2606 OID 97627)
-- Name: tsgnomincidencia nom_incidencia_id; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomincidencia
    ADD CONSTRAINT nom_incidencia_id PRIMARY KEY (cod_incidenciaid);


--
-- TOC entry 3573 (class 2606 OID 97629)
-- Name: tsgnommanterceros nom_manuales_terceros_pkey; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnommanterceros
    ADD CONSTRAINT nom_manuales_terceros_pkey PRIMARY KEY (cod_mantercerosid);


--
-- TOC entry 3575 (class 2606 OID 97631)
-- Name: tsgnomnominaimss tsgnomnominaimss_pkey; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomnominaimss
    ADD CONSTRAINT tsgnomnominaimss_pkey PRIMARY KEY (cod_nominaimssid);


--
-- TOC entry 3613 (class 2606 OID 97740)
-- Name: tsgnomnominaimss cod_cabeceraid_fk; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomnominaimss
    ADD CONSTRAINT cod_cabeceraid_fk FOREIGN KEY (cod_cabeceraid_fk) REFERENCES sgnom.tsgnomcabecera(cod_cabeceraid);


--
-- TOC entry 3592 (class 2606 OID 97745)
-- Name: tsgnomcncptoquincht concepto_quincena_id_copia_fk_conceptos_quincena; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomcncptoquincht
    ADD CONSTRAINT concepto_quincena_id_copia_fk_conceptos_quincena FOREIGN KEY (cod_conceptoid_fk) REFERENCES sgnom.tsgnomconcepto(cod_conceptoid) ON UPDATE CASCADE;


--
-- TOC entry 3590 (class 2606 OID 97750)
-- Name: tsgnomcncptoquinc concepto_quincena_id_fk_conceptos_quincena; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomcncptoquinc
    ADD CONSTRAINT concepto_quincena_id_fk_conceptos_quincena FOREIGN KEY (cod_conceptoid_fk) REFERENCES sgnom.tsgnomconcepto(cod_conceptoid) ON UPDATE CASCADE;


--
-- TOC entry 3593 (class 2606 OID 97755)
-- Name: tsgnomcncptoquincht empleado_concepto_id_copia_fk_conceptos_quincena; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomcncptoquincht
    ADD CONSTRAINT empleado_concepto_id_copia_fk_conceptos_quincena FOREIGN KEY (cod_empquincenaid_fk) REFERENCES sgnom.tsgnomempquincena(cod_empquincenaid) ON UPDATE CASCADE;


--
-- TOC entry 3591 (class 2606 OID 97760)
-- Name: tsgnomcncptoquinc empleado_concepto_id_fk_conceptos_quincena; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomcncptoquinc
    ADD CONSTRAINT empleado_concepto_id_fk_conceptos_quincena FOREIGN KEY (cod_empquincenaid_fk) REFERENCES sgnom.tsgnomempquincena(cod_empquincenaid) ON UPDATE CASCADE;


--
-- TOC entry 3601 (class 2606 OID 97765)
-- Name: tsgnomempquincena nom_cabecera_id_fk_empleados_quincena; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomempquincena
    ADD CONSTRAINT nom_cabecera_id_fk_empleados_quincena FOREIGN KEY (cod_cabeceraid_fk) REFERENCES sgnom.tsgnomcabecera(cod_cabeceraid) ON UPDATE CASCADE;


--
-- TOC entry 3603 (class 2606 OID 97770)
-- Name: tsgnomempquincenaht nom_cabecera_id_fk_empleados_quincena_copia; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomempquincenaht
    ADD CONSTRAINT nom_cabecera_id_fk_empleados_quincena_copia FOREIGN KEY (cod_cabeceraid_fk) REFERENCES sgnom.tsgnomcabecera(cod_cabeceraid) ON UPDATE CASCADE;


--
-- TOC entry 3594 (class 2606 OID 97775)
-- Name: tsgnomconcepto nom_cat_clasificador_id_fk_cat_conceptos; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomconcepto
    ADD CONSTRAINT nom_cat_clasificador_id_fk_cat_conceptos FOREIGN KEY (cod_clasificadorid_fk) REFERENCES sgnom.tsgnomclasificador(cod_clasificadorid) ON UPDATE CASCADE;


--
-- TOC entry 3609 (class 2606 OID 97780)
-- Name: tsgnommanterceros nom_cat_conceptos_fk_manuales_terceros; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnommanterceros
    ADD CONSTRAINT nom_cat_conceptos_fk_manuales_terceros FOREIGN KEY (cod_conceptoid_fk) REFERENCES sgnom.tsgnomconcepto(cod_conceptoid) ON UPDATE CASCADE;


--
-- TOC entry 3614 (class 2606 OID 97785)
-- Name: tsgnomquincena nom_cat_ejercicio_id_fk_cat_quincenas; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomquincena
    ADD CONSTRAINT nom_cat_ejercicio_id_fk_cat_quincenas FOREIGN KEY (cod_ejercicioid_fk) REFERENCES sgnom.tsgnomejercicio(cod_ejercicioid) ON UPDATE CASCADE;


--
-- TOC entry 3587 (class 2606 OID 97790)
-- Name: tsgnomcabeceraht nom_cat_estatus_nomina_id_copia_fk_cabeceras; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomcabeceraht
    ADD CONSTRAINT nom_cat_estatus_nomina_id_copia_fk_cabeceras FOREIGN KEY (cod_estatusnomid_fk) REFERENCES sgnom.tsgnomestatusnom(cod_estatusnomid) ON UPDATE CASCADE;


--
-- TOC entry 3584 (class 2606 OID 97795)
-- Name: tsgnomcabecera nom_cat_estatus_nomina_id_fk_cabeceras; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomcabecera
    ADD CONSTRAINT nom_cat_estatus_nomina_id_fk_cabeceras FOREIGN KEY (cod_estatusnomid_fk) REFERENCES sgnom.tsgnomestatusnom(cod_estatusnomid) ON UPDATE CASCADE;


--
-- TOC entry 3605 (class 2606 OID 97800)
-- Name: tsgnomincidencia nom_cat_incidencia_id_fk_incidencias; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomincidencia
    ADD CONSTRAINT nom_cat_incidencia_id_fk_incidencias FOREIGN KEY (cod_catincidenciaid_fk) REFERENCES sgnom.tsgnomcatincidencia(cod_catincidenciaid) ON UPDATE CASCADE;


--
-- TOC entry 3588 (class 2606 OID 97805)
-- Name: tsgnomcabeceraht nom_cat_quincena_id_copia_fk_cabeceras; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomcabeceraht
    ADD CONSTRAINT nom_cat_quincena_id_copia_fk_cabeceras FOREIGN KEY (cod_quincenaid_fk) REFERENCES sgnom.tsgnomquincena(cod_quincenaid) ON UPDATE CASCADE;


--
-- TOC entry 3585 (class 2606 OID 97810)
-- Name: tsgnomcabecera nom_cat_quincena_id_fk_cabeceras; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomcabecera
    ADD CONSTRAINT nom_cat_quincena_id_fk_cabeceras FOREIGN KEY (cod_quincenaid_fk) REFERENCES sgnom.tsgnomquincena(cod_quincenaid) ON UPDATE CASCADE;


--
-- TOC entry 3606 (class 2606 OID 97815)
-- Name: tsgnomincidencia nom_cat_quincena_id_fk_incidencias; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomincidencia
    ADD CONSTRAINT nom_cat_quincena_id_fk_incidencias FOREIGN KEY (cod_quincenaid_fk) REFERENCES sgnom.tsgnomquincena(cod_quincenaid) ON UPDATE CASCADE;


--
-- TOC entry 3610 (class 2606 OID 97820)
-- Name: tsgnommanterceros nom_cat_quincenas_fk_manuales_terceros_fin; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnommanterceros
    ADD CONSTRAINT nom_cat_quincenas_fk_manuales_terceros_fin FOREIGN KEY (cod_quincenafin_fk) REFERENCES sgnom.tsgnomquincena(cod_quincenaid) ON UPDATE CASCADE;


--
-- TOC entry 3611 (class 2606 OID 97825)
-- Name: tsgnommanterceros nom_cat_quincenas_fk_manuales_terceros_inicio; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnommanterceros
    ADD CONSTRAINT nom_cat_quincenas_fk_manuales_terceros_inicio FOREIGN KEY (cod_quincenainicio_fk) REFERENCES sgnom.tsgnomquincena(cod_quincenaid) ON UPDATE CASCADE;


--
-- TOC entry 3583 (class 2606 OID 97830)
-- Name: tsgnombitacora nom_cat_tabla_id_fk_nom_cat_tablas; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnombitacora
    ADD CONSTRAINT nom_cat_tabla_id_fk_nom_cat_tablas FOREIGN KEY (cod_tablaid_fk) REFERENCES sgnom.tsgnomhisttabla(cod_tablaid) ON UPDATE CASCADE;


--
-- TOC entry 3589 (class 2606 OID 97835)
-- Name: tsgnomcabeceraht nom_cat_tipo_nomina_id_copia_fk_cabeceras; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomcabeceraht
    ADD CONSTRAINT nom_cat_tipo_nomina_id_copia_fk_cabeceras FOREIGN KEY (cod_tiponominaid_fk) REFERENCES sgnom.tsgnomtiponomina(cod_tiponominaid) ON UPDATE CASCADE;


--
-- TOC entry 3586 (class 2606 OID 97840)
-- Name: tsgnomcabecera nom_cat_tipo_nomina_id_fk_cabeceras; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomcabecera
    ADD CONSTRAINT nom_cat_tipo_nomina_id_fk_cabeceras FOREIGN KEY (cod_tiponominaid_fk) REFERENCES sgnom.tsgnomtiponomina(cod_tiponominaid) ON UPDATE CASCADE;


--
-- TOC entry 3595 (class 2606 OID 97845)
-- Name: tsgnomconcepto nom_concepto_sat_id_fk_cat_conceptos; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomconcepto
    ADD CONSTRAINT nom_concepto_sat_id_fk_cat_conceptos FOREIGN KEY (cod_conceptosatid_fk) REFERENCES sgnom.tsgnomconceptosat(cod_conceptosatid) ON UPDATE CASCADE;


--
-- TOC entry 3607 (class 2606 OID 97850)
-- Name: tsgnomincidencia nom_emp_autoriza_fk_incidencias; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomincidencia
    ADD CONSTRAINT nom_emp_autoriza_fk_incidencias FOREIGN KEY (cod_empautoriza_fk) REFERENCES sgnom.tsgnomempleados(cod_empleadoid) ON UPDATE CASCADE;


--
-- TOC entry 3608 (class 2606 OID 97855)
-- Name: tsgnomincidencia nom_emp_reporta_fk_incidencias; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomincidencia
    ADD CONSTRAINT nom_emp_reporta_fk_incidencias FOREIGN KEY (cod_empreporta_fk) REFERENCES sgnom.tsgnomempleados(cod_empleadoid) ON UPDATE CASCADE;


--
-- TOC entry 3602 (class 2606 OID 97860)
-- Name: tsgnomempquincena nom_empleado_quincena_id_fk_empleados_quincena; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomempquincena
    ADD CONSTRAINT nom_empleado_quincena_id_fk_empleados_quincena FOREIGN KEY (cod_empleadoid_fk) REFERENCES sgnom.tsgnomempleados(cod_empleadoid) ON UPDATE CASCADE;


--
-- TOC entry 3604 (class 2606 OID 97865)
-- Name: tsgnomempquincenaht nom_empleado_quincena_id_fk_empleados_quincena_copia; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomempquincenaht
    ADD CONSTRAINT nom_empleado_quincena_id_fk_empleados_quincena_copia FOREIGN KEY (cod_empleadoid_fk) REFERENCES sgnom.tsgnomempleados(cod_empleadoid) ON UPDATE CASCADE;


--
-- TOC entry 3612 (class 2606 OID 97870)
-- Name: tsgnommanterceros nom_empleados_fk_manuales_terceros; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnommanterceros
    ADD CONSTRAINT nom_empleados_fk_manuales_terceros FOREIGN KEY (cod_empleadoid_fk) REFERENCES sgnom.tsgnomempleados(cod_empleadoid) ON UPDATE CASCADE;


--
-- TOC entry 3582 (class 2606 OID 97875)
-- Name: tsgnomaguinaldo nom_empleados_quincena_fk_aguinaldos; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomaguinaldo
    ADD CONSTRAINT nom_empleados_quincena_fk_aguinaldos FOREIGN KEY (cod_empquincenaid_fk) REFERENCES sgnom.tsgnomempquincena(cod_empquincenaid) ON UPDATE CASCADE;


--
-- TOC entry 3600 (class 2606 OID 97880)
-- Name: tsgnomconfpago nom_empleados_quincena_fk_conf_pago; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomconfpago
    ADD CONSTRAINT nom_empleados_quincena_fk_conf_pago FOREIGN KEY (cod_empquincenaid_fk) REFERENCES sgnom.tsgnomempquincena(cod_empquincenaid) ON UPDATE CASCADE;


--
-- TOC entry 3596 (class 2606 OID 97885)
-- Name: tsgnomconcepto nom_formula_id_fk_cat_conceptos; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomconcepto
    ADD CONSTRAINT nom_formula_id_fk_cat_conceptos FOREIGN KEY (cod_formulaid_fk) REFERENCES sgnom.tsgnomformula(cod_formulaid) ON UPDATE CASCADE;


--
-- TOC entry 3597 (class 2606 OID 97890)
-- Name: tsgnomconcepto nom_tipo_calculo_id_fk_cat_conceptos; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomconcepto
    ADD CONSTRAINT nom_tipo_calculo_id_fk_cat_conceptos FOREIGN KEY (cod_calculoid_fk) REFERENCES sgnom.tsgnomcalculo(cod_calculoid) ON UPDATE CASCADE;


--
-- TOC entry 3598 (class 2606 OID 97895)
-- Name: tsgnomconcepto nom_tipo_concepto_id_fk_cat_conceptos; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomconcepto
    ADD CONSTRAINT nom_tipo_concepto_id_fk_cat_conceptos FOREIGN KEY (cod_tipoconceptoid_fk) REFERENCES sgnom.tsgnomtipoconcepto(cod_tipoconceptoid) ON UPDATE CASCADE;


--
-- TOC entry 3599 (class 2606 OID 97900)
-- Name: tsgnomconcepto nom_tipo_nomina_id_fk_cat_conceptos; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomconcepto
    ADD CONSTRAINT nom_tipo_nomina_id_fk_cat_conceptos FOREIGN KEY (cod_tiponominaid_fk) REFERENCES sgnom.tsgnomtiponomina(cod_tiponominaid) ON UPDATE CASCADE;


--
-- TOC entry 3775 (class 0 OID 0)
-- Dependencies: 11
-- Name: SCHEMA sgnom; Type: ACL; Schema: -; Owner: suite
--

REVOKE ALL ON SCHEMA sgnom FROM suite;
GRANT ALL ON SCHEMA sgnom TO suite WITH GRANT OPTION;


--
-- TOC entry 3776 (class 0 OID 0)
-- Dependencies: 479
-- Name: FUNCTION actualizar_comentarios_incidencia(incidenciaid integer, comentarios text, reporta integer); Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON FUNCTION sgnom.actualizar_comentarios_incidencia(incidenciaid integer, comentarios text, reporta integer) FROM suite;
GRANT ALL ON FUNCTION sgnom.actualizar_comentarios_incidencia(incidenciaid integer, comentarios text, reporta integer) TO suite WITH GRANT OPTION;


--
-- TOC entry 3777 (class 0 OID 0)
-- Dependencies: 480
-- Name: FUNCTION actualizar_importe_incidencia(incidenciaid integer, importe numeric, reporta integer); Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON FUNCTION sgnom.actualizar_importe_incidencia(incidenciaid integer, importe numeric, reporta integer) FROM suite;
GRANT ALL ON FUNCTION sgnom.actualizar_importe_incidencia(incidenciaid integer, importe numeric, reporta integer) TO suite WITH GRANT OPTION;


--
-- TOC entry 3778 (class 0 OID 0)
-- Dependencies: 481
-- Name: FUNCTION actualizar_incidencia(incidenciaid integer, comentarios text, importe numeric, reporta integer); Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON FUNCTION sgnom.actualizar_incidencia(incidenciaid integer, comentarios text, importe numeric, reporta integer) FROM suite;
GRANT ALL ON FUNCTION sgnom.actualizar_incidencia(incidenciaid integer, comentarios text, importe numeric, reporta integer) TO suite WITH GRANT OPTION;


--
-- TOC entry 3779 (class 0 OID 0)
-- Dependencies: 516
-- Name: FUNCTION altasvalidadas(); Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON FUNCTION sgnom.altasvalidadas() FROM suite;
GRANT ALL ON FUNCTION sgnom.altasvalidadas() TO suite WITH GRANT OPTION;


--
-- TOC entry 3780 (class 0 OID 0)
-- Dependencies: 488
-- Name: FUNCTION bajasvalidadas(); Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON FUNCTION sgnom.bajasvalidadas() FROM suite;
GRANT ALL ON FUNCTION sgnom.bajasvalidadas() TO suite WITH GRANT OPTION;


--
-- TOC entry 3781 (class 0 OID 0)
-- Dependencies: 517
-- Name: FUNCTION buscar_detalle_emp(cabecera integer); Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON FUNCTION sgnom.buscar_detalle_emp(cabecera integer) FROM suite;
GRANT ALL ON FUNCTION sgnom.buscar_detalle_emp(cabecera integer) TO suite WITH GRANT OPTION;


--
-- TOC entry 3782 (class 0 OID 0)
-- Dependencies: 491
-- Name: FUNCTION buscar_incidencias_por_empleado(idempleado integer); Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON FUNCTION sgnom.buscar_incidencias_por_empleado(idempleado integer) FROM suite;
GRANT ALL ON FUNCTION sgnom.buscar_incidencias_por_empleado(idempleado integer) TO suite WITH GRANT OPTION;


--
-- TOC entry 3783 (class 0 OID 0)
-- Dependencies: 493
-- Name: FUNCTION detalle_desglose_deduccion(cod_empleado integer, cod_cabecera integer); Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON FUNCTION sgnom.detalle_desglose_deduccion(cod_empleado integer, cod_cabecera integer) FROM suite;
GRANT ALL ON FUNCTION sgnom.detalle_desglose_deduccion(cod_empleado integer, cod_cabecera integer) TO suite WITH GRANT OPTION;


--
-- TOC entry 3784 (class 0 OID 0)
-- Dependencies: 518
-- Name: FUNCTION detalle_desglose_percepcion(cod_empleado integer, cod_cabecera integer); Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON FUNCTION sgnom.detalle_desglose_percepcion(cod_empleado integer, cod_cabecera integer) FROM suite;
GRANT ALL ON FUNCTION sgnom.detalle_desglose_percepcion(cod_empleado integer, cod_cabecera integer) TO suite WITH GRANT OPTION;


--
-- TOC entry 3785 (class 0 OID 0)
-- Dependencies: 495
-- Name: FUNCTION detallespersonal(); Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON FUNCTION sgnom.detallespersonal() FROM suite;
GRANT ALL ON FUNCTION sgnom.detallespersonal() TO suite WITH GRANT OPTION;


--
-- TOC entry 3786 (class 0 OID 0)
-- Dependencies: 519
-- Name: FUNCTION eliminar_incidencia_por_empleado(incidenciaid integer, reporta integer); Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON FUNCTION sgnom.eliminar_incidencia_por_empleado(incidenciaid integer, reporta integer) FROM suite;
GRANT ALL ON FUNCTION sgnom.eliminar_incidencia_por_empleado(incidenciaid integer, reporta integer) TO suite WITH GRANT OPTION;


--
-- TOC entry 3787 (class 0 OID 0)
-- Dependencies: 496
-- Name: FUNCTION empleado_confpago(cabecera integer, empleado integer); Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON FUNCTION sgnom.empleado_confpago(cabecera integer, empleado integer) FROM suite;
GRANT ALL ON FUNCTION sgnom.empleado_confpago(cabecera integer, empleado integer) TO suite WITH GRANT OPTION;


--
-- TOC entry 3788 (class 0 OID 0)
-- Dependencies: 520
-- Name: FUNCTION empleados_por_cabecera(idcabecera integer); Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON FUNCTION sgnom.empleados_por_cabecera(idcabecera integer) FROM suite;
GRANT ALL ON FUNCTION sgnom.empleados_por_cabecera(idcabecera integer) TO suite WITH GRANT OPTION;


--
-- TOC entry 3789 (class 0 OID 0)
-- Dependencies: 497
-- Name: FUNCTION fn_calcula_importes_nomina(idnomina integer, vempleados character varying, todos numeric); Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON FUNCTION sgnom.fn_calcula_importes_nomina(idnomina integer, vempleados character varying, todos numeric) FROM suite;
GRANT ALL ON FUNCTION sgnom.fn_calcula_importes_nomina(idnomina integer, vempleados character varying, todos numeric) TO suite WITH GRANT OPTION;


--
-- TOC entry 3790 (class 0 OID 0)
-- Dependencies: 521
-- Name: FUNCTION fn_calcula_nomina(vidnomina integer); Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON FUNCTION sgnom.fn_calcula_nomina(vidnomina integer) FROM suite;
GRANT ALL ON FUNCTION sgnom.fn_calcula_nomina(vidnomina integer) TO suite WITH GRANT OPTION;


--
-- TOC entry 3791 (class 0 OID 0)
-- Dependencies: 523
-- Name: FUNCTION fn_dias_laborados(empid integer, prmnumquincenacalculo integer); Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON FUNCTION sgnom.fn_dias_laborados(empid integer, prmnumquincenacalculo integer) FROM suite;
GRANT ALL ON FUNCTION sgnom.fn_dias_laborados(empid integer, prmnumquincenacalculo integer) TO suite WITH GRANT OPTION;


--
-- TOC entry 3792 (class 0 OID 0)
-- Dependencies: 524
-- Name: FUNCTION fn_incidencias_por_quincena(quincena integer); Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON FUNCTION sgnom.fn_incidencias_por_quincena(quincena integer) FROM suite;
GRANT ALL ON FUNCTION sgnom.fn_incidencias_por_quincena(quincena integer) TO suite WITH GRANT OPTION;


--
-- TOC entry 3793 (class 0 OID 0)
-- Dependencies: 539
-- Name: FUNCTION fn_insertacabecera(quincena integer, tipo integer, nombre character varying, credopor integer); Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON FUNCTION sgnom.fn_insertacabecera(quincena integer, tipo integer, nombre character varying, credopor integer) FROM suite;
GRANT ALL ON FUNCTION sgnom.fn_insertacabecera(quincena integer, tipo integer, nombre character varying, credopor integer) TO suite WITH GRANT OPTION;


--
-- TOC entry 3794 (class 0 OID 0)
-- Dependencies: 527
-- Name: FUNCTION fn_insertacabecera_buena(quincena integer, tipo integer, nombre character varying, credopor integer); Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON FUNCTION sgnom.fn_insertacabecera_buena(quincena integer, tipo integer, nombre character varying, credopor integer) FROM suite;
GRANT ALL ON FUNCTION sgnom.fn_insertacabecera_buena(quincena integer, tipo integer, nombre character varying, credopor integer) TO suite WITH GRANT OPTION;


--
-- TOC entry 3795 (class 0 OID 0)
-- Dependencies: 525
-- Name: FUNCTION fn_sueldo_base(empid integer); Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON FUNCTION sgnom.fn_sueldo_base(empid integer) FROM suite;
GRANT ALL ON FUNCTION sgnom.fn_sueldo_base(empid integer) TO suite WITH GRANT OPTION;


--
-- TOC entry 3796 (class 0 OID 0)
-- Dependencies: 499
-- Name: FUNCTION fn_validapagosnomina(cabecera integer); Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON FUNCTION sgnom.fn_validapagosnomina(cabecera integer) FROM suite;
GRANT ALL ON FUNCTION sgnom.fn_validapagosnomina(cabecera integer) TO suite WITH GRANT OPTION;


--
-- TOC entry 3797 (class 0 OID 0)
-- Dependencies: 526
-- Name: FUNCTION historialquincenasemp(empleado integer); Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON FUNCTION sgnom.historialquincenasemp(empleado integer) FROM suite;
GRANT ALL ON FUNCTION sgnom.historialquincenasemp(empleado integer) TO suite WITH GRANT OPTION;


--
-- TOC entry 3798 (class 0 OID 0)
-- Dependencies: 522
-- Name: FUNCTION incidencias_por_area(area integer); Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON FUNCTION sgnom.incidencias_por_area(area integer) FROM suite;
GRANT ALL ON FUNCTION sgnom.incidencias_por_area(area integer) TO suite WITH GRANT OPTION;


--
-- TOC entry 3799 (class 0 OID 0)
-- Dependencies: 501
-- Name: FUNCTION incidencias_quincena(); Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON FUNCTION sgnom.incidencias_quincena() FROM suite;
GRANT ALL ON FUNCTION sgnom.incidencias_quincena() TO suite WITH GRANT OPTION;


--
-- TOC entry 3800 (class 0 OID 0)
-- Dependencies: 528
-- Name: FUNCTION insertar_incidencia_por_empleado(incidenciaid integer, cantidad integer, actividad character varying, comentarios text, reporta integer, crea integer, monto numeric, fechas character varying); Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON FUNCTION sgnom.insertar_incidencia_por_empleado(incidenciaid integer, cantidad integer, actividad character varying, comentarios text, reporta integer, crea integer, monto numeric, fechas character varying) FROM suite;
GRANT ALL ON FUNCTION sgnom.insertar_incidencia_por_empleado(incidenciaid integer, cantidad integer, actividad character varying, comentarios text, reporta integer, crea integer, monto numeric, fechas character varying) TO suite WITH GRANT OPTION;


--
-- TOC entry 3801 (class 0 OID 0)
-- Dependencies: 529
-- Name: FUNCTION totalimpcab(id integer); Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON FUNCTION sgnom.totalimpcab(id integer) FROM suite;
GRANT ALL ON FUNCTION sgnom.totalimpcab(id integer) TO suite WITH GRANT OPTION;


--
-- TOC entry 3802 (class 0 OID 0)
-- Dependencies: 502
-- Name: FUNCTION validaraltas(); Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON FUNCTION sgnom.validaraltas() FROM suite;
GRANT ALL ON FUNCTION sgnom.validaraltas() TO suite WITH GRANT OPTION;


--
-- TOC entry 3803 (class 0 OID 0)
-- Dependencies: 530
-- Name: FUNCTION validarbajas(); Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON FUNCTION sgnom.validarbajas() FROM suite;
GRANT ALL ON FUNCTION sgnom.validarbajas() TO suite WITH GRANT OPTION;


--
-- TOC entry 3804 (class 0 OID 0)
-- Dependencies: 531
-- Name: FUNCTION verinformaciondepersonal(empleado integer); Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON FUNCTION sgnom.verinformaciondepersonal(empleado integer) FROM suite;
GRANT ALL ON FUNCTION sgnom.verinformaciondepersonal(empleado integer) TO suite WITH GRANT OPTION;


--
-- TOC entry 3805 (class 0 OID 0)
-- Dependencies: 358
-- Name: SEQUENCE seq_cabecera; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON SEQUENCE sgnom.seq_cabecera FROM suite;
GRANT UPDATE ON SEQUENCE sgnom.seq_cabecera TO suite;
GRANT SELECT,USAGE ON SEQUENCE sgnom.seq_cabecera TO suite WITH GRANT OPTION;


--
-- TOC entry 3806 (class 0 OID 0)
-- Dependencies: 359
-- Name: SEQUENCE seq_cncptoquinc; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON SEQUENCE sgnom.seq_cncptoquinc FROM suite;
GRANT UPDATE ON SEQUENCE sgnom.seq_cncptoquinc TO suite;
GRANT SELECT,USAGE ON SEQUENCE sgnom.seq_cncptoquinc TO suite WITH GRANT OPTION;


--
-- TOC entry 3807 (class 0 OID 0)
-- Dependencies: 360
-- Name: SEQUENCE seq_confpago; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON SEQUENCE sgnom.seq_confpago FROM suite;
GRANT UPDATE ON SEQUENCE sgnom.seq_confpago TO suite;
GRANT SELECT,USAGE ON SEQUENCE sgnom.seq_confpago TO suite WITH GRANT OPTION;


--
-- TOC entry 3808 (class 0 OID 0)
-- Dependencies: 361
-- Name: SEQUENCE seq_empquincena; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON SEQUENCE sgnom.seq_empquincena FROM suite;
GRANT UPDATE ON SEQUENCE sgnom.seq_empquincena TO suite;
GRANT SELECT,USAGE ON SEQUENCE sgnom.seq_empquincena TO suite WITH GRANT OPTION;


--
-- TOC entry 3809 (class 0 OID 0)
-- Dependencies: 362
-- Name: SEQUENCE seq_incidencia; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON SEQUENCE sgnom.seq_incidencia FROM suite;
GRANT UPDATE ON SEQUENCE sgnom.seq_incidencia TO suite;
GRANT SELECT,USAGE ON SEQUENCE sgnom.seq_incidencia TO suite WITH GRANT OPTION;


--
-- TOC entry 3811 (class 0 OID 0)
-- Dependencies: 363
-- Name: TABLE tsgnomaguinaldo; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnomaguinaldo FROM suite;
GRANT ALL ON TABLE sgnom.tsgnomaguinaldo TO suite WITH GRANT OPTION;


--
-- TOC entry 3812 (class 0 OID 0)
-- Dependencies: 364
-- Name: TABLE tsgnomargumento; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnomargumento FROM suite;
GRANT ALL ON TABLE sgnom.tsgnomargumento TO suite WITH GRANT OPTION;


--
-- TOC entry 3813 (class 0 OID 0)
-- Dependencies: 365
-- Name: TABLE tsgnombitacora; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnombitacora FROM suite;
GRANT ALL ON TABLE sgnom.tsgnombitacora TO suite WITH GRANT OPTION;


--
-- TOC entry 3814 (class 0 OID 0)
-- Dependencies: 366
-- Name: TABLE tsgnomcabecera; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnomcabecera FROM suite;
GRANT ALL ON TABLE sgnom.tsgnomcabecera TO suite WITH GRANT OPTION;


--
-- TOC entry 3815 (class 0 OID 0)
-- Dependencies: 367
-- Name: TABLE tsgnomcabeceraht; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnomcabeceraht FROM suite;
GRANT ALL ON TABLE sgnom.tsgnomcabeceraht TO suite WITH GRANT OPTION;


--
-- TOC entry 3817 (class 0 OID 0)
-- Dependencies: 368
-- Name: TABLE tsgnomcalculo; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnomcalculo FROM suite;
GRANT ALL ON TABLE sgnom.tsgnomcalculo TO suite WITH GRANT OPTION;


--
-- TOC entry 3819 (class 0 OID 0)
-- Dependencies: 369
-- Name: TABLE tsgnomcatincidencia; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnomcatincidencia FROM suite;
GRANT ALL ON TABLE sgnom.tsgnomcatincidencia TO suite WITH GRANT OPTION;


--
-- TOC entry 3820 (class 0 OID 0)
-- Dependencies: 370
-- Name: TABLE tsgnomclasificador; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnomclasificador FROM suite;
GRANT ALL ON TABLE sgnom.tsgnomclasificador TO suite WITH GRANT OPTION;


--
-- TOC entry 3821 (class 0 OID 0)
-- Dependencies: 371
-- Name: TABLE tsgnomcncptoquinc; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnomcncptoquinc FROM suite;
GRANT ALL ON TABLE sgnom.tsgnomcncptoquinc TO suite WITH GRANT OPTION;


--
-- TOC entry 3822 (class 0 OID 0)
-- Dependencies: 372
-- Name: TABLE tsgnomcncptoquincht; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnomcncptoquincht FROM suite;
GRANT ALL ON TABLE sgnom.tsgnomcncptoquincht TO suite WITH GRANT OPTION;


--
-- TOC entry 3824 (class 0 OID 0)
-- Dependencies: 373
-- Name: TABLE tsgnomconcepto; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnomconcepto FROM suite;
GRANT ALL ON TABLE sgnom.tsgnomconcepto TO suite WITH GRANT OPTION;


--
-- TOC entry 3826 (class 0 OID 0)
-- Dependencies: 374
-- Name: TABLE tsgnomconceptosat; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnomconceptosat FROM suite;
GRANT ALL ON TABLE sgnom.tsgnomconceptosat TO suite WITH GRANT OPTION;


--
-- TOC entry 3828 (class 0 OID 0)
-- Dependencies: 375
-- Name: TABLE tsgnomconfpago; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnomconfpago FROM suite;
GRANT ALL ON TABLE sgnom.tsgnomconfpago TO suite WITH GRANT OPTION;


--
-- TOC entry 3829 (class 0 OID 0)
-- Dependencies: 376
-- Name: TABLE tsgnomejercicio; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnomejercicio FROM suite;
GRANT ALL ON TABLE sgnom.tsgnomejercicio TO suite WITH GRANT OPTION;


--
-- TOC entry 3831 (class 0 OID 0)
-- Dependencies: 377
-- Name: TABLE tsgnomempleados; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnomempleados FROM suite;
GRANT ALL ON TABLE sgnom.tsgnomempleados TO suite WITH GRANT OPTION;


--
-- TOC entry 3833 (class 0 OID 0)
-- Dependencies: 378
-- Name: TABLE tsgnomempquincena; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnomempquincena FROM suite;
GRANT ALL ON TABLE sgnom.tsgnomempquincena TO suite WITH GRANT OPTION;


--
-- TOC entry 3834 (class 0 OID 0)
-- Dependencies: 379
-- Name: TABLE tsgnomempquincenaht; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnomempquincenaht FROM suite;
GRANT ALL ON TABLE sgnom.tsgnomempquincenaht TO suite WITH GRANT OPTION;


--
-- TOC entry 3836 (class 0 OID 0)
-- Dependencies: 380
-- Name: TABLE tsgnomestatusnom; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnomestatusnom FROM suite;
GRANT ALL ON TABLE sgnom.tsgnomestatusnom TO suite WITH GRANT OPTION;


--
-- TOC entry 3838 (class 0 OID 0)
-- Dependencies: 381
-- Name: TABLE tsgnomformula; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnomformula FROM suite;
GRANT ALL ON TABLE sgnom.tsgnomformula TO suite WITH GRANT OPTION;


--
-- TOC entry 3839 (class 0 OID 0)
-- Dependencies: 382
-- Name: TABLE tsgnomfuncion; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnomfuncion FROM suite;
GRANT ALL ON TABLE sgnom.tsgnomfuncion TO suite WITH GRANT OPTION;


--
-- TOC entry 3840 (class 0 OID 0)
-- Dependencies: 383
-- Name: TABLE tsgnomhisttabla; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnomhisttabla FROM suite;
GRANT ALL ON TABLE sgnom.tsgnomhisttabla TO suite WITH GRANT OPTION;


--
-- TOC entry 3844 (class 0 OID 0)
-- Dependencies: 384
-- Name: TABLE tsgnomincidencia; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnomincidencia FROM suite;
GRANT ALL ON TABLE sgnom.tsgnomincidencia TO suite WITH GRANT OPTION;


--
-- TOC entry 3845 (class 0 OID 0)
-- Dependencies: 385
-- Name: TABLE tsgnommanterceros; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnommanterceros FROM suite;
GRANT ALL ON TABLE sgnom.tsgnommanterceros TO suite WITH GRANT OPTION;


--
-- TOC entry 3846 (class 0 OID 0)
-- Dependencies: 386
-- Name: TABLE tsgnomnominaimss; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnomnominaimss FROM suite;
GRANT ALL ON TABLE sgnom.tsgnomnominaimss TO suite WITH GRANT OPTION;


--
-- TOC entry 3847 (class 0 OID 0)
-- Dependencies: 387
-- Name: TABLE tsgnomquincena; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnomquincena FROM suite;
GRANT ALL ON TABLE sgnom.tsgnomquincena TO suite WITH GRANT OPTION;


--
-- TOC entry 3849 (class 0 OID 0)
-- Dependencies: 388
-- Name: TABLE tsgnomtipoconcepto; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnomtipoconcepto FROM suite;
GRANT ALL ON TABLE sgnom.tsgnomtipoconcepto TO suite WITH GRANT OPTION;


--
-- TOC entry 3851 (class 0 OID 0)
-- Dependencies: 389
-- Name: TABLE tsgnomtiponomina; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnomtiponomina FROM suite;
GRANT ALL ON TABLE sgnom.tsgnomtiponomina TO suite WITH GRANT OPTION;


--
-- TOC entry 2496 (class 826 OID 98872)
-- Name: DEFAULT PRIVILEGES FOR SEQUENCES; Type: DEFAULT ACL; Schema: sgnom; Owner: suite
--

ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sgnom REVOKE ALL ON SEQUENCES  FROM suite;
ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sgnom GRANT SELECT,USAGE ON SEQUENCES  TO suite WITH GRANT OPTION;


--
-- TOC entry 2497 (class 826 OID 98873)
-- Name: DEFAULT PRIVILEGES FOR TYPES; Type: DEFAULT ACL; Schema: sgnom; Owner: suite
--

ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sgnom REVOKE ALL ON TYPES  FROM PUBLIC;
ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sgnom REVOKE ALL ON TYPES  FROM suite;
ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sgnom GRANT ALL ON TYPES  TO suite WITH GRANT OPTION;


--
-- TOC entry 2498 (class 826 OID 98874)
-- Name: DEFAULT PRIVILEGES FOR FUNCTIONS; Type: DEFAULT ACL; Schema: sgnom; Owner: suite
--

ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sgnom REVOKE ALL ON FUNCTIONS  FROM PUBLIC;
ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sgnom REVOKE ALL ON FUNCTIONS  FROM suite;
ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sgnom GRANT ALL ON FUNCTIONS  TO suite WITH GRANT OPTION;


--
-- TOC entry 2499 (class 826 OID 98875)
-- Name: DEFAULT PRIVILEGES FOR TABLES; Type: DEFAULT ACL; Schema: sgnom; Owner: suite
--

ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sgnom REVOKE ALL ON TABLES  FROM suite;
ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sgnom GRANT ALL ON TABLES  TO suite WITH GRANT OPTION;


-- Completed on 2019-11-21 11:34:16

--
-- PostgreSQL database dump complete
--

