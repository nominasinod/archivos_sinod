--
-- PostgreSQL database dump
--

-- Dumped from database version 11.5
-- Dumped by pg_dump version 11.5

-- Started on 2019-12-10 16:28:12

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 22 (class 2615 OID 99010)
-- Name: sgco; Type: SCHEMA; Schema: -; Owner: suite
--

CREATE SCHEMA sgco;


ALTER SCHEMA sgco OWNER TO suite;

--
-- TOC entry 4880 (class 0 OID 0)
-- Dependencies: 22
-- Name: SCHEMA sgco; Type: COMMENT; Schema: -; Owner: suite
--

COMMENT ON SCHEMA sgco IS 'Sistema de Gestion de Conocimiento de la Organizacion.';


--
-- TOC entry 26 (class 2615 OID 101941)
-- Name: sgnom; Type: SCHEMA; Schema: -; Owner: suite
--

CREATE SCHEMA sgnom;


ALTER SCHEMA sgnom OWNER TO suite;

--
-- TOC entry 4882 (class 0 OID 0)
-- Dependencies: 26
-- Name: SCHEMA sgnom; Type: COMMENT; Schema: -; Owner: suite
--

COMMENT ON SCHEMA sgnom IS 'Sistema de Gestion de Nomina.';


--
-- TOC entry 13 (class 2615 OID 99012)
-- Name: sgrh; Type: SCHEMA; Schema: -; Owner: suite
--

CREATE SCHEMA sgrh;


ALTER SCHEMA sgrh OWNER TO suite;

--
-- TOC entry 4884 (class 0 OID 0)
-- Dependencies: 13
-- Name: SCHEMA sgrh; Type: COMMENT; Schema: -; Owner: suite
--

COMMENT ON SCHEMA sgrh IS 'ESQUEMA QUE CONTIENE LAS TABLAS DE SISTEMA DE GESTION DE RECURSOS HUMANOS';


--
-- TOC entry 7 (class 2615 OID 99013)
-- Name: sgrt; Type: SCHEMA; Schema: -; Owner: suite
--

CREATE SCHEMA sgrt;


ALTER SCHEMA sgrt OWNER TO suite;

--
-- TOC entry 15 (class 2615 OID 102646)
-- Name: sisat; Type: SCHEMA; Schema: -; Owner: suite
--

CREATE SCHEMA sisat;


ALTER SCHEMA sisat OWNER TO suite;

--
-- TOC entry 4886 (class 0 OID 0)
-- Dependencies: 15
-- Name: SCHEMA sisat; Type: COMMENT; Schema: -; Owner: suite
--

COMMENT ON SCHEMA sisat IS 'Esquema que contiene las tablas del Sistema de Seleccion y Adquisicion de Talentos';


--
-- TOC entry 3 (class 3079 OID 102498)
-- Name: pldbgapi; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS pldbgapi WITH SCHEMA sgnom;


--
-- TOC entry 4887 (class 0 OID 0)
-- Dependencies: 3
-- Name: EXTENSION pldbgapi; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION pldbgapi IS 'server-side support for debugging PL/pgSQL functions';


--
-- TOC entry 2 (class 3079 OID 99015)
-- Name: tablefunc; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS tablefunc WITH SCHEMA public;


--
-- TOC entry 4888 (class 0 OID 0)
-- Dependencies: 2
-- Name: EXTENSION tablefunc; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION tablefunc IS 'Functions that manipulate whole tables, including crosstab';


--
-- TOC entry 1493 (class 1247 OID 99037)
-- Name: edo_encuesta; Type: TYPE; Schema: sgrh; Owner: suite
--

CREATE TYPE sgrh.edo_encuesta AS ENUM (
    '--',
    'En proceso',
    'Corregido',
    'Aceptado'
);


ALTER TYPE sgrh.edo_encuesta OWNER TO suite;

--
-- TOC entry 1494 (class 1247 OID 99046)
-- Name: destinatario; Type: TYPE; Schema: sgrt; Owner: suite
--

CREATE TYPE sgrt.destinatario AS ENUM (
    'USR',
    'EMPLEADO',
    'GRUPO',
    'DEPTO'
);


ALTER TYPE sgrt.destinatario OWNER TO suite;

--
-- TOC entry 1495 (class 1247 OID 99056)
-- Name: edoticket; Type: TYPE; Schema: sgrt; Owner: suite
--

CREATE TYPE sgrt.edoticket AS ENUM (
    'Abierto',
    'Cerrado'
);


ALTER TYPE sgrt.edoticket OWNER TO suite;

--
-- TOC entry 1496 (class 1247 OID 99062)
-- Name: encriptacion; Type: TYPE; Schema: sgrt; Owner: suite
--

CREATE TYPE sgrt.encriptacion AS ENUM (
    'NONE',
    'SSL'
);


ALTER TYPE sgrt.encriptacion OWNER TO suite;

--
-- TOC entry 1497 (class 1247 OID 99068)
-- Name: estatus; Type: TYPE; Schema: sgrt; Owner: suite
--

CREATE TYPE sgrt.estatus AS ENUM (
    'Enviado',
    'Pendiente'
);


ALTER TYPE sgrt.estatus OWNER TO suite;

--
-- TOC entry 1498 (class 1247 OID 99074)
-- Name: estatus_compromiso; Type: TYPE; Schema: sgrt; Owner: suite
--

CREATE TYPE sgrt.estatus_compromiso AS ENUM (
    'Pendiente',
    'Terminado'
);


ALTER TYPE sgrt.estatus_compromiso OWNER TO suite;

--
-- TOC entry 1499 (class 1247 OID 99080)
-- Name: modulo; Type: TYPE; Schema: sgrt; Owner: suite
--

CREATE TYPE sgrt.modulo AS ENUM (
    'SGRT',
    'SSV'
);


ALTER TYPE sgrt.modulo OWNER TO suite;

--
-- TOC entry 1500 (class 1247 OID 99086)
-- Name: origencontac; Type: TYPE; Schema: sgrt; Owner: suite
--

CREATE TYPE sgrt.origencontac AS ENUM (
    'Web',
    'Email',
    'Reunion',
    'Telefono',
    'Otro'
);


ALTER TYPE sgrt.origencontac OWNER TO suite;

--
-- TOC entry 1501 (class 1247 OID 99098)
-- Name: prioridad; Type: TYPE; Schema: sgrt; Owner: suite
--

CREATE TYPE sgrt.prioridad AS ENUM (
    'Alta',
    'Media',
    'Baja'
);


ALTER TYPE sgrt.prioridad OWNER TO suite;

--
-- TOC entry 1502 (class 1247 OID 99106)
-- Name: protocolo; Type: TYPE; Schema: sgrt; Owner: suite
--

CREATE TYPE sgrt.protocolo AS ENUM (
    'POP',
    'IMAP'
);


ALTER TYPE sgrt.protocolo OWNER TO suite;

--
-- TOC entry 1503 (class 1247 OID 99112)
-- Name: tipo; Type: TYPE; Schema: sgrt; Owner: suite
--

CREATE TYPE sgrt.tipo AS ENUM (
    'ReunionAgendada',
    'ReunionCancelada',
    'NuevaSolicitud',
    'SolicitudPagada',
    'ViaticoCancelado'
);


ALTER TYPE sgrt.tipo OWNER TO suite;

--
-- TOC entry 1504 (class 1247 OID 99124)
-- Name: tipo_compromiso; Type: TYPE; Schema: sgrt; Owner: suite
--

CREATE TYPE sgrt.tipo_compromiso AS ENUM (
    'Acuerdo',
    'Pendiente'
);


ALTER TYPE sgrt.tipo_compromiso OWNER TO suite;

--
-- TOC entry 521 (class 1255 OID 101942)
-- Name: actualizar_comentarios_incidencia(integer, text, integer); Type: FUNCTION; Schema: sgnom; Owner: suite
--

CREATE FUNCTION sgnom.actualizar_comentarios_incidencia(incidenciaid integer, comentarios text, reporta integer) RETURNS boolean
    LANGUAGE sql
    AS $$
		UPDATE sgnom.tsgnomincidencia 
			SET txt_comentarios = comentarios,
			aud_codmodificadopor = reporta,
			aud_fecmodificacion = CURRENT_DATE
		WHERE cod_incidenciaid = incidenciaid RETURNING TRUE; 
$$;


ALTER FUNCTION sgnom.actualizar_comentarios_incidencia(incidenciaid integer, comentarios text, reporta integer) OWNER TO suite;

--
-- TOC entry 515 (class 1255 OID 101943)
-- Name: actualizar_importe_incidencia(integer, numeric, integer); Type: FUNCTION; Schema: sgnom; Owner: suite
--

CREATE FUNCTION sgnom.actualizar_importe_incidencia(incidenciaid integer, importe numeric, reporta integer) RETURNS boolean
    LANGUAGE sql
    AS $$
		UPDATE sgnom.tsgnomincidencia 
			SET imp_monto = importe,
			aud_codmodificadopor = reporta,
			aud_fecmodificacion = CURRENT_DATE
		WHERE "cod_incidenciaid" = incidenciaid RETURNING TRUE; 
$$;


ALTER FUNCTION sgnom.actualizar_importe_incidencia(incidenciaid integer, importe numeric, reporta integer) OWNER TO suite;

--
-- TOC entry 520 (class 1255 OID 101944)
-- Name: actualizar_incidencia(integer, text, numeric, integer); Type: FUNCTION; Schema: sgnom; Owner: suite
--

CREATE FUNCTION sgnom.actualizar_incidencia(incidenciaid integer, comentarios text, importe numeric, reporta integer) RETURNS boolean
    LANGUAGE sql
    AS $$
		UPDATE sgnom.tsgnomincidencia 
			SET txt_comentarios = comentarios,
			imp_monto = importe,
			aud_codmodificadopor = reporta,
			aud_fecmodificacion = CURRENT_DATE
		WHERE cod_incidenciaid = incidenciaid RETURNING TRUE; 
$$;


ALTER FUNCTION sgnom.actualizar_incidencia(incidenciaid integer, comentarios text, importe numeric, reporta integer) OWNER TO suite;

--
-- TOC entry 576 (class 1255 OID 102538)
-- Name: altasvalidadas(); Type: FUNCTION; Schema: sgnom; Owner: suite
--

CREATE FUNCTION sgnom.altasvalidadas() RETURNS TABLE(cod_empleadoid integer, nom_empleado text, cod_rfc character varying, cod_curp character varying, des_nbarea character varying, des_puesto character varying, validar boolean, fecha_validacion text)
    LANGUAGE plpgsql COST 10
    AS $$
BEGIN 
RETURN QUERY 
 SELECT nomempleados.cod_empleadoid,
    (((rhempleados.des_nombre::text || ' '::text) || rhempleados.des_apepaterno::text) || ' '::text) || rhempleados.des_apematerno::text AS nom_empleado,
    rhempleados.cod_rfc,
	rhempleados.cod_curp,
    rharea.des_nbarea,
    rhpuestos.des_puesto,
	nomempleados.des_validacion,
	to_char(nomempleados.aud_fecvalidacion,'dd/MM/yyyy')as fecha_validacion
   FROM sgnom.tsgnomempleados nomempleados
     JOIN sgrh.tsgrhempleados rhempleados ON nomempleados.cod_empleado_fk = rhempleados.cod_empleado
     JOIN sgrh.tsgrhareas rharea ON rhempleados.cod_area = rharea.cod_area
     JOIN sgrh.tsgrhpuestos rhpuestos ON rhempleados.cod_puesto = rhpuestos.cod_puesto 
  WHERE  nomempleados.des_validacion is not null 
  GROUP BY nomempleados.cod_empleadoid, ((((rhempleados.des_nombre::text || ' '::text) || rhempleados.des_apepaterno::text) || ' '::text) || rhempleados.des_apematerno::text), rhempleados.cod_rfc, 
  rhempleados.cod_curp,rharea.des_nbarea, rhpuestos.des_puesto
  ORDER BY nomempleados.cod_empleadoid;
END;
$$;


ALTER FUNCTION sgnom.altasvalidadas() OWNER TO suite;

--
-- TOC entry 572 (class 1255 OID 102477)
-- Name: bajasvalidadas(); Type: FUNCTION; Schema: sgnom; Owner: suite
--

CREATE FUNCTION sgnom.bajasvalidadas() RETURNS TABLE(cod_empleadoid integer, nom_empleado text, cod_rfc character varying, des_nbarea character varying, des_puesto character varying, validar boolean, fecha_validacion text)
    LANGUAGE plpgsql COST 10
    AS $$
BEGIN 
RETURN QUERY 
 SELECT nomempleados.cod_empleadoid,
    (((rhempleados.des_nombre::text || ' '::text) || rhempleados.des_apepaterno::text) || ' '::text) || rhempleados.des_apematerno::text AS nom_empleado,
    rhempleados.cod_rfc,
    rharea.des_nbarea,
    rhpuestos.des_puesto,
	nomempleados.des_validacion,
	to_char(nomempleados.aud_fecvalidacion,'dd/MM/yyyy')as fecha_validacion
   FROM sgnom.tsgnomempleados nomempleados
     JOIN sgrh.tsgrhempleados rhempleados ON nomempleados.cod_empleado_fk = rhempleados.cod_empleado
     JOIN sgrh.tsgrhareas rharea ON rhempleados.cod_area = rharea.cod_area
     JOIN sgrh.tsgrhpuestos rhpuestos ON rhempleados.cod_puesto = rhpuestos.cod_puesto 
 WHERE nomempleados.des_validacion is not null AND  nomempleados.cod_validaciones='c' OR nomempleados.cod_validaciones='d' 
  GROUP BY nomempleados.cod_empleadoid, ((((rhempleados.des_nombre::text || ' '::text) || rhempleados.des_apepaterno::text) || ' '::text) || rhempleados.des_apematerno::text), rhempleados.cod_rfc, rharea.des_nbarea, rhpuestos.des_puesto
  ORDER BY nomempleados.cod_empleadoid;
END;
$$;


ALTER FUNCTION sgnom.bajasvalidadas() OWNER TO suite;

--
-- TOC entry 522 (class 1255 OID 101947)
-- Name: buscar_detalle_emp(integer); Type: FUNCTION; Schema: sgnom; Owner: suite
--

CREATE FUNCTION sgnom.buscar_detalle_emp(cabecera integer) RETURNS TABLE(codempleado integer, despuesto character varying, nomcompleto text, desnbarea character varying, codempqui integer, bol_pagorh boolean, bol_pagofinanzas boolean, bol_pagoempleado boolean, monto numeric)
    LANGUAGE plpgsql
    AS $$
BEGIN 
RETURN QUERY 

SELECT rh_emp.cod_empleado, puesto.des_puesto, 
    CONCAT(rh_emp.des_nombre, ' ', rh_emp.des_nombres, ' ', rh_emp.des_apepaterno, ' ', rh_emp.des_apematerno) AS nom_completo,
    area.des_nbarea,
    empQui.cod_empquincenaid cod_empqui,
    confPago.bol_pagorh rh,
    confPago.bol_pagofinanzas,
    confPago.bol_pagoempleado,
	(SELECT imp_totalemp FROM sgnom.tsgnomempquincena AS pago 
	 WHERE pago.cod_empquincenaid = empQui.cod_empquincenaid) AS monto
	
	FROM sgrh.tsgrhareas AS area, 
		 sgrh.tsgrhempleados AS rh_emp, 
		 sgrh.tsgrhpuestos AS puesto, 
		 sgnom.tsgnomempleados AS nom_emp,
	     sgnom.tsgnomempquincena AS empQui, 
		 sgnom.tsgnomconfpago AS confPago

	WHERE area.cod_area = puesto.cod_area
		--AND rh_emp.cod_area = area.cod_area
		AND rh_emp.cod_puesto = puesto.cod_puesto
		AND rh_emp.cod_empleadoactivo = 't'
		AND rh_emp.cod_empleado = nom_emp.cod_empleadoid
		AND rh_emp.cod_empleado = empQui.cod_empleadoid_fk
		AND empQui.cod_empquincenaid = confPago.cod_empquincenaid_fk
		AND empQui.cod_cabeceraid_fk = cabecera
		AND nom_emp.bol_estatus = 't'
	ORDER BY 3, 1, 2;

END;
$$;


ALTER FUNCTION sgnom.buscar_detalle_emp(cabecera integer) OWNER TO suite;

--
-- TOC entry 493 (class 1255 OID 101948)
-- Name: buscar_detalle_empleados(); Type: FUNCTION; Schema: sgnom; Owner: suite
--

CREATE FUNCTION sgnom.buscar_detalle_empleados() RETURNS TABLE(codempleado integer, despuesto character varying, nomcompleto text, desnbarea character varying)
    LANGUAGE plpgsql
    AS $$
BEGIN 
RETURN QUERY 

SELECT rh_emp.cod_empleado, puesto.des_puesto, 
	CONCAT(rh_emp.des_nombre, ' ', rh_emp.des_nombres, ' ', rh_emp.des_apepaterno, ' ', rh_emp.des_apematerno) AS nom_completo,
	area.des_nbarea
FROM sgrh.tsgrhareas AS area, sgrh.tsgrhempleados AS rh_emp, sgrh.tsgrhpuestos AS puesto, sgnom.tsgnomempleados AS nom_emp
WHERE area.cod_area = puesto.cod_area
AND rh_emp.cod_area = area.cod_area
AND rh_emp.cod_puesto = puesto.cod_puesto
AND rh_emp.cod_empleadoactivo = 't'
AND rh_emp.cod_empleado = nom_emp.cod_empleadoid
AND nom_emp.bol_estatus = 't'
ORDER BY 3, 1, 2;

END;
$$;


ALTER FUNCTION sgnom.buscar_detalle_empleados() OWNER TO suite;

--
-- TOC entry 494 (class 1255 OID 101949)
-- Name: buscar_detalle_empleados(integer); Type: FUNCTION; Schema: sgnom; Owner: suite
--

CREATE FUNCTION sgnom.buscar_detalle_empleados(idempleado integer) RETURNS TABLE(codempleado integer, despuesto character varying, nomcompleto text, desnbarea character varying)
    LANGUAGE plpgsql
    AS $$
BEGIN 
RETURN QUERY 

SELECT rh_emp.cod_empleado, puesto.des_puesto, 
	CONCAT(rh_emp.des_nombre, ' ', rh_emp.des_nombres, ' ', rh_emp.des_apepaterno, ' ', rh_emp.des_apematerno) AS nom_completo,
	area.des_nbarea
FROM sgrh.tsgrhareas AS area, sgrh.tsgrhempleados AS rh_emp, sgrh.tsgrhpuestos AS puesto, sgnom.tsgnomempleados AS nom_emp
WHERE area.cod_area = puesto.cod_area
AND rh_emp.cod_area = area.cod_area
AND rh_emp.cod_puesto = puesto.cod_puesto
AND rh_emp.cod_empleadoactivo = 't'
AND rh_emp.cod_empleado = idempleado
AND nom_emp.bol_estatus = 't'
ORDER BY 3, 1, 2;

END;
$$;


ALTER FUNCTION sgnom.buscar_detalle_empleados(idempleado integer) OWNER TO suite;

--
-- TOC entry 492 (class 1255 OID 101950)
-- Name: buscar_detalle_todos_empleados(); Type: FUNCTION; Schema: sgnom; Owner: suite
--

CREATE FUNCTION sgnom.buscar_detalle_todos_empleados() RETURNS TABLE(codempleado integer, despuesto character varying, nomcompleto text, desnbarea character varying)
    LANGUAGE plpgsql
    AS $$
BEGIN 
RETURN QUERY 

SELECT rh_emp.cod_empleado, puesto.des_puesto, 
	CONCAT(rh_emp.des_nombre, ' ', rh_emp.des_nombres, ' ', rh_emp.des_apepaterno, ' ', rh_emp.des_apematerno) AS nom_completo,
	area.des_nbarea
FROM sgrh.tsgrhareas AS area, sgrh.tsgrhempleados AS rh_emp, sgrh.tsgrhpuestos AS puesto, sgnom.tsgnomempleados AS nom_emp
WHERE area.cod_area = puesto.cod_area
AND rh_emp.cod_area = area.cod_area
AND rh_emp.cod_puesto = puesto.cod_puesto
AND rh_emp.cod_empleadoactivo = 't'
AND rh_emp.cod_empleado = nom_emp.cod_empleadoid
AND nom_emp.bol_estatus = 't'
ORDER BY 3, 1, 2;

END;
$$;


ALTER FUNCTION sgnom.buscar_detalle_todos_empleados() OWNER TO suite;

--
-- TOC entry 523 (class 1255 OID 101951)
-- Name: buscar_incidencias_por_empleado(integer); Type: FUNCTION; Schema: sgnom; Owner: suite
--

CREATE FUNCTION sgnom.buscar_incidencias_por_empleado(idempleado integer) RETURNS TABLE(idincidencia integer, fechaalta date, clave character varying, incidencia character varying, idtipo character, desctipo text, cantidad smallint, actividad character varying, comentarios text, reportaid integer, reportanb text, autorizaid integer, autorizanb text, perfil character varying, detallefechas text, montoincidencia numeric, montopagado numeric, aceptacion boolean, validacion boolean, quincenaid integer, desquincena character varying, creaid integer, creanb text)
    LANGUAGE plpgsql
    AS $$

BEGIN 
RETURN QUERY 

	SELECT incidencias.cod_incidenciaid idincidencia,
		incidencias.aud_feccreacion fechaalta,
		catincidencias.cod_claveincidencia clave,
		catincidencias.cod_nbincidencia incidencia,
		catincidencias.cod_tipoincidencia idtipo,
		CASE 
			WHEN catincidencias.cod_tipoincidencia='1' THEN
			 'HORAS'
			WHEN catincidencias.cod_tipoincidencia='2' THEN
			 'DIAS' 
		  WHEN catincidencias.cod_tipoincidencia='3' THEN
			 'ACTIVIDAD' 
			ELSE
			 'NO DATA'
		END desc_tipo,
		incidencias.cnu_cantidad cantidad,
		incidencias.des_actividad actividad,
		incidencias.txt_comentarios comentarios,
		incidencias.cod_empreporta_fk reportaid,
		(SELECT CONCAT(rhempleados.des_nombre ||' ', rhempleados.des_nombres ,' ' || rhempleados.des_apepaterno || ' ' || rhempleados.des_apematerno) 
		FROM sgrh.tsgrhempleados rhempleados
		WHERE rhempleados.cod_empleado = (SELECT nom.cod_empleado_fk FROM sgnom.tsgnomempleados nom WHERE nom.cod_empleadoid = idempleado)
		) reportanb,
		incidencias.cod_empautoriza_fk autorizaid,
		(SELECT CONCAT(rhempleados.des_nombre ||' ', rhempleados.des_nombres ,' ' || rhempleados.des_apepaterno || ' ' || rhempleados.des_apematerno) 
		FROM sgrh.tsgrhempleados rhempleados
		WHERE rhempleados.cod_empleado = incidencias.cod_empautoriza_fk
		) autorizanb,
		catincidencias.cod_perfilincidencia perfil,
		CAST(incidencias.xml_detcantidad AS TEXT) detallefechas,
		catincidencias.imp_monto montoincidencia,
		incidencias.imp_monto montopagado,
		incidencias.bol_validacion validacion, --validacion rh
		incidencias.bol_aceptacion aceptacion, --aceptacion lider de celula
		incidencias.cod_quincenaid_fk quincena,
		(SELECT qui.des_quincena FROM sgnom.tsgnomquincena qui WHERE qui.cod_quincenaid = incidencias.cod_quincenaid_fk) desquincena,
		incidencias.aud_codcreadopor creaid,
		(SELECT CONCAT(rhempleados.des_nombre ||' ', rhempleados.des_nombres ,' ' || rhempleados.des_apepaterno || ' ' || rhempleados.des_apematerno) 
		FROM sgrh.tsgrhempleados rhempleados
		WHERE rhempleados.cod_empleado = incidencias.aud_codcreadopor
		) creanb
FROM sgnom.tsgnomincidencia incidencias
JOIN sgnom.tsgnomcatincidencia catincidencias 
ON incidencias.cod_catincidenciaid_fk = catincidencias.cod_catincidenciaid
WHERE incidencias.cod_empreporta_fk = idempleado 
	AND incidencias.cod_quincenaid_fk = (SELECT nomquincena.cod_quincenaid 
											FROM sgnom.tsgnomquincena nomquincena
											WHERE nomquincena.fec_inicio <= CURRENT_DATE AND nomquincena.fec_fin >= CURRENT_DATE)
	AND incidencias.bol_estatus = true 
ORDER BY 1 DESC, 2;

END;
$$;


ALTER FUNCTION sgnom.buscar_incidencias_por_empleado(idempleado integer) OWNER TO suite;

--
-- TOC entry 505 (class 1255 OID 101952)
-- Name: detalle_desglose(integer, integer); Type: FUNCTION; Schema: sgnom; Owner: suite
--

CREATE FUNCTION sgnom.detalle_desglose(cod_empleado integer, cod_cabecera integer) RETURNS TABLE(imp_concepto numeric, imp_exento numeric, imp_gravado numeric, nombre character varying, clave character varying, cod_conceptoid_fk integer)
    LANGUAGE plpgsql
    AS $$
BEGIN 
RETURN QUERY 

SELECT CQ.imp_concepto, CQ.imp_exento, CQ.imp_gravado, 
							(SELECT cod_nbconcepto
                            FROM sgnom.tsgnomconcepto CO
                            WHERE cod_conceptoid = CQ.cod_conceptoid_fk) nombre, 
							(SELECT cod_claveconcepto
                            FROM sgnom.tsgnomconcepto CO
                            WHERE cod_conceptoid = CQ.cod_conceptoid_fk) clave, 
							CQ.cod_conceptoid_fk
FROM sgnom.tsgnomcncptoquinc CQ
WHERE CQ.cod_empquincenaid_fk = (
                            SELECT cod_empquincenaid 
                            FROM sgnom.tsgnomempquincena EQ
                            WHERE EQ.cod_cabeceraid_fk  = cod_cabecera --cabecera a seleccionar
                            AND EQ.cod_empleadoid_fk = (SELECT cod_empleadoid
                                                       FROM sgnom.tsgnomempleados
                                                       WHERE cod_empleado_fk = cod_empleado )); --empleado elegido
 --tipo concepto 1=deduccion, 2=percepcion

END;
$$;


ALTER FUNCTION sgnom.detalle_desglose(cod_empleado integer, cod_cabecera integer) OWNER TO suite;

--
-- TOC entry 525 (class 1255 OID 101953)
-- Name: detalle_desglose_deduccion(integer, integer); Type: FUNCTION; Schema: sgnom; Owner: suite
--

CREATE FUNCTION sgnom.detalle_desglose_deduccion(cod_empleado integer, cod_cabecera integer) RETURNS TABLE(imp_concepto numeric, imp_exento numeric, imp_gravado numeric, nombre character varying, clave character varying, cod_conceptoid_fk integer)
    LANGUAGE plpgsql
    AS $$
BEGIN 
RETURN QUERY 

SELECT CQ.imp_concepto, CQ.imp_exento, CQ.imp_gravado, 
							(SELECT cod_nbconcepto
                            FROM sgnom.tsgnomconcepto CO
                            WHERE cod_conceptoid = CQ.cod_conceptoid_fk) nombre, 
							(SELECT cod_claveconcepto
                            FROM sgnom.tsgnomconcepto CO
                            WHERE cod_conceptoid = CQ.cod_conceptoid_fk) clave, 
							CQ.cod_conceptoid_fk
FROM sgnom.tsgnomcncptoquinc CQ
WHERE CQ.cod_empquincenaid_fk = (
                            SELECT cod_empquincenaid 
                            FROM sgnom.tsgnomempquincena EQ
                            WHERE EQ.cod_cabeceraid_fk  = cod_cabecera --cabecera a seleccionar
                            AND EQ.cod_empleadoid_fk = (SELECT cod_empleadoid
                                                       FROM sgnom.tsgnomempleados
                                                       WHERE cod_empleado_fk = cod_empleado )) --empleado elegido
 AND CQ.cod_conceptoid_fk IN (SELECT cod_conceptoid
                            FROM sgnom.tsgnomconcepto CO
                            WHERE CO.cod_tipoconceptoid_fk = 1);--tipo concepto 1=deduccion, 2=percepcion

END;
$$;


ALTER FUNCTION sgnom.detalle_desglose_deduccion(cod_empleado integer, cod_cabecera integer) OWNER TO suite;

--
-- TOC entry 526 (class 1255 OID 101954)
-- Name: detalle_desglose_percepcion(integer, integer); Type: FUNCTION; Schema: sgnom; Owner: suite
--

CREATE FUNCTION sgnom.detalle_desglose_percepcion(cod_empleado integer, cod_cabecera integer) RETURNS TABLE(imp_concepto numeric, imp_exento numeric, imp_gravado numeric, nombre character varying, clave character varying, cod_conceptoid_fk integer)
    LANGUAGE plpgsql
    AS $$
BEGIN 
RETURN QUERY 

SELECT CQ.imp_concepto, CQ.imp_exento, CQ.imp_gravado, 
							(SELECT cod_nbconcepto
                            FROM sgnom.tsgnomconcepto CO
                            WHERE cod_conceptoid = CQ.cod_conceptoid_fk) nombre, 
							(SELECT cod_claveconcepto
                            FROM sgnom.tsgnomconcepto CO
                            WHERE cod_conceptoid = CQ.cod_conceptoid_fk) clave, 
							CQ.cod_conceptoid_fk
FROM sgnom.tsgnomcncptoquinc CQ
WHERE CQ.cod_empquincenaid_fk = (
                            SELECT cod_empquincenaid 
                            FROM sgnom.tsgnomempquincena EQ
                            WHERE EQ.cod_cabeceraid_fk  = cod_cabecera --cabecera a seleccionar
                            AND EQ.cod_empleadoid_fk = (SELECT cod_empleadoid
                                                       FROM sgnom.tsgnomempleados
                                                       WHERE cod_empleado_fk = cod_empleado )) --empleado elegido
 AND CQ.cod_conceptoid_fk IN (SELECT cod_conceptoid
                            FROM sgnom.tsgnomconcepto CO
                            WHERE CO.cod_tipoconceptoid_fk = 2);--tipo concepto 1=deduccion, 2=percepcion

END;
$$;


ALTER FUNCTION sgnom.detalle_desglose_percepcion(cod_empleado integer, cod_cabecera integer) OWNER TO suite;

--
-- TOC entry 507 (class 1255 OID 101955)
-- Name: detalle_desglose_persepcion(integer, integer, integer); Type: FUNCTION; Schema: sgnom; Owner: suite
--

CREATE FUNCTION sgnom.detalle_desglose_persepcion(cod_empleado integer, cod_cabecera integer, tipo_concepto integer) RETURNS TABLE(imp_concepto numeric, imp_exento numeric, imp_gravado numeric, nombre character varying, clave character varying, cod_conceptoid_fk integer)
    LANGUAGE plpgsql
    AS $$
BEGIN 
RETURN QUERY 

SELECT CQ.imp_concepto, CQ.imp_exento, CQ.imp_gravado, 
							(SELECT cod_nbconcepto
                            FROM sgnom.tsgnomconcepto CO
                            WHERE cod_conceptoid = CQ.cod_conceptoid_fk) nombre, 
							(SELECT cod_claveconcepto
                            FROM sgnom.tsgnomconcepto CO
                            WHERE cod_conceptoid = CQ.cod_conceptoid_fk) clave, 
							CQ.cod_conceptoid_fk
FROM sgnom.tsgnomcncptoquinc CQ
WHERE CQ.cod_empquincenaid_fk = (
                            SELECT cod_empquincenaid 
                            FROM sgnom.tsgnomempquincena EQ
                            WHERE EQ.cod_cabeceraid_fk  = cod_cabecera --cabecera a seleccionar
                            AND EQ.cod_empleadoid_fk = (SELECT cod_empleadoid
                                                       FROM sgnom.tsgnomempleados
                                                       WHERE cod_empleado_fk = cod_empleado )) --empleado elegido
 AND CQ.cod_conceptoid_fk IN (SELECT cod_conceptoid
                            FROM sgnom.tsgnomconcepto CO
                            WHERE CO.cod_tipoconceptoid_fk = tipo_concepto);--tipo concepto 1=deduccion, 2=percepcion

END;
$$;


ALTER FUNCTION sgnom.detalle_desglose_persepcion(cod_empleado integer, cod_cabecera integer, tipo_concepto integer) OWNER TO suite;

--
-- TOC entry 527 (class 1255 OID 101956)
-- Name: detallespersonal(); Type: FUNCTION; Schema: sgnom; Owner: suite
--

CREATE FUNCTION sgnom.detallespersonal() RETURNS TABLE(codempleado integer, nomcompleto text, desrfc character varying, descurp character varying, desnbarea character varying, despuesto character varying)
    LANGUAGE plpgsql COST 10
    AS $$
BEGIN
RETURN QUERY
SELECT nomempleados.cod_empleadoid,
   (((rhempleados.des_nombre::text || ' '::text) || rhempleados.des_apepaterno::text) || ' '::text) || rhempleados.des_apematerno::text AS nom_empleado,
   rhempleados.cod_rfc,
    rhempleados.cod_curp,
   rharea.des_nbarea,
   rhpuestos.des_puesto
  FROM sgnom.tsgnomempleados nomempleados
    JOIN sgrh.tsgrhempleados rhempleados ON nomempleados.cod_empleado_fk = rhempleados.cod_empleado
    JOIN sgrh.tsgrhareas rharea ON rhempleados.cod_area = rharea.cod_area
    JOIN sgrh.tsgrhpuestos rhpuestos ON rhempleados.cod_puesto = rhpuestos.cod_puesto
 WHERE nomempleados.des_validacion=true
 and nomempleados.cod_validaciones='a' OR nomempleados.cod_validaciones='d'
 GROUP BY nomempleados.cod_empleadoid, ((((rhempleados.des_nombre::text || ' '::text) || rhempleados.des_apepaterno::text) || ' '::text) || rhempleados.des_apematerno::text), rhempleados.cod_rfc, rhempleados.cod_curp,rharea.des_nbarea, rhpuestos.des_puesto
 ORDER BY nomempleados.cod_empleadoid;
END;
$$;


ALTER FUNCTION sgnom.detallespersonal() OWNER TO suite;

--
-- TOC entry 508 (class 1255 OID 101957)
-- Name: eliminar_incidencia_por_empleado(integer, integer); Type: FUNCTION; Schema: sgnom; Owner: suite
--

CREATE FUNCTION sgnom.eliminar_incidencia_por_empleado(incidenciaid integer, reporta integer) RETURNS boolean
    LANGUAGE sql
    AS $$
	--No es eliminacion fisica, actualiza el estatus a 'false'
	UPDATE sgnom.tsgnomincidencia 
		SET bol_estatus = 'f',
		aud_codmodificadopor = reporta,
		aud_fecmodificacion = CURRENT_DATE
	WHERE "cod_incidenciaid" = incidenciaid	RETURNING TRUE;

$$;


ALTER FUNCTION sgnom.eliminar_incidencia_por_empleado(incidenciaid integer, reporta integer) OWNER TO suite;

--
-- TOC entry 528 (class 1255 OID 101958)
-- Name: empleado_confpago(integer, integer); Type: FUNCTION; Schema: sgnom; Owner: suite
--

CREATE FUNCTION sgnom.empleado_confpago(cabecera integer, empleado integer) RETURNS TABLE(codempleado integer, despuesto character varying, nomcompleto text, desnbarea character varying, codempqui integer, bol_pagorh boolean, bol_pagofinanzas boolean, bol_pagoempleado boolean, monto numeric)
    LANGUAGE plpgsql
    AS $$
BEGIN 
RETURN QUERY 

SELECT rh_emp.cod_empleado, puesto.des_puesto, 
    CONCAT(rh_emp.des_nombre, ' ', rh_emp.des_nombres, ' ', rh_emp.des_apepaterno, ' ', rh_emp.des_apematerno) AS nom_completo,
    area.des_nbarea,
    empQui.cod_empquincenaid cod_empqui,
    confPago.bol_pagorh rh,
    confPago.bol_pagofinanzas,
    confPago.bol_pagoempleado,
	(SELECT imp_totalemp FROM sgnom.tsgnomempquincena AS pago WHERE pago.cod_empquincenaid = empQui.cod_empquincenaid) AS monto
	
FROM sgrh.tsgrhareas AS area, 
	 sgrh.tsgrhempleados AS rh_emp, 
	 sgrh.tsgrhpuestos AS puesto, 
	 sgnom.tsgnomempleados AS nom_emp,
     sgnom.tsgnomempquincena AS empQui, 
	 sgnom.tsgnomconfpago AS confPago

WHERE area.cod_area = puesto.cod_area
--AND rh_emp.cod_area = area.cod_area
AND rh_emp.cod_puesto = puesto.cod_puesto
AND rh_emp.cod_empleadoactivo = 't'
AND rh_emp.cod_empleado = nom_emp.cod_empleadoid
AND rh_emp.cod_empleado = empQui.cod_empleadoid_fk
AND empQui.cod_empquincenaid = confPago.cod_empquincenaid_fk
AND empQui.cod_cabeceraid_fk = cabecera
AND empQui.cod_empleadoid_fk = empleado
AND nom_emp.bol_estatus = 't'
ORDER BY 3, 1, 2;

END;
$$;


ALTER FUNCTION sgnom.empleado_confpago(cabecera integer, empleado integer) OWNER TO suite;

--
-- TOC entry 579 (class 1255 OID 102537)
-- Name: empleados_por_area(integer); Type: FUNCTION; Schema: sgnom; Owner: postgres
--

CREATE FUNCTION sgnom.empleados_por_area(area integer) RETURNS TABLE(empleadoid integer)
    LANGUAGE plpgsql
    AS $$
BEGIN 
RETURN QUERY 
		SELECT nom.cod_empleadoid FROM sgnom.tsgnomempleados nom WHERE nom.cod_empleado_fk IN 
			(SELECT rhe.cod_empleado FROM sgrh.tsgrhempleados rhe WHERE rhe.cod_puesto IN 
				(SELECT pst.cod_puesto FROM sgrh.tsgrhpuestos pst WHERE pst.cod_area = area)
			);
END;
$$;


ALTER FUNCTION sgnom.empleados_por_area(area integer) OWNER TO postgres;

--
-- TOC entry 529 (class 1255 OID 101959)
-- Name: empleados_por_cabecera(integer); Type: FUNCTION; Schema: sgnom; Owner: suite
--

CREATE FUNCTION sgnom.empleados_por_cabecera(idcabecera integer) RETURNS TABLE(nom_empleado text, rol character varying, area character varying, cod_empleado integer, cod_empquincenaid integer)
    LANGUAGE plpgsql
    AS $$
BEGIN 
RETURN QUERY 
SELECT 
(((rhempleados.des_nombre::text || ' '::text) || rhempleados.des_apepaterno::text) || ' '::text) || rhempleados.des_apematerno::text AS nom_empleado,
rhpuestos.des_puesto,
rharea.des_nbarea,
emp.cod_empleadoid,
empquin.cod_empquincenaid
FROM sgrh.tsgrhempleados rhempleados
JOIN sgnom.tsgnomempleados emp ON emp.cod_empleadoid = rhempleados.cod_empleado
JOIN sgnom.tsgnomempquincena empquin ON emp.cod_empleadoid =  empquin.cod_empleadoid_fk
JOIN sgrh.tsgrhpuestos rhpuestos ON rhempleados.cod_puesto = rhpuestos.cod_puesto
JOIN sgrh.tsgrhareas rharea ON rhempleados.cod_area = rharea.cod_area
WHERE empquin.cod_cabeceraid_fk = idcabecera;	
END;
$$;


ALTER FUNCTION sgnom.empleados_por_cabecera(idcabecera integer) OWNER TO suite;

--
-- TOC entry 533 (class 1255 OID 102368)
-- Name: empleadosnom_por_area(integer); Type: FUNCTION; Schema: sgnom; Owner: postgres
--

CREATE FUNCTION sgnom.empleadosnom_por_area(area integer) RETURNS TABLE(empleadoid integer)
    LANGUAGE plpgsql
    AS $$
BEGIN 
RETURN QUERY 
		SELECT nom.cod_empleadoid FROM sgnom.tsgnomempleados nom WHERE nom.cod_empleado_fk IN 
			(SELECT rhe.cod_empleado FROM sgrh.tsgrhempleados rhe WHERE rhe.cod_puesto IN 
				(SELECT pst.cod_puesto FROM sgrh.tsgrhpuestos pst WHERE pst.cod_area = area)
			);
END;
$$;


ALTER FUNCTION sgnom.empleadosnom_por_area(area integer) OWNER TO postgres;

--
-- TOC entry 581 (class 1255 OID 102638)
-- Name: fn_bono_adicional_incidencia(integer); Type: FUNCTION; Schema: sgnom; Owner: postgres
--

CREATE FUNCTION sgnom.fn_bono_adicional_incidencia(idempleadoquincena integer) RETURNS numeric
    LANGUAGE plpgsql
    AS $$
    DECLARE
    rec RECORD;
    vXML VARCHAR(4000);
	xmlDesglose VARCHAR(3000);
    BEGIN
        
        FOR rec IN SELECT (SELECT cinc.cod_claveincidencia 
                            FROM sgnom.tsgnomcatincidencia cinc
                        WHERE cinc.cod_catincidenciaid = inc.cod_catincidenciaid_fk) clave,
                            (SELECT cinc.cod_nbincidencia 
                            FROM sgnom.tsgnomcatincidencia cinc
                        WHERE cinc.cod_catincidenciaid = inc.cod_catincidenciaid_fk) nombre, 
                        inc.cnu_cantidad cantidad,
                        inc.imp_monto monto_total
                    FROM sgnom.tsgnomincidencia inc 
                    WHERE inc.cod_empreporta_fk = (SELECT empq.cod_empleadoid_fk 
                                                FROM sgnom.tsgnomempquincena empq
                                                    WHERE empq.cod_empquincenaid = idEmpleadoQuincena)
                    AND (inc.bol_validacion, inc.bol_pago, inc.bol_aceptacion) = (true, true, true)
        LOOP     
            vXML :='<?xml version="1.0" encoding="ISO8859_1"?>                
                <mx.org.ift.serp.conceptoBindingXml.DetallesConcepto>                  
                <claveConcepto> ' || rec.clave || ' </claveConcepto>                  
                <nombreConcepto> ' || rec.nombre || ' </nombreConcepto>';
                vXML :=vXML ||'<formula> </formula>';
                --Se obtiene el numero de argumentos que contiene la formula               
                --Se continua la construccion del XML agregando cada argumento
                RAISE INFO 'RES 1 %', vXML;
                vXML:= vXML || '<argumentos><mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>' || rec.clave || '</nombre>                            
                <valor>' || rec.monto_total || '</valor> <descripcion>' || rec.nombre || '</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento>';
                RAISE INFO 'RES 2 %', vXML;
                vXML :=vXML || '</argumentos>';
                RAISE INFO 'RES 3 %', vXML;
                IF(xmlDesglose IS NOT NULL) THEN 
                    vXML := vXML || xmlDesglose;
                    xmlDesglose :=NULL;
                END IF;
				RAISE INFO 'RES 4 %', vXML;
                
                
				--Se guarda el importe del concepto y el XML generado
                
                
                vXML:=vXML || '<importe> ' || rec.monto_total || '</importe>   </mx.org.ift.serp.conceptoBindingXml.DetallesConcepto>';
		RAISE INFO 'RES 5 %', vXML;
		INSERT INTO sgnom.tsgnomcncptoquinc(
                cod_empquincenaid_fk, cod_conceptoid_fk, 
                imp_concepto, imp_gravado, imp_exento, 
                xml_desgloce)
                VALUES (idEmpleadoQuincena, 9, 
                rec.monto_total, 0.0, 0.0, 
                vXML::xml);
                vXML = '';
        END LOOP;
		
		return idEmpleadoQuincena;
     END;
$$;


ALTER FUNCTION sgnom.fn_bono_adicional_incidencia(idempleadoquincena integer) OWNER TO postgres;

--
-- TOC entry 509 (class 1255 OID 101960)
-- Name: fn_bono_asistencia_imss(integer); Type: FUNCTION; Schema: sgnom; Owner: postgres
--

CREATE FUNCTION sgnom.fn_bono_asistencia_imss(idempleado integer) RETURNS numeric
    LANGUAGE plpgsql
    AS $$
    DECLARE
		valor DECIMAL;
		--rec RECORD;
    BEGIN
		SELECT imss.premio_asistencia
        FROM sgnom.tsgnomnominaimss imss
        WHERE (imss.curp, imss.nss, imss.rfc) =        
            (SELECT cod_curp, cod_nss, cod_rfc 
            FROM sgrh.tsgrhempleados 
            WHERE cod_empleado  = idEmpleado) INTO valor;
		RETURN valor;
    END;
$$;


ALTER FUNCTION sgnom.fn_bono_asistencia_imss(idempleado integer) OWNER TO postgres;

--
-- TOC entry 510 (class 1255 OID 101961)
-- Name: fn_bono_puntualidad_imss(integer); Type: FUNCTION; Schema: sgnom; Owner: postgres
--

CREATE FUNCTION sgnom.fn_bono_puntualidad_imss(idempleado integer) RETURNS numeric
    LANGUAGE plpgsql
    AS $$
    DECLARE
		valor DECIMAL;
		--rec RECORD;
    BEGIN
		SELECT imss.premio_puntualidad
        FROM sgnom.tsgnomnominaimss imss
        WHERE (imss.curp, imss.nss, imss.rfc) =        
            (SELECT cod_curp, cod_nss, cod_rfc 
            FROM sgrh.tsgrhempleados 
            WHERE cod_empleado  = idEmpleado) INTO valor;
		RETURN valor;
    END;
$$;


ALTER FUNCTION sgnom.fn_bono_puntualidad_imss(idempleado integer) OWNER TO postgres;

--
-- TOC entry 536 (class 1255 OID 101962)
-- Name: fn_calcula_importes_nomina(integer, character varying, numeric); Type: FUNCTION; Schema: sgnom; Owner: suite
--

CREATE FUNCTION sgnom.fn_calcula_importes_nomina(idnomina integer, vempleados character varying, todos numeric) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
    DECLARE
        vTotalPercepcion          NUMERIC(10,2):=0;
        vTotalDeduccion           NUMERIC(10,2):=0;
        vImporteTotal             NUMERIC(10,2):=0;
        vNumRegistros             INTEGER:=0;
        vNumRegistros2            INTEGER:=0;
        vBandera                  INTEGER;
        vIdEmpleadoNomina         INTEGER;
        vTotalPercepcionC         NUMERIC(10,2):=0;
        vTotalDeduccionC          NUMERIC(10,2):=0;
        vImporteTotalC            NUMERIC(10,2):=0;
        vNumeroEmpleados          INTEGER;
        vNomEmpleado              NUMERIC(10,2);
        vTotalEmpleadosNomina     INTEGER;
        vTotalEmpleadosCalculados INTEGER;
        ERR_CODE       INTEGER;
        ERR_MSG        VARCHAR(250);
        a_count INTEGER;
        b_count INTEGER;
        --CURSORES

        C5 CURSOR FOR
            SELECT EQ.cod_empquincenaid
            FROM sgnom.tsgnomempquincena EQ
            WHERE EQ.cod_cabeceraid_fk = idNomina
            ORDER BY EQ.cod_empquincenaid;

        C6 CURSOR FOR
            SELECT EQ.cod_empquincenaid
            FROM sgnom.tsgnomempquincena EQ
            WHERE EQ.cod_cabeceraid_fk = idNomina
            AND EQ.bol_estatusemp = 'f'
            ORDER BY EQ.cod_empquincenaid;

    BEGIN
        OPEN C5;
        RAISE INFO 'abre cursor C1';
        LOOP 
            FETCH C5 INTO vIdEmpleadoNomina;
            EXIT WHEN not found;
            RAISE INFO 'empleado %, idNomina %', vIdEmpleadoNomina,idNomina;
            --Se realiza la suma de los conceptos que son percepciones
            SELECT SUM(CQ.imp_concepto) INTO vTotalPercepcion
            FROM sgnom.tsgnomcncptoquinc CQ, sgnom.tsgnomconcepto CO, sgnom.tsgnomempquincena EQ
            WHERE CO.cod_tipoconceptoid_fk = 2
            AND EQ.cod_cabeceraid_fk = idNomina
            AND EQ.cod_empquincenaid = vIdEmpleadoNomina
            AND EQ.cod_empquincenaid = CQ.cod_empquincenaid_fk
            AND CO.cod_conceptoid = CQ.cod_conceptoid_fk;
            RAISE INFO 'vTotalPercepcion %', vTotalPercepcion;
            IF(vTotalPercepcion        IS NOT NULL) THEN
                vTotalPercepcionC        :=vTotalPercepcionC+vTotalPercepcion;
            ELSIF(vTotalPercepcion     IS NULL) THEN
                vTotalPercepcion         :=0;
                vTotalPercepcionC        :=vTotalPercepcionC+vTotalPercepcion;
            END IF;
            RAISE INFO 'vTotalPercepcionC %', vTotalPercepcionC;
            --Se realiza la suma de los conceptos que son deducciones
            SELECT SUM(CQ.imp_concepto) INTO vTotalDeduccion
            FROM sgnom.tsgnomcncptoquinc CQ, sgnom.tsgnomconcepto CO, sgnom.tsgnomempquincena EQ
            WHERE CO.cod_tipoconceptoid_fk = 2
            AND EQ.cod_cabeceraid_fk = idNomina
            AND EQ.cod_empleadoid_fk = vIdEmpleadoNomina
            AND EQ.cod_empquincenaid = CQ.cod_empquincenaid_fk
            AND CO.cod_conceptoid = CQ.cod_conceptoid_fk;
            RAISE INFO 'vTotalDeduccion %', vTotalDeduccion;
            IF(vTotalDeduccion         IS NOT NULL) THEN
                vTotalDeduccionC         :=vTotalDeduccionC+vTotalDeduccion;
            ELSIF (vTotalDeduccion     IS NULL) THEN
                vTotalDeduccion          :=0;
                vTotalDeduccionC         :=vTotalDeduccionC+vTotalDeduccion;
            END IF;
            RAISE INFO 'vTotalDeduccionC %', vTotalDeduccionC;
            IF(vTotalPercepcion IS NOT NULL AND vTotalDeduccion IS NOT NULL) THEN
                SELECT (vTotalPercepcion - vTotalDeduccion) INTO vImporteTotal ;
            ELSIF(vTotalPercepcion IS NULL AND vTotalDeduccion IS NOT NULL) THEN
                vImporteTotal        :=vTotalDeduccion;
            ELSIF(vTotalPercepcion IS NOT NULL AND vTotalDeduccion IS NULL) THEN
                vImporteTotal        :=vTotalPercepcion;
            END IF;
            
            IF(vImporteTotal IS NOT NULL) THEN
                vImporteTotalC :=vImporteTotalC+vImporteTotal;
            ELSE
                vImporteTotalC:=0;
            END IF;
            --Se actualizan los importes de cada empleado asi como el estatus
            UPDATE sgnom.tsgnomempquincena
            SET imp_totpercepcion     =vTotalPercepcion,
                imp_totdeduccion        =vTotalDeduccion,
                imp_totalemp          =vImporteTotal
                --bol_estatusemp ='B'
            WHERE cod_empquincenaid = vIdEmpleadoNomina
            AND cod_cabeceraid_fk = idNomina;
            GET DIAGNOSTICS a_count = ROW_COUNT;
			RAISE INFO 'a_count %', a_count;
            IF (a_count>0) THEN 
                vNumRegistros := vNumRegistros + 1 ;
				RAISE INFO 'vNumRegistros %', vNumRegistros;
            END IF;
            --COMMIT;
        END LOOP;
        CLOSE C5;
        UPDATE sgnom.tsgnomcabecera
        SET imp_totpercepcion = vTotalPercepcionC,
        imp_totdeduccion = vTotalDeduccionC,
        imp_totalemp = vImporteTotalC,
        cod_estatusnomid_fk = 2
        WHERE cod_cabeceraid = idNomina
        AND (cod_estatusnomid_fk=1 OR cod_estatusnomid_fk=2);
        GET DIAGNOSTICS b_count = ROW_COUNT;
        IF (b_count >0) THEN
            vNumRegistros2      := vNumRegistros2 + 1 ;
        END IF; 

        IF (vNumRegistros > 0 AND vNumRegistros2 > 0) THEN
            vBandera := 1;
        ELSIF (vNumRegistros = 0 OR vNumRegistros2 = 0) THEN
            vBandera := 0;
        ELSE
            vBandera:=2;
        END IF;
    RETURN vBandera;
		EXCEPTION
		WHEN NO_DATA_FOUND THEN
			RAISE EXCEPTION '% exp no data found', vIdEmpleado;
			RETURN NULL;
    END;
$$;


ALTER FUNCTION sgnom.fn_calcula_importes_nomina(idnomina integer, vempleados character varying, todos numeric) OWNER TO suite;

--
-- TOC entry 538 (class 1255 OID 101963)
-- Name: fn_calcula_nomina(integer); Type: FUNCTION; Schema: sgnom; Owner: suite
--

CREATE FUNCTION sgnom.fn_calcula_nomina(vidnomina integer) RETURNS integer
    LANGUAGE plpgsql
    AS $_$
    DECLARE
    vBonoAsistenciaIMSS NUMERIC(10,2):=0.0;
    --rtSueldoBase INTEGER;
    --vFormula CHARACTER VARYING; --valor de la formula
    --vArgumento CHARACTER VARYING; --valor del argumento
    --vNombreArgumento CHARACTER VARYING; --valor del argumento
    --vNumeroArgumentos INTEGER; --numero de argumentos
    --VARIABLES
    total INTEGER;
    vIdNomEmpPla01 INTEGER;
    vIdConEmpPla01 INTEGER;
    vIdConcepto01fk INTEGER;
    vNumRegistros INTEGER:=0;
    vBandera INTEGER;
    vFormula VARCHAR(150);
    vArgumento VARCHAR(100);
    vNumeroArgumentos INTEGER;
    vFuncionOracle VARCHAR(100);
    vDiasLaborados INTEGER;
    vCadenaEjecutar VARCHAR(500);
    vFormulaEjecutar VARCHAR(500);
    vTabulador INTEGER:=0;
    vIdEmpleado INTEGER;
    vIdPlaza INTEGER;
    vNumeroQuincena INTEGER;
    vSueldoBase NUMERIC(10,2):=0.0;
    vConstante NUMERIC(10,2);
    vFormula2 VARCHAR(150);
    vImporteActualizado NUMERIC(10,2);
    vCadenaImporte VARCHAR(250);
    vXML VARCHAR(4000);
    vNombreConcepto VARCHAR(150);
    vValorArgumento NUMERIC(10,2);
    vNombreArgumento VARCHAR(100);
    vCompensasionG INTEGER:=0;
    vNumeroEmpleados INTEGER;
    vNomEmpleado INTEGER;
    vBanderaImporte INTEGER:=0;
    vIdManTer INTEGER;
    vPorcentajeManTer INTEGER;
    vClaveConcepto VARCHAR(20);
    vClaveConcepto2 VARCHAR(20);
    vApoyoVehicular INTEGER;
    vAportacionSSIGF INTEGER;
    vPrimaVacacional INTEGER;
    vFormulaCICAS VARCHAR(150);
    vFormulaCICAC VARCHAR(150);
    vNumeroArgumentosCAS INTEGER;
    vSueldoDiario INTEGER:=0;
    vExisteAguinaldo INTEGER;
    vIdAguinaldo INTEGER;
    vIdQuincena INTEGER;
    vFaltasSueldoEst INTEGER:=0;
    vFaltasSueldoEv INTEGER:=0;
    vFaltasSueldoEstAA INTEGER:=0;
    vFaltasCompEst INTEGER:=0;
    vFaltasCompEv INTEGER:=0;
    vFaltasCompEstAA INTEGER:=0;
    vISRSueldo INTEGER;
    vISRCompGaran INTEGER;
    vISRSSI INTEGER;
    vISRApoyoVehic INTEGER;
    vISRPrimaVac INTEGER;
    vValorQuinquenio INTEGER:=0;
    vSueldoCompactado INTEGER:=0;
    vSueldoBimestral INTEGER:=0;
    vQuinquenioBimestral INTEGER:=0;
    vSumaSRCEAYV INTEGER;
    vSueldoBimestralEV INTEGER;
    vQuinquenioBimestralEV INTEGER;
    vSumaSRCEAYVEV INTEGER;
    vSueldoCompactadoEV INTEGER;
    vPensionAlimenticia INTEGER;
    vDespensa INTEGER:=0;
    xmlDesglose VARCHAR(3000);
    --cursorPrimaVacacional SYS_REFCURSOR;
    --xmlApoyoVehicular VARCHAR(3000);
    --cursorApoyoVehicular SYS_REFCURSOR;
    -- xmlISRSSI VARCHAR(3000);
    --cursorISRSSI SYS_REFCURSOR;
    vSumaSARP INTEGER;
    vSumaFOVP INTEGER;
    vSumaSRCEAYVPAT INTEGER;
    vPorcentajePension INTEGER;
    vISREstDesemp INTEGER;
    vISRHonorarios INTEGER;
    vValorEstimulo INTEGER;
    vNumeroFaltas INTEGER;
    vDiasLicencia INTEGER;
    ERR_CODE INTEGER;
    ERR_MSG VARCHAR(250);
    vAhorroSolidario INTEGER;
    DecimaImpar INTEGER;
    vNumeroQuincenaBIM INTEGER;
    vTipoNominaBIM INTEGER;
    vEjercicioBIM INTEGER;
    vBanderaSegBaja INTEGER;
    vValISREstDesempA INTEGER;
    vExisteAnt INTEGER;
    vSueldoAnt INTEGER;
    vCompAnt INTEGER;
    vDiasLabAnterior INTEGER;
    vFechaNomina INTEGER;
    vImpLicMedSueldo INTEGER;
    vBanderaEmpNeg INTEGER;
	a_count INTEGER;
    cncptoquincid INTEGER;
    ---- CURSORES
    C1 CURSOR FOR
        SELECT  EQ.cod_empquincenaid,EQ.cod_empleadoid_fk,Q.cnu_numquincena,Q.cod_quincenaid, --CQ.cod_conceptoid_fk
				CQ.cod_cncptoquincid, CQ.cod_empquincenaid_fk, CQ.cod_conceptoid_fk, CO.cod_nbconcepto, CO.cod_claveconcepto
        FROM sgnom.tsgnomempquincena EQ
        INNER JOIN sgnom.tsgnomcabecera C 
            ON C.cod_cabeceraid = EQ.cod_cabeceraid_fk
        INNER JOIN sgnom.tsgnomquincena Q
            ON Q.cod_quincenaid = C.cod_quincenaid_fk
		INNER JOIN sgnom.tsgnomcncptoquinc CQ 
			ON CQ.cod_empquincenaid_fk = EQ.cod_empquincenaid
		INNER JOIN sgnom.tsgnomconcepto CO 
		    ON CO.cod_conceptoid = CQ.cod_conceptoid_fk
        WHERE C.cod_cabeceraid = vIdNomina
		AND CO.cod_calculoid_fk = 2
        AND bol_estatusemp != 'f'
        ORDER BY CO.cod_tipoconceptoid_fk, CO.cnu_prioricalculo, EQ.cod_empquincenaid;
    
    C3 CURSOR FOR
        SELECT CQ.cod_cncptoquincid, CQ.cod_empquincenaid_fk, CQ.cod_conceptoid_fk, CO.cod_nbconcepto, CO.cod_claveconcepto
        FROM sgnom.tsgnomcncptoquinc CQ
        INNER JOIN sgnom.tsgnomconcepto CO ON CO.cod_conceptoid = CQ.cod_conceptoid_fk
        WHERE CO.cod_calculoid_fk = 2
		AND CQ.cod_conceptoid_fk != 9
        ORDER BY CO.cod_tipoconceptoid_fk, CO.cnu_prioricalculo;

    C4 CURSOR FOR
        SELECT des_formula
        FROM sgnom.tsgnomformula
        WHERE cod_formulaid =
                (SELECT CO.cod_formulaid_fk 
                FROM sgnom.tsgnomconcepto CO
                WHERE CO.cod_conceptoid = vIdConcepto01fk );

    C10 CURSOR FOR
        SELECT des_formula
        FROM sgnom.tsgnomformula
        WHERE cod_formulaid = 9;

    

    BEGIN
        OPEN C1;
        RAISE INFO 'abre cursor C1';
        LOOP 
            FETCH C1 INTO vIdNomEmpPla01, vIdEmpleado,vNumeroQuincena,vIdQuincena,cncptoquincid, vIdConEmpPla01,vIdConcepto01fk,vNombreConcepto,vClaveConcepto;
            EXIT WHEN not found;
            OPEN C4;
            RAISE INFO 'abre cursor C10 -> C4';
            LOOP
                FETCH C4 INTO vFormula;
                EXIT WHEN not found;
                vFormula2 :=vFormula;
                vNumeroArgumentos:=(SELECT COUNT(*)
                                    FROM regexp_matches(vFormula, ':[^#0-9/*+$%-:]+', 'g'));
                RAISE INFO 'vformula %', vFormula;
                RAISE INFO 'vNumeroArgumentos %', vNumeroArgumentos;
                FOR i IN 1 .. vNumeroArgumentos
                LOOP
                    SELECT regexp_replace(
                        (SELECT contenido
                        FROM
                        (SELECT contenido, row_number() OVER () AS indice --COUNT(*)
                        FROM 
                        regexp_matches(vFormula, ':[^#0-9/*+$%-:]+', 'g') AS contenido
                        ) AS todo
                        WHERE indice = i)::varchar, '[{:}"]', '', 'g') 
                        --WHERE indice = )::"varchar", '[[:punct:]]', '', 'g')
                        INTO vArgumento;
                    --Se obtiene el nombre del argumento
                    SELECT cod_nbargumento INTO vNombreArgumento
                    FROM sgnom.tsgnomargumento
                    WHERE cod_nbargumento = vArgumento;
                    RAISE INFO 'nomARG: % %', vArgumento, vNombreArgumento;
                    --Se obtiene la funci�n del argumento
                    SELECT des_funcionbd INTO vFuncionOracle
                    FROM sgnom.tsgnomargumento
                   	WHERE cod_nbargumento = vArgumento;
                    RAISE INFO 'fnARG: %, %', vArgumento,vFuncionOracle;
                    IF (vFuncionOracle IS NOT NULL) THEN
                        IF (vFuncionOracle = 'fn_dias_laborados') THEN 
                            vFormulaEjecutar :='sgnom.'||vFuncionOracle||'('||vIdEmpleado||','||vIdQuincena||')';
                            vCadenaEjecutar :='select ' || vFormulaEjecutar;
                            RAISE INFO 'cadena %', vCadenaEjecutar;
                            EXECUTE vCadenaEjecutar INTO vDiasLaborados;
                            vValorArgumento :=vDiasLaborados;
                            RAISE INFO 'RES laborados %', vValorArgumento;
						ELSIF (vFuncionOracle = 'fn_sueldo_base') THEN 
                            vFormulaEjecutar :='sgnom.'||vFuncionOracle||'('||vIdEmpleado||')';
                            vCadenaEjecutar :='select ' || vFormulaEjecutar;
                            RAISE INFO 'cadena %', vCadenaEjecutar;
                            EXECUTE vCadenaEjecutar INTO vSueldoBase;
                            vValorArgumento :=vSueldoBase;
                            RAISE INFO 'RES SB %', vValorArgumento;
                        ELSIF (vFuncionOracle = 'fn_bono_asistencia_imss') THEN 
                            vFormulaEjecutar :='sgnom.'||vFuncionOracle||'('||vIdEmpleado||')';
                            vCadenaEjecutar :='select ' || vFormulaEjecutar;
                            EXECUTE vCadenaEjecutar INTO vBonoAsistenciaIMSS;
                            vValorArgumento :=vBonoAsistenciaIMSS;
                        END IF;
                    ELSE
                    	SELECT imp_valorconst INTO vConstante
						FROM sgnom.tsgnomargumento
                   		WHERE cod_nbargumento = vArgumento;
                    	vValorArgumento :=vConstante;
						RAISE INFO 'RES constante %', vConstante;
					END IF;
                    SELECT regexp_replace(vFormula2, vArgumento, vValorArgumento::"varchar", 'g') INTO vFormula2;
                    RAISE INFO 'RES vFormula2 %, vArgumento %, vValorArgumento % ', 
					vFormula2,vArgumento,vValorArgumento;
                    
                END LOOP;
				SELECT regexp_replace(vFormula2, '[:]', '', 'g') INTO vFormula2;
				vCadenaImporte:='SELECT '|| vFormula2 || '';
				RAISE INFO 'RES vCadenaImporte %',  vCadenaImporte;
                -- DBMS_OUTPUT.PUT_LINE(vFormula2);
                EXECUTE vCadenaImporte INTO vImporteActualizado;
                RAISE INFO 'RES vImporteActualizado %', vImporteActualizado;
                --COMMIT;
            END LOOP;
            CLOSE C4;
            OPEN C4;
            LOOP 
                FETCH C4 INTO vFormula;
                EXIT WHEN not found;
                vFormula2 :=vFormula;
                vXML :='<?xml version="1.0" encoding="ISO8859_1"?>                
                <mx.org.ift.serp.conceptoBindingXml.DetallesConcepto>                  
                <claveConcepto>' || vClaveConcepto || '</claveConcepto>                  
                <nombreConcepto>' || vNombreConcepto || '</nombreConcepto>';
                vXML :=vXML ||'<formula>' || vFormula || '</formula>                  
                <argumentos>';
                --Se obtiene el numero de argumentos que contiene la formula
                vNumeroArgumentos:=(SELECT COUNT(*)
                                    FROM regexp_matches(vFormula, ':[^#0-9/*+$%-:]+', 'g'));
                FOR i IN 1 .. vNumeroArgumentos
                LOOP
                    SELECT regexp_replace(
                        (SELECT contenido
                        FROM
                            (SELECT contenido, row_number() OVER () AS indice --COUNT(*)
                            FROM 
                            regexp_matches(vFormula, ':[^#0-9/*+$%-:]+', 'g') AS contenido
                            ) AS todo
                        WHERE indice = i)::varchar, '[{:}"]', '', 'g') 
                        --WHERE indice = )::"varchar", '[[:punct:]]', '', 'g')
                        INTO vArgumento;
                    --Se obtiene el nombre del argumento
                    SELECT cod_nbargumento INTO vNombreArgumento
                    FROM sgnom.tsgnomargumento
                    WHERE cod_nbargumento = vArgumento;
                    RAISE INFO 'nomARG: % %', vArgumento, vNombreArgumento;
                    --Se obtiene la funci�n del argumento
                    SELECT des_funcionbd INTO vFuncionOracle
                    FROM sgnom.tsgnomargumento
                    WHERE cod_nbargumento = vArgumento;
                    RAISE INFO 'fnARG: %, %', vArgumento,vFuncionOracle;
                    --Se calcula la funci�n del argumento
                    IF (vFuncionOracle IS NOT NULL) THEN
                        IF (vFuncionOracle = 'fn_dias_laborados') THEN 
                            vFormulaEjecutar :='sgnom.'||vFuncionOracle||'('||vIdEmpleado||','||vIdQuincena||')';
                            vCadenaEjecutar :='select ' || vFormulaEjecutar;
                            RAISE INFO 'cadena %', vCadenaEjecutar;
                            EXECUTE vCadenaEjecutar INTO vDiasLaborados;
                            vValorArgumento :=vDiasLaborados;
                            RAISE INFO 'RES laborados %', vValorArgumento;
                        ELSIF (vFuncionOracle = 'fn_sueldo_base') THEN 
                            vFormulaEjecutar :='sgnom.'||vFuncionOracle||'('||vIdEmpleado||')';
                            vCadenaEjecutar :='select ' || vFormulaEjecutar;
                            RAISE INFO 'cadena %', vCadenaEjecutar;
                            EXECUTE vCadenaEjecutar INTO vSueldoBase;
                            vValorArgumento :=vSueldoBase;
                            RAISE INFO 'RES SB %', vValorArgumento;
                        END IF;
                    ELSE
                        SELECT imp_valorconst INTO vConstante
                        FROM sgnom.tsgnomargumento
                        WHERE cod_nbargumento = vArgumento;
                        vValorArgumento :=vConstante;
                        RAISE INFO 'RES constante %', vConstante;
                    END IF;
                    IF(vValorArgumento IS NULL) THEN 
                        vValorArgumento :=0;
                    END IF;
                    SELECT regexp_replace(vFormula2, '[:]', '', 'g') INTO vFormula2
                    FROM (SELECT des_formula
                            FROM sgnom.tsgnomformula
                            WHERE cod_formulaid =
                                    (SELECT CO.cod_formulaid_fk 
                                    FROM sgnom.tsgnomconcepto CO
                                    WHERE CO.cod_conceptoid = vIdConcepto01fk)) form;
                    --Se continua la construccion del XML agregando cada argumento
                    vXML:= vXML || '<mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>' || vArgumento || '</nombre>                            
                    <valor>' || vValorArgumento || '</valor> <descripcion>' || vNombreArgumento || '</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento>';
                END LOOP;
                EXECUTE vCadenaImporte INTO vImporteActualizado;
                vXML :=vXML || '</argumentos>';
                IF(xmlDesglose IS NOT NULL) THEN 
                    vXML := vXML || xmlDesglose;
                    xmlDesglose :=NULL;
                END IF;
                --Se agrega el importe total al XML
                vXML:=vXML || '<importe>' || vImporteActualizado || '</importe> </mx.org.ift.serp.conceptoBindingXml.DetallesConcepto>  ';
                --Se guarda el importe del concepto y el XML generado
				RAISE INFO 'dentro C4 ACTUALIZANDO EL EQ %, id %', vIdConEmpPla01, vIdEmpleado;
                UPDATE sgnom.tsgnomcncptoquinc
				SET imp_concepto = vImporteActualizado, xml_desgloce = vXML::xml
				WHERE cod_empquincenaid_fk = vIdConEmpPla01;
                --AND cod_conceptoid_fk = vIdConcepto01fk
                --AND cod_cncptoquincid = cncptoquincid;
				GET DIAGNOSTICS a_count = ROW_COUNT;
				RAISE INFO 'a_count %', a_count;
                IF (a_count>0) THEN 
                    vNumRegistros := vNumRegistros + 1 ;
					RAISE INFO 'vNumRegistros %', vNumRegistros;
                END IF;
                --COMMIT; 
            END LOOP;
            CLOSE C4;
			RAISE INFO 'fuera C4 ACTUALIZANDO EL EQ %, id %', vIdConEmpPla01, vIdEmpleado;
            SELECT sgnom.fn_bono_adicional_incidencia(vIdConEmpPla01);
            vSueldoBase :=0.0;
            vDiasLaborados :=0;
            vCompensasionG :=0;
            vDespensa :=0;
            vPorcentajeManTer :=0;
            vApoyoVehicular :=0;
            vAportacionSSIGF :=0;
            vPrimaVacacional :=0;
            vFaltasSueldoEst :=0;
            vFaltasSueldoEv :=0;
            vFaltasSueldoEstAA :=0;
            vFaltasCompEst :=0;
            vFaltasCompEv :=0;
            vFaltasCompEstAA :=0;
            vISRSueldo :=0;
            vISRCompGaran :=0;
            vISRSSI :=0;
            vISRApoyoVehic :=0;
            vISRPrimaVac :=0;
            vValorQuinquenio :=0;
            vSueldoCompactado :=0;
            vSueldoBimestral :=0;
            vQuinquenioBimestral :=0;
            vSumaSRCEAYV :=0;
            vSueldoBimestralEV :=0;
            vQuinquenioBimestralEV:=0;
            vSumaSRCEAYVEV :=0;
            vPensionAlimenticia :=0;
            vSueldoCompactado :=0;
            vSumaSARP :=0;
            vSumaFOVP :=0;
            vSumaSRCEAYVPAT :=0;
            vSumaSRCEAYVPAT :=0;
            vPorcentajePension :=0;
            vISREstDesemp :=0;
            vISRHonorarios :=0;
            vValorEstimulo :=0;
            vAhorroSolidario :=0;
            vConstante :=0;
            vImpLicMedSueldo :=0; 
        END LOOP;
		CLOSE C1;
        --Se llama la funcion que calcula los importes de cada concepto
        SELECT sgnom.fn_calcula_importes_nomina(vIdNomina,NULL,0) INTO vBanderaImporte;
		--RETURN vNumRegistros::varchar;
        IF vNumRegistros > 0 THEN 
            vBandera := 1;
        ELSIF vNumRegistros = 0 THEN 
            vBandera := 0;
        ELSE 
            vBandera:=2;
        END IF;
        RETURN vBandera;
		EXCEPTION
		WHEN NO_DATA_FOUND THEN
			RAISE EXCEPTION '% exp no data found', vIdEmpleado;
			RETURN NULL;
    END;
$_$;


ALTER FUNCTION sgnom.fn_calcula_nomina(vidnomina integer) OWNER TO suite;

--
-- TOC entry 580 (class 1255 OID 101966)
-- Name: fn_cargar_nom_imss(character varying, integer); Type: FUNCTION; Schema: sgnom; Owner: postgres
--

CREATE FUNCTION sgnom.fn_cargar_nom_imss(archivo character varying, cabecera integer) RETURNS boolean
    LANGUAGE plpgsql
    AS $_$
    DECLARE
		art text;
		rec RECORD;
    BEGIN
		CREATE TEMPORARY TABLE temporal
            (
    clave integer,
    nss text,
    rfc text,
    curp text,
    fecha_alta date,
    tipo_salario text,
    salario_diario numeric,
    sdi numeric,
    dias_trabajados numeric,
    faltas numeric,
    sueldo numeric,
    premio_asistencia numeric,
    premio_puntualidad numeric,
    total_percepciones numeric,
    total_gravable numeric,
    total_imss numeric,
    total_isr numeric,
    isr numeric,
    imss numeric,
    credito_infonavit numeric,
    subsidio_empleo numeric,
    subsidio_empleo_aplicado numeric,
    total_deducciones numeric,
    neto_pagado numeric,   
    vacaciones numeric,
    prima_vacacional numeric,
    prestamo_personal numeric,
    aguinaldo numeric
            );
		--SELECT regexp_replace(archivo , '''', '', 'g') into art;
		--SELECT quote_ident(archivo) into art;
		--SELECT regexp_matches(archivo, ':[^#0-9/*+$%-:]+', 'g') into art;
		--RAISE INFO 'RES tipo %', art;
        --COPY sgnom.tsgnomnominaimss FROM '/tmp/'.concat(archivo) DELIMITER ',' CSV HEADER;
		--execute format('copy sgnom.tsgnomnominaimss from ''/tmp/%L with delimiter '','' quote ''"'' csv ', archivo);
		execute 'copy temporal from ''C:/tmp/archivos/' || archivo || ''' with delimiter '',''  CSV HEADER';
		----copy temporal FROM 'C:\tmp\copia2.csv' DELIMITER ',' CSV HEADER;
		--execute 'copy sgnom.tsgnomnominaimss from ''/tmp/$1'' with delimiter '','' quote ''"'' csv ' USING art;
        --SELECT * FROM temporal;
		INSERT INTO sgnom.tsgnomnominaimss SELECT nextval('sgnom.seq_nom_imss'::regclass), cabecera, * FROM temporal;
		--Raise 'x % ', (SELECT nombre_trabajador FROM temporal);
		RETURN true;
    END;
$_$;


ALTER FUNCTION sgnom.fn_cargar_nom_imss(archivo character varying, cabecera integer) OWNER TO postgres;

--
-- TOC entry 511 (class 1255 OID 101967)
-- Name: fn_cuota_quin_imss(integer); Type: FUNCTION; Schema: sgnom; Owner: postgres
--

CREATE FUNCTION sgnom.fn_cuota_quin_imss(idempleado integer) RETURNS numeric
    LANGUAGE plpgsql
    AS $$
    DECLARE
		valor DECIMAL;
		--rec RECORD;
    BEGIN
		SELECT imss.imss
        FROM sgnom.tsgnomnominaimss imss
        WHERE (imss.curp, imss.nss, imss.rfc) =        
            (SELECT cod_curp, cod_nss, cod_rfc 
            FROM sgrh.tsgrhempleados 
            WHERE cod_empleado  = idEmpleado) INTO valor;
		RETURN valor;
    END;
$$;


ALTER FUNCTION sgnom.fn_cuota_quin_imss(idempleado integer) OWNER TO postgres;

--
-- TOC entry 537 (class 1255 OID 101968)
-- Name: fn_dias_laborados(integer, integer); Type: FUNCTION; Schema: sgnom; Owner: suite
--

CREATE FUNCTION sgnom.fn_dias_laborados(empid integer, prmnumquincenacalculo integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$

    DECLARE
    ----variables
    vFechaIngreso DATE;
    vFechaBaja DATE;
    vFechaIQuincena DATE;
    vFechaFQuincena DATE;
    vDiasLaborados INTEGER;
    vFecha1 INTEGER;
    vFecha2 INTEGER;
    vFecha3 INTEGER;
    vFecha1Dia INTEGER;
    vCadenaResultado VARCHAR(500);
    ERR_CODE INTEGER;
    ERR_MSG VARCHAR(250);

    ---cursores
    C11 CURSOR FOR 
        SELECT EMP.fec_ingreso, EMP.fec_salida
        FROM sgnom.tsgnomempleados EMP
        WHERE EMP.cod_empleadoid = empid;

    C12 CURSOR FOR 
        SELECT fec_inicio, fec_fin
        FROM sgnom.tsgnomquincena
        WHERE cod_quincenaid =
                    (SELECT cod_quincenaid
                    FROM sgnom.tsgnomquincena Q
                    INNER JOIN sgnom.tsgnomejercicio E ON Q.cod_ejercicioid_fk = E.cod_ejercicioid
                    WHERE cod_quincenaid = prmNumQuincenaCalculo
                    AND E.cnu_valorejercicio = (
                        SELECT MAX(cnu_valorejercicio)
                        FROM sgnom.tsgnomejercicio EJC
                        INNER JOIN sgnom.tsgnomquincena CQ ON EJC.cod_ejercicioid = CQ.cod_ejercicioid_fk
                        INNER JOIN sgnom.tsgnomcabecera CAB ON CQ.cod_quincenaid = CAB.cod_quincenaid_fk));

    BEGIN
		RAISE INFO 'inicia funcion laborales';
        OPEN C11; RAISE INFO 'inicia C11 laboral';
        LOOP
        FETCH C11 INTO vFechaIngreso, vFechaBaja;
			RAISE INFO 'contenido C11 % , %', vFechaIngreso,vFechaBaja;
            EXIT WHEN not found;
            OPEN C12; RAISE INFO 'inicia C12 laboral';
            LOOP
            FETCH C12 INTO vFechaIQuincena, vFechaFQuincena;
                RAISE INFO 'contenido C12 % , %', vFechaIQuincena,vFechaFQuincena;
                EXIT WHEN not found;
                -- DBMS_OUTPUT.PUT_LINE('vFechaIngreso '||vFechaIngreso||' vFechaBaja '||vFechaBaja||' vFechaIQuincena '||vFechaIQuincena||' vFechaFQuincena '||vFechaFQuincena );
                --Escenario1:trabajo la quincena completa,
                --ya sea porque ya llevaba tiempo trabajando o ingreso en inicio de quiincena
                IF(vFechaIngreso<=vFechaIQuincena AND vFechaBaja IS NULL) THEN
                    vDiasLaborados:= 15;
                --Escenario2:ingreso despues del inicio de la quincena,
                --se toma la fecha fin de la quincea y se le resta la fecha en la que entro,
                ELSIF(vFechaIngreso >vFechaIQuincena AND vFechaBaja IS NULL) THEN
                    vFecha1 :=TO_CHAR(vFechaFQuincena,'YYYYMMDD');
                    vFecha1Dia :=TO_CHAR(vFechaFQuincena,'DD');
                    vFecha2 :=TO_CHAR(vFechaIngreso,'YYYYMMDD');
                    IF(vFecha1!=vFecha2) THEN
                        IF(vFecha1Dia =28) THEN
                            vFecha1 :=vFecha1+2;
                        ELSIF(vFecha1Dia=29) THEN
                            vFecha1 :=vFecha1+1;
                        ELSIF(vFecha1Dia=31) THEN
                            vFecha1 :=vFecha1-1;
                        END IF;
                    END IF;
                    vDiasLaborados:= (vFecha1-vFecha2)+1;
                --Escenario3:se dio de baja antes de que termine la quincena, se toma la fecha de baja y se le resta la fecha de inicio de quincena
                ELSIF(vFechaIngreso<=vFechaIQuincena AND vFechaBaja IS NOT NULL) THEN
                    vFecha1 :=TO_CHAR(vFechaBaja,'YYYYMMDD');
                    vFecha2 :=TO_CHAR(vFechaIQuincena,'YYYYMMDD');
                    vDiasLaborados :=(vFecha1-vFecha2)+1;
                    IF(vDiasLaborados >15) THEN
                        vDiasLaborados :=15;
                    END IF;
                --Escenario4:se dio de alta despues del inicio de la quincena y se dio de baja antes que terminar la quincena,
                --se toma la fecha de baja y se le resta la fecha de ingreso
                ELSIF(vFechaIngreso>vFechaIQuincena AND vFechaBaja IS NOT NULL) THEN
                    vFecha1 :=TO_CHAR(vFechaBaja,'YYYYMMDD');
                    vFecha2 :=TO_CHAR(vFechaIngreso,'YYYYMMDD');
                    vFecha3 :=TO_CHAR(vFechaFQuincena,'YYYYMMDD');
                    vFecha1Dia :=TO_CHAR(vFechaFQuincena,'DD');
                    IF(vFecha1=vFecha3) THEN
                        IF(vFecha1Dia =28) THEN
                            vFecha1 :=vFecha1+2;
                        ELSIF(vFecha1Dia=29) THEN
                            vFecha1 :=vFecha1+1;
                        ELSIF(vFecha1Dia=31) THEN
                            vFecha1 :=vFecha1-1;
                        END IF;
                    END IF;
                    vDiasLaborados := (vFecha1-vFecha2)+1;
                END IF;
                RAISE INFO 'dentro loop laborados %', vDiasLaborados;
            END LOOP;
			RAISE INFO 'fin loop 2';
            CLOSE C12;
        END LOOP;
        RAISE INFO 'fin loop 1';
        CLOSE C11;
        --vCadenaResultado:=TO_CHAR(vDiasLaborados);
        RAISE INFO 'final laborados %', vDiasLaborados;
        --DBMS_OUTPUT.PUT_LINE(vCadenaResultado);
        RETURN vDiasLaborados;
		EXCEPTION
		WHEN NO_DATA_FOUND THEN
			RAISE EXCEPTION '% exp no data found', empid;
			RETURN NULL;
		WHEN OTHERS THEN
			--err_code := SQLCODE;
			--err_msg := SUBSTR(SQLERRM, 1, 200);
			RAISE EXCEPTION '% exp others', empid;
			--SPDERRORES('FN_DIAS_LABORADOS',err_code,err_msg,prmIdEmpleado);
			RETURN NULL;
    END;
$$;


ALTER FUNCTION sgnom.fn_dias_laborados(empid integer, prmnumquincenacalculo integer) OWNER TO suite;

--
-- TOC entry 506 (class 1255 OID 101969)
-- Name: fn_empleadosquincena(integer); Type: FUNCTION; Schema: sgnom; Owner: postgres
--

CREATE FUNCTION sgnom.fn_empleadosquincena(idcabecera integer) RETURNS TABLE(idemp_nom integer, idemp_quincena integer, nombre text, puesto character varying, area character varying)
    LANGUAGE plpgsql
    AS $$
BEGIN 
RETURN QUERY 
	SELECT empq.cod_empleadoid_fk idemp_nom,
		empq.cod_empquincenaid idemp_quincena,
		--rhemp.cod_empleado idemp_rh,
		CONCAT(rhemp.des_nombre , ' ', rhemp.des_nombres,  ' ', rhemp.des_apepaterno, ' ', rhemp.des_apematerno)::text nombre,
		(SELECT ps.des_puesto FROM sgrh.tsgrhpuestos ps WHERE ps.cod_puesto = rhemp.cod_puesto ) puesto,
		(SELECT ar.des_nbarea FROM sgrh.tsgrhareas ar WHERE ar.cod_area = (SELECT ps.cod_puesto FROM sgrh.tsgrhpuestos ps WHERE ps.cod_puesto = rhemp.cod_puesto )) area
	FROM sgnom.tsgnomempleados nomemp 
	JOIN sgrh.tsgrhempleados rhemp ON nomemp.cod_empleado_fk = rhemp.cod_empleado
	JOIN sgnom.tsgnomempquincena empq ON nomemp.cod_empleadoid = empq.cod_empleadoid_fk
	WHERE empq.cod_cabeceraid_fk = idcabecera
	ORDER BY 1;
END;
$$;


ALTER FUNCTION sgnom.fn_empleadosquincena(idcabecera integer) OWNER TO postgres;

--
-- TOC entry 577 (class 1255 OID 102535)
-- Name: fn_incidencias_por_quincena(integer); Type: FUNCTION; Schema: sgnom; Owner: postgres
--

CREATE FUNCTION sgnom.fn_incidencias_por_quincena(quincena integer) RETURNS TABLE(idincidencia integer, fechaalta date, clave character varying, incidencia character varying, idtipo character, desctipo text, cantidad smallint, actividad character varying, comentarios text, reportaid integer, reportanb text, autorizaid integer, autorizanb text, perfil character varying, detallefechas text, montoincidencia numeric, montopagado numeric, validacion boolean, aceptacion boolean, autorizacion boolean, quincenaid integer, desquincena character varying, creaid integer, creanb text)
    LANGUAGE plpgsql
    AS $$
BEGIN 
RETURN QUERY 
SELECT incidencias.cod_incidenciaid idincidencia,
		incidencias.aud_feccreacion fechaalta,
		catincidencias.cod_claveincidencia clave,
		catincidencias.cod_nbincidencia incidencia,
		catincidencias.cod_tipoincidencia idtipo,
		CASE 
			WHEN catincidencias.cod_tipoincidencia='1' THEN
			 'HORAS'
			WHEN catincidencias.cod_tipoincidencia='2' THEN
			 'DIAS' 
		  WHEN catincidencias.cod_tipoincidencia='3' THEN
			 'ACTIVIDAD' 
			ELSE
			 'NO DATA'
		END desc_tipo,
		incidencias.cnu_cantidad cantidad,
		incidencias.des_actividad actividad,
		incidencias.txt_comentarios comentarios,
		incidencias.cod_empreporta_fk reportaid,
		(SELECT CONCAT(rhempleados.des_nombre ||' ', rhempleados.des_nombres ,' ' || rhempleados.des_apepaterno || ' ' || rhempleados.des_apematerno) 
		FROM sgrh.tsgrhempleados rhempleados
		WHERE rhempleados.cod_empleado = (SELECT nom.cod_empleado_fk FROM sgnom.tsgnomempleados nom WHERE nom.cod_empleadoid = incidencias.cod_empreporta_fk)
		) reportanb,
		incidencias.cod_empautoriza_fk autorizaid,
		(SELECT CONCAT(rhempleados.des_nombre ||' ', rhempleados.des_nombres ,' ' || rhempleados.des_apepaterno || ' ' || rhempleados.des_apematerno) 
		FROM sgrh.tsgrhempleados rhempleados
		WHERE rhempleados.cod_empleado = incidencias.cod_empautoriza_fk
		) autorizanb,
		catincidencias.cod_perfilincidencia perfil,
		CAST(incidencias.xml_detcantidad AS TEXT) detallefechas,
		catincidencias.imp_monto montoincidencia,
		incidencias.imp_monto montopagado,
		incidencias.bol_validacion validacion, --validacion rh
		incidencias.bol_aceptacion aceptacion, --aceptacion lider de celula
		incidencias.bol_pago autorizacion, --aceptacion finanzas
		incidencias.cod_quincenaid_fk quincena,
		(SELECT qui.des_quincena FROM sgnom.tsgnomquincena qui WHERE qui.cod_quincenaid = incidencias.cod_quincenaid_fk) desquincena,
		incidencias.aud_codcreadopor creaid,
		(SELECT CONCAT(rhempleados.des_nombre ||' ', rhempleados.des_nombres ,' ' || rhempleados.des_apepaterno || ' ' || rhempleados.des_apematerno) 
		FROM sgrh.tsgrhempleados rhempleados
		WHERE rhempleados.cod_empleado = incidencias.aud_codcreadopor
		) creanb
FROM sgnom.tsgnomincidencia incidencias
JOIN sgnom.tsgnomcatincidencia catincidencias 
ON incidencias.cod_catincidenciaid_fk = catincidencias.cod_catincidenciaid
--WHERE incidencias.cod_empreporta_fk = idempleado 
	AND incidencias.cod_quincenaid_fk = quincena
	AND incidencias.bol_estatus = true 
ORDER BY 1 DESC, 2;

END;
$$;


ALTER FUNCTION sgnom.fn_incidencias_por_quincena(quincena integer) OWNER TO postgres;

--
-- TOC entry 512 (class 1255 OID 101971)
-- Name: fn_infonavit_imss(integer); Type: FUNCTION; Schema: sgnom; Owner: postgres
--

CREATE FUNCTION sgnom.fn_infonavit_imss(idempleado integer) RETURNS numeric
    LANGUAGE plpgsql
    AS $$
    DECLARE
		valor DECIMAL;
		--rec RECORD;
    BEGIN
		SELECT imss.credito_infonavit
        FROM sgnom.tsgnomnominaimss imss
        WHERE (imss.curp, imss.nss, imss.rfc) =        
            (SELECT cod_curp, cod_nss, cod_rfc 
            FROM sgrh.tsgrhempleados 
            WHERE cod_empleado  = idEmpleado) INTO valor;
		RETURN valor;
    END;
$$;


ALTER FUNCTION sgnom.fn_infonavit_imss(idempleado integer) OWNER TO postgres;

--
-- TOC entry 539 (class 1255 OID 101972)
-- Name: fn_insertacabecera(integer, integer, character varying, integer); Type: FUNCTION; Schema: sgnom; Owner: suite
--

CREATE FUNCTION sgnom.fn_insertacabecera(quincena integer, tipo integer, nombre character varying, credopor integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$
    DECLARE
    banderaRepetido BOOLEAN;
    rec1 RECORD;
    contador integer := 0;
    bandera boolean := false;
    rec RECORD;
    rec2 RECORD;
    cabecera INTEGER;
    BEGIN

        FOR rec1 IN SELECT cod_quincenaid_fk, cod_tiponominaid_fk, cod_cabeceraid
        FROM sgnom.tsgnomcabecera
		WHERE cod_estatusnomid_fk != 6
        ORDER BY 3 DESC
        LOOP
            RAISE INFO 'RES for quincena %', rec1.cod_quincenaid_fk;
            RAISE INFO 'RES for tipo %', rec1.cod_tiponominaid_fk;
            IF((rec1.cod_quincenaid_fk = quincena) AND (rec1.cod_tiponominaid_fk = tipo)) THEN
                RAISE INFO 'RES if quincena %', rec1.cod_quincenaid_fk;
                RAISE INFO 'RES if tipo %', rec1.cod_tiponominaid_fk;
                RAISE INFO 'repetido ';
                banderaRepetido = TRUE;
            END IF;
        END LOOP; 

        IF (banderaRepetido) THEN
            RAISE INFO '\\\\\\\\\\';
        ELSE
            RAISE INFO 'crea nomina';
            
            INSERT INTO sgnom.tsgnomcabecera(
                cod_nbnomina, fec_creacion, 
                imp_totpercepcion, imp_totdeduccion, imp_totalemp, 
                cod_quincenaid_fk, cod_tiponominaid_fk, cod_estatusnomid_fk, 
                aud_codcreadopor, aud_feccreacion)
                VALUES (nombre, CURRENT_DATE, 
                0.0, 0.0, 0.0, 
                quincena, tipo, 1,
                credopor, CURRENT_DATE)  RETURNING cod_cabeceraid INTO cabecera;

            FOR rec IN SELECT cod_empleadoid 
                        FROM sgnom.tsgnomempleados 
                        WHERE tsgnomempleados.fec_ingreso <= 
                                (SELECT tsgnomquincena.fec_fin
                                FROM sgnom.tsgnomquincena, sgnom.tsgnomcabecera
                                WHERE tsgnomquincena.cod_quincenaid = tsgnomcabecera.cod_quincenaid_fk
                                AND tsgnomcabecera.cod_cabeceraid = cabecera)
                        AND (tsgnomempleados.fec_salida >= 
                                (SELECT tsgnomquincena.fec_inicio
                                FROM sgnom.tsgnomquincena, sgnom.tsgnomcabecera
                                WHERE tsgnomquincena.cod_quincenaid = tsgnomcabecera.cod_quincenaid_fk
                                AND tsgnomcabecera.cod_cabeceraid = cabecera)
                        OR tsgnomempleados.fec_salida IS NULL)
                        AND tsgnomempleados.bol_estatus = 't'
                        ORDER BY 1
            LOOP
                INSERT INTO sgnom.tsgnomempquincena(cod_empleadoid_fk, cod_cabeceraid_fk, bol_estatusemp,
												   imp_totpercepcion, imp_totdeduccion, imp_totalemp)
                VALUES (rec.cod_empleadoid, cabecera, 't',
					   0.0, 0.0, 0.0);
                
                contador := contador + 1;
            END LOOP;
            FOR rec2 IN SELECT cod_empquincenaid 
                        FROM sgnom.tsgnomempquincena 
                        WHERE cod_cabeceraid_fk = cabecera
            LOOP
                INSERT INTO sgnom.tsgnomconfpago(
                    cod_empquincenaid_fk)
                    VALUES (rec2.cod_empquincenaid);
				INSERT INTO sgnom.tsgnomcncptoquinc(
	 				cod_empquincenaid_fk, cod_conceptoid_fk, imp_concepto, 
					imp_gravado, imp_exento)
				VALUES (rec2.cod_empquincenaid, 1, 0.0, 
						0.0, 0.0);
            END LOOP;
            IF (contador > 0) THEN
				UPDATE sgnom.tsgnomcabecera SET cnu_totalemp = contador WHERE cod_cabeceraid = cabecera;

                bandera := true;

            ELSIF (contador = 0) THEN
                --ROLLBACK;
                bandera := false;
            END IF;
            
        END IF;
        IF bandera = false THEN
            --devuelve esta bandera
            --RAISE 'entra';
            DELETE FROM sgnom.tsgnomcabecera WHERE cod_cabeceraid = cabecera;
            return 0;
        END IF;
        --EXCEPTION 
        --WHEN OTHERS THEN 
            --ROLLBACK;
        RETURN cabecera;
    END;
$$;


ALTER FUNCTION sgnom.fn_insertacabecera(quincena integer, tipo integer, nombre character varying, credopor integer) OWNER TO suite;

--
-- TOC entry 513 (class 1255 OID 101974)
-- Name: fn_isr_imss(integer); Type: FUNCTION; Schema: sgnom; Owner: postgres
--

CREATE FUNCTION sgnom.fn_isr_imss(idempleado integer) RETURNS numeric
    LANGUAGE plpgsql
    AS $$
    DECLARE
		valor DECIMAL;
		--rec RECORD;
    BEGIN
		SELECT imss.isr
        FROM sgnom.tsgnomnominaimss imss
        WHERE (imss.curp, imss.nss, imss.rfc) =        
            (SELECT cod_curp, cod_nss, cod_rfc 
            FROM sgrh.tsgrhempleados 
            WHERE cod_empleado  = idEmpleado) INTO valor;
		RETURN valor;
    END;
$$;


ALTER FUNCTION sgnom.fn_isr_imss(idempleado integer) OWNER TO postgres;

--
-- TOC entry 530 (class 1255 OID 102344)
-- Name: fn_prestamo_personal_imss(integer); Type: FUNCTION; Schema: sgnom; Owner: postgres
--

CREATE FUNCTION sgnom.fn_prestamo_personal_imss(idempleado integer) RETURNS numeric
    LANGUAGE plpgsql
    AS $$
    DECLARE
		valor DECIMAL;
		--rec RECORD;
    BEGIN
		SELECT imss.prestamo_personal
        FROM sgnom.tsgnomnominaimss imss
        WHERE (imss.curp, imss.nss, imss.rfc) =        
            (SELECT cod_curp, cod_nss, cod_rfc 
            FROM sgrh.tsgrhempleados 
            WHERE cod_empleado  = idEmpleado) INTO valor;
		RETURN valor;
    END;
$$;


ALTER FUNCTION sgnom.fn_prestamo_personal_imss(idempleado integer) OWNER TO postgres;

--
-- TOC entry 514 (class 1255 OID 101975)
-- Name: fn_prima_vacacional_imss(integer); Type: FUNCTION; Schema: sgnom; Owner: postgres
--

CREATE FUNCTION sgnom.fn_prima_vacacional_imss(idempleado integer) RETURNS numeric
    LANGUAGE plpgsql
    AS $$
    DECLARE
		valor DECIMAL;
		--rec RECORD;
    BEGIN
		SELECT imss.prima_vacacional
        FROM sgnom.tsgnomnominaimss imss
        WHERE (imss.curp, imss.nss, imss.rfc) =        
            (SELECT cod_curp, cod_nss, cod_rfc 
            FROM sgrh.tsgrhempleados 
            WHERE cod_empleado  = idEmpleado) INTO valor;
		RETURN valor;
    END;
$$;


ALTER FUNCTION sgnom.fn_prima_vacacional_imss(idempleado integer) OWNER TO postgres;

--
-- TOC entry 571 (class 1255 OID 102541)
-- Name: fn_salario_diario(integer); Type: FUNCTION; Schema: sgnom; Owner: postgres
--

CREATE FUNCTION sgnom.fn_salario_diario(empid integer) RETURNS numeric
    LANGUAGE plpgsql
    AS $$
    DECLARE
    rtSueldoBase numeric;
    BEGIN
        SELECT imp_honorarios / 30
        INTO rtSueldoBase
        FROM sgnom.tsgnomempleados
        WHERE cod_empleadoid = empid;
        RETURN rtSueldoBase;
    END;
$$;


ALTER FUNCTION sgnom.fn_salario_diario(empid integer) OWNER TO postgres;

--
-- TOC entry 516 (class 1255 OID 101976)
-- Name: fn_subsidio_empleo_imss(integer); Type: FUNCTION; Schema: sgnom; Owner: postgres
--

CREATE FUNCTION sgnom.fn_subsidio_empleo_imss(idempleado integer) RETURNS numeric
    LANGUAGE plpgsql
    AS $$
    DECLARE
		valor DECIMAL;
		--rec RECORD;
    BEGIN
		SELECT imss.subsidio_empleo
        FROM sgnom.tsgnomnominaimss imss
        WHERE (imss.curp, imss.nss, imss.rfc) =        
            (SELECT cod_curp, cod_nss, cod_rfc 
            FROM sgrh.tsgrhempleados 
            WHERE cod_empleado  = idEmpleado) INTO valor;
		RETURN valor;
    END;
$$;


ALTER FUNCTION sgnom.fn_subsidio_empleo_imss(idempleado integer) OWNER TO postgres;

--
-- TOC entry 524 (class 1255 OID 101977)
-- Name: fn_sueldo_base(integer); Type: FUNCTION; Schema: sgnom; Owner: suite
--

CREATE FUNCTION sgnom.fn_sueldo_base(empid integer) RETURNS numeric
    LANGUAGE plpgsql
    AS $$
    DECLARE
    rtSueldoBase numeric;
    BEGIN
        SELECT imp_honorarios 
        INTO rtSueldoBase
        FROM sgnom.tsgnomempleados
        WHERE cod_empleadoid = empid;
        RETURN rtSueldoBase;
    END;
$$;


ALTER FUNCTION sgnom.fn_sueldo_base(empid integer) OWNER TO suite;

--
-- TOC entry 535 (class 1255 OID 102345)
-- Name: fn_sueldo_imss(integer); Type: FUNCTION; Schema: sgnom; Owner: postgres
--

CREATE FUNCTION sgnom.fn_sueldo_imss(idempleado integer) RETURNS numeric
    LANGUAGE plpgsql
    AS $$
    DECLARE
		valorIMSS DECIMAL;
		valorNomina DECIMAL;
		--rec RECORD;
    BEGIN
		SELECT imss.sueldo
		FROM sgnom.tsgnomnominaimss imss
        WHERE (imss.curp, imss.nss, imss.rfc) =        
            (SELECT cod_curp, cod_nss, cod_rfc 
            FROM sgrh.tsgrhempleados 
            WHERE cod_empleado = idEmpleado) INTO valorIMSS;
		
		SELECT imp_sueldoimss
		FROM sgnom.tsgnomempleados
		WHERE cod_empleado_fk = idEmpleado INTO valorNomina;
		
		--IF (valorIMSS = valorNomina) THEN
	--		RETURN valorNomina;
		--END IF;
		RETURN valorNomina;
    END;
$$;


ALTER FUNCTION sgnom.fn_sueldo_imss(idempleado integer) OWNER TO postgres;

--
-- TOC entry 531 (class 1255 OID 102367)
-- Name: fn_total_deduc_imss(integer); Type: FUNCTION; Schema: sgnom; Owner: postgres
--

CREATE FUNCTION sgnom.fn_total_deduc_imss(idempleado integer) RETURNS numeric
    LANGUAGE plpgsql
    AS $$
    DECLARE
		total DECIMAL;
		--rec RECORD;
    BEGIN
		SELECT sgnom.fn_isr_imss(idempleado) + sgnom.fn_cuota_quin_imss(idempleado)
				+ sgnom.fn_infonavit_imss(idempleado) + sgnom.fn_subsidio_empleo_imss(idempleado)
				+ sgnom.fn_prestamo_personal_imss(idempleado) INTO total;
		RETURN total;
    END;
$$;


ALTER FUNCTION sgnom.fn_total_deduc_imss(idempleado integer) OWNER TO postgres;

--
-- TOC entry 532 (class 1255 OID 102347)
-- Name: fn_total_percep_imss(integer); Type: FUNCTION; Schema: sgnom; Owner: postgres
--

CREATE FUNCTION sgnom.fn_total_percep_imss(idempleado integer) RETURNS numeric
    LANGUAGE plpgsql
    AS $$
    DECLARE
		total DECIMAL;
		--rec RECORD;
    BEGIN
		SELECT sgnom.fn_sueldo_imss(idempleado) + sgnom.fn_bono_asistencia_imss(idempleado)
				+ sgnom.fn_bono_puntualidad_imss(idempleado) + sgnom.fn_vacaciones_imss(idempleado)
				+ sgnom.fn_prima_vacacional_imss(idempleado) INTO total;
		RETURN total;
    END;
$$;


ALTER FUNCTION sgnom.fn_total_percep_imss(idempleado integer) OWNER TO postgres;

--
-- TOC entry 517 (class 1255 OID 101978)
-- Name: fn_vacaciones_imss(integer); Type: FUNCTION; Schema: sgnom; Owner: postgres
--

CREATE FUNCTION sgnom.fn_vacaciones_imss(idempleado integer) RETURNS numeric
    LANGUAGE plpgsql
    AS $$
    DECLARE
		valor DECIMAL;
		--rec RECORD;
    BEGIN
		SELECT imss.vacaciones
        FROM sgnom.tsgnomnominaimss imss
        WHERE (imss.curp, imss.nss, imss.rfc) =        
            (SELECT cod_curp, cod_nss, cod_rfc 
            FROM sgrh.tsgrhempleados 
            WHERE cod_empleado  = idEmpleado) INTO valor;
		RETURN valor;
    END;
$$;


ALTER FUNCTION sgnom.fn_vacaciones_imss(idempleado integer) OWNER TO postgres;

--
-- TOC entry 540 (class 1255 OID 101979)
-- Name: fn_validapagosnomina(integer); Type: FUNCTION; Schema: sgnom; Owner: suite
--

CREATE FUNCTION sgnom.fn_validapagosnomina(cabecera integer) RETURNS boolean
    LANGUAGE plpgsql
    AS $$
    DECLARE
    contador integer := 0;
    bandera boolean := false;
    rec RECORD;
    BEGIN
		FOR rec IN SELECT bol_pagofinanzas, bol_pagorh
                    FROM sgnom.Tsgnomconfpago 
                    WHERE cod_empquincenaid_fk IN ( SELECT cod_empquincenaid  
                                                    FROM sgnom.tsgnomempquincena 
                                                    WHERE cod_cabeceraid_fk = cabecera )
            LOOP
                RAISE INFO 'finanzas %, rh % ', rec.bol_pagofinanzas, rec.bol_pagorh;
                IF (rec.bol_pagofinanzas = rec.bol_pagorh) THEN
                    RAISE INFO 'acuerdo ';
                ELSE 
                    RAISE INFO 'desacuerdo ';
                    contador := contador + 1;
                END IF;
            END LOOP;

            IF (contador > 0) THEN
				RAISE INFO 'error ';
                bandera := false;
            ELSIF (contador = 0) THEN
                --ROLLBACK;
                RAISE INFO 'bueno ';
                bandera := true;
            END IF;
        RETURN bandera;
    END;
$$;


ALTER FUNCTION sgnom.fn_validapagosnomina(cabecera integer) OWNER TO suite;

--
-- TOC entry 574 (class 1255 OID 102493)
-- Name: historialempleado(integer); Type: FUNCTION; Schema: sgnom; Owner: suite
--

CREATE FUNCTION sgnom.historialempleado(empleado integer) RETURNS TABLE(fecha date, sueldoimss numeric, sueldohono numeric, estatus text)
    LANGUAGE plpgsql
    AS $$
BEGIN 
RETURN QUERY
SELECT (regexp_matches(unnest(xpath('//fecha',
                                        (datos.xml_historial )))::text, ']*>(.*)<\/fecha>'))[1]::DATE AS fecha,

        (regexp_matches(unnest(xpath('//sueldoimss',
                                        (datos.xml_historial )))::text, ']*>(.*)<\/sueldoimss>'))[1]::NUMERIC AS sueldoIMMS,

        (regexp_matches(unnest(xpath('//sueldoadicional',
                                        (datos.xml_historial )))::text, ']*>(.*)<\/sueldoadicional>'))[1]::NUMERIC AS sueldoHono,

        (regexp_matches(unnest(xpath('//estatus',
                                        (datos.xml_historial )))::text, ']*>(.*)<\/estatus>'))[1] AS estatus
										
										FROM (SELECT xml_historial
                                         FROM sgnom.tsgnomempleados
                                         WHERE cod_empleadoid = (empleado)) datos;
		END;
       $$;


ALTER FUNCTION sgnom.historialempleado(empleado integer) OWNER TO suite;

--
-- TOC entry 541 (class 1255 OID 101980)
-- Name: historialquincenasemp(integer); Type: FUNCTION; Schema: sgnom; Owner: suite
--

CREATE FUNCTION sgnom.historialquincenasemp(empleado integer) RETURNS TABLE(cod_empleado integer, fecha_ingreso text, nom_empleado text, des_nbarea character varying, des_puesto character varying, des_quincena character varying, fec_inicio date, fec_fin date, fec_pago date, imp_concepto numeric, quincena integer)
    LANGUAGE plpgsql
    AS $$
BEGIN 
RETURN QUERY 
select empquincena.cod_empleadoid_fk,to_char(nomempl.fec_ingreso,'dd/MM/yyyy'),
(((rhempleados.des_nombre::text || ' '::text) || rhempleados.des_apepaterno::text) || ' '::text) || rhempleados.des_apematerno::text AS nom_empleado,
rharea.des_nbarea,
rhpuestos.des_puesto,
nomquincenas.des_quincena,
nomquincenas.fec_inicio,
nomquincenas.fec_fin,
nomquincenas.fec_pago,
cncptoquincenas.imp_concepto,
cncptoquincenas.cod_empquincenaid_fk
from sgnom.tsgnomcncptoquinc cncptoquincenas
JOIN sgnom.tsgnomempquincena as empquincena
ON empquincena.cod_empquincenaid = cncptoquincenas.cod_empquincenaid_fk
JOIN sgnom.tsgnomcabecera nomcabecera
ON empquincena.cod_cabeceraid_fk = nomcabecera.cod_cabeceraid
JOIN sgnom.tsgnomquincena nomquincenas
ON nomcabecera.cod_quincenaid_fk = nomquincenas.cod_quincenaid
JOIN sgnom.tsgnomempleados as nomempl
ON nomempl.cod_empleadoid = empquincena.cod_empleadoid_fk
JOIN sgrh.tsgrhempleados rhempleados
ON nomempl.cod_empleadoid = rhempleados.cod_empleado
JOIN sgrh.tsgrhpuestos rhpuestos ON rhempleados.cod_puesto = rhpuestos.cod_puesto
JOIN sgrh.tsgrhareas rharea ON rhempleados.cod_area = rharea.cod_area
WHERE nomempl.cod_empleadoid =(empleado);
END;
$$;


ALTER FUNCTION sgnom.historialquincenasemp(empleado integer) OWNER TO suite;

--
-- TOC entry 542 (class 1255 OID 101981)
-- Name: incidencias_por_area(integer); Type: FUNCTION; Schema: sgnom; Owner: suite
--

CREATE FUNCTION sgnom.incidencias_por_area(area integer) RETURNS TABLE(incidenciaid integer)
    LANGUAGE plpgsql
    AS $$
BEGIN 
RETURN QUERY 
	SELECT inc.cod_incidenciaid incidenciaid FROM sgnom.tsgnomincidencia inc WHERE inc.cod_empreporta_fk IN 
		(SELECT nom.cod_empleadoid FROM sgnom.tsgnomempleados nom WHERE nom.cod_empleado_fk IN 
			(SELECT rhe.cod_empleado FROM sgrh.tsgrhempleados rhe WHERE rhe.cod_puesto IN 
				(SELECT pst.cod_puesto FROM sgrh.tsgrhpuestos pst WHERE pst.cod_area = area)
			) 
		);
END;
$$;


ALTER FUNCTION sgnom.incidencias_por_area(area integer) OWNER TO suite;

--
-- TOC entry 578 (class 1255 OID 102536)
-- Name: incidencias_quincena(); Type: FUNCTION; Schema: sgnom; Owner: postgres
--

CREATE FUNCTION sgnom.incidencias_quincena() RETURNS TABLE(idincidencia integer, clave character varying, incidencia character varying, idtipo character, desctipo text, cantidad smallint, actividad character varying, detallefechas text, comentarios text, importe numeric, reporta integer, autoriza integer, nombre text, perfil character varying, aceptacion boolean, validacion boolean, modifica integer, autpago boolean, quincenaid integer, desquincena character varying)
    LANGUAGE plpgsql
    AS $$
BEGIN 
RETURN QUERY 
SELECT incidencias.cod_incidenciaid idincidencia,
        catincidencias.cod_claveincidencia clave,
        catincidencias.cod_nbincidencia incidencia,
        catincidencias.cod_tipoincidencia idtipo,
        CASE 
            WHEN catincidencias.cod_tipoincidencia='1' THEN
             'HORAS'
            WHEN catincidencias.cod_tipoincidencia='2' THEN
             'DIAS' 
          WHEN catincidencias.cod_tipoincidencia='3' THEN
             'ACTIVIDAD' 
            ELSE
             'NO DATA'
        END desctipo,
        incidencias.cnu_cantidad cantidad,
        incidencias.des_actividad actividad,
				CAST(incidencias.xml_detcantidad AS TEXT) detallefechas,
        incidencias.txt_comentarios comentarios,
        incidencias.imp_monto importe,
        incidencias.cod_empreporta_fk reporta, --Codigo del empleado que reporta (sgnom)
				incidencias.cod_empautoriza_fk autoriza, --Codigo del empleado que autoriza (sgnom)
        CONCAT(rhempleados.des_nombre ||' ', rhempleados.des_nombres ,' ' || rhempleados.des_apepaterno || ' ' || rhempleados.des_apematerno), --Nombre del empleado (RH)
        catincidencias.cod_perfilincidencia perfil, --Perfil del empleado que reporta (Incidencia)
				incidencias.bol_aceptacion aceptacion,
        incidencias.bol_validacion validacion,
				incidencias.aud_codmodificadopor modifica,
				incidencias.bol_pago autpago,
				incidencias.cod_quincenaid_fk quincenaid,
				(SELECT qui.des_quincena FROM sgnom.tsgnomquincena qui WHERE qui.cod_quincenaid = incidencias.cod_quincenaid_fk) desquincena
FROM sgnom.tsgnomincidencia incidencias
JOIN sgnom.tsgnomcatincidencia catincidencias ON incidencias.cod_catincidenciaid_fk = catincidencias.cod_catincidenciaid
JOIN sgnom.tsgnomempleados nomemp ON incidencias.cod_empreporta_fk = nomemp.cod_empleadoid
JOIN sgrh.tsgrhempleados rhempleados ON rhempleados.cod_empleado = nomemp.cod_empleado_fk
WHERE incidencias.cod_quincenaid_fk = (SELECT nomquincena.cod_quincenaid FROM sgnom.tsgnomquincena nomquincena
WHERE nomquincena.fec_inicio <= CURRENT_DATE AND nomquincena.fec_fin >= CURRENT_DATE)AND incidencias.bol_estatus = true 
ORDER BY idincidencia;
END;
$$;


ALTER FUNCTION sgnom.incidencias_quincena() OWNER TO postgres;

--
-- TOC entry 518 (class 1255 OID 101983)
-- Name: infohistorial(integer); Type: FUNCTION; Schema: sgnom; Owner: suite
--

CREATE FUNCTION sgnom.infohistorial(empleado integer) RETURNS TABLE(cod_empleadoid integer, nombre_completo text, area character varying, puesto character varying, sueldo numeric, fec_ingreso date)
    LANGUAGE plpgsql
    AS $$
BEGIN 
RETURN QUERY 
SELECT nomempleados.cod_empleadoid,
    (((rhempleados.des_nombre::text || ' '::text) || rhempleados.des_apepaterno::text) || ' '::text) || rhempleados.des_apematerno::text AS nombre_completo,
    rharea.des_nbarea,
    rhpuestos.des_puesto,
    sum(nomempleados.imp_sueldoimss + nomempleados.imp_honorarios) AS sueldo,
	nomempleados.fec_ingreso
   FROM sgnom.tsgnomempleados nomempleados
     JOIN sgrh.tsgrhempleados rhempleados ON nomempleados.cod_empleado_fk = rhempleados.cod_empleado
     JOIN sgrh.tsgrhareas rharea ON rhempleados.cod_area = rharea.cod_area
     JOIN sgrh.tsgrhpuestos rhpuestos ON rhempleados.cod_puesto = rhpuestos.cod_puesto
  where nomempleados.cod_empleadoid=(empleado)
  GROUP BY nomempleados.cod_empleadoid, ((((rhempleados.des_nombre::text || ' '::text) || rhempleados.des_apepaterno::text) || ' '::text) || rhempleados.des_apematerno::text), rharea.des_nbarea, rhpuestos.des_puesto
  ORDER BY nomempleados.cod_empleadoid;
  END;
$$;


ALTER FUNCTION sgnom.infohistorial(empleado integer) OWNER TO suite;

--
-- TOC entry 544 (class 1255 OID 101984)
-- Name: insertar_incidencia_por_empleado(integer, integer, character varying, text, integer, integer, numeric, character varying); Type: FUNCTION; Schema: sgnom; Owner: suite
--

CREATE FUNCTION sgnom.insertar_incidencia_por_empleado(incidenciaid integer, cantidad integer, actividad character varying, comentarios text, reporta integer, crea integer, monto numeric, fechas character varying) RETURNS boolean
    LANGUAGE plpgsql
    AS $$

DECLARE vXML varchar(4000);
arregloFechas DATE[];

BEGIN 
arregloFechas = fechas::DATE[];
vXML :='<?xml version="1.0" encoding="UTF-8"?>                
		<DetalleFechas>';
FOR i IN 1 .. array_upper(arregloFechas, 1)
   LOOP
      vXML := vXML ||'<fecha>' || TO_CHAR(arregloFechas[i],'dd-MM-yyyy') || '</fecha>';
   END LOOP;
vXML := vXML || '</DetalleFechas>';

	INSERT INTO sgnom.tsgnomincidencia(
											"cod_incidenciaid",
											"cod_catincidenciaid_fk", 
											"cnu_cantidad", 
											"des_actividad", 
											"txt_comentarios", 
											"cod_empreporta_fk",
										  "imp_monto",
										  "xml_detcantidad",
											"bol_estatus", 
											"cod_quincenaid_fk",
											"aud_codcreadopor",
											"aud_feccreacion")
			VALUES (
											NEXTVAL('sgnom.seq_incidencia'::regclass), 
											incidenciaid, 
											cantidad, 
											actividad, 
											comentarios, 
											reporta,--nom
											monto,
											vXML :: XML,
											't',
											(SELECT nomquincena.cod_quincenaid 
												FROM sgnom.tsgnomquincena nomquincena
												WHERE nomquincena.fec_inicio <= CURRENT_DATE AND nomquincena.fec_fin >= CURRENT_DATE),
											crea,
											CURRENT_DATE
								);RETURN TRUE;
END;
$$;


ALTER FUNCTION sgnom.insertar_incidencia_por_empleado(incidenciaid integer, cantidad integer, actividad character varying, comentarios text, reporta integer, crea integer, monto numeric, fechas character varying) OWNER TO suite;

--
-- TOC entry 519 (class 1255 OID 101985)
-- Name: obtener_correos(); Type: FUNCTION; Schema: sgnom; Owner: postgres
--

CREATE FUNCTION sgnom.obtener_correos() RETURNS TABLE(nomempleado integer, correo character varying)
    LANGUAGE plpgsql
    AS $$
BEGIN
RETURN QUERY
	SELECT nomemp.cod_empleadoid nomempleado,
			rhemp.des_correo correo
	FROM sgnom.tsgnomempleados nomemp
		JOIN sgrh.tsgrhempleados rhemp
		ON nomemp.cod_empleado_fk = rhemp.cod_empleado
	ORDER BY 1;
END;
$$;


ALTER FUNCTION sgnom.obtener_correos() OWNER TO postgres;

--
-- TOC entry 534 (class 1255 OID 102369)
-- Name: posponer_pago_incidencia(integer, text, integer); Type: FUNCTION; Schema: sgnom; Owner: postgres
--

CREATE FUNCTION sgnom.posponer_pago_incidencia(incidenciaid integer, comentarios text, reporta integer) RETURNS boolean
    LANGUAGE plpgsql
    AS $$
	
	DECLARE comentariosupdate TEXT;
	
	BEGIN 
	comentariosupdate = (SELECT inc.txt_comentarios FROM sgnom.tsgnomincidencia inc WHERE inc.cod_incidenciaid = incidenciaid) || ' Pago pospuesto por FINANZAS: ' || comentarios;
	
		UPDATE sgnom.tsgnomincidencia 
			SET txt_comentarios = comentariosupdate,
			cod_quincenaid_fk = cod_quincenaid_fk +1,
			aud_codmodificadopor = reporta,
			aud_fecmodificacion = CURRENT_DATE
		WHERE cod_incidenciaid = incidenciaid; RETURN TRUE; 
END;
$$;


ALTER FUNCTION sgnom.posponer_pago_incidencia(incidenciaid integer, comentarios text, reporta integer) OWNER TO postgres;

--
-- TOC entry 545 (class 1255 OID 101986)
-- Name: totalimpcab(integer); Type: FUNCTION; Schema: sgnom; Owner: suite
--

CREATE FUNCTION sgnom.totalimpcab(id integer) RETURNS boolean
    LANGUAGE plpgsql
    AS $$
DECLARE
 total numeric(10,2);
 BEGIN
   SELECT (imp_totpercepcion - imp_totdeduccion)
   INTO total
   FROM sgnom.tsgnomcabecera
   WHERE cod_cabeceraid = id;
  UPDATE sgnom.tsgnomcabecera SET imp_totalemp=total
   WHERE cod_cabeceraid = id;
RETURN true;
END;
$$;


ALTER FUNCTION sgnom.totalimpcab(id integer) OWNER TO suite;

--
-- TOC entry 575 (class 1255 OID 102479)
-- Name: trigger_empleadoshst(); Type: FUNCTION; Schema: sgnom; Owner: suite
--

CREATE FUNCTION sgnom.trigger_empleadoshst() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE 
hstXML varchar(4000) := OLD.xml_historial;
sueldoimssXML NUMERIC := OLD.imp_sueldoimss;
sueldoadicionalXML NUMERIC := OLD.imp_honorarios;
estatusXML TEXT := OLD.cod_validaciones;
nuevoXML varchar(4000);
longitud INTEGER = 0;
BEGIN
longitud = (SELECT length(hstXML));
nuevoXML = (SELECT substring(hstXML from 1 for (longitud - 44)));
IF NEW.cod_validaciones <> OLD.cod_validaciones THEN
	nuevoXML := nuevoXML || '<validacion>
                            <mx.org.mbn.serp.historialBindingXml.Dato>
                                <fecha>'|| TO_CHAR(CURRENT_DATE, 'dd-MM-yyyy') ||'</fecha>
                                <estatus>'|| estatusXML ||'</estatus>
                            </mx.org.mbn.serp.historialBindingXml.Dato>
                        </validacion>
                    </mx.org.mbn.serp.historialBindingXml.Datos>';
UPDATE sgnom.tsgnomempleados SET xml_historial = nuevoXML::xml WHERE cod_empleadoid = OLD.cod_empleadoid; RETURN NEW;
ELSE
	nuevoXML := nuevoXML || '<sueldos>
                            <mx.org.mbn.serp.historialBindingXml.Dato>
                                <fecha>'|| TO_CHAR(CURRENT_DATE, 'dd-MM-yyyy') ||'</fecha>
                                <sueldoimss>'|| sueldoimssXML ||'</sueldoimss>
                                <sueldoadicional>' || sueldoadicionalXML ||'</sueldoadicional>
                            </mx.org.mbn.serp.historialBindingXml.Dato>
                        </sueldos>
                    </mx.org.mbn.serp.historialBindingXml.Datos>';
UPDATE sgnom.tsgnomempleados SET xml_historial = nuevoXML::xml WHERE cod_empleadoid = OLD.cod_empleadoid; RETURN NEW;
END IF;

END;
$$;


ALTER FUNCTION sgnom.trigger_empleadoshst() OWNER TO suite;

--
-- TOC entry 543 (class 1255 OID 102494)
-- Name: trigger_nuevoempleado(); Type: FUNCTION; Schema: sgnom; Owner: suite
--

CREATE FUNCTION sgnom.trigger_nuevoempleado() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE 
nuevoXML varchar(4000);
BEGIN 
nuevoXML ='                
               
<mx.org.mbn.serp.historialBindingXml.Datos>
</mx.org.mbn.serp.historialBindingXml.Datos>';
UPDATE "sgnom"."tsgnomempleados" 
SET "xml_historial" = nuevoXML::XML
	WHERE cod_empleadoid = NEW.cod_empleadoid;
RETURN NEW;
END;
$$;


ALTER FUNCTION sgnom.trigger_nuevoempleado() OWNER TO suite;

--
-- TOC entry 563 (class 1255 OID 101987)
-- Name: validaraltas(); Type: FUNCTION; Schema: sgnom; Owner: suite
--

CREATE FUNCTION sgnom.validaraltas() RETURNS TABLE(cod_empleadoid integer, nom_empleado text, cod_rfc character varying, cod_curp character varying, des_nbarea character varying, des_puesto character varying, validar boolean)
    LANGUAGE plpgsql COST 10
    AS $$
BEGIN 
RETURN QUERY 
SELECT nomempleados.cod_empleadoid,
    (((rhempleados.des_nombre::text || ' '::text) || rhempleados.des_apepaterno::text) || ' '::text) || rhempleados.des_apematerno::text AS nom_empleado,
    rhempleados.cod_rfc,
	rhempleados.cod_curp,
    rharea.des_nbarea,
    rhpuestos.des_puesto,
	nomempleados.des_validacion
   FROM sgnom.tsgnomempleados nomempleados
     JOIN sgrh.tsgrhempleados rhempleados ON nomempleados.cod_empleado_fk = rhempleados.cod_empleado
     JOIN sgrh.tsgrhareas rharea ON rhempleados.cod_area = rharea.cod_area
     JOIN sgrh.tsgrhpuestos rhpuestos ON rhempleados.cod_puesto = rhpuestos.cod_puesto 
 WHERE nomempleados.bol_estatus = true
 AND nomempleados.des_validacion is null
  GROUP BY nomempleados.cod_empleadoid, ((((rhempleados.des_nombre::text || ' '::text) || rhempleados.des_apepaterno::text) || ' '::text) || rhempleados.des_apematerno::text), rhempleados.cod_rfc, 
  rhempleados.cod_curp,rharea.des_nbarea, rhpuestos.des_puesto
  ORDER BY nomempleados.cod_empleadoid;
  END;
$$;


ALTER FUNCTION sgnom.validaraltas() OWNER TO suite;

--
-- TOC entry 564 (class 1255 OID 101988)
-- Name: validarbajas(); Type: FUNCTION; Schema: sgnom; Owner: suite
--

CREATE FUNCTION sgnom.validarbajas() RETURNS TABLE(cod_empleadoid integer, nom_empleado text, cod_rfc character varying, cod_curp character varying, des_nbarea character varying, des_puesto character varying, validar boolean)
    LANGUAGE plpgsql COST 10
    AS $$
BEGIN 
RETURN QUERY 
 SELECT nomempleados.cod_empleadoid,
    (((rhempleados.des_nombre::text || ' '::text) || rhempleados.des_apepaterno::text) || ' '::text) || rhempleados.des_apematerno::text AS nom_empleado,
    rhempleados.cod_rfc,
	rhempleados.cod_curp,
    rharea.des_nbarea,
    rhpuestos.des_puesto,
	nomempleados.des_validacion
   FROM sgnom.tsgnomempleados nomempleados
     JOIN sgrh.tsgrhempleados rhempleados ON nomempleados.cod_empleado_fk = rhempleados.cod_empleado
     JOIN sgrh.tsgrhareas rharea ON rhempleados.cod_area = rharea.cod_area
     JOIN sgrh.tsgrhpuestos rhpuestos ON rhempleados.cod_puesto = rhpuestos.cod_puesto
  WHERE nomempleados.bol_estatus = false
  AND nomempleados.des_validacion is null
  GROUP BY nomempleados.cod_empleadoid, ((((rhempleados.des_nombre::text || ' '::text) || rhempleados.des_apepaterno::text) || ' '::text) || rhempleados.des_apematerno::text), rhempleados.cod_rfc, rhempleados.cod_curp,rharea.des_nbarea, rhpuestos.des_puesto
  ORDER BY nomempleados.cod_empleadoid;
END;
$$;


ALTER FUNCTION sgnom.validarbajas() OWNER TO suite;

--
-- TOC entry 573 (class 1255 OID 102478)
-- Name: verinformaciondepersonal(integer); Type: FUNCTION; Schema: sgnom; Owner: suite
--

CREATE FUNCTION sgnom.verinformaciondepersonal(empleado integer) RETURNS TABLE(cod_empleado integer, fec_ingreso text, fec_modificacion text, des_nombre character varying, des_apepaterno character varying, des_apematerno character varying, des_nbarea character varying, des_puesto character varying, des_rol character varying, cod_rfc character varying, cod_curp character varying, cod_nss character varying, fec_nacimiento text, des_direccion character varying, des_correo character varying, correo_personal character varying, cod_tipoaguinaldo character, imp_aguinaldo numeric, quincena integer)
    LANGUAGE plpgsql
    AS $$
BEGIN 
RETURN QUERY 
select nomEmpleados.cod_empleadoid,
                to_char(nomEmpleados.fec_ingreso,'dd/MM/yyyy')as fec_ingreso,
				to_char(nomEmpleados.aud_fecmodificacion,'dd/MM/yyyy')as fec_modificacion,
                rhEmpleados.des_nombre, 
                rhEmpleados.des_apepaterno,
                rhEmpleados.des_apematerno,
                rhArea.des_nbarea,
                rhPuestos.des_puesto,
				rhRoles.des_nbrol,
                rhEmpleados.cod_rfc, 
                rhEmpleados.cod_curp, 
                rhEmpleados.cod_nss,  
                to_char(rhEmpleados.fec_nacimiento,'dd/MM/yyyy')as fec_nacimiento,
                rhEmpleados.des_direccion, 
                rhEmpleados.des_correo,
				rhEmpleados.des_correopersonal,
    (select nomAguinaldo.cod_tipoaguinaldo from sgnom.tsgnomaguinaldo as nomAguinaldo
        join sgnom.tsgnomempquincena nomEmpQuincena on nomAguinaldo.cod_empquincenaid_fk=nomEmpQuincena.cod_empquincenaid
        where nomEmpQuincena.cod_empleadoid_fk=(empleado)),
    (select nomAguinaldo.imp_aguinaldo from sgnom.tsgnomaguinaldo as nomAguinaldo
        join sgnom.tsgnomempquincena nomEmpQuincena on nomAguinaldo.cod_empquincenaid_fk=nomEmpQuincena.cod_empquincenaid
        where nomEmpQuincena.cod_empleadoid_fk=(empleado)),
    (SELECT nomquincena.cod_quincenaid 
            FROM sgnom.tsgnomquincena nomquincena
            WHERE nomquincena.fec_inicio < CURRENT_DATE AND nomquincena.fec_fin > CURRENT_DATE) AS "Quincena actual"
from sgnom.tsgnomempleados as nomEmpleados
    join sgrh.tsgrhempleados rhEmpleados on nomEmpleados.cod_empleado_fk = rhEmpleados.cod_empleado
    join sgrh.tsgrhareas rhArea 
        using (cod_area)
    join sgrh.tsgrhpuestos rhPuestos
        using (cod_puesto)
	join sgrh.tsgrhroles rhRoles
		using (cod_rol)										
where nomEmpleados.cod_empleadoid=(empleado)
group by       nomEmpleados.cod_empleadoid,
				nomEmpleados.aud_fecmodificacion,								
				rhEmpleados.fec_ingreso,
                rhEmpleados.des_nombre, 
                rhEmpleados.des_apepaterno,
                rhEmpleados.des_apematerno,
                rhArea.des_nbarea,
                rhPuestos.des_puesto,
                rhRoles.des_nbrol,
                rhEmpleados.cod_rfc, 
                rhEmpleados.cod_curp, 
                rhEmpleados.cod_nss, 
                rhEmpleados.cod_diasvacaciones, 
                rhEmpleados.fec_nacimiento,
                rhEmpleados.des_direccion, 
                rhEmpleados.des_correo,
		       rhEmpleados.des_correopersonal;											
END;
$$;


ALTER FUNCTION sgnom.verinformaciondepersonal(empleado integer) OWNER TO suite;

--
-- TOC entry 495 (class 1255 OID 99177)
-- Name: buscar_plan(integer); Type: FUNCTION; Schema: sgrh; Owner: suite
--

CREATE FUNCTION sgrh.buscar_plan(planid integer) RETURNS TABLE(cod_plancapacitacion integer, des_nombre character varying, des_instructor character varying, des_criterios character varying, cod_tipocapacitacion integer, aud_feccreacion date, aud_creadopor integer)
    LANGUAGE plpgsql COST 1
    AS $$
BEGIN
RETURN QUERY
select
plan.cod_plancapacitacion,
plan.des_nombre,
plan.des_instructor,
plan.des_criterios,
plan.cod_tipocapacitacion,
plan.aud_feccreacion,
plan.aud_creadopor
from sgrh.tsgrhplancapacitacion plan
WHERE plan.cod_plancapacitacion= planid
group by plan.cod_plancapacitacion,
plan.des_nombre,
plan.des_instructor,
plan.des_criterios,
plan.cod_tipocapacitacion,
plan.aud_feccreacion,
plan.aud_creadopor  order by cod_plancapacitacion asc;
END;
$$;


ALTER FUNCTION sgrh.buscar_plan(planid integer) OWNER TO suite;

--
-- TOC entry 496 (class 1255 OID 99178)
-- Name: crosstab_report_encuesta(integer); Type: FUNCTION; Schema: sgrh; Owner: suite
--

CREATE FUNCTION sgrh.crosstab_report_encuesta(integer) RETURNS TABLE(pregunta character varying, resp1 character varying, resp2 character varying, resp3 character varying, resp4 character varying, resp5 character varying)
    LANGUAGE sql
    AS $_$        
            SELECT * FROM crosstab(
                'SELECT p.des_pregunta AS rowid, 
                        cr.cod_ponderacion as attribute, 
                        cr.des_respuesta as value
                FROM sgrh.tsgrhpreguntasenc p
                INNER JOIN sgrh.tsgrhencuesta e ON p.cod_encuesta = e.cod_encuesta
                LEFT JOIN sgrh.tsgrhrespuestasenc r ON p.cod_pregunta = r.cod_pregunta
                LEFT JOIN sgrh.tsgrhcatrespuestas cr ON r.cod_catrespuesta = cr.cod_catrespuesta
                WHERE e.cod_encuesta = ' || $1
			) 
            AS (
                pregunta VARCHAR(200), 
                resp1 VARCHAR(200), 
                resp2 VARCHAR(200), 
                resp3 VARCHAR(200), 
                resp4 VARCHAR(200), 
                resp5 VARCHAR(200)
            );
    $_$;


ALTER FUNCTION sgrh.crosstab_report_encuesta(integer) OWNER TO suite;

--
-- TOC entry 491 (class 1255 OID 99179)
-- Name: factualizarfecha(); Type: FUNCTION; Schema: sgrh; Owner: suite
--

CREATE FUNCTION sgrh.factualizarfecha() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
declare begin
	new.fec_modificacion:=current_date;
	return new;

end;
$$;


ALTER FUNCTION sgrh.factualizarfecha() OWNER TO suite;

--
-- TOC entry 497 (class 1255 OID 99180)
-- Name: buscar_asistentes_minuta(integer); Type: FUNCTION; Schema: sgrt; Owner: suite
--

CREATE FUNCTION sgrt.buscar_asistentes_minuta(reunionid integer) RETURNS TABLE(nombre_asistente text, area_asistente character varying)
    LANGUAGE sql
    AS $$
SELECT
des_nombre as nombre_asistente,
CONCAT((SELECT area.des_nbarea FROM sgrh.tsgrhempleados emp LEFT JOIN sgrh.tsgrhareas area ON area.cod_area=emp.cod_area WHERE emp.cod_empleado=invitado.cod_empleado),
des_empresa) as area_asistente
FROM sgrt.tsgrtinvitados invitado WHERE  invitado.cnu_asiste='1' and invitado.cod_reunion=reunionid;
$$;


ALTER FUNCTION sgrt.buscar_asistentes_minuta(reunionid integer) OWNER TO suite;

--
-- TOC entry 498 (class 1255 OID 99181)
-- Name: buscar_compromisos_roles_list(integer); Type: FUNCTION; Schema: sgrt; Owner: suite
--

CREATE FUNCTION sgrt.buscar_compromisos_roles_list(reunionid integer) RETURNS TABLE(cod_compromiso integer, des_descripcion character varying, cod_estatus sgrt.estatus_compromiso, fec_solicitud text, fec_compromiso text, validador text, verificador text, ejecutor text)
    LANGUAGE plpgsql
    AS $$

BEGIN
RETURN QUERY
select
CAST(ROW_NUMBER() OVER(ORDER BY (SELECT 1)) as INTEGER) AS cod_compromiso,
com.des_descripcion,
com.cod_estatus,
CAST(to_char(com.fec_solicitud, 'DD/MM/YYYY') as text) as fec_solicitud,
CAST(to_char(com.fec_compromiso, 'DD/MM/YYYY') as text) as fec_compromiso,
(select CONCAT(des_nombre, ' ', des_nombres, ' ', des_apepaterno, ' ', des_apematerno) from sgrh.tsgrhempleados where cod_empleado=com.cod_validador) AS validador,
(select CONCAT(des_nombre, ' ', des_nombres, ' ', des_apepaterno, ' ', des_apematerno) from sgrh.tsgrhempleados where cod_empleado=com.cod_verificador) AS verificador,
(select CONCAT(des_nombre, ' ', des_nombres, ' ', des_apepaterno, ' ', des_apematerno) from sgrh.tsgrhempleados where cod_empleado=com.cod_ejecutor) AS ejecutor
from sgrt.tsgrtcompromisos com where com.cod_reunion=reunionid;

END;
$$;


ALTER FUNCTION sgrt.buscar_compromisos_roles_list(reunionid integer) OWNER TO suite;

--
-- TOC entry 499 (class 1255 OID 99182)
-- Name: buscar_minutas_fechas(text, text); Type: FUNCTION; Schema: sgrt; Owner: suite
--

CREATE FUNCTION sgrt.buscar_minutas_fechas(fecha_inicio text, fecha_fin text) RETURNS TABLE(cod_area integer, des_nbarea character varying, cantidad_minutas integer)
    LANGUAGE sql
    AS $$
SELECT
cod_area,
cod_acronimo as des_nbarea,
cast((SELECT
COUNT(*)
FROM sgrt.tsgrtreuniones reu
INNER JOIN sgrh.tsgrhempleados emp
ON reu.cod_responsable=emp.cod_empleado
INNER JOIN sgrh.tsgrhareas area
ON emp.cod_area=area.cod_area
WHERE area.cod_area=a.cod_area and
reu.fec_fecha >= cast(fecha_inicio as date)
AND reu.fec_fecha <=  cast(fecha_fin as date)
) as INTEGER) AS cantidad_minutas
FROM sgrh.tsgrhareas a
$$;


ALTER FUNCTION sgrt.buscar_minutas_fechas(fecha_inicio text, fecha_fin text) OWNER TO suite;

--
-- TOC entry 500 (class 1255 OID 99183)
-- Name: buscar_proxima_reunion(integer); Type: FUNCTION; Schema: sgrt; Owner: suite
--

CREATE FUNCTION sgrt.buscar_proxima_reunion(reunionid integer, OUT cod_reunion integer, OUT des_nombre_reunion character varying, OUT fec_fecha text, OUT cod_lugar integer, OUT tim_hora character varying, OUT des_nombre_lugar character varying, OUT cod_ciudad integer, OUT des_nbciudad character varying, OUT cod_estadorep_ciudad integer, OUT des_nbestado character varying, OUT cod_estadorep_estado integer) RETURNS SETOF record
    LANGUAGE sql
    AS $$
  select
  reunion.cod_reunion,
  reunion.des_nombre,
  CAST(to_char(reunion.fec_fecha, 'DD/MM/YYYY')as text) as fec_fecha,
  reunion.cod_lugar,
  CAST(reunion.tim_hora as text),
  lugar.des_nombre,
  lugar.cod_ciudad,
ciudad.des_nbciudad,
ciudad.cod_estadorep,
estado.des_nbestado,
estado.cod_estadorep
from sgrt.tsgrtreuniones reunion inner join sgrt.tsgrtlugares lugar on reunion.cod_lugar=lugar.cod_lugar
inner join sgrt.tsgrtciudades ciudad on lugar.cod_ciudad=ciudad.cod_ciudad
inner join sgrt.tsgrtestados estado on ciudad.cod_estadorep=estado.cod_estadorep
where cod_reunion=(select cod_proximareunion from sgrt.tsgrtreuniones where cod_reunion=reunionId);

$$;


ALTER FUNCTION sgrt.buscar_proxima_reunion(reunionid integer, OUT cod_reunion integer, OUT des_nombre_reunion character varying, OUT fec_fecha text, OUT cod_lugar integer, OUT tim_hora character varying, OUT des_nombre_lugar character varying, OUT cod_ciudad integer, OUT des_nbciudad character varying, OUT cod_estadorep_ciudad integer, OUT des_nbestado character varying, OUT cod_estadorep_estado integer) OWNER TO suite;

--
-- TOC entry 501 (class 1255 OID 99184)
-- Name: compromisos_areas_fechas(text, text); Type: FUNCTION; Schema: sgrt; Owner: suite
--

CREATE FUNCTION sgrt.compromisos_areas_fechas(fecha_inicio text, fecha_fin text) RETURNS TABLE(cod_area integer, des_nbarea character varying, tipo text, num integer)
    LANGUAGE sql
    AS $$
select
cod_area,
cod_acronimo as des_nbarea,
CAST('Terminado' as text) as tipo,
cast((SELECT
COUNT(*)
FROM sgrt.tsgrtcompromisos reu
INNER JOIN sgrh.tsgrhempleados emp
ON reu.cod_ejecutor=emp.cod_empleado
INNER JOIN sgrh.tsgrhareas area
ON emp.cod_area=area.cod_area
WHERE reu.cod_estatus='Terminado' AND
area.cod_area=a.cod_area AND
reu.fec_compromiso >= cast(fecha_inicio as date)
AND reu.fec_compromiso <= cast(fecha_fin as date)
)as INTEGER) as num
FROM
sgrh.tsgrhareas a
UNION
select
cod_area,
cod_acronimo as des_nbarea,
CAST('Pendiente' as text) as tipo,
cast((SELECT
COUNT(*)
FROM sgrt.tsgrtcompromisos reu
INNER JOIN sgrh.tsgrhempleados emp
ON reu.cod_ejecutor=emp.cod_empleado
INNER JOIN sgrh.tsgrhareas area
ON emp.cod_area=area.cod_area
WHERE reu.cod_estatus='Pendiente' and
area.cod_area=a.cod_area AND
reu.fec_compromiso >= cast(fecha_inicio as date)
AND reu.fec_compromiso <= cast(fecha_fin as date))as INTEGER) as num
FROM
sgrh.tsgrhareas a
$$;


ALTER FUNCTION sgrt.compromisos_areas_fechas(fecha_inicio text, fecha_fin text) OWNER TO suite;

--
-- TOC entry 502 (class 1255 OID 99185)
-- Name: compromisos_dia(text); Type: FUNCTION; Schema: sgrt; Owner: suite
--

CREATE FUNCTION sgrt.compromisos_dia(fechacompromiso text) RETURNS TABLE(cod_empleado integer, nombre text, area text, fec_compromiso text, des_descripcion text, cod_estatus text, tiempo_demora integer)
    LANGUAGE sql
    AS $$
SELECT
emp.cod_empleado,
CONCAT(emp.des_nombre, ' ', emp.des_nombres, ' ', emp.des_apepaterno, ' ', emp.des_apematerno) as nombre,
(SELECT cod_acronimo FROM sgrh.tsgrhareas WHERE cod_area=emp.cod_area) as area,
cast(to_char(com.fec_compromiso,'DD/MM/YYYY') as text) fec_compromiso,
com.des_descripcion,
cast(com.cod_estatus as text),
cast((select count(the_day) from
    (select generate_series(com.fec_compromiso+1, com.fec_entrega, '1 day') as the_day) days
where extract('dow' from the_day) not in (0,6))as INTEGER) as tiempo_demora
FROM sgrt.tsgrtcompromisos com
LEFT JOIN sgrh.tsgrhempleados emp ON com.cod_ejecutor=emp.cod_empleado or com.cod_validador=emp.cod_empleado or com.cod_verificador=emp.cod_empleado
WHERE com.fec_compromiso=cast(fechaCompromiso as date);
$$;


ALTER FUNCTION sgrt.compromisos_dia(fechacompromiso text) OWNER TO suite;

--
-- TOC entry 503 (class 1255 OID 99186)
-- Name: compromisos_generales(); Type: FUNCTION; Schema: sgrt; Owner: suite
--

CREATE FUNCTION sgrt.compromisos_generales() RETURNS TABLE(cod_empleado integer, cod_reunion integer, nombre text, rol text, area text, descripcion text, minuta text, estatus text, fec_registro text, fec_compromiso text, dias_habiles integer, tiempo_demora integer)
    LANGUAGE sql
    AS $$
SELECT
emp.cod_empleado,
reunion.cod_reunion,
CONCAT(emp.des_nombre, ' ', emp.des_nombres, ' ', emp.des_apepaterno, ' ', emp.des_apematerno) as nombre,
CAST('Validador' as text) as rol,
cast(area.cod_acronimo as text) as area,
cast(com.des_descripcion as text) as descripcion,
cast(reunion.des_nombre as text) as minuta,
cast(com.cod_estatus as text)as estatus,
cast(to_char(com.fec_solicitud,'DD/MM/YYYY') as text) as fec_registro,
cast(to_char(com.fec_compromiso,'DD/MM/YYYY') as text),
cast((select count(the_day) from
    (select generate_series(com.fec_solicitud+1, com.fec_compromiso, '1 day') as the_day) days
where extract('dow' from the_day) not in (0,6))as INTEGER) as dias_habiles,
cast((select count(the_day) from
    (select generate_series(com.fec_compromiso+1, com.fec_entrega, '1 day') as the_day) days
where extract('dow' from the_day) not in (0,6))as INTEGER) as tiempo_demora
FROM sgrt.tsgrtcompromisos com
LEFT JOIN sgrt.tsgrtreuniones reunion ON reunion.cod_reunion=com.cod_reunion
LEFT JOIN sgrh.tsgrhempleados emp ON emp.cod_empleado=com.cod_validador
LEFT JOIN sgrh.tsgrhareas area ON area.cod_area=emp.cod_area
WHERE emp.cod_empleado=com.cod_validador
UNION
SELECT
emp.cod_empleado,
reunion.cod_reunion,
CONCAT(emp.des_nombre, ' ', emp.des_nombres, ' ', emp.des_apepaterno, ' ', emp.des_apematerno) as nombre,
CAST('Verificador' as text) as rol,
cast(area.cod_acronimo as text) as area,
cast(com.des_descripcion as text) as descripcion,
cast(reunion.des_nombre as text) as minuta,
cast(com.cod_estatus as text)as estatus,
cast(to_char(com.fec_solicitud,'DD/MM/YYYY') as text) as fec_registro,
cast(to_char(com.fec_compromiso,'DD/MM/YYYY') as text),
cast((select count(the_day) from
    (select generate_series(com.fec_solicitud+1, com.fec_compromiso, '1 day') as the_day) days
where extract('dow' from the_day) not in (0,6))as INTEGER) as dias_habiles,
cast((select count(the_day) from
    (select generate_series(com.fec_compromiso+1, com.fec_entrega, '1 day') as the_day) days
where extract('dow' from the_day) not in (0,6))as INTEGER) as tiempo_demora
FROM sgrt.tsgrtcompromisos com
LEFT JOIN sgrt.tsgrtreuniones reunion ON reunion.cod_reunion=com.cod_reunion
LEFT JOIN sgrh.tsgrhempleados emp ON emp.cod_empleado=com.cod_verificador
LEFT JOIN sgrh.tsgrhareas area ON area.cod_area=emp.cod_area
WHERE emp.cod_empleado=com.cod_verificador
UNION
SELECT
emp.cod_empleado,
reunion.cod_reunion,
CONCAT(emp.des_nombre, ' ', emp.des_nombres, ' ', emp.des_apepaterno, ' ', emp.des_apematerno) as nombre,
CAST('Ejecutor' as text) as rol,
cast(area.cod_acronimo as text) as area,
cast(com.des_descripcion as text) as descripcion,
cast(reunion.des_nombre as text) as minuta,
cast(com.cod_estatus as text)as estatus,
cast(to_char(com.fec_solicitud,'DD/MM/YYYY') as text) as fec_registro,
cast(to_char(com.fec_compromiso,'DD/MM/YYYY') as text),
cast((select count(the_day) from
    (select generate_series(com.fec_solicitud+1, com.fec_compromiso, '1 day') as the_day) days
where extract('dow' from the_day) not in (0,6))as INTEGER) as dias_habiles,
cast((select count(the_day) from
    (select generate_series(com.fec_compromiso+1, com.fec_entrega, '1 day') as the_day) days
where extract('dow' from the_day) not in (0,6))as INTEGER) as tiempo_demora
FROM sgrt.tsgrtcompromisos com
LEFT JOIN sgrt.tsgrtreuniones reunion ON reunion.cod_reunion=com.cod_reunion
LEFT JOIN sgrh.tsgrhempleados emp ON emp.cod_empleado=com.cod_ejecutor
LEFT JOIN sgrh.tsgrhareas area ON area.cod_area=emp.cod_area
WHERE emp.cod_empleado=com.cod_ejecutor
$$;


ALTER FUNCTION sgrt.compromisos_generales() OWNER TO suite;

--
-- TOC entry 504 (class 1255 OID 99187)
-- Name: reporte_por_tema(integer); Type: FUNCTION; Schema: sgrt; Owner: suite
--

CREATE FUNCTION sgrt.reporte_por_tema(reunionid integer) RETURNS TABLE(nombre_minuta character varying, responsable text, fecha text, acuerdos integer, pendientes integer, total integer, tiempo_invertido text)
    LANGUAGE plpgsql
    AS $$
DECLARE
    var_r record;
BEGIN
   FOR var_r IN(SELECT * FROM sgrt.tsgrtreuniones WHERE cod_reunion=reunionId OR cod_proximareunion=reunionId)
     LOOP
              RETURN QUERY
		select c.nombre_minuta, c.responsable, c.fecha,a.Acuerdos, b.Pendientes, cast(COALESCE(a.Acuerdos,0)+COALESCE(b.Pendientes,0)as int) AS Total, c.tiempo_invertido from
		(select cast(count(cod_tipocompromiso) as int) as Acuerdos from sgrt.tsgrtcompromisos where cod_tipocompromiso='Acuerdo' and cod_reunion=var_r.cod_reunion) a,
		(select cast(count(cod_tipocompromiso)as int) as Pendientes from sgrt.tsgrtcompromisos where cod_tipocompromiso='Pendiente' and cod_reunion=var_r.cod_reunion) b,
		(select reunion.des_nombre AS nombre_minuta, CONCAT(empleado.des_nombre, ' ', empleado.des_nombres, ' ', empleado.des_apepaterno, ' ', empleado.des_apematerno) AS responsable,
		CAST(to_char(reunion.fec_fecha, 'DD/MM/YYYY') as text) as fecha, CAST(to_char(reunion.tim_duracion,'HH24:MI') as text) as tiempo_invertido from
		sgrt.tsgrtreuniones reunion,
		sgrh.tsgrhempleados empleado
		WHERE reunion.cod_responsable=empleado.cod_empleado and
		cod_reunion=var_r.cod_reunion) c;
            END LOOP;
END; $$;


ALTER FUNCTION sgrt.reporte_por_tema(reunionid integer) OWNER TO suite;

--
-- TOC entry 583 (class 1255 OID 102647)
-- Name: buscar_asignacion_recurso(integer); Type: FUNCTION; Schema: sisat; Owner: suite
--

CREATE FUNCTION sisat.buscar_asignacion_recurso(asignacion_cod integer) RETURNS TABLE(cod_asignacion integer, cod_prospecto integer, nombreprospecto text, des_perfil character varying, des_observacion character varying, des_actividades character varying, des_lugarsalida character varying, des_lugarllegada character varying, fec_llegada date, fec_salida date, cod_transporte character varying, des_lugarhopedaje character varying, fec_hospedaje date, des_computadora character varying, cod_telefono character varying, des_accesorios character varying, des_nbresponsable character varying, des_nbpuesto character varying, des_lugarresp character varying, cod_telefonoresp character varying, tim_horario time without time zone, fec_iniciocontra date, fec_terminocontra date, imp_sueldomensual numeric, imp_nominaimss numeric, imp_honorarios numeric, imp_otros numeric, cod_rfc character varying, des_razonsocial character varying, des_correo character varying, cod_cpostal integer, des_direccionfact character varying, des_nbcliente character varying, des_direccioncte character varying, des_nbcontactocte character varying, des_correocte character varying, cod_telefonocte character varying, audcodmodificacion integer, audfechamodificacion date, cod_empleado integer, codrhta text, codgpy text, codape text, codrys text)
    LANGUAGE plpgsql
    AS $$
BEGIN 
RETURN QUERY
SELECT c.cod_asignacion, p.cod_prospecto, p.des_nombre ||' '|| (case when p.des_nombres is NULL then '' else p.des_nombres::character varying(60) end) ||' '|| p.des_appaterno ||' '|| p.des_apmaterno AS NombreProspecto, perf.des_perfil, c.des_observacion, 
    c.des_actividades, c.des_lugarsalida, c.des_lugarllegada, c.fec_llegada,c.fec_salida, c.cod_transporte, c.des_lugarhopedaje, 
    c.fec_hospedaje, c.des_computadora, c.cod_telefono, c.des_accesorios, c.des_nbresponsable, c.des_nbpuesto, c.des_lugarresp, 
    c.cod_telefonoresp, c.tim_horario, c.fec_iniciocontra, c.fec_terminocontra, c.imp_sueldomensual, c.imp_nominaimss, 
    c.imp_honorarios, c.imp_otros,c.cod_rfc, c.des_razonsocial, c.des_correo, c.cod_cpostal, c.des_direccionfact, 
    cte.des_nbcliente, cte.des_direccioncte, cte.des_nbcontactocte, cte.des_correocte, cte.cod_telefonocte,c.aud_cod_modificadopor, c.aud_fecha_modificacion, emp.cod_empleado,
    emp.des_nombre ||' '|| (case when emp.des_nombres is NULL then '' else emp.des_nombres::character varying(60) end) ||' '|| emp.des_apepaterno ||' '|| emp.des_apematerno AS Cod_Rhta, (
    
    SELECT 
    empl.des_nombre ||' '|| (case when empl.des_nombres is NULL then '' else empl.des_nombres::character varying(60) end) ||' '|| empl.des_apepaterno ||' '|| empl.des_apematerno AS CodGpy
        
    FROM sisat.tsisatcartaasignacion ca
    inner join  sisat.tsisatprospectos pr on pr.cod_prospecto= ca.cod_prospecto
    inner join sgrh.tsgrhempleados empl on empl.cod_empleado = ca.cod_gpy 
    WHERE ca.cod_asignacion = c.cod_asignacion
    ), (
    SELECT 
    empl.des_nombre ||' '|| (case when empl.des_nombres is NULL then '' else empl.des_nombres::character varying(60) end) ||' '|| empl.des_apepaterno ||' '|| empl.des_apematerno AS CodApe
        
    FROM sisat.tsisatcartaasignacion ca
    inner join  sisat.tsisatprospectos pr on pr.cod_prospecto= ca.cod_prospecto
    inner join sgrh.tsgrhempleados empl on empl.cod_empleado = ca.cod_ape
    WHERE ca.cod_asignacion = c.cod_asignacion), (
    SELECT 
    empl.des_nombre ||' '|| (case when empl.des_nombres is NULL then '' else empl.des_nombres::character varying(60) end) ||' '|| empl.des_apepaterno ||' '|| empl.des_apematerno AS CodRys
        
    FROM sisat.tsisatcartaasignacion ca
    inner join  sisat.tsisatprospectos pr on pr.cod_prospecto= ca.cod_prospecto
    inner join sgrh.tsgrhempleados empl on empl.cod_empleado = ca.cod_rys 
    WHERE ca.cod_asignacion = c.cod_asignacion
    )
        
FROM sisat.tsisatcartaasignacion c
inner join  sgrh.tsgrhperfiles perf on perf.cod_perfil=c.cod_perfil
inner join sgrh.tsgrhclientes cte on cte.cod_cliente=c.cod_cliente
inner join  sisat.tsisatprospectos p on p.cod_prospecto= c.cod_prospecto
inner join sgrh.tsgrhempleados emp on emp.cod_empleado = c.cod_rhta
WHERE p.cod_prospecto= asignacion_cod
GROUP BY c.cod_asignacion, p.cod_prospecto, NombreProspecto, perf.des_perfil, c.des_observacion, 
    c.des_actividades, c.des_lugarsalida, c.des_lugarllegada, c.fec_llegada,c.fec_salida, c.cod_transporte, c.des_lugarhopedaje, 
    c.fec_hospedaje, c.des_computadora, c.cod_telefono, c.des_accesorios, c.des_nbresponsable, c.des_nbpuesto, c.des_lugarresp, 
    c.cod_telefonoresp, c.tim_horario, c.fec_iniciocontra, c.fec_terminocontra, c.imp_sueldomensual, c.imp_nominaimss, 
    c.imp_honorarios, c.imp_otros,c.cod_rfc, c.des_razonsocial, c.des_correo, c.cod_cpostal, c.des_direccionfact, 
    cte.des_nbcliente, cte.des_direccioncte, cte.des_nbcontactocte, cte.des_correocte, cte.cod_telefonocte,c.aud_cod_modificadopor, c.aud_fecha_modificacion, emp.cod_empleado, CodRhta 
ORDER BY cod_asignacion  asc;
END;
$$;


ALTER FUNCTION sisat.buscar_asignacion_recurso(asignacion_cod integer) OWNER TO suite;

--
-- TOC entry 569 (class 1255 OID 102648)
-- Name: buscar_clientes(integer); Type: FUNCTION; Schema: sisat; Owner: postgres
--

CREATE FUNCTION sisat.buscar_clientes(clienteid integer) RETURNS TABLE(cod_cliente integer, des_razonsocial character varying, cod_rfc character varying, des_direccioncte character varying, cod_cpostal integer, des_correocte character varying, cod_telefonocte character varying, des_nbcliente character varying)
    LANGUAGE plpgsql COST 1
    AS $$
BEGIN
RETURN QUERY
select 
cte.cod_cliente,
cart.des_razonsocial,
cart.cod_rfc,
cte.des_direccioncte,
cart.cod_cpostal,
cte.des_correocte,
cte.cod_telefonocte,
cte.des_nbcliente
from sgrh.tsgrhclientes cte join sisat.tsisatcartaasignacion cart  using(cod_cliente)
WHERE cte.cod_cliente = clienteid
group by cte.cod_cliente,cart.des_razonsocial,cart.cod_rfc,cte.des_direccioncte,cart.cod_cpostal,cte.des_correocte,cte.cod_telefonocte,cte.des_nbcliente  order by cod_cliente asc;
END;
$$;


ALTER FUNCTION sisat.buscar_clientes(clienteid integer) OWNER TO postgres;

--
-- TOC entry 570 (class 1255 OID 102649)
-- Name: buscar_prospectos(integer); Type: FUNCTION; Schema: sisat; Owner: postgres
--

CREATE FUNCTION sisat.buscar_prospectos(prospectoid integer) RETURNS TABLE(cod_prospecto integer, nombre_completo text, des_actividades character varying, imp_sueldomensual numeric, imp_nominaimss numeric, imp_honorarios numeric, imp_otros numeric)
    LANGUAGE plpgsql
    AS $$
BEGIN
RETURN QUERY
select
pros.cod_prospecto,
pros.des_nombre || ' '  ||
(case when pros.des_nombres is null then '' else pros.des_nombres::character varying(60) end) || ' '  ||
pros.des_appaterno || ' ' ||
pros.des_apmaterno as Nombre_completo,
cart.des_actividades,
cart.imp_sueldomensual,
cart.imp_nominaimss,
cart.imp_honorarios,
cart.imp_otros
from sisat.tsisatcartaasignacion cart join sisat.tsisatprospectos pros using(cod_prospecto)
WHERE pros.cod_prospecto = prospectoid
GROUP BY pros.cod_prospecto, Nombre_completo ,cart.des_actividades,cart.imp_sueldomensual,cart.imp_nominaimss,cart.imp_honorarios,cart.imp_otros
ORDER BY cod_prospecto  asc;
END;
$$;


ALTER FUNCTION sisat.buscar_prospectos(prospectoid integer) OWNER TO postgres;

--
-- TOC entry 582 (class 1255 OID 102650)
-- Name: requisicion_de_personal(integer); Type: FUNCTION; Schema: sisat; Owner: suite
--

CREATE FUNCTION sisat.requisicion_de_personal(cliente integer) RETURNS TABLE(codvacante integer, fecsolicitud date, fecentrega date, desrqvacante character varying, desescolaridad character varying, sexo character varying, cnuanexperiencia smallint, txtexperiencia text, txtconocimientostecno text, codnbidioma character varying, codnivelidioma character varying, fecinicio date, fectermino date, desesquema character varying, codsalarioestmin numeric, codsalarioestmax numeric, timjornada character varying, desnbcliente character varying, desdireccioncte character varying, codtelefonocte character varying, descorreocte character varying, nbempleado text, nbsolicita text)
    LANGUAGE plpgsql
    AS $$
BEGIN 
RETURN QUERY
SELECT v.cod_vacante, v.fec_solicitud, v.fec_entrega, v.des_rqvacante, 
		v.des_escolaridad, v.sexo, v.cnu_anexperiencia, v.txt_experiencia, 
		v.txt_conocimientostecno, i.cod_nbidioma, v.cod_nivelidioma, c.fec_inicio,
		c.fec_termino,c.des_esquema, c.cod_salarioestmin, c.cod_salarioestmax, 
		c.tim_jornada, cte.des_nbcliente, cte.des_direccioncte, cte.cod_telefonocte,
		cte.des_correocte, (SELECT 
    empl.des_nombre ||' '|| (case when empl.des_nombres is NULL then '' else empl.des_nombres::character varying(60) end) ||' '|| empl.des_apepaterno ||' '|| empl.des_apematerno AS nbempleado
        
    FROM sisat.tsisatfirmareqper f
    inner join  sisat.tsisatvacantes pr on pr.cod_vacante = f.cod_vacante
    inner join sgrh.tsgrhempleados empl on empl.cod_empleado = f.cod_solicita 
    WHERE f.cod_vacante = v.cod_vacante),(SELECT 
    empl.des_nombre ||' '|| (case when empl.des_nombres is NULL then '' else empl.des_nombres::character varying(60) end) ||' '|| empl.des_apepaterno ||' '|| empl.des_apematerno AS nbautoriza
        
    FROM sisat.tsisatfirmareqper f
    inner join  sisat.tsisatvacantes pr on pr.cod_vacante = f.cod_vacante
    inner join sgrh.tsgrhempleados empl on empl.cod_empleado = f.cod_autoriza
    WHERE f.cod_vacante = v.cod_vacante)
FROM sisat.tsisatvacantes v
INNER JOIN sisat.tsisatidiomas i ON i.cod_idioma = v.cod_idioma
INNER JOIN sisat.tsisatcontrataciones c ON c.cod_contratacion = v.cod_contratacion
INNER JOIN sgrh.tsgrhclientes cte on cte.cod_cliente=v.cod_cliente
WHERE v.cod_cliente = cliente
GROUP BY v.cod_vacante, v.fec_solicitud, v.fec_entrega, v.des_rqvacante, 
		v.des_escolaridad, v.sexo, v.cnu_anexperiencia, i.cod_nbidioma, v.cod_nivelidioma,
		v.txt_experiencia, v.txt_conocimientostecno, v.cod_cliente, c.fec_inicio, c.fec_termino, 
		c.des_esquema, c.cod_salarioestmin, c.cod_salarioestmax, c.tim_jornada,
		cte.des_nbcliente, cte.des_direccioncte, cte.cod_telefonocte,
		cte.des_correocte
ORDER BY fec_solicitud desc;
END;
	$$;


ALTER FUNCTION sisat.requisicion_de_personal(cliente integer) OWNER TO suite;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 218 (class 1259 OID 99192)
-- Name: rtsueldobase; Type: TABLE; Schema: public; Owner: suite
--

CREATE TABLE public.rtsueldobase (
    imp_honorarios numeric(10,2)
);


ALTER TABLE public.rtsueldobase OWNER TO suite;

--
-- TOC entry 219 (class 1259 OID 99195)
-- Name: seq_sistema; Type: SEQUENCE; Schema: sgco; Owner: suite
--

CREATE SEQUENCE sgco.seq_sistema
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgco.seq_sistema OWNER TO suite;

--
-- TOC entry 220 (class 1259 OID 99197)
-- Name: seq_tipousuario; Type: SEQUENCE; Schema: sgco; Owner: suite
--

CREATE SEQUENCE sgco.seq_tipousuario
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgco.seq_tipousuario OWNER TO suite;

--
-- TOC entry 221 (class 1259 OID 99199)
-- Name: seq_usuarios; Type: SEQUENCE; Schema: sgco; Owner: suite
--

CREATE SEQUENCE sgco.seq_usuarios
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgco.seq_usuarios OWNER TO suite;

--
-- TOC entry 222 (class 1259 OID 99201)
-- Name: tsgcosistemas; Type: TABLE; Schema: sgco; Owner: suite
--

CREATE TABLE sgco.tsgcosistemas (
    cod_sistema integer NOT NULL,
    des_nbsistema character varying(50) NOT NULL,
    des_descripcion character varying(300)
);


ALTER TABLE sgco.tsgcosistemas OWNER TO suite;

--
-- TOC entry 223 (class 1259 OID 99204)
-- Name: tsgcotipousuario; Type: TABLE; Schema: sgco; Owner: suite
--

CREATE TABLE sgco.tsgcotipousuario (
    cod_tipousuario integer NOT NULL,
    cod_usuario integer NOT NULL,
    cod_sistema integer NOT NULL,
    cod_rol integer NOT NULL
);


ALTER TABLE sgco.tsgcotipousuario OWNER TO suite;

--
-- TOC entry 224 (class 1259 OID 99207)
-- Name: tsgcousuarios; Type: TABLE; Schema: sgco; Owner: suite
--

CREATE TABLE sgco.tsgcousuarios (
    cod_usuario integer NOT NULL,
    cod_empleado integer NOT NULL,
    des_correo character varying(60) NOT NULL,
    des_clave character varying(30) NOT NULL
);


ALTER TABLE sgco.tsgcousuarios OWNER TO suite;

--
-- TOC entry 367 (class 1259 OID 101990)
-- Name: seq_cabecera; Type: SEQUENCE; Schema: sgnom; Owner: suite
--

CREATE SEQUENCE sgnom.seq_cabecera
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgnom.seq_cabecera OWNER TO suite;

--
-- TOC entry 368 (class 1259 OID 101992)
-- Name: seq_cncptoquinc; Type: SEQUENCE; Schema: sgnom; Owner: suite
--

CREATE SEQUENCE sgnom.seq_cncptoquinc
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgnom.seq_cncptoquinc OWNER TO suite;

--
-- TOC entry 369 (class 1259 OID 101994)
-- Name: seq_confpago; Type: SEQUENCE; Schema: sgnom; Owner: suite
--

CREATE SEQUENCE sgnom.seq_confpago
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgnom.seq_confpago OWNER TO suite;

--
-- TOC entry 370 (class 1259 OID 101996)
-- Name: seq_empquincena; Type: SEQUENCE; Schema: sgnom; Owner: suite
--

CREATE SEQUENCE sgnom.seq_empquincena
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgnom.seq_empquincena OWNER TO suite;

--
-- TOC entry 371 (class 1259 OID 101998)
-- Name: seq_incidencia; Type: SEQUENCE; Schema: sgnom; Owner: suite
--

CREATE SEQUENCE sgnom.seq_incidencia
    START WITH 60
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgnom.seq_incidencia OWNER TO suite;

--
-- TOC entry 372 (class 1259 OID 102000)
-- Name: seq_nom_imss; Type: SEQUENCE; Schema: sgnom; Owner: suite
--

CREATE SEQUENCE sgnom.seq_nom_imss
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgnom.seq_nom_imss OWNER TO suite;

--
-- TOC entry 373 (class 1259 OID 102002)
-- Name: tsgnomaguinaldo; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnomaguinaldo (
    cod_aguinaldoid integer NOT NULL,
    imp_aguinaldo numeric(10,2) NOT NULL,
    cod_tipoaguinaldo character(1) NOT NULL,
    cod_empquincenaid_fk integer NOT NULL,
    bol_estatus boolean NOT NULL,
    xml_desgloce xml
);


ALTER TABLE sgnom.tsgnomaguinaldo OWNER TO suite;

--
-- TOC entry 4948 (class 0 OID 0)
-- Dependencies: 373
-- Name: TABLE tsgnomaguinaldo; Type: COMMENT; Schema: sgnom; Owner: suite
--

COMMENT ON TABLE sgnom.tsgnomaguinaldo IS 'cod_tipoaguinaldo

(

i = imss,

h = honorarios

)';


--
-- TOC entry 374 (class 1259 OID 102008)
-- Name: tsgnomargumento; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnomargumento (
    cod_argumentoid integer NOT NULL,
    cod_nbargumento character varying(30) NOT NULL,
    cod_clavearg character varying(5) NOT NULL,
    imp_valorconst numeric(10,2),
    des_funcionbd character varying(60),
    cod_tipoargumento character(1),
    bol_estatus boolean NOT NULL,
    txt_descripcion text NOT NULL,
    aud_codcreadopor integer NOT NULL,
    aud_codmodificadopor integer,
    aud_feccreacion date NOT NULL,
    aud_fecmodificacion date
);


ALTER TABLE sgnom.tsgnomargumento OWNER TO suite;

--
-- TOC entry 375 (class 1259 OID 102014)
-- Name: tsgnombitacora; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnombitacora (
    cod_bitacoraid integer NOT NULL,
    xml_bitacora xml NOT NULL,
    cod_tablaid_fk integer NOT NULL
);


ALTER TABLE sgnom.tsgnombitacora OWNER TO suite;

--
-- TOC entry 376 (class 1259 OID 102020)
-- Name: tsgnomcabecera; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnomcabecera (
    cod_cabeceraid integer DEFAULT nextval('sgnom.seq_cabecera'::regclass) NOT NULL,
    cod_nbnomina character varying(40) NOT NULL,
    fec_creacion date NOT NULL,
    fec_ejecucion date,
    fec_cierre date,
    imp_totpercepcion numeric(10,2) NOT NULL,
    imp_totdeduccion numeric(10,2) NOT NULL,
    imp_totalemp numeric(10,2) NOT NULL,
    cod_quincenaid_fk integer NOT NULL,
    cod_tiponominaid_fk integer NOT NULL,
    cod_estatusnomid_fk integer NOT NULL,
    cnu_totalemp integer,
    aud_codcreadopor integer NOT NULL,
    aud_codmodificadopor integer,
    aud_feccreacion date NOT NULL,
    aud_fecmodificacion date
);


ALTER TABLE sgnom.tsgnomcabecera OWNER TO suite;

--
-- TOC entry 377 (class 1259 OID 102024)
-- Name: tsgnomcabeceraht; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnomcabeceraht (
    cod_cabeceraid integer NOT NULL,
    cod_nbnomina character varying(40) NOT NULL,
    fec_creacion date NOT NULL,
    fec_ejecucion date,
    fec_cierre date,
    imp_totpercepcion numeric(10,2),
    imp_totdeduccion numeric(10,2),
    imp_totalemp numeric(10,2),
    cod_quincenaid_fk integer NOT NULL,
    cod_tiponominaid_fk integer NOT NULL,
    cod_estatusnomid_fk integer NOT NULL,
    cnu_totalemp integer,
    aud_codcreadopor integer NOT NULL,
    aud_codmodificadopor integer,
    aud_feccreacion date NOT NULL,
    aud_fecmodificacion date
);


ALTER TABLE sgnom.tsgnomcabeceraht OWNER TO suite;

--
-- TOC entry 378 (class 1259 OID 102027)
-- Name: tsgnomcalculo; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnomcalculo (
    cod_calculoid integer NOT NULL,
    cod_tpcalculo character varying(25) NOT NULL,
    bol_estatus boolean NOT NULL
);


ALTER TABLE sgnom.tsgnomcalculo OWNER TO suite;

--
-- TOC entry 4954 (class 0 OID 0)
-- Dependencies: 378
-- Name: TABLE tsgnomcalculo; Type: COMMENT; Schema: sgnom; Owner: suite
--

COMMENT ON TABLE sgnom.tsgnomcalculo IS 'bol_estatus

(

activo, inactivo

)';


--
-- TOC entry 379 (class 1259 OID 102030)
-- Name: tsgnomcatincidencia; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnomcatincidencia (
    cod_catincidenciaid integer NOT NULL,
    cod_claveincidencia character varying(5) NOT NULL,
    cod_nbincidencia character varying(20),
    cod_perfilincidencia character varying(25),
    bol_estatus boolean,
    cod_tipoincidencia character(1),
    imp_monto numeric(10,2)
);


ALTER TABLE sgnom.tsgnomcatincidencia OWNER TO suite;

--
-- TOC entry 4956 (class 0 OID 0)
-- Dependencies: 379
-- Name: TABLE tsgnomcatincidencia; Type: COMMENT; Schema: sgnom; Owner: suite
--

COMMENT ON TABLE sgnom.tsgnomcatincidencia IS 'cod_tipoincidencia

(

1 = horas

2 = dias

3 = actividad

)';


--
-- TOC entry 380 (class 1259 OID 102033)
-- Name: tsgnomclasificador; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnomclasificador (
    cod_clasificadorid integer NOT NULL,
    cod_tpclasificador character varying(20),
    bol_estatus boolean
);


ALTER TABLE sgnom.tsgnomclasificador OWNER TO suite;

--
-- TOC entry 381 (class 1259 OID 102036)
-- Name: tsgnomcncptoquinc; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnomcncptoquinc (
    cod_cncptoquincid integer DEFAULT nextval('sgnom.seq_cncptoquinc'::regclass) NOT NULL,
    cod_empquincenaid_fk integer NOT NULL,
    cod_conceptoid_fk integer NOT NULL,
    imp_concepto numeric(10,2) NOT NULL,
    imp_gravado numeric(10,2),
    imp_exento numeric(10,2),
    xml_desgloce xml
);


ALTER TABLE sgnom.tsgnomcncptoquinc OWNER TO suite;

--
-- TOC entry 382 (class 1259 OID 102043)
-- Name: tsgnomcncptoquincht; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnomcncptoquincht (
    cod_cncptoquinchtid integer NOT NULL,
    cod_empquincenaid_fk integer NOT NULL,
    cod_conceptoid_fk integer NOT NULL,
    imp_concepto numeric(10,2) NOT NULL,
    imp_gravado numeric(10,2),
    imp_exento numeric(10,2),
    xml_desgloce xml
);


ALTER TABLE sgnom.tsgnomcncptoquincht OWNER TO suite;

--
-- TOC entry 383 (class 1259 OID 102049)
-- Name: tsgnomconcepto; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnomconcepto (
    cod_conceptoid integer NOT NULL,
    cod_nbconcepto character varying(20) NOT NULL,
    cod_claveconcepto character varying(4) NOT NULL,
    cnu_prioricalculo integer NOT NULL,
    cnu_articulo integer NOT NULL,
    bol_estatus boolean NOT NULL,
    cod_formulaid_fk integer,
    cod_tipoconceptoid_fk integer NOT NULL,
    cod_calculoid_fk integer NOT NULL,
    cod_conceptosatid_fk integer NOT NULL,
    cod_frecuenciapago character varying(20) NOT NULL,
    cod_partidaprep integer NOT NULL,
    cnu_cuentacontable character varying(18) NOT NULL,
    cod_gravado character(1),
    cod_excento character(1),
    bol_aplicaisn boolean,
    bol_retroactividad boolean NOT NULL,
    cnu_topeex integer,
    cod_clasificadorid_fk integer NOT NULL,
    cod_tiponominaid_fk integer,
    aud_codcreadopor integer NOT NULL,
    aud_codmodificadopor integer,
    aud_feccreacion date NOT NULL,
    aud_fecmodificacion date,
    imp_monto double precision
);


ALTER TABLE sgnom.tsgnomconcepto OWNER TO suite;

--
-- TOC entry 4961 (class 0 OID 0)
-- Dependencies: 383
-- Name: TABLE tsgnomconcepto; Type: COMMENT; Schema: sgnom; Owner: suite
--

COMMENT ON TABLE sgnom.tsgnomconcepto IS 'bol_estatus

(

activo, inactivo

)



gravado

(

1 = dias, 

2 = porcentaje, 

3 = no aplica

)



excento

(

activo, inactivo

)





validar tope_ex';


--
-- TOC entry 384 (class 1259 OID 102052)
-- Name: tsgnomconceptosat; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnomconceptosat (
    cod_conceptosatid integer NOT NULL,
    des_conceptosat character varying(51) NOT NULL,
    des_descconcepto character varying(51) NOT NULL,
    bol_estatus boolean NOT NULL
);


ALTER TABLE sgnom.tsgnomconceptosat OWNER TO suite;

--
-- TOC entry 4963 (class 0 OID 0)
-- Dependencies: 384
-- Name: TABLE tsgnomconceptosat; Type: COMMENT; Schema: sgnom; Owner: suite
--

COMMENT ON TABLE sgnom.tsgnomconceptosat IS 'bol_estatus

(

activo, inactivo

)';


--
-- TOC entry 385 (class 1259 OID 102055)
-- Name: tsgnomconfpago; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnomconfpago (
    cod_confpagoid integer DEFAULT nextval('sgnom.seq_confpago'::regclass) NOT NULL,
    bol_pagoempleado boolean,
    bol_pagorh boolean,
    bol_pagofinanzas boolean,
    cod_empquincenaid_fk integer
);


ALTER TABLE sgnom.tsgnomconfpago OWNER TO suite;

--
-- TOC entry 4965 (class 0 OID 0)
-- Dependencies: 385
-- Name: TABLE tsgnomconfpago; Type: COMMENT; Schema: sgnom; Owner: suite
--

COMMENT ON TABLE sgnom.tsgnomconfpago IS 'bol_pagoempleado

(

confirmado, pendiente 

)



pago_rh

(

autorizado, pendiente

)



pago_fnzas

(

autorizado, pendiente

)';


--
-- TOC entry 386 (class 1259 OID 102059)
-- Name: tsgnomejercicio; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnomejercicio (
    cod_ejercicioid integer NOT NULL,
    cnu_valorejercicio integer NOT NULL,
    bol_estatus boolean NOT NULL
);


ALTER TABLE sgnom.tsgnomejercicio OWNER TO suite;

--
-- TOC entry 387 (class 1259 OID 102062)
-- Name: tsgnomempleados; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnomempleados (
    cod_empleadoid integer NOT NULL,
    fec_ingreso date NOT NULL,
    fec_salida date,
    bol_estatus boolean NOT NULL,
    cod_empleado_fk integer NOT NULL,
    imp_sueldoimss numeric(10,2),
    imp_honorarios numeric(10,2),
    imp_finiquito numeric(10,2),
    cod_tipoimss character(1),
    cod_tipohonorarios character(1),
    cod_banco character varying(50),
    cod_sucursal integer,
    cod_cuenta character varying(20),
    txt_descripcionbaja text,
    aud_codcreadopor integer NOT NULL,
    aud_feccreacion date NOT NULL,
    aud_codmodificadopor integer,
    aud_fecmodificacion date,
    cod_clabe character varying(18),
    des_validacion boolean,
    cod_validaciones "char",
    cod_bancoh character varying,
    cod_sucursalh integer,
    cod_cuentah character varying,
    cod_clabeh character varying,
    xml_historial xml,
    aud_fecvalidacion date
);


ALTER TABLE sgnom.tsgnomempleados OWNER TO suite;

--
-- TOC entry 4968 (class 0 OID 0)
-- Dependencies: 387
-- Name: TABLE tsgnomempleados; Type: COMMENT; Schema: sgnom; Owner: suite
--

COMMENT ON TABLE sgnom.tsgnomempleados IS 'cod_empleado_fk hace referencia al schema sgrh en la tabla tsgrhempleados';


--
-- TOC entry 388 (class 1259 OID 102068)
-- Name: tsgnomempquincena; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnomempquincena (
    cod_empquincenaid integer DEFAULT nextval('sgnom.seq_empquincena'::regclass) NOT NULL,
    cod_empleadoid_fk integer NOT NULL,
    cod_cabeceraid_fk integer NOT NULL,
    imp_totpercepcion numeric(10,2) NOT NULL,
    imp_totdeduccion numeric(10,2) NOT NULL,
    imp_totalemp numeric(10,2) NOT NULL,
    bol_estatusemp boolean NOT NULL
);


ALTER TABLE sgnom.tsgnomempquincena OWNER TO suite;

--
-- TOC entry 4970 (class 0 OID 0)
-- Dependencies: 388
-- Name: TABLE tsgnomempquincena; Type: COMMENT; Schema: sgnom; Owner: suite
--

COMMENT ON TABLE sgnom.tsgnomempquincena IS 'bol_estatusemp

(

activo, inactivo

)';


--
-- TOC entry 389 (class 1259 OID 102072)
-- Name: tsgnomempquincenaht; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnomempquincenaht (
    cod_empquincenahtid integer NOT NULL,
    cod_empleadoid_fk integer NOT NULL,
    cod_cabeceraid_fk integer NOT NULL,
    imp_totpercepcion numeric(10,2) NOT NULL,
    imp_totdeduccion numeric(10,2) NOT NULL,
    imp_totalemp numeric(10,2) NOT NULL,
    bol_estatusemp boolean NOT NULL
);


ALTER TABLE sgnom.tsgnomempquincenaht OWNER TO suite;

--
-- TOC entry 390 (class 1259 OID 102075)
-- Name: tsgnomestatusnom; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnomestatusnom (
    cod_estatusnomid integer NOT NULL,
    cod_estatusnomina character varying(15) NOT NULL,
    bol_estatus boolean NOT NULL
);


ALTER TABLE sgnom.tsgnomestatusnom OWNER TO suite;

--
-- TOC entry 4973 (class 0 OID 0)
-- Dependencies: 390
-- Name: TABLE tsgnomestatusnom; Type: COMMENT; Schema: sgnom; Owner: suite
--

COMMENT ON TABLE sgnom.tsgnomestatusnom IS 'cod_estatusnomid 

estatus

(

abierta,

calculada,

revision,

validada,

cerrada

)



nota: en caso de no ser validada entra en revision y posteriormente calcular nuevamente 

hasta ser validada';


--
-- TOC entry 391 (class 1259 OID 102078)
-- Name: tsgnomformula; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnomformula (
    cod_formulaid integer NOT NULL,
    des_nbformula character varying(60) NOT NULL,
    des_formula character varying(250) NOT NULL,
    bol_estatus boolean NOT NULL
);


ALTER TABLE sgnom.tsgnomformula OWNER TO suite;

--
-- TOC entry 4975 (class 0 OID 0)
-- Dependencies: 391
-- Name: TABLE tsgnomformula; Type: COMMENT; Schema: sgnom; Owner: suite
--

COMMENT ON TABLE sgnom.tsgnomformula IS 'bol_estatus

(

activo, inactivo

)';


--
-- TOC entry 392 (class 1259 OID 102081)
-- Name: tsgnomfuncion; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnomfuncion (
    cod_funcionid integer NOT NULL,
    cod_nbfuncion character varying(50) NOT NULL,
    bol_estatus boolean NOT NULL
);


ALTER TABLE sgnom.tsgnomfuncion OWNER TO suite;

--
-- TOC entry 393 (class 1259 OID 102084)
-- Name: tsgnomhisttabla; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnomhisttabla (
    cod_tablaid integer NOT NULL,
    cod_nbtabla character varying(18) NOT NULL,
    bol_estatus boolean NOT NULL
);


ALTER TABLE sgnom.tsgnomhisttabla OWNER TO suite;

--
-- TOC entry 394 (class 1259 OID 102087)
-- Name: tsgnomincidencia; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnomincidencia (
    cod_incidenciaid integer NOT NULL,
    cod_catincidenciaid_fk integer NOT NULL,
    cnu_cantidad smallint,
    des_actividad character varying(100),
    txt_comentarios text,
    cod_empreporta_fk integer,
    cod_empautoriza_fk integer,
    imp_monto numeric(10,2),
    xml_detcantidad xml,
    bol_estatus boolean,
    cod_quincenaid_fk integer NOT NULL,
    bol_validacion boolean,
    fec_validacion date,
    aud_codcreadopor integer NOT NULL,
    aud_feccreacion date NOT NULL,
    aud_codmodificadopor integer,
    aud_fecmodificacion date,
    bol_aceptacion boolean,
    bol_pago boolean
);


ALTER TABLE sgnom.tsgnomincidencia OWNER TO suite;

--
-- TOC entry 4979 (class 0 OID 0)
-- Dependencies: 394
-- Name: TABLE tsgnomincidencia; Type: COMMENT; Schema: sgnom; Owner: suite
--

COMMENT ON TABLE sgnom.tsgnomincidencia IS 'bol_estatus: (activo, inactivo) (1, 0)

validacion: (validar, denegar) (1, 0) 



';


--
-- TOC entry 4980 (class 0 OID 0)
-- Dependencies: 394
-- Name: COLUMN tsgnomincidencia.bol_validacion; Type: COMMENT; Schema: sgnom; Owner: suite
--

COMMENT ON COLUMN sgnom.tsgnomincidencia.bol_validacion IS 'Validacion de la incidencia por parte de RH (pasa a finanzas)';


--
-- TOC entry 4981 (class 0 OID 0)
-- Dependencies: 394
-- Name: COLUMN tsgnomincidencia.bol_aceptacion; Type: COMMENT; Schema: sgnom; Owner: suite
--

COMMENT ON COLUMN sgnom.tsgnomincidencia.bol_aceptacion IS 'Validacion de la incidencia por parte del lider de celula (pasa a RH)';


--
-- TOC entry 395 (class 1259 OID 102093)
-- Name: tsgnommanterceros; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnommanterceros (
    cod_mantercerosid integer NOT NULL,
    cod_conceptoid_fk integer,
    imp_monto numeric(10,2),
    cod_quincenainicio_fk integer,
    cod_quincenafin_fk integer,
    cod_empleadoid_fk integer,
    cod_frecuenciapago character varying(20),
    bol_estatus boolean,
    aud_codcreadopor integer NOT NULL,
    aud_codmodificadopor integer,
    aud_fecreacion date NOT NULL,
    aud_fecmodificacion date
);


ALTER TABLE sgnom.tsgnommanterceros OWNER TO suite;

--
-- TOC entry 399 (class 1259 OID 102447)
-- Name: tsgnomnominaimss; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnomnominaimss (
    cod_nominaimssid integer NOT NULL,
    cod_cabeceraid_fk integer NOT NULL,
    clave integer,
    nss text,
    rfc text,
    curp text,
    fecha_alta date,
    tipo_salario text,
    salario_diario numeric,
    sdi numeric,
    dias_trabajados numeric,
    faltas numeric,
    sueldo numeric,
    premio_asistencia numeric,
    premio_puntualidad numeric,
    total_percepciones numeric,
    total_gravable numeric,
    total_imss numeric,
    total_isr numeric,
    isr numeric,
    imss numeric,
    credito_infonavit numeric,
    subsidio_empleo numeric,
    subsidio_empleo_aplicado numeric,
    total_deducciones numeric,
    neto_pagado numeric,
    vacaciones numeric,
    prima_vacacional numeric,
    prestamo_personal numeric,
    aguinaldo numeric
);


ALTER TABLE sgnom.tsgnomnominaimss OWNER TO suite;

--
-- TOC entry 396 (class 1259 OID 102102)
-- Name: tsgnomquincena; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnomquincena (
    cod_quincenaid integer NOT NULL,
    des_quincena character varying(70) NOT NULL,
    fec_inicio date NOT NULL,
    fec_fin date NOT NULL,
    fec_pago date NOT NULL,
    fec_dispersion date NOT NULL,
    cnu_numquincena integer NOT NULL,
    cod_ejercicioid_fk integer NOT NULL,
    bol_estatus boolean NOT NULL
);


ALTER TABLE sgnom.tsgnomquincena OWNER TO suite;

--
-- TOC entry 397 (class 1259 OID 102105)
-- Name: tsgnomtipoconcepto; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnomtipoconcepto (
    cod_tipoconceptoid integer NOT NULL,
    cod_tipoconcepto character varying(25) NOT NULL,
    bol_estatus boolean NOT NULL
);


ALTER TABLE sgnom.tsgnomtipoconcepto OWNER TO suite;

--
-- TOC entry 4985 (class 0 OID 0)
-- Dependencies: 397
-- Name: TABLE tsgnomtipoconcepto; Type: COMMENT; Schema: sgnom; Owner: suite
--

COMMENT ON TABLE sgnom.tsgnomtipoconcepto IS 'bol_estatus

(

activo, inactivo

)';


--
-- TOC entry 398 (class 1259 OID 102108)
-- Name: tsgnomtiponomina; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnomtiponomina (
    cod_tiponominaid integer NOT NULL,
    cod_nomina character varying(30) NOT NULL,
    bol_estatus boolean NOT NULL
);


ALTER TABLE sgnom.tsgnomtiponomina OWNER TO suite;

--
-- TOC entry 4987 (class 0 OID 0)
-- Dependencies: 398
-- Name: TABLE tsgnomtiponomina; Type: COMMENT; Schema: sgnom; Owner: suite
--

COMMENT ON TABLE sgnom.tsgnomtiponomina IS 'bol_estatus (activa, inactiva)';


--
-- TOC entry 225 (class 1259 OID 99331)
-- Name: seq_area; Type: SEQUENCE; Schema: sgrh; Owner: suite
--

CREATE SEQUENCE sgrh.seq_area
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_area OWNER TO suite;

--
-- TOC entry 226 (class 1259 OID 99333)
-- Name: seq_asignacion_encuesta; Type: SEQUENCE; Schema: sgrh; Owner: suite
--

CREATE SEQUENCE sgrh.seq_asignacion_encuesta
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_asignacion_encuesta OWNER TO suite;

--
-- TOC entry 227 (class 1259 OID 99335)
-- Name: seq_asignacionesemp; Type: SEQUENCE; Schema: sgrh; Owner: suite
--

CREATE SEQUENCE sgrh.seq_asignacionesemp
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_asignacionesemp OWNER TO suite;

--
-- TOC entry 228 (class 1259 OID 99337)
-- Name: seq_capacitaciones; Type: SEQUENCE; Schema: sgrh; Owner: suite
--

CREATE SEQUENCE sgrh.seq_capacitaciones
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_capacitaciones OWNER TO suite;

--
-- TOC entry 229 (class 1259 OID 99339)
-- Name: seq_cartaasignacion; Type: SEQUENCE; Schema: sgrh; Owner: suite
--

CREATE SEQUENCE sgrh.seq_cartaasignacion
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_cartaasignacion OWNER TO suite;

--
-- TOC entry 230 (class 1259 OID 99341)
-- Name: seq_cat_encuesta_participantes; Type: SEQUENCE; Schema: sgrh; Owner: suite
--

CREATE SEQUENCE sgrh.seq_cat_encuesta_participantes
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_cat_encuesta_participantes OWNER TO suite;

--
-- TOC entry 231 (class 1259 OID 99343)
-- Name: seq_catrespuestas; Type: SEQUENCE; Schema: sgrh; Owner: suite
--

CREATE SEQUENCE sgrh.seq_catrespuestas
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_catrespuestas OWNER TO suite;

--
-- TOC entry 232 (class 1259 OID 99345)
-- Name: seq_clientes; Type: SEQUENCE; Schema: sgrh; Owner: suite
--

CREATE SEQUENCE sgrh.seq_clientes
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_clientes OWNER TO suite;

--
-- TOC entry 233 (class 1259 OID 99347)
-- Name: seq_contrataciones; Type: SEQUENCE; Schema: sgrh; Owner: suite
--

CREATE SEQUENCE sgrh.seq_contrataciones
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_contrataciones OWNER TO suite;

--
-- TOC entry 234 (class 1259 OID 99349)
-- Name: seq_contratos; Type: SEQUENCE; Schema: sgrh; Owner: suite
--

CREATE SEQUENCE sgrh.seq_contratos
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_contratos OWNER TO suite;

--
-- TOC entry 235 (class 1259 OID 99351)
-- Name: seq_empleado; Type: SEQUENCE; Schema: sgrh; Owner: suite
--

CREATE SEQUENCE sgrh.seq_empleado
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_empleado OWNER TO suite;

--
-- TOC entry 236 (class 1259 OID 99353)
-- Name: seq_encuestas; Type: SEQUENCE; Schema: sgrh; Owner: suite
--

CREATE SEQUENCE sgrh.seq_encuestas
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_encuestas OWNER TO suite;

--
-- TOC entry 237 (class 1259 OID 99355)
-- Name: seq_escolaridad; Type: SEQUENCE; Schema: sgrh; Owner: suite
--

CREATE SEQUENCE sgrh.seq_escolaridad
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_escolaridad OWNER TO suite;

--
-- TOC entry 238 (class 1259 OID 99357)
-- Name: seq_estatus; Type: SEQUENCE; Schema: sgrh; Owner: suite
--

CREATE SEQUENCE sgrh.seq_estatus
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_estatus OWNER TO suite;

--
-- TOC entry 239 (class 1259 OID 99359)
-- Name: seq_evacapacitacion; Type: SEQUENCE; Schema: sgrh; Owner: suite
--

CREATE SEQUENCE sgrh.seq_evacapacitacion
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_evacapacitacion OWNER TO suite;

--
-- TOC entry 240 (class 1259 OID 99361)
-- Name: seq_evacontestadas; Type: SEQUENCE; Schema: sgrh; Owner: suite
--

CREATE SEQUENCE sgrh.seq_evacontestadas
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_evacontestadas OWNER TO suite;

--
-- TOC entry 241 (class 1259 OID 99363)
-- Name: seq_evaluaciones; Type: SEQUENCE; Schema: sgrh; Owner: suite
--

CREATE SEQUENCE sgrh.seq_evaluaciones
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_evaluaciones OWNER TO suite;

--
-- TOC entry 242 (class 1259 OID 99365)
-- Name: seq_experiencialab; Type: SEQUENCE; Schema: sgrh; Owner: suite
--

CREATE SEQUENCE sgrh.seq_experiencialab
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_experiencialab OWNER TO suite;

--
-- TOC entry 243 (class 1259 OID 99367)
-- Name: seq_factoreseva; Type: SEQUENCE; Schema: sgrh; Owner: suite
--

CREATE SEQUENCE sgrh.seq_factoreseva
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_factoreseva OWNER TO suite;

--
-- TOC entry 244 (class 1259 OID 99369)
-- Name: seq_idiomas; Type: SEQUENCE; Schema: sgrh; Owner: suite
--

CREATE SEQUENCE sgrh.seq_idiomas
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_idiomas OWNER TO suite;

--
-- TOC entry 245 (class 1259 OID 99371)
-- Name: seq_logistica; Type: SEQUENCE; Schema: sgrh; Owner: suite
--

CREATE SEQUENCE sgrh.seq_logistica
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_logistica OWNER TO suite;

--
-- TOC entry 246 (class 1259 OID 99373)
-- Name: seq_lugar; Type: SEQUENCE; Schema: sgrh; Owner: suite
--

CREATE SEQUENCE sgrh.seq_lugar
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_lugar OWNER TO suite;

--
-- TOC entry 247 (class 1259 OID 99375)
-- Name: seq_modo; Type: SEQUENCE; Schema: sgrh; Owner: suite
--

CREATE SEQUENCE sgrh.seq_modo
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_modo OWNER TO suite;

--
-- TOC entry 248 (class 1259 OID 99377)
-- Name: seq_perfiles; Type: SEQUENCE; Schema: sgrh; Owner: suite
--

CREATE SEQUENCE sgrh.seq_perfiles
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_perfiles OWNER TO suite;

--
-- TOC entry 249 (class 1259 OID 99379)
-- Name: seq_plancapacitacion; Type: SEQUENCE; Schema: sgrh; Owner: suite
--

CREATE SEQUENCE sgrh.seq_plancapacitacion
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_plancapacitacion OWNER TO suite;

--
-- TOC entry 250 (class 1259 OID 99381)
-- Name: seq_planesoperativos; Type: SEQUENCE; Schema: sgrh; Owner: suite
--

CREATE SEQUENCE sgrh.seq_planesoperativos
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_planesoperativos OWNER TO suite;

--
-- TOC entry 251 (class 1259 OID 99383)
-- Name: seq_preguntasenc; Type: SEQUENCE; Schema: sgrh; Owner: suite
--

CREATE SEQUENCE sgrh.seq_preguntasenc
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_preguntasenc OWNER TO suite;

--
-- TOC entry 252 (class 1259 OID 99385)
-- Name: seq_preguntaseva; Type: SEQUENCE; Schema: sgrh; Owner: suite
--

CREATE SEQUENCE sgrh.seq_preguntaseva
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_preguntaseva OWNER TO suite;

--
-- TOC entry 253 (class 1259 OID 99387)
-- Name: seq_proceso; Type: SEQUENCE; Schema: sgrh; Owner: suite
--

CREATE SEQUENCE sgrh.seq_proceso
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_proceso OWNER TO suite;

--
-- TOC entry 254 (class 1259 OID 99389)
-- Name: seq_proveedor; Type: SEQUENCE; Schema: sgrh; Owner: suite
--

CREATE SEQUENCE sgrh.seq_proveedor
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_proveedor OWNER TO suite;

--
-- TOC entry 255 (class 1259 OID 99391)
-- Name: seq_puestos; Type: SEQUENCE; Schema: sgrh; Owner: suite
--

CREATE SEQUENCE sgrh.seq_puestos
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_puestos OWNER TO suite;

--
-- TOC entry 256 (class 1259 OID 99393)
-- Name: seq_respuestasenc; Type: SEQUENCE; Schema: sgrh; Owner: suite
--

CREATE SEQUENCE sgrh.seq_respuestasenc
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_respuestasenc OWNER TO suite;

--
-- TOC entry 257 (class 1259 OID 99395)
-- Name: seq_respuestaseva; Type: SEQUENCE; Schema: sgrh; Owner: suite
--

CREATE SEQUENCE sgrh.seq_respuestaseva
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_respuestaseva OWNER TO suite;

--
-- TOC entry 258 (class 1259 OID 99397)
-- Name: seq_revplanesoperativos; Type: SEQUENCE; Schema: sgrh; Owner: suite
--

CREATE SEQUENCE sgrh.seq_revplanesoperativos
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_revplanesoperativos OWNER TO suite;

--
-- TOC entry 259 (class 1259 OID 99399)
-- Name: seq_rolempleado; Type: SEQUENCE; Schema: sgrh; Owner: suite
--

CREATE SEQUENCE sgrh.seq_rolempleado
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_rolempleado OWNER TO suite;

--
-- TOC entry 260 (class 1259 OID 99401)
-- Name: seq_roles; Type: SEQUENCE; Schema: sgrh; Owner: suite
--

CREATE SEQUENCE sgrh.seq_roles
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_roles OWNER TO suite;

--
-- TOC entry 261 (class 1259 OID 99403)
-- Name: seq_subfactoreseva; Type: SEQUENCE; Schema: sgrh; Owner: suite
--

CREATE SEQUENCE sgrh.seq_subfactoreseva
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_subfactoreseva OWNER TO suite;

--
-- TOC entry 262 (class 1259 OID 99405)
-- Name: seq_tipocapacitacion; Type: SEQUENCE; Schema: sgrh; Owner: suite
--

CREATE SEQUENCE sgrh.seq_tipocapacitacion
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_tipocapacitacion OWNER TO suite;

--
-- TOC entry 263 (class 1259 OID 99407)
-- Name: seq_tiposcapacitaciones; Type: SEQUENCE; Schema: sgrh; Owner: suite
--

CREATE SEQUENCE sgrh.seq_tiposcapacitaciones
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_tiposcapacitaciones OWNER TO suite;

--
-- TOC entry 264 (class 1259 OID 99409)
-- Name: seq_validaevaluaciones; Type: SEQUENCE; Schema: sgrh; Owner: suite
--

CREATE SEQUENCE sgrh.seq_validaevaluaciones
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_validaevaluaciones OWNER TO suite;

--
-- TOC entry 265 (class 1259 OID 99411)
-- Name: tsgrhareas; Type: TABLE; Schema: sgrh; Owner: suite
--

CREATE TABLE sgrh.tsgrhareas (
    cod_area integer DEFAULT nextval('sgrh.seq_area'::regclass) NOT NULL,
    des_nbarea character varying(50) NOT NULL,
    cod_acronimo character varying(5) NOT NULL,
    cnu_activo boolean NOT NULL,
    cod_sistemasuite integer,
    cod_creadopor integer,
    cod_modificadopor integer,
    fec_creacion date,
    fec_modificado date
);


ALTER TABLE sgrh.tsgrhareas OWNER TO suite;

--
-- TOC entry 266 (class 1259 OID 99415)
-- Name: tsgrhasignacion_encuesta; Type: TABLE; Schema: sgrh; Owner: suite
--

CREATE TABLE sgrh.tsgrhasignacion_encuesta (
    cod_asignacion integer DEFAULT nextval('sgrh.seq_asignacion_encuesta'::regclass) NOT NULL,
    cod_empleado integer,
    cod_encuesta integer,
    cod_encuesta_realizada boolean
);


ALTER TABLE sgrh.tsgrhasignacion_encuesta OWNER TO suite;

--
-- TOC entry 267 (class 1259 OID 99419)
-- Name: tsgrhasignacionesemp; Type: TABLE; Schema: sgrh; Owner: suite
--

CREATE TABLE sgrh.tsgrhasignacionesemp (
    cod_asignacion integer DEFAULT nextval('sgrh.seq_asignacionesemp'::regclass) NOT NULL,
    cod_empleado integer NOT NULL,
    cod_puesto integer NOT NULL,
    cod_asignadopor integer NOT NULL,
    cod_modificadopor integer,
    status character varying(10) NOT NULL,
    fec_creacion date NOT NULL,
    fec_modificacion date
);


ALTER TABLE sgrh.tsgrhasignacionesemp OWNER TO suite;

--
-- TOC entry 268 (class 1259 OID 99423)
-- Name: tsgrhcapacitaciones; Type: TABLE; Schema: sgrh; Owner: suite
--

CREATE TABLE sgrh.tsgrhcapacitaciones (
    cod_capacitacion integer DEFAULT nextval('sgrh.seq_capacitaciones'::regclass) NOT NULL,
    cod_empleado integer NOT NULL,
    cod_tipocurso character varying(40) NOT NULL,
    des_nbcurso character varying(50) NOT NULL,
    des_organismo character varying(50) NOT NULL,
    fec_termino date NOT NULL,
    des_duracion character varying(40) NOT NULL,
    bin_documento bytea
);


ALTER TABLE sgrh.tsgrhcapacitaciones OWNER TO suite;

--
-- TOC entry 269 (class 1259 OID 99430)
-- Name: tsgrhcartaasignacion; Type: TABLE; Schema: sgrh; Owner: suite
--

CREATE TABLE sgrh.tsgrhcartaasignacion (
    cod_asignacion integer DEFAULT nextval('sgrh.seq_cartaasignacion'::regclass) NOT NULL,
    cod_empleado integer NOT NULL,
    cod_perfil integer NOT NULL,
    des_actividades character varying(200),
    des_lugarsalida character varying(100),
    des_lugarllegada character varying(100),
    fec_salida date,
    fec_llegada date,
    cod_transporte character varying(20),
    des_lugarhopedaje character varying(60),
    fec_hospedaje date,
    des_computadora character varying(150),
    cod_telefono character varying(16),
    des_accesorios character varying(150),
    des_nbresponsable character varying(50),
    des_nbpuesto character varying(50),
    des_lugarresp character varying(100),
    cod_telefonoresp character varying(16),
    tim_horario time without time zone,
    fec_iniciocontra date,
    fec_terminocontra date,
    imp_sueldomensual numeric(6,2),
    imp_nominaimss numeric(6,2),
    imp_honorarios numeric(6,2),
    imp_otros numeric(6,2),
    cod_rfc character varying(13),
    des_razonsocial character varying(45),
    des_correo character varying(50),
    cod_cpostal integer,
    des_direccionfact character varying(200),
    cod_cliente integer NOT NULL,
    cod_gpy integer,
    cod_rhta integer,
    cod_ape integer,
    cod_rys integer,
    cod_creadopor integer NOT NULL,
    cod_modificadopor integer NOT NULL,
    fec_creacion date DEFAULT CURRENT_DATE NOT NULL,
    fec_modificacion date DEFAULT CURRENT_DATE NOT NULL
);


ALTER TABLE sgrh.tsgrhcartaasignacion OWNER TO suite;

--
-- TOC entry 270 (class 1259 OID 99439)
-- Name: tsgrhcatrespuestas; Type: TABLE; Schema: sgrh; Owner: suite
--

CREATE TABLE sgrh.tsgrhcatrespuestas (
    cod_catrespuesta integer DEFAULT nextval('sgrh.seq_catrespuestas'::regclass) NOT NULL,
    des_respuesta character varying(100) NOT NULL,
    cod_ponderacion integer NOT NULL
);


ALTER TABLE sgrh.tsgrhcatrespuestas OWNER TO suite;

--
-- TOC entry 271 (class 1259 OID 99443)
-- Name: tsgrhclientes; Type: TABLE; Schema: sgrh; Owner: suite
--

CREATE TABLE sgrh.tsgrhclientes (
    cod_cliente integer DEFAULT nextval('sgrh.seq_clientes'::regclass) NOT NULL,
    des_nbcliente character varying(90),
    des_direccioncte character varying(150) NOT NULL,
    des_nbcontactocte character varying(70) NOT NULL,
    des_correocte character varying(50) NOT NULL,
    cod_telefonocte character varying(16) DEFAULT NULL::character varying
);


ALTER TABLE sgrh.tsgrhclientes OWNER TO suite;

--
-- TOC entry 272 (class 1259 OID 99448)
-- Name: tsgrhcontrataciones; Type: TABLE; Schema: sgrh; Owner: suite
--

CREATE TABLE sgrh.tsgrhcontrataciones (
    cod_contratacion integer DEFAULT nextval('sgrh.seq_contrataciones'::regclass) NOT NULL,
    fec_inicio date NOT NULL,
    fec_termino date,
    des_esquema character varying(30) NOT NULL,
    cod_salarioestmin numeric(6,2) NOT NULL,
    cod_salarioestmax numeric(6,2),
    tim_jornada time without time zone,
    cod_creadopor integer NOT NULL,
    cod_modificadopor integer NOT NULL,
    fec_creacion date DEFAULT CURRENT_DATE NOT NULL,
    fec_modificacion date DEFAULT CURRENT_DATE NOT NULL
);


ALTER TABLE sgrh.tsgrhcontrataciones OWNER TO suite;

--
-- TOC entry 273 (class 1259 OID 99454)
-- Name: tsgrhcontratos; Type: TABLE; Schema: sgrh; Owner: suite
--

CREATE TABLE sgrh.tsgrhcontratos (
    cod_contrato integer DEFAULT nextval('sgrh.seq_contratos'::regclass) NOT NULL,
    des_nbconsultor character varying(45) NOT NULL,
    des_appaterno character varying(45) NOT NULL,
    des_apmaterno character varying(45) NOT NULL,
    cod_creadopor integer NOT NULL,
    cod_modificadopor integer NOT NULL,
    fec_creacion date DEFAULT CURRENT_DATE NOT NULL,
    fec_modificacion date DEFAULT CURRENT_DATE NOT NULL
);


ALTER TABLE sgrh.tsgrhcontratos OWNER TO suite;

--
-- TOC entry 274 (class 1259 OID 99460)
-- Name: tsgrhempleados; Type: TABLE; Schema: sgrh; Owner: suite
--

CREATE TABLE sgrh.tsgrhempleados (
    cod_empleado integer DEFAULT nextval('sgrh.seq_empleado'::regclass) NOT NULL,
    des_nombre character varying(45) NOT NULL,
    des_nombres character varying(60),
    des_apepaterno character varying(40) NOT NULL,
    des_apematerno character varying(40),
    des_direccion character varying(150) NOT NULL,
    fec_nacimiento date NOT NULL,
    des_lugarnacimiento character varying(50) NOT NULL,
    cod_edad integer NOT NULL,
    des_correo character varying(50),
    cod_tiposangre character varying(5) NOT NULL,
    cod_telefonocasa character varying(16),
    cod_telefonocelular character varying(16),
    cod_telemergencia character varying(16),
    bin_identificacion bytea,
    bin_pasaporte bytea,
    bin_visa bytea,
    cod_licenciamanejo character varying(20),
    fec_ingreso date NOT NULL,
    cod_rfc character varying(13),
    cod_nss character varying(20),
    cod_curp character varying(18) NOT NULL,
    bin_foto bytea,
    cod_tipofoto character varying(30),
    cod_extensionfoto character varying(5),
    cod_empleadoactivo boolean,
    cod_estatusempleado integer NOT NULL,
    cod_estadocivil integer NOT NULL,
    cod_rol integer,
    cod_puesto integer,
    cod_diasvacaciones integer,
    cod_sistemasuite integer,
    fec_creacion date DEFAULT CURRENT_DATE NOT NULL,
    fec_modificacion date DEFAULT CURRENT_DATE NOT NULL,
    cod_creadopor integer,
    cod_modificadopor integer,
    cod_area integer NOT NULL,
    des_correopersonal character varying(50)
);


ALTER TABLE sgrh.tsgrhempleados OWNER TO suite;

--
-- TOC entry 275 (class 1259 OID 99469)
-- Name: tsgrhencuesta; Type: TABLE; Schema: sgrh; Owner: suite
--

CREATE TABLE sgrh.tsgrhencuesta (
    cod_encuesta integer DEFAULT nextval('sgrh.seq_encuestas'::regclass) NOT NULL,
    des_nbencuesta character varying(50) NOT NULL,
    cod_edoencuesta character varying(20) NOT NULL,
    fec_fechaencuesta date NOT NULL,
    cod_lugar integer NOT NULL,
    tim_duracion time without time zone,
    des_elementosvalidar character varying(200),
    des_defectos character varying(200),
    des_introduccion character varying(200),
    cod_aceptado boolean,
    cod_edoeliminar boolean,
    cod_creadopor integer NOT NULL,
    cod_modificadopor integer NOT NULL,
    fec_creacion date DEFAULT CURRENT_DATE NOT NULL,
    fec_modificacion date DEFAULT CURRENT_DATE NOT NULL,
    cod_area integer NOT NULL,
    CONSTRAINT tsgrhencuesta_cod_edoencuesta_check CHECK (((cod_edoencuesta)::text = ANY (ARRAY[('En Proceso'::character varying)::text, ('Corregido'::character varying)::text, ('Aceptado'::character varying)::text])))
);


ALTER TABLE sgrh.tsgrhencuesta OWNER TO suite;

--
-- TOC entry 276 (class 1259 OID 99479)
-- Name: tsgrhencuesta_participantes; Type: TABLE; Schema: sgrh; Owner: suite
--

CREATE TABLE sgrh.tsgrhencuesta_participantes (
    cod_participantenc integer DEFAULT nextval('sgrh.seq_cat_encuesta_participantes'::regclass) NOT NULL,
    cod_empleado integer NOT NULL,
    cod_encuesta integer NOT NULL,
    cod_pregunta integer NOT NULL,
    cod_respuesta integer,
    respuesta_abierta text
);


ALTER TABLE sgrh.tsgrhencuesta_participantes OWNER TO suite;

--
-- TOC entry 277 (class 1259 OID 99486)
-- Name: tsgrhescolaridad; Type: TABLE; Schema: sgrh; Owner: suite
--

CREATE TABLE sgrh.tsgrhescolaridad (
    cod_escolaridad integer DEFAULT nextval('sgrh.seq_escolaridad'::regclass) NOT NULL,
    cod_empleado integer NOT NULL,
    des_nbinstitucion character varying(70) NOT NULL,
    des_nivelestudios character varying(30) NOT NULL,
    cod_titulo boolean NOT NULL,
    fec_inicio date NOT NULL,
    fec_termino date NOT NULL,
    bin_titulo bytea
);


ALTER TABLE sgrh.tsgrhescolaridad OWNER TO suite;

--
-- TOC entry 278 (class 1259 OID 99493)
-- Name: tsgrhestatuscapacitacion; Type: TABLE; Schema: sgrh; Owner: suite
--

CREATE TABLE sgrh.tsgrhestatuscapacitacion (
    cod_estatus integer DEFAULT nextval('sgrh.seq_estatus'::regclass) NOT NULL,
    des_estatus character varying(50) NOT NULL,
    aud_feccreacion date NOT NULL,
    aud_fecmodificacion date,
    aud_creadopor integer NOT NULL,
    aud_modificadopor integer
);


ALTER TABLE sgrh.tsgrhestatuscapacitacion OWNER TO suite;

--
-- TOC entry 279 (class 1259 OID 99497)
-- Name: tsgrhevacapacitacion; Type: TABLE; Schema: sgrh; Owner: suite
--

CREATE TABLE sgrh.tsgrhevacapacitacion (
    cod_evacapacitacion integer DEFAULT nextval('sgrh.seq_evacapacitacion'::regclass) NOT NULL,
    cod_plancapacitacion integer NOT NULL,
    cod_empleado integer,
    des_estado character varying(50) NOT NULL,
    des_evaluacion character varying(50),
    auf_feccreacion date NOT NULL,
    aud_fecmodificacion date,
    aud_creadopor integer NOT NULL,
    aud_modificadopor integer NOT NULL
);


ALTER TABLE sgrh.tsgrhevacapacitacion OWNER TO suite;

--
-- TOC entry 280 (class 1259 OID 99501)
-- Name: tsgrhevacontestadas; Type: TABLE; Schema: sgrh; Owner: suite
--

CREATE TABLE sgrh.tsgrhevacontestadas (
    cod_evacontestada integer DEFAULT nextval('sgrh.seq_evacontestadas'::regclass) NOT NULL,
    cod_evaluacion integer NOT NULL,
    cod_evaluador integer NOT NULL,
    cod_evaluado integer NOT NULL,
    cod_total integer NOT NULL,
    bin_reporte bytea
);


ALTER TABLE sgrh.tsgrhevacontestadas OWNER TO suite;

--
-- TOC entry 281 (class 1259 OID 99508)
-- Name: tsgrhevaluaciones; Type: TABLE; Schema: sgrh; Owner: suite
--

CREATE TABLE sgrh.tsgrhevaluaciones (
    cod_evaluacion integer DEFAULT nextval('sgrh.seq_evaluaciones'::regclass) NOT NULL,
    des_nbevaluacion character varying(60) NOT NULL,
    cod_creadopor integer NOT NULL,
    cod_modificadopor integer NOT NULL,
    fec_creacion date DEFAULT CURRENT_DATE NOT NULL,
    fec_modificacion date DEFAULT CURRENT_DATE NOT NULL,
    des_edoevaluacion character varying(30) DEFAULT '--'::character varying,
    cod_edoeliminar boolean,
    CONSTRAINT tsgrhevaluaciones_des_edoevaluacion_check CHECK (((des_edoevaluacion)::text = ANY (ARRAY[('--'::character varying)::text, ('En Proceso'::character varying)::text, ('Corregido'::character varying)::text, ('Aceptado'::character varying)::text])))
);


ALTER TABLE sgrh.tsgrhevaluaciones OWNER TO suite;

--
-- TOC entry 282 (class 1259 OID 99516)
-- Name: tsgrhexperienciaslaborales; Type: TABLE; Schema: sgrh; Owner: suite
--

CREATE TABLE sgrh.tsgrhexperienciaslaborales (
    cod_experiencia integer DEFAULT nextval('sgrh.seq_experiencialab'::regclass) NOT NULL,
    cod_empleado integer NOT NULL,
    des_nbempresa character varying(50) NOT NULL,
    des_nbpuesto character varying(50) NOT NULL,
    fec_inicio date NOT NULL,
    fec_termino date NOT NULL,
    txt_actividades text NOT NULL,
    des_ubicacion character varying(70),
    des_nbcliente character varying(70),
    des_proyecto character varying(70),
    txt_logros character varying(300)
);


ALTER TABLE sgrh.tsgrhexperienciaslaborales OWNER TO suite;

--
-- TOC entry 283 (class 1259 OID 99523)
-- Name: tsgrhfactoreseva; Type: TABLE; Schema: sgrh; Owner: suite
--

CREATE TABLE sgrh.tsgrhfactoreseva (
    cod_factor integer DEFAULT nextval('sgrh.seq_factoreseva'::regclass) NOT NULL,
    des_nbfactor character varying(60) NOT NULL,
    cod_edoeliminar boolean NOT NULL
);


ALTER TABLE sgrh.tsgrhfactoreseva OWNER TO suite;

--
-- TOC entry 284 (class 1259 OID 99527)
-- Name: tsgrhidiomas; Type: TABLE; Schema: sgrh; Owner: suite
--

CREATE TABLE sgrh.tsgrhidiomas (
    cod_idioma integer DEFAULT nextval('sgrh.seq_idiomas'::regclass) NOT NULL,
    des_nbidioma character varying(45) NOT NULL,
    por_dominiooral integer,
    por_dominioescrito integer,
    cod_empleado integer
);


ALTER TABLE sgrh.tsgrhidiomas OWNER TO suite;

--
-- TOC entry 285 (class 1259 OID 99531)
-- Name: tsgrhlogistica; Type: TABLE; Schema: sgrh; Owner: suite
--

CREATE TABLE sgrh.tsgrhlogistica (
    cod_logistica integer DEFAULT nextval('sgrh.seq_logistica'::regclass) NOT NULL,
    tim_totalhoras integer NOT NULL,
    tim_horafin time without time zone NOT NULL,
    tim_horainicio time without time zone NOT NULL,
    des_requerimientos character varying(200),
    des_lugarcapacitacion character varying(200),
    fec_fecinicio date NOT NULL,
    fec_termino date NOT NULL,
    cod_plancapacitacion integer NOT NULL,
    aud_feccreacion date NOT NULL,
    aud_fecmodificacion date NOT NULL,
    aud_creadopor integer NOT NULL,
    aud_modificadopor integer NOT NULL
);


ALTER TABLE sgrh.tsgrhlogistica OWNER TO suite;

--
-- TOC entry 286 (class 1259 OID 99535)
-- Name: tsgrhmodo; Type: TABLE; Schema: sgrh; Owner: suite
--

CREATE TABLE sgrh.tsgrhmodo (
    cod_modo integer DEFAULT nextval('sgrh.seq_modo'::regclass) NOT NULL,
    des_nbmodo character varying(50) NOT NULL,
    aud_feccreacion date NOT NULL,
    aud_fecmodificacion date,
    aud_creadopor integer NOT NULL,
    aud_modificadopor integer
);


ALTER TABLE sgrh.tsgrhmodo OWNER TO suite;

--
-- TOC entry 287 (class 1259 OID 99539)
-- Name: tsgrhperfiles; Type: TABLE; Schema: sgrh; Owner: suite
--

CREATE TABLE sgrh.tsgrhperfiles (
    cod_perfil integer DEFAULT nextval('sgrh.seq_perfiles'::regclass) NOT NULL,
    des_perfil character varying(100) NOT NULL
);


ALTER TABLE sgrh.tsgrhperfiles OWNER TO suite;

--
-- TOC entry 288 (class 1259 OID 99543)
-- Name: tsgrhplancapacitacion; Type: TABLE; Schema: sgrh; Owner: suite
--

CREATE TABLE sgrh.tsgrhplancapacitacion (
    cod_plancapacitacion integer DEFAULT nextval('sgrh.seq_plancapacitacion'::regclass) NOT NULL,
    des_nombre character varying(50) NOT NULL,
    cod_modo integer NOT NULL,
    cod_tipocapacitacion integer NOT NULL,
    des_criterios character varying(200) NOT NULL,
    cod_proceso integer NOT NULL,
    des_instructor character varying(50) NOT NULL,
    cod_proveedor integer NOT NULL,
    cod_estatus integer NOT NULL,
    des_comentarios character varying(200),
    des_evaluacion character varying(50),
    aud_feccreacion date NOT NULL,
    aud_fecmodificacion date NOT NULL,
    aud_creadopor integer NOT NULL,
    aud_modificadopor integer NOT NULL
);


ALTER TABLE sgrh.tsgrhplancapacitacion OWNER TO suite;

--
-- TOC entry 289 (class 1259 OID 99550)
-- Name: tsgrhplanoperativo; Type: TABLE; Schema: sgrh; Owner: suite
--

CREATE TABLE sgrh.tsgrhplanoperativo (
    cod_planoperativo integer DEFAULT nextval('sgrh.seq_planesoperativos'::regclass) NOT NULL,
    des_nbplan character varying(100) NOT NULL,
    cod_version character varying(5) NOT NULL,
    cod_anio integer NOT NULL,
    cod_estatus character varying(20) NOT NULL,
    bin_planoperativo bytea,
    cod_creadopor integer NOT NULL,
    cod_modificadopor integer NOT NULL,
    fec_creacion date DEFAULT CURRENT_DATE NOT NULL,
    fec_modificacion date DEFAULT CURRENT_DATE NOT NULL
);


ALTER TABLE sgrh.tsgrhplanoperativo OWNER TO suite;

--
-- TOC entry 290 (class 1259 OID 99559)
-- Name: tsgrhpreguntasenc; Type: TABLE; Schema: sgrh; Owner: suite
--

CREATE TABLE sgrh.tsgrhpreguntasenc (
    cod_pregunta integer DEFAULT nextval('sgrh.seq_preguntasenc'::regclass) NOT NULL,
    des_pregunta character varying(200) NOT NULL,
    cod_tipopregunta boolean,
    cod_edoeliminar boolean,
    cod_encuesta integer NOT NULL
);


ALTER TABLE sgrh.tsgrhpreguntasenc OWNER TO suite;

--
-- TOC entry 291 (class 1259 OID 99563)
-- Name: tsgrhpreguntaseva; Type: TABLE; Schema: sgrh; Owner: suite
--

CREATE TABLE sgrh.tsgrhpreguntaseva (
    cod_pregunta integer DEFAULT nextval('sgrh.seq_preguntaseva'::regclass) NOT NULL,
    des_pregunta character varying(100) NOT NULL,
    cod_edoeliminar boolean NOT NULL,
    cod_evaluacion integer NOT NULL,
    cod_subfactor integer NOT NULL
);


ALTER TABLE sgrh.tsgrhpreguntaseva OWNER TO suite;

--
-- TOC entry 292 (class 1259 OID 99567)
-- Name: tsgrhprocesos; Type: TABLE; Schema: sgrh; Owner: suite
--

CREATE TABLE sgrh.tsgrhprocesos (
    cod_proceso integer DEFAULT nextval('sgrh.seq_proceso'::regclass) NOT NULL,
    des_nbproceso character varying(50) NOT NULL,
    aud_feccreacion date NOT NULL,
    aud_fecmodificacion date,
    aud_creadopor integer NOT NULL,
    aud_modificadopor integer
);


ALTER TABLE sgrh.tsgrhprocesos OWNER TO suite;

--
-- TOC entry 293 (class 1259 OID 99571)
-- Name: tsgrhproveedores; Type: TABLE; Schema: sgrh; Owner: suite
--

CREATE TABLE sgrh.tsgrhproveedores (
    cod_proveedor integer DEFAULT nextval('sgrh.seq_proveedor'::regclass) NOT NULL,
    des_nbproveedor character varying(50) NOT NULL,
    aud_feccreacion date NOT NULL,
    aud_fecmodificacion date,
    aud_creadopor integer NOT NULL,
    aud_modificadopor integer
);


ALTER TABLE sgrh.tsgrhproveedores OWNER TO suite;

--
-- TOC entry 294 (class 1259 OID 99575)
-- Name: tsgrhpuestos; Type: TABLE; Schema: sgrh; Owner: suite
--

CREATE TABLE sgrh.tsgrhpuestos (
    cod_puesto integer DEFAULT nextval('sgrh.seq_puestos'::regclass) NOT NULL,
    des_puesto character varying(100) NOT NULL,
    cod_area integer NOT NULL,
    cod_acronimo character varying(5)
);


ALTER TABLE sgrh.tsgrhpuestos OWNER TO suite;

--
-- TOC entry 295 (class 1259 OID 99579)
-- Name: tsgrhrelacionroles; Type: TABLE; Schema: sgrh; Owner: suite
--

CREATE TABLE sgrh.tsgrhrelacionroles (
    cod_plancapacitacion integer NOT NULL,
    cod_rolempleado integer NOT NULL,
    aud_feccreacion date NOT NULL,
    aud_fecmodificacion date,
    aud_creadopor integer NOT NULL,
    aud_modificadopor integer
);


ALTER TABLE sgrh.tsgrhrelacionroles OWNER TO suite;

--
-- TOC entry 296 (class 1259 OID 99582)
-- Name: tsgrhrespuestasenc; Type: TABLE; Schema: sgrh; Owner: suite
--

CREATE TABLE sgrh.tsgrhrespuestasenc (
    cod_respuesta integer DEFAULT nextval('sgrh.seq_respuestasenc'::regclass) NOT NULL,
    cod_catrespuesta integer,
    cod_pregunta integer NOT NULL,
    cod_edoeliminar boolean
);


ALTER TABLE sgrh.tsgrhrespuestasenc OWNER TO suite;

--
-- TOC entry 297 (class 1259 OID 99586)
-- Name: tsgrhrespuestaseva; Type: TABLE; Schema: sgrh; Owner: suite
--

CREATE TABLE sgrh.tsgrhrespuestaseva (
    cod_respuesta integer DEFAULT nextval('sgrh.seq_respuestaseva'::regclass) NOT NULL,
    des_respuesta character varying(200) NOT NULL,
    cod_pregunta integer NOT NULL,
    cod_evacontestada integer NOT NULL
);


ALTER TABLE sgrh.tsgrhrespuestaseva OWNER TO suite;

--
-- TOC entry 298 (class 1259 OID 99590)
-- Name: tsgrhrevplanoperativo; Type: TABLE; Schema: sgrh; Owner: suite
--

CREATE TABLE sgrh.tsgrhrevplanoperativo (
    cod_revplanoperativo integer DEFAULT nextval('sgrh.seq_revplanesoperativos'::regclass) NOT NULL,
    fec_creacion date DEFAULT CURRENT_DATE NOT NULL,
    cod_lugar integer NOT NULL,
    tim_duracion time without time zone,
    cod_participante1 integer,
    cod_participante2 integer,
    cod_participante3 integer,
    cod_participante4 integer,
    cod_participante5 integer,
    cod_planoperativo integer,
    des_puntosatratar character varying(250),
    des_acuerdosobtenidos character varying(500),
    cod_creadopor integer NOT NULL,
    cod_modificadopor integer NOT NULL,
    fec_modificacion date DEFAULT CURRENT_DATE NOT NULL
);


ALTER TABLE sgrh.tsgrhrevplanoperativo OWNER TO suite;

--
-- TOC entry 299 (class 1259 OID 99599)
-- Name: tsgrhrolempleado; Type: TABLE; Schema: sgrh; Owner: suite
--

CREATE TABLE sgrh.tsgrhrolempleado (
    cod_rolempleado integer DEFAULT nextval('sgrh.seq_rolempleado'::regclass) NOT NULL,
    des_nbrol character varying(50) NOT NULL,
    aud_feccreacion date NOT NULL,
    aud_fecmodificacion date,
    aud_creadopor integer NOT NULL,
    aud_modificadopor integer
);


ALTER TABLE sgrh.tsgrhrolempleado OWNER TO suite;

--
-- TOC entry 300 (class 1259 OID 99603)
-- Name: tsgrhroles; Type: TABLE; Schema: sgrh; Owner: suite
--

CREATE TABLE sgrh.tsgrhroles (
    cod_rol integer DEFAULT nextval('sgrh.seq_roles'::regclass) NOT NULL,
    des_nbrol character varying(15) NOT NULL
);


ALTER TABLE sgrh.tsgrhroles OWNER TO suite;

--
-- TOC entry 301 (class 1259 OID 99607)
-- Name: tsgrhsubfactoreseva; Type: TABLE; Schema: sgrh; Owner: suite
--

CREATE TABLE sgrh.tsgrhsubfactoreseva (
    cod_subfactor integer DEFAULT nextval('sgrh.seq_subfactoreseva'::regclass) NOT NULL,
    des_nbsubfactor character varying(60) NOT NULL,
    cod_factor integer NOT NULL,
    cod_edoeliminar boolean NOT NULL
);


ALTER TABLE sgrh.tsgrhsubfactoreseva OWNER TO suite;

--
-- TOC entry 302 (class 1259 OID 99611)
-- Name: tsgrhtipocapacitacion; Type: TABLE; Schema: sgrh; Owner: suite
--

CREATE TABLE sgrh.tsgrhtipocapacitacion (
    cod_tipocapacitacion integer DEFAULT nextval('sgrh.seq_tipocapacitacion'::regclass) NOT NULL,
    des_nbtipocapacitacion character varying(50) NOT NULL,
    aud_feccreacion date NOT NULL,
    aud_fecmodificacion date,
    aud_creadopor integer NOT NULL,
    aud_modificadopor integer
);


ALTER TABLE sgrh.tsgrhtipocapacitacion OWNER TO suite;

--
-- TOC entry 303 (class 1259 OID 99615)
-- Name: tsgrhvalidaevaluaciondes; Type: TABLE; Schema: sgrh; Owner: suite
--

CREATE TABLE sgrh.tsgrhvalidaevaluaciondes (
    cod_validacion integer DEFAULT nextval('sgrh.seq_validaevaluaciones'::regclass) NOT NULL,
    des_edoevaluacion character varying(30) NOT NULL,
    fec_validacion date,
    cod_lugar integer,
    tim_duracion time without time zone,
    des_defectosevalucacion character varying(1000),
    cod_edoeliminar boolean,
    cod_evaluacion integer,
    cod_participante1 integer NOT NULL,
    cod_participante2 integer,
    cod_participante3 integer,
    cod_participante4 integer,
    cod_evaluador integer,
    cod_evaluado integer,
    CONSTRAINT tsgrhvalidaevaluaciondes_des_edoevaluacion_check CHECK (((des_edoevaluacion)::text = ANY (ARRAY[('--'::character varying)::text, ('En Proceso'::character varying)::text, ('Corregido'::character varying)::text, ('Aceptado'::character varying)::text])))
);


ALTER TABLE sgrh.tsgrhvalidaevaluaciondes OWNER TO suite;

--
-- TOC entry 304 (class 1259 OID 99623)
-- Name: seq_agenda; Type: SEQUENCE; Schema: sgrt; Owner: suite
--

CREATE SEQUENCE sgrt.seq_agenda
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_agenda OWNER TO suite;

--
-- TOC entry 305 (class 1259 OID 99625)
-- Name: seq_archivo; Type: SEQUENCE; Schema: sgrt; Owner: suite
--

CREATE SEQUENCE sgrt.seq_archivo
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_archivo OWNER TO suite;

--
-- TOC entry 306 (class 1259 OID 99627)
-- Name: seq_asistente; Type: SEQUENCE; Schema: sgrt; Owner: suite
--

CREATE SEQUENCE sgrt.seq_asistente
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_asistente OWNER TO suite;

--
-- TOC entry 307 (class 1259 OID 99629)
-- Name: seq_attach; Type: SEQUENCE; Schema: sgrt; Owner: suite
--

CREATE SEQUENCE sgrt.seq_attach
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_attach OWNER TO suite;

--
-- TOC entry 308 (class 1259 OID 99631)
-- Name: seq_categoriafaq; Type: SEQUENCE; Schema: sgrt; Owner: suite
--

CREATE SEQUENCE sgrt.seq_categoriafaq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_categoriafaq OWNER TO suite;

--
-- TOC entry 309 (class 1259 OID 99633)
-- Name: seq_chat; Type: SEQUENCE; Schema: sgrt; Owner: suite
--

CREATE SEQUENCE sgrt.seq_chat
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 99999999999999
    CACHE 1;


ALTER TABLE sgrt.seq_chat OWNER TO suite;

--
-- TOC entry 310 (class 1259 OID 99635)
-- Name: seq_ciudad; Type: SEQUENCE; Schema: sgrt; Owner: suite
--

CREATE SEQUENCE sgrt.seq_ciudad
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_ciudad OWNER TO suite;

--
-- TOC entry 311 (class 1259 OID 99637)
-- Name: seq_comentsagenda; Type: SEQUENCE; Schema: sgrt; Owner: suite
--

CREATE SEQUENCE sgrt.seq_comentsagenda
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_comentsagenda OWNER TO suite;

--
-- TOC entry 312 (class 1259 OID 99639)
-- Name: seq_comentsreunion; Type: SEQUENCE; Schema: sgrt; Owner: suite
--

CREATE SEQUENCE sgrt.seq_comentsreunion
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_comentsreunion OWNER TO suite;

--
-- TOC entry 313 (class 1259 OID 99641)
-- Name: seq_compromiso; Type: SEQUENCE; Schema: sgrt; Owner: suite
--

CREATE SEQUENCE sgrt.seq_compromiso
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_compromiso OWNER TO suite;

--
-- TOC entry 314 (class 1259 OID 99643)
-- Name: seq_contacto; Type: SEQUENCE; Schema: sgrt; Owner: suite
--

CREATE SEQUENCE sgrt.seq_contacto
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_contacto OWNER TO suite;

--
-- TOC entry 315 (class 1259 OID 99645)
-- Name: seq_correo; Type: SEQUENCE; Schema: sgrt; Owner: suite
--

CREATE SEQUENCE sgrt.seq_correo
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_correo OWNER TO suite;

--
-- TOC entry 316 (class 1259 OID 99647)
-- Name: seq_depto; Type: SEQUENCE; Schema: sgrt; Owner: suite
--

CREATE SEQUENCE sgrt.seq_depto
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_depto OWNER TO suite;

--
-- TOC entry 317 (class 1259 OID 99649)
-- Name: seq_edoacuerdo; Type: SEQUENCE; Schema: sgrt; Owner: suite
--

CREATE SEQUENCE sgrt.seq_edoacuerdo
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_edoacuerdo OWNER TO suite;

--
-- TOC entry 318 (class 1259 OID 99651)
-- Name: seq_elemento; Type: SEQUENCE; Schema: sgrt; Owner: suite
--

CREATE SEQUENCE sgrt.seq_elemento
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_elemento OWNER TO suite;

--
-- TOC entry 319 (class 1259 OID 99653)
-- Name: seq_estadorep; Type: SEQUENCE; Schema: sgrt; Owner: suite
--

CREATE SEQUENCE sgrt.seq_estadorep
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_estadorep OWNER TO suite;

--
-- TOC entry 320 (class 1259 OID 99655)
-- Name: seq_faq; Type: SEQUENCE; Schema: sgrt; Owner: suite
--

CREATE SEQUENCE sgrt.seq_faq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_faq OWNER TO suite;

--
-- TOC entry 321 (class 1259 OID 99657)
-- Name: seq_grupo; Type: SEQUENCE; Schema: sgrt; Owner: suite
--

CREATE SEQUENCE sgrt.seq_grupo
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_grupo OWNER TO suite;

--
-- TOC entry 322 (class 1259 OID 99659)
-- Name: seq_invitado; Type: SEQUENCE; Schema: sgrt; Owner: suite
--

CREATE SEQUENCE sgrt.seq_invitado
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_invitado OWNER TO suite;

--
-- TOC entry 323 (class 1259 OID 99661)
-- Name: seq_lugar; Type: SEQUENCE; Schema: sgrt; Owner: suite
--

CREATE SEQUENCE sgrt.seq_lugar
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_lugar OWNER TO suite;

--
-- TOC entry 324 (class 1259 OID 99663)
-- Name: seq_mensaje; Type: SEQUENCE; Schema: sgrt; Owner: suite
--

CREATE SEQUENCE sgrt.seq_mensaje
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_mensaje OWNER TO suite;

--
-- TOC entry 325 (class 1259 OID 99665)
-- Name: seq_nota; Type: SEQUENCE; Schema: sgrt; Owner: suite
--

CREATE SEQUENCE sgrt.seq_nota
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_nota OWNER TO suite;

--
-- TOC entry 326 (class 1259 OID 99667)
-- Name: seq_plantillacorreo; Type: SEQUENCE; Schema: sgrt; Owner: suite
--

CREATE SEQUENCE sgrt.seq_plantillacorreo
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_plantillacorreo OWNER TO suite;

--
-- TOC entry 327 (class 1259 OID 99669)
-- Name: seq_prioridad; Type: SEQUENCE; Schema: sgrt; Owner: suite
--

CREATE SEQUENCE sgrt.seq_prioridad
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_prioridad OWNER TO suite;

--
-- TOC entry 328 (class 1259 OID 99671)
-- Name: seq_resp; Type: SEQUENCE; Schema: sgrt; Owner: suite
--

CREATE SEQUENCE sgrt.seq_resp
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_resp OWNER TO suite;

--
-- TOC entry 329 (class 1259 OID 99673)
-- Name: seq_respuesta; Type: SEQUENCE; Schema: sgrt; Owner: suite
--

CREATE SEQUENCE sgrt.seq_respuesta
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_respuesta OWNER TO suite;

--
-- TOC entry 330 (class 1259 OID 99675)
-- Name: seq_respuestas_participantes; Type: SEQUENCE; Schema: sgrt; Owner: suite
--

CREATE SEQUENCE sgrt.seq_respuestas_participantes
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_respuestas_participantes OWNER TO suite;

--
-- TOC entry 331 (class 1259 OID 99677)
-- Name: seq_reunion; Type: SEQUENCE; Schema: sgrt; Owner: suite
--

CREATE SEQUENCE sgrt.seq_reunion
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_reunion OWNER TO suite;

--
-- TOC entry 332 (class 1259 OID 99679)
-- Name: seq_servicio; Type: SEQUENCE; Schema: sgrt; Owner: suite
--

CREATE SEQUENCE sgrt.seq_servicio
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_servicio OWNER TO suite;

--
-- TOC entry 333 (class 1259 OID 99681)
-- Name: seq_solicitud; Type: SEQUENCE; Schema: sgrt; Owner: suite
--

CREATE SEQUENCE sgrt.seq_solicitud
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_solicitud OWNER TO suite;

--
-- TOC entry 334 (class 1259 OID 99683)
-- Name: seq_ticket; Type: SEQUENCE; Schema: sgrt; Owner: suite
--

CREATE SEQUENCE sgrt.seq_ticket
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_ticket OWNER TO suite;

--
-- TOC entry 335 (class 1259 OID 99685)
-- Name: seq_topico; Type: SEQUENCE; Schema: sgrt; Owner: suite
--

CREATE SEQUENCE sgrt.seq_topico
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_topico OWNER TO suite;

--
-- TOC entry 336 (class 1259 OID 99687)
-- Name: tsgrtagenda; Type: TABLE; Schema: sgrt; Owner: suite
--

CREATE TABLE sgrt.tsgrtagenda (
    cod_agenda integer DEFAULT nextval('sgrt.seq_agenda'::regclass) NOT NULL,
    des_texto character varying(200) NOT NULL,
    cnu_tratado smallint DEFAULT '0'::smallint,
    cod_reunion integer NOT NULL
);


ALTER TABLE sgrt.tsgrtagenda OWNER TO suite;

--
-- TOC entry 337 (class 1259 OID 99692)
-- Name: tsgrtarchivos; Type: TABLE; Schema: sgrt; Owner: suite
--

CREATE TABLE sgrt.tsgrtarchivos (
    cod_archivo integer DEFAULT nextval('sgrt.seq_archivo'::regclass) NOT NULL,
    bin_archivo bytea,
    cod_reunion integer NOT NULL
);


ALTER TABLE sgrt.tsgrtarchivos OWNER TO suite;

--
-- TOC entry 338 (class 1259 OID 99699)
-- Name: tsgrtasistentes; Type: TABLE; Schema: sgrt; Owner: suite
--

CREATE TABLE sgrt.tsgrtasistentes (
    cod_asistente integer DEFAULT nextval('sgrt.seq_asistente'::regclass) NOT NULL,
    cod_reunion integer NOT NULL,
    cod_empleado integer,
    cnu_asiste smallint NOT NULL,
    cod_invitado integer
);


ALTER TABLE sgrt.tsgrtasistentes OWNER TO suite;

--
-- TOC entry 339 (class 1259 OID 99703)
-- Name: tsgrtattchticket; Type: TABLE; Schema: sgrt; Owner: suite
--

CREATE TABLE sgrt.tsgrtattchticket (
    cod_attach integer DEFAULT nextval('sgrt.seq_attach'::regclass) NOT NULL,
    cod_ticket integer NOT NULL,
    cod_tamano character varying(20) NOT NULL,
    des_nombre character varying(128) NOT NULL,
    cod_sistemasuite integer NOT NULL,
    fec_ultactualizacion timestamp without time zone,
    cod_ultactualizacionpor integer NOT NULL,
    cod_creadopor integer NOT NULL,
    fec_creacion timestamp without time zone,
    bin_attach bytea
);


ALTER TABLE sgrt.tsgrtattchticket OWNER TO suite;

--
-- TOC entry 340 (class 1259 OID 99710)
-- Name: tsgrtayudatopico; Type: TABLE; Schema: sgrt; Owner: suite
--

CREATE TABLE sgrt.tsgrtayudatopico (
    cod_topico integer DEFAULT nextval('sgrt.seq_topico'::regclass) NOT NULL,
    cnu_activo smallint NOT NULL,
    cnu_autorespuesta smallint NOT NULL,
    cod_prioridad smallint NOT NULL,
    cod_depto integer NOT NULL,
    des_topico character varying(36) NOT NULL,
    cod_sistemasuite integer NOT NULL,
    fec_ultactualizacion timestamp without time zone,
    cod_ultactualizacionpor integer,
    cod_creadopor integer NOT NULL,
    fec_creacion timestamp without time zone
);


ALTER TABLE sgrt.tsgrtayudatopico OWNER TO suite;

--
-- TOC entry 341 (class 1259 OID 99714)
-- Name: tsgrtcategoriafaq; Type: TABLE; Schema: sgrt; Owner: suite
--

CREATE TABLE sgrt.tsgrtcategoriafaq (
    cod_categoriafaq integer DEFAULT nextval('sgrt.seq_categoriafaq'::regclass) NOT NULL,
    cnu_tipo smallint NOT NULL,
    des_categoria character varying(255) NOT NULL,
    des_descripcion character varying(255) NOT NULL,
    des_notas character varying(255) NOT NULL,
    tim_ultactualiza timestamp without time zone NOT NULL,
    fec_creacion timestamp without time zone NOT NULL,
    fec_ultactualizadopor integer NOT NULL,
    cod_creadopor integer NOT NULL
);


ALTER TABLE sgrt.tsgrtcategoriafaq OWNER TO suite;

--
-- TOC entry 342 (class 1259 OID 99721)
-- Name: tsgrtchat; Type: TABLE; Schema: sgrt; Owner: suite
--

CREATE TABLE sgrt.tsgrtchat (
    cod_chat integer DEFAULT nextval('sgrt.seq_chat'::regclass) NOT NULL,
    chat character varying NOT NULL
);


ALTER TABLE sgrt.tsgrtchat OWNER TO suite;

--
-- TOC entry 343 (class 1259 OID 99728)
-- Name: tsgrtciudades; Type: TABLE; Schema: sgrt; Owner: suite
--

CREATE TABLE sgrt.tsgrtciudades (
    cod_ciudad integer DEFAULT nextval('sgrt.seq_ciudad'::regclass) NOT NULL,
    des_nbciudad character varying(100) NOT NULL,
    cod_estadorep integer NOT NULL
);


ALTER TABLE sgrt.tsgrtciudades OWNER TO suite;

--
-- TOC entry 344 (class 1259 OID 99732)
-- Name: tsgrtcomentariosagenda; Type: TABLE; Schema: sgrt; Owner: suite
--

CREATE TABLE sgrt.tsgrtcomentariosagenda (
    cod_comentsagenda integer DEFAULT nextval('sgrt.seq_comentsagenda'::regclass) NOT NULL,
    des_comentario character varying(500) NOT NULL,
    cod_agenda integer NOT NULL,
    cod_invitado integer NOT NULL
);


ALTER TABLE sgrt.tsgrtcomentariosagenda OWNER TO suite;

--
-- TOC entry 345 (class 1259 OID 99739)
-- Name: tsgrtcomentariosreunion; Type: TABLE; Schema: sgrt; Owner: suite
--

CREATE TABLE sgrt.tsgrtcomentariosreunion (
    cod_commentsreunion integer DEFAULT nextval('sgrt.seq_comentsreunion'::regclass) NOT NULL,
    des_comentario character varying(500) NOT NULL,
    cod_invitado integer NOT NULL,
    cod_reunion integer NOT NULL
);


ALTER TABLE sgrt.tsgrtcomentariosreunion OWNER TO suite;

--
-- TOC entry 346 (class 1259 OID 99746)
-- Name: tsgrtcompromisos; Type: TABLE; Schema: sgrt; Owner: suite
--

CREATE TABLE sgrt.tsgrtcompromisos (
    cod_compromiso integer DEFAULT nextval('sgrt.seq_compromiso'::regclass) NOT NULL,
    des_descripcion character varying(200) NOT NULL,
    fec_solicitud date NOT NULL,
    fec_compromiso date NOT NULL,
    cod_reunion integer NOT NULL,
    cod_validador integer NOT NULL,
    cod_verificador integer NOT NULL,
    cod_estado integer,
    des_valor character varying(45),
    cod_ejecutor integer NOT NULL,
    cod_tipoejecutor character varying(10),
    cnu_revisado smallint,
    cod_estatus sgrt.estatus_compromiso DEFAULT 'Pendiente'::sgrt.estatus_compromiso NOT NULL,
    cod_tipocompromiso sgrt.tipo_compromiso DEFAULT 'Pendiente'::sgrt.tipo_compromiso NOT NULL,
    cod_chat integer,
    fec_entrega date
);


ALTER TABLE sgrt.tsgrtcompromisos OWNER TO suite;

--
-- TOC entry 347 (class 1259 OID 99752)
-- Name: tsgrtcorreo; Type: TABLE; Schema: sgrt; Owner: suite
--

CREATE TABLE sgrt.tsgrtcorreo (
    cod_correo integer DEFAULT nextval('sgrt.seq_correo'::regclass) NOT NULL,
    cnu_autorespuesta smallint NOT NULL,
    cod_prioridad smallint NOT NULL,
    cod_depto integer,
    des_nbusuario character varying(32) NOT NULL,
    des_correo character varying(50) NOT NULL,
    des_nombre character varying(70) NOT NULL,
    des_contrasena character varying(30) NOT NULL,
    cnu_activo smallint NOT NULL,
    des_dirhost character varying(125) NOT NULL,
    cod_protocolo sgrt.protocolo DEFAULT 'POP'::sgrt.protocolo NOT NULL,
    cod_encriptacion sgrt.encriptacion DEFAULT 'NONE'::sgrt.encriptacion NOT NULL,
    cod_puerto integer,
    cnu_frecsinc smallint NOT NULL,
    cnu_nummaxcorreo smallint NOT NULL,
    cnu_eliminar smallint NOT NULL,
    cnu_errores smallint NOT NULL,
    fec_ulterror timestamp without time zone,
    fec_ultsincr timestamp without time zone,
    cnu_smtpactivo smallint,
    des_smtphost character varying(125) NOT NULL,
    cod_smtpport integer,
    cnu_smtpsecure smallint NOT NULL,
    cnu_smtpauth smallint NOT NULL,
    cod_sistemasuite integer NOT NULL,
    fec_ultactualizacion timestamp without time zone,
    cod_ultactualizacionpor integer NOT NULL,
    cod_creadopor integer NOT NULL,
    fec_creacion timestamp without time zone,
    cod_usuario integer NOT NULL
);


ALTER TABLE sgrt.tsgrtcorreo OWNER TO suite;

--
-- TOC entry 348 (class 1259 OID 99758)
-- Name: tsgrtdatossolicitud; Type: TABLE; Schema: sgrt; Owner: suite
--

CREATE TABLE sgrt.tsgrtdatossolicitud (
    cod_datosolicitud integer NOT NULL,
    cod_elemento integer NOT NULL,
    des_descripcion character varying(45) NOT NULL,
    cod_solicitud integer NOT NULL,
    cod_edosolicitud integer NOT NULL
);


ALTER TABLE sgrt.tsgrtdatossolicitud OWNER TO suite;

--
-- TOC entry 349 (class 1259 OID 99761)
-- Name: tsgrtdepartamento; Type: TABLE; Schema: sgrt; Owner: suite
--

CREATE TABLE sgrt.tsgrtdepartamento (
    cod_depto integer DEFAULT nextval('sgrt.seq_depto'::regclass) NOT NULL,
    cod_plantillacorreo integer NOT NULL,
    cod_correo integer NOT NULL,
    cod_manager integer DEFAULT 0 NOT NULL,
    des_nombre character varying(50) NOT NULL,
    cod_ncorto character varying(10) NOT NULL,
    des_firma text NOT NULL,
    cnu_publico smallint NOT NULL,
    cod_sistemasuite integer DEFAULT 1 NOT NULL,
    fec_ultactualizacion timestamp without time zone,
    cod_ultactualizacionpor integer NOT NULL,
    cod_creadopor integer NOT NULL,
    fec_creacion timestamp without time zone
);


ALTER TABLE sgrt.tsgrtdepartamento OWNER TO suite;

--
-- TOC entry 350 (class 1259 OID 99770)
-- Name: tsgrtedosolicitudes; Type: TABLE; Schema: sgrt; Owner: suite
--

CREATE TABLE sgrt.tsgrtedosolicitudes (
    cod_edosolicitud integer NOT NULL,
    cod_nbedosolicitud integer NOT NULL
);


ALTER TABLE sgrt.tsgrtedosolicitudes OWNER TO suite;

--
-- TOC entry 351 (class 1259 OID 99773)
-- Name: tsgrtelementos; Type: TABLE; Schema: sgrt; Owner: suite
--

CREATE TABLE sgrt.tsgrtelementos (
    cod_elemento integer DEFAULT nextval('sgrt.seq_elemento'::regclass) NOT NULL,
    des_nombre character varying(50) NOT NULL,
    cnu_activo smallint NOT NULL
);


ALTER TABLE sgrt.tsgrtelementos OWNER TO suite;

--
-- TOC entry 352 (class 1259 OID 99777)
-- Name: tsgrtestados; Type: TABLE; Schema: sgrt; Owner: suite
--

CREATE TABLE sgrt.tsgrtestados (
    cod_estadorep integer DEFAULT nextval('sgrt.seq_estadorep'::regclass) NOT NULL,
    des_nbestado character varying(60) NOT NULL
);


ALTER TABLE sgrt.tsgrtestados OWNER TO suite;

--
-- TOC entry 353 (class 1259 OID 99781)
-- Name: tsgrtfaq; Type: TABLE; Schema: sgrt; Owner: suite
--

CREATE TABLE sgrt.tsgrtfaq (
    cod_faq integer DEFAULT nextval('sgrt.seq_faq'::regclass) NOT NULL,
    cod_categoriafaq integer NOT NULL,
    des_pregunta character varying(255) NOT NULL,
    cnu_activo smallint NOT NULL,
    des_respuesta character varying(255) NOT NULL,
    des_notasint character varying(255) NOT NULL,
    fec_ultactualizacion timestamp without time zone NOT NULL,
    fec_creacion timestamp without time zone NOT NULL,
    cod_creadopor integer NOT NULL,
    cod_ultactualizacionpor integer NOT NULL
);


ALTER TABLE sgrt.tsgrtfaq OWNER TO suite;

--
-- TOC entry 354 (class 1259 OID 99788)
-- Name: tsgrtgrupo; Type: TABLE; Schema: sgrt; Owner: suite
--

CREATE TABLE sgrt.tsgrtgrupo (
    cod_grupo integer DEFAULT nextval('sgrt.seq_grupo'::regclass) NOT NULL,
    cnu_activo smallint NOT NULL,
    des_nombre character varying(50) NOT NULL,
    cnu_crear smallint NOT NULL,
    cnu_editar smallint NOT NULL,
    cnu_borrar smallint NOT NULL,
    cnu_cerrar smallint NOT NULL,
    cnu_transferir smallint NOT NULL,
    cnu_prohibir smallint NOT NULL,
    cnu_administrar smallint NOT NULL,
    cod_sistemasuite integer DEFAULT 1 NOT NULL,
    fec_ultactualizacion timestamp without time zone,
    cod_ultactualizacionpor integer NOT NULL,
    cod_creadopor integer NOT NULL,
    fec_creacion timestamp without time zone
);


ALTER TABLE sgrt.tsgrtgrupo OWNER TO suite;

--
-- TOC entry 355 (class 1259 OID 99793)
-- Name: tsgrtinvitados; Type: TABLE; Schema: sgrt; Owner: suite
--

CREATE TABLE sgrt.tsgrtinvitados (
    cod_invitado integer DEFAULT nextval('sgrt.seq_invitado'::regclass) NOT NULL,
    cod_reunion integer NOT NULL,
    des_nombre character varying,
    des_correo character varying,
    cnu_invitacionenv smallint NOT NULL,
    cnu_asiste smallint NOT NULL,
    cod_empleado integer,
    des_empresa character varying
);


ALTER TABLE sgrt.tsgrtinvitados OWNER TO suite;

--
-- TOC entry 356 (class 1259 OID 99800)
-- Name: tsgrtlugares; Type: TABLE; Schema: sgrt; Owner: suite
--

CREATE TABLE sgrt.tsgrtlugares (
    cod_lugar integer DEFAULT nextval('sgrt.seq_lugar'::regclass) NOT NULL,
    des_nombre character varying(60) NOT NULL,
    cod_ciudad integer NOT NULL
);


ALTER TABLE sgrt.tsgrtlugares OWNER TO suite;

--
-- TOC entry 357 (class 1259 OID 99804)
-- Name: tsgrtmsjticket; Type: TABLE; Schema: sgrt; Owner: suite
--

CREATE TABLE sgrt.tsgrtmsjticket (
    cod_mensaje integer DEFAULT nextval('sgrt.seq_mensaje'::regclass) NOT NULL,
    cod_ticket integer NOT NULL,
    cod_usuario integer DEFAULT 0 NOT NULL,
    des_mensaje text NOT NULL,
    cod_fuente character varying(16) DEFAULT NULL::character varying,
    cod_sistemasuite integer DEFAULT 1 NOT NULL,
    fec_ultactualizacion timestamp without time zone,
    cod_ultactualizacionpor integer NOT NULL,
    cod_creadopor integer NOT NULL,
    fec_creacion timestamp without time zone
);


ALTER TABLE sgrt.tsgrtmsjticket OWNER TO suite;

--
-- TOC entry 358 (class 1259 OID 99814)
-- Name: tsgrtnota; Type: TABLE; Schema: sgrt; Owner: suite
--

CREATE TABLE sgrt.tsgrtnota (
    cod_nota integer DEFAULT nextval('sgrt.seq_nota'::regclass) NOT NULL,
    cod_ticket integer NOT NULL,
    cod_empleado integer NOT NULL,
    des_fuente character varying(32) NOT NULL,
    des_titulo character varying(255) DEFAULT 'Nota INTEGERerna Generica'::character varying NOT NULL,
    des_nota text NOT NULL,
    cod_sistemasuite integer NOT NULL,
    fec_ultactualizacion timestamp without time zone,
    cod_ultactualizacionpor integer NOT NULL,
    cod_creadopor integer NOT NULL,
    fec_creacion timestamp without time zone
);


ALTER TABLE sgrt.tsgrtnota OWNER TO suite;

--
-- TOC entry 359 (class 1259 OID 99822)
-- Name: tsgrtplantillacorreos; Type: TABLE; Schema: sgrt; Owner: suite
--

CREATE TABLE sgrt.tsgrtplantillacorreos (
    cod_plantillacorreo integer DEFAULT nextval('sgrt.seq_plantillacorreo'::regclass) NOT NULL,
    des_nombre character varying(32) NOT NULL,
    des_notas text,
    cod_tipodestinario sgrt.destinatario DEFAULT 'USR'::sgrt.destinatario,
    cod_sistemasuite integer NOT NULL,
    fec_ultactualizacion timestamp without time zone,
    cod_ultactualizacionpor integer NOT NULL,
    cod_creadopor integer NOT NULL,
    fec_creacion timestamp without time zone,
    des_asunto character varying(45),
    des_cuerpo character varying(255)
);


ALTER TABLE sgrt.tsgrtplantillacorreos OWNER TO suite;

--
-- TOC entry 360 (class 1259 OID 99830)
-- Name: tsgrtprioridad; Type: TABLE; Schema: sgrt; Owner: suite
--

CREATE TABLE sgrt.tsgrtprioridad (
    cod_prioridad integer DEFAULT nextval('sgrt.seq_prioridad'::regclass) NOT NULL,
    des_nombre character varying(60) NOT NULL,
    des_descripcion character varying(30) NOT NULL,
    cod_color character varying(7) NOT NULL,
    cnu_valprioridad smallint NOT NULL,
    cnu_publica smallint NOT NULL,
    cod_sistemasuite integer NOT NULL,
    fec_ultactualizacion timestamp without time zone,
    cod_ultactualizacionpor integer NOT NULL,
    cod_creadopor integer NOT NULL,
    fec_creacion timestamp without time zone
);


ALTER TABLE sgrt.tsgrtprioridad OWNER TO suite;

--
-- TOC entry 361 (class 1259 OID 99834)
-- Name: tsgrtresppredefinida; Type: TABLE; Schema: sgrt; Owner: suite
--

CREATE TABLE sgrt.tsgrtresppredefinida (
    cod_respuesta integer DEFAULT nextval('sgrt.seq_resp'::regclass) NOT NULL,
    cod_depto integer NOT NULL,
    cnu_activo smallint NOT NULL,
    des_titulo character varying(125) NOT NULL,
    des_respuesta text NOT NULL,
    cod_sistemasuite integer NOT NULL,
    fec_ultactualizacion timestamp without time zone,
    cod_ultactualizacionpor integer NOT NULL,
    cod_creadopor integer NOT NULL,
    fec_creacion timestamp without time zone
);


ALTER TABLE sgrt.tsgrtresppredefinida OWNER TO suite;

--
-- TOC entry 362 (class 1259 OID 99841)
-- Name: tsgrtrespuesta; Type: TABLE; Schema: sgrt; Owner: suite
--

CREATE TABLE sgrt.tsgrtrespuesta (
    cod_respuesta integer DEFAULT nextval('sgrt.seq_respuesta'::regclass) NOT NULL,
    cod_mensaje integer NOT NULL,
    cod_ticket integer NOT NULL,
    cod_empleado integer NOT NULL,
    des_respuesta text NOT NULL,
    cod_sistemasuite integer NOT NULL,
    fec_ultactualizacion timestamp without time zone,
    cod_ultactualizacionpor integer NOT NULL,
    cod_creadopor integer NOT NULL,
    fec_creacion timestamp without time zone
);


ALTER TABLE sgrt.tsgrtrespuesta OWNER TO suite;

--
-- TOC entry 363 (class 1259 OID 99848)
-- Name: tsgrtreuniones; Type: TABLE; Schema: sgrt; Owner: suite
--

CREATE TABLE sgrt.tsgrtreuniones (
    cod_reunion integer DEFAULT nextval('sgrt.seq_reunion'::regclass) NOT NULL,
    des_nombre character varying(45) NOT NULL,
    fec_fecha date NOT NULL,
    des_objetivo character varying(700) NOT NULL,
    cod_lugar integer NOT NULL,
    cod_responsable integer NOT NULL,
    cod_proximareunion integer,
    cod_creadorreunion integer NOT NULL,
    tim_duracion time without time zone,
    tim_hora time without time zone
);


ALTER TABLE sgrt.tsgrtreuniones OWNER TO suite;

--
-- TOC entry 364 (class 1259 OID 99855)
-- Name: tsgrtservicios; Type: TABLE; Schema: sgrt; Owner: suite
--

CREATE TABLE sgrt.tsgrtservicios (
    cod_servicio integer DEFAULT nextval('sgrt.seq_servicio'::regclass) NOT NULL,
    des_nombre_servicio character varying(45) NOT NULL,
    des_descripcion character varying(100) NOT NULL,
    fec_contratacion date NOT NULL
);


ALTER TABLE sgrt.tsgrtservicios OWNER TO suite;

--
-- TOC entry 365 (class 1259 OID 99859)
-- Name: tsgrtsolicitudservicios; Type: TABLE; Schema: sgrt; Owner: suite
--

CREATE TABLE sgrt.tsgrtsolicitudservicios (
    cod_solicitud integer DEFAULT nextval('sgrt.seq_solicitud'::regclass) NOT NULL,
    cod_empleado integer NOT NULL,
    cod_ticket integer NOT NULL,
    cod_servicio integer NOT NULL
);


ALTER TABLE sgrt.tsgrtsolicitudservicios OWNER TO suite;

--
-- TOC entry 366 (class 1259 OID 99863)
-- Name: tsgrtticket; Type: TABLE; Schema: sgrt; Owner: suite
--

CREATE TABLE sgrt.tsgrtticket (
    cod_ticket integer DEFAULT nextval('sgrt.seq_ticket'::regclass) NOT NULL,
    des_folio character varying(45) NOT NULL,
    cod_reunion integer,
    cod_acuerdo integer,
    cod_responsable integer,
    cod_validador integer,
    cod_depto integer NOT NULL,
    cod_prioridad smallint NOT NULL,
    cod_topico integer NOT NULL,
    cod_empleado integer NOT NULL,
    des_correo character varying(50) NOT NULL,
    des_nombre character varying(50) NOT NULL,
    des_tema character varying(64) DEFAULT '[Sin Asunto]'::character varying NOT NULL,
    des_temaayuda character varying(255) DEFAULT NULL::character varying,
    cod_telefono character varying(16) DEFAULT NULL::character varying,
    cod_extension character varying(8) DEFAULT NULL::character varying,
    cod_estadot sgrt.edoticket DEFAULT 'Abierto'::sgrt.edoticket NOT NULL,
    cod_origent sgrt.origencontac DEFAULT 'Otro'::sgrt.origencontac NOT NULL,
    cnu_expirado smallint NOT NULL,
    cnu_atendido smallint NOT NULL,
    fec_exp timestamp without time zone,
    fec_reap timestamp without time zone,
    fec_cierre timestamp without time zone,
    fec_ultimomsg timestamp without time zone,
    fec_ultimaresp timestamp without time zone,
    cod_sistemasuite integer DEFAULT 1 NOT NULL,
    fec_ultactualizacion timestamp without time zone,
    cod_ultactualizacionpor integer NOT NULL,
    cod_creadopor integer NOT NULL,
    fec_creacion timestamp without time zone,
    cod_ejecutor integer
);


ALTER TABLE sgrt.tsgrtticket OWNER TO suite;

--
-- TOC entry 405 (class 1259 OID 102651)
-- Name: seq_aceptaciones; Type: SEQUENCE; Schema: sisat; Owner: suite
--

CREATE SEQUENCE sisat.seq_aceptaciones
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sisat.seq_aceptaciones OWNER TO suite;

--
-- TOC entry 406 (class 1259 OID 102653)
-- Name: seq_asignaciones; Type: SEQUENCE; Schema: sisat; Owner: suite
--

CREATE SEQUENCE sisat.seq_asignaciones
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sisat.seq_asignaciones OWNER TO suite;

--
-- TOC entry 407 (class 1259 OID 102655)
-- Name: seq_candidatos; Type: SEQUENCE; Schema: sisat; Owner: suite
--

CREATE SEQUENCE sisat.seq_candidatos
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sisat.seq_candidatos OWNER TO suite;

--
-- TOC entry 408 (class 1259 OID 102657)
-- Name: seq_cartaasignaciones; Type: SEQUENCE; Schema: sisat; Owner: suite
--

CREATE SEQUENCE sisat.seq_cartaasignaciones
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sisat.seq_cartaasignaciones OWNER TO suite;

--
-- TOC entry 409 (class 1259 OID 102659)
-- Name: seq_comentcartaasignacion; Type: SEQUENCE; Schema: sisat; Owner: suite
--

CREATE SEQUENCE sisat.seq_comentcartaasignacion
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sisat.seq_comentcartaasignacion OWNER TO suite;

--
-- TOC entry 410 (class 1259 OID 102661)
-- Name: seq_comentcosteo; Type: SEQUENCE; Schema: sisat; Owner: suite
--

CREATE SEQUENCE sisat.seq_comentcosteo
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sisat.seq_comentcosteo OWNER TO suite;

--
-- TOC entry 411 (class 1259 OID 102663)
-- Name: seq_comententrevista; Type: SEQUENCE; Schema: sisat; Owner: suite
--

CREATE SEQUENCE sisat.seq_comententrevista
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sisat.seq_comententrevista OWNER TO suite;

--
-- TOC entry 412 (class 1259 OID 102665)
-- Name: seq_comentvacantes; Type: SEQUENCE; Schema: sisat; Owner: suite
--

CREATE SEQUENCE sisat.seq_comentvacantes
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sisat.seq_comentvacantes OWNER TO suite;

--
-- TOC entry 413 (class 1259 OID 102667)
-- Name: seq_contrataciones; Type: SEQUENCE; Schema: sisat; Owner: suite
--

CREATE SEQUENCE sisat.seq_contrataciones
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sisat.seq_contrataciones OWNER TO suite;

--
-- TOC entry 414 (class 1259 OID 102669)
-- Name: seq_cotizaciones; Type: SEQUENCE; Schema: sisat; Owner: suite
--

CREATE SEQUENCE sisat.seq_cotizaciones
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sisat.seq_cotizaciones OWNER TO suite;

--
-- TOC entry 415 (class 1259 OID 102671)
-- Name: seq_cursos; Type: SEQUENCE; Schema: sisat; Owner: suite
--

CREATE SEQUENCE sisat.seq_cursos
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sisat.seq_cursos OWNER TO suite;

--
-- TOC entry 416 (class 1259 OID 102673)
-- Name: seq_entrevistas; Type: SEQUENCE; Schema: sisat; Owner: suite
--

CREATE SEQUENCE sisat.seq_entrevistas
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sisat.seq_entrevistas OWNER TO suite;

--
-- TOC entry 417 (class 1259 OID 102675)
-- Name: seq_envios; Type: SEQUENCE; Schema: sisat; Owner: suite
--

CREATE SEQUENCE sisat.seq_envios
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sisat.seq_envios OWNER TO suite;

--
-- TOC entry 418 (class 1259 OID 102677)
-- Name: seq_escolaridad; Type: SEQUENCE; Schema: sisat; Owner: suite
--

CREATE SEQUENCE sisat.seq_escolaridad
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sisat.seq_escolaridad OWNER TO suite;

--
-- TOC entry 419 (class 1259 OID 102679)
-- Name: seq_experiencias; Type: SEQUENCE; Schema: sisat; Owner: suite
--

CREATE SEQUENCE sisat.seq_experiencias
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sisat.seq_experiencias OWNER TO suite;

--
-- TOC entry 420 (class 1259 OID 102681)
-- Name: seq_firmareqper; Type: SEQUENCE; Schema: sisat; Owner: suite
--

CREATE SEQUENCE sisat.seq_firmareqper
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sisat.seq_firmareqper OWNER TO suite;

--
-- TOC entry 421 (class 1259 OID 102683)
-- Name: seq_firmas; Type: SEQUENCE; Schema: sisat; Owner: suite
--

CREATE SEQUENCE sisat.seq_firmas
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sisat.seq_firmas OWNER TO suite;

--
-- TOC entry 422 (class 1259 OID 102685)
-- Name: seq_habilidades; Type: SEQUENCE; Schema: sisat; Owner: suite
--

CREATE SEQUENCE sisat.seq_habilidades
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sisat.seq_habilidades OWNER TO suite;

--
-- TOC entry 423 (class 1259 OID 102687)
-- Name: seq_idiomas; Type: SEQUENCE; Schema: sisat; Owner: suite
--

CREATE SEQUENCE sisat.seq_idiomas
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sisat.seq_idiomas OWNER TO suite;

--
-- TOC entry 424 (class 1259 OID 102689)
-- Name: seq_ordenservicios; Type: SEQUENCE; Schema: sisat; Owner: suite
--

CREATE SEQUENCE sisat.seq_ordenservicios
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sisat.seq_ordenservicios OWNER TO suite;

--
-- TOC entry 425 (class 1259 OID 102691)
-- Name: seq_prospectos; Type: SEQUENCE; Schema: sisat; Owner: suite
--

CREATE SEQUENCE sisat.seq_prospectos
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sisat.seq_prospectos OWNER TO suite;

--
-- TOC entry 426 (class 1259 OID 102693)
-- Name: seq_proyectos; Type: SEQUENCE; Schema: sisat; Owner: suite
--

CREATE SEQUENCE sisat.seq_proyectos
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sisat.seq_proyectos OWNER TO suite;

--
-- TOC entry 427 (class 1259 OID 102695)
-- Name: seq_vacantes; Type: SEQUENCE; Schema: sisat; Owner: suite
--

CREATE SEQUENCE sisat.seq_vacantes
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sisat.seq_vacantes OWNER TO suite;

--
-- TOC entry 428 (class 1259 OID 102697)
-- Name: tsisatappservices; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisatappservices (
    cod_appservice integer NOT NULL,
    des_appservice character varying(30) NOT NULL
);


ALTER TABLE sisat.tsisatappservices OWNER TO suite;

--
-- TOC entry 429 (class 1259 OID 102700)
-- Name: tsisatarquitecturas; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisatarquitecturas (
    cod_arquitectura integer NOT NULL,
    des_arquitectura character varying(30) NOT NULL
);


ALTER TABLE sisat.tsisatarquitecturas OWNER TO suite;

--
-- TOC entry 430 (class 1259 OID 102703)
-- Name: tsisatasignaciones; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisatasignaciones (
    cod_asignacion integer DEFAULT nextval('sisat.seq_asignaciones'::regclass) NOT NULL,
    cod_prospecto integer NOT NULL,
    cod_perfil integer NOT NULL,
    cod_cliente integer NOT NULL,
    des_correocte character varying(40) NOT NULL,
    cod_telefonocte character varying(16) NOT NULL,
    des_direccioncte character varying(200),
    cod_empleado integer,
    aud_cod_creadopor integer NOT NULL,
    aud_cod_modificadopor integer,
    aud_fec_creacion date NOT NULL,
    aud_fec_modificacion date
);


ALTER TABLE sisat.tsisatasignaciones OWNER TO suite;

--
-- TOC entry 431 (class 1259 OID 102707)
-- Name: tsisatcandidatos; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisatcandidatos (
    cod_candidato integer DEFAULT nextval('sisat.seq_candidatos'::regclass) NOT NULL,
    des_nombre character varying(50) NOT NULL,
    cod_perfil integer NOT NULL,
    imp_sueldo numeric(6,2),
    imp_sueldodia numeric(6,2),
    imp_nominaimss numeric(6,2),
    imp_honorarios numeric(6,2),
    imp_cargasocial numeric(6,2),
    imp_prestaciones numeric(6,2),
    imp_viaticos numeric(6,2),
    imp_subtotalcandidato numeric(6,2),
    imp_costoadmin numeric(6,2),
    cnu_financiamiento smallint,
    imp_isr numeric(6,2),
    imp_financiamiento numeric(6,2),
    imp_adicionales numeric(6,2),
    imp_subtotaladmin1 numeric(6,2),
    imp_comisiones numeric(6,2),
    imp_otrosgastos numeric(6,2),
    imp_subtotaladmin2 numeric(6,2),
    imp_total numeric(6,2),
    imp_iva numeric(6,2),
    por_utilidad numeric(4,2),
    imp_utilidad numeric(6,2),
    imp_tarifa numeric(6,2),
    aud_fecha_creacion date NOT NULL,
    aud_fecha_modificacion date,
    aud_cod_creadopor integer NOT NULL,
    aud_cod_modificadopor integer,
    imp_hospedaje numeric(6,2),
    imp_alimentacion numeric(6,2),
    imp_transporte numeric(6,2),
    imp_incentivos numeric(6,2),
    imp_eqcomputo numeric(6,2)
);


ALTER TABLE sisat.tsisatcandidatos OWNER TO suite;

--
-- TOC entry 432 (class 1259 OID 102711)
-- Name: tsisatcartaaceptacion; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisatcartaaceptacion (
    cod_aceptacion integer DEFAULT nextval('sisat.seq_aceptaciones'::regclass) NOT NULL,
    des_objetivo character varying(200) NOT NULL,
    txt_oferta text,
    des_esquema character varying(30) NOT NULL,
    tim_jornada time without time zone,
    txt_especificaciones text,
    aud_cod_creadopor integer NOT NULL,
    aud_cod_modificadopor integer,
    aud_fec_creacion date NOT NULL,
    aud_fec_modificacion date
);


ALTER TABLE sisat.tsisatcartaaceptacion OWNER TO suite;

--
-- TOC entry 433 (class 1259 OID 102718)
-- Name: tsisatcartaasignacion; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisatcartaasignacion (
    cod_asignacion integer DEFAULT nextval('sisat.seq_cartaasignaciones'::regclass) NOT NULL,
    cod_prospecto integer NOT NULL,
    cod_perfil integer NOT NULL,
    des_actividades character varying(200),
    des_lugarsalida character varying(100),
    des_lugarllegada character varying(100),
    fec_salida date,
    fec_llegada date,
    cod_transporte character varying(20),
    des_lugarhopedaje character varying(60),
    fec_hospedaje date,
    des_computadora character varying(150),
    cod_telefono character varying(16),
    des_accesorios character varying(150),
    des_nbresponsable character varying(50),
    des_nbpuesto character varying(50),
    des_lugarresp character varying(100),
    cod_telefonoresp character varying(16),
    tim_horario time without time zone,
    fec_iniciocontra date,
    fec_terminocontra date,
    imp_sueldomensual numeric(6,2),
    imp_nominaimss numeric(6,2),
    imp_honorarios numeric(6,2),
    imp_otros numeric(6,2),
    cod_rfc character varying(13),
    des_razonsocial character varying(45),
    des_correo character varying(50),
    cod_cpostal integer,
    des_direccionfact character varying(200),
    cod_cliente integer NOT NULL,
    cod_gpy integer,
    cod_rhta integer,
    cod_ape integer,
    cod_rys integer,
    des_observacion character varying(200),
    aud_cod_creadopor integer NOT NULL,
    aud_cod_modificadopor integer,
    aud_fecha_creacion date NOT NULL,
    aud_fecha_modificacion date
);


ALTER TABLE sisat.tsisatcartaasignacion OWNER TO suite;

--
-- TOC entry 434 (class 1259 OID 102725)
-- Name: tsisatcomentcartaasignacion; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisatcomentcartaasignacion (
    cod_comentcartaasignacion integer DEFAULT nextval('sisat.seq_comentcartaasignacion'::regclass) NOT NULL,
    des_comentcartaasignacion character varying(300),
    bol_validacion boolean NOT NULL,
    aud_fecha_creacion date NOT NULL,
    aud_fecha_modificacion date,
    aud_cod_creadopor integer NOT NULL,
    aud_cod_modificadopor integer,
    cod_asignacion integer NOT NULL
);


ALTER TABLE sisat.tsisatcomentcartaasignacion OWNER TO suite;

--
-- TOC entry 435 (class 1259 OID 102729)
-- Name: tsisatcomentcosteo; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisatcomentcosteo (
    cod_comentcosteo integer DEFAULT nextval('sisat.seq_comentcosteo'::regclass) NOT NULL,
    des_comentcosteo character varying(300),
    bol_validacion boolean NOT NULL,
    aud_fecha_creacion date NOT NULL,
    aud_fecha_modificacion date,
    aud_cod_creadopor integer NOT NULL,
    aud_cod_modificadopor integer,
    cod_candidato integer NOT NULL
);


ALTER TABLE sisat.tsisatcomentcosteo OWNER TO suite;

--
-- TOC entry 436 (class 1259 OID 102733)
-- Name: tsisatcomententrevista; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisatcomententrevista (
    cod_comententrevista integer DEFAULT nextval('sisat.seq_comententrevista'::regclass) NOT NULL,
    des_comententrevista character varying(500),
    aud_fecha_creacion date NOT NULL,
    aud_fecha_modificacion date,
    aud_cod_creadopor integer NOT NULL,
    aud_cod_modificadopor integer,
    cod_entrevista integer NOT NULL
);


ALTER TABLE sisat.tsisatcomententrevista OWNER TO suite;

--
-- TOC entry 437 (class 1259 OID 102740)
-- Name: tsisatcomentvacantes; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisatcomentvacantes (
    cod_comentvacante integer DEFAULT nextval('sisat.seq_comentvacantes'::regclass) NOT NULL,
    des_comentvacante character varying(300),
    bol_validacion boolean NOT NULL,
    aud_fecha_creacion date NOT NULL,
    aud_fecha_modificacion date,
    aud_cod_creadopor integer NOT NULL,
    aud_cod_modificadopor integer,
    cod_vacante integer NOT NULL
);


ALTER TABLE sisat.tsisatcomentvacantes OWNER TO suite;

--
-- TOC entry 438 (class 1259 OID 102744)
-- Name: tsisatcontrataciones; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisatcontrataciones (
    cod_contratacion integer DEFAULT nextval('sisat.seq_contrataciones'::regclass) NOT NULL,
    fec_inicio date,
    fec_termino date,
    des_esquema character varying(30),
    cod_salarioestmin numeric(6,2),
    cod_salarioestmax numeric(6,2),
    tim_jornada character varying(20),
    aud_cod_creadopor integer NOT NULL,
    aud_cod_modificadopor integer,
    aud_fec_creacion date NOT NULL,
    aud_fec_modificacion date
);


ALTER TABLE sisat.tsisatcontrataciones OWNER TO suite;

--
-- TOC entry 439 (class 1259 OID 102748)
-- Name: tsisatcotizaciones; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisatcotizaciones (
    cod_cotizacion integer NOT NULL,
    cod_ciudad integer,
    cod_estado integer,
    fec_fecha date NOT NULL,
    "des_nbcontacto " character varying(50) NOT NULL,
    cod_puesto integer,
    des_compania character varying(50) NOT NULL,
    des_nbservicio character varying(50) NOT NULL,
    cnu_cantidad smallint NOT NULL,
    txt_concepto text NOT NULL,
    imp_inversionhr numeric(6,2) NOT NULL,
    txt_condicionescomer text NOT NULL,
    des_nbatentamente character varying(60) NOT NULL,
    des_correoatentamente character varying(50) NOT NULL,
    aud_cod_creadopor integer NOT NULL,
    aud_cod_modificadopor integer,
    aud_fec_creacion date NOT NULL,
    aud_fec_modificacion date
);


ALTER TABLE sisat.tsisatcotizaciones OWNER TO suite;

--
-- TOC entry 440 (class 1259 OID 102754)
-- Name: tsisatcursosycertificados; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisatcursosycertificados (
    cod_curso integer DEFAULT nextval('sisat.seq_cursos'::regclass) NOT NULL,
    cod_prospecto integer NOT NULL,
    des_curso character varying(100) NOT NULL,
    des_institucion character varying(70) NOT NULL,
    fec_termino date NOT NULL,
    fec_inicio date NOT NULL
);


ALTER TABLE sisat.tsisatcursosycertificados OWNER TO suite;

--
-- TOC entry 441 (class 1259 OID 102758)
-- Name: tsisatentrevistas; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisatentrevistas (
    cod_entrevista integer DEFAULT nextval('sisat.seq_entrevistas'::regclass) NOT NULL,
    des_nbentrevistador character varying(90) NOT NULL,
    des_puesto character varying(50) NOT NULL,
    des_correoent character varying(40) NOT NULL,
    cod_telefonoent character varying(16) NOT NULL,
    des_direccionent character varying(200),
    tim_horarioent time without time zone,
    fec_fechaent date NOT NULL,
    comentarios character varying(700) NOT NULL,
    cod_prospecto integer NOT NULL,
    aud_cod_creadopor integer NOT NULL,
    aud_cod_modificadopor integer,
    aud_fec_creacion date NOT NULL,
    aud_fec_modificacion date
);


ALTER TABLE sisat.tsisatentrevistas OWNER TO suite;

--
-- TOC entry 442 (class 1259 OID 102765)
-- Name: tsisatenviocorreos; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisatenviocorreos (
    cod_envio integer DEFAULT nextval('sisat.seq_envios'::regclass) NOT NULL,
    des_destinatario character varying(90) NOT NULL,
    des_asunto character varying(50) NOT NULL,
    des_mensaje character varying(200) NOT NULL,
    bin_adjunto bytea,
    aud_cod_creadopor integer NOT NULL,
    aud_cod_modificadopor integer,
    aud_fec_creacion date NOT NULL,
    aud_fec_modificacion date
);


ALTER TABLE sisat.tsisatenviocorreos OWNER TO suite;

--
-- TOC entry 443 (class 1259 OID 102772)
-- Name: tsisatescolaridad; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisatescolaridad (
    cod_escolaridad integer DEFAULT nextval('sisat.seq_escolaridad'::regclass) NOT NULL,
    cod_prospecto integer NOT NULL,
    des_escolaridad character varying(45) NOT NULL,
    des_escuela character varying(70) NOT NULL,
    fec_inicio date NOT NULL,
    fec_termino date NOT NULL,
    cod_estatus character varying(20)
);


ALTER TABLE sisat.tsisatescolaridad OWNER TO suite;

--
-- TOC entry 444 (class 1259 OID 102776)
-- Name: tsisatexperienciaslaborales; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisatexperienciaslaborales (
    cod_experiencia integer DEFAULT nextval('sisat.seq_experiencias'::regclass) NOT NULL,
    cod_prospecto integer NOT NULL,
    des_empresa character varying(50) NOT NULL,
    des_puesto character varying(40) NOT NULL,
    fec_inicio date,
    fec_termino date,
    des_ubicacion character varying(70),
    txt_funciones text,
    des_nbcliente character varying(70),
    des_proyecto character varying(70),
    txt_logros character varying(300),
    txt_tecnologiasemple text NOT NULL
);


ALTER TABLE sisat.tsisatexperienciaslaborales OWNER TO suite;

--
-- TOC entry 445 (class 1259 OID 102783)
-- Name: tsisatfirmareqper; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisatfirmareqper (
    cod_firma integer DEFAULT nextval('sisat.seq_firmas'::regclass) NOT NULL,
    cod_solicita integer,
    cod_puestosolicita integer,
    cod_autoriza integer,
    cod_puestoautoriza integer,
    cod_vacante integer
);


ALTER TABLE sisat.tsisatfirmareqper OWNER TO suite;

--
-- TOC entry 446 (class 1259 OID 102787)
-- Name: tsisatframeworks; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisatframeworks (
    cod_framework integer NOT NULL,
    des_framework character varying(30) NOT NULL
);


ALTER TABLE sisat.tsisatframeworks OWNER TO suite;

--
-- TOC entry 447 (class 1259 OID 102790)
-- Name: tsisathabilidades; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisathabilidades (
    cod_habilidad integer DEFAULT nextval('sisat.seq_habilidades'::regclass) NOT NULL,
    cod_prospecto integer NOT NULL,
    des_habilidad character varying(50),
    des_dominio character varying(50)
);


ALTER TABLE sisat.tsisathabilidades OWNER TO suite;

--
-- TOC entry 448 (class 1259 OID 102794)
-- Name: tsisatherramientas; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisatherramientas (
    cod_herramientas integer NOT NULL,
    json_tecnologia json,
    cod_prospecto integer NOT NULL,
    des_nivel character varying(30) NOT NULL,
    cnu_experiencia numeric(5,1) NOT NULL
);


ALTER TABLE sisat.tsisatherramientas OWNER TO suite;

--
-- TOC entry 449 (class 1259 OID 102800)
-- Name: tsisatherramientas_cod_herramientas_seq; Type: SEQUENCE; Schema: sisat; Owner: suite
--

CREATE SEQUENCE sisat.tsisatherramientas_cod_herramientas_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sisat.tsisatherramientas_cod_herramientas_seq OWNER TO suite;

--
-- TOC entry 5175 (class 0 OID 0)
-- Dependencies: 449
-- Name: tsisatherramientas_cod_herramientas_seq; Type: SEQUENCE OWNED BY; Schema: sisat; Owner: suite
--

ALTER SEQUENCE sisat.tsisatherramientas_cod_herramientas_seq OWNED BY sisat.tsisatherramientas.cod_herramientas;


--
-- TOC entry 450 (class 1259 OID 102802)
-- Name: tsisatides; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisatides (
    cod_ide integer NOT NULL,
    des_ide character varying(30) NOT NULL
);


ALTER TABLE sisat.tsisatides OWNER TO suite;

--
-- TOC entry 451 (class 1259 OID 102805)
-- Name: tsisatidiomas; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisatidiomas (
    cod_idioma integer DEFAULT nextval('sisat.seq_idiomas'::regclass) NOT NULL,
    cod_nbidioma character varying(20) NOT NULL
);


ALTER TABLE sisat.tsisatidiomas OWNER TO suite;

--
-- TOC entry 452 (class 1259 OID 102809)
-- Name: tsisatlenguajes; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisatlenguajes (
    cod_lenguaje integer NOT NULL,
    des_lenguaje character varying(30) NOT NULL
);


ALTER TABLE sisat.tsisatlenguajes OWNER TO suite;

--
-- TOC entry 453 (class 1259 OID 102812)
-- Name: tsisatmaquetados; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisatmaquetados (
    cod_maquetado integer NOT NULL,
    des_maquetado character varying(50) NOT NULL
);


ALTER TABLE sisat.tsisatmaquetados OWNER TO suite;

--
-- TOC entry 454 (class 1259 OID 102815)
-- Name: tsisatmetodologias; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisatmetodologias (
    cod_metodologia integer NOT NULL,
    des_metodologia character varying(30) NOT NULL
);


ALTER TABLE sisat.tsisatmetodologias OWNER TO suite;

--
-- TOC entry 455 (class 1259 OID 102818)
-- Name: tsisatmodelados; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisatmodelados (
    cod_modelado integer NOT NULL,
    des_modelado character varying(30) NOT NULL
);


ALTER TABLE sisat.tsisatmodelados OWNER TO suite;

--
-- TOC entry 456 (class 1259 OID 102821)
-- Name: tsisatordenservicio; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisatordenservicio (
    cod_ordenservicio integer DEFAULT nextval('sisat.seq_ordenservicios'::regclass) NOT NULL,
    cod_estadorep integer NOT NULL,
    cod_ciudad integer NOT NULL,
    fec_fecha date NOT NULL,
    cod_puestocte character varying,
    des_nbservicio character varying(60),
    cnu_cantidad smallint,
    txt_concepto text,
    imp_inversionhr numeric(6,2),
    txt_condicionescomer text,
    des_ubcnconsultor character varying(100),
    fec_finservicio date,
    cod_gpy integer NOT NULL,
    cod_cliente integer NOT NULL,
    aud_cod_creadopor integer NOT NULL,
    aud_cod_modificadopor integer,
    aud_fec_creacion date NOT NULL,
    aud_fec_modificacion date
);


ALTER TABLE sisat.tsisatordenservicio OWNER TO suite;

--
-- TOC entry 457 (class 1259 OID 102828)
-- Name: tsisatpatrones; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisatpatrones (
    cod_patron integer NOT NULL,
    des_patron character varying(30) NOT NULL
);


ALTER TABLE sisat.tsisatpatrones OWNER TO suite;

--
-- TOC entry 458 (class 1259 OID 102831)
-- Name: tsisatprospectos; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisatprospectos (
    cod_prospecto integer DEFAULT nextval('sisat.seq_prospectos'::regclass) NOT NULL,
    des_nombre character varying(45) NOT NULL,
    des_nombres character varying(60),
    des_appaterno character varying(40) NOT NULL,
    des_apmaterno character varying(40),
    des_lugarnacimiento character varying(50) NOT NULL,
    fec_nacimiento date NOT NULL,
    cod_edad integer NOT NULL,
    cod_edocivil character varying(15) NOT NULL,
    des_nbpadre character varying(70),
    des_nbmadre character varying(70),
    cod_numhermanos integer,
    des_nbcalle character varying(60),
    cod_numcasa integer,
    des_colonia character varying(60),
    des_localidad character varying(60),
    des_municipio character varying(60),
    des_estado character varying(60),
    cod_cpostal integer,
    cod_tiposangre character varying(5),
    des_emailmbn character varying(40),
    des_emailpersonal character varying(40),
    des_pasatiempo character varying(200),
    cod_telefonocasa character varying(16) DEFAULT NULL::character varying,
    cod_telefonomovil character varying(16) DEFAULT NULL::character varying,
    cod_rfc character varying(13),
    cod_nss character varying(20),
    cod_curp character varying(18) NOT NULL,
    des_nacionalidad character varying(30),
    cod_administrador integer,
    fec_fechacoment date,
    txt_comentarios text,
    bin_foto bytea,
    cod_tipofoto character varying(30),
    cod_extensionfoto character varying(5),
    des_puestovacante character varying(50),
    anio_experiencia integer,
    aud_cod_creadopor integer NOT NULL,
    aud_cod_modificadopor integer,
    aud_fec_creacion date NOT NULL,
    aud_fec_modifiacion date
);


ALTER TABLE sisat.tsisatprospectos OWNER TO suite;

--
-- TOC entry 459 (class 1259 OID 102840)
-- Name: tsisatprospectos_idiomas; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisatprospectos_idiomas (
    cod_pros_idoma integer NOT NULL,
    cod_prospecto integer,
    cod_idioma integer,
    cod_nivel character varying(20),
    des_certificado character varying(40)
);


ALTER TABLE sisat.tsisatprospectos_idiomas OWNER TO suite;

--
-- TOC entry 460 (class 1259 OID 102843)
-- Name: tsisatprotocolos; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisatprotocolos (
    cod_protocolo integer NOT NULL,
    des_protocolo character varying(30) NOT NULL
);


ALTER TABLE sisat.tsisatprotocolos OWNER TO suite;

--
-- TOC entry 461 (class 1259 OID 102846)
-- Name: tsisatproyectos; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisatproyectos (
    cod_proyecto integer DEFAULT nextval('sisat.seq_proyectos'::regclass) NOT NULL,
    cod_prospecto integer NOT NULL,
    cod_perfil integer NOT NULL,
    des_nbcliente character varying(50) NOT NULL,
    des_nbresponsable character varying(50) NOT NULL,
    des_correo character varying(50) NOT NULL,
    cod_telefono character varying(16) NOT NULL,
    des_direccion character varying(200) NOT NULL,
    aud_cod_creadopor integer NOT NULL,
    aud_cod_modificadopor integer,
    aud_fec_creacion date NOT NULL,
    aud_fec_modificacion date
);


ALTER TABLE sisat.tsisatproyectos OWNER TO suite;

--
-- TOC entry 462 (class 1259 OID 102850)
-- Name: tsisatqa; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisatqa (
    cod_qa integer NOT NULL,
    des_qa character varying(30) NOT NULL
);


ALTER TABLE sisat.tsisatqa OWNER TO suite;

--
-- TOC entry 463 (class 1259 OID 102853)
-- Name: tsisatrepositoriolibrerias; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisatrepositoriolibrerias (
    cod_repositoriolibreria integer NOT NULL,
    des_repositoriolibreria character varying(30) NOT NULL
);


ALTER TABLE sisat.tsisatrepositoriolibrerias OWNER TO suite;

--
-- TOC entry 464 (class 1259 OID 102856)
-- Name: tsisatrepositorios; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisatrepositorios (
    cod_repositorio integer NOT NULL,
    des_repositorio character varying(30) NOT NULL
);


ALTER TABLE sisat.tsisatrepositorios OWNER TO suite;

--
-- TOC entry 465 (class 1259 OID 102859)
-- Name: tsisatsgbd; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisatsgbd (
    cod_sgbd integer NOT NULL,
    des_sgbd character varying(30) NOT NULL
);


ALTER TABLE sisat.tsisatsgbd OWNER TO suite;

--
-- TOC entry 466 (class 1259 OID 102862)
-- Name: tsisatso; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisatso (
    cod_so integer NOT NULL,
    des_so character varying(30) NOT NULL
);


ALTER TABLE sisat.tsisatso OWNER TO suite;

--
-- TOC entry 467 (class 1259 OID 102865)
-- Name: tsisatvacantes; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisatvacantes (
    cod_vacante integer DEFAULT nextval('sisat.seq_vacantes'::regclass) NOT NULL,
    des_rqvacante character varying(200) NOT NULL,
    cnu_anexperiencia smallint,
    txt_experiencia text,
    des_escolaridad character varying(50),
    txt_herramientas text,
    txt_habilidades text,
    des_lugartrabajo character varying(100),
    imp_sueldo numeric(6,2),
    cod_idioma integer,
    sexo character varying(11) NOT NULL,
    fec_solicitud date NOT NULL,
    fec_entrega date,
    cod_cliente integer NOT NULL,
    aud_cod_creadopor integer NOT NULL,
    aud_cod_modificadopor integer,
    aud_fec_creacion date NOT NULL,
    aud_fec_modificacion date,
    cod_contratacion integer NOT NULL,
    statusvacante boolean,
    txt_conocimientostecno text,
    cod_nivelidioma character varying(20)
);


ALTER TABLE sisat.tsisatvacantes OWNER TO suite;

--
-- TOC entry 3839 (class 2604 OID 102872)
-- Name: tsisatherramientas cod_herramientas; Type: DEFAULT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatherramientas ALTER COLUMN cod_herramientas SET DEFAULT nextval('sisat.tsisatherramientas_cod_herramientas_seq'::regclass);


--
-- TOC entry 4630 (class 0 OID 99192)
-- Dependencies: 218
-- Data for Name: rtsueldobase; Type: TABLE DATA; Schema: public; Owner: suite
--

INSERT INTO public.rtsueldobase (imp_honorarios) VALUES (213.00);
INSERT INTO public.rtsueldobase (imp_honorarios) VALUES (213.00);


--
-- TOC entry 4634 (class 0 OID 99201)
-- Dependencies: 222
-- Data for Name: tsgcosistemas; Type: TABLE DATA; Schema: sgco; Owner: suite
--

INSERT INTO sgco.tsgcosistemas (cod_sistema, des_nbsistema, des_descripcion) VALUES (1, 'SGRHAT', 'Sistema de Recursos Humanos y Ambiente de Trabajo');
INSERT INTO sgco.tsgcosistemas (cod_sistema, des_nbsistema, des_descripcion) VALUES (2, 'SISAT', 'Sistema de Selección y Administración de Talentos');
INSERT INTO sgco.tsgcosistemas (cod_sistema, des_nbsistema, des_descripcion) VALUES (3, 'SGNOM', 'Sistema de Nómina');


--
-- TOC entry 4635 (class 0 OID 99204)
-- Dependencies: 223
-- Data for Name: tsgcotipousuario; Type: TABLE DATA; Schema: sgco; Owner: suite
--

INSERT INTO sgco.tsgcotipousuario (cod_tipousuario, cod_usuario, cod_sistema, cod_rol) VALUES (1, 1, 3, 1);
INSERT INTO sgco.tsgcotipousuario (cod_tipousuario, cod_usuario, cod_sistema, cod_rol) VALUES (3, 3, 3, 3);
INSERT INTO sgco.tsgcotipousuario (cod_tipousuario, cod_usuario, cod_sistema, cod_rol) VALUES (2, 2, 3, 2);
INSERT INTO sgco.tsgcotipousuario (cod_tipousuario, cod_usuario, cod_sistema, cod_rol) VALUES (4, 4, 3, 4);
INSERT INTO sgco.tsgcotipousuario (cod_tipousuario, cod_usuario, cod_sistema, cod_rol) VALUES (5, 5, 2, 1);


--
-- TOC entry 4636 (class 0 OID 99207)
-- Dependencies: 224
-- Data for Name: tsgcousuarios; Type: TABLE DATA; Schema: sgco; Owner: suite
--

INSERT INTO sgco.tsgcousuarios (cod_usuario, cod_empleado, des_correo, des_clave) VALUES (2, 11, 'adrian.suarez@gmail.com', '12345');
INSERT INTO sgco.tsgcousuarios (cod_usuario, cod_empleado, des_correo, des_clave) VALUES (3, 12, 'carlos.antonio@gmail.com', '12345');
INSERT INTO sgco.tsgcousuarios (cod_usuario, cod_empleado, des_correo, des_clave) VALUES (4, 13, 'angel.roano@gmail.com', '12345');
INSERT INTO sgco.tsgcousuarios (cod_usuario, cod_empleado, des_correo, des_clave) VALUES (1, 10, 'mateorj96@gmail.com', '12345');
INSERT INTO sgco.tsgcousuarios (cod_usuario, cod_empleado, des_correo, des_clave) VALUES (5, 14, 'antonioherrera@gmail.com', '12345');


--
-- TOC entry 4785 (class 0 OID 102002)
-- Dependencies: 373
-- Data for Name: tsgnomaguinaldo; Type: TABLE DATA; Schema: sgnom; Owner: suite
--



--
-- TOC entry 4786 (class 0 OID 102008)
-- Dependencies: 374
-- Data for Name: tsgnomargumento; Type: TABLE DATA; Schema: sgnom; Owner: suite
--

INSERT INTO sgnom.tsgnomargumento (cod_argumentoid, cod_nbargumento, cod_clavearg, imp_valorconst, des_funcionbd, cod_tipoargumento, bol_estatus, txt_descripcion, aud_codcreadopor, aud_codmodificadopor, aud_feccreacion, aud_fecmodificacion) VALUES (1, 'SUELDOBASE', 'SB', NULL, 'fn_sueldo_base', '0', true, 'obtiene sueldo base asd', 10, 13, '2019-07-01', '2019-08-26');
INSERT INTO sgnom.tsgnomargumento (cod_argumentoid, cod_nbargumento, cod_clavearg, imp_valorconst, des_funcionbd, cod_tipoargumento, bol_estatus, txt_descripcion, aud_codcreadopor, aud_codmodificadopor, aud_feccreacion, aud_fecmodificacion) VALUES (4, 'DIASLABORADOS', 'DL', NULL, 'fn_dias_laborados', '0', true, 'dias laborados', 13, 13, '2019-07-15', '2019-09-09');
INSERT INTO sgnom.tsgnomargumento (cod_argumentoid, cod_nbargumento, cod_clavearg, imp_valorconst, des_funcionbd, cod_tipoargumento, bol_estatus, txt_descripcion, aud_codcreadopor, aud_codmodificadopor, aud_feccreacion, aud_fecmodificacion) VALUES (11, 'AMBROSIO', 'P001', NULL, 'fn_sueldo_base', '0', false, 'ambro', 13, 13, '2019-09-30', '2019-09-30');
INSERT INTO sgnom.tsgnomargumento (cod_argumentoid, cod_nbargumento, cod_clavearg, imp_valorconst, des_funcionbd, cod_tipoargumento, bol_estatus, txt_descripcion, aud_codcreadopor, aud_codmodificadopor, aud_feccreacion, aud_fecmodificacion) VALUES (12, 'TORRES', 'TORRE', 45.00, NULL, '1', false, 'torres', 13, NULL, '2019-09-30', NULL);
INSERT INTO sgnom.tsgnomargumento (cod_argumentoid, cod_nbargumento, cod_clavearg, imp_valorconst, des_funcionbd, cod_tipoargumento, bol_estatus, txt_descripcion, aud_codcreadopor, aud_codmodificadopor, aud_feccreacion, aud_fecmodificacion) VALUES (13, 'DGDFGDFGSDG', 'FSD', 5.00, NULL, '1', false, 'dgsdgds', 13, NULL, '2019-09-30', NULL);
INSERT INTO sgnom.tsgnomargumento (cod_argumentoid, cod_nbargumento, cod_clavearg, imp_valorconst, des_funcionbd, cod_tipoargumento, bol_estatus, txt_descripcion, aud_codcreadopor, aud_codmodificadopor, aud_feccreacion, aud_fecmodificacion) VALUES (14, 'SFSA', 'SFS', 5.00, NULL, '1', false, 'sfafas', 13, NULL, '2019-09-30', NULL);
INSERT INTO sgnom.tsgnomargumento (cod_argumentoid, cod_nbargumento, cod_clavearg, imp_valorconst, des_funcionbd, cod_tipoargumento, bol_estatus, txt_descripcion, aud_codcreadopor, aud_codmodificadopor, aud_feccreacion, aud_fecmodificacion) VALUES (15, '1', '1', 1.00, NULL, '1', false, '1', 13, NULL, '2019-09-30', NULL);
INSERT INTO sgnom.tsgnomargumento (cod_argumentoid, cod_nbargumento, cod_clavearg, imp_valorconst, des_funcionbd, cod_tipoargumento, bol_estatus, txt_descripcion, aud_codcreadopor, aud_codmodificadopor, aud_feccreacion, aud_fecmodificacion) VALUES (16, '2', '2', 2.00, NULL, '1', false, '2', 13, NULL, '2019-09-30', NULL);
INSERT INTO sgnom.tsgnomargumento (cod_argumentoid, cod_nbargumento, cod_clavearg, imp_valorconst, des_funcionbd, cod_tipoargumento, bol_estatus, txt_descripcion, aud_codcreadopor, aud_codmodificadopor, aud_feccreacion, aud_fecmodificacion) VALUES (17, '3', '3', 3.00, NULL, '1', false, '3', 13, NULL, '2019-09-30', NULL);
INSERT INTO sgnom.tsgnomargumento (cod_argumentoid, cod_nbargumento, cod_clavearg, imp_valorconst, des_funcionbd, cod_tipoargumento, bol_estatus, txt_descripcion, aud_codcreadopor, aud_codmodificadopor, aud_feccreacion, aud_fecmodificacion) VALUES (26, '', '', NULL, NULL, NULL, false, '', 13, NULL, '2019-10-28', NULL);
INSERT INTO sgnom.tsgnomargumento (cod_argumentoid, cod_nbargumento, cod_clavearg, imp_valorconst, des_funcionbd, cod_tipoargumento, bol_estatus, txt_descripcion, aud_codcreadopor, aud_codmodificadopor, aud_feccreacion, aud_fecmodificacion) VALUES (25, '', '', NULL, NULL, NULL, false, '', 13, NULL, '2019-10-28', NULL);
INSERT INTO sgnom.tsgnomargumento (cod_argumentoid, cod_nbargumento, cod_clavearg, imp_valorconst, des_funcionbd, cod_tipoargumento, bol_estatus, txt_descripcion, aud_codcreadopor, aud_codmodificadopor, aud_feccreacion, aud_fecmodificacion) VALUES (24, '', '', NULL, NULL, NULL, false, '', 13, NULL, '2019-10-28', NULL);
INSERT INTO sgnom.tsgnomargumento (cod_argumentoid, cod_nbargumento, cod_clavearg, imp_valorconst, des_funcionbd, cod_tipoargumento, bol_estatus, txt_descripcion, aud_codcreadopor, aud_codmodificadopor, aud_feccreacion, aud_fecmodificacion) VALUES (23, '', '', NULL, NULL, NULL, false, '', 13, NULL, '2019-10-28', NULL);
INSERT INTO sgnom.tsgnomargumento (cod_argumentoid, cod_nbargumento, cod_clavearg, imp_valorconst, des_funcionbd, cod_tipoargumento, bol_estatus, txt_descripcion, aud_codcreadopor, aud_codmodificadopor, aud_feccreacion, aud_fecmodificacion) VALUES (22, '', '', NULL, NULL, NULL, false, '', 13, NULL, '2019-10-28', NULL);
INSERT INTO sgnom.tsgnomargumento (cod_argumentoid, cod_nbargumento, cod_clavearg, imp_valorconst, des_funcionbd, cod_tipoargumento, bol_estatus, txt_descripcion, aud_codcreadopor, aud_codmodificadopor, aud_feccreacion, aud_fecmodificacion) VALUES (21, '', '', NULL, NULL, NULL, false, '', 13, NULL, '2019-10-28', NULL);
INSERT INTO sgnom.tsgnomargumento (cod_argumentoid, cod_nbargumento, cod_clavearg, imp_valorconst, des_funcionbd, cod_tipoargumento, bol_estatus, txt_descripcion, aud_codcreadopor, aud_codmodificadopor, aud_feccreacion, aud_fecmodificacion) VALUES (20, '', '', NULL, NULL, NULL, false, '', 13, NULL, '2019-10-28', NULL);
INSERT INTO sgnom.tsgnomargumento (cod_argumentoid, cod_nbargumento, cod_clavearg, imp_valorconst, des_funcionbd, cod_tipoargumento, bol_estatus, txt_descripcion, aud_codcreadopor, aud_codmodificadopor, aud_feccreacion, aud_fecmodificacion) VALUES (36, '1', '1', NULL, NULL, NULL, false, '1', 13, NULL, '2019-10-28', NULL);
INSERT INTO sgnom.tsgnomargumento (cod_argumentoid, cod_nbargumento, cod_clavearg, imp_valorconst, des_funcionbd, cod_tipoargumento, bol_estatus, txt_descripcion, aud_codcreadopor, aud_codmodificadopor, aud_feccreacion, aud_fecmodificacion) VALUES (19, '', '', NULL, NULL, NULL, false, '', 13, NULL, '2019-10-28', NULL);
INSERT INTO sgnom.tsgnomargumento (cod_argumentoid, cod_nbargumento, cod_clavearg, imp_valorconst, des_funcionbd, cod_tipoargumento, bol_estatus, txt_descripcion, aud_codcreadopor, aud_codmodificadopor, aud_feccreacion, aud_fecmodificacion) VALUES (18, 'EDITAR', 'EDT', NULL, 'fn_sueldo_base', '0', false, 'esto lo estoy editando', 13, 10, '2019-10-28', '2019-11-12');
INSERT INTO sgnom.tsgnomargumento (cod_argumentoid, cod_nbargumento, cod_clavearg, imp_valorconst, des_funcionbd, cod_tipoargumento, bol_estatus, txt_descripcion, aud_codcreadopor, aud_codmodificadopor, aud_feccreacion, aud_fecmodificacion) VALUES (29, 'SUELDODIARIO', 'A001', 123.00, NULL, '1', false, 'sueldo que se le paga por una ', 13, NULL, '2019-10-28', NULL);
INSERT INTO sgnom.tsgnomargumento (cod_argumentoid, cod_nbargumento, cod_clavearg, imp_valorconst, des_funcionbd, cod_tipoargumento, bol_estatus, txt_descripcion, aud_codcreadopor, aud_codmodificadopor, aud_feccreacion, aud_fecmodificacion) VALUES (32, 'Q', 'A', NULL, NULL, NULL, false, '', 13, NULL, '2019-10-28', NULL);
INSERT INTO sgnom.tsgnomargumento (cod_argumentoid, cod_nbargumento, cod_clavearg, imp_valorconst, des_funcionbd, cod_tipoargumento, bol_estatus, txt_descripcion, aud_codcreadopor, aud_codmodificadopor, aud_feccreacion, aud_fecmodificacion) VALUES (37, 'PRUEBA1', 'SD2', 50.00, NULL, '1', false, 'esto es una prueba', 10, NULL, '2019-11-12', NULL);
INSERT INTO sgnom.tsgnomargumento (cod_argumentoid, cod_nbargumento, cod_clavearg, imp_valorconst, des_funcionbd, cod_tipoargumento, bol_estatus, txt_descripcion, aud_codcreadopor, aud_codmodificadopor, aud_feccreacion, aud_fecmodificacion) VALUES (3, 'SueldoXHora', 'SH', NULL, 'fn_sueldo_hora', '0', false, 'consulta el sueldo por hora segun su sueldo base', 10, NULL, '2019-09-20', NULL);
INSERT INTO sgnom.tsgnomargumento (cod_argumentoid, cod_nbargumento, cod_clavearg, imp_valorconst, des_funcionbd, cod_tipoargumento, bol_estatus, txt_descripcion, aud_codcreadopor, aud_codmodificadopor, aud_feccreacion, aud_fecmodificacion) VALUES (5, 'SueldoXDia', 'SD', NULL, 'fn_sueldo_dia', '0', false, 'calcula el sueldo por dia segun su sueldo base', 10, NULL, '2019-09-20', NULL);
INSERT INTO sgnom.tsgnomargumento (cod_argumentoid, cod_nbargumento, cod_clavearg, imp_valorconst, des_funcionbd, cod_tipoargumento, bol_estatus, txt_descripcion, aud_codcreadopor, aud_codmodificadopor, aud_feccreacion, aud_fecmodificacion) VALUES (10, 'sin proyecto acuerdo', 'SPA', NULL, 'fn_sueldo_porcentaje', '0', false, 'calcula el sueldo segun el porcentaje acordado si no se cuenta con proyecto', 10, NULL, '2019-09-21', NULL);
INSERT INTO sgnom.tsgnomargumento (cod_argumentoid, cod_nbargumento, cod_clavearg, imp_valorconst, des_funcionbd, cod_tipoargumento, bol_estatus, txt_descripcion, aud_codcreadopor, aud_codmodificadopor, aud_feccreacion, aud_fecmodificacion) VALUES (9, 'sin proyecto', 'SP', 50.00, NULL, '1', false, 'sueldo sin proyecto', 10, NULL, '2019-09-20', NULL);
INSERT INTO sgnom.tsgnomargumento (cod_argumentoid, cod_nbargumento, cod_clavearg, imp_valorconst, des_funcionbd, cod_tipoargumento, bol_estatus, txt_descripcion, aud_codcreadopor, aud_codmodificadopor, aud_feccreacion, aud_fecmodificacion) VALUES (8, 'con proyecto', 'CP', 100.00, NULL, '1', false, 'sueldo con proyecto', 10, NULL, '2019-09-20', NULL);
INSERT INTO sgnom.tsgnomargumento (cod_argumentoid, cod_nbargumento, cod_clavearg, imp_valorconst, des_funcionbd, cod_tipoargumento, bol_estatus, txt_descripcion, aud_codcreadopor, aud_codmodificadopor, aud_feccreacion, aud_fecmodificacion) VALUES (7, 'Dias trabajados', 'DT', NULL, 'fn_dias_trabajados', '0', false, 'consulta el numero de dias segun la incidencia', 10, NULL, '2019-09-20', NULL);
INSERT INTO sgnom.tsgnomargumento (cod_argumentoid, cod_nbargumento, cod_clavearg, imp_valorconst, des_funcionbd, cod_tipoargumento, bol_estatus, txt_descripcion, aud_codcreadopor, aud_codmodificadopor, aud_feccreacion, aud_fecmodificacion) VALUES (6, 'Horas trabajadas', 'HT', NULL, 'fn_horas_trabajadas', '0', false, 'consulta el numero de horas registradas en la incidencia', 10, NULL, '2019-09-20', NULL);
INSERT INTO sgnom.tsgnomargumento (cod_argumentoid, cod_nbargumento, cod_clavearg, imp_valorconst, des_funcionbd, cod_tipoargumento, bol_estatus, txt_descripcion, aud_codcreadopor, aud_codmodificadopor, aud_feccreacion, aud_fecmodificacion) VALUES (2, 'DIASDELMES', 'DM', 30.00, NULL, '1', true, 'dias del mes pdadsfsfsfdgdgbfg', 10, 13, '2019-12-03', '2019-08-26');
INSERT INTO sgnom.tsgnomargumento (cod_argumentoid, cod_nbargumento, cod_clavearg, imp_valorconst, des_funcionbd, cod_tipoargumento, bol_estatus, txt_descripcion, aud_codcreadopor, aud_codmodificadopor, aud_feccreacion, aud_fecmodificacion) VALUES (38, 'BONOASISTENCIA', 'BAI', NULL, 'fn_bono_asistencia_imss', '0', true, 'bono asistencia del imss', 10, NULL, '2019-12-03', NULL);
INSERT INTO sgnom.tsgnomargumento (cod_argumentoid, cod_nbargumento, cod_clavearg, imp_valorconst, des_funcionbd, cod_tipoargumento, bol_estatus, txt_descripcion, aud_codcreadopor, aud_codmodificadopor, aud_feccreacion, aud_fecmodificacion) VALUES (39, 'BONOPUNTUALIDAD', 'BPI', NULL, 'fn_bono_puntualidad_imss', '0', true, 'bono puntualidad', 10, NULL, '2019-12-03', NULL);
INSERT INTO sgnom.tsgnomargumento (cod_argumentoid, cod_nbargumento, cod_clavearg, imp_valorconst, des_funcionbd, cod_tipoargumento, bol_estatus, txt_descripcion, aud_codcreadopor, aud_codmodificadopor, aud_feccreacion, aud_fecmodificacion) VALUES (40, 'CUOTAQUINCENAL', 'CQI', NULL, 'fn_cuota_quin_imss', '0', true, 'cuota quincenal del imss', 10, NULL, '2019-12-03', NULL);
INSERT INTO sgnom.tsgnomargumento (cod_argumentoid, cod_nbargumento, cod_clavearg, imp_valorconst, des_funcionbd, cod_tipoargumento, bol_estatus, txt_descripcion, aud_codcreadopor, aud_codmodificadopor, aud_feccreacion, aud_fecmodificacion) VALUES (41, 'VACACIONES', 'VI', NULL, 'fn_vacaciones_imss', '0', true, 'vacaciones imss', 10, NULL, '2019-12-03', NULL);
INSERT INTO sgnom.tsgnomargumento (cod_argumentoid, cod_nbargumento, cod_clavearg, imp_valorconst, des_funcionbd, cod_tipoargumento, bol_estatus, txt_descripcion, aud_codcreadopor, aud_codmodificadopor, aud_feccreacion, aud_fecmodificacion) VALUES (42, 'SUELDO', 'SI', NULL, 'fn_sueldo_imss', '0', true, 'sueldo del imss', 10, NULL, '2019-12-03', NULL);
INSERT INTO sgnom.tsgnomargumento (cod_argumentoid, cod_nbargumento, cod_clavearg, imp_valorconst, des_funcionbd, cod_tipoargumento, bol_estatus, txt_descripcion, aud_codcreadopor, aud_codmodificadopor, aud_feccreacion, aud_fecmodificacion) VALUES (43, 'SUBSIDIOEMPLEO', 'SEI', NULL, 'fn_subsidio_empleo_imss', '0', true, 'subsidio para el empleo imss', 10, NULL, '2019-12-03', NULL);
INSERT INTO sgnom.tsgnomargumento (cod_argumentoid, cod_nbargumento, cod_clavearg, imp_valorconst, des_funcionbd, cod_tipoargumento, bol_estatus, txt_descripcion, aud_codcreadopor, aud_codmodificadopor, aud_feccreacion, aud_fecmodificacion) VALUES (44, 'PRIMAVACACIONA', 'PVI', NULL, 'fn_prima_vacacional_imss', '0', true, 'prima vacacional imss', 10, NULL, '2019-12-06', NULL);
INSERT INTO sgnom.tsgnomargumento (cod_argumentoid, cod_nbargumento, cod_clavearg, imp_valorconst, des_funcionbd, cod_tipoargumento, bol_estatus, txt_descripcion, aud_codcreadopor, aud_codmodificadopor, aud_feccreacion, aud_fecmodificacion) VALUES (45, 'PRESTAMOPERSONA', 'PPI', NULL, 'fn_prestamo_personal_imss', '0', true, 'prestamo personal imss', 10, NULL, '2019-12-06', NULL);
INSERT INTO sgnom.tsgnomargumento (cod_argumentoid, cod_nbargumento, cod_clavearg, imp_valorconst, des_funcionbd, cod_tipoargumento, bol_estatus, txt_descripcion, aud_codcreadopor, aud_codmodificadopor, aud_feccreacion, aud_fecmodificacion) VALUES (46, 'ISRIMSS', 'II', NULL, 'fn_isr_imss', '0', true, 'ISR IMSS', 10, NULL, '2019-12-06', NULL);
INSERT INTO sgnom.tsgnomargumento (cod_argumentoid, cod_nbargumento, cod_clavearg, imp_valorconst, des_funcionbd, cod_tipoargumento, bol_estatus, txt_descripcion, aud_codcreadopor, aud_codmodificadopor, aud_feccreacion, aud_fecmodificacion) VALUES (47, 'INFONAVIT', 'INI', NULL, 'fn_infonavit_imss', '0', true, 'infonavit', 10, NULL, '2019-12-06', NULL);
INSERT INTO sgnom.tsgnomargumento (cod_argumentoid, cod_nbargumento, cod_clavearg, imp_valorconst, des_funcionbd, cod_tipoargumento, bol_estatus, txt_descripcion, aud_codcreadopor, aud_codmodificadopor, aud_feccreacion, aud_fecmodificacion) VALUES (48, 'DIASAGUINALDO', 'DA', 15.00, NULL, '1', true, 'dias de aguinaldo', 10, NULL, '2019-12-10', NULL);
INSERT INTO sgnom.tsgnomargumento (cod_argumentoid, cod_nbargumento, cod_clavearg, imp_valorconst, des_funcionbd, cod_tipoargumento, bol_estatus, txt_descripcion, aud_codcreadopor, aud_codmodificadopor, aud_feccreacion, aud_fecmodificacion) VALUES (49, 'SALARIODIARIO', 'SD', NULL, 'fn_salario_diario', '0', true, 'salario diario integrado', 10, NULL, '2019-12-10', NULL);


--
-- TOC entry 4787 (class 0 OID 102014)
-- Dependencies: 375
-- Data for Name: tsgnombitacora; Type: TABLE DATA; Schema: sgnom; Owner: suite
--



--
-- TOC entry 4788 (class 0 OID 102020)
-- Dependencies: 376
-- Data for Name: tsgnomcabecera; Type: TABLE DATA; Schema: sgnom; Owner: suite
--

INSERT INTO sgnom.tsgnomcabecera (cod_cabeceraid, cod_nbnomina, fec_creacion, fec_ejecucion, fec_cierre, imp_totpercepcion, imp_totdeduccion, imp_totalemp, cod_quincenaid_fk, cod_tiponominaid_fk, cod_estatusnomid_fk, cnu_totalemp, aud_codcreadopor, aud_codmodificadopor, aud_feccreacion, aud_fecmodificacion) VALUES (42, 'General / 1era de diciembre / 2019', '2019-12-10', NULL, NULL, 35249.50, 0.00, 35249.50, 23, 1, 2, 13, 10, NULL, '2019-12-10', NULL);


--
-- TOC entry 4789 (class 0 OID 102024)
-- Dependencies: 377
-- Data for Name: tsgnomcabeceraht; Type: TABLE DATA; Schema: sgnom; Owner: suite
--



--
-- TOC entry 4790 (class 0 OID 102027)
-- Dependencies: 378
-- Data for Name: tsgnomcalculo; Type: TABLE DATA; Schema: sgnom; Owner: suite
--

INSERT INTO sgnom.tsgnomcalculo (cod_calculoid, cod_tpcalculo, bol_estatus) VALUES (1, 'Importe', true);
INSERT INTO sgnom.tsgnomcalculo (cod_calculoid, cod_tpcalculo, bol_estatus) VALUES (2, 'Calculo', true);


--
-- TOC entry 4791 (class 0 OID 102030)
-- Dependencies: 379
-- Data for Name: tsgnomcatincidencia; Type: TABLE DATA; Schema: sgnom; Owner: suite
--

INSERT INTO sgnom.tsgnomcatincidencia (cod_catincidenciaid, cod_claveincidencia, cod_nbincidencia, cod_perfilincidencia, bol_estatus, cod_tipoincidencia, imp_monto) VALUES (33, 'BA1', 'BONO ADICIONAL 100%', '', true, '1', 150.00);
INSERT INTO sgnom.tsgnomcatincidencia (cod_catincidenciaid, cod_claveincidencia, cod_nbincidencia, cod_perfilincidencia, bol_estatus, cod_tipoincidencia, imp_monto) VALUES (34, 'BA2', 'BONO ADICIONAL 50%', '', true, '1', 100.00);


--
-- TOC entry 4792 (class 0 OID 102033)
-- Dependencies: 380
-- Data for Name: tsgnomclasificador; Type: TABLE DATA; Schema: sgnom; Owner: suite
--

INSERT INTO sgnom.tsgnomclasificador (cod_clasificadorid, cod_tpclasificador, bol_estatus) VALUES (1, 'clas1', true);
INSERT INTO sgnom.tsgnomclasificador (cod_clasificadorid, cod_tpclasificador, bol_estatus) VALUES (2, 'clas2', true);


--
-- TOC entry 4793 (class 0 OID 102036)
-- Dependencies: 381
-- Data for Name: tsgnomcncptoquinc; Type: TABLE DATA; Schema: sgnom; Owner: suite
--

INSERT INTO sgnom.tsgnomcncptoquinc (cod_cncptoquincid, cod_empquincenaid_fk, cod_conceptoid_fk, imp_concepto, imp_gravado, imp_exento, xml_desgloce) VALUES (99, 239, 1, 20000.00, 0.00, 0.00, '                
                <mx.org.ift.serp.conceptoBindingXml.DetallesConcepto>                  
                <claveConcepto>SQ</claveConcepto>                  
                <nombreConcepto>SUELDO QUINCENA</nombreConcepto><formula>:(:SUELDOBASE:/:DIASDELMES:):*:DIASLABORADOS</formula>                  
                <argumentos><mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>SUELDOBASE</nombre>                            
                    <valor>40000.00</valor> <descripcion>SUELDOBASE</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento><mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>DIASDELMES</nombre>                            
                    <valor>30.00</valor> <descripcion>DIASDELMES</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento><mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>DIASLABORADOS</nombre>                            
                    <valor>15.00</valor> <descripcion>DIASLABORADOS</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento></argumentos><importe>20000.00</importe> </mx.org.ift.serp.conceptoBindingXml.DetallesConcepto>  ');
INSERT INTO sgnom.tsgnomcncptoquinc (cod_cncptoquincid, cod_empquincenaid_fk, cod_conceptoid_fk, imp_concepto, imp_gravado, imp_exento, xml_desgloce) VALUES (100, 240, 1, 6000.00, 0.00, 0.00, '                
                <mx.org.ift.serp.conceptoBindingXml.DetallesConcepto>                  
                <claveConcepto>SQ</claveConcepto>                  
                <nombreConcepto>SUELDO QUINCENA</nombreConcepto><formula>:(:SUELDOBASE:/:DIASDELMES:):*:DIASLABORADOS</formula>                  
                <argumentos><mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>SUELDOBASE</nombre>                            
                    <valor>12000.00</valor> <descripcion>SUELDOBASE</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento><mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>DIASDELMES</nombre>                            
                    <valor>30.00</valor> <descripcion>DIASDELMES</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento><mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>DIASLABORADOS</nombre>                            
                    <valor>15.00</valor> <descripcion>DIASLABORADOS</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento></argumentos><importe>6000.00</importe> </mx.org.ift.serp.conceptoBindingXml.DetallesConcepto>  ');
INSERT INTO sgnom.tsgnomcncptoquinc (cod_cncptoquincid, cod_empquincenaid_fk, cod_conceptoid_fk, imp_concepto, imp_gravado, imp_exento, xml_desgloce) VALUES (101, 241, 1, 4100.00, 0.00, 0.00, '                
                <mx.org.ift.serp.conceptoBindingXml.DetallesConcepto>                  
                <claveConcepto>SQ</claveConcepto>                  
                <nombreConcepto>SUELDO QUINCENA</nombreConcepto><formula>:(:SUELDOBASE:/:DIASDELMES:):*:DIASLABORADOS</formula>                  
                <argumentos><mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>SUELDOBASE</nombre>                            
                    <valor>8200.00</valor> <descripcion>SUELDOBASE</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento><mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>DIASDELMES</nombre>                            
                    <valor>30.00</valor> <descripcion>DIASDELMES</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento><mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>DIASLABORADOS</nombre>                            
                    <valor>15.00</valor> <descripcion>DIASLABORADOS</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento></argumentos><importe>4100.00</importe> </mx.org.ift.serp.conceptoBindingXml.DetallesConcepto>  ');
INSERT INTO sgnom.tsgnomcncptoquinc (cod_cncptoquincid, cod_empquincenaid_fk, cod_conceptoid_fk, imp_concepto, imp_gravado, imp_exento, xml_desgloce) VALUES (102, 242, 1, 106.50, 0.00, 0.00, '                
                <mx.org.ift.serp.conceptoBindingXml.DetallesConcepto>                  
                <claveConcepto>SQ</claveConcepto>                  
                <nombreConcepto>SUELDO QUINCENA</nombreConcepto><formula>:(:SUELDOBASE:/:DIASDELMES:):*:DIASLABORADOS</formula>                  
                <argumentos><mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>SUELDOBASE</nombre>                            
                    <valor>213.00</valor> <descripcion>SUELDOBASE</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento><mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>DIASDELMES</nombre>                            
                    <valor>30.00</valor> <descripcion>DIASDELMES</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento><mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>DIASLABORADOS</nombre>                            
                    <valor>15.00</valor> <descripcion>DIASLABORADOS</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento></argumentos><importe>106.50</importe> </mx.org.ift.serp.conceptoBindingXml.DetallesConcepto>  ');
INSERT INTO sgnom.tsgnomcncptoquinc (cod_cncptoquincid, cod_empquincenaid_fk, cod_conceptoid_fk, imp_concepto, imp_gravado, imp_exento, xml_desgloce) VALUES (103, 243, 1, 106.50, 0.00, 0.00, '                
                <mx.org.ift.serp.conceptoBindingXml.DetallesConcepto>                  
                <claveConcepto>SQ</claveConcepto>                  
                <nombreConcepto>SUELDO QUINCENA</nombreConcepto><formula>:(:SUELDOBASE:/:DIASDELMES:):*:DIASLABORADOS</formula>                  
                <argumentos><mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>SUELDOBASE</nombre>                            
                    <valor>213.00</valor> <descripcion>SUELDOBASE</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento><mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>DIASDELMES</nombre>                            
                    <valor>30.00</valor> <descripcion>DIASDELMES</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento><mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>DIASLABORADOS</nombre>                            
                    <valor>15.00</valor> <descripcion>DIASLABORADOS</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento></argumentos><importe>106.50</importe> </mx.org.ift.serp.conceptoBindingXml.DetallesConcepto>  ');
INSERT INTO sgnom.tsgnomcncptoquinc (cod_cncptoquincid, cod_empquincenaid_fk, cod_conceptoid_fk, imp_concepto, imp_gravado, imp_exento, xml_desgloce) VALUES (104, 244, 1, 106.50, 0.00, 0.00, '                
                <mx.org.ift.serp.conceptoBindingXml.DetallesConcepto>                  
                <claveConcepto>SQ</claveConcepto>                  
                <nombreConcepto>SUELDO QUINCENA</nombreConcepto><formula>:(:SUELDOBASE:/:DIASDELMES:):*:DIASLABORADOS</formula>                  
                <argumentos><mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>SUELDOBASE</nombre>                            
                    <valor>213.00</valor> <descripcion>SUELDOBASE</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento><mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>DIASDELMES</nombre>                            
                    <valor>30.00</valor> <descripcion>DIASDELMES</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento><mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>DIASLABORADOS</nombre>                            
                    <valor>15.00</valor> <descripcion>DIASLABORADOS</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento></argumentos><importe>106.50</importe> </mx.org.ift.serp.conceptoBindingXml.DetallesConcepto>  ');
INSERT INTO sgnom.tsgnomcncptoquinc (cod_cncptoquincid, cod_empquincenaid_fk, cod_conceptoid_fk, imp_concepto, imp_gravado, imp_exento, xml_desgloce) VALUES (105, 245, 1, 500.00, 0.00, 0.00, '                
                <mx.org.ift.serp.conceptoBindingXml.DetallesConcepto>                  
                <claveConcepto>SQ</claveConcepto>                  
                <nombreConcepto>SUELDO QUINCENA</nombreConcepto><formula>:(:SUELDOBASE:/:DIASDELMES:):*:DIASLABORADOS</formula>                  
                <argumentos><mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>SUELDOBASE</nombre>                            
                    <valor>1000.00</valor> <descripcion>SUELDOBASE</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento><mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>DIASDELMES</nombre>                            
                    <valor>30.00</valor> <descripcion>DIASDELMES</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento><mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>DIASLABORADOS</nombre>                            
                    <valor>15.00</valor> <descripcion>DIASLABORADOS</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento></argumentos><importe>500.00</importe> </mx.org.ift.serp.conceptoBindingXml.DetallesConcepto>  ');
INSERT INTO sgnom.tsgnomcncptoquinc (cod_cncptoquincid, cod_empquincenaid_fk, cod_conceptoid_fk, imp_concepto, imp_gravado, imp_exento, xml_desgloce) VALUES (106, 246, 1, 106.50, 0.00, 0.00, '                
                <mx.org.ift.serp.conceptoBindingXml.DetallesConcepto>                  
                <claveConcepto>SQ</claveConcepto>                  
                <nombreConcepto>SUELDO QUINCENA</nombreConcepto><formula>:(:SUELDOBASE:/:DIASDELMES:):*:DIASLABORADOS</formula>                  
                <argumentos><mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>SUELDOBASE</nombre>                            
                    <valor>213.00</valor> <descripcion>SUELDOBASE</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento><mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>DIASDELMES</nombre>                            
                    <valor>30.00</valor> <descripcion>DIASDELMES</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento><mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>DIASLABORADOS</nombre>                            
                    <valor>15.00</valor> <descripcion>DIASLABORADOS</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento></argumentos><importe>106.50</importe> </mx.org.ift.serp.conceptoBindingXml.DetallesConcepto>  ');
INSERT INTO sgnom.tsgnomcncptoquinc (cod_cncptoquincid, cod_empquincenaid_fk, cod_conceptoid_fk, imp_concepto, imp_gravado, imp_exento, xml_desgloce) VALUES (107, 247, 1, 1106.50, 0.00, 0.00, '                
                <mx.org.ift.serp.conceptoBindingXml.DetallesConcepto>                  
                <claveConcepto>SQ</claveConcepto>                  
                <nombreConcepto>SUELDO QUINCENA</nombreConcepto><formula>:(:SUELDOBASE:/:DIASDELMES:):*:DIASLABORADOS</formula>                  
                <argumentos><mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>SUELDOBASE</nombre>                            
                    <valor>2213.00</valor> <descripcion>SUELDOBASE</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento><mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>DIASDELMES</nombre>                            
                    <valor>30.00</valor> <descripcion>DIASDELMES</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento><mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>DIASLABORADOS</nombre>                            
                    <valor>15.00</valor> <descripcion>DIASLABORADOS</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento></argumentos><importe>1106.50</importe> </mx.org.ift.serp.conceptoBindingXml.DetallesConcepto>  ');
INSERT INTO sgnom.tsgnomcncptoquinc (cod_cncptoquincid, cod_empquincenaid_fk, cod_conceptoid_fk, imp_concepto, imp_gravado, imp_exento, xml_desgloce) VALUES (108, 248, 1, 617.00, 0.00, 0.00, '                
                <mx.org.ift.serp.conceptoBindingXml.DetallesConcepto>                  
                <claveConcepto>SQ</claveConcepto>                  
                <nombreConcepto>SUELDO QUINCENA</nombreConcepto><formula>:(:SUELDOBASE:/:DIASDELMES:):*:DIASLABORADOS</formula>                  
                <argumentos><mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>SUELDOBASE</nombre>                            
                    <valor>1234.00</valor> <descripcion>SUELDOBASE</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento><mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>DIASDELMES</nombre>                            
                    <valor>30.00</valor> <descripcion>DIASDELMES</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento><mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>DIASLABORADOS</nombre>                            
                    <valor>15.00</valor> <descripcion>DIASLABORADOS</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento></argumentos><importe>617.00</importe> </mx.org.ift.serp.conceptoBindingXml.DetallesConcepto>  ');
INSERT INTO sgnom.tsgnomcncptoquinc (cod_cncptoquincid, cod_empquincenaid_fk, cod_conceptoid_fk, imp_concepto, imp_gravado, imp_exento, xml_desgloce) VALUES (109, 249, 1, 500.00, 0.00, 0.00, '                
                <mx.org.ift.serp.conceptoBindingXml.DetallesConcepto>                  
                <claveConcepto>SQ</claveConcepto>                  
                <nombreConcepto>SUELDO QUINCENA</nombreConcepto><formula>:(:SUELDOBASE:/:DIASDELMES:):*:DIASLABORADOS</formula>                  
                <argumentos><mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>SUELDOBASE</nombre>                            
                    <valor>1000.00</valor> <descripcion>SUELDOBASE</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento><mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>DIASDELMES</nombre>                            
                    <valor>30.00</valor> <descripcion>DIASDELMES</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento><mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>DIASLABORADOS</nombre>                            
                    <valor>15.00</valor> <descripcion>DIASLABORADOS</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento></argumentos><importe>500.00</importe> </mx.org.ift.serp.conceptoBindingXml.DetallesConcepto>  ');
INSERT INTO sgnom.tsgnomcncptoquinc (cod_cncptoquincid, cod_empquincenaid_fk, cod_conceptoid_fk, imp_concepto, imp_gravado, imp_exento, xml_desgloce) VALUES (110, 250, 1, 1000.00, 0.00, 0.00, '                
                <mx.org.ift.serp.conceptoBindingXml.DetallesConcepto>                  
                <claveConcepto>SQ</claveConcepto>                  
                <nombreConcepto>SUELDO QUINCENA</nombreConcepto><formula>:(:SUELDOBASE:/:DIASDELMES:):*:DIASLABORADOS</formula>                  
                <argumentos><mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>SUELDOBASE</nombre>                            
                    <valor>2000.00</valor> <descripcion>SUELDOBASE</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento><mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>DIASDELMES</nombre>                            
                    <valor>30.00</valor> <descripcion>DIASDELMES</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento><mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>DIASLABORADOS</nombre>                            
                    <valor>15.00</valor> <descripcion>DIASLABORADOS</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento></argumentos><importe>1000.00</importe> </mx.org.ift.serp.conceptoBindingXml.DetallesConcepto>  ');
INSERT INTO sgnom.tsgnomcncptoquinc (cod_cncptoquincid, cod_empquincenaid_fk, cod_conceptoid_fk, imp_concepto, imp_gravado, imp_exento, xml_desgloce) VALUES (131, 243, 9, 900.00, 0.00, 0.00, '                
                <mx.org.ift.serp.conceptoBindingXml.DetallesConcepto>                  
                <claveConcepto> BA1 </claveConcepto>                  
                <nombreConcepto> BONO ADICIONAL 100% </nombreConcepto><formula> </formula><argumentos><mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>BA1</nombre>                            
                <valor>900.00</valor> <descripcion>BONO ADICIONAL 100%</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento></argumentos><importe> 900.00</importe>   </mx.org.ift.serp.conceptoBindingXml.DetallesConcepto>');
INSERT INTO sgnom.tsgnomcncptoquinc (cod_cncptoquincid, cod_empquincenaid_fk, cod_conceptoid_fk, imp_concepto, imp_gravado, imp_exento, xml_desgloce) VALUES (111, 251, 1, 1000.00, 0.00, 0.00, '                
                <mx.org.ift.serp.conceptoBindingXml.DetallesConcepto>                  
                <claveConcepto>SQ</claveConcepto>                  
                <nombreConcepto>SUELDO QUINCENA</nombreConcepto><formula>:(:SUELDOBASE:/:DIASDELMES:):*:DIASLABORADOS</formula>                  
                <argumentos><mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>SUELDOBASE</nombre>                            
                    <valor>2000.00</valor> <descripcion>SUELDOBASE</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento><mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>DIASDELMES</nombre>                            
                    <valor>30.00</valor> <descripcion>DIASDELMES</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento><mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>DIASLABORADOS</nombre>                            
                    <valor>15.00</valor> <descripcion>DIASLABORADOS</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento></argumentos><importe>1000.00</importe> </mx.org.ift.serp.conceptoBindingXml.DetallesConcepto>  ');
INSERT INTO sgnom.tsgnomcncptoquinc (cod_cncptoquincid, cod_empquincenaid_fk, cod_conceptoid_fk, imp_concepto, imp_gravado, imp_exento, xml_desgloce) VALUES (132, 243, 9, 1300.00, 0.00, 0.00, '                
                <mx.org.ift.serp.conceptoBindingXml.DetallesConcepto>                  
                <claveConcepto> BA2 </claveConcepto>                  
                <nombreConcepto> BONO ADICIONAL 50% </nombreConcepto><formula> </formula><argumentos><mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>BA2</nombre>                            
                <valor>1300.00</valor> <descripcion>BONO ADICIONAL 50%</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento></argumentos><importe> 1300.00</importe>   </mx.org.ift.serp.conceptoBindingXml.DetallesConcepto>');


--
-- TOC entry 4794 (class 0 OID 102043)
-- Dependencies: 382
-- Data for Name: tsgnomcncptoquincht; Type: TABLE DATA; Schema: sgnom; Owner: suite
--



--
-- TOC entry 4795 (class 0 OID 102049)
-- Dependencies: 383
-- Data for Name: tsgnomconcepto; Type: TABLE DATA; Schema: sgnom; Owner: suite
--

INSERT INTO sgnom.tsgnomconcepto (cod_conceptoid, cod_nbconcepto, cod_claveconcepto, cnu_prioricalculo, cnu_articulo, bol_estatus, cod_formulaid_fk, cod_tipoconceptoid_fk, cod_calculoid_fk, cod_conceptosatid_fk, cod_frecuenciapago, cod_partidaprep, cnu_cuentacontable, cod_gravado, cod_excento, bol_aplicaisn, bol_retroactividad, cnu_topeex, cod_clasificadorid_fk, cod_tiponominaid_fk, aud_codcreadopor, aud_codmodificadopor, aud_feccreacion, aud_fecmodificacion, imp_monto) VALUES (8, 'QWE', 'QWE', 2, 45, false, NULL, 1, 1, 1, 'Q', 4536, '345346463646436345', NULL, NULL, false, false, NULL, 1, 2, 13, NULL, '2019-11-05', NULL, 123456);
INSERT INTO sgnom.tsgnomconcepto (cod_conceptoid, cod_nbconcepto, cod_claveconcepto, cnu_prioricalculo, cnu_articulo, bol_estatus, cod_formulaid_fk, cod_tipoconceptoid_fk, cod_calculoid_fk, cod_conceptosatid_fk, cod_frecuenciapago, cod_partidaprep, cnu_cuentacontable, cod_gravado, cod_excento, bol_aplicaisn, bol_retroactividad, cnu_topeex, cod_clasificadorid_fk, cod_tiponominaid_fk, aud_codcreadopor, aud_codmodificadopor, aud_feccreacion, aud_fecmodificacion, imp_monto) VALUES (6, 'RENTA', 'RENT', 3, 989, false, NULL, 1, 1, 1, 'M', 31321, '12321312321', '1', '1', true, true, 546, 1, 1, 13, NULL, '2019-11-05', NULL, 456);
INSERT INTO sgnom.tsgnomconcepto (cod_conceptoid, cod_nbconcepto, cod_claveconcepto, cnu_prioricalculo, cnu_articulo, bol_estatus, cod_formulaid_fk, cod_tipoconceptoid_fk, cod_calculoid_fk, cod_conceptosatid_fk, cod_frecuenciapago, cod_partidaprep, cnu_cuentacontable, cod_gravado, cod_excento, bol_aplicaisn, bol_retroactividad, cnu_topeex, cod_clasificadorid_fk, cod_tiponominaid_fk, aud_codcreadopor, aud_codmodificadopor, aud_feccreacion, aud_fecmodificacion, imp_monto) VALUES (4, 'U', 'U', 4, 0, false, 13, 1, 2, 1, 'Q', 0, '123456789012345678', '0', '0', false, false, NULL, 1, 2, 13, 13, '2019-10-07', '2019-11-05', NULL);
INSERT INTO sgnom.tsgnomconcepto (cod_conceptoid, cod_nbconcepto, cod_claveconcepto, cnu_prioricalculo, cnu_articulo, bol_estatus, cod_formulaid_fk, cod_tipoconceptoid_fk, cod_calculoid_fk, cod_conceptosatid_fk, cod_frecuenciapago, cod_partidaprep, cnu_cuentacontable, cod_gravado, cod_excento, bol_aplicaisn, bol_retroactividad, cnu_topeex, cod_clasificadorid_fk, cod_tiponominaid_fk, aud_codcreadopor, aud_codmodificadopor, aud_feccreacion, aud_fecmodificacion, imp_monto) VALUES (3, 'T', 'T', 1, 5, false, 11, 1, 2, 1, 'Q', 5, '5', NULL, NULL, false, false, NULL, 1, 2, 13, NULL, '2019-10-07', NULL, NULL);
INSERT INTO sgnom.tsgnomconcepto (cod_conceptoid, cod_nbconcepto, cod_claveconcepto, cnu_prioricalculo, cnu_articulo, bol_estatus, cod_formulaid_fk, cod_tipoconceptoid_fk, cod_calculoid_fk, cod_conceptosatid_fk, cod_frecuenciapago, cod_partidaprep, cnu_cuentacontable, cod_gravado, cod_excento, bol_aplicaisn, bol_retroactividad, cnu_topeex, cod_clasificadorid_fk, cod_tiponominaid_fk, aud_codcreadopor, aud_codmodificadopor, aud_feccreacion, aud_fecmodificacion, imp_monto) VALUES (1, 'SUELDO QUINCENA', 'SQ', 1, 1, true, 9, 2, 2, 1, 'Q', 1, '1', '0', '0', false, false, NULL, 1, 1, 13, 13, '2019-07-08', '2019-08-19', NULL);
INSERT INTO sgnom.tsgnomconcepto (cod_conceptoid, cod_nbconcepto, cod_claveconcepto, cnu_prioricalculo, cnu_articulo, bol_estatus, cod_formulaid_fk, cod_tipoconceptoid_fk, cod_calculoid_fk, cod_conceptosatid_fk, cod_frecuenciapago, cod_partidaprep, cnu_cuentacontable, cod_gravado, cod_excento, bol_aplicaisn, bol_retroactividad, cnu_topeex, cod_clasificadorid_fk, cod_tiponominaid_fk, aud_codcreadopor, aud_codmodificadopor, aud_feccreacion, aud_fecmodificacion, imp_monto) VALUES (9, 'CONCEPTO NUEVO', 'CON', 2, 25, false, NULL, 2, 1, 2, 'Q', 10, '454648646843513478', NULL, NULL, false, false, NULL, 1, 1, 10, NULL, '2019-11-12', NULL, 500);
INSERT INTO sgnom.tsgnomconcepto (cod_conceptoid, cod_nbconcepto, cod_claveconcepto, cnu_prioricalculo, cnu_articulo, bol_estatus, cod_formulaid_fk, cod_tipoconceptoid_fk, cod_calculoid_fk, cod_conceptosatid_fk, cod_frecuenciapago, cod_partidaprep, cnu_cuentacontable, cod_gravado, cod_excento, bol_aplicaisn, bol_retroactividad, cnu_topeex, cod_clasificadorid_fk, cod_tiponominaid_fk, aud_codcreadopor, aud_codmodificadopor, aud_feccreacion, aud_fecmodificacion, imp_monto) VALUES (2, 'QW', 'QW', 3, 2, false, 10, 2, 2, 1, 'Q', 20, '2', '0', '0', false, false, NULL, 1, 1, 13, 13, '2019-09-09', '2019-11-05', NULL);
INSERT INTO sgnom.tsgnomconcepto (cod_conceptoid, cod_nbconcepto, cod_claveconcepto, cnu_prioricalculo, cnu_articulo, bol_estatus, cod_formulaid_fk, cod_tipoconceptoid_fk, cod_calculoid_fk, cod_conceptosatid_fk, cod_frecuenciapago, cod_partidaprep, cnu_cuentacontable, cod_gravado, cod_excento, bol_aplicaisn, bol_retroactividad, cnu_topeex, cod_clasificadorid_fk, cod_tiponominaid_fk, aud_codcreadopor, aud_codmodificadopor, aud_feccreacion, aud_fecmodificacion, imp_monto) VALUES (11, 'AGUINALDO', 'A', 4, 23, true, 15, 2, 2, 1, 'Q', 2, '123234324435465477', NULL, NULL, false, false, NULL, 2, 1, 10, NULL, '2019-12-10', NULL, NULL);


--
-- TOC entry 4796 (class 0 OID 102052)
-- Dependencies: 384
-- Data for Name: tsgnomconceptosat; Type: TABLE DATA; Schema: sgnom; Owner: suite
--

INSERT INTO sgnom.tsgnomconceptosat (cod_conceptosatid, des_conceptosat, des_descconcepto, bol_estatus) VALUES (1, 'ss1', 'sat1', true);
INSERT INTO sgnom.tsgnomconceptosat (cod_conceptosatid, des_conceptosat, des_descconcepto, bol_estatus) VALUES (2, 'ss2', 'sat2', true);


--
-- TOC entry 4797 (class 0 OID 102055)
-- Dependencies: 385
-- Data for Name: tsgnomconfpago; Type: TABLE DATA; Schema: sgnom; Owner: suite
--

INSERT INTO sgnom.tsgnomconfpago (cod_confpagoid, bol_pagoempleado, bol_pagorh, bol_pagofinanzas, cod_empquincenaid_fk) VALUES (147, NULL, NULL, NULL, 239);
INSERT INTO sgnom.tsgnomconfpago (cod_confpagoid, bol_pagoempleado, bol_pagorh, bol_pagofinanzas, cod_empquincenaid_fk) VALUES (148, NULL, NULL, NULL, 240);
INSERT INTO sgnom.tsgnomconfpago (cod_confpagoid, bol_pagoempleado, bol_pagorh, bol_pagofinanzas, cod_empquincenaid_fk) VALUES (149, NULL, NULL, NULL, 241);
INSERT INTO sgnom.tsgnomconfpago (cod_confpagoid, bol_pagoempleado, bol_pagorh, bol_pagofinanzas, cod_empquincenaid_fk) VALUES (150, NULL, NULL, NULL, 242);
INSERT INTO sgnom.tsgnomconfpago (cod_confpagoid, bol_pagoempleado, bol_pagorh, bol_pagofinanzas, cod_empquincenaid_fk) VALUES (151, NULL, NULL, NULL, 243);
INSERT INTO sgnom.tsgnomconfpago (cod_confpagoid, bol_pagoempleado, bol_pagorh, bol_pagofinanzas, cod_empquincenaid_fk) VALUES (152, NULL, NULL, NULL, 244);
INSERT INTO sgnom.tsgnomconfpago (cod_confpagoid, bol_pagoempleado, bol_pagorh, bol_pagofinanzas, cod_empquincenaid_fk) VALUES (153, NULL, NULL, NULL, 245);
INSERT INTO sgnom.tsgnomconfpago (cod_confpagoid, bol_pagoempleado, bol_pagorh, bol_pagofinanzas, cod_empquincenaid_fk) VALUES (154, NULL, NULL, NULL, 246);
INSERT INTO sgnom.tsgnomconfpago (cod_confpagoid, bol_pagoempleado, bol_pagorh, bol_pagofinanzas, cod_empquincenaid_fk) VALUES (155, NULL, NULL, NULL, 247);
INSERT INTO sgnom.tsgnomconfpago (cod_confpagoid, bol_pagoempleado, bol_pagorh, bol_pagofinanzas, cod_empquincenaid_fk) VALUES (156, NULL, NULL, NULL, 248);
INSERT INTO sgnom.tsgnomconfpago (cod_confpagoid, bol_pagoempleado, bol_pagorh, bol_pagofinanzas, cod_empquincenaid_fk) VALUES (157, NULL, NULL, NULL, 249);
INSERT INTO sgnom.tsgnomconfpago (cod_confpagoid, bol_pagoempleado, bol_pagorh, bol_pagofinanzas, cod_empquincenaid_fk) VALUES (158, NULL, NULL, NULL, 250);
INSERT INTO sgnom.tsgnomconfpago (cod_confpagoid, bol_pagoempleado, bol_pagorh, bol_pagofinanzas, cod_empquincenaid_fk) VALUES (159, NULL, NULL, NULL, 251);


--
-- TOC entry 4798 (class 0 OID 102059)
-- Dependencies: 386
-- Data for Name: tsgnomejercicio; Type: TABLE DATA; Schema: sgnom; Owner: suite
--

INSERT INTO sgnom.tsgnomejercicio (cod_ejercicioid, cnu_valorejercicio, bol_estatus) VALUES (1, 2014, true);
INSERT INTO sgnom.tsgnomejercicio (cod_ejercicioid, cnu_valorejercicio, bol_estatus) VALUES (2, 2015, true);
INSERT INTO sgnom.tsgnomejercicio (cod_ejercicioid, cnu_valorejercicio, bol_estatus) VALUES (3, 2016, true);
INSERT INTO sgnom.tsgnomejercicio (cod_ejercicioid, cnu_valorejercicio, bol_estatus) VALUES (4, 2017, true);
INSERT INTO sgnom.tsgnomejercicio (cod_ejercicioid, cnu_valorejercicio, bol_estatus) VALUES (5, 2018, true);
INSERT INTO sgnom.tsgnomejercicio (cod_ejercicioid, cnu_valorejercicio, bol_estatus) VALUES (6, 2019, true);
INSERT INTO sgnom.tsgnomejercicio (cod_ejercicioid, cnu_valorejercicio, bol_estatus) VALUES (7, 2020, true);
INSERT INTO sgnom.tsgnomejercicio (cod_ejercicioid, cnu_valorejercicio, bol_estatus) VALUES (8, 2021, true);


--
-- TOC entry 4799 (class 0 OID 102062)
-- Dependencies: 387
-- Data for Name: tsgnomempleados; Type: TABLE DATA; Schema: sgnom; Owner: suite
--

INSERT INTO sgnom.tsgnomempleados (cod_empleadoid, fec_ingreso, fec_salida, bol_estatus, cod_empleado_fk, imp_sueldoimss, imp_honorarios, imp_finiquito, cod_tipoimss, cod_tipohonorarios, cod_banco, cod_sucursal, cod_cuenta, txt_descripcionbaja, aud_codcreadopor, aud_feccreacion, aud_codmodificadopor, aud_fecmodificacion, cod_clabe, des_validacion, cod_validaciones, cod_bancoh, cod_sucursalh, cod_cuentah, cod_clabeh, xml_historial, aud_fecvalidacion) VALUES (1, '2019-05-07', NULL, true, 1, 6083.32, 40000.00, 123.00, '1', '1', '123', 321, '12345', 'baja', 1, '2190-12-21', 1, '2019-09-30', '12345', true, 'a', NULL, NULL, NULL, NULL, '', NULL);
INSERT INTO sgnom.tsgnomempleados (cod_empleadoid, fec_ingreso, fec_salida, bol_estatus, cod_empleado_fk, imp_sueldoimss, imp_honorarios, imp_finiquito, cod_tipoimss, cod_tipohonorarios, cod_banco, cod_sucursal, cod_cuenta, txt_descripcionbaja, aud_codcreadopor, aud_feccreacion, aud_codmodificadopor, aud_fecmodificacion, cod_clabe, des_validacion, cod_validaciones, cod_bancoh, cod_sucursalh, cod_cuentah, cod_clabeh, xml_historial, aud_fecvalidacion) VALUES (11, '2019-05-07', NULL, true, 11, 3954.16, 8200.00, 123.00, '1', 'o', '123', 321, '12345', 'baja', 1, '2190-12-21', 1, '2019-09-30', '12345', true, 'a', NULL, NULL, NULL, NULL, '', NULL);
INSERT INTO sgnom.tsgnomempleados (cod_empleadoid, fec_ingreso, fec_salida, bol_estatus, cod_empleado_fk, imp_sueldoimss, imp_honorarios, imp_finiquito, cod_tipoimss, cod_tipohonorarios, cod_banco, cod_sucursal, cod_cuenta, txt_descripcionbaja, aud_codcreadopor, aud_feccreacion, aud_codmodificadopor, aud_fecmodificacion, cod_clabe, des_validacion, cod_validaciones, cod_bancoh, cod_sucursalh, cod_cuentah, cod_clabeh, xml_historial, aud_fecvalidacion) VALUES (12, '2019-05-07', NULL, true, 12, 2585.41, 213.00, 123.00, '1', 'o', '123', 321, '12345', 'baja', 1, '2190-12-21', 1, '2019-09-30', '12345', true, 'a', NULL, NULL, NULL, NULL, '

<mx.org.mbn.serp.historiaBindingXml.Dato>
    <historial>
        <mx.org.mbn.serp.historialBindingXml.Datos>
            <fecha>27-11-2019</fecha>
            <sueldoimss>10000000.00</sueldoimss>
            <sueldoadicional>213.00</sueldoadicional>
            <estatus>d</estatus>
        </mx.org.mbn.serp.historialBindingXml.Datos>
    </historial>
    <historial>
        <mx.org.mbn.serp.historialBindingXml.Datos>
            <fecha>27-11-2019</fecha>
            <sueldoimss>10000000.00</sueldoimss>
            <sueldoadicional>213.00</sueldoadicional>
            <estatus>d</estatus>
        </mx.org.mbn.serp.historialBindingXml.Datos>
    </historial>
    <historial>
        <mx.org.mbn.serp.historialBindingXml.Datos>
            <fecha>27-11-2019</fecha>
            <sueldoimss>10000000.00</sueldoimss>
            <sueldoadicional>213.00</sueldoadicional>
            <estatus>d</estatus>
        </mx.org.mbn.serp.historialBindingXml.Datos>
    </historial>
    <historial>
        <mx.org.mbn.serp.historialBindingXml.Datos>
            <fecha>27-11-2019</fecha>
            <sueldoimss>1.00</sueldoimss>
            <sueldoadicional>213.00</sueldoadicional>
            <estatus>d</estatus>
        </mx.org.mbn.serp.historialBindingXml.Datos>
    </historial>
</mx.org.mbn.serp.historiaBindingXml.Dato>', NULL);
INSERT INTO sgnom.tsgnomempleados (cod_empleadoid, fec_ingreso, fec_salida, bol_estatus, cod_empleado_fk, imp_sueldoimss, imp_honorarios, imp_finiquito, cod_tipoimss, cod_tipohonorarios, cod_banco, cod_sucursal, cod_cuenta, txt_descripcionbaja, aud_codcreadopor, aud_feccreacion, aud_codmodificadopor, aud_fecmodificacion, cod_clabe, des_validacion, cod_validaciones, cod_bancoh, cod_sucursalh, cod_cuentah, cod_clabeh, xml_historial, aud_fecvalidacion) VALUES (16, '2019-05-07', NULL, true, 16, 8137.96, 213.00, 123.00, '1', 'o', '123', 321, '12345', 'baja', 1, '2190-12-21', 1, '2019-09-30', '12345', true, 'a', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO sgnom.tsgnomempleados (cod_empleadoid, fec_ingreso, fec_salida, bol_estatus, cod_empleado_fk, imp_sueldoimss, imp_honorarios, imp_finiquito, cod_tipoimss, cod_tipohonorarios, cod_banco, cod_sucursal, cod_cuenta, txt_descripcionbaja, aud_codcreadopor, aud_feccreacion, aud_codmodificadopor, aud_fecmodificacion, cod_clabe, des_validacion, cod_validaciones, cod_bancoh, cod_sucursalh, cod_cuentah, cod_clabeh, xml_historial, aud_fecvalidacion) VALUES (15, '2019-05-07', NULL, true, 15, 4562.49, 1000.00, 123.00, '1', 'o', '123', 321, '12345', 'baja', 1, '2190-12-21', 1, '2019-09-30', '12345', true, 'a', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO sgnom.tsgnomempleados (cod_empleadoid, fec_ingreso, fec_salida, bol_estatus, cod_empleado_fk, imp_sueldoimss, imp_honorarios, imp_finiquito, cod_tipoimss, cod_tipohonorarios, cod_banco, cod_sucursal, cod_cuenta, txt_descripcionbaja, aud_codcreadopor, aud_feccreacion, aud_codmodificadopor, aud_fecmodificacion, cod_clabe, des_validacion, cod_validaciones, cod_bancoh, cod_sucursalh, cod_cuentah, cod_clabeh, xml_historial, aud_fecvalidacion) VALUES (17, '2019-05-07', NULL, true, 17, 1977.08, 2213.00, 123.00, '1', '0', '123', 321, '12345', 'baja', 1, '2190-12-21', 1, '2019-09-30', '12345', true, 'a', NULL, NULL, NULL, NULL, '', NULL);
INSERT INTO sgnom.tsgnomempleados (cod_empleadoid, fec_ingreso, fec_salida, bol_estatus, cod_empleado_fk, imp_sueldoimss, imp_honorarios, imp_finiquito, cod_tipoimss, cod_tipohonorarios, cod_banco, cod_sucursal, cod_cuenta, txt_descripcionbaja, aud_codcreadopor, aud_feccreacion, aud_codmodificadopor, aud_fecmodificacion, cod_clabe, des_validacion, cod_validaciones, cod_bancoh, cod_sucursalh, cod_cuentah, cod_clabeh, xml_historial, aud_fecvalidacion) VALUES (19, '2019-05-07', NULL, true, 19, 2284.74, 1000.00, 123.00, '1', '0', '123', 321, '12345', 'baja', 1, '2190-12-21', 1, '2019-09-30', '12345', true, 'a', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO sgnom.tsgnomempleados (cod_empleadoid, fec_ingreso, fec_salida, bol_estatus, cod_empleado_fk, imp_sueldoimss, imp_honorarios, imp_finiquito, cod_tipoimss, cod_tipohonorarios, cod_banco, cod_sucursal, cod_cuenta, txt_descripcionbaja, aud_codcreadopor, aud_feccreacion, aud_codmodificadopor, aud_fecmodificacion, cod_clabe, des_validacion, cod_validaciones, cod_bancoh, cod_sucursalh, cod_cuentah, cod_clabeh, xml_historial, aud_fecvalidacion) VALUES (20, '2019-05-07', NULL, true, 20, 1672.91, 2000.00, 123.00, '0', '0', '123', 321, '12345', 'baja', 1, '2190-12-21', 1, '2019-09-30', '12345', true, 'a', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO sgnom.tsgnomempleados (cod_empleadoid, fec_ingreso, fec_salida, bol_estatus, cod_empleado_fk, imp_sueldoimss, imp_honorarios, imp_finiquito, cod_tipoimss, cod_tipohonorarios, cod_banco, cod_sucursal, cod_cuenta, txt_descripcionbaja, aud_codcreadopor, aud_feccreacion, aud_codmodificadopor, aud_fecmodificacion, cod_clabe, des_validacion, cod_validaciones, cod_bancoh, cod_sucursalh, cod_cuentah, cod_clabeh, xml_historial, aud_fecvalidacion) VALUES (21, '2019-05-07', NULL, true, 21, 1672.91, 2000.00, 123.00, '1', '0', '123', 321, '12345', 'baja', 1, '2190-12-21', 1, '2019-09-30', '12345', true, 'a', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO sgnom.tsgnomempleados (cod_empleadoid, fec_ingreso, fec_salida, bol_estatus, cod_empleado_fk, imp_sueldoimss, imp_honorarios, imp_finiquito, cod_tipoimss, cod_tipohonorarios, cod_banco, cod_sucursal, cod_cuenta, txt_descripcionbaja, aud_codcreadopor, aud_feccreacion, aud_codmodificadopor, aud_fecmodificacion, cod_clabe, des_validacion, cod_validaciones, cod_bancoh, cod_sucursalh, cod_cuentah, cod_clabeh, xml_historial, aud_fecvalidacion) VALUES (18, '2019-05-07', NULL, true, 18, 1596.87, 1234.00, 123.00, '1', '0', '123', 321, '12345', 'baja', 1, '2190-12-21', 10, '2019-11-29', '12345', true, 'a', NULL, NULL, NULL, NULL, '<mx.org.mbn.serp.historialBindingXml.Datos><historial>
                            <mx.org.mbn.serp.historialBindingXml.Dato>
                                <fecha>29-11-2019</fecha>
                                <sueldoimss>1.00</sueldoimss>
                                <sueldoadicional>1234.00</sueldoadicional>
                                <estatus>d</estatus>
                            </mx.org.mbn.serp.historialBindingXml.Dato>
                        </historial>
                    <historial>
                            <mx.org.mbn.serp.historialBindingXml.Dato>
                                <fecha>29-11-2019</fecha>
                                <sueldoimss>1.00</sueldoimss>
                                <sueldoadicional>1234.00</sueldoadicional>
                                <estatus>d</estatus>
                            </mx.org.mbn.serp.historialBindingXml.Dato>
                        </historial>
                    <historial>
                            <mx.org.mbn.serp.historialBindingXml.Dato>
                                <fecha>29-11-2019</fecha>
                                <sueldoimss>1.00</sueldoimss>
                                <sueldoadicional>1234.00</sueldoadicional>
                                <estatus>d</estatus>
                            </mx.org.mbn.serp.historialBindingXml.Dato>
                        </historial>
                    <historial>
                            <mx.org.mbn.serp.historialBindingXml.Dato>
                                <fecha>29-11-2019</fecha>
                                <sueldoimss>1.00</sueldoimss>
                                <sueldoadicional>1234.00</sueldoadicional>
                                <estatus>d</estatus>
                            </mx.org.mbn.serp.historialBindingXml.Dato>
                        </historial>
                    </mx.org.mbn.serp.historialBindingXml.Datos>', NULL);
INSERT INTO sgnom.tsgnomempleados (cod_empleadoid, fec_ingreso, fec_salida, bol_estatus, cod_empleado_fk, imp_sueldoimss, imp_honorarios, imp_finiquito, cod_tipoimss, cod_tipohonorarios, cod_banco, cod_sucursal, cod_cuenta, txt_descripcionbaja, aud_codcreadopor, aud_feccreacion, aud_codmodificadopor, aud_fecmodificacion, cod_clabe, des_validacion, cod_validaciones, cod_bancoh, cod_sucursalh, cod_cuentah, cod_clabeh, xml_historial, aud_fecvalidacion) VALUES (13, '2019-05-07', '2020-05-10', true, 13, 2585.41, 213.00, 123.00, '1', 'o', '123', 321, '12345', 'baja', 1, '2190-12-21', 1, '2019-09-30', '12345', true, 'a', NULL, NULL, NULL, NULL, '
    <historial>
        <mx.org.mbn.serp.historialBindingXml.Datos>
            <fecha>27-11-2019</fecha>
            <sueldoimss>10000000.00</sueldoimss>
            <sueldoadicional>213.00</sueldoadicional>
            <estatus>d</estatus>
        </mx.org.mbn.serp.historialBindingXml.Datos>
    </historial>
    <historial>
        <mx.org.mbn.serp.historialBindingXml.Datos>
            <fecha>27-11-2019</fecha>
            <sueldoimss>10000000.00</sueldoimss>
            <sueldoadicional>213.00</sueldoadicional>
            <estatus>d</estatus>
        </mx.org.mbn.serp.historialBindingXml.Datos>
    </historial>
    <historial>
        <mx.org.mbn.serp.historialBindingXml.Datos>
            <fecha>27-11-2019</fecha>
            <sueldoimss>10000000.00</sueldoimss>
            <sueldoadicional>213.00</sueldoadicional>
            <estatus>d</estatus>
        </mx.org.mbn.serp.historialBindingXml.Datos>
    </historial>
    <historial>
        <mx.org.mbn.serp.historialBindingXml.Datos>
            <fecha>27-11-2019</fecha>
            <sueldoimss>1.00</sueldoimss>
            <sueldoadicional>213.00</sueldoadicional>
            <estatus>d</estatus>
        </mx.org.mbn.serp.historialBindingXml.Datos>
    </historial>', NULL);
INSERT INTO sgnom.tsgnomempleados (cod_empleadoid, fec_ingreso, fec_salida, bol_estatus, cod_empleado_fk, imp_sueldoimss, imp_honorarios, imp_finiquito, cod_tipoimss, cod_tipohonorarios, cod_banco, cod_sucursal, cod_cuenta, txt_descripcionbaja, aud_codcreadopor, aud_feccreacion, aud_codmodificadopor, aud_fecmodificacion, cod_clabe, des_validacion, cod_validaciones, cod_bancoh, cod_sucursalh, cod_cuentah, cod_clabeh, xml_historial, aud_fecvalidacion) VALUES (14, '2019-05-07', NULL, true, 14, 8516.65, 213.00, 123.00, '1', 'o', '123', 321, '12345', 'baja', 1, '2190-12-21', 1, '2019-09-30', '12345', true, 'a', NULL, NULL, NULL, NULL, '
<mx.org.mbn.serp.historiaBindingXml.Dato>
    <historial>
        <mx.org.mbn.serp.historialBindingXml.Datos>
            <fecha>27-11-2019</fecha>
            <sueldoimss>10000000.00</sueldoimss>
            <sueldoadicional>213.00</sueldoadicional>
            <estatus>d</estatus>
        </mx.org.mbn.serp.historialBindingXml.Datos>
    </historial>
    <historial>
        <mx.org.mbn.serp.historialBindingXml.Datos>
            <fecha>27-11-2019</fecha>
            <sueldoimss>10000000.00</sueldoimss>
            <sueldoadicional>213.00</sueldoadicional>
            <estatus>d</estatus>
        </mx.org.mbn.serp.historialBindingXml.Datos>
    </historial>
    <historial>
        <mx.org.mbn.serp.historialBindingXml.Datos>
            <fecha>27-11-2019</fecha>
            <sueldoimss>10000000.00</sueldoimss>
            <sueldoadicional>213.00</sueldoadicional>
            <estatus>d</estatus>
        </mx.org.mbn.serp.historialBindingXml.Datos>
    </historial>
    <historial>
        <mx.org.mbn.serp.historialBindingXml.Datos>
            <fecha>27-11-2019</fecha>
            <sueldoimss>1.00</sueldoimss>
            <sueldoadicional>213.00</sueldoadicional>
            <estatus>d</estatus>
        </mx.org.mbn.serp.historialBindingXml.Datos>
    </historial>
</mx.org.mbn.serp.historiaBindingXml.Dato>', NULL);
INSERT INTO sgnom.tsgnomempleados (cod_empleadoid, fec_ingreso, fec_salida, bol_estatus, cod_empleado_fk, imp_sueldoimss, imp_honorarios, imp_finiquito, cod_tipoimss, cod_tipohonorarios, cod_banco, cod_sucursal, cod_cuenta, txt_descripcionbaja, aud_codcreadopor, aud_feccreacion, aud_codmodificadopor, aud_fecmodificacion, cod_clabe, des_validacion, cod_validaciones, cod_bancoh, cod_sucursalh, cod_cuentah, cod_clabeh, xml_historial, aud_fecvalidacion) VALUES (10, '2019-05-07', NULL, true, 10, 883.32, 12000.00, 123.00, '1', '0', '123', 321, '12345', 'baja', 1, '2190-12-21', 1, '2019-11-13', '12345', true, 'a', NULL, NULL, NULL, NULL, '
<mx.org.mbn.serp.historiaBindingXml.Dato>
    <historial>
        <mx.org.mbn.serp.historialBindingXml.Datos>
            <fecha>27-11-2019</fecha>
            <sueldoimss>10000000.00</sueldoimss>
            <sueldoadicional>213.00</sueldoadicional>
            <estatus>d</estatus>
        </mx.org.mbn.serp.historialBindingXml.Datos>
    </historial>
    <historial>
        <mx.org.mbn.serp.historialBindingXml.Datos>
            <fecha>27-11-2019</fecha>
            <sueldoimss>10000000.00</sueldoimss>
            <sueldoadicional>213.00</sueldoadicional>
            <estatus>d</estatus>
        </mx.org.mbn.serp.historialBindingXml.Datos>
    </historial>
    <historial>
        <mx.org.mbn.serp.historialBindingXml.Datos>
            <fecha>27-11-2019</fecha>
            <sueldoimss>10000000.00</sueldoimss>
            <sueldoadicional>213.00</sueldoadicional>
            <estatus>d</estatus>
        </mx.org.mbn.serp.historialBindingXml.Datos>
    </historial>
    <historial>
        <mx.org.mbn.serp.historialBindingXml.Datos>
            <fecha>27-11-2019</fecha>
            <sueldoimss>1.00</sueldoimss>
            <sueldoadicional>213.00</sueldoadicional>
            <estatus>d</estatus>
        </mx.org.mbn.serp.historialBindingXml.Datos>
    </historial>
</mx.org.mbn.serp.historiaBindingXml.Dato>', NULL);


--
-- TOC entry 4800 (class 0 OID 102068)
-- Dependencies: 388
-- Data for Name: tsgnomempquincena; Type: TABLE DATA; Schema: sgnom; Owner: suite
--

INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (239, 1, 42, 20000.00, 0.00, 20000.00, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (240, 10, 42, 6000.00, 0.00, 6000.00, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (241, 11, 42, 4100.00, 0.00, 4100.00, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (242, 12, 42, 106.50, 0.00, 106.50, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (243, 13, 42, 106.50, 0.00, 106.50, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (244, 14, 42, 106.50, 0.00, 106.50, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (245, 15, 42, 500.00, 0.00, 500.00, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (246, 16, 42, 106.50, 0.00, 106.50, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (247, 17, 42, 1106.50, 0.00, 1106.50, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (248, 18, 42, 617.00, 0.00, 617.00, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (249, 19, 42, 500.00, 0.00, 500.00, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (250, 20, 42, 1000.00, 0.00, 1000.00, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (251, 21, 42, 1000.00, 0.00, 1000.00, true);


--
-- TOC entry 4801 (class 0 OID 102072)
-- Dependencies: 389
-- Data for Name: tsgnomempquincenaht; Type: TABLE DATA; Schema: sgnom; Owner: suite
--



--
-- TOC entry 4802 (class 0 OID 102075)
-- Dependencies: 390
-- Data for Name: tsgnomestatusnom; Type: TABLE DATA; Schema: sgnom; Owner: suite
--

INSERT INTO sgnom.tsgnomestatusnom (cod_estatusnomid, cod_estatusnomina, bol_estatus) VALUES (2, 'calculada', true);
INSERT INTO sgnom.tsgnomestatusnom (cod_estatusnomid, cod_estatusnomina, bol_estatus) VALUES (1, 'abierta', true);
INSERT INTO sgnom.tsgnomestatusnom (cod_estatusnomid, cod_estatusnomina, bol_estatus) VALUES (3, 'revision', true);
INSERT INTO sgnom.tsgnomestatusnom (cod_estatusnomid, cod_estatusnomina, bol_estatus) VALUES (4, 'validada', true);
INSERT INTO sgnom.tsgnomestatusnom (cod_estatusnomid, cod_estatusnomina, bol_estatus) VALUES (5, 'cerrada', true);
INSERT INTO sgnom.tsgnomestatusnom (cod_estatusnomid, cod_estatusnomina, bol_estatus) VALUES (6, 'eliminada', true);


--
-- TOC entry 4803 (class 0 OID 102078)
-- Dependencies: 391
-- Data for Name: tsgnomformula; Type: TABLE DATA; Schema: sgnom; Owner: suite
--

INSERT INTO sgnom.tsgnomformula (cod_formulaid, des_nbformula, des_formula, bol_estatus) VALUES (9, 'SUELDO QUINCENA', ':(:SUELDOBASE:/:DIASDELMES:):*:DIASLABORADOS', true);
INSERT INTO sgnom.tsgnomformula (cod_formulaid, des_nbformula, des_formula, bol_estatus) VALUES (11, 'YY', ':DIASDELMES:+:SUELDOBASE', true);
INSERT INTO sgnom.tsgnomformula (cod_formulaid, des_nbformula, des_formula, bol_estatus) VALUES (12, 'HHHHH', ':DIASDELMES', true);
INSERT INTO sgnom.tsgnomformula (cod_formulaid, des_nbformula, des_formula, bol_estatus) VALUES (13, 'DGSDG', ':DIASDELMES:*:SUELDOBASE', true);
INSERT INTO sgnom.tsgnomformula (cod_formulaid, des_nbformula, des_formula, bol_estatus) VALUES (10, 'QW', ':(:SUELDOBASE:/:con proyecto:):*:DIASLABORADOS', true);
INSERT INTO sgnom.tsgnomformula (cod_formulaid, des_nbformula, des_formula, bol_estatus) VALUES (14, 'AGUINALDO', ':SALARIODIARIO:*:DIASAGUINALDO', true);
INSERT INTO sgnom.tsgnomformula (cod_formulaid, des_nbformula, des_formula, bol_estatus) VALUES (15, 'AGUINALDO', ':SALARIODIARIO:*:DIASAGUINALDO', true);


--
-- TOC entry 4804 (class 0 OID 102081)
-- Dependencies: 392
-- Data for Name: tsgnomfuncion; Type: TABLE DATA; Schema: sgnom; Owner: suite
--

INSERT INTO sgnom.tsgnomfuncion (cod_funcionid, cod_nbfuncion, bol_estatus) VALUES (1, 'fn_sueldo_base', true);
INSERT INTO sgnom.tsgnomfuncion (cod_funcionid, cod_nbfuncion, bol_estatus) VALUES (2, 'fn_dias_laborados', true);
INSERT INTO sgnom.tsgnomfuncion (cod_funcionid, cod_nbfuncion, bol_estatus) VALUES (3, 'fn_bono_asistencia_imss', true);
INSERT INTO sgnom.tsgnomfuncion (cod_funcionid, cod_nbfuncion, bol_estatus) VALUES (4, 'fn_bono_puntualidad_imss', true);
INSERT INTO sgnom.tsgnomfuncion (cod_funcionid, cod_nbfuncion, bol_estatus) VALUES (5, 'fn_cuota_quin_imss', true);
INSERT INTO sgnom.tsgnomfuncion (cod_funcionid, cod_nbfuncion, bol_estatus) VALUES (6, 'fn_infonavit_imss', true);
INSERT INTO sgnom.tsgnomfuncion (cod_funcionid, cod_nbfuncion, bol_estatus) VALUES (7, 'fn_isr_imss', true);
INSERT INTO sgnom.tsgnomfuncion (cod_funcionid, cod_nbfuncion, bol_estatus) VALUES (8, 'fn_prestamo_personal_imss', true);
INSERT INTO sgnom.tsgnomfuncion (cod_funcionid, cod_nbfuncion, bol_estatus) VALUES (9, 'fn_prima_vacacional_imss', true);
INSERT INTO sgnom.tsgnomfuncion (cod_funcionid, cod_nbfuncion, bol_estatus) VALUES (10, 'fn_subsidio_empleo_imss', true);
INSERT INTO sgnom.tsgnomfuncion (cod_funcionid, cod_nbfuncion, bol_estatus) VALUES (11, 'fn_sueldo_imss', true);
INSERT INTO sgnom.tsgnomfuncion (cod_funcionid, cod_nbfuncion, bol_estatus) VALUES (12, 'fn_vacaciones_imss', true);
INSERT INTO sgnom.tsgnomfuncion (cod_funcionid, cod_nbfuncion, bol_estatus) VALUES (13, 'fn_salario_diario', true);


--
-- TOC entry 4805 (class 0 OID 102084)
-- Dependencies: 393
-- Data for Name: tsgnomhisttabla; Type: TABLE DATA; Schema: sgnom; Owner: suite
--



--
-- TOC entry 4806 (class 0 OID 102087)
-- Dependencies: 394
-- Data for Name: tsgnomincidencia; Type: TABLE DATA; Schema: sgnom; Owner: suite
--

INSERT INTO sgnom.tsgnomincidencia (cod_incidenciaid, cod_catincidenciaid_fk, cnu_cantidad, des_actividad, txt_comentarios, cod_empreporta_fk, cod_empautoriza_fk, imp_monto, xml_detcantidad, bol_estatus, cod_quincenaid_fk, bol_validacion, fec_validacion, aud_codcreadopor, aud_feccreacion, aud_codmodificadopor, aud_fecmodificacion, bol_aceptacion, bol_pago) VALUES (75, 33, 6, '', 'sdfsd', 13, 10, 900.00, '                
		<DetalleFechas><fecha>05-12-2019</fecha></DetalleFechas>', true, 23, true, '2019-12-10', 13, '2019-12-10', 11, '2019-12-10', true, true);
INSERT INTO sgnom.tsgnomincidencia (cod_incidenciaid, cod_catincidenciaid_fk, cnu_cantidad, des_actividad, txt_comentarios, cod_empreporta_fk, cod_empautoriza_fk, imp_monto, xml_detcantidad, bol_estatus, cod_quincenaid_fk, bol_validacion, fec_validacion, aud_codcreadopor, aud_feccreacion, aud_codmodificadopor, aud_fecmodificacion, bol_aceptacion, bol_pago) VALUES (76, 34, 13, '', 'sgs', 13, 10, 1300.00, '                
		<DetalleFechas><fecha>11-12-2019</fecha></DetalleFechas>', true, 23, true, '2019-12-10', 13, '2019-12-10', 11, '2019-12-10', true, true);
INSERT INTO sgnom.tsgnomincidencia (cod_incidenciaid, cod_catincidenciaid_fk, cnu_cantidad, des_actividad, txt_comentarios, cod_empreporta_fk, cod_empautoriza_fk, imp_monto, xml_detcantidad, bol_estatus, cod_quincenaid_fk, bol_validacion, fec_validacion, aud_codcreadopor, aud_feccreacion, aud_codmodificadopor, aud_fecmodificacion, bol_aceptacion, bol_pago) VALUES (77, 33, 1, '', 'ghf Pago pospuesto por FINANZAS: hgvg', 12, NULL, 0.00, '                
		<DetalleFechas><fecha>06-12-2019</fecha></DetalleFechas>', true, 24, NULL, NULL, 12, '2019-12-10', 11, '2019-12-10', NULL, NULL);
INSERT INTO sgnom.tsgnomincidencia (cod_incidenciaid, cod_catincidenciaid_fk, cnu_cantidad, des_actividad, txt_comentarios, cod_empreporta_fk, cod_empautoriza_fk, imp_monto, xml_detcantidad, bol_estatus, cod_quincenaid_fk, bol_validacion, fec_validacion, aud_codcreadopor, aud_feccreacion, aud_codmodificadopor, aud_fecmodificacion, bol_aceptacion, bol_pago) VALUES (78, 33, 2, '', 'acsc Pago pospuesto por FINANZAS: kjn
\', 11, NULL, 0.00, '                
		<DetalleFechas><fecha>04-12-2019</fecha></DetalleFechas>', true, 24, NULL, NULL, 11, '2019-12-10', 11, '2019-12-10', NULL, NULL);
INSERT INTO sgnom.tsgnomincidencia (cod_incidenciaid, cod_catincidenciaid_fk, cnu_cantidad, des_actividad, txt_comentarios, cod_empreporta_fk, cod_empautoriza_fk, imp_monto, xml_detcantidad, bol_estatus, cod_quincenaid_fk, bol_validacion, fec_validacion, aud_codcreadopor, aud_feccreacion, aud_codmodificadopor, aud_fecmodificacion, bol_aceptacion, bol_pago) VALUES (79, 33, 4, '', 'gf Pago pospuesto por FINANZAS: 15', 10, NULL, 0.00, '                
		<DetalleFechas><fecha>11-12-2019</fecha></DetalleFechas>', true, 24, NULL, NULL, 10, '2019-12-10', 11, '2019-12-10', NULL, NULL);
INSERT INTO sgnom.tsgnomincidencia (cod_incidenciaid, cod_catincidenciaid_fk, cnu_cantidad, des_actividad, txt_comentarios, cod_empreporta_fk, cod_empautoriza_fk, imp_monto, xml_detcantidad, bol_estatus, cod_quincenaid_fk, bol_validacion, fec_validacion, aud_codcreadopor, aud_feccreacion, aud_codmodificadopor, aud_fecmodificacion, bol_aceptacion, bol_pago) VALUES (80, 33, 3, '', 'frdf Pago pospuesto por FINANZAS: 55', 14, NULL, 0.00, '                
		<DetalleFechas><fecha>04-12-2019</fecha></DetalleFechas>', true, 24, NULL, NULL, 14, '2019-12-10', 11, '2019-12-10', NULL, NULL);


--
-- TOC entry 4807 (class 0 OID 102093)
-- Dependencies: 395
-- Data for Name: tsgnommanterceros; Type: TABLE DATA; Schema: sgnom; Owner: suite
--



--
-- TOC entry 4811 (class 0 OID 102447)
-- Dependencies: 399
-- Data for Name: tsgnomnominaimss; Type: TABLE DATA; Schema: sgnom; Owner: suite
--



--
-- TOC entry 4808 (class 0 OID 102102)
-- Dependencies: 396
-- Data for Name: tsgnomquincena; Type: TABLE DATA; Schema: sgnom; Owner: suite
--

INSERT INTO sgnom.tsgnomquincena (cod_quincenaid, des_quincena, fec_inicio, fec_fin, fec_pago, fec_dispersion, cnu_numquincena, cod_ejercicioid_fk, bol_estatus) VALUES (1, '1era de enero', '2019-01-01', '2019-01-15', '2019-01-01', '2019-01-15', 1, 6, true);
INSERT INTO sgnom.tsgnomquincena (cod_quincenaid, des_quincena, fec_inicio, fec_fin, fec_pago, fec_dispersion, cnu_numquincena, cod_ejercicioid_fk, bol_estatus) VALUES (2, '2da de enero', '2019-01-16', '2019-01-31', '2019-01-16', '2019-01-31', 2, 6, true);
INSERT INTO sgnom.tsgnomquincena (cod_quincenaid, des_quincena, fec_inicio, fec_fin, fec_pago, fec_dispersion, cnu_numquincena, cod_ejercicioid_fk, bol_estatus) VALUES (3, '1era de febrero', '2019-02-01', '2019-02-15', '2019-02-01', '2019-02-15', 3, 6, true);
INSERT INTO sgnom.tsgnomquincena (cod_quincenaid, des_quincena, fec_inicio, fec_fin, fec_pago, fec_dispersion, cnu_numquincena, cod_ejercicioid_fk, bol_estatus) VALUES (4, '2da de febrero', '2019-02-16', '2019-02-28', '2019-02-16', '2019-02-28', 4, 6, true);
INSERT INTO sgnom.tsgnomquincena (cod_quincenaid, des_quincena, fec_inicio, fec_fin, fec_pago, fec_dispersion, cnu_numquincena, cod_ejercicioid_fk, bol_estatus) VALUES (5, '1era de marzo', '2019-03-01', '2019-03-15', '2019-03-01', '2019-03-15', 5, 6, true);
INSERT INTO sgnom.tsgnomquincena (cod_quincenaid, des_quincena, fec_inicio, fec_fin, fec_pago, fec_dispersion, cnu_numquincena, cod_ejercicioid_fk, bol_estatus) VALUES (6, '2da de marzo', '2019-03-16', '2019-03-31', '2019-03-16', '2019-03-31', 6, 6, true);
INSERT INTO sgnom.tsgnomquincena (cod_quincenaid, des_quincena, fec_inicio, fec_fin, fec_pago, fec_dispersion, cnu_numquincena, cod_ejercicioid_fk, bol_estatus) VALUES (7, '1era de abril', '2019-04-01', '2019-04-15', '2019-04-01', '2019-04-15', 7, 6, true);
INSERT INTO sgnom.tsgnomquincena (cod_quincenaid, des_quincena, fec_inicio, fec_fin, fec_pago, fec_dispersion, cnu_numquincena, cod_ejercicioid_fk, bol_estatus) VALUES (8, '2da de abril', '2019-04-16', '2019-04-30', '2019-04-16', '2019-04-30', 8, 6, true);
INSERT INTO sgnom.tsgnomquincena (cod_quincenaid, des_quincena, fec_inicio, fec_fin, fec_pago, fec_dispersion, cnu_numquincena, cod_ejercicioid_fk, bol_estatus) VALUES (9, '1era de mayo', '2019-05-01', '2019-05-15', '2019-05-01', '2019-05-15', 9, 6, true);
INSERT INTO sgnom.tsgnomquincena (cod_quincenaid, des_quincena, fec_inicio, fec_fin, fec_pago, fec_dispersion, cnu_numquincena, cod_ejercicioid_fk, bol_estatus) VALUES (10, '2da de mayo', '2019-05-16', '2019-05-31', '2019-05-16', '2019-05-31', 10, 6, true);
INSERT INTO sgnom.tsgnomquincena (cod_quincenaid, des_quincena, fec_inicio, fec_fin, fec_pago, fec_dispersion, cnu_numquincena, cod_ejercicioid_fk, bol_estatus) VALUES (11, '1era de junio', '2019-06-01', '2019-06-15', '2019-06-01', '2019-06-15', 11, 6, true);
INSERT INTO sgnom.tsgnomquincena (cod_quincenaid, des_quincena, fec_inicio, fec_fin, fec_pago, fec_dispersion, cnu_numquincena, cod_ejercicioid_fk, bol_estatus) VALUES (12, '2da de junio', '2019-06-16', '2019-06-30', '2019-06-16', '2019-06-30', 12, 6, true);
INSERT INTO sgnom.tsgnomquincena (cod_quincenaid, des_quincena, fec_inicio, fec_fin, fec_pago, fec_dispersion, cnu_numquincena, cod_ejercicioid_fk, bol_estatus) VALUES (13, '1era de julio', '2019-07-01', '2019-07-15', '2019-07-01', '2019-07-15', 13, 6, true);
INSERT INTO sgnom.tsgnomquincena (cod_quincenaid, des_quincena, fec_inicio, fec_fin, fec_pago, fec_dispersion, cnu_numquincena, cod_ejercicioid_fk, bol_estatus) VALUES (14, '2da de julio', '2019-07-16', '2019-07-31', '2019-07-16', '2019-07-31', 14, 6, true);
INSERT INTO sgnom.tsgnomquincena (cod_quincenaid, des_quincena, fec_inicio, fec_fin, fec_pago, fec_dispersion, cnu_numquincena, cod_ejercicioid_fk, bol_estatus) VALUES (15, '1era de agosto', '2019-08-01', '2019-08-15', '2019-08-01', '2019-08-15', 15, 6, true);
INSERT INTO sgnom.tsgnomquincena (cod_quincenaid, des_quincena, fec_inicio, fec_fin, fec_pago, fec_dispersion, cnu_numquincena, cod_ejercicioid_fk, bol_estatus) VALUES (16, '2da de agosto', '2019-08-16', '2019-08-31', '2019-08-16', '2019-08-31', 16, 6, true);
INSERT INTO sgnom.tsgnomquincena (cod_quincenaid, des_quincena, fec_inicio, fec_fin, fec_pago, fec_dispersion, cnu_numquincena, cod_ejercicioid_fk, bol_estatus) VALUES (17, '1era de septiembre', '2019-09-01', '2019-09-15', '2019-09-01', '2019-09-15', 17, 6, true);
INSERT INTO sgnom.tsgnomquincena (cod_quincenaid, des_quincena, fec_inicio, fec_fin, fec_pago, fec_dispersion, cnu_numquincena, cod_ejercicioid_fk, bol_estatus) VALUES (18, '2da de septiembre', '2019-09-16', '2019-09-30', '2019-09-16', '2019-09-30', 18, 6, true);
INSERT INTO sgnom.tsgnomquincena (cod_quincenaid, des_quincena, fec_inicio, fec_fin, fec_pago, fec_dispersion, cnu_numquincena, cod_ejercicioid_fk, bol_estatus) VALUES (19, '1era de octubre', '2019-10-01', '2019-10-15', '2019-10-01', '2019-10-15', 19, 6, true);
INSERT INTO sgnom.tsgnomquincena (cod_quincenaid, des_quincena, fec_inicio, fec_fin, fec_pago, fec_dispersion, cnu_numquincena, cod_ejercicioid_fk, bol_estatus) VALUES (20, '2da de octubre', '2019-10-16', '2019-10-31', '2019-10-16', '2019-10-31', 20, 6, true);
INSERT INTO sgnom.tsgnomquincena (cod_quincenaid, des_quincena, fec_inicio, fec_fin, fec_pago, fec_dispersion, cnu_numquincena, cod_ejercicioid_fk, bol_estatus) VALUES (21, '1era de noviembre', '2019-11-01', '2019-11-15', '2019-11-01', '2019-11-15', 21, 6, true);
INSERT INTO sgnom.tsgnomquincena (cod_quincenaid, des_quincena, fec_inicio, fec_fin, fec_pago, fec_dispersion, cnu_numquincena, cod_ejercicioid_fk, bol_estatus) VALUES (22, '2da de noviembre', '2019-11-16', '2019-11-30', '2019-11-16', '2019-11-30', 22, 6, true);
INSERT INTO sgnom.tsgnomquincena (cod_quincenaid, des_quincena, fec_inicio, fec_fin, fec_pago, fec_dispersion, cnu_numquincena, cod_ejercicioid_fk, bol_estatus) VALUES (23, '1era de diciembre', '2019-12-01', '2019-12-15', '2019-12-01', '2019-12-15', 23, 6, true);
INSERT INTO sgnom.tsgnomquincena (cod_quincenaid, des_quincena, fec_inicio, fec_fin, fec_pago, fec_dispersion, cnu_numquincena, cod_ejercicioid_fk, bol_estatus) VALUES (24, '2da de diciembre', '2019-12-16', '2019-12-31', '2019-12-16', '2019-12-31', 24, 6, true);


--
-- TOC entry 4809 (class 0 OID 102105)
-- Dependencies: 397
-- Data for Name: tsgnomtipoconcepto; Type: TABLE DATA; Schema: sgnom; Owner: suite
--

INSERT INTO sgnom.tsgnomtipoconcepto (cod_tipoconceptoid, cod_tipoconcepto, bol_estatus) VALUES (1, 'Deducción', true);
INSERT INTO sgnom.tsgnomtipoconcepto (cod_tipoconceptoid, cod_tipoconcepto, bol_estatus) VALUES (2, 'Percepción', true);


--
-- TOC entry 4810 (class 0 OID 102108)
-- Dependencies: 398
-- Data for Name: tsgnomtiponomina; Type: TABLE DATA; Schema: sgnom; Owner: suite
--

INSERT INTO sgnom.tsgnomtiponomina (cod_tiponominaid, cod_nomina, bol_estatus) VALUES (2, 'Aguinaldo', true);
INSERT INTO sgnom.tsgnomtiponomina (cod_tiponominaid, cod_nomina, bol_estatus) VALUES (1, 'General', true);
INSERT INTO sgnom.tsgnomtiponomina (cod_tiponominaid, cod_nomina, bol_estatus) VALUES (3, 'test', true);
INSERT INTO sgnom.tsgnomtiponomina (cod_tiponominaid, cod_nomina, bol_estatus) VALUES (4, 'mp', true);


--
-- TOC entry 4677 (class 0 OID 99411)
-- Dependencies: 265
-- Data for Name: tsgrhareas; Type: TABLE DATA; Schema: sgrh; Owner: suite
--

INSERT INTO sgrh.tsgrhareas (cod_area, des_nbarea, cod_acronimo, cnu_activo, cod_sistemasuite, cod_creadopor, cod_modificadopor, fec_creacion, fec_modificado) VALUES (1, 'BASE DE DATOS', 'DB', true, NULL, NULL, NULL, NULL, NULL);
INSERT INTO sgrh.tsgrhareas (cod_area, des_nbarea, cod_acronimo, cnu_activo, cod_sistemasuite, cod_creadopor, cod_modificadopor, fec_creacion, fec_modificado) VALUES (2, 'FABRICA DE SOFTWARE', 'FS', true, NULL, NULL, NULL, NULL, NULL);
INSERT INTO sgrh.tsgrhareas (cod_area, des_nbarea, cod_acronimo, cnu_activo, cod_sistemasuite, cod_creadopor, cod_modificadopor, fec_creacion, fec_modificado) VALUES (3, 'DISEÑO', 'DS', true, NULL, NULL, NULL, NULL, NULL);
INSERT INTO sgrh.tsgrhareas (cod_area, des_nbarea, cod_acronimo, cnu_activo, cod_sistemasuite, cod_creadopor, cod_modificadopor, fec_creacion, fec_modificado) VALUES (4, 'SOPORTE TECNICO', 'ST', true, NULL, NULL, NULL, NULL, NULL);
INSERT INTO sgrh.tsgrhareas (cod_area, des_nbarea, cod_acronimo, cnu_activo, cod_sistemasuite, cod_creadopor, cod_modificadopor, fec_creacion, fec_modificado) VALUES (5, 'RECURSOS HUMANOS', 'RH', true, NULL, NULL, NULL, NULL, NULL);
INSERT INTO sgrh.tsgrhareas (cod_area, des_nbarea, cod_acronimo, cnu_activo, cod_sistemasuite, cod_creadopor, cod_modificadopor, fec_creacion, fec_modificado) VALUES (6, 'FINANZAS', 'FINAN', true, NULL, NULL, NULL, NULL, NULL);
INSERT INTO sgrh.tsgrhareas (cod_area, des_nbarea, cod_acronimo, cnu_activo, cod_sistemasuite, cod_creadopor, cod_modificadopor, fec_creacion, fec_modificado) VALUES (7, 'PROCESOS', 'PROCE', true, NULL, NULL, NULL, NULL, NULL);
INSERT INTO sgrh.tsgrhareas (cod_area, des_nbarea, cod_acronimo, cnu_activo, cod_sistemasuite, cod_creadopor, cod_modificadopor, fec_creacion, fec_modificado) VALUES (8, 'CONOCIMIENTO DE LA ORGANIZACION', 'CO', true, NULL, NULL, NULL, NULL, NULL);
INSERT INTO sgrh.tsgrhareas (cod_area, des_nbarea, cod_acronimo, cnu_activo, cod_sistemasuite, cod_creadopor, cod_modificadopor, fec_creacion, fec_modificado) VALUES (9, 'ASEGURAMIENTO DE LA CALIDAD ', 'AC', true, NULL, NULL, NULL, NULL, NULL);
INSERT INTO sgrh.tsgrhareas (cod_area, des_nbarea, cod_acronimo, cnu_activo, cod_sistemasuite, cod_creadopor, cod_modificadopor, fec_creacion, fec_modificado) VALUES (10, 'CIDT', 'CIDT', true, NULL, NULL, NULL, NULL, NULL);
INSERT INTO sgrh.tsgrhareas (cod_area, des_nbarea, cod_acronimo, cnu_activo, cod_sistemasuite, cod_creadopor, cod_modificadopor, fec_creacion, fec_modificado) VALUES (11, 'MARKETING', 'MARKE', true, NULL, NULL, NULL, NULL, NULL);
INSERT INTO sgrh.tsgrhareas (cod_area, des_nbarea, cod_acronimo, cnu_activo, cod_sistemasuite, cod_creadopor, cod_modificadopor, fec_creacion, fec_modificado) VALUES (12, 'SERVICIOS GENERALES DE LIMPIEZA', 'SGL', true, NULL, NULL, NULL, NULL, NULL);
INSERT INTO sgrh.tsgrhareas (cod_area, des_nbarea, cod_acronimo, cnu_activo, cod_sistemasuite, cod_creadopor, cod_modificadopor, fec_creacion, fec_modificado) VALUES (13, 'DESARROLLO Y MANTENIMIENTO DE SOFTWARE', 'SMS', true, NULL, NULL, NULL, NULL, NULL);
INSERT INTO sgrh.tsgrhareas (cod_area, des_nbarea, cod_acronimo, cnu_activo, cod_sistemasuite, cod_creadopor, cod_modificadopor, fec_creacion, fec_modificado) VALUES (14, 'DBA', 'DBA', true, NULL, NULL, NULL, NULL, NULL);
INSERT INTO sgrh.tsgrhareas (cod_area, des_nbarea, cod_acronimo, cnu_activo, cod_sistemasuite, cod_creadopor, cod_modificadopor, fec_creacion, fec_modificado) VALUES (15, 'VENTAS GOBIERNO', 'VG', true, NULL, NULL, NULL, NULL, NULL);
INSERT INTO sgrh.tsgrhareas (cod_area, des_nbarea, cod_acronimo, cnu_activo, cod_sistemasuite, cod_creadopor, cod_modificadopor, fec_creacion, fec_modificado) VALUES (16, 'OUTSOURCING', 'OS', true, NULL, NULL, NULL, NULL, NULL);
INSERT INTO sgrh.tsgrhareas (cod_area, des_nbarea, cod_acronimo, cnu_activo, cod_sistemasuite, cod_creadopor, cod_modificadopor, fec_creacion, fec_modificado) VALUES (17, '3X3', '3X3', true, NULL, NULL, NULL, NULL, NULL);
INSERT INTO sgrh.tsgrhareas (cod_area, des_nbarea, cod_acronimo, cnu_activo, cod_sistemasuite, cod_creadopor, cod_modificadopor, fec_creacion, fec_modificado) VALUES (18, 'ERP', 'ERP', true, NULL, NULL, NULL, NULL, NULL);
INSERT INTO sgrh.tsgrhareas (cod_area, des_nbarea, cod_acronimo, cnu_activo, cod_sistemasuite, cod_creadopor, cod_modificadopor, fec_creacion, fec_modificado) VALUES (19, 'REALIDAD AUMENTADA', 'RA', true, NULL, NULL, NULL, NULL, NULL);
INSERT INTO sgrh.tsgrhareas (cod_area, des_nbarea, cod_acronimo, cnu_activo, cod_sistemasuite, cod_creadopor, cod_modificadopor, fec_creacion, fec_modificado) VALUES (20, 'ADMINISTRACION DE PROYECTOS ESPECIFICOS', 'APE', true, NULL, NULL, NULL, NULL, NULL);
INSERT INTO sgrh.tsgrhareas (cod_area, des_nbarea, cod_acronimo, cnu_activo, cod_sistemasuite, cod_creadopor, cod_modificadopor, fec_creacion, fec_modificado) VALUES (21, 'RECLUTAMIENTO Y SELECCION', 'R&S', true, NULL, NULL, NULL, NULL, NULL);
INSERT INTO sgrh.tsgrhareas (cod_area, des_nbarea, cod_acronimo, cnu_activo, cod_sistemasuite, cod_creadopor, cod_modificadopor, fec_creacion, fec_modificado) VALUES (22, 'GESTION DE PROYECTOS', 'GPY', true, NULL, NULL, NULL, NULL, NULL);
INSERT INTO sgrh.tsgrhareas (cod_area, des_nbarea, cod_acronimo, cnu_activo, cod_sistemasuite, cod_creadopor, cod_modificadopor, fec_creacion, fec_modificado) VALUES (23, 'OPERACIONES', 'OP', true, NULL, NULL, NULL, NULL, NULL);


--
-- TOC entry 4678 (class 0 OID 99415)
-- Dependencies: 266
-- Data for Name: tsgrhasignacion_encuesta; Type: TABLE DATA; Schema: sgrh; Owner: suite
--

INSERT INTO sgrh.tsgrhasignacion_encuesta (cod_asignacion, cod_empleado, cod_encuesta, cod_encuesta_realizada) VALUES (1, 10, 5, false);
INSERT INTO sgrh.tsgrhasignacion_encuesta (cod_asignacion, cod_empleado, cod_encuesta, cod_encuesta_realizada) VALUES (2, 32, 5, false);
INSERT INTO sgrh.tsgrhasignacion_encuesta (cod_asignacion, cod_empleado, cod_encuesta, cod_encuesta_realizada) VALUES (4, 34, 5, false);
INSERT INTO sgrh.tsgrhasignacion_encuesta (cod_asignacion, cod_empleado, cod_encuesta, cod_encuesta_realizada) VALUES (5, 33, 5, false);
INSERT INTO sgrh.tsgrhasignacion_encuesta (cod_asignacion, cod_empleado, cod_encuesta, cod_encuesta_realizada) VALUES (6, 13, 5, false);
INSERT INTO sgrh.tsgrhasignacion_encuesta (cod_asignacion, cod_empleado, cod_encuesta, cod_encuesta_realizada) VALUES (7, 28, 5, false);
INSERT INTO sgrh.tsgrhasignacion_encuesta (cod_asignacion, cod_empleado, cod_encuesta, cod_encuesta_realizada) VALUES (8, 11, 5, false);
INSERT INTO sgrh.tsgrhasignacion_encuesta (cod_asignacion, cod_empleado, cod_encuesta, cod_encuesta_realizada) VALUES (9, 31, 5, false);
INSERT INTO sgrh.tsgrhasignacion_encuesta (cod_asignacion, cod_empleado, cod_encuesta, cod_encuesta_realizada) VALUES (10, 30, 5, false);


--
-- TOC entry 4679 (class 0 OID 99419)
-- Dependencies: 267
-- Data for Name: tsgrhasignacionesemp; Type: TABLE DATA; Schema: sgrh; Owner: suite
--



--
-- TOC entry 4680 (class 0 OID 99423)
-- Dependencies: 268
-- Data for Name: tsgrhcapacitaciones; Type: TABLE DATA; Schema: sgrh; Owner: suite
--



--
-- TOC entry 4681 (class 0 OID 99430)
-- Dependencies: 269
-- Data for Name: tsgrhcartaasignacion; Type: TABLE DATA; Schema: sgrh; Owner: suite
--



--
-- TOC entry 4682 (class 0 OID 99439)
-- Dependencies: 270
-- Data for Name: tsgrhcatrespuestas; Type: TABLE DATA; Schema: sgrh; Owner: suite
--

INSERT INTO sgrh.tsgrhcatrespuestas (cod_catrespuesta, des_respuesta, cod_ponderacion) VALUES (1, 'Nunca', 1);
INSERT INTO sgrh.tsgrhcatrespuestas (cod_catrespuesta, des_respuesta, cod_ponderacion) VALUES (2, 'Algunas veces', 2);
INSERT INTO sgrh.tsgrhcatrespuestas (cod_catrespuesta, des_respuesta, cod_ponderacion) VALUES (3, 'Regular', 3);
INSERT INTO sgrh.tsgrhcatrespuestas (cod_catrespuesta, des_respuesta, cod_ponderacion) VALUES (4, 'Con frecuencia', 4);
INSERT INTO sgrh.tsgrhcatrespuestas (cod_catrespuesta, des_respuesta, cod_ponderacion) VALUES (5, 'Siempre', 5);
INSERT INTO sgrh.tsgrhcatrespuestas (cod_catrespuesta, des_respuesta, cod_ponderacion) VALUES (6, 'Muy malo', 1);
INSERT INTO sgrh.tsgrhcatrespuestas (cod_catrespuesta, des_respuesta, cod_ponderacion) VALUES (7, 'Malo', 2);
INSERT INTO sgrh.tsgrhcatrespuestas (cod_catrespuesta, des_respuesta, cod_ponderacion) VALUES (8, 'Bueno', 4);
INSERT INTO sgrh.tsgrhcatrespuestas (cod_catrespuesta, des_respuesta, cod_ponderacion) VALUES (9, 'Muy bueno', 5);
INSERT INTO sgrh.tsgrhcatrespuestas (cod_catrespuesta, des_respuesta, cod_ponderacion) VALUES (10, 'Muy bajo', 1);
INSERT INTO sgrh.tsgrhcatrespuestas (cod_catrespuesta, des_respuesta, cod_ponderacion) VALUES (11, 'Bajo', 2);
INSERT INTO sgrh.tsgrhcatrespuestas (cod_catrespuesta, des_respuesta, cod_ponderacion) VALUES (12, 'Alto', 3);
INSERT INTO sgrh.tsgrhcatrespuestas (cod_catrespuesta, des_respuesta, cod_ponderacion) VALUES (13, 'Muy alto', 4);
INSERT INTO sgrh.tsgrhcatrespuestas (cod_catrespuesta, des_respuesta, cod_ponderacion) VALUES (14, 'Muy incómodo', 1);
INSERT INTO sgrh.tsgrhcatrespuestas (cod_catrespuesta, des_respuesta, cod_ponderacion) VALUES (15, 'Incómodo', 2);
INSERT INTO sgrh.tsgrhcatrespuestas (cod_catrespuesta, des_respuesta, cod_ponderacion) VALUES (16, 'Soportable', 3);
INSERT INTO sgrh.tsgrhcatrespuestas (cod_catrespuesta, des_respuesta, cod_ponderacion) VALUES (17, 'Confortable', 4);
INSERT INTO sgrh.tsgrhcatrespuestas (cod_catrespuesta, des_respuesta, cod_ponderacion) VALUES (18, 'Muy confortable', 5);


--
-- TOC entry 4683 (class 0 OID 99443)
-- Dependencies: 271
-- Data for Name: tsgrhclientes; Type: TABLE DATA; Schema: sgrh; Owner: suite
--

INSERT INTO sgrh.tsgrhclientes (cod_cliente, des_nbcliente, des_direccioncte, des_nbcontactocte, des_correocte, cod_telefonocte) VALUES (1, 'Pemex', 'Tamaulipas', 'Samuel Velzaco', 'samuel-@hotmail.com', '5578457687');
INSERT INTO sgrh.tsgrhclientes (cod_cliente, des_nbcliente, des_direccioncte, des_nbcontactocte, des_correocte, cod_telefonocte) VALUES (2, 'Cable vision', 'Mexico', 'Alfredo Gomez', 'alfgom-@hotmail.com', '5578457687');
INSERT INTO sgrh.tsgrhclientes (cod_cliente, des_nbcliente, des_direccioncte, des_nbcontactocte, des_correocte, cod_telefonocte) VALUES (3, 'Telecom', 'Mexico', 'Miguel Romero', 'romero@gmail.com', '55344344334');
INSERT INTO sgrh.tsgrhclientes (cod_cliente, des_nbcliente, des_direccioncte, des_nbcontactocte, des_correocte, cod_telefonocte) VALUES (4, 'Bancomer', 'Mexico', 'Jose Mauro', 'jose@bbva.com', '55344344334');
INSERT INTO sgrh.tsgrhclientes (cod_cliente, des_nbcliente, des_direccioncte, des_nbcontactocte, des_correocte, cod_telefonocte) VALUES (5, 'Nuevo', 'conocido', 'josue', 'dfd2@nuevo.com', '556322');


--
-- TOC entry 4684 (class 0 OID 99448)
-- Dependencies: 272
-- Data for Name: tsgrhcontrataciones; Type: TABLE DATA; Schema: sgrh; Owner: suite
--



--
-- TOC entry 4685 (class 0 OID 99454)
-- Dependencies: 273
-- Data for Name: tsgrhcontratos; Type: TABLE DATA; Schema: sgrh; Owner: suite
--



--
-- TOC entry 4686 (class 0 OID 99460)
-- Dependencies: 274
-- Data for Name: tsgrhempleados; Type: TABLE DATA; Schema: sgrh; Owner: suite
--

INSERT INTO sgrh.tsgrhempleados (cod_empleado, des_nombre, des_nombres, des_apepaterno, des_apematerno, des_direccion, fec_nacimiento, des_lugarnacimiento, cod_edad, des_correo, cod_tiposangre, cod_telefonocasa, cod_telefonocelular, cod_telemergencia, bin_identificacion, bin_pasaporte, bin_visa, cod_licenciamanejo, fec_ingreso, cod_rfc, cod_nss, cod_curp, bin_foto, cod_tipofoto, cod_extensionfoto, cod_empleadoactivo, cod_estatusempleado, cod_estadocivil, cod_rol, cod_puesto, cod_diasvacaciones, cod_sistemasuite, fec_creacion, fec_modificacion, cod_creadopor, cod_modificadopor, cod_area, des_correopersonal) VALUES (22, 'HEIDY', '', 'TRINIDAD', 'MORELOS', 'DOMICILIO CONOCIDO', '1995-03-05', 'TLAXCO, TLAX.', 23, 'maito9.example@gmail.com', 'A+', '', '', '', NULL, NULL, NULL, '', '2019-02-14', '', '', 'TRMH950305HPZDMT34', NULL, '', '', true, 1, 0, 3, 1, NULL, NULL, '2019-02-14', '2019-02-14', 10, NULL, 3, NULL);
INSERT INTO sgrh.tsgrhempleados (cod_empleado, des_nombre, des_nombres, des_apepaterno, des_apematerno, des_direccion, fec_nacimiento, des_lugarnacimiento, cod_edad, des_correo, cod_tiposangre, cod_telefonocasa, cod_telefonocelular, cod_telemergencia, bin_identificacion, bin_pasaporte, bin_visa, cod_licenciamanejo, fec_ingreso, cod_rfc, cod_nss, cod_curp, bin_foto, cod_tipofoto, cod_extensionfoto, cod_empleadoactivo, cod_estatusempleado, cod_estadocivil, cod_rol, cod_puesto, cod_diasvacaciones, cod_sistemasuite, fec_creacion, fec_modificacion, cod_creadopor, cod_modificadopor, cod_area, des_correopersonal) VALUES (23, 'JESUS', 'MIGUEL', 'VELAZCO', 'MARQUEZ', 'DOMICILIO CONOCIDO', '1995-03-06', 'POZA RICA, VER.', 23, 'maito10.example@gmail.com', 'A+', '', '', '', NULL, NULL, NULL, '', '2019-02-14', '', '', 'MVMJ950306HPZDNT35', NULL, '', '', true, 1, 0, 3, 2, NULL, NULL, '2019-02-14', '2019-02-14', 10, NULL, 3, NULL);
INSERT INTO sgrh.tsgrhempleados (cod_empleado, des_nombre, des_nombres, des_apepaterno, des_apematerno, des_direccion, fec_nacimiento, des_lugarnacimiento, cod_edad, des_correo, cod_tiposangre, cod_telefonocasa, cod_telefonocelular, cod_telemergencia, bin_identificacion, bin_pasaporte, bin_visa, cod_licenciamanejo, fec_ingreso, cod_rfc, cod_nss, cod_curp, bin_foto, cod_tipofoto, cod_extensionfoto, cod_empleadoactivo, cod_estatusempleado, cod_estadocivil, cod_rol, cod_puesto, cod_diasvacaciones, cod_sistemasuite, fec_creacion, fec_modificacion, cod_creadopor, cod_modificadopor, cod_area, des_correopersonal) VALUES (24, 'RAUL', '', 'ESPINOZA', 'MARTINEZ', 'DOMICILIO CONOCIDO', '1995-03-07', 'XALAPA, VER.', 23, 'maito11.example@gmail.com', 'A+', '', '', '', NULL, NULL, NULL, '', '2019-02-14', '', '', 'ESMR950307HPZDOT36', NULL, '', '', true, 1, 0, 3, 3, NULL, NULL, '2019-02-14', '2019-02-14', 10, NULL, 3, NULL);
INSERT INTO sgrh.tsgrhempleados (cod_empleado, des_nombre, des_nombres, des_apepaterno, des_apematerno, des_direccion, fec_nacimiento, des_lugarnacimiento, cod_edad, des_correo, cod_tiposangre, cod_telefonocasa, cod_telefonocelular, cod_telemergencia, bin_identificacion, bin_pasaporte, bin_visa, cod_licenciamanejo, fec_ingreso, cod_rfc, cod_nss, cod_curp, bin_foto, cod_tipofoto, cod_extensionfoto, cod_empleadoactivo, cod_estatusempleado, cod_estadocivil, cod_rol, cod_puesto, cod_diasvacaciones, cod_sistemasuite, fec_creacion, fec_modificacion, cod_creadopor, cod_modificadopor, cod_area, des_correopersonal) VALUES (25, 'JOSE', 'EDUARDO', 'TRINIDAD', 'MORELOS', 'DOMICILIO CONOCIDO', '1995-03-08', 'COAHUILA, COAH.', 23, 'maito12.example@gmail.com', 'A+', '', '', '', NULL, NULL, NULL, '', '2019-02-14', '', '', 'ETMJ950308HPZDPT37', NULL, '', '', true, 1, 0, 3, 4, NULL, NULL, '2019-02-14', '2019-02-14', 10, NULL, 4, NULL);
INSERT INTO sgrh.tsgrhempleados (cod_empleado, des_nombre, des_nombres, des_apepaterno, des_apematerno, des_direccion, fec_nacimiento, des_lugarnacimiento, cod_edad, des_correo, cod_tiposangre, cod_telefonocasa, cod_telefonocelular, cod_telemergencia, bin_identificacion, bin_pasaporte, bin_visa, cod_licenciamanejo, fec_ingreso, cod_rfc, cod_nss, cod_curp, bin_foto, cod_tipofoto, cod_extensionfoto, cod_empleadoactivo, cod_estatusempleado, cod_estadocivil, cod_rol, cod_puesto, cod_diasvacaciones, cod_sistemasuite, fec_creacion, fec_modificacion, cod_creadopor, cod_modificadopor, cod_area, des_correopersonal) VALUES (26, 'SARAHI', '', 'GONZALEZ', 'SUAREZ', 'DOMICILIO CONOCIDO', '1995-03-09', 'HUACHINANGO, PUE.', 23, 'maito13.example@gmail.com', 'A+', '', '', '', NULL, NULL, NULL, '', '2019-02-14', '', '', 'GOSS950309HPZDQT38', NULL, '', '', true, 1, 0, 3, 1, NULL, NULL, '2019-02-14', '2019-02-14', 10, NULL, 4, NULL);
INSERT INTO sgrh.tsgrhempleados (cod_empleado, des_nombre, des_nombres, des_apepaterno, des_apematerno, des_direccion, fec_nacimiento, des_lugarnacimiento, cod_edad, des_correo, cod_tiposangre, cod_telefonocasa, cod_telefonocelular, cod_telemergencia, bin_identificacion, bin_pasaporte, bin_visa, cod_licenciamanejo, fec_ingreso, cod_rfc, cod_nss, cod_curp, bin_foto, cod_tipofoto, cod_extensionfoto, cod_empleadoactivo, cod_estatusempleado, cod_estadocivil, cod_rol, cod_puesto, cod_diasvacaciones, cod_sistemasuite, fec_creacion, fec_modificacion, cod_creadopor, cod_modificadopor, cod_area, des_correopersonal) VALUES (27, 'FEDERICO', '', 'GUZMAN', 'TRINIDAD', 'DOMICILIO CONOCIDO', '1995-03-10', 'TULANCINGO, HID.', 23, 'maito14.example@gmail.com', 'A+', '', '', '', NULL, NULL, NULL, '', '2019-02-14', '', '', 'GUTF950310HPZDRT39', NULL, '', '', true, 1, 0, 3, 2, NULL, NULL, '2019-02-14', '2019-02-14', 10, NULL, 4, NULL);
INSERT INTO sgrh.tsgrhempleados (cod_empleado, des_nombre, des_nombres, des_apepaterno, des_apematerno, des_direccion, fec_nacimiento, des_lugarnacimiento, cod_edad, des_correo, cod_tiposangre, cod_telefonocasa, cod_telefonocelular, cod_telemergencia, bin_identificacion, bin_pasaporte, bin_visa, cod_licenciamanejo, fec_ingreso, cod_rfc, cod_nss, cod_curp, bin_foto, cod_tipofoto, cod_extensionfoto, cod_empleadoactivo, cod_estatusempleado, cod_estadocivil, cod_rol, cod_puesto, cod_diasvacaciones, cod_sistemasuite, fec_creacion, fec_modificacion, cod_creadopor, cod_modificadopor, cod_area, des_correopersonal) VALUES (28, 'JOSE', 'IVAN', 'VACILIO', 'SANCHEZ', 'DOMICILIO CONOCIDO', '1995-03-11', 'XOXONANCATLA, PUE.', 23, 'maito15.example@gmail.com', 'B-', '', '', '', NULL, NULL, NULL, '', '2019-02-14', '', '', 'IVSJ950311HPZDTT40', NULL, '', '', true, 1, 0, 3, 3, NULL, NULL, '2019-02-14', '2019-02-14', 10, NULL, 4, NULL);
INSERT INTO sgrh.tsgrhempleados (cod_empleado, des_nombre, des_nombres, des_apepaterno, des_apematerno, des_direccion, fec_nacimiento, des_lugarnacimiento, cod_edad, des_correo, cod_tiposangre, cod_telefonocasa, cod_telefonocelular, cod_telemergencia, bin_identificacion, bin_pasaporte, bin_visa, cod_licenciamanejo, fec_ingreso, cod_rfc, cod_nss, cod_curp, bin_foto, cod_tipofoto, cod_extensionfoto, cod_empleadoactivo, cod_estatusempleado, cod_estadocivil, cod_rol, cod_puesto, cod_diasvacaciones, cod_sistemasuite, fec_creacion, fec_modificacion, cod_creadopor, cod_modificadopor, cod_area, des_correopersonal) VALUES (29, 'ERICA', '', 'ESPINOZA', 'CANDELARIA', 'DOMICILIO CONOCIDO', '1995-03-12', 'XICOTEPEC, PUE.', 23, 'maito16.example@gmail.com', 'B-', '', '', '', NULL, NULL, NULL, '', '2019-02-14', '', '', 'ESCE950312HPZDUT41', NULL, '', '', true, 1, 0, 3, 4, NULL, NULL, '2019-02-14', '2019-02-14', 10, NULL, 5, NULL);
INSERT INTO sgrh.tsgrhempleados (cod_empleado, des_nombre, des_nombres, des_apepaterno, des_apematerno, des_direccion, fec_nacimiento, des_lugarnacimiento, cod_edad, des_correo, cod_tiposangre, cod_telefonocasa, cod_telefonocelular, cod_telemergencia, bin_identificacion, bin_pasaporte, bin_visa, cod_licenciamanejo, fec_ingreso, cod_rfc, cod_nss, cod_curp, bin_foto, cod_tipofoto, cod_extensionfoto, cod_empleadoactivo, cod_estatusempleado, cod_estadocivil, cod_rol, cod_puesto, cod_diasvacaciones, cod_sistemasuite, fec_creacion, fec_modificacion, cod_creadopor, cod_modificadopor, cod_area, des_correopersonal) VALUES (30, 'ROBERTO', '', 'ORTEGA', 'ALVAREZ', 'DOMICILIO CONOCIDO', '1995-03-13', 'JICOLAPA, PUE.', 23, 'maito17.example@gmail.com', 'A-', '', '', '', NULL, NULL, NULL, '', '2019-02-14', '', '', 'ORAR950313HPZDVT42', NULL, '', '', true, 1, 0, 3, 1, NULL, NULL, '2019-02-14', '2019-02-14', 10, NULL, 5, NULL);
INSERT INTO sgrh.tsgrhempleados (cod_empleado, des_nombre, des_nombres, des_apepaterno, des_apematerno, des_direccion, fec_nacimiento, des_lugarnacimiento, cod_edad, des_correo, cod_tiposangre, cod_telefonocasa, cod_telefonocelular, cod_telemergencia, bin_identificacion, bin_pasaporte, bin_visa, cod_licenciamanejo, fec_ingreso, cod_rfc, cod_nss, cod_curp, bin_foto, cod_tipofoto, cod_extensionfoto, cod_empleadoactivo, cod_estatusempleado, cod_estadocivil, cod_rol, cod_puesto, cod_diasvacaciones, cod_sistemasuite, fec_creacion, fec_modificacion, cod_creadopor, cod_modificadopor, cod_area, des_correopersonal) VALUES (31, 'CESARIO', '', 'TELLEZ', 'REYES', 'DOMICILIO CONOCIDO', '1995-03-14', 'SANTA INES, PUE.', 23, 'maito18.example@gmail.com', 'A-', '', '', '', NULL, NULL, NULL, '', '2019-02-14', '', '', 'TERC950314HPZDWT43', NULL, '', '', true, 1, 0, 3, 2, NULL, NULL, '2019-02-14', '2019-02-14', 10, NULL, 5, NULL);
INSERT INTO sgrh.tsgrhempleados (cod_empleado, des_nombre, des_nombres, des_apepaterno, des_apematerno, des_direccion, fec_nacimiento, des_lugarnacimiento, cod_edad, des_correo, cod_tiposangre, cod_telefonocasa, cod_telefonocelular, cod_telemergencia, bin_identificacion, bin_pasaporte, bin_visa, cod_licenciamanejo, fec_ingreso, cod_rfc, cod_nss, cod_curp, bin_foto, cod_tipofoto, cod_extensionfoto, cod_empleadoactivo, cod_estatusempleado, cod_estadocivil, cod_rol, cod_puesto, cod_diasvacaciones, cod_sistemasuite, fec_creacion, fec_modificacion, cod_creadopor, cod_modificadopor, cod_area, des_correopersonal) VALUES (32, 'CARLOS', 'EFREN', 'SANCHEZ', 'JUAN', 'DOMICILIO CONOCIDO', '1995-03-15', 'CHOLULA, PUE.', 23, 'maito19.example@gmail.com', 'A-', '', '', '', NULL, NULL, NULL, '', '2019-02-14', '', '', 'ESJC950315HPZDXT44', NULL, '', '', true, 1, 0, 3, 3, NULL, NULL, '2019-02-14', '2019-02-14', 10, NULL, 5, NULL);
INSERT INTO sgrh.tsgrhempleados (cod_empleado, des_nombre, des_nombres, des_apepaterno, des_apematerno, des_direccion, fec_nacimiento, des_lugarnacimiento, cod_edad, des_correo, cod_tiposangre, cod_telefonocasa, cod_telefonocelular, cod_telemergencia, bin_identificacion, bin_pasaporte, bin_visa, cod_licenciamanejo, fec_ingreso, cod_rfc, cod_nss, cod_curp, bin_foto, cod_tipofoto, cod_extensionfoto, cod_empleadoactivo, cod_estatusempleado, cod_estadocivil, cod_rol, cod_puesto, cod_diasvacaciones, cod_sistemasuite, fec_creacion, fec_modificacion, cod_creadopor, cod_modificadopor, cod_area, des_correopersonal) VALUES (33, 'JOSE', '', 'DE LOS SANTOS', 'TRINIDAD', 'DOMICILIO CONOCIDO', '1995-03-16', 'DF, DF.', 23, 'maito20.example@gmail.com', 'A-', '', '', '', NULL, NULL, NULL, '', '2019-02-14', '', '', 'DETJ950316HPZDYT45', NULL, '', '', true, 1, 0, 3, 4, NULL, NULL, '2019-02-14', '2019-02-14', 10, NULL, 5, NULL);
INSERT INTO sgrh.tsgrhempleados (cod_empleado, des_nombre, des_nombres, des_apepaterno, des_apematerno, des_direccion, fec_nacimiento, des_lugarnacimiento, cod_edad, des_correo, cod_tiposangre, cod_telefonocasa, cod_telefonocelular, cod_telemergencia, bin_identificacion, bin_pasaporte, bin_visa, cod_licenciamanejo, fec_ingreso, cod_rfc, cod_nss, cod_curp, bin_foto, cod_tipofoto, cod_extensionfoto, cod_empleadoactivo, cod_estatusempleado, cod_estadocivil, cod_rol, cod_puesto, cod_diasvacaciones, cod_sistemasuite, fec_creacion, fec_modificacion, cod_creadopor, cod_modificadopor, cod_area, des_correopersonal) VALUES (34, 'FIDEL', '', 'SANCHEZ', 'VAZQUEZ', 'DOMICILIO CONOCIDO', '1995-03-17', 'ZACATELCO, TLAX.', 23, 'maito21.example@gmail.com', 'A-', '', '', '', NULL, NULL, NULL, '', '2019-02-14', '', '', 'SAVF950317HPZDZT46', NULL, '', '', true, 1, 0, 3, 5, NULL, NULL, '2019-02-14', '2019-02-14', 10, NULL, 5, NULL);
INSERT INTO sgrh.tsgrhempleados (cod_empleado, des_nombre, des_nombres, des_apepaterno, des_apematerno, des_direccion, fec_nacimiento, des_lugarnacimiento, cod_edad, des_correo, cod_tiposangre, cod_telefonocasa, cod_telefonocelular, cod_telemergencia, bin_identificacion, bin_pasaporte, bin_visa, cod_licenciamanejo, fec_ingreso, cod_rfc, cod_nss, cod_curp, bin_foto, cod_tipofoto, cod_extensionfoto, cod_empleadoactivo, cod_estatusempleado, cod_estadocivil, cod_rol, cod_puesto, cod_diasvacaciones, cod_sistemasuite, fec_creacion, fec_modificacion, cod_creadopor, cod_modificadopor, cod_area, des_correopersonal) VALUES (11, 'ADRIAN', NULL, 'SUAREZ', 'DE LA CRUZ', 'DOMICILIO CONOCIDO', '1996-06-14', 'AHUACATLAN, PUE.', 23, 'adrian.suarezc@gmail.com', 'O+', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-14', '1', '2', 'SDCA960614HPZDBT23', NULL, NULL, NULL, true, 1, 0, 2, 7, NULL, NULL, '2018-12-01', '2018-12-01', 10, NULL, 1, NULL);
INSERT INTO sgrh.tsgrhempleados (cod_empleado, des_nombre, des_nombres, des_apepaterno, des_apematerno, des_direccion, fec_nacimiento, des_lugarnacimiento, cod_edad, des_correo, cod_tiposangre, cod_telefonocasa, cod_telefonocelular, cod_telemergencia, bin_identificacion, bin_pasaporte, bin_visa, cod_licenciamanejo, fec_ingreso, cod_rfc, cod_nss, cod_curp, bin_foto, cod_tipofoto, cod_extensionfoto, cod_empleadoactivo, cod_estatusempleado, cod_estadocivil, cod_rol, cod_puesto, cod_diasvacaciones, cod_sistemasuite, fec_creacion, fec_modificacion, cod_creadopor, cod_modificadopor, cod_area, des_correopersonal) VALUES (12, 'CARLOS', NULL, 'ANTONIO', 'TRINIDAD', 'DOMICILIO CONOCIDO', '1996-03-12', 'AHUACATLAN, PUE.', 23, 'trinidad.carlos@gmail.com', 'O+', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-14', '1', '2', 'ANTC960312HPZDCT24', NULL, NULL, NULL, true, 1, 0, 3, 2, NULL, NULL, '2018-12-01', '2018-12-01', 10, NULL, 1, NULL);
INSERT INTO sgrh.tsgrhempleados (cod_empleado, des_nombre, des_nombres, des_apepaterno, des_apematerno, des_direccion, fec_nacimiento, des_lugarnacimiento, cod_edad, des_correo, cod_tiposangre, cod_telefonocasa, cod_telefonocelular, cod_telemergencia, bin_identificacion, bin_pasaporte, bin_visa, cod_licenciamanejo, fec_ingreso, cod_rfc, cod_nss, cod_curp, bin_foto, cod_tipofoto, cod_extensionfoto, cod_empleadoactivo, cod_estatusempleado, cod_estadocivil, cod_rol, cod_puesto, cod_diasvacaciones, cod_sistemasuite, fec_creacion, fec_modificacion, cod_creadopor, cod_modificadopor, cod_area, des_correopersonal) VALUES (13, 'ANGEL', 'ANTONIO', 'ROANO', 'ALVARADO', 'DOMICILIO CONOCIDO', '1995-02-19', 'TETELA, PUE.', 23, 'angel.antonio.roa@gmail.com', 'O+', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-14', '1', '2', 'ROAA950219HPZDDT25', NULL, NULL, NULL, true, 1, 0, 4, 3, NULL, NULL, '2018-12-01', '2018-12-01', 10, NULL, 1, NULL);
INSERT INTO sgrh.tsgrhempleados (cod_empleado, des_nombre, des_nombres, des_apepaterno, des_apematerno, des_direccion, fec_nacimiento, des_lugarnacimiento, cod_edad, des_correo, cod_tiposangre, cod_telefonocasa, cod_telefonocelular, cod_telemergencia, bin_identificacion, bin_pasaporte, bin_visa, cod_licenciamanejo, fec_ingreso, cod_rfc, cod_nss, cod_curp, bin_foto, cod_tipofoto, cod_extensionfoto, cod_empleadoactivo, cod_estatusempleado, cod_estadocivil, cod_rol, cod_puesto, cod_diasvacaciones, cod_sistemasuite, fec_creacion, fec_modificacion, cod_creadopor, cod_modificadopor, cod_area, des_correopersonal) VALUES (14, 'JUAN', '', 'MARQUEZ', 'SAVEDO', 'DOMICILIO CONOCIDO', '1995-02-12', 'APIZACO, TLAX', 23, 'maito1.example@gmail.com', 'O-', '', '', '', NULL, NULL, NULL, '', '2019-02-14', '1', '2', 'MASJ950212HPZDET26', NULL, '', '', true, 1, 0, 1, 9, NULL, NULL, '2019-02-14', '2019-02-14', 10, NULL, 20, NULL);
INSERT INTO sgrh.tsgrhempleados (cod_empleado, des_nombre, des_nombres, des_apepaterno, des_apematerno, des_direccion, fec_nacimiento, des_lugarnacimiento, cod_edad, des_correo, cod_tiposangre, cod_telefonocasa, cod_telefonocelular, cod_telemergencia, bin_identificacion, bin_pasaporte, bin_visa, cod_licenciamanejo, fec_ingreso, cod_rfc, cod_nss, cod_curp, bin_foto, cod_tipofoto, cod_extensionfoto, cod_empleadoactivo, cod_estatusempleado, cod_estadocivil, cod_rol, cod_puesto, cod_diasvacaciones, cod_sistemasuite, fec_creacion, fec_modificacion, cod_creadopor, cod_modificadopor, cod_area, des_correopersonal) VALUES (15, 'ANTONIO', '', 'HERRERA', 'CHAVEZ', 'DOMICILIO CONOCIDO', '1995-02-20', 'TLAXCALA, TLAX', 23, 'maito2.example@gmail.com', 'O-', '', '', '', NULL, NULL, NULL, '', '2019-02-14', '1', '2', 'HECA950220HPZDFT27', NULL, '', '', true, 1, 0, 2, 10, NULL, NULL, '2019-02-14', '2019-02-14', 10, NULL, 21, NULL);
INSERT INTO sgrh.tsgrhempleados (cod_empleado, des_nombre, des_nombres, des_apepaterno, des_apematerno, des_direccion, fec_nacimiento, des_lugarnacimiento, cod_edad, des_correo, cod_tiposangre, cod_telefonocasa, cod_telefonocelular, cod_telemergencia, bin_identificacion, bin_pasaporte, bin_visa, cod_licenciamanejo, fec_ingreso, cod_rfc, cod_nss, cod_curp, bin_foto, cod_tipofoto, cod_extensionfoto, cod_empleadoactivo, cod_estatusempleado, cod_estadocivil, cod_rol, cod_puesto, cod_diasvacaciones, cod_sistemasuite, fec_creacion, fec_modificacion, cod_creadopor, cod_modificadopor, cod_area, des_correopersonal) VALUES (1, 'TU', NULL, 'OTRO', NULL, 'CONOCIDO', '1990-02-02', 'AQUI', 24, 'Q@Q.COM', 'A', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-15', '1', '2', 'SABCHBAJSBCHJSDBC', NULL, NULL, NULL, true, 1, 0, 3, 4, NULL, NULL, '2019-05-05', '2019-11-15', 10, NULL, 5, NULL);
INSERT INTO sgrh.tsgrhempleados (cod_empleado, des_nombre, des_nombres, des_apepaterno, des_apematerno, des_direccion, fec_nacimiento, des_lugarnacimiento, cod_edad, des_correo, cod_tiposangre, cod_telefonocasa, cod_telefonocelular, cod_telemergencia, bin_identificacion, bin_pasaporte, bin_visa, cod_licenciamanejo, fec_ingreso, cod_rfc, cod_nss, cod_curp, bin_foto, cod_tipofoto, cod_extensionfoto, cod_empleadoactivo, cod_estatusempleado, cod_estadocivil, cod_rol, cod_puesto, cod_diasvacaciones, cod_sistemasuite, fec_creacion, fec_modificacion, cod_creadopor, cod_modificadopor, cod_area, des_correopersonal) VALUES (10, 'MATEO', NULL, 'RODRIGUEZ', 'JUAREZ', 'DOMICILIO CONOCIDO', '1996-04-09', 'MECATLAN, VER.', 23, 'mateorj96@gmail.com', 'O+', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-14', '1', '2', 'ROJM960409HPZDAT22', NULL, NULL, NULL, true, 1, 0, 1, 6, NULL, NULL, '2018-12-01', '2018-12-01', NULL, NULL, 5, NULL);
INSERT INTO sgrh.tsgrhempleados (cod_empleado, des_nombre, des_nombres, des_apepaterno, des_apematerno, des_direccion, fec_nacimiento, des_lugarnacimiento, cod_edad, des_correo, cod_tiposangre, cod_telefonocasa, cod_telefonocelular, cod_telemergencia, bin_identificacion, bin_pasaporte, bin_visa, cod_licenciamanejo, fec_ingreso, cod_rfc, cod_nss, cod_curp, bin_foto, cod_tipofoto, cod_extensionfoto, cod_empleadoactivo, cod_estatusempleado, cod_estadocivil, cod_rol, cod_puesto, cod_diasvacaciones, cod_sistemasuite, fec_creacion, fec_modificacion, cod_creadopor, cod_modificadopor, cod_area, des_correopersonal) VALUES (16, 'JAVIER', '', 'CHICHARITO', 'HERNANDEZ', 'DOMICILIO CONOCIDO', '1995-02-21', 'AHUCATLAN, PUE.', 23, 'maito3.example@gmail.com', 'O-', '', '', '', NULL, NULL, NULL, '', '2019-02-14', '1', '2', 'CHHJ950221HPZDGT28', NULL, '', '', true, 1, 0, 4, 8, NULL, NULL, '2019-02-14', '2019-02-14', 10, NULL, 22, NULL);
INSERT INTO sgrh.tsgrhempleados (cod_empleado, des_nombre, des_nombres, des_apepaterno, des_apematerno, des_direccion, fec_nacimiento, des_lugarnacimiento, cod_edad, des_correo, cod_tiposangre, cod_telefonocasa, cod_telefonocelular, cod_telemergencia, bin_identificacion, bin_pasaporte, bin_visa, cod_licenciamanejo, fec_ingreso, cod_rfc, cod_nss, cod_curp, bin_foto, cod_tipofoto, cod_extensionfoto, cod_empleadoactivo, cod_estatusempleado, cod_estadocivil, cod_rol, cod_puesto, cod_diasvacaciones, cod_sistemasuite, fec_creacion, fec_modificacion, cod_creadopor, cod_modificadopor, cod_area, des_correopersonal) VALUES (17, 'MANUEL', '', 'GONZALEZ', 'PEREZ', 'DOMICILIO CONOCIDO', '1995-02-22', 'TEPANGO PUE.', 23, 'maito4.example@gmail.com', 'O-', '', '', '', NULL, NULL, NULL, '', '2019-02-14', '1', '2', 'GOPM950222HPZDHT29', NULL, '', '', true, 1, 0, 4, 11, NULL, NULL, '2019-02-14', '2019-02-14', 10, NULL, 23, NULL);
INSERT INTO sgrh.tsgrhempleados (cod_empleado, des_nombre, des_nombres, des_apepaterno, des_apematerno, des_direccion, fec_nacimiento, des_lugarnacimiento, cod_edad, des_correo, cod_tiposangre, cod_telefonocasa, cod_telefonocelular, cod_telemergencia, bin_identificacion, bin_pasaporte, bin_visa, cod_licenciamanejo, fec_ingreso, cod_rfc, cod_nss, cod_curp, bin_foto, cod_tipofoto, cod_extensionfoto, cod_empleadoactivo, cod_estatusempleado, cod_estadocivil, cod_rol, cod_puesto, cod_diasvacaciones, cod_sistemasuite, fec_creacion, fec_modificacion, cod_creadopor, cod_modificadopor, cod_area, des_correopersonal) VALUES (18, 'MIRIAM', '', 'RAMOS', 'FLORES', 'DOMICILIO CONOCIDO', '1995-03-01', 'ZAPOTITLAN, PUE.', 23, 'maito5.example@gmail.com', 'B+', '', '', '', NULL, NULL, NULL, '', '2019-02-14', '1', '2', 'RAFS950301HPZDIT30', NULL, '', '', true, 1, 0, 2, 11, NULL, NULL, '2019-02-14', '2019-02-14', 10, NULL, 23, NULL);
INSERT INTO sgrh.tsgrhempleados (cod_empleado, des_nombre, des_nombres, des_apepaterno, des_apematerno, des_direccion, fec_nacimiento, des_lugarnacimiento, cod_edad, des_correo, cod_tiposangre, cod_telefonocasa, cod_telefonocelular, cod_telemergencia, bin_identificacion, bin_pasaporte, bin_visa, cod_licenciamanejo, fec_ingreso, cod_rfc, cod_nss, cod_curp, bin_foto, cod_tipofoto, cod_extensionfoto, cod_empleadoactivo, cod_estatusempleado, cod_estadocivil, cod_rol, cod_puesto, cod_diasvacaciones, cod_sistemasuite, fec_creacion, fec_modificacion, cod_creadopor, cod_modificadopor, cod_area, des_correopersonal) VALUES (19, 'CARINE', '', 'BENZEMA', 'ALVAREZ', 'DOMICILIO CONOCIDO', '1995-03-02', 'AQUIXTLA, PUE.', 23, 'maito6.example@gmail.com', 'B+', '', '', '', NULL, NULL, NULL, '', '2019-02-14', '1', '2', 'BEAC950302HPZDJT31', NULL, '', '', true, 1, 0, 3, 2, NULL, NULL, '2019-02-14', '2019-02-14', 10, NULL, 2, NULL);
INSERT INTO sgrh.tsgrhempleados (cod_empleado, des_nombre, des_nombres, des_apepaterno, des_apematerno, des_direccion, fec_nacimiento, des_lugarnacimiento, cod_edad, des_correo, cod_tiposangre, cod_telefonocasa, cod_telefonocelular, cod_telemergencia, bin_identificacion, bin_pasaporte, bin_visa, cod_licenciamanejo, fec_ingreso, cod_rfc, cod_nss, cod_curp, bin_foto, cod_tipofoto, cod_extensionfoto, cod_empleadoactivo, cod_estatusempleado, cod_estadocivil, cod_rol, cod_puesto, cod_diasvacaciones, cod_sistemasuite, fec_creacion, fec_modificacion, cod_creadopor, cod_modificadopor, cod_area, des_correopersonal) VALUES (20, 'CAROLINA', '', 'JIMENEZ', 'VAZQUEZ', 'DOMICILIO CONOCIDO', '1995-03-03', 'ZACATLAN, PUE.', 23, 'maito7.example@gmail.com', 'B+', '', '', '', NULL, NULL, NULL, '', '2019-02-14', '1', '2', 'JIVC950303HPZDKT32', NULL, '', '', true, 1, 0, 3, 3, NULL, NULL, '2019-02-14', '2019-02-14', 10, NULL, 3, NULL);
INSERT INTO sgrh.tsgrhempleados (cod_empleado, des_nombre, des_nombres, des_apepaterno, des_apematerno, des_direccion, fec_nacimiento, des_lugarnacimiento, cod_edad, des_correo, cod_tiposangre, cod_telefonocasa, cod_telefonocelular, cod_telemergencia, bin_identificacion, bin_pasaporte, bin_visa, cod_licenciamanejo, fec_ingreso, cod_rfc, cod_nss, cod_curp, bin_foto, cod_tipofoto, cod_extensionfoto, cod_empleadoactivo, cod_estatusempleado, cod_estadocivil, cod_rol, cod_puesto, cod_diasvacaciones, cod_sistemasuite, fec_creacion, fec_modificacion, cod_creadopor, cod_modificadopor, cod_area, des_correopersonal) VALUES (21, 'VERONICA', '', 'SANCHEZ', 'TRINIDAD', 'DOMICILIO CONOCIDO', '1995-03-04', 'CHIGNAHUAPAN, PUE.', 23, 'maito8.example@gmail.com', 'B+', '', '', '', NULL, NULL, NULL, '', '2019-02-14', '1', '2', 'SATV950304HPZDLT33', NULL, '', '', true, 1, 0, 3, 4, NULL, NULL, '2019-02-14', '2019-02-14', 10, NULL, 3, NULL);


--
-- TOC entry 4687 (class 0 OID 99469)
-- Dependencies: 275
-- Data for Name: tsgrhencuesta; Type: TABLE DATA; Schema: sgrh; Owner: suite
--

INSERT INTO sgrh.tsgrhencuesta (cod_encuesta, des_nbencuesta, cod_edoencuesta, fec_fechaencuesta, cod_lugar, tim_duracion, des_elementosvalidar, des_defectos, des_introduccion, cod_aceptado, cod_edoeliminar, cod_creadopor, cod_modificadopor, fec_creacion, fec_modificacion, cod_area) VALUES (8, 'ENCUESTA 00000004', 'Aceptado', '2019-02-10', 2, '00:10:00', NULL, NULL, NULL, true, false, 10, 10, '2019-02-10', '2019-02-10', 1);
INSERT INTO sgrh.tsgrhencuesta (cod_encuesta, des_nbencuesta, cod_edoencuesta, fec_fechaencuesta, cod_lugar, tim_duracion, des_elementosvalidar, des_defectos, des_introduccion, cod_aceptado, cod_edoeliminar, cod_creadopor, cod_modificadopor, fec_creacion, fec_modificacion, cod_area) VALUES (9, 'ENCUESTA 00000005', 'Aceptado', '2019-02-10', 2, '00:10:00', NULL, NULL, NULL, true, false, 10, 10, '2019-02-10', '2019-02-10', 1);
INSERT INTO sgrh.tsgrhencuesta (cod_encuesta, des_nbencuesta, cod_edoencuesta, fec_fechaencuesta, cod_lugar, tim_duracion, des_elementosvalidar, des_defectos, des_introduccion, cod_aceptado, cod_edoeliminar, cod_creadopor, cod_modificadopor, fec_creacion, fec_modificacion, cod_area) VALUES (10, 'ENCUESTA 00000006', 'Aceptado', '2019-02-10', 2, '00:10:00', NULL, NULL, NULL, true, false, 10, 10, '2019-02-10', '2019-02-10', 1);
INSERT INTO sgrh.tsgrhencuesta (cod_encuesta, des_nbencuesta, cod_edoencuesta, fec_fechaencuesta, cod_lugar, tim_duracion, des_elementosvalidar, des_defectos, des_introduccion, cod_aceptado, cod_edoeliminar, cod_creadopor, cod_modificadopor, fec_creacion, fec_modificacion, cod_area) VALUES (11, 'ENCUESTA 00000007', 'Aceptado', '2019-02-10', 2, '00:10:00', NULL, NULL, NULL, true, false, 10, 10, '2019-02-10', '2019-02-10', 1);
INSERT INTO sgrh.tsgrhencuesta (cod_encuesta, des_nbencuesta, cod_edoencuesta, fec_fechaencuesta, cod_lugar, tim_duracion, des_elementosvalidar, des_defectos, des_introduccion, cod_aceptado, cod_edoeliminar, cod_creadopor, cod_modificadopor, fec_creacion, fec_modificacion, cod_area) VALUES (12, 'ENCUESTA 00000008', 'Aceptado', '2019-02-10', 2, '00:10:00', NULL, NULL, NULL, true, false, 10, 10, '2019-02-10', '2019-02-10', 1);
INSERT INTO sgrh.tsgrhencuesta (cod_encuesta, des_nbencuesta, cod_edoencuesta, fec_fechaencuesta, cod_lugar, tim_duracion, des_elementosvalidar, des_defectos, des_introduccion, cod_aceptado, cod_edoeliminar, cod_creadopor, cod_modificadopor, fec_creacion, fec_modificacion, cod_area) VALUES (13, 'ENCUESTA 00000009', 'Aceptado', '2019-02-10', 2, '00:10:00', NULL, NULL, NULL, true, false, 10, 10, '2019-02-10', '2019-02-10', 2);
INSERT INTO sgrh.tsgrhencuesta (cod_encuesta, des_nbencuesta, cod_edoencuesta, fec_fechaencuesta, cod_lugar, tim_duracion, des_elementosvalidar, des_defectos, des_introduccion, cod_aceptado, cod_edoeliminar, cod_creadopor, cod_modificadopor, fec_creacion, fec_modificacion, cod_area) VALUES (14, 'ENCUESTA 00000010', 'Aceptado', '2019-02-10', 2, '00:10:00', NULL, NULL, NULL, true, false, 10, 10, '2019-02-10', '2019-02-10', 2);
INSERT INTO sgrh.tsgrhencuesta (cod_encuesta, des_nbencuesta, cod_edoencuesta, fec_fechaencuesta, cod_lugar, tim_duracion, des_elementosvalidar, des_defectos, des_introduccion, cod_aceptado, cod_edoeliminar, cod_creadopor, cod_modificadopor, fec_creacion, fec_modificacion, cod_area) VALUES (15, 'ENCUESTA 00000011', 'Aceptado', '2019-02-10', 2, '00:10:00', NULL, NULL, NULL, true, false, 10, 10, '2019-02-10', '2019-02-10', 2);
INSERT INTO sgrh.tsgrhencuesta (cod_encuesta, des_nbencuesta, cod_edoencuesta, fec_fechaencuesta, cod_lugar, tim_duracion, des_elementosvalidar, des_defectos, des_introduccion, cod_aceptado, cod_edoeliminar, cod_creadopor, cod_modificadopor, fec_creacion, fec_modificacion, cod_area) VALUES (16, 'ENCUESTA 00000012', 'Aceptado', '2019-02-10', 2, '00:10:00', NULL, NULL, NULL, true, false, 10, 10, '2019-02-10', '2019-02-10', 2);
INSERT INTO sgrh.tsgrhencuesta (cod_encuesta, des_nbencuesta, cod_edoencuesta, fec_fechaencuesta, cod_lugar, tim_duracion, des_elementosvalidar, des_defectos, des_introduccion, cod_aceptado, cod_edoeliminar, cod_creadopor, cod_modificadopor, fec_creacion, fec_modificacion, cod_area) VALUES (7, 'ENCUESTA 00000003', 'Aceptado', '2018-12-08', 2, '00:15:00', NULL, NULL, NULL, true, false, 10, 10, '2018-12-10', '2019-02-15', 1);
INSERT INTO sgrh.tsgrhencuesta (cod_encuesta, des_nbencuesta, cod_edoencuesta, fec_fechaencuesta, cod_lugar, tim_duracion, des_elementosvalidar, des_defectos, des_introduccion, cod_aceptado, cod_edoeliminar, cod_creadopor, cod_modificadopor, fec_creacion, fec_modificacion, cod_area) VALUES (19, 'roanoq', 'Corregido', '2019-04-12', 4, '03:03:00', 'asdasd', 'asda', 'asdasd', false, false, 13, 13, '2019-04-11', '2019-04-18', 2);
INSERT INTO sgrh.tsgrhencuesta (cod_encuesta, des_nbencuesta, cod_edoencuesta, fec_fechaencuesta, cod_lugar, tim_duracion, des_elementosvalidar, des_defectos, des_introduccion, cod_aceptado, cod_edoeliminar, cod_creadopor, cod_modificadopor, fec_creacion, fec_modificacion, cod_area) VALUES (18, 'asdasd encuesta 13', 'Aceptado', '2019-03-31', 2, '01:00:00', 'validos', 'defectoss sd adaksd', 'introducción chida', false, true, 13, 13, '2019-04-05', '2019-04-18', 1);
INSERT INTO sgrh.tsgrhencuesta (cod_encuesta, des_nbencuesta, cod_edoencuesta, fec_fechaencuesta, cod_lugar, tim_duracion, des_elementosvalidar, des_defectos, des_introduccion, cod_aceptado, cod_edoeliminar, cod_creadopor, cod_modificadopor, fec_creacion, fec_modificacion, cod_area) VALUES (6, 'ENCUESTA 00000002', 'Aceptado', '2018-12-10', 2, '00:15:00', NULL, NULL, NULL, false, false, 10, 13, '2018-12-10', '2019-04-27', 5);
INSERT INTO sgrh.tsgrhencuesta (cod_encuesta, des_nbencuesta, cod_edoencuesta, fec_fechaencuesta, cod_lugar, tim_duracion, des_elementosvalidar, des_defectos, des_introduccion, cod_aceptado, cod_edoeliminar, cod_creadopor, cod_modificadopor, fec_creacion, fec_modificacion, cod_area) VALUES (5, 'roano encuesta 01adsa', 'En Proceso', '2018-12-10', 2, '00:15:00', '', '', '', false, false, 10, 13, '2018-12-10', '2019-07-18', 5);


--
-- TOC entry 4688 (class 0 OID 99479)
-- Dependencies: 276
-- Data for Name: tsgrhencuesta_participantes; Type: TABLE DATA; Schema: sgrh; Owner: suite
--

INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (1, 29, 5, 82, 477, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (2, 29, 5, 83, 482, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (3, 29, 5, 84, 488, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (4, 29, 5, 85, 493, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (5, 29, 5, 86, 498, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (6, 29, 5, 87, 502, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (7, 29, 5, 88, 507, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (8, 29, 5, 89, 512, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (9, 29, 5, 90, 516, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (10, 29, 5, 91, NULL, '');
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (11, 29, 5, 92, NULL, '');
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (12, 29, 5, 93, 523, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (13, 29, 5, 94, 528, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (14, 29, 5, 95, 533, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (15, 29, 5, 97, NULL, '');
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (16, 29, 5, 98, 539, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (17, 29, 5, 99, 543, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (18, 29, 5, 100, 548, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (19, 29, 5, 101, 553, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (20, 29, 5, 102, 557, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (21, 29, 5, 104, 564, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (22, 29, 5, 105, 568, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (23, 29, 5, 106, 574, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (24, 30, 5, 82, 477, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (25, 30, 5, 83, 482, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (26, 30, 5, 84, 489, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (27, 30, 5, 85, 494, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (28, 30, 5, 86, 498, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (29, 30, 5, 87, 502, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (30, 30, 5, 88, 507, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (31, 30, 5, 89, 512, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (32, 30, 5, 90, 515, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (33, 30, 5, 91, NULL, '');
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (34, 30, 5, 92, NULL, '');
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (35, 30, 5, 93, 522, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (36, 30, 5, 94, 529, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (37, 30, 5, 95, 533, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (38, 30, 5, 97, NULL, '');
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (39, 30, 5, 98, 539, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (40, 30, 5, 99, 543, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (41, 30, 5, 100, 548, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (42, 30, 5, 101, 553, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (43, 30, 5, 102, 557, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (44, 30, 5, 104, 563, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (45, 30, 5, 105, 568, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (46, 30, 5, 106, 573, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (47, 31, 5, 82, 479, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (48, 31, 5, 83, 484, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (49, 31, 5, 84, 487, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (50, 31, 5, 85, 492, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (51, 31, 5, 86, 499, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (52, 31, 5, 87, 502, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (53, 31, 5, 88, 507, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (54, 31, 5, 89, 512, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (55, 31, 5, 90, 517, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (56, 31, 5, 91, NULL, '');
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (57, 31, 5, 92, NULL, '');
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (58, 31, 5, 93, 524, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (59, 31, 5, 94, 529, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (60, 31, 5, 95, 533, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (61, 31, 5, 97, NULL, '');
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (62, 31, 5, 98, 537, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (63, 31, 5, 99, 544, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (64, 31, 5, 100, 548, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (65, 31, 5, 101, 553, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (66, 31, 5, 102, 558, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (67, 31, 5, 104, 564, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (68, 31, 5, 105, 570, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (69, 31, 5, 106, 574, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (70, 32, 5, 82, 478, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (71, 32, 5, 83, 483, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (72, 32, 5, 84, 488, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (73, 32, 5, 85, 492, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (74, 32, 5, 86, 498, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (75, 32, 5, 87, 502, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (76, 32, 5, 88, 508, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (77, 32, 5, 89, 512, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (78, 32, 5, 90, 517, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (79, 32, 5, 91, NULL, '');
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (80, 32, 5, 92, NULL, '');
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (81, 32, 5, 93, 523, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (82, 32, 5, 94, 529, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (83, 32, 5, 95, 533, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (84, 32, 5, 97, NULL, '');
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (85, 32, 5, 98, 539, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (86, 32, 5, 99, 543, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (87, 32, 5, 100, 548, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (88, 32, 5, 101, 553, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (89, 32, 5, 102, 557, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (90, 32, 5, 104, 565, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (91, 32, 5, 105, 569, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (92, 32, 5, 106, 575, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (93, 33, 5, 82, 477, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (94, 33, 5, 83, 482, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (95, 33, 5, 84, 488, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (96, 33, 5, 85, 493, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (97, 33, 5, 86, 499, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (98, 33, 5, 87, 502, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (99, 33, 5, 88, 507, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (100, 33, 5, 89, 512, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (101, 33, 5, 90, 518, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (102, 33, 5, 91, NULL, '');
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (103, 33, 5, 92, NULL, '');
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (104, 33, 5, 93, 523, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (105, 33, 5, 94, 529, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (106, 33, 5, 95, 532, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (107, 33, 5, 97, NULL, '');
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (108, 33, 5, 98, 538, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (109, 33, 5, 99, 543, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (110, 33, 5, 100, 548, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (111, 33, 5, 101, 553, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (112, 33, 5, 102, 558, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (113, 33, 5, 104, 565, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (114, 33, 5, 105, 570, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (115, 33, 5, 106, 575, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (116, 34, 5, 82, 477, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (117, 34, 5, 83, 482, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (118, 34, 5, 84, 488, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (119, 34, 5, 85, 492, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (120, 34, 5, 86, 499, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (121, 34, 5, 87, 502, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (122, 34, 5, 88, 508, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (123, 34, 5, 89, 512, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (124, 34, 5, 90, 518, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (125, 34, 5, 91, NULL, '');
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (126, 34, 5, 92, NULL, '');
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (127, 34, 5, 93, 523, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (128, 34, 5, 94, 527, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (129, 34, 5, 95, 532, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (130, 34, 5, 97, NULL, '');
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (131, 34, 5, 98, 538, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (132, 34, 5, 99, 543, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (133, 34, 5, 100, 549, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (134, 34, 5, 101, 553, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (135, 34, 5, 102, 559, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (136, 34, 5, 104, 563, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (137, 34, 5, 105, 569, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (138, 34, 5, 106, 575, NULL);


--
-- TOC entry 4689 (class 0 OID 99486)
-- Dependencies: 277
-- Data for Name: tsgrhescolaridad; Type: TABLE DATA; Schema: sgrh; Owner: suite
--



--
-- TOC entry 4690 (class 0 OID 99493)
-- Dependencies: 278
-- Data for Name: tsgrhestatuscapacitacion; Type: TABLE DATA; Schema: sgrh; Owner: suite
--

INSERT INTO sgrh.tsgrhestatuscapacitacion (cod_estatus, des_estatus, aud_feccreacion, aud_fecmodificacion, aud_creadopor, aud_modificadopor) VALUES (1, 'Pendiente', '2019-10-29', '2019-10-29', 10, 10);
INSERT INTO sgrh.tsgrhestatuscapacitacion (cod_estatus, des_estatus, aud_feccreacion, aud_fecmodificacion, aud_creadopor, aud_modificadopor) VALUES (2, 'Aceptado', '2019-10-29', '2019-10-29', 10, 10);
INSERT INTO sgrh.tsgrhestatuscapacitacion (cod_estatus, des_estatus, aud_feccreacion, aud_fecmodificacion, aud_creadopor, aud_modificadopor) VALUES (3, 'Rechazado', '2019-10-29', '2019-10-29', 10, 10);


--
-- TOC entry 4691 (class 0 OID 99497)
-- Dependencies: 279
-- Data for Name: tsgrhevacapacitacion; Type: TABLE DATA; Schema: sgrh; Owner: suite
--

INSERT INTO sgrh.tsgrhevacapacitacion (cod_evacapacitacion, cod_plancapacitacion, cod_empleado, des_estado, des_evaluacion, auf_feccreacion, aud_fecmodificacion, aud_creadopor, aud_modificadopor) VALUES (1, 2, 10, 'Pendiente', NULL, '2019-11-13', '2019-11-13', 11, 11);
INSERT INTO sgrh.tsgrhevacapacitacion (cod_evacapacitacion, cod_plancapacitacion, cod_empleado, des_estado, des_evaluacion, auf_feccreacion, aud_fecmodificacion, aud_creadopor, aud_modificadopor) VALUES (2, 3, 10, 'Pendiente', NULL, '2019-11-13', '2019-11-13', 11, 11);


--
-- TOC entry 4692 (class 0 OID 99501)
-- Dependencies: 280
-- Data for Name: tsgrhevacontestadas; Type: TABLE DATA; Schema: sgrh; Owner: suite
--



--
-- TOC entry 4693 (class 0 OID 99508)
-- Dependencies: 281
-- Data for Name: tsgrhevaluaciones; Type: TABLE DATA; Schema: sgrh; Owner: suite
--



--
-- TOC entry 4694 (class 0 OID 99516)
-- Dependencies: 282
-- Data for Name: tsgrhexperienciaslaborales; Type: TABLE DATA; Schema: sgrh; Owner: suite
--



--
-- TOC entry 4695 (class 0 OID 99523)
-- Dependencies: 283
-- Data for Name: tsgrhfactoreseva; Type: TABLE DATA; Schema: sgrh; Owner: suite
--



--
-- TOC entry 4696 (class 0 OID 99527)
-- Dependencies: 284
-- Data for Name: tsgrhidiomas; Type: TABLE DATA; Schema: sgrh; Owner: suite
--

INSERT INTO sgrh.tsgrhidiomas (cod_idioma, des_nbidioma, por_dominiooral, por_dominioescrito, cod_empleado) VALUES (1, 'Ingles', 30, 30, NULL);
INSERT INTO sgrh.tsgrhidiomas (cod_idioma, des_nbidioma, por_dominiooral, por_dominioescrito, cod_empleado) VALUES (2, 'Ingles', 30, 30, NULL);
INSERT INTO sgrh.tsgrhidiomas (cod_idioma, des_nbidioma, por_dominiooral, por_dominioescrito, cod_empleado) VALUES (3, 'Ingles', 30, 30, NULL);
INSERT INTO sgrh.tsgrhidiomas (cod_idioma, des_nbidioma, por_dominiooral, por_dominioescrito, cod_empleado) VALUES (4, 'ingles', 30, 30, NULL);
INSERT INTO sgrh.tsgrhidiomas (cod_idioma, des_nbidioma, por_dominiooral, por_dominioescrito, cod_empleado) VALUES (5, 'ingles', 30, 30, NULL);
INSERT INTO sgrh.tsgrhidiomas (cod_idioma, des_nbidioma, por_dominiooral, por_dominioescrito, cod_empleado) VALUES (6, 'ingles', 30, 30, NULL);
INSERT INTO sgrh.tsgrhidiomas (cod_idioma, des_nbidioma, por_dominiooral, por_dominioescrito, cod_empleado) VALUES (7, 'ingles', 30, 30, NULL);
INSERT INTO sgrh.tsgrhidiomas (cod_idioma, des_nbidioma, por_dominiooral, por_dominioescrito, cod_empleado) VALUES (8, 'ingles', 20, 20, NULL);
INSERT INTO sgrh.tsgrhidiomas (cod_idioma, des_nbidioma, por_dominiooral, por_dominioescrito, cod_empleado) VALUES (9, 'Ingles', 20, 20, 34);
INSERT INTO sgrh.tsgrhidiomas (cod_idioma, des_nbidioma, por_dominiooral, por_dominioescrito, cod_empleado) VALUES (10, 'Frances', 20, 20, 34);
INSERT INTO sgrh.tsgrhidiomas (cod_idioma, des_nbidioma, por_dominiooral, por_dominioescrito, cod_empleado) VALUES (11, 'Fran', 20, 20, NULL);
INSERT INTO sgrh.tsgrhidiomas (cod_idioma, des_nbidioma, por_dominiooral, por_dominioescrito, cod_empleado) VALUES (12, 'Fran', 20, 20, NULL);


--
-- TOC entry 4697 (class 0 OID 99531)
-- Dependencies: 285
-- Data for Name: tsgrhlogistica; Type: TABLE DATA; Schema: sgrh; Owner: suite
--

INSERT INTO sgrh.tsgrhlogistica (cod_logistica, tim_totalhoras, tim_horafin, tim_horainicio, des_requerimientos, des_lugarcapacitacion, fec_fecinicio, fec_termino, cod_plancapacitacion, aud_feccreacion, aud_fecmodificacion, aud_creadopor, aud_modificadopor) VALUES (1, 100, '14:00:00', '16:00:00', 'N/A', NULL, '2019-09-07', '2019-09-08', 2, '2019-08-20', '2019-08-20', 10, 10);
INSERT INTO sgrh.tsgrhlogistica (cod_logistica, tim_totalhoras, tim_horafin, tim_horainicio, des_requerimientos, des_lugarcapacitacion, fec_fecinicio, fec_termino, cod_plancapacitacion, aud_feccreacion, aud_fecmodificacion, aud_creadopor, aud_modificadopor) VALUES (2, 100, '14:00:00', '16:00:00', 'N/A', 'Tlaxcala', '2019-09-07', '2019-09-08', 3, '2019-08-20', '2019-08-20', 10, 10);


--
-- TOC entry 4698 (class 0 OID 99535)
-- Dependencies: 286
-- Data for Name: tsgrhmodo; Type: TABLE DATA; Schema: sgrh; Owner: suite
--

INSERT INTO sgrh.tsgrhmodo (cod_modo, des_nbmodo, aud_feccreacion, aud_fecmodificacion, aud_creadopor, aud_modificadopor) VALUES (1, 'Modo 1', '2019-10-27', '2019-10-27', 10, 10);
INSERT INTO sgrh.tsgrhmodo (cod_modo, des_nbmodo, aud_feccreacion, aud_fecmodificacion, aud_creadopor, aud_modificadopor) VALUES (4, 'Modo 3', '2019-10-28', '2019-10-28', 10, 10);
INSERT INTO sgrh.tsgrhmodo (cod_modo, des_nbmodo, aud_feccreacion, aud_fecmodificacion, aud_creadopor, aud_modificadopor) VALUES (2, 'Hola mundo', '2019-10-28', '2019-10-28', 10, 10);
INSERT INTO sgrh.tsgrhmodo (cod_modo, des_nbmodo, aud_feccreacion, aud_fecmodificacion, aud_creadopor, aud_modificadopor) VALUES (3, 'Que rollo', '2019-10-28', '2019-10-28', 10, 10);


--
-- TOC entry 4699 (class 0 OID 99539)
-- Dependencies: 287
-- Data for Name: tsgrhperfiles; Type: TABLE DATA; Schema: sgrh; Owner: suite
--

INSERT INTO sgrh.tsgrhperfiles (cod_perfil, des_perfil) VALUES (1, 'Administrador');
INSERT INTO sgrh.tsgrhperfiles (cod_perfil, des_perfil) VALUES (2, 'Nuevo');
INSERT INTO sgrh.tsgrhperfiles (cod_perfil, des_perfil) VALUES (3, 'Prueba');
INSERT INTO sgrh.tsgrhperfiles (cod_perfil, des_perfil) VALUES (4, '.NET, java scrip');
INSERT INTO sgrh.tsgrhperfiles (cod_perfil, des_perfil) VALUES (5, 'Nuevo');
INSERT INTO sgrh.tsgrhperfiles (cod_perfil, des_perfil) VALUES (6, 'Nuevo');
INSERT INTO sgrh.tsgrhperfiles (cod_perfil, des_perfil) VALUES (7, 'luness');
INSERT INTO sgrh.tsgrhperfiles (cod_perfil, des_perfil) VALUES (8, 'nuevo');


--
-- TOC entry 4700 (class 0 OID 99543)
-- Dependencies: 288
-- Data for Name: tsgrhplancapacitacion; Type: TABLE DATA; Schema: sgrh; Owner: suite
--

INSERT INTO sgrh.tsgrhplancapacitacion (cod_plancapacitacion, des_nombre, cod_modo, cod_tipocapacitacion, des_criterios, cod_proceso, des_instructor, cod_proveedor, cod_estatus, des_comentarios, des_evaluacion, aud_feccreacion, aud_fecmodificacion, aud_creadopor, aud_modificadopor) VALUES (2, 'Sanhz', 1, 2, 'aSDFawefr qwehjrf jerf jqherfwklqejf jkwer agfvavb', 2, 'Pavo v:', 2, 1, 'Pendiente', 'No Evaluado', '2019-10-28', '2019-10-28', 10, 10);
INSERT INTO sgrh.tsgrhplancapacitacion (cod_plancapacitacion, des_nombre, cod_modo, cod_tipocapacitacion, des_criterios, cod_proceso, des_instructor, cod_proveedor, cod_estatus, des_comentarios, des_evaluacion, aud_feccreacion, aud_fecmodificacion, aud_creadopor, aud_modificadopor) VALUES (3, 'Luicho', 1, 1, ' Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque in auctor nisl, ac tristique arcu. Ut placerat mi dui. Etiam convallis, velit eu hendrerit interdum, justo eros lacinia ipsum, eget vu', 1, 'Pavo v:', 1, 1, 'Pendiente', 'No Evaluado', '2019-10-28', '2019-10-28', 10, 10);


--
-- TOC entry 4701 (class 0 OID 99550)
-- Dependencies: 289
-- Data for Name: tsgrhplanoperativo; Type: TABLE DATA; Schema: sgrh; Owner: suite
--



--
-- TOC entry 4702 (class 0 OID 99559)
-- Dependencies: 290
-- Data for Name: tsgrhpreguntasenc; Type: TABLE DATA; Schema: sgrh; Owner: suite
--

INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (107, 'En su centro de trabajo las oportunidades de desarrollo laboral solo las reciben unas cuantas personas privilegiadas', false, false, 6);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (109, 'En  su centro de trabajo se cuenta con programas de capacitación en materia de igualdad laboral y no discriminación.', false, false, 6);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (111, 'En su centro de trabajo para lograr la contratación, una promoción o un ascenso cuentan más las recomendaciones que los conocimientos y capacidades de la persona.', false, false, 6);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (113, 'En su centro de trabajo la competencia por mejores puestos, condiciones laborales o salariales es justa y equitativa.', false, false, 6);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (115, 'En su centro de trabajo se cuenta con un sistema de evaluación de desempeño del personal.', false, false, 6);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (117, 'Usted siente que se le trata con respeto en su trabajo actual.', false, false, 7);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (119, 'En su centro de trabajo todas las personas que laboran obtienen un trato digno y decente.', false, false, 7);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (126, 'En su centro de trabajo existen campañas de difusión internas de promoción de la igualdad laboral y no discriminación.', false, false, 7);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (137, 'La organización cuenta con planes y acciones específicos destinados a mejorar mi trabajo.', false, false, 8);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (141, 'El nivel de compromiso por apoyar el trabajo de los demás en la organización es', false, false, 8);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (143, 'Mi jefe me respalda frente a sus superiores', false, false, 8);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (145, 'Participo de las actividades culturales y recreacionales que la organización realiza.', false, false, 8);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (147, 'Mi jefe me brinda la retroalimentación necesaria para reforzar mis puntos débiles según la evaluación de desempeño.', false, false, 8);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (149, 'Los jefes reconocen y valoran mi trabajo', false, false, 8);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (151, 'La distribución de la carga de trabajo que tiene mi área es', false, false, 8);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (153, '¿Cómo calificaría su nivel de satisfacción con el trabajo que realiza en la organización?', false, false, 8);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (155, 'Te agradeceremos nos hagas llegar algunos comentarios acerca de aspectos que ayudarían a mejorar nuestro ambiente de trabajo.', true, false, 8);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (157, 'Usted tiene el suficiente tiempo para realizar su trabajo habitual:', false, false, 9);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (159, '¿Considera que recibe una justa retribución económica por las labores desempeñadas?', false, false, 9);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (161, '¿Cómo calificaría su nivel de satisfacción por trabajar en la organización?', false, false, 9);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (121, 'En su centro de trabajo, en general hay personas que discriminan, tratan mal o le faltan el respeto a sus compañeras/os, colegas o subordinadas/os.', false, false, 7);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (124, 'En su centro de trabajo  las y los superiores reciben un trato mucho más respetuoso que subordinados(as) y personal administrativo.', false, false, 7);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (128, 'En su centro de trabajo las cargas de trabajo se distribuyen de acuerdo a la responsabilidad del cargo.', false, false, 7);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (130, 'En mi oficina se fomenta y desarrolla el trabajo en equipo', false, false, 8);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (132, 'Existe comunicación dentro de mi grupo de trabajo', false, false, 8);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (134, 'Siento que no me alcanza el tiempo para completar mi trabajo', false, false, 8);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (136, 'La relación entre compañeros de trabajo en la organización es', false, false, 8);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (138, 'La organización otorga buenos y equitativos beneficios a los trabajadores', false, false, 8);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (140, 'Las remuneraciones están al nivel de los sueldos de mis colegas en el mercado', false, false, 8);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (95, 'Soy responsable del trabajo que realizo', false, false, 5);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (94, 'Mi superior me motiva a cumplir con mi trabajo de la manera que yo considere mejor.', false, false, 5);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (93, 'Considero que necesito capacitación en alguna área de mi interés y que forma parte importante de mi desarrollo.', false, false, 5);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (142, 'Siento apoyo en mi jefe cuando me encuentro en dificultades', false, false, 8);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (91, '¿Cree que su trabajo es compatible con los objetivos de la empresa?', true, false, 5);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (90, 'Cuento con los materiales y equipos necesarios para realizar mi trabajo.', false, false, 5);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (89, 'Está conforme con la limpieza, higiene y salubridad en su lugar de trabajo.', false, false, 5);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (88, 'Si  hay  un  nuevo Plan  Estratégico, estoy dispuesto a servir de voluntario para iniciar los cambios.', false, false, 5);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (87, 'En esta Institución, la gente planifica cuidadosamente antes de tomar acción.', false, false, 5);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (144, 'Mi jefe me da autonomía para tomar las decisiones necesarias para el cumplimiento de mis responsabilidades.', false, false, 8);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (86, 'Yo aporto al proceso de planificación en mi área de trabajo.', false, false, 5);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (83, 'En mi organización está claramente definida su Misión y Visión.', false, false, 5);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (146, 'Mi jefe me proporciona información suficiente, adecuada para realizar bien mi trabajo', false, false, 8);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (148, 'El nivel de recursos (materiales, equipos e infraestructura) con los que cuento para realizar bien mi trabajo es', false, false, 8);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (150, 'Mi remuneración, comparada con lo que otros ganan y hacen en la organización, está acorde con las responsabilidades de mi cargo', false, false, 8);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (152, '¿Cómo calificaría su nivel de satisfacción por pertenecer a la organización?', false, false, 8);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (154, '¿Cómo calificaría su nivel de identificación con la organización?', false, false, 8);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (102, 'Siento que formo parte de un equipo que trabaja hacia una meta común', false, true, 5);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (101, 'En mi grupo de trabajo, solucionar el problema es más importante que encontrar algún culpable.', false, true, 5);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (100, 'Mis compañeros y yo trabajamos juntos de manera efectiva', false, true, 5);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (99, 'El horario de trabajo me permite atender mis necesidades personales', false, true, 5);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (98, 'Me siento comprometido para alcanzar las metas establecidas.', false, true, 5);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (156, 'En relación a las condiciones físicas de su puesto de trabajo (iluminación, temperatura, etc.)  usted considera que éste es:', false, false, 9);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (158, '¿Está usted de acuerdo en cómo está gestionado el departamento en el que trabaja respecto a las metas que éste tiene encomendadas?', false, false, 9);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (82, 'Me siento muy satisfecho con mi ambiente de trabajo.', false, false, 5);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (160, 'Considera que su remuneración está por encima de la media en su entorno social, fuera de la empresa?', false, false, 9);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (92, 'Considero que me pagan lo justo por mi trabajo.', true, false, 5);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (97, 'Conozco las exigencias de mi trabajo.', true, false, 5);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (108, 'En su centro de trabajo mujeres y hombres tienen por igual oportunidades de ascenso y capacitación.', false, false, 6);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (110, 'En los últimos 12 meses usted ha participado  en programas de capacitación.', false, false, 6);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (112, 'En su centro de trabajo se ha despedido a alguna mujer por embarazo u orillado a renunciar al regresar de su licencia de maternidad.', false, false, 6);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (114, 'En su centro de trabajo mujeres y hombres tienen las mismas oportunidades para ocupar puestos de decisión.', false, false, 6);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (116, 'En los últimos 12 meses le han realizado una evaluación de desempeño.', false, false, 6);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (118, 'En su centro de trabajo quienes realizan tareas personales para las y los jefes logran privilegios.', false, false, 7);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (120, 'En su centro de trabajo las valoraciones que se realizan a sus actividades dependen más de la calidad y responsabilidad que de cualquier otra cuestión personal.', false, false, 7);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (122, 'En su centro de trabajo debido a sus características personales hay personas que sufren un trato inferior o de burla.', false, false, 7);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (125, 'En su centro de trabajo las y los superiores están abiertos a la comunicación con el personal.', false, false, 7);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (127, 'En su centro de trabajo las funciones y tareas se transmiten de manera clara y precisa.', false, false, 7);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (85, 'Existe un plan para lograr los  objetivos de la organización.', false, false, 5);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (84, 'La  dirección manifiesta sus objetivos de tal forma que se crea un sentido común de misión e identidad entre sus miembros.', false, false, 5);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (129, 'Si manifiesto mi preocupación sobre algún asunto relacionado con la igualdad de género o prácticas discriminatorias, se le da seguimiento', false, false, 7);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (131, 'Para el desempeño de mis labores mi ambiente de trabajo es', false, false, 8);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (133, 'Existe comunicación fluida entre mi Región y la sede central.', false, false, 8);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (135, 'Los jefes en la organización se preocupan por mantener elevado el nivel de motivación del personal', false, false, 8);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (139, 'En la organización las funciones están claramente definidas', false, false, 8);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (205, 'preguntaejemplo', false, true, 5);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (106, 'Hay evidencia de que mi jefe me apoya utilizando mis ideas o propuestas para mejorar el trabajo', false, true, 5);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (105, 'Tengo mucho trabajo y poco tiempo para realizarlo', false, true, 5);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (104, 'Mi superior inmediato toma acciones que refuerzan el objetivo común de la Institución.', false, true, 5);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (206, 'lo que sea', false, false, 5);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (207, 'asdfg', true, false, 6);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (208, 'asd', true, false, 5);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (209, 'asdfgh', true, false, 5);


--
-- TOC entry 4703 (class 0 OID 99563)
-- Dependencies: 291
-- Data for Name: tsgrhpreguntaseva; Type: TABLE DATA; Schema: sgrh; Owner: suite
--



--
-- TOC entry 4704 (class 0 OID 99567)
-- Dependencies: 292
-- Data for Name: tsgrhprocesos; Type: TABLE DATA; Schema: sgrh; Owner: suite
--

INSERT INTO sgrh.tsgrhprocesos (cod_proceso, des_nbproceso, aud_feccreacion, aud_fecmodificacion, aud_creadopor, aud_modificadopor) VALUES (1, 'ASDFGH', '2019-10-28', '2019-10-28', 10, 10);
INSERT INTO sgrh.tsgrhprocesos (cod_proceso, des_nbproceso, aud_feccreacion, aud_fecmodificacion, aud_creadopor, aud_modificadopor) VALUES (2, 'Interno', '2019-10-28', '2019-10-28', 10, 10);


--
-- TOC entry 4705 (class 0 OID 99571)
-- Dependencies: 293
-- Data for Name: tsgrhproveedores; Type: TABLE DATA; Schema: sgrh; Owner: suite
--

INSERT INTO sgrh.tsgrhproveedores (cod_proveedor, des_nbproveedor, aud_feccreacion, aud_fecmodificacion, aud_creadopor, aud_modificadopor) VALUES (1, 'ASDFGHJKL', '2019-10-28', '2019-10-28', 10, 10);
INSERT INTO sgrh.tsgrhproveedores (cod_proveedor, des_nbproveedor, aud_feccreacion, aud_fecmodificacion, aud_creadopor, aud_modificadopor) VALUES (2, 'Oracle', '2019-10-28', '2019-10-28', 10, 10);


--
-- TOC entry 4706 (class 0 OID 99575)
-- Dependencies: 294
-- Data for Name: tsgrhpuestos; Type: TABLE DATA; Schema: sgrh; Owner: suite
--

INSERT INTO sgrh.tsgrhpuestos (cod_puesto, des_puesto, cod_area, cod_acronimo) VALUES (1, 'DISEÑO Y MANTENIMIENTO DE BASE DE DATOS', 1, 'DBA');
INSERT INTO sgrh.tsgrhpuestos (cod_puesto, des_puesto, cod_area, cod_acronimo) VALUES (2, 'DESARROLLO BACKEND DE SOFTWARE COMERCIALIZABLE', 2, 'BACK');
INSERT INTO sgrh.tsgrhpuestos (cod_puesto, des_puesto, cod_area, cod_acronimo) VALUES (3, 'DISEÑO FRONTEND DE APLICATIVOS COMERCIALIZABLES', 3, 'FRONT');
INSERT INTO sgrh.tsgrhpuestos (cod_puesto, des_puesto, cod_area, cod_acronimo) VALUES (4, 'SOLUCION DE PROBLEMAS CON EL APLICATIVO DESARROLLADO Y MANTENIMIENTO DE CODIGO FUENTE', 4, 'QA');
INSERT INTO sgrh.tsgrhpuestos (cod_puesto, des_puesto, cod_area, cod_acronimo) VALUES (5, 'RESPONSABLE DE CAPACITACIÓN', 5, 'RC');
INSERT INTO sgrh.tsgrhpuestos (cod_puesto, des_puesto, cod_area, cod_acronimo) VALUES (6, 'RESPONSABLE DE RECURSOS HUMANOS Y AMBIENTE DE TRABAJO', 5, 'RRHAT');
INSERT INTO sgrh.tsgrhpuestos (cod_puesto, des_puesto, cod_area, cod_acronimo) VALUES (7, 'RESPOSABLE DE GESTIÓN DE RECURSOS ', 6, 'RGR');
INSERT INTO sgrh.tsgrhpuestos (cod_puesto, des_puesto, cod_area, cod_acronimo) VALUES (8, 'RESPONSABLE DE GESTIÓN DE PROYECTOS', 22, 'GPY');
INSERT INTO sgrh.tsgrhpuestos (cod_puesto, des_puesto, cod_area, cod_acronimo) VALUES (9, 'RESPONSABLE DE ADMINISTRACIÓN DE PROYECTOS ESPECÍFICOS', 20, 'APE');
INSERT INTO sgrh.tsgrhpuestos (cod_puesto, des_puesto, cod_area, cod_acronimo) VALUES (10, 'RESPONSABLE RECLUTAMIENTO Y SELECCIÓN', 21, 'R&S');
INSERT INTO sgrh.tsgrhpuestos (cod_puesto, des_puesto, cod_area, cod_acronimo) VALUES (11, 'REPRESENTANTE LEGAL', 23, 'RL');


--
-- TOC entry 4707 (class 0 OID 99579)
-- Dependencies: 295
-- Data for Name: tsgrhrelacionroles; Type: TABLE DATA; Schema: sgrh; Owner: suite
--

INSERT INTO sgrh.tsgrhrelacionroles (cod_plancapacitacion, cod_rolempleado, aud_feccreacion, aud_fecmodificacion, aud_creadopor, aud_modificadopor) VALUES (2, 1, '2019-10-28', '2019-10-28', 10, 10);
INSERT INTO sgrh.tsgrhrelacionroles (cod_plancapacitacion, cod_rolempleado, aud_feccreacion, aud_fecmodificacion, aud_creadopor, aud_modificadopor) VALUES (2, 2, '2019-10-28', '2019-10-28', 10, 10);
INSERT INTO sgrh.tsgrhrelacionroles (cod_plancapacitacion, cod_rolempleado, aud_feccreacion, aud_fecmodificacion, aud_creadopor, aud_modificadopor) VALUES (3, 1, '2019-10-28', '2019-10-28', 10, 10);
INSERT INTO sgrh.tsgrhrelacionroles (cod_plancapacitacion, cod_rolempleado, aud_feccreacion, aud_fecmodificacion, aud_creadopor, aud_modificadopor) VALUES (3, 2, '2019-10-28', '2019-10-28', 10, 10);


--
-- TOC entry 4708 (class 0 OID 99582)
-- Dependencies: 296
-- Data for Name: tsgrhrespuestasenc; Type: TABLE DATA; Schema: sgrh; Owner: suite
--

INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (475, 1, 82, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (476, 2, 82, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (477, 3, 82, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (478, 4, 82, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (479, 5, 82, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (480, 1, 83, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (481, 2, 83, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (482, 3, 83, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (483, 4, 83, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (484, 5, 83, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (485, 1, 84, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (486, 2, 84, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (487, 3, 84, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (488, 4, 84, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (489, 5, 84, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (490, 1, 85, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (491, 2, 85, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (492, 3, 85, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (493, 4, 85, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (494, 5, 85, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (495, 1, 86, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (496, 2, 86, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (497, 3, 86, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (498, 4, 86, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (499, 5, 86, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (500, 1, 87, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (501, 2, 87, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (502, 3, 87, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (503, 4, 87, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (504, 5, 87, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (505, 1, 88, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (506, 2, 88, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (507, 3, 88, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (508, 4, 88, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (509, 5, 88, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (510, 1, 89, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (511, 2, 89, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (512, 3, 89, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (513, 4, 89, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (514, 5, 89, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (515, 1, 90, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (516, 2, 90, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (517, 3, 90, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (518, 4, 90, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (519, 5, 90, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (520, 1, 93, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (521, 2, 93, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (522, 3, 93, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (523, 4, 93, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (524, 5, 93, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (525, 1, 94, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (526, 2, 94, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (527, 3, 94, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (528, 4, 94, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (529, 5, 94, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (530, 1, 95, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (531, 2, 95, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (532, 3, 95, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (533, 4, 95, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (534, 5, 95, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (535, 1, 98, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (536, 2, 98, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (537, 3, 98, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (538, 4, 98, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (539, 5, 98, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (540, 1, 99, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (541, 2, 99, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (542, 3, 99, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (543, 4, 99, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (544, 5, 99, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (545, 1, 100, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (546, 2, 100, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (547, 3, 100, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (548, 4, 100, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (549, 5, 100, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (550, 1, 101, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (551, 2, 101, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (552, 3, 101, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (553, 4, 101, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (554, 5, 101, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (555, 1, 102, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (556, 2, 102, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (557, 3, 102, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (558, 4, 102, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (559, 5, 102, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (561, 1, 104, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (562, 2, 104, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (563, 3, 104, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (564, 4, 104, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (565, 5, 104, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (566, 1, 105, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (567, 2, 105, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (568, 3, 105, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (569, 4, 105, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (570, 5, 105, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (571, 1, 106, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (572, 2, 106, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (573, 3, 106, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (574, 4, 106, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (575, 5, 106, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (576, 1, 156, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (577, 2, 156, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (578, 3, 156, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (579, 4, 156, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (580, 5, 156, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (581, 1, 157, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (582, 2, 157, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (583, 3, 157, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (584, 4, 157, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (585, 5, 157, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (586, 1, 158, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (587, 2, 158, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (588, 3, 158, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (589, 4, 158, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (590, 5, 158, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (591, 1, 159, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (592, 2, 159, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (593, 3, 159, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (594, 4, 159, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (595, 5, 159, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (596, 1, 160, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (597, 2, 160, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (598, 3, 160, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (599, 4, 160, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (600, 5, 160, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (601, 1, 161, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (602, 2, 161, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (603, 3, 161, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (604, 4, 161, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (605, 5, 161, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (606, 1, 117, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (607, 2, 117, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (608, 3, 117, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (609, 4, 117, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (610, 5, 117, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (611, 1, 118, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (612, 2, 118, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (613, 3, 118, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (614, 4, 118, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (615, 5, 118, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (616, 1, 119, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (617, 2, 119, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (618, 3, 119, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (619, 4, 119, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (620, 5, 119, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (621, 1, 120, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (622, 2, 120, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (623, 3, 120, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (624, 4, 120, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (625, 5, 120, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (626, 1, 121, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (627, 2, 121, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (628, 3, 121, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (629, 4, 121, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (630, 5, 121, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (631, 1, 122, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (632, 2, 122, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (633, 3, 122, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (634, 4, 122, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (635, 5, 122, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (637, 1, 124, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (638, 2, 124, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (639, 3, 124, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (640, 4, 124, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (641, 5, 124, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (642, 1, 125, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (643, 2, 125, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (644, 3, 125, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (645, 4, 125, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (646, 5, 125, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (647, 1, 126, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (648, 2, 126, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (649, 3, 126, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (650, 4, 126, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (651, 5, 126, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (652, 1, 127, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (653, 2, 127, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (654, 3, 127, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (655, 4, 127, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (656, 5, 127, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (657, 1, 128, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (658, 2, 128, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (659, 3, 128, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (660, 4, 128, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (661, 5, 128, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (662, 1, 129, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (663, 2, 129, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (664, 3, 129, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (665, 4, 129, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (666, 5, 129, false);


--
-- TOC entry 4709 (class 0 OID 99586)
-- Dependencies: 297
-- Data for Name: tsgrhrespuestaseva; Type: TABLE DATA; Schema: sgrh; Owner: suite
--



--
-- TOC entry 4710 (class 0 OID 99590)
-- Dependencies: 298
-- Data for Name: tsgrhrevplanoperativo; Type: TABLE DATA; Schema: sgrh; Owner: suite
--



--
-- TOC entry 4711 (class 0 OID 99599)
-- Dependencies: 299
-- Data for Name: tsgrhrolempleado; Type: TABLE DATA; Schema: sgrh; Owner: suite
--

INSERT INTO sgrh.tsgrhrolempleado (cod_rolempleado, des_nbrol, aud_feccreacion, aud_fecmodificacion, aud_creadopor, aud_modificadopor) VALUES (1, 'DMS', '2019-10-28', '2019-10-28', 10, 10);
INSERT INTO sgrh.tsgrhrolempleado (cod_rolempleado, des_nbrol, aud_feccreacion, aud_fecmodificacion, aud_creadopor, aud_modificadopor) VALUES (2, 'RH', '2019-10-28', '2019-10-28', 10, 10);
INSERT INTO sgrh.tsgrhrolempleado (cod_rolempleado, des_nbrol, aud_feccreacion, aud_fecmodificacion, aud_creadopor, aud_modificadopor) VALUES (3, 'TL', '2019-10-28', '2019-10-28', 10, 10);


--
-- TOC entry 4712 (class 0 OID 99603)
-- Dependencies: 300
-- Data for Name: tsgrhroles; Type: TABLE DATA; Schema: sgrh; Owner: suite
--

INSERT INTO sgrh.tsgrhroles (cod_rol, des_nbrol) VALUES (1, 'ADMINISTRADOR');
INSERT INTO sgrh.tsgrhroles (cod_rol, des_nbrol) VALUES (2, 'RESPONSABLE');
INSERT INTO sgrh.tsgrhroles (cod_rol, des_nbrol) VALUES (3, 'EMPLEADO');
INSERT INTO sgrh.tsgrhroles (cod_rol, des_nbrol) VALUES (4, 'LIDER DE CELULA');


--
-- TOC entry 4713 (class 0 OID 99607)
-- Dependencies: 301
-- Data for Name: tsgrhsubfactoreseva; Type: TABLE DATA; Schema: sgrh; Owner: suite
--



--
-- TOC entry 4714 (class 0 OID 99611)
-- Dependencies: 302
-- Data for Name: tsgrhtipocapacitacion; Type: TABLE DATA; Schema: sgrh; Owner: suite
--

INSERT INTO sgrh.tsgrhtipocapacitacion (cod_tipocapacitacion, des_nbtipocapacitacion, aud_feccreacion, aud_fecmodificacion, aud_creadopor, aud_modificadopor) VALUES (1, 'RHC', '2019-10-28', '2019-10-28', 10, 10);
INSERT INTO sgrh.tsgrhtipocapacitacion (cod_tipocapacitacion, des_nbtipocapacitacion, aud_feccreacion, aud_fecmodificacion, aud_creadopor, aud_modificadopor) VALUES (2, 'MLXG', '2019-10-28', '2019-10-28', 10, 10);


--
-- TOC entry 4715 (class 0 OID 99615)
-- Dependencies: 303
-- Data for Name: tsgrhvalidaevaluaciondes; Type: TABLE DATA; Schema: sgrh; Owner: suite
--



--
-- TOC entry 4748 (class 0 OID 99687)
-- Dependencies: 336
-- Data for Name: tsgrtagenda; Type: TABLE DATA; Schema: sgrt; Owner: suite
--



--
-- TOC entry 4749 (class 0 OID 99692)
-- Dependencies: 337
-- Data for Name: tsgrtarchivos; Type: TABLE DATA; Schema: sgrt; Owner: suite
--



--
-- TOC entry 4750 (class 0 OID 99699)
-- Dependencies: 338
-- Data for Name: tsgrtasistentes; Type: TABLE DATA; Schema: sgrt; Owner: suite
--



--
-- TOC entry 4751 (class 0 OID 99703)
-- Dependencies: 339
-- Data for Name: tsgrtattchticket; Type: TABLE DATA; Schema: sgrt; Owner: suite
--



--
-- TOC entry 4752 (class 0 OID 99710)
-- Dependencies: 340
-- Data for Name: tsgrtayudatopico; Type: TABLE DATA; Schema: sgrt; Owner: suite
--



--
-- TOC entry 4753 (class 0 OID 99714)
-- Dependencies: 341
-- Data for Name: tsgrtcategoriafaq; Type: TABLE DATA; Schema: sgrt; Owner: suite
--



--
-- TOC entry 4754 (class 0 OID 99721)
-- Dependencies: 342
-- Data for Name: tsgrtchat; Type: TABLE DATA; Schema: sgrt; Owner: suite
--



--
-- TOC entry 4755 (class 0 OID 99728)
-- Dependencies: 343
-- Data for Name: tsgrtciudades; Type: TABLE DATA; Schema: sgrt; Owner: suite
--

INSERT INTO sgrt.tsgrtciudades (cod_ciudad, des_nbciudad, cod_estadorep) VALUES (6, 'ZACATLAN DE LAS MANZANAS', 1);
INSERT INTO sgrt.tsgrtciudades (cod_ciudad, des_nbciudad, cod_estadorep) VALUES (7, 'CIUDAD DE MEXICO', 4);
INSERT INTO sgrt.tsgrtciudades (cod_ciudad, des_nbciudad, cod_estadorep) VALUES (8, 'XALAPA', 3);
INSERT INTO sgrt.tsgrtciudades (cod_ciudad, des_nbciudad, cod_estadorep) VALUES (9, 'POZA RICA', 3);
INSERT INTO sgrt.tsgrtciudades (cod_ciudad, des_nbciudad, cod_estadorep) VALUES (10, 'TLAXCALA', 2);
INSERT INTO sgrt.tsgrtciudades (cod_ciudad, des_nbciudad, cod_estadorep) VALUES (11, 'APIZACO', 2);


--
-- TOC entry 4756 (class 0 OID 99732)
-- Dependencies: 344
-- Data for Name: tsgrtcomentariosagenda; Type: TABLE DATA; Schema: sgrt; Owner: suite
--



--
-- TOC entry 4757 (class 0 OID 99739)
-- Dependencies: 345
-- Data for Name: tsgrtcomentariosreunion; Type: TABLE DATA; Schema: sgrt; Owner: suite
--



--
-- TOC entry 4758 (class 0 OID 99746)
-- Dependencies: 346
-- Data for Name: tsgrtcompromisos; Type: TABLE DATA; Schema: sgrt; Owner: suite
--



--
-- TOC entry 4759 (class 0 OID 99752)
-- Dependencies: 347
-- Data for Name: tsgrtcorreo; Type: TABLE DATA; Schema: sgrt; Owner: suite
--



--
-- TOC entry 4760 (class 0 OID 99758)
-- Dependencies: 348
-- Data for Name: tsgrtdatossolicitud; Type: TABLE DATA; Schema: sgrt; Owner: suite
--



--
-- TOC entry 4761 (class 0 OID 99761)
-- Dependencies: 349
-- Data for Name: tsgrtdepartamento; Type: TABLE DATA; Schema: sgrt; Owner: suite
--



--
-- TOC entry 4762 (class 0 OID 99770)
-- Dependencies: 350
-- Data for Name: tsgrtedosolicitudes; Type: TABLE DATA; Schema: sgrt; Owner: suite
--



--
-- TOC entry 4763 (class 0 OID 99773)
-- Dependencies: 351
-- Data for Name: tsgrtelementos; Type: TABLE DATA; Schema: sgrt; Owner: suite
--



--
-- TOC entry 4764 (class 0 OID 99777)
-- Dependencies: 352
-- Data for Name: tsgrtestados; Type: TABLE DATA; Schema: sgrt; Owner: suite
--

INSERT INTO sgrt.tsgrtestados (cod_estadorep, des_nbestado) VALUES (1, 'PUEBLA');
INSERT INTO sgrt.tsgrtestados (cod_estadorep, des_nbestado) VALUES (2, 'TLAXCALA');
INSERT INTO sgrt.tsgrtestados (cod_estadorep, des_nbestado) VALUES (3, 'VERACRUZ');
INSERT INTO sgrt.tsgrtestados (cod_estadorep, des_nbestado) VALUES (4, 'DISTRITO FEDERAL');


--
-- TOC entry 4765 (class 0 OID 99781)
-- Dependencies: 353
-- Data for Name: tsgrtfaq; Type: TABLE DATA; Schema: sgrt; Owner: suite
--



--
-- TOC entry 4766 (class 0 OID 99788)
-- Dependencies: 354
-- Data for Name: tsgrtgrupo; Type: TABLE DATA; Schema: sgrt; Owner: suite
--



--
-- TOC entry 4767 (class 0 OID 99793)
-- Dependencies: 355
-- Data for Name: tsgrtinvitados; Type: TABLE DATA; Schema: sgrt; Owner: suite
--



--
-- TOC entry 4768 (class 0 OID 99800)
-- Dependencies: 356
-- Data for Name: tsgrtlugares; Type: TABLE DATA; Schema: sgrt; Owner: suite
--

INSERT INTO sgrt.tsgrtlugares (cod_lugar, des_nombre, cod_ciudad) VALUES (2, 'TLAXCALA DE XICONTECATL', 10);
INSERT INTO sgrt.tsgrtlugares (cod_lugar, des_nombre, cod_ciudad) VALUES (3, 'SANTA ANA', 10);
INSERT INTO sgrt.tsgrtlugares (cod_lugar, des_nombre, cod_ciudad) VALUES (4, 'CHIUATEMPAN', 10);


--
-- TOC entry 4769 (class 0 OID 99804)
-- Dependencies: 357
-- Data for Name: tsgrtmsjticket; Type: TABLE DATA; Schema: sgrt; Owner: suite
--



--
-- TOC entry 4770 (class 0 OID 99814)
-- Dependencies: 358
-- Data for Name: tsgrtnota; Type: TABLE DATA; Schema: sgrt; Owner: suite
--



--
-- TOC entry 4771 (class 0 OID 99822)
-- Dependencies: 359
-- Data for Name: tsgrtplantillacorreos; Type: TABLE DATA; Schema: sgrt; Owner: suite
--



--
-- TOC entry 4772 (class 0 OID 99830)
-- Dependencies: 360
-- Data for Name: tsgrtprioridad; Type: TABLE DATA; Schema: sgrt; Owner: suite
--



--
-- TOC entry 4773 (class 0 OID 99834)
-- Dependencies: 361
-- Data for Name: tsgrtresppredefinida; Type: TABLE DATA; Schema: sgrt; Owner: suite
--



--
-- TOC entry 4774 (class 0 OID 99841)
-- Dependencies: 362
-- Data for Name: tsgrtrespuesta; Type: TABLE DATA; Schema: sgrt; Owner: suite
--



--
-- TOC entry 4775 (class 0 OID 99848)
-- Dependencies: 363
-- Data for Name: tsgrtreuniones; Type: TABLE DATA; Schema: sgrt; Owner: suite
--



--
-- TOC entry 4776 (class 0 OID 99855)
-- Dependencies: 364
-- Data for Name: tsgrtservicios; Type: TABLE DATA; Schema: sgrt; Owner: suite
--



--
-- TOC entry 4777 (class 0 OID 99859)
-- Dependencies: 365
-- Data for Name: tsgrtsolicitudservicios; Type: TABLE DATA; Schema: sgrt; Owner: suite
--



--
-- TOC entry 4778 (class 0 OID 99863)
-- Dependencies: 366
-- Data for Name: tsgrtticket; Type: TABLE DATA; Schema: sgrt; Owner: suite
--



--
-- TOC entry 4835 (class 0 OID 102697)
-- Dependencies: 428
-- Data for Name: tsisatappservices; Type: TABLE DATA; Schema: sisat; Owner: suite
--



--
-- TOC entry 4836 (class 0 OID 102700)
-- Dependencies: 429
-- Data for Name: tsisatarquitecturas; Type: TABLE DATA; Schema: sisat; Owner: suite
--



--
-- TOC entry 4837 (class 0 OID 102703)
-- Dependencies: 430
-- Data for Name: tsisatasignaciones; Type: TABLE DATA; Schema: sisat; Owner: suite
--



--
-- TOC entry 4838 (class 0 OID 102707)
-- Dependencies: 431
-- Data for Name: tsisatcandidatos; Type: TABLE DATA; Schema: sisat; Owner: suite
--



--
-- TOC entry 4839 (class 0 OID 102711)
-- Dependencies: 432
-- Data for Name: tsisatcartaaceptacion; Type: TABLE DATA; Schema: sisat; Owner: suite
--



--
-- TOC entry 4840 (class 0 OID 102718)
-- Dependencies: 433
-- Data for Name: tsisatcartaasignacion; Type: TABLE DATA; Schema: sisat; Owner: suite
--

INSERT INTO sisat.tsisatcartaasignacion (cod_asignacion, cod_prospecto, cod_perfil, des_actividades, des_lugarsalida, des_lugarllegada, fec_salida, fec_llegada, cod_transporte, des_lugarhopedaje, fec_hospedaje, des_computadora, cod_telefono, des_accesorios, des_nbresponsable, des_nbpuesto, des_lugarresp, cod_telefonoresp, tim_horario, fec_iniciocontra, fec_terminocontra, imp_sueldomensual, imp_nominaimss, imp_honorarios, imp_otros, cod_rfc, des_razonsocial, des_correo, cod_cpostal, des_direccionfact, cod_cliente, cod_gpy, cod_rhta, cod_ape, cod_rys, des_observacion, aud_cod_creadopor, aud_cod_modificadopor, aud_fecha_creacion, aud_fecha_modificacion) VALUES (2, 1, 2, 'DESARROLLO WEB', 'TLAXCALA', 'CD. DE MEXICO', '2019-06-07', '2019-06-07', 'PUBLICO', 'RENTA DE CASA', '2019-06-07', 'CLIENTE', 'PROPIO', 'PROPIO', 'RAMIRO MONTEZ', 'RESPONSABLE DE DESARROLLO', 'CD. DE MEXICO', '6789327689', '08:00:00', '2019-06-07', '2019-06-07', 1200.00, 1000.00, 500.00, 0.00, 'FNCKCCKFHVNC', 'PATITOS', 'PATITOSLOVER@GMAIL.COM', 67890, 'CALLE NICARAGUA', 3, 16, 10, 14, 15, 'SIN ', 10, 10, '2019-06-07', '2019-06-07');
INSERT INTO sisat.tsisatcartaasignacion (cod_asignacion, cod_prospecto, cod_perfil, des_actividades, des_lugarsalida, des_lugarllegada, fec_salida, fec_llegada, cod_transporte, des_lugarhopedaje, fec_hospedaje, des_computadora, cod_telefono, des_accesorios, des_nbresponsable, des_nbpuesto, des_lugarresp, cod_telefonoresp, tim_horario, fec_iniciocontra, fec_terminocontra, imp_sueldomensual, imp_nominaimss, imp_honorarios, imp_otros, cod_rfc, des_razonsocial, des_correo, cod_cpostal, des_direccionfact, cod_cliente, cod_gpy, cod_rhta, cod_ape, cod_rys, des_observacion, aud_cod_creadopor, aud_cod_modificadopor, aud_fecha_creacion, aud_fecha_modificacion) VALUES (4, 2, 3, 'PRUEBAS DE JAVA', 'APIZACO', 'PUEBLA', '2019-11-17', '2019-11-17', 'COLECTIVO', 'HOTEL', '2019-11-17', 'MBN', 'EMPRESA', 'PROPIO', 'PABLO DEL MONTE', 'GERENTE', 'CALLE MONTE', '(778)888-88-88', '08:00:00', '2019-01-18', '2019-01-27', 1600.00, 500.00, 400.00, 0.00, 'YTFG789NOY65F', 'PABLOMIX', 'JUANPABLITO@GMAIL.COM', 76868, 'CALLE BUENA VISTA', 4, 13, 10, 14, 12, NULL, 10, NULL, '2019-11-15', NULL);
INSERT INTO sisat.tsisatcartaasignacion (cod_asignacion, cod_prospecto, cod_perfil, des_actividades, des_lugarsalida, des_lugarllegada, fec_salida, fec_llegada, cod_transporte, des_lugarhopedaje, fec_hospedaje, des_computadora, cod_telefono, des_accesorios, des_nbresponsable, des_nbpuesto, des_lugarresp, cod_telefonoresp, tim_horario, fec_iniciocontra, fec_terminocontra, imp_sueldomensual, imp_nominaimss, imp_honorarios, imp_otros, cod_rfc, des_razonsocial, des_correo, cod_cpostal, des_direccionfact, cod_cliente, cod_gpy, cod_rhta, cod_ape, cod_rys, des_observacion, aud_cod_creadopor, aud_cod_modificadopor, aud_fecha_creacion, aud_fecha_modificacion) VALUES (5, 2, 3, 'PRUEBAS DE JAVA', 'APIZACO', 'PUEBLA', '2019-11-17', '2019-11-17', 'COLECTIVO', 'HOTEL', '2019-11-17', 'MBN', 'EMPRESA', 'PROPIO', 'PABLO DEL MONTE', 'GERENTE', 'CALLE MONTE', '(778)888-88-88', '08:00:00', '2019-01-18', '2019-12-27', 1600.00, 500.00, 400.00, 0.00, 'YTFG789NOY65F', 'PABLOMIX', 'JUANPABLITO@GMAIL.COM', 76868, 'CALLE BUENA VISTA', 4, 13, 10, 14, 12, NULL, 10, NULL, '2019-11-15', NULL);
INSERT INTO sisat.tsisatcartaasignacion (cod_asignacion, cod_prospecto, cod_perfil, des_actividades, des_lugarsalida, des_lugarllegada, fec_salida, fec_llegada, cod_transporte, des_lugarhopedaje, fec_hospedaje, des_computadora, cod_telefono, des_accesorios, des_nbresponsable, des_nbpuesto, des_lugarresp, cod_telefonoresp, tim_horario, fec_iniciocontra, fec_terminocontra, imp_sueldomensual, imp_nominaimss, imp_honorarios, imp_otros, cod_rfc, des_razonsocial, des_correo, cod_cpostal, des_direccionfact, cod_cliente, cod_gpy, cod_rhta, cod_ape, cod_rys, des_observacion, aud_cod_creadopor, aud_cod_modificadopor, aud_fecha_creacion, aud_fecha_modificacion) VALUES (1, 1, 1, 'DESARROLLAR', 'APIZACO', 'TLAXCALA', '2019-02-23', '2019-02-23', 'COLECTIVO', 'HOTEL', '2019-02-23', 'MBN', 'MBN', 'MBN', 'PEDRO PEREZ', 'RESPONSABLE', 'TLAXCALA', '4343535598', '08:00:00', '2019-02-23', '2019-12-07', 1600.00, 1000.00, 900.00, 100.00, 'BFCOBJVJKVBK', 'PEDRITOSPEDRAZA', 'JDSKHVCKS@GMAIL.COM', 56980, 'AVENIDA FLORES VAGON', 1, 16, 10, 14, 15, 'SIN OBSERVACION', 10, 10, '2019-02-23', '2019-02-23');
INSERT INTO sisat.tsisatcartaasignacion (cod_asignacion, cod_prospecto, cod_perfil, des_actividades, des_lugarsalida, des_lugarllegada, fec_salida, fec_llegada, cod_transporte, des_lugarhopedaje, fec_hospedaje, des_computadora, cod_telefono, des_accesorios, des_nbresponsable, des_nbpuesto, des_lugarresp, cod_telefonoresp, tim_horario, fec_iniciocontra, fec_terminocontra, imp_sueldomensual, imp_nominaimss, imp_honorarios, imp_otros, cod_rfc, des_razonsocial, des_correo, cod_cpostal, des_direccionfact, cod_cliente, cod_gpy, cod_rhta, cod_ape, cod_rys, des_observacion, aud_cod_creadopor, aud_cod_modificadopor, aud_fecha_creacion, aud_fecha_modificacion) VALUES (3, 2, 3, 'PRUEBAS ', 'APIZACO ', 'PUEBLA', '2019-11-15', '2019-11-29', 'COLECTIVO', 'HOTEL', '2019-11-15', 'MBN', 'EMPRESA', 'PROPIO', 'ANA KAREN PICHÓN SÁNCHEZ', 'LIDER', 'PUEBLA', '(246)989-89-98', '08:00:00', '2019-01-18', '2018-12-24', 16.00, 500.00, 400.00, 0.00, 'HGHXBDGDBY', 'ANNIKA', 'ANNIKA@GMAIL.COM', 90480, 'CALLE ANAKAREN', 2, 19, 10, 14, 15, NULL, 10, NULL, '2019-11-15', NULL);


--
-- TOC entry 4841 (class 0 OID 102725)
-- Dependencies: 434
-- Data for Name: tsisatcomentcartaasignacion; Type: TABLE DATA; Schema: sisat; Owner: suite
--

INSERT INTO sisat.tsisatcomentcartaasignacion (cod_comentcartaasignacion, des_comentcartaasignacion, bol_validacion, aud_fecha_creacion, aud_fecha_modificacion, aud_cod_creadopor, aud_cod_modificadopor, cod_asignacion) VALUES (4, 'EJEMPLO', true, '2019-11-28', NULL, 10, NULL, 1);


--
-- TOC entry 4842 (class 0 OID 102729)
-- Dependencies: 435
-- Data for Name: tsisatcomentcosteo; Type: TABLE DATA; Schema: sisat; Owner: suite
--



--
-- TOC entry 4843 (class 0 OID 102733)
-- Dependencies: 436
-- Data for Name: tsisatcomententrevista; Type: TABLE DATA; Schema: sisat; Owner: suite
--



--
-- TOC entry 4844 (class 0 OID 102740)
-- Dependencies: 437
-- Data for Name: tsisatcomentvacantes; Type: TABLE DATA; Schema: sisat; Owner: suite
--

INSERT INTO sisat.tsisatcomentvacantes (cod_comentvacante, des_comentvacante, bol_validacion, aud_fecha_creacion, aud_fecha_modificacion, aud_cod_creadopor, aud_cod_modificadopor, cod_vacante) VALUES (3, 'RECHAZADO', false, '2019-11-20', NULL, 10, NULL, 1);


--
-- TOC entry 4845 (class 0 OID 102744)
-- Dependencies: 438
-- Data for Name: tsisatcontrataciones; Type: TABLE DATA; Schema: sisat; Owner: suite
--

INSERT INTO sisat.tsisatcontrataciones (cod_contratacion, fec_inicio, fec_termino, des_esquema, cod_salarioestmin, cod_salarioestmax, tim_jornada, aud_cod_creadopor, aud_cod_modificadopor, aud_fec_creacion, aud_fec_modificacion) VALUES (1, '2019-11-14', '2020-02-28', '100% NÓMINA IMSS', 1000.00, 2500.00, '10 a 5 pm', 10, 10, '2019-11-14', '2019-11-14');
INSERT INTO sisat.tsisatcontrataciones (cod_contratacion, fec_inicio, fec_termino, des_esquema, cod_salarioestmin, cod_salarioestmax, tim_jornada, aud_cod_creadopor, aud_cod_modificadopor, aud_fec_creacion, aud_fec_modificacion) VALUES (2, '2019-11-14', '2020-01-16', 'ESQUEMA MIXTO', 1000.00, 2500.00, '10 a 5 pm', 10, 10, '2019-11-14', '2019-11-14');
INSERT INTO sisat.tsisatcontrataciones (cod_contratacion, fec_inicio, fec_termino, des_esquema, cod_salarioestmin, cod_salarioestmax, tim_jornada, aud_cod_creadopor, aud_cod_modificadopor, aud_fec_creacion, aud_fec_modificacion) VALUES (10, '2019-11-18', '2020-02-28', '100% NÓMINA IMSS', 4000.00, 6500.00, '10 a 5 pm', 10, 10, '2019-11-15', '2019-11-15');


--
-- TOC entry 4846 (class 0 OID 102748)
-- Dependencies: 439
-- Data for Name: tsisatcotizaciones; Type: TABLE DATA; Schema: sisat; Owner: suite
--



--
-- TOC entry 4847 (class 0 OID 102754)
-- Dependencies: 440
-- Data for Name: tsisatcursosycertificados; Type: TABLE DATA; Schema: sisat; Owner: suite
--



--
-- TOC entry 4848 (class 0 OID 102758)
-- Dependencies: 441
-- Data for Name: tsisatentrevistas; Type: TABLE DATA; Schema: sisat; Owner: suite
--



--
-- TOC entry 4849 (class 0 OID 102765)
-- Dependencies: 442
-- Data for Name: tsisatenviocorreos; Type: TABLE DATA; Schema: sisat; Owner: suite
--



--
-- TOC entry 4850 (class 0 OID 102772)
-- Dependencies: 443
-- Data for Name: tsisatescolaridad; Type: TABLE DATA; Schema: sisat; Owner: suite
--



--
-- TOC entry 4851 (class 0 OID 102776)
-- Dependencies: 444
-- Data for Name: tsisatexperienciaslaborales; Type: TABLE DATA; Schema: sisat; Owner: suite
--



--
-- TOC entry 4852 (class 0 OID 102783)
-- Dependencies: 445
-- Data for Name: tsisatfirmareqper; Type: TABLE DATA; Schema: sisat; Owner: suite
--

INSERT INTO sisat.tsisatfirmareqper (cod_firma, cod_solicita, cod_puestosolicita, cod_autoriza, cod_puestoautoriza, cod_vacante) VALUES (1, 17, 11, 24, 1, 1);
INSERT INTO sisat.tsisatfirmareqper (cod_firma, cod_solicita, cod_puestosolicita, cod_autoriza, cod_puestoautoriza, cod_vacante) VALUES (2, 17, 11, 34, 1, 2);
INSERT INTO sisat.tsisatfirmareqper (cod_firma, cod_solicita, cod_puestosolicita, cod_autoriza, cod_puestoautoriza, cod_vacante) VALUES (3, 17, 11, 10, 2, 3);


--
-- TOC entry 4853 (class 0 OID 102787)
-- Dependencies: 446
-- Data for Name: tsisatframeworks; Type: TABLE DATA; Schema: sisat; Owner: suite
--



--
-- TOC entry 4854 (class 0 OID 102790)
-- Dependencies: 447
-- Data for Name: tsisathabilidades; Type: TABLE DATA; Schema: sisat; Owner: suite
--



--
-- TOC entry 4855 (class 0 OID 102794)
-- Dependencies: 448
-- Data for Name: tsisatherramientas; Type: TABLE DATA; Schema: sisat; Owner: suite
--



--
-- TOC entry 4857 (class 0 OID 102802)
-- Dependencies: 450
-- Data for Name: tsisatides; Type: TABLE DATA; Schema: sisat; Owner: suite
--



--
-- TOC entry 4858 (class 0 OID 102805)
-- Dependencies: 451
-- Data for Name: tsisatidiomas; Type: TABLE DATA; Schema: sisat; Owner: suite
--

INSERT INTO sisat.tsisatidiomas (cod_idioma, cod_nbidioma) VALUES (1, 'INGLES');
INSERT INTO sisat.tsisatidiomas (cod_idioma, cod_nbidioma) VALUES (2, 'ALEMAN');
INSERT INTO sisat.tsisatidiomas (cod_idioma, cod_nbidioma) VALUES (3, 'PORTUGUES');


--
-- TOC entry 4859 (class 0 OID 102809)
-- Dependencies: 452
-- Data for Name: tsisatlenguajes; Type: TABLE DATA; Schema: sisat; Owner: suite
--



--
-- TOC entry 4860 (class 0 OID 102812)
-- Dependencies: 453
-- Data for Name: tsisatmaquetados; Type: TABLE DATA; Schema: sisat; Owner: suite
--



--
-- TOC entry 4861 (class 0 OID 102815)
-- Dependencies: 454
-- Data for Name: tsisatmetodologias; Type: TABLE DATA; Schema: sisat; Owner: suite
--



--
-- TOC entry 4862 (class 0 OID 102818)
-- Dependencies: 455
-- Data for Name: tsisatmodelados; Type: TABLE DATA; Schema: sisat; Owner: suite
--



--
-- TOC entry 4863 (class 0 OID 102821)
-- Dependencies: 456
-- Data for Name: tsisatordenservicio; Type: TABLE DATA; Schema: sisat; Owner: suite
--

INSERT INTO sisat.tsisatordenservicio (cod_ordenservicio, cod_estadorep, cod_ciudad, fec_fecha, cod_puestocte, des_nbservicio, cnu_cantidad, txt_concepto, imp_inversionhr, txt_condicionescomer, des_ubcnconsultor, fec_finservicio, cod_gpy, cod_cliente, aud_cod_creadopor, aud_cod_modificadopor, aud_fec_creacion, aud_fec_modificacion) VALUES (12, 1, 6, '2019-12-09', 'EJEMPLO', 'EJEMPLO', 20, 'EJEMPLO', 10.00, 'EJEMPLO', 'UBICACIÓN DE EJEMPLO', '2019-12-09', 16, 4, 10, NULL, '2019-12-09', NULL);
INSERT INTO sisat.tsisatordenservicio (cod_ordenservicio, cod_estadorep, cod_ciudad, fec_fecha, cod_puestocte, des_nbservicio, cnu_cantidad, txt_concepto, imp_inversionhr, txt_condicionescomer, des_ubcnconsultor, fec_finservicio, cod_gpy, cod_cliente, aud_cod_creadopor, aud_cod_modificadopor, aud_fec_creacion, aud_fec_modificacion) VALUES (13, 2, 11, '2019-12-09', 'EJEMPLO', 'EJEMPLO', 20, 'EJEMPLO', 10.00, 'EJEMPLO', 'UBICACIÓN DE EJEMPLO', '2019-12-09', 16, 2, 10, NULL, '2019-12-09', NULL);
INSERT INTO sisat.tsisatordenservicio (cod_ordenservicio, cod_estadorep, cod_ciudad, fec_fecha, cod_puestocte, des_nbservicio, cnu_cantidad, txt_concepto, imp_inversionhr, txt_condicionescomer, des_ubcnconsultor, fec_finservicio, cod_gpy, cod_cliente, aud_cod_creadopor, aud_cod_modificadopor, aud_fec_creacion, aud_fec_modificacion) VALUES (47, 1, 6, '2019-12-09', 'EJEMPLO', 'EJEMPLO', 1000, 'EJEMPLO', 20.00, 'EJEMPLO', 'EJEMPLO', '2019-12-09', 16, 2, 10, 10, '2019-12-09', '2019-12-09');
INSERT INTO sisat.tsisatordenservicio (cod_ordenservicio, cod_estadorep, cod_ciudad, fec_fecha, cod_puestocte, des_nbservicio, cnu_cantidad, txt_concepto, imp_inversionhr, txt_condicionescomer, des_ubcnconsultor, fec_finservicio, cod_gpy, cod_cliente, aud_cod_creadopor, aud_cod_modificadopor, aud_fec_creacion, aud_fec_modificacion) VALUES (48, 1, 6, '2019-12-09', 'EJEMPLO', 'EJEMPLO', 1000, 'EJEMPLO', 20.00, 'EJEMPLO', 'EJEMPLO', '2019-12-09', 16, 2, 10, NULL, '2019-12-09', NULL);
INSERT INTO sisat.tsisatordenservicio (cod_ordenservicio, cod_estadorep, cod_ciudad, fec_fecha, cod_puestocte, des_nbservicio, cnu_cantidad, txt_concepto, imp_inversionhr, txt_condicionescomer, des_ubcnconsultor, fec_finservicio, cod_gpy, cod_cliente, aud_cod_creadopor, aud_cod_modificadopor, aud_fec_creacion, aud_fec_modificacion) VALUES (49, 1, 6, '2019-12-09', 'EJEMPLO', 'EJEMPLO', 1000, 'EJEMPLO', 20.00, 'EJEMPLO', 'EJEMPLO', '2019-12-09', 16, 2, 10, NULL, '2019-12-09', NULL);
INSERT INTO sisat.tsisatordenservicio (cod_ordenservicio, cod_estadorep, cod_ciudad, fec_fecha, cod_puestocte, des_nbservicio, cnu_cantidad, txt_concepto, imp_inversionhr, txt_condicionescomer, des_ubcnconsultor, fec_finservicio, cod_gpy, cod_cliente, aud_cod_creadopor, aud_cod_modificadopor, aud_fec_creacion, aud_fec_modificacion) VALUES (50, 2, 10, '2019-12-10', 'EJEMPLO', 'EJEMPLO', 20, 'EJEMPLO', 10.00, 'EJEMPLO', 'UBICACIÓN DE EJEMPLO', '2019-12-10', 16, 1, 10, 10, '2019-12-10', '2019-12-10');
INSERT INTO sisat.tsisatordenservicio (cod_ordenservicio, cod_estadorep, cod_ciudad, fec_fecha, cod_puestocte, des_nbservicio, cnu_cantidad, txt_concepto, imp_inversionhr, txt_condicionescomer, des_ubcnconsultor, fec_finservicio, cod_gpy, cod_cliente, aud_cod_creadopor, aud_cod_modificadopor, aud_fec_creacion, aud_fec_modificacion) VALUES (51, 3, 9, '2019-12-10', 'EJEMPLO', 'EJEMPLO', 20, 'EJEMPLO', 10.00, 'EJEMPLO', 'UBICACIÓN DE EJEMPLO', '2019-12-10', 16, 2, 10, 10, '2019-12-10', '2019-12-10');
INSERT INTO sisat.tsisatordenservicio (cod_ordenservicio, cod_estadorep, cod_ciudad, fec_fecha, cod_puestocte, des_nbservicio, cnu_cantidad, txt_concepto, imp_inversionhr, txt_condicionescomer, des_ubcnconsultor, fec_finservicio, cod_gpy, cod_cliente, aud_cod_creadopor, aud_cod_modificadopor, aud_fec_creacion, aud_fec_modificacion) VALUES (52, 1, 6, '2019-12-10', 'EJEMPLO', 'EJEMPLO', 20, 'EJEMPLO', 10.00, 'EJEMPLO', 'UBICACIÓN DE EJEMPLO', '2019-12-10', 16, 1, 10, 10, '2019-12-10', '2019-12-10');
INSERT INTO sisat.tsisatordenservicio (cod_ordenservicio, cod_estadorep, cod_ciudad, fec_fecha, cod_puestocte, des_nbservicio, cnu_cantidad, txt_concepto, imp_inversionhr, txt_condicionescomer, des_ubcnconsultor, fec_finservicio, cod_gpy, cod_cliente, aud_cod_creadopor, aud_cod_modificadopor, aud_fec_creacion, aud_fec_modificacion) VALUES (53, 1, 6, '2019-12-10', 'EJEMPLO', 'EJEMPLO', 20, 'EJEMPLO', 10.00, 'EJEMPLO', 'UBICACIÓN DE EJEMPLO', '2019-12-10', 16, 1, 10, 10, '2019-12-10', '2019-12-10');
INSERT INTO sisat.tsisatordenservicio (cod_ordenservicio, cod_estadorep, cod_ciudad, fec_fecha, cod_puestocte, des_nbservicio, cnu_cantidad, txt_concepto, imp_inversionhr, txt_condicionescomer, des_ubcnconsultor, fec_finservicio, cod_gpy, cod_cliente, aud_cod_creadopor, aud_cod_modificadopor, aud_fec_creacion, aud_fec_modificacion) VALUES (54, 1, 6, '2019-12-10', 'EJEMPLO', 'EJEMPLO', 20, 'EJEMPLO', 10.00, 'EJEMPLO', 'UBICACIÓN DE EJEMPLO', '2019-12-10', 16, 1, 10, 10, '2019-12-10', '2019-12-10');


--
-- TOC entry 4864 (class 0 OID 102828)
-- Dependencies: 457
-- Data for Name: tsisatpatrones; Type: TABLE DATA; Schema: sisat; Owner: suite
--



--
-- TOC entry 4865 (class 0 OID 102831)
-- Dependencies: 458
-- Data for Name: tsisatprospectos; Type: TABLE DATA; Schema: sisat; Owner: suite
--

INSERT INTO sisat.tsisatprospectos (cod_prospecto, des_nombre, des_nombres, des_appaterno, des_apmaterno, des_lugarnacimiento, fec_nacimiento, cod_edad, cod_edocivil, des_nbpadre, des_nbmadre, cod_numhermanos, des_nbcalle, cod_numcasa, des_colonia, des_localidad, des_municipio, des_estado, cod_cpostal, cod_tiposangre, des_emailmbn, des_emailpersonal, des_pasatiempo, cod_telefonocasa, cod_telefonomovil, cod_rfc, cod_nss, cod_curp, des_nacionalidad, cod_administrador, fec_fechacoment, txt_comentarios, bin_foto, cod_tipofoto, cod_extensionfoto, des_puestovacante, anio_experiencia, aud_cod_creadopor, aud_cod_modificadopor, aud_fec_creacion, aud_fec_modifiacion) VALUES (2, 'FERNANDO', ' ', 'MORALES', 'MOZO', 'TLAXCALA', '1996-05-30', 23, 'SOLTERO', 'PEDRITO', 'JUANITA', 2, 'FRANCISCO I MADERO', 50, 'VENUSTIANO', 'XALOZTOC', 'XALOZTOC', 'TLAXCALA', 23578, 'O+', 'PABLITO.MBN@GMAIL.COM', 'JUANPABLITO@GMAIL.COM', 'GAMER', '54543', '4353678903', 'BBDFKGKDFJBVK', '5464647', 'LJFEVBKJBJF', 'MEXICANO', 1, '2019-02-23', 'USUSKIHDKBL', NULL, 'JPG', 'JPG', 'DESARROLLADOR', 1, 10, 10, '2019-02-23', '2019-02-23');
INSERT INTO sisat.tsisatprospectos (cod_prospecto, des_nombre, des_nombres, des_appaterno, des_apmaterno, des_lugarnacimiento, fec_nacimiento, cod_edad, cod_edocivil, des_nbpadre, des_nbmadre, cod_numhermanos, des_nbcalle, cod_numcasa, des_colonia, des_localidad, des_municipio, des_estado, cod_cpostal, cod_tiposangre, des_emailmbn, des_emailpersonal, des_pasatiempo, cod_telefonocasa, cod_telefonomovil, cod_rfc, cod_nss, cod_curp, des_nacionalidad, cod_administrador, fec_fechacoment, txt_comentarios, bin_foto, cod_tipofoto, cod_extensionfoto, des_puestovacante, anio_experiencia, aud_cod_creadopor, aud_cod_modificadopor, aud_fec_creacion, aud_fec_modifiacion) VALUES (1, 'JUAN', 'PABLO', 'VENEGAS', 'SANCHEZ', 'TLAXCALA', '1996-05-09', 23, 'SOLTERO', 'PEDRITO', 'JUANITA', 2, 'FRANCISCO I MADERO', 50, 'VENUSTIANO', 'XALOZTOC', 'XALOZTOC', 'TLAXCALA', 23578, 'O+', 'PABLITO.MBN@GMAIL.COM', 'FERNANDO.MORALES.MOZO@GMAIL.COM', 'GAMER', '54543', '4353678903', 'BBDFKGKDFJBVK', '5464647', 'LJFEVBKJBJF', 'MEXICANO', 1, '2019-02-23', 'USUSKIHDKBL', NULL, 'JPG', 'JPG', 'DESARROLLADOR', 1, 10, 10, '2019-02-23', '2019-02-23');


--
-- TOC entry 4866 (class 0 OID 102840)
-- Dependencies: 459
-- Data for Name: tsisatprospectos_idiomas; Type: TABLE DATA; Schema: sisat; Owner: suite
--



--
-- TOC entry 4867 (class 0 OID 102843)
-- Dependencies: 460
-- Data for Name: tsisatprotocolos; Type: TABLE DATA; Schema: sisat; Owner: suite
--



--
-- TOC entry 4868 (class 0 OID 102846)
-- Dependencies: 461
-- Data for Name: tsisatproyectos; Type: TABLE DATA; Schema: sisat; Owner: suite
--



--
-- TOC entry 4869 (class 0 OID 102850)
-- Dependencies: 462
-- Data for Name: tsisatqa; Type: TABLE DATA; Schema: sisat; Owner: suite
--



--
-- TOC entry 4870 (class 0 OID 102853)
-- Dependencies: 463
-- Data for Name: tsisatrepositoriolibrerias; Type: TABLE DATA; Schema: sisat; Owner: suite
--



--
-- TOC entry 4871 (class 0 OID 102856)
-- Dependencies: 464
-- Data for Name: tsisatrepositorios; Type: TABLE DATA; Schema: sisat; Owner: suite
--



--
-- TOC entry 4872 (class 0 OID 102859)
-- Dependencies: 465
-- Data for Name: tsisatsgbd; Type: TABLE DATA; Schema: sisat; Owner: suite
--



--
-- TOC entry 4873 (class 0 OID 102862)
-- Dependencies: 466
-- Data for Name: tsisatso; Type: TABLE DATA; Schema: sisat; Owner: suite
--



--
-- TOC entry 4874 (class 0 OID 102865)
-- Dependencies: 467
-- Data for Name: tsisatvacantes; Type: TABLE DATA; Schema: sisat; Owner: suite
--

INSERT INTO sisat.tsisatvacantes (cod_vacante, des_rqvacante, cnu_anexperiencia, txt_experiencia, des_escolaridad, txt_herramientas, txt_habilidades, des_lugartrabajo, imp_sueldo, cod_idioma, sexo, fec_solicitud, fec_entrega, cod_cliente, aud_cod_creadopor, aud_cod_modificadopor, aud_fec_creacion, aud_fec_modificacion, cod_contratacion, statusvacante, txt_conocimientostecno, cod_nivelidioma) VALUES (2, 'DESARROLLADOR DBA', 2, 'TRABAJO EN EQUIPO', 'ING. EN COMPUTACIÓN', NULL, NULL, NULL, NULL, 1, 'INDISTINTO', '2019-11-21', '2019-11-21', 3, 10, NULL, '2019-11-14', NULL, 2, NULL, 'SEGURIDAD Y MANTENIMIENTO DE UNA B.D', 'ALTO');
INSERT INTO sisat.tsisatvacantes (cod_vacante, des_rqvacante, cnu_anexperiencia, txt_experiencia, des_escolaridad, txt_herramientas, txt_habilidades, des_lugartrabajo, imp_sueldo, cod_idioma, sexo, fec_solicitud, fec_entrega, cod_cliente, aud_cod_creadopor, aud_cod_modificadopor, aud_fec_creacion, aud_fec_modificacion, cod_contratacion, statusvacante, txt_conocimientostecno, cod_nivelidioma) VALUES (1, 'DESARROLLADOR JAVA FULL STACK', 2, 'COMUNICACIÓN FLUIDA', 'ING. SITEMAS', NULL, NULL, NULL, NULL, 1, 'INDISTINTO', '2019-11-14', '2019-11-14', 3, 10, NULL, '2019-11-14', NULL, 1, NULL, 'TECNOLOGÍAS HTML5, JAVASCRIPT Y CSS
', 'MEDIO');
INSERT INTO sisat.tsisatvacantes (cod_vacante, des_rqvacante, cnu_anexperiencia, txt_experiencia, des_escolaridad, txt_herramientas, txt_habilidades, des_lugartrabajo, imp_sueldo, cod_idioma, sexo, fec_solicitud, fec_entrega, cod_cliente, aud_cod_creadopor, aud_cod_modificadopor, aud_fec_creacion, aud_fec_modificacion, cod_contratacion, statusvacante, txt_conocimientostecno, cod_nivelidioma) VALUES (3, 'DESARROLLADOR BACK-END', NULL, 'DESARROLLADOR DE APLICACIONES WEB ', 'ING. SITEMAS', NULL, NULL, NULL, NULL, 1, 'INDISTINTO', '2019-11-15', '2019-11-20', 4, 10, NULL, '2019-11-15', NULL, 10, NULL, 'INTERACCIÓN CON B.D, CONOCIMIENTO EN SQL SERVER, POSTGRESQL Y JAVA ', 'MEDIO');


--
-- TOC entry 5195 (class 0 OID 0)
-- Dependencies: 219
-- Name: seq_sistema; Type: SEQUENCE SET; Schema: sgco; Owner: suite
--

SELECT pg_catalog.setval('sgco.seq_sistema', 1, false);


--
-- TOC entry 5196 (class 0 OID 0)
-- Dependencies: 220
-- Name: seq_tipousuario; Type: SEQUENCE SET; Schema: sgco; Owner: suite
--

SELECT pg_catalog.setval('sgco.seq_tipousuario', 1, false);


--
-- TOC entry 5197 (class 0 OID 0)
-- Dependencies: 221
-- Name: seq_usuarios; Type: SEQUENCE SET; Schema: sgco; Owner: suite
--

SELECT pg_catalog.setval('sgco.seq_usuarios', 1, false);


--
-- TOC entry 5198 (class 0 OID 0)
-- Dependencies: 367
-- Name: seq_cabecera; Type: SEQUENCE SET; Schema: sgnom; Owner: suite
--

SELECT pg_catalog.setval('sgnom.seq_cabecera', 42, true);


--
-- TOC entry 5199 (class 0 OID 0)
-- Dependencies: 368
-- Name: seq_cncptoquinc; Type: SEQUENCE SET; Schema: sgnom; Owner: suite
--

SELECT pg_catalog.setval('sgnom.seq_cncptoquinc', 132, true);


--
-- TOC entry 5200 (class 0 OID 0)
-- Dependencies: 369
-- Name: seq_confpago; Type: SEQUENCE SET; Schema: sgnom; Owner: suite
--

SELECT pg_catalog.setval('sgnom.seq_confpago', 159, true);


--
-- TOC entry 5201 (class 0 OID 0)
-- Dependencies: 370
-- Name: seq_empquincena; Type: SEQUENCE SET; Schema: sgnom; Owner: suite
--

SELECT pg_catalog.setval('sgnom.seq_empquincena', 251, true);


--
-- TOC entry 5202 (class 0 OID 0)
-- Dependencies: 371
-- Name: seq_incidencia; Type: SEQUENCE SET; Schema: sgnom; Owner: suite
--

SELECT pg_catalog.setval('sgnom.seq_incidencia', 80, true);


--
-- TOC entry 5203 (class 0 OID 0)
-- Dependencies: 372
-- Name: seq_nom_imss; Type: SEQUENCE SET; Schema: sgnom; Owner: suite
--

SELECT pg_catalog.setval('sgnom.seq_nom_imss', 241, true);


--
-- TOC entry 5204 (class 0 OID 0)
-- Dependencies: 225
-- Name: seq_area; Type: SEQUENCE SET; Schema: sgrh; Owner: suite
--

SELECT pg_catalog.setval('sgrh.seq_area', 5, true);


--
-- TOC entry 5205 (class 0 OID 0)
-- Dependencies: 226
-- Name: seq_asignacion_encuesta; Type: SEQUENCE SET; Schema: sgrh; Owner: suite
--

SELECT pg_catalog.setval('sgrh.seq_asignacion_encuesta', 10, true);


--
-- TOC entry 5206 (class 0 OID 0)
-- Dependencies: 227
-- Name: seq_asignacionesemp; Type: SEQUENCE SET; Schema: sgrh; Owner: suite
--

SELECT pg_catalog.setval('sgrh.seq_asignacionesemp', 1, false);


--
-- TOC entry 5207 (class 0 OID 0)
-- Dependencies: 228
-- Name: seq_capacitaciones; Type: SEQUENCE SET; Schema: sgrh; Owner: suite
--

SELECT pg_catalog.setval('sgrh.seq_capacitaciones', 1, false);


--
-- TOC entry 5208 (class 0 OID 0)
-- Dependencies: 229
-- Name: seq_cartaasignacion; Type: SEQUENCE SET; Schema: sgrh; Owner: suite
--

SELECT pg_catalog.setval('sgrh.seq_cartaasignacion', 1, false);


--
-- TOC entry 5209 (class 0 OID 0)
-- Dependencies: 230
-- Name: seq_cat_encuesta_participantes; Type: SEQUENCE SET; Schema: sgrh; Owner: suite
--

SELECT pg_catalog.setval('sgrh.seq_cat_encuesta_participantes', 138, true);


--
-- TOC entry 5210 (class 0 OID 0)
-- Dependencies: 231
-- Name: seq_catrespuestas; Type: SEQUENCE SET; Schema: sgrh; Owner: suite
--

SELECT pg_catalog.setval('sgrh.seq_catrespuestas', 200, false);


--
-- TOC entry 5211 (class 0 OID 0)
-- Dependencies: 232
-- Name: seq_clientes; Type: SEQUENCE SET; Schema: sgrh; Owner: suite
--

SELECT pg_catalog.setval('sgrh.seq_clientes', 1, false);


--
-- TOC entry 5212 (class 0 OID 0)
-- Dependencies: 233
-- Name: seq_contrataciones; Type: SEQUENCE SET; Schema: sgrh; Owner: suite
--

SELECT pg_catalog.setval('sgrh.seq_contrataciones', 1, false);


--
-- TOC entry 5213 (class 0 OID 0)
-- Dependencies: 234
-- Name: seq_contratos; Type: SEQUENCE SET; Schema: sgrh; Owner: suite
--

SELECT pg_catalog.setval('sgrh.seq_contratos', 1, false);


--
-- TOC entry 5214 (class 0 OID 0)
-- Dependencies: 235
-- Name: seq_empleado; Type: SEQUENCE SET; Schema: sgrh; Owner: suite
--

SELECT pg_catalog.setval('sgrh.seq_empleado', 13, true);


--
-- TOC entry 5215 (class 0 OID 0)
-- Dependencies: 236
-- Name: seq_encuestas; Type: SEQUENCE SET; Schema: sgrh; Owner: suite
--

SELECT pg_catalog.setval('sgrh.seq_encuestas', 19, true);


--
-- TOC entry 5216 (class 0 OID 0)
-- Dependencies: 237
-- Name: seq_escolaridad; Type: SEQUENCE SET; Schema: sgrh; Owner: suite
--

SELECT pg_catalog.setval('sgrh.seq_escolaridad', 1, false);


--
-- TOC entry 5217 (class 0 OID 0)
-- Dependencies: 238
-- Name: seq_estatus; Type: SEQUENCE SET; Schema: sgrh; Owner: suite
--

SELECT pg_catalog.setval('sgrh.seq_estatus', 1, true);


--
-- TOC entry 5218 (class 0 OID 0)
-- Dependencies: 239
-- Name: seq_evacapacitacion; Type: SEQUENCE SET; Schema: sgrh; Owner: suite
--

SELECT pg_catalog.setval('sgrh.seq_evacapacitacion', 1, false);


--
-- TOC entry 5219 (class 0 OID 0)
-- Dependencies: 240
-- Name: seq_evacontestadas; Type: SEQUENCE SET; Schema: sgrh; Owner: suite
--

SELECT pg_catalog.setval('sgrh.seq_evacontestadas', 1, false);


--
-- TOC entry 5220 (class 0 OID 0)
-- Dependencies: 241
-- Name: seq_evaluaciones; Type: SEQUENCE SET; Schema: sgrh; Owner: suite
--

SELECT pg_catalog.setval('sgrh.seq_evaluaciones', 1, false);


--
-- TOC entry 5221 (class 0 OID 0)
-- Dependencies: 242
-- Name: seq_experiencialab; Type: SEQUENCE SET; Schema: sgrh; Owner: suite
--

SELECT pg_catalog.setval('sgrh.seq_experiencialab', 1, false);


--
-- TOC entry 5222 (class 0 OID 0)
-- Dependencies: 243
-- Name: seq_factoreseva; Type: SEQUENCE SET; Schema: sgrh; Owner: suite
--

SELECT pg_catalog.setval('sgrh.seq_factoreseva', 1, false);


--
-- TOC entry 5223 (class 0 OID 0)
-- Dependencies: 244
-- Name: seq_idiomas; Type: SEQUENCE SET; Schema: sgrh; Owner: suite
--

SELECT pg_catalog.setval('sgrh.seq_idiomas', 1, false);


--
-- TOC entry 5224 (class 0 OID 0)
-- Dependencies: 245
-- Name: seq_logistica; Type: SEQUENCE SET; Schema: sgrh; Owner: suite
--

SELECT pg_catalog.setval('sgrh.seq_logistica', 2, true);


--
-- TOC entry 5225 (class 0 OID 0)
-- Dependencies: 246
-- Name: seq_lugar; Type: SEQUENCE SET; Schema: sgrh; Owner: suite
--

SELECT pg_catalog.setval('sgrh.seq_lugar', 1, false);


--
-- TOC entry 5226 (class 0 OID 0)
-- Dependencies: 247
-- Name: seq_modo; Type: SEQUENCE SET; Schema: sgrh; Owner: suite
--

SELECT pg_catalog.setval('sgrh.seq_modo', 4, true);


--
-- TOC entry 5227 (class 0 OID 0)
-- Dependencies: 248
-- Name: seq_perfiles; Type: SEQUENCE SET; Schema: sgrh; Owner: suite
--

SELECT pg_catalog.setval('sgrh.seq_perfiles', 1, false);


--
-- TOC entry 5228 (class 0 OID 0)
-- Dependencies: 249
-- Name: seq_plancapacitacion; Type: SEQUENCE SET; Schema: sgrh; Owner: suite
--

SELECT pg_catalog.setval('sgrh.seq_plancapacitacion', 3, true);


--
-- TOC entry 5229 (class 0 OID 0)
-- Dependencies: 250
-- Name: seq_planesoperativos; Type: SEQUENCE SET; Schema: sgrh; Owner: suite
--

SELECT pg_catalog.setval('sgrh.seq_planesoperativos', 1, false);


--
-- TOC entry 5230 (class 0 OID 0)
-- Dependencies: 251
-- Name: seq_preguntasenc; Type: SEQUENCE SET; Schema: sgrh; Owner: suite
--

SELECT pg_catalog.setval('sgrh.seq_preguntasenc', 209, true);


--
-- TOC entry 5231 (class 0 OID 0)
-- Dependencies: 252
-- Name: seq_preguntaseva; Type: SEQUENCE SET; Schema: sgrh; Owner: suite
--

SELECT pg_catalog.setval('sgrh.seq_preguntaseva', 1, false);


--
-- TOC entry 5232 (class 0 OID 0)
-- Dependencies: 253
-- Name: seq_proceso; Type: SEQUENCE SET; Schema: sgrh; Owner: suite
--

SELECT pg_catalog.setval('sgrh.seq_proceso', 2, true);


--
-- TOC entry 5233 (class 0 OID 0)
-- Dependencies: 254
-- Name: seq_proveedor; Type: SEQUENCE SET; Schema: sgrh; Owner: suite
--

SELECT pg_catalog.setval('sgrh.seq_proveedor', 2, true);


--
-- TOC entry 5234 (class 0 OID 0)
-- Dependencies: 255
-- Name: seq_puestos; Type: SEQUENCE SET; Schema: sgrh; Owner: suite
--

SELECT pg_catalog.setval('sgrh.seq_puestos', 5, true);


--
-- TOC entry 5235 (class 0 OID 0)
-- Dependencies: 256
-- Name: seq_respuestasenc; Type: SEQUENCE SET; Schema: sgrh; Owner: suite
--

SELECT pg_catalog.setval('sgrh.seq_respuestasenc', 500, false);


--
-- TOC entry 5236 (class 0 OID 0)
-- Dependencies: 257
-- Name: seq_respuestaseva; Type: SEQUENCE SET; Schema: sgrh; Owner: suite
--

SELECT pg_catalog.setval('sgrh.seq_respuestaseva', 1, false);


--
-- TOC entry 5237 (class 0 OID 0)
-- Dependencies: 258
-- Name: seq_revplanesoperativos; Type: SEQUENCE SET; Schema: sgrh; Owner: suite
--

SELECT pg_catalog.setval('sgrh.seq_revplanesoperativos', 1, false);


--
-- TOC entry 5238 (class 0 OID 0)
-- Dependencies: 259
-- Name: seq_rolempleado; Type: SEQUENCE SET; Schema: sgrh; Owner: suite
--

SELECT pg_catalog.setval('sgrh.seq_rolempleado', 3, true);


--
-- TOC entry 5239 (class 0 OID 0)
-- Dependencies: 260
-- Name: seq_roles; Type: SEQUENCE SET; Schema: sgrh; Owner: suite
--

SELECT pg_catalog.setval('sgrh.seq_roles', 1, false);


--
-- TOC entry 5240 (class 0 OID 0)
-- Dependencies: 261
-- Name: seq_subfactoreseva; Type: SEQUENCE SET; Schema: sgrh; Owner: suite
--

SELECT pg_catalog.setval('sgrh.seq_subfactoreseva', 1, false);


--
-- TOC entry 5241 (class 0 OID 0)
-- Dependencies: 262
-- Name: seq_tipocapacitacion; Type: SEQUENCE SET; Schema: sgrh; Owner: suite
--

SELECT pg_catalog.setval('sgrh.seq_tipocapacitacion', 2, true);


--
-- TOC entry 5242 (class 0 OID 0)
-- Dependencies: 263
-- Name: seq_tiposcapacitaciones; Type: SEQUENCE SET; Schema: sgrh; Owner: suite
--

SELECT pg_catalog.setval('sgrh.seq_tiposcapacitaciones', 14, true);


--
-- TOC entry 5243 (class 0 OID 0)
-- Dependencies: 264
-- Name: seq_validaevaluaciones; Type: SEQUENCE SET; Schema: sgrh; Owner: suite
--

SELECT pg_catalog.setval('sgrh.seq_validaevaluaciones', 1, false);


--
-- TOC entry 5244 (class 0 OID 0)
-- Dependencies: 304
-- Name: seq_agenda; Type: SEQUENCE SET; Schema: sgrt; Owner: suite
--

SELECT pg_catalog.setval('sgrt.seq_agenda', 1, false);


--
-- TOC entry 5245 (class 0 OID 0)
-- Dependencies: 305
-- Name: seq_archivo; Type: SEQUENCE SET; Schema: sgrt; Owner: suite
--

SELECT pg_catalog.setval('sgrt.seq_archivo', 1, false);


--
-- TOC entry 5246 (class 0 OID 0)
-- Dependencies: 306
-- Name: seq_asistente; Type: SEQUENCE SET; Schema: sgrt; Owner: suite
--

SELECT pg_catalog.setval('sgrt.seq_asistente', 1, false);


--
-- TOC entry 5247 (class 0 OID 0)
-- Dependencies: 307
-- Name: seq_attach; Type: SEQUENCE SET; Schema: sgrt; Owner: suite
--

SELECT pg_catalog.setval('sgrt.seq_attach', 1, false);


--
-- TOC entry 5248 (class 0 OID 0)
-- Dependencies: 308
-- Name: seq_categoriafaq; Type: SEQUENCE SET; Schema: sgrt; Owner: suite
--

SELECT pg_catalog.setval('sgrt.seq_categoriafaq', 1, false);


--
-- TOC entry 5249 (class 0 OID 0)
-- Dependencies: 309
-- Name: seq_chat; Type: SEQUENCE SET; Schema: sgrt; Owner: suite
--

SELECT pg_catalog.setval('sgrt.seq_chat', 1, false);


--
-- TOC entry 5250 (class 0 OID 0)
-- Dependencies: 310
-- Name: seq_ciudad; Type: SEQUENCE SET; Schema: sgrt; Owner: suite
--

SELECT pg_catalog.setval('sgrt.seq_ciudad', 11, true);


--
-- TOC entry 5251 (class 0 OID 0)
-- Dependencies: 311
-- Name: seq_comentsagenda; Type: SEQUENCE SET; Schema: sgrt; Owner: suite
--

SELECT pg_catalog.setval('sgrt.seq_comentsagenda', 1, false);


--
-- TOC entry 5252 (class 0 OID 0)
-- Dependencies: 312
-- Name: seq_comentsreunion; Type: SEQUENCE SET; Schema: sgrt; Owner: suite
--

SELECT pg_catalog.setval('sgrt.seq_comentsreunion', 1, false);


--
-- TOC entry 5253 (class 0 OID 0)
-- Dependencies: 313
-- Name: seq_compromiso; Type: SEQUENCE SET; Schema: sgrt; Owner: suite
--

SELECT pg_catalog.setval('sgrt.seq_compromiso', 1, false);


--
-- TOC entry 5254 (class 0 OID 0)
-- Dependencies: 314
-- Name: seq_contacto; Type: SEQUENCE SET; Schema: sgrt; Owner: suite
--

SELECT pg_catalog.setval('sgrt.seq_contacto', 1, false);


--
-- TOC entry 5255 (class 0 OID 0)
-- Dependencies: 315
-- Name: seq_correo; Type: SEQUENCE SET; Schema: sgrt; Owner: suite
--

SELECT pg_catalog.setval('sgrt.seq_correo', 1, false);


--
-- TOC entry 5256 (class 0 OID 0)
-- Dependencies: 316
-- Name: seq_depto; Type: SEQUENCE SET; Schema: sgrt; Owner: suite
--

SELECT pg_catalog.setval('sgrt.seq_depto', 1, false);


--
-- TOC entry 5257 (class 0 OID 0)
-- Dependencies: 317
-- Name: seq_edoacuerdo; Type: SEQUENCE SET; Schema: sgrt; Owner: suite
--

SELECT pg_catalog.setval('sgrt.seq_edoacuerdo', 1, false);


--
-- TOC entry 5258 (class 0 OID 0)
-- Dependencies: 318
-- Name: seq_elemento; Type: SEQUENCE SET; Schema: sgrt; Owner: suite
--

SELECT pg_catalog.setval('sgrt.seq_elemento', 1, false);


--
-- TOC entry 5259 (class 0 OID 0)
-- Dependencies: 319
-- Name: seq_estadorep; Type: SEQUENCE SET; Schema: sgrt; Owner: suite
--

SELECT pg_catalog.setval('sgrt.seq_estadorep', 4, true);


--
-- TOC entry 5260 (class 0 OID 0)
-- Dependencies: 320
-- Name: seq_faq; Type: SEQUENCE SET; Schema: sgrt; Owner: suite
--

SELECT pg_catalog.setval('sgrt.seq_faq', 1, false);


--
-- TOC entry 5261 (class 0 OID 0)
-- Dependencies: 321
-- Name: seq_grupo; Type: SEQUENCE SET; Schema: sgrt; Owner: suite
--

SELECT pg_catalog.setval('sgrt.seq_grupo', 1, false);


--
-- TOC entry 5262 (class 0 OID 0)
-- Dependencies: 322
-- Name: seq_invitado; Type: SEQUENCE SET; Schema: sgrt; Owner: suite
--

SELECT pg_catalog.setval('sgrt.seq_invitado', 1, false);


--
-- TOC entry 5263 (class 0 OID 0)
-- Dependencies: 323
-- Name: seq_lugar; Type: SEQUENCE SET; Schema: sgrt; Owner: suite
--

SELECT pg_catalog.setval('sgrt.seq_lugar', 4, true);


--
-- TOC entry 5264 (class 0 OID 0)
-- Dependencies: 324
-- Name: seq_mensaje; Type: SEQUENCE SET; Schema: sgrt; Owner: suite
--

SELECT pg_catalog.setval('sgrt.seq_mensaje', 1, false);


--
-- TOC entry 5265 (class 0 OID 0)
-- Dependencies: 325
-- Name: seq_nota; Type: SEQUENCE SET; Schema: sgrt; Owner: suite
--

SELECT pg_catalog.setval('sgrt.seq_nota', 1, false);


--
-- TOC entry 5266 (class 0 OID 0)
-- Dependencies: 326
-- Name: seq_plantillacorreo; Type: SEQUENCE SET; Schema: sgrt; Owner: suite
--

SELECT pg_catalog.setval('sgrt.seq_plantillacorreo', 1, false);


--
-- TOC entry 5267 (class 0 OID 0)
-- Dependencies: 327
-- Name: seq_prioridad; Type: SEQUENCE SET; Schema: sgrt; Owner: suite
--

SELECT pg_catalog.setval('sgrt.seq_prioridad', 1, false);


--
-- TOC entry 5268 (class 0 OID 0)
-- Dependencies: 328
-- Name: seq_resp; Type: SEQUENCE SET; Schema: sgrt; Owner: suite
--

SELECT pg_catalog.setval('sgrt.seq_resp', 1, false);


--
-- TOC entry 5269 (class 0 OID 0)
-- Dependencies: 329
-- Name: seq_respuesta; Type: SEQUENCE SET; Schema: sgrt; Owner: suite
--

SELECT pg_catalog.setval('sgrt.seq_respuesta', 1, false);


--
-- TOC entry 5270 (class 0 OID 0)
-- Dependencies: 330
-- Name: seq_respuestas_participantes; Type: SEQUENCE SET; Schema: sgrt; Owner: suite
--

SELECT pg_catalog.setval('sgrt.seq_respuestas_participantes', 1, false);


--
-- TOC entry 5271 (class 0 OID 0)
-- Dependencies: 331
-- Name: seq_reunion; Type: SEQUENCE SET; Schema: sgrt; Owner: suite
--

SELECT pg_catalog.setval('sgrt.seq_reunion', 1, false);


--
-- TOC entry 5272 (class 0 OID 0)
-- Dependencies: 332
-- Name: seq_servicio; Type: SEQUENCE SET; Schema: sgrt; Owner: suite
--

SELECT pg_catalog.setval('sgrt.seq_servicio', 1, false);


--
-- TOC entry 5273 (class 0 OID 0)
-- Dependencies: 333
-- Name: seq_solicitud; Type: SEQUENCE SET; Schema: sgrt; Owner: suite
--

SELECT pg_catalog.setval('sgrt.seq_solicitud', 1, false);


--
-- TOC entry 5274 (class 0 OID 0)
-- Dependencies: 334
-- Name: seq_ticket; Type: SEQUENCE SET; Schema: sgrt; Owner: suite
--

SELECT pg_catalog.setval('sgrt.seq_ticket', 1, false);


--
-- TOC entry 5275 (class 0 OID 0)
-- Dependencies: 335
-- Name: seq_topico; Type: SEQUENCE SET; Schema: sgrt; Owner: suite
--

SELECT pg_catalog.setval('sgrt.seq_topico', 1, false);


--
-- TOC entry 5276 (class 0 OID 0)
-- Dependencies: 405
-- Name: seq_aceptaciones; Type: SEQUENCE SET; Schema: sisat; Owner: suite
--

SELECT pg_catalog.setval('sisat.seq_aceptaciones', 1, false);


--
-- TOC entry 5277 (class 0 OID 0)
-- Dependencies: 406
-- Name: seq_asignaciones; Type: SEQUENCE SET; Schema: sisat; Owner: suite
--

SELECT pg_catalog.setval('sisat.seq_asignaciones', 1, false);


--
-- TOC entry 5278 (class 0 OID 0)
-- Dependencies: 407
-- Name: seq_candidatos; Type: SEQUENCE SET; Schema: sisat; Owner: suite
--

SELECT pg_catalog.setval('sisat.seq_candidatos', 1, false);


--
-- TOC entry 5279 (class 0 OID 0)
-- Dependencies: 408
-- Name: seq_cartaasignaciones; Type: SEQUENCE SET; Schema: sisat; Owner: suite
--

SELECT pg_catalog.setval('sisat.seq_cartaasignaciones', 11, true);


--
-- TOC entry 5280 (class 0 OID 0)
-- Dependencies: 409
-- Name: seq_comentcartaasignacion; Type: SEQUENCE SET; Schema: sisat; Owner: suite
--

SELECT pg_catalog.setval('sisat.seq_comentcartaasignacion', 4, true);


--
-- TOC entry 5281 (class 0 OID 0)
-- Dependencies: 410
-- Name: seq_comentcosteo; Type: SEQUENCE SET; Schema: sisat; Owner: suite
--

SELECT pg_catalog.setval('sisat.seq_comentcosteo', 1, false);


--
-- TOC entry 5282 (class 0 OID 0)
-- Dependencies: 411
-- Name: seq_comententrevista; Type: SEQUENCE SET; Schema: sisat; Owner: suite
--

SELECT pg_catalog.setval('sisat.seq_comententrevista', 1, false);


--
-- TOC entry 5283 (class 0 OID 0)
-- Dependencies: 412
-- Name: seq_comentvacantes; Type: SEQUENCE SET; Schema: sisat; Owner: suite
--

SELECT pg_catalog.setval('sisat.seq_comentvacantes', 3, true);


--
-- TOC entry 5284 (class 0 OID 0)
-- Dependencies: 413
-- Name: seq_contrataciones; Type: SEQUENCE SET; Schema: sisat; Owner: suite
--

SELECT pg_catalog.setval('sisat.seq_contrataciones', 12, true);


--
-- TOC entry 5285 (class 0 OID 0)
-- Dependencies: 414
-- Name: seq_cotizaciones; Type: SEQUENCE SET; Schema: sisat; Owner: suite
--

SELECT pg_catalog.setval('sisat.seq_cotizaciones', 1, false);


--
-- TOC entry 5286 (class 0 OID 0)
-- Dependencies: 415
-- Name: seq_cursos; Type: SEQUENCE SET; Schema: sisat; Owner: suite
--

SELECT pg_catalog.setval('sisat.seq_cursos', 1, false);


--
-- TOC entry 5287 (class 0 OID 0)
-- Dependencies: 416
-- Name: seq_entrevistas; Type: SEQUENCE SET; Schema: sisat; Owner: suite
--

SELECT pg_catalog.setval('sisat.seq_entrevistas', 1, false);


--
-- TOC entry 5288 (class 0 OID 0)
-- Dependencies: 417
-- Name: seq_envios; Type: SEQUENCE SET; Schema: sisat; Owner: suite
--

SELECT pg_catalog.setval('sisat.seq_envios', 1, false);


--
-- TOC entry 5289 (class 0 OID 0)
-- Dependencies: 418
-- Name: seq_escolaridad; Type: SEQUENCE SET; Schema: sisat; Owner: suite
--

SELECT pg_catalog.setval('sisat.seq_escolaridad', 1, false);


--
-- TOC entry 5290 (class 0 OID 0)
-- Dependencies: 419
-- Name: seq_experiencias; Type: SEQUENCE SET; Schema: sisat; Owner: suite
--

SELECT pg_catalog.setval('sisat.seq_experiencias', 1, false);


--
-- TOC entry 5291 (class 0 OID 0)
-- Dependencies: 420
-- Name: seq_firmareqper; Type: SEQUENCE SET; Schema: sisat; Owner: suite
--

SELECT pg_catalog.setval('sisat.seq_firmareqper', 1, false);


--
-- TOC entry 5292 (class 0 OID 0)
-- Dependencies: 421
-- Name: seq_firmas; Type: SEQUENCE SET; Schema: sisat; Owner: suite
--

SELECT pg_catalog.setval('sisat.seq_firmas', 5, true);


--
-- TOC entry 5293 (class 0 OID 0)
-- Dependencies: 422
-- Name: seq_habilidades; Type: SEQUENCE SET; Schema: sisat; Owner: suite
--

SELECT pg_catalog.setval('sisat.seq_habilidades', 1, false);


--
-- TOC entry 5294 (class 0 OID 0)
-- Dependencies: 423
-- Name: seq_idiomas; Type: SEQUENCE SET; Schema: sisat; Owner: suite
--

SELECT pg_catalog.setval('sisat.seq_idiomas', 1, false);


--
-- TOC entry 5295 (class 0 OID 0)
-- Dependencies: 424
-- Name: seq_ordenservicios; Type: SEQUENCE SET; Schema: sisat; Owner: suite
--

SELECT pg_catalog.setval('sisat.seq_ordenservicios', 54, true);


--
-- TOC entry 5296 (class 0 OID 0)
-- Dependencies: 425
-- Name: seq_prospectos; Type: SEQUENCE SET; Schema: sisat; Owner: suite
--

SELECT pg_catalog.setval('sisat.seq_prospectos', 1, false);


--
-- TOC entry 5297 (class 0 OID 0)
-- Dependencies: 426
-- Name: seq_proyectos; Type: SEQUENCE SET; Schema: sisat; Owner: suite
--

SELECT pg_catalog.setval('sisat.seq_proyectos', 1, false);


--
-- TOC entry 5298 (class 0 OID 0)
-- Dependencies: 427
-- Name: seq_vacantes; Type: SEQUENCE SET; Schema: sisat; Owner: suite
--

SELECT pg_catalog.setval('sisat.seq_vacantes', 5, true);


--
-- TOC entry 5299 (class 0 OID 0)
-- Dependencies: 449
-- Name: tsisatherramientas_cod_herramientas_seq; Type: SEQUENCE SET; Schema: sisat; Owner: suite
--

SELECT pg_catalog.setval('sisat.tsisatherramientas_cod_herramientas_seq', 1, false);


--
-- TOC entry 3848 (class 2606 OID 100096)
-- Name: tsgcosistemas cod_agenda; Type: CONSTRAINT; Schema: sgco; Owner: suite
--

ALTER TABLE ONLY sgco.tsgcosistemas
    ADD CONSTRAINT cod_agenda UNIQUE (cod_sistema);


--
-- TOC entry 3852 (class 2606 OID 100098)
-- Name: tsgcotipousuario cod_archivo; Type: CONSTRAINT; Schema: sgco; Owner: suite
--

ALTER TABLE ONLY sgco.tsgcotipousuario
    ADD CONSTRAINT cod_archivo UNIQUE (cod_tipousuario);


--
-- TOC entry 3856 (class 2606 OID 100100)
-- Name: tsgcousuarios cod_asistente; Type: CONSTRAINT; Schema: sgco; Owner: suite
--

ALTER TABLE ONLY sgco.tsgcousuarios
    ADD CONSTRAINT cod_asistente UNIQUE (cod_usuario);


--
-- TOC entry 3850 (class 2606 OID 100102)
-- Name: tsgcosistemas tsgcosistemas_pkey; Type: CONSTRAINT; Schema: sgco; Owner: suite
--

ALTER TABLE ONLY sgco.tsgcosistemas
    ADD CONSTRAINT tsgcosistemas_pkey PRIMARY KEY (cod_sistema);


--
-- TOC entry 3854 (class 2606 OID 100104)
-- Name: tsgcotipousuario tsgcotipousuario_pkey; Type: CONSTRAINT; Schema: sgco; Owner: suite
--

ALTER TABLE ONLY sgco.tsgcotipousuario
    ADD CONSTRAINT tsgcotipousuario_pkey PRIMARY KEY (cod_tipousuario);


--
-- TOC entry 3858 (class 2606 OID 100106)
-- Name: tsgcousuarios tsgcousuarios_pkey; Type: CONSTRAINT; Schema: sgco; Owner: suite
--

ALTER TABLE ONLY sgco.tsgcousuarios
    ADD CONSTRAINT tsgcousuarios_pkey PRIMARY KEY (cod_usuario);


--
-- TOC entry 4102 (class 2606 OID 102112)
-- Name: tsgnomcatincidencia cat_incidencia_id; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomcatincidencia
    ADD CONSTRAINT cat_incidencia_id PRIMARY KEY (cod_catincidenciaid);


--
-- TOC entry 4118 (class 2606 OID 102636)
-- Name: tsgnomempleados empleadoRH; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomempleados
    ADD CONSTRAINT "empleadoRH" UNIQUE (cod_empleado_fk);


--
-- TOC entry 4090 (class 2606 OID 102114)
-- Name: tsgnomaguinaldo nom_aguinaldo_id; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomaguinaldo
    ADD CONSTRAINT nom_aguinaldo_id PRIMARY KEY (cod_aguinaldoid);


--
-- TOC entry 4094 (class 2606 OID 102116)
-- Name: tsgnombitacora nom_bitacora_id; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnombitacora
    ADD CONSTRAINT nom_bitacora_id PRIMARY KEY (cod_bitacoraid);


--
-- TOC entry 4098 (class 2606 OID 102118)
-- Name: tsgnomcabeceraht nom_cabecera_copia_id; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomcabeceraht
    ADD CONSTRAINT nom_cabecera_copia_id PRIMARY KEY (cod_cabeceraid);


--
-- TOC entry 4096 (class 2606 OID 102120)
-- Name: tsgnomcabecera nom_cabecera_id; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomcabecera
    ADD CONSTRAINT nom_cabecera_id PRIMARY KEY (cod_cabeceraid);


--
-- TOC entry 4092 (class 2606 OID 102122)
-- Name: tsgnomargumento nom_cat_argumento_id; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomargumento
    ADD CONSTRAINT nom_cat_argumento_id PRIMARY KEY (cod_argumentoid);


--
-- TOC entry 4110 (class 2606 OID 102124)
-- Name: tsgnomconcepto nom_cat_concepto_id; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomconcepto
    ADD CONSTRAINT nom_cat_concepto_id PRIMARY KEY (cod_conceptoid);


--
-- TOC entry 4112 (class 2606 OID 102126)
-- Name: tsgnomconceptosat nom_cat_concepto_sat_id; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomconceptosat
    ADD CONSTRAINT nom_cat_concepto_sat_id PRIMARY KEY (cod_conceptosatid);


--
-- TOC entry 4116 (class 2606 OID 102128)
-- Name: tsgnomejercicio nom_cat_ejercicio_id; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomejercicio
    ADD CONSTRAINT nom_cat_ejercicio_id PRIMARY KEY (cod_ejercicioid);


--
-- TOC entry 4126 (class 2606 OID 102130)
-- Name: tsgnomestatusnom nom_cat_estatus_nomina_id; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomestatusnom
    ADD CONSTRAINT nom_cat_estatus_nomina_id PRIMARY KEY (cod_estatusnomid);


--
-- TOC entry 4128 (class 2606 OID 102132)
-- Name: tsgnomformula nom_cat_formula_id; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomformula
    ADD CONSTRAINT nom_cat_formula_id PRIMARY KEY (cod_formulaid);


--
-- TOC entry 4130 (class 2606 OID 102134)
-- Name: tsgnomfuncion nom_cat_funcion_id; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomfuncion
    ADD CONSTRAINT nom_cat_funcion_id PRIMARY KEY (cod_funcionid);


--
-- TOC entry 4138 (class 2606 OID 102136)
-- Name: tsgnomquincena nom_cat_quincena_id; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomquincena
    ADD CONSTRAINT nom_cat_quincena_id PRIMARY KEY (cod_quincenaid);


--
-- TOC entry 4132 (class 2606 OID 102138)
-- Name: tsgnomhisttabla nom_cat_tabla_id; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomhisttabla
    ADD CONSTRAINT nom_cat_tabla_id PRIMARY KEY (cod_tablaid);


--
-- TOC entry 4100 (class 2606 OID 102140)
-- Name: tsgnomcalculo nom_cat_tipo_calculo_id; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomcalculo
    ADD CONSTRAINT nom_cat_tipo_calculo_id PRIMARY KEY (cod_calculoid);


--
-- TOC entry 4104 (class 2606 OID 102142)
-- Name: tsgnomclasificador nom_cat_tipo_clasificador_id; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomclasificador
    ADD CONSTRAINT nom_cat_tipo_clasificador_id PRIMARY KEY (cod_clasificadorid);


--
-- TOC entry 4140 (class 2606 OID 102144)
-- Name: tsgnomtipoconcepto nom_cat_tipo_conepto_id; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomtipoconcepto
    ADD CONSTRAINT nom_cat_tipo_conepto_id PRIMARY KEY (cod_tipoconceptoid);


--
-- TOC entry 4142 (class 2606 OID 102146)
-- Name: tsgnomtiponomina nom_cat_tipo_nomina_id; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomtiponomina
    ADD CONSTRAINT nom_cat_tipo_nomina_id PRIMARY KEY (cod_tiponominaid);


--
-- TOC entry 4108 (class 2606 OID 102148)
-- Name: tsgnomcncptoquincht nom_conceptos_quincena_copia_id; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomcncptoquincht
    ADD CONSTRAINT nom_conceptos_quincena_copia_id PRIMARY KEY (cod_cncptoquinchtid);


--
-- TOC entry 4106 (class 2606 OID 102150)
-- Name: tsgnomcncptoquinc nom_conceptos_quincena_id; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomcncptoquinc
    ADD CONSTRAINT nom_conceptos_quincena_id PRIMARY KEY (cod_cncptoquincid);


--
-- TOC entry 4114 (class 2606 OID 102152)
-- Name: tsgnomconfpago nom_conf_pago_pkey; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomconfpago
    ADD CONSTRAINT nom_conf_pago_pkey PRIMARY KEY (cod_confpagoid);


--
-- TOC entry 4120 (class 2606 OID 102154)
-- Name: tsgnomempleados nom_empleado_id; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomempleados
    ADD CONSTRAINT nom_empleado_id PRIMARY KEY (cod_empleadoid);


--
-- TOC entry 4124 (class 2606 OID 102156)
-- Name: tsgnomempquincenaht nom_empleado_quincena_copia_id; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomempquincenaht
    ADD CONSTRAINT nom_empleado_quincena_copia_id PRIMARY KEY (cod_empquincenahtid);


--
-- TOC entry 4122 (class 2606 OID 102158)
-- Name: tsgnomempquincena nom_empleado_quincena_id; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomempquincena
    ADD CONSTRAINT nom_empleado_quincena_id PRIMARY KEY (cod_empquincenaid);


--
-- TOC entry 4134 (class 2606 OID 102160)
-- Name: tsgnomincidencia nom_incidencia_id; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomincidencia
    ADD CONSTRAINT nom_incidencia_id PRIMARY KEY (cod_incidenciaid);


--
-- TOC entry 4136 (class 2606 OID 102162)
-- Name: tsgnommanterceros nom_manuales_terceros_pkey; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnommanterceros
    ADD CONSTRAINT nom_manuales_terceros_pkey PRIMARY KEY (cod_mantercerosid);


--
-- TOC entry 3872 (class 2606 OID 100162)
-- Name: tsgrhcatrespuestas catrespuestas_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhcatrespuestas
    ADD CONSTRAINT catrespuestas_pkey PRIMARY KEY (cod_catrespuesta);


--
-- TOC entry 3908 (class 2606 OID 100164)
-- Name: tsgrhmodo cod_capacitacion_pk; Type: CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhmodo
    ADD CONSTRAINT cod_capacitacion_pk PRIMARY KEY (cod_modo);


--
-- TOC entry 3890 (class 2606 OID 100166)
-- Name: tsgrhestatuscapacitacion cod_estatus_pk; Type: CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhestatuscapacitacion
    ADD CONSTRAINT cod_estatus_pk PRIMARY KEY (cod_estatus);


--
-- TOC entry 3920 (class 2606 OID 100168)
-- Name: tsgrhprocesos cod_procesos_pk; Type: CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhprocesos
    ADD CONSTRAINT cod_procesos_pk PRIMARY KEY (cod_proceso);


--
-- TOC entry 3922 (class 2606 OID 100170)
-- Name: tsgrhproveedores cod_proveedor_pk; Type: CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhproveedores
    ADD CONSTRAINT cod_proveedor_pk PRIMARY KEY (cod_proveedor);


--
-- TOC entry 3926 (class 2606 OID 100172)
-- Name: tsgrhrelacionroles cod_relacion_pk; Type: CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhrelacionroles
    ADD CONSTRAINT cod_relacion_pk PRIMARY KEY (cod_plancapacitacion, cod_rolempleado);


--
-- TOC entry 3936 (class 2606 OID 100174)
-- Name: tsgrhroles cod_rol_pk; Type: CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhroles
    ADD CONSTRAINT cod_rol_pk PRIMARY KEY (cod_rol);


--
-- TOC entry 3934 (class 2606 OID 100176)
-- Name: tsgrhrolempleado cod_rolempleado_pk; Type: CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhrolempleado
    ADD CONSTRAINT cod_rolempleado_pk PRIMARY KEY (cod_rolempleado);


--
-- TOC entry 3940 (class 2606 OID 100178)
-- Name: tsgrhtipocapacitacion cod_tipocapacitacion_pk; Type: CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhtipocapacitacion
    ADD CONSTRAINT cod_tipocapacitacion_pk PRIMARY KEY (cod_tipocapacitacion);


--
-- TOC entry 3866 (class 2606 OID 100180)
-- Name: tsgrhasignacionesemp pk_cod_asignacion_asignacionesempleados; Type: CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhasignacionesemp
    ADD CONSTRAINT pk_cod_asignacion_asignacionesempleados PRIMARY KEY (cod_asignacion);


--
-- TOC entry 3892 (class 2606 OID 100182)
-- Name: tsgrhevacapacitacion pk_cod_evaluacioncap_evacapacitacionesemp; Type: CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhevacapacitacion
    ADD CONSTRAINT pk_cod_evaluacioncap_evacapacitacionesemp PRIMARY KEY (cod_evacapacitacion);


--
-- TOC entry 3860 (class 2606 OID 100184)
-- Name: tsgrhareas tsgrhareas_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhareas
    ADD CONSTRAINT tsgrhareas_pkey PRIMARY KEY (cod_area);


--
-- TOC entry 3862 (class 2606 OID 100186)
-- Name: tsgrhasignacion_encuesta tsgrhasignacion_encuesta_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhasignacion_encuesta
    ADD CONSTRAINT tsgrhasignacion_encuesta_pkey PRIMARY KEY (cod_asignacion);


--
-- TOC entry 3864 (class 2606 OID 100188)
-- Name: tsgrhasignacion_encuesta tsgrhasignacion_encuesta_unique; Type: CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhasignacion_encuesta
    ADD CONSTRAINT tsgrhasignacion_encuesta_unique UNIQUE (cod_empleado, cod_encuesta);


--
-- TOC entry 3868 (class 2606 OID 100190)
-- Name: tsgrhcapacitaciones tsgrhcapacitaciones_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhcapacitaciones
    ADD CONSTRAINT tsgrhcapacitaciones_pkey PRIMARY KEY (cod_capacitacion);


--
-- TOC entry 3870 (class 2606 OID 100192)
-- Name: tsgrhcartaasignacion tsgrhcartaasignacion_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhcartaasignacion
    ADD CONSTRAINT tsgrhcartaasignacion_pkey PRIMARY KEY (cod_asignacion);


--
-- TOC entry 3874 (class 2606 OID 100194)
-- Name: tsgrhclientes tsgrhclientes_des_correocte_key; Type: CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhclientes
    ADD CONSTRAINT tsgrhclientes_des_correocte_key UNIQUE (des_correocte);


--
-- TOC entry 3876 (class 2606 OID 100196)
-- Name: tsgrhclientes tsgrhclientes_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhclientes
    ADD CONSTRAINT tsgrhclientes_pkey PRIMARY KEY (cod_cliente);


--
-- TOC entry 3878 (class 2606 OID 100198)
-- Name: tsgrhcontrataciones tsgrhcontrataciones_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhcontrataciones
    ADD CONSTRAINT tsgrhcontrataciones_pkey PRIMARY KEY (cod_contratacion);


--
-- TOC entry 3880 (class 2606 OID 100200)
-- Name: tsgrhcontratos tsgrhcontratos_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhcontratos
    ADD CONSTRAINT tsgrhcontratos_pkey PRIMARY KEY (cod_contrato);


--
-- TOC entry 3882 (class 2606 OID 100202)
-- Name: tsgrhempleados tsgrhempleados_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhempleados
    ADD CONSTRAINT tsgrhempleados_pkey PRIMARY KEY (cod_empleado);


--
-- TOC entry 3886 (class 2606 OID 100204)
-- Name: tsgrhencuesta_participantes tsgrhencuesta_participantes_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhencuesta_participantes
    ADD CONSTRAINT tsgrhencuesta_participantes_pkey PRIMARY KEY (cod_participantenc);


--
-- TOC entry 3884 (class 2606 OID 100206)
-- Name: tsgrhencuesta tsgrhencuesta_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhencuesta
    ADD CONSTRAINT tsgrhencuesta_pkey PRIMARY KEY (cod_encuesta);


--
-- TOC entry 3888 (class 2606 OID 100208)
-- Name: tsgrhescolaridad tsgrhescolaridad_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhescolaridad
    ADD CONSTRAINT tsgrhescolaridad_pkey PRIMARY KEY (cod_escolaridad);


--
-- TOC entry 3894 (class 2606 OID 100210)
-- Name: tsgrhevacontestadas tsgrhevacontestadas_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhevacontestadas
    ADD CONSTRAINT tsgrhevacontestadas_pkey PRIMARY KEY (cod_evacontestada);


--
-- TOC entry 3896 (class 2606 OID 100212)
-- Name: tsgrhevaluaciones tsgrhevaluaciones_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhevaluaciones
    ADD CONSTRAINT tsgrhevaluaciones_pkey PRIMARY KEY (cod_evaluacion);


--
-- TOC entry 3898 (class 2606 OID 100214)
-- Name: tsgrhexperienciaslaborales tsgrhexperienciaslaborales_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhexperienciaslaborales
    ADD CONSTRAINT tsgrhexperienciaslaborales_pkey PRIMARY KEY (cod_experiencia);


--
-- TOC entry 3900 (class 2606 OID 100216)
-- Name: tsgrhfactoreseva tsgrhfactoreseva_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhfactoreseva
    ADD CONSTRAINT tsgrhfactoreseva_pkey PRIMARY KEY (cod_factor);


--
-- TOC entry 3902 (class 2606 OID 100218)
-- Name: tsgrhidiomas tsgrhidiomas_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhidiomas
    ADD CONSTRAINT tsgrhidiomas_pkey PRIMARY KEY (cod_idioma);


--
-- TOC entry 3910 (class 2606 OID 100220)
-- Name: tsgrhperfiles tsgrhperfiles_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhperfiles
    ADD CONSTRAINT tsgrhperfiles_pkey PRIMARY KEY (cod_perfil);


--
-- TOC entry 3912 (class 2606 OID 100222)
-- Name: tsgrhplancapacitacion tsgrhplancapacitacion_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhplancapacitacion
    ADD CONSTRAINT tsgrhplancapacitacion_pkey PRIMARY KEY (cod_plancapacitacion);


--
-- TOC entry 3914 (class 2606 OID 100224)
-- Name: tsgrhplanoperativo tsgrhplanoperativo_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhplanoperativo
    ADD CONSTRAINT tsgrhplanoperativo_pkey PRIMARY KEY (cod_planoperativo);


--
-- TOC entry 3916 (class 2606 OID 100226)
-- Name: tsgrhpreguntasenc tsgrhpreguntasenc_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhpreguntasenc
    ADD CONSTRAINT tsgrhpreguntasenc_pkey PRIMARY KEY (cod_pregunta);


--
-- TOC entry 3918 (class 2606 OID 100228)
-- Name: tsgrhpreguntaseva tsgrhpreguntaseva_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhpreguntaseva
    ADD CONSTRAINT tsgrhpreguntaseva_pkey PRIMARY KEY (cod_pregunta);


--
-- TOC entry 3924 (class 2606 OID 100230)
-- Name: tsgrhpuestos tsgrhpuestos_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhpuestos
    ADD CONSTRAINT tsgrhpuestos_pkey PRIMARY KEY (cod_puesto);


--
-- TOC entry 3904 (class 2606 OID 100232)
-- Name: tsgrhlogistica tsgrhreglogistica_cod_capacitacion_key; Type: CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhlogistica
    ADD CONSTRAINT tsgrhreglogistica_cod_capacitacion_key UNIQUE (cod_plancapacitacion);


--
-- TOC entry 3906 (class 2606 OID 100234)
-- Name: tsgrhlogistica tsgrhreglogistica_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhlogistica
    ADD CONSTRAINT tsgrhreglogistica_pkey PRIMARY KEY (cod_logistica);


--
-- TOC entry 3928 (class 2606 OID 100236)
-- Name: tsgrhrespuestasenc tsgrhrespuestasenc_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhrespuestasenc
    ADD CONSTRAINT tsgrhrespuestasenc_pkey PRIMARY KEY (cod_respuesta);


--
-- TOC entry 3930 (class 2606 OID 100238)
-- Name: tsgrhrespuestaseva tsgrhrespuestaseva_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhrespuestaseva
    ADD CONSTRAINT tsgrhrespuestaseva_pkey PRIMARY KEY (cod_respuesta);


--
-- TOC entry 3932 (class 2606 OID 100240)
-- Name: tsgrhrevplanoperativo tsgrhrevplanoperativo_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhrevplanoperativo
    ADD CONSTRAINT tsgrhrevplanoperativo_pkey PRIMARY KEY (cod_revplanoperativo);


--
-- TOC entry 3938 (class 2606 OID 100242)
-- Name: tsgrhsubfactoreseva tsgrhsubfactoreseva_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhsubfactoreseva
    ADD CONSTRAINT tsgrhsubfactoreseva_pkey PRIMARY KEY (cod_subfactor);


--
-- TOC entry 3942 (class 2606 OID 100244)
-- Name: tsgrhvalidaevaluaciondes tsgrhvalidaevaluaciondes_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhvalidaevaluaciondes
    ADD CONSTRAINT tsgrhvalidaevaluaciondes_pkey PRIMARY KEY (cod_validacion);


--
-- TOC entry 3944 (class 2606 OID 100246)
-- Name: tsgrtagenda cod_agenda; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtagenda
    ADD CONSTRAINT cod_agenda UNIQUE (cod_agenda);


--
-- TOC entry 3948 (class 2606 OID 100248)
-- Name: tsgrtarchivos cod_archivo; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtarchivos
    ADD CONSTRAINT cod_archivo UNIQUE (cod_archivo);


--
-- TOC entry 3954 (class 2606 OID 100250)
-- Name: tsgrtasistentes cod_asistente; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtasistentes
    ADD CONSTRAINT cod_asistente UNIQUE (cod_asistente);


--
-- TOC entry 3958 (class 2606 OID 100252)
-- Name: tsgrtattchticket cod_attach; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtattchticket
    ADD CONSTRAINT cod_attach UNIQUE (cod_attach);


--
-- TOC entry 4019 (class 2606 OID 100254)
-- Name: tsgrtfaq cod_categofaq; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtfaq
    ADD CONSTRAINT cod_categofaq UNIQUE (cod_categoriafaq);


--
-- TOC entry 3970 (class 2606 OID 100256)
-- Name: tsgrtcategoriafaq cod_categoriafaq; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtcategoriafaq
    ADD CONSTRAINT cod_categoriafaq UNIQUE (cod_categoriafaq);


--
-- TOC entry 3978 (class 2606 OID 100258)
-- Name: tsgrtcomentariosagenda cod_comentsagenda; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtcomentariosagenda
    ADD CONSTRAINT cod_comentsagenda UNIQUE (cod_comentsagenda);


--
-- TOC entry 3991 (class 2606 OID 100260)
-- Name: tsgrtcorreo cod_correo; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtcorreo
    ADD CONSTRAINT cod_correo UNIQUE (cod_correo);


--
-- TOC entry 3997 (class 2606 OID 100262)
-- Name: tsgrtdatossolicitud cod_datosolicitud; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtdatossolicitud
    ADD CONSTRAINT cod_datosolicitud UNIQUE (cod_datosolicitud);


--
-- TOC entry 4001 (class 2606 OID 100264)
-- Name: tsgrtdepartamento cod_depto; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtdepartamento
    ADD CONSTRAINT cod_depto UNIQUE (cod_depto);


--
-- TOC entry 4078 (class 2606 OID 100266)
-- Name: tsgrtticket cod_deptoticket; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtticket
    ADD CONSTRAINT cod_deptoticket UNIQUE (cod_depto);


--
-- TOC entry 4007 (class 2606 OID 100268)
-- Name: tsgrtedosolicitudes cod_edosolicitud; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtedosolicitudes
    ADD CONSTRAINT cod_edosolicitud UNIQUE (cod_edosolicitud);


--
-- TOC entry 4011 (class 2606 OID 100270)
-- Name: tsgrtelementos cod_elemento; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtelementos
    ADD CONSTRAINT cod_elemento UNIQUE (cod_elemento);


--
-- TOC entry 4080 (class 2606 OID 100272)
-- Name: tsgrtticket cod_empleado; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtticket
    ADD CONSTRAINT cod_empleado UNIQUE (cod_empleado);


--
-- TOC entry 4015 (class 2606 OID 100274)
-- Name: tsgrtestados cod_estadorep; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtestados
    ADD CONSTRAINT cod_estadorep UNIQUE (cod_estadorep);


--
-- TOC entry 4021 (class 2606 OID 100276)
-- Name: tsgrtfaq cod_faq; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtfaq
    ADD CONSTRAINT cod_faq UNIQUE (cod_faq);


--
-- TOC entry 4025 (class 2606 OID 100278)
-- Name: tsgrtgrupo cod_grupo; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtgrupo
    ADD CONSTRAINT cod_grupo UNIQUE (cod_grupo);


--
-- TOC entry 4029 (class 2606 OID 100280)
-- Name: tsgrtinvitados cod_invitado; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtinvitados
    ADD CONSTRAINT cod_invitado UNIQUE (cod_invitado);


--
-- TOC entry 4034 (class 2606 OID 100282)
-- Name: tsgrtlugares cod_lugar; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtlugares
    ADD CONSTRAINT cod_lugar UNIQUE (cod_lugar);


--
-- TOC entry 4038 (class 2606 OID 100284)
-- Name: tsgrtmsjticket cod_mensaje; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtmsjticket
    ADD CONSTRAINT cod_mensaje UNIQUE (cod_mensaje);


--
-- TOC entry 4042 (class 2606 OID 100286)
-- Name: tsgrtnota cod_nota; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtnota
    ADD CONSTRAINT cod_nota UNIQUE (cod_nota);


--
-- TOC entry 4046 (class 2606 OID 100288)
-- Name: tsgrtplantillacorreos cod_plantillacorreo; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtplantillacorreos
    ADD CONSTRAINT cod_plantillacorreo UNIQUE (cod_plantillacorreo);


--
-- TOC entry 4050 (class 2606 OID 100290)
-- Name: tsgrtprioridad cod_prioridad; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtprioridad
    ADD CONSTRAINT cod_prioridad UNIQUE (cod_prioridad);


--
-- TOC entry 4062 (class 2606 OID 100292)
-- Name: tsgrtrespuesta cod_respuesta; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtrespuesta
    ADD CONSTRAINT cod_respuesta UNIQUE (cod_respuesta);


--
-- TOC entry 4056 (class 2606 OID 100294)
-- Name: tsgrtresppredefinida cod_respuestapredf; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtresppredefinida
    ADD CONSTRAINT cod_respuestapredf UNIQUE (cod_respuesta);


--
-- TOC entry 3950 (class 2606 OID 100296)
-- Name: tsgrtarchivos cod_reunionarchivos; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtarchivos
    ADD CONSTRAINT cod_reunionarchivos UNIQUE (cod_reunion);


--
-- TOC entry 4066 (class 2606 OID 100298)
-- Name: tsgrtreuniones cod_reuniones; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtreuniones
    ADD CONSTRAINT cod_reuniones UNIQUE (cod_reunion);


--
-- TOC entry 4070 (class 2606 OID 100300)
-- Name: tsgrtservicios cod_servicio; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtservicios
    ADD CONSTRAINT cod_servicio UNIQUE (cod_servicio);


--
-- TOC entry 4074 (class 2606 OID 100302)
-- Name: tsgrtsolicitudservicios cod_solicitud; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtsolicitudservicios
    ADD CONSTRAINT cod_solicitud UNIQUE (cod_solicitud);


--
-- TOC entry 4082 (class 2606 OID 100304)
-- Name: tsgrtticket cod_ticket; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtticket
    ADD CONSTRAINT cod_ticket UNIQUE (cod_ticket);


--
-- TOC entry 3960 (class 2606 OID 100306)
-- Name: tsgrtattchticket cod_ticketattach; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtattchticket
    ADD CONSTRAINT cod_ticketattach UNIQUE (cod_ticket);


--
-- TOC entry 3964 (class 2606 OID 100308)
-- Name: tsgrtayudatopico cod_topico; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtayudatopico
    ADD CONSTRAINT cod_topico UNIQUE (cod_topico);


--
-- TOC entry 3946 (class 2606 OID 100310)
-- Name: tsgrtagenda tsgrtagenda_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtagenda
    ADD CONSTRAINT tsgrtagenda_pkey PRIMARY KEY (cod_agenda);


--
-- TOC entry 3952 (class 2606 OID 100312)
-- Name: tsgrtarchivos tsgrtarchivos_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtarchivos
    ADD CONSTRAINT tsgrtarchivos_pkey PRIMARY KEY (cod_archivo);


--
-- TOC entry 3956 (class 2606 OID 100314)
-- Name: tsgrtasistentes tsgrtasistentes_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtasistentes
    ADD CONSTRAINT tsgrtasistentes_pkey PRIMARY KEY (cod_asistente);


--
-- TOC entry 3962 (class 2606 OID 100316)
-- Name: tsgrtattchticket tsgrtattchticket_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtattchticket
    ADD CONSTRAINT tsgrtattchticket_pkey PRIMARY KEY (cod_attach);


--
-- TOC entry 3966 (class 2606 OID 100318)
-- Name: tsgrtayudatopico tsgrtayudatopico_des_topico_key; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtayudatopico
    ADD CONSTRAINT tsgrtayudatopico_des_topico_key UNIQUE (des_topico);


--
-- TOC entry 3968 (class 2606 OID 100320)
-- Name: tsgrtayudatopico tsgrtayudatopico_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtayudatopico
    ADD CONSTRAINT tsgrtayudatopico_pkey PRIMARY KEY (cod_topico);


--
-- TOC entry 3972 (class 2606 OID 100322)
-- Name: tsgrtcategoriafaq tsgrtcategoriafaq_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtcategoriafaq
    ADD CONSTRAINT tsgrtcategoriafaq_pkey PRIMARY KEY (cod_categoriafaq);


--
-- TOC entry 3974 (class 2606 OID 100324)
-- Name: tsgrtchat tsgrtchat_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtchat
    ADD CONSTRAINT tsgrtchat_pkey PRIMARY KEY (cod_chat);


--
-- TOC entry 3976 (class 2606 OID 100326)
-- Name: tsgrtciudades tsgrtciudades_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtciudades
    ADD CONSTRAINT tsgrtciudades_pkey PRIMARY KEY (cod_ciudad);


--
-- TOC entry 3981 (class 2606 OID 100328)
-- Name: tsgrtcomentariosagenda tsgrtcomentariosagenda_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtcomentariosagenda
    ADD CONSTRAINT tsgrtcomentariosagenda_pkey PRIMARY KEY (cod_comentsagenda);


--
-- TOC entry 3984 (class 2606 OID 100330)
-- Name: tsgrtcomentariosreunion tsgrtcomentariosreunion_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtcomentariosreunion
    ADD CONSTRAINT tsgrtcomentariosreunion_pkey PRIMARY KEY (cod_commentsreunion);


--
-- TOC entry 3987 (class 2606 OID 100332)
-- Name: tsgrtcompromisos tsgrtcompromisos_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtcompromisos
    ADD CONSTRAINT tsgrtcompromisos_pkey PRIMARY KEY (cod_compromiso);


--
-- TOC entry 3993 (class 2606 OID 100334)
-- Name: tsgrtcorreo tsgrtcorreo_des_correo_key; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtcorreo
    ADD CONSTRAINT tsgrtcorreo_des_correo_key UNIQUE (des_correo);


--
-- TOC entry 3995 (class 2606 OID 100336)
-- Name: tsgrtcorreo tsgrtcorreo_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtcorreo
    ADD CONSTRAINT tsgrtcorreo_pkey PRIMARY KEY (cod_correo);


--
-- TOC entry 3999 (class 2606 OID 100338)
-- Name: tsgrtdatossolicitud tsgrtdatossolicitud_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtdatossolicitud
    ADD CONSTRAINT tsgrtdatossolicitud_pkey PRIMARY KEY (cod_datosolicitud);


--
-- TOC entry 4003 (class 2606 OID 100340)
-- Name: tsgrtdepartamento tsgrtdepartamento_des_nombre_key; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtdepartamento
    ADD CONSTRAINT tsgrtdepartamento_des_nombre_key UNIQUE (des_nombre);


--
-- TOC entry 4005 (class 2606 OID 100342)
-- Name: tsgrtdepartamento tsgrtdepartamento_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtdepartamento
    ADD CONSTRAINT tsgrtdepartamento_pkey PRIMARY KEY (cod_depto);


--
-- TOC entry 4009 (class 2606 OID 100344)
-- Name: tsgrtedosolicitudes tsgrtedosolicitudes_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtedosolicitudes
    ADD CONSTRAINT tsgrtedosolicitudes_pkey PRIMARY KEY (cod_edosolicitud);


--
-- TOC entry 4013 (class 2606 OID 100346)
-- Name: tsgrtelementos tsgrtelementos_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtelementos
    ADD CONSTRAINT tsgrtelementos_pkey PRIMARY KEY (cod_elemento);


--
-- TOC entry 4017 (class 2606 OID 100348)
-- Name: tsgrtestados tsgrtestados_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtestados
    ADD CONSTRAINT tsgrtestados_pkey PRIMARY KEY (cod_estadorep);


--
-- TOC entry 4023 (class 2606 OID 100350)
-- Name: tsgrtfaq tsgrtfaq_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtfaq
    ADD CONSTRAINT tsgrtfaq_pkey PRIMARY KEY (cod_faq);


--
-- TOC entry 4027 (class 2606 OID 100352)
-- Name: tsgrtgrupo tsgrtgrupo_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtgrupo
    ADD CONSTRAINT tsgrtgrupo_pkey PRIMARY KEY (cod_grupo);


--
-- TOC entry 4032 (class 2606 OID 100354)
-- Name: tsgrtinvitados tsgrtinvitados_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtinvitados
    ADD CONSTRAINT tsgrtinvitados_pkey PRIMARY KEY (cod_invitado);


--
-- TOC entry 4036 (class 2606 OID 100356)
-- Name: tsgrtlugares tsgrtlugares_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtlugares
    ADD CONSTRAINT tsgrtlugares_pkey PRIMARY KEY (cod_lugar);


--
-- TOC entry 4040 (class 2606 OID 100358)
-- Name: tsgrtmsjticket tsgrtmsjticket_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtmsjticket
    ADD CONSTRAINT tsgrtmsjticket_pkey PRIMARY KEY (cod_mensaje);


--
-- TOC entry 4044 (class 2606 OID 100360)
-- Name: tsgrtnota tsgrtnota_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtnota
    ADD CONSTRAINT tsgrtnota_pkey PRIMARY KEY (cod_nota);


--
-- TOC entry 4048 (class 2606 OID 100362)
-- Name: tsgrtplantillacorreos tsgrtplantillacorreos_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtplantillacorreos
    ADD CONSTRAINT tsgrtplantillacorreos_pkey PRIMARY KEY (cod_plantillacorreo);


--
-- TOC entry 4052 (class 2606 OID 100364)
-- Name: tsgrtprioridad tsgrtprioridad_des_nombre_key; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtprioridad
    ADD CONSTRAINT tsgrtprioridad_des_nombre_key UNIQUE (des_nombre);


--
-- TOC entry 4054 (class 2606 OID 100366)
-- Name: tsgrtprioridad tsgrtprioridad_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtprioridad
    ADD CONSTRAINT tsgrtprioridad_pkey PRIMARY KEY (cod_prioridad);


--
-- TOC entry 4058 (class 2606 OID 100368)
-- Name: tsgrtresppredefinida tsgrtresppredefinida_des_titulo_key; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtresppredefinida
    ADD CONSTRAINT tsgrtresppredefinida_des_titulo_key UNIQUE (des_titulo);


--
-- TOC entry 4060 (class 2606 OID 100370)
-- Name: tsgrtresppredefinida tsgrtresppredefinida_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtresppredefinida
    ADD CONSTRAINT tsgrtresppredefinida_pkey PRIMARY KEY (cod_respuesta);


--
-- TOC entry 4064 (class 2606 OID 100372)
-- Name: tsgrtrespuesta tsgrtrespuesta_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtrespuesta
    ADD CONSTRAINT tsgrtrespuesta_pkey PRIMARY KEY (cod_respuesta);


--
-- TOC entry 4068 (class 2606 OID 100374)
-- Name: tsgrtreuniones tsgrtreuniones_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtreuniones
    ADD CONSTRAINT tsgrtreuniones_pkey PRIMARY KEY (cod_reunion);


--
-- TOC entry 4072 (class 2606 OID 100376)
-- Name: tsgrtservicios tsgrtservicios_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtservicios
    ADD CONSTRAINT tsgrtservicios_pkey PRIMARY KEY (cod_servicio);


--
-- TOC entry 4076 (class 2606 OID 100378)
-- Name: tsgrtsolicitudservicios tsgrtsolicitudservicios_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtsolicitudservicios
    ADD CONSTRAINT tsgrtsolicitudservicios_pkey PRIMARY KEY (cod_solicitud);


--
-- TOC entry 4084 (class 2606 OID 100380)
-- Name: tsgrtticket tsgrtticket_des_correo_key; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtticket
    ADD CONSTRAINT tsgrtticket_des_correo_key UNIQUE (des_correo);


--
-- TOC entry 4086 (class 2606 OID 100382)
-- Name: tsgrtticket tsgrtticket_des_folio_key; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtticket
    ADD CONSTRAINT tsgrtticket_des_folio_key UNIQUE (des_folio);


--
-- TOC entry 4088 (class 2606 OID 100384)
-- Name: tsgrtticket tsgrtticket_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtticket
    ADD CONSTRAINT tsgrtticket_pkey PRIMARY KEY (cod_ticket);


--
-- TOC entry 3989 (class 2606 OID 100386)
-- Name: tsgrtcompromisos unique_cod_chat; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtcompromisos
    ADD CONSTRAINT unique_cod_chat UNIQUE (cod_chat);


--
-- TOC entry 4204 (class 2606 OID 102874)
-- Name: tsisatprospectos_idiomas pk_cod_pros_idioma; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatprospectos_idiomas
    ADD CONSTRAINT pk_cod_pros_idioma PRIMARY KEY (cod_pros_idoma);


--
-- TOC entry 4144 (class 2606 OID 102876)
-- Name: tsisatappservices tsisatappservices_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatappservices
    ADD CONSTRAINT tsisatappservices_pkey PRIMARY KEY (cod_appservice);


--
-- TOC entry 4146 (class 2606 OID 102878)
-- Name: tsisatarquitecturas tsisatarquitecturas_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatarquitecturas
    ADD CONSTRAINT tsisatarquitecturas_pkey PRIMARY KEY (cod_arquitectura);


--
-- TOC entry 4148 (class 2606 OID 102880)
-- Name: tsisatasignaciones tsisatasignaciones_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatasignaciones
    ADD CONSTRAINT tsisatasignaciones_pkey PRIMARY KEY (cod_asignacion);


--
-- TOC entry 4150 (class 2606 OID 102882)
-- Name: tsisatcandidatos tsisatcandidatos_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcandidatos
    ADD CONSTRAINT tsisatcandidatos_pkey PRIMARY KEY (cod_candidato);


--
-- TOC entry 4152 (class 2606 OID 102884)
-- Name: tsisatcartaaceptacion tsisatcartaaceptacion_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcartaaceptacion
    ADD CONSTRAINT tsisatcartaaceptacion_pkey PRIMARY KEY (cod_aceptacion);


--
-- TOC entry 4154 (class 2606 OID 102886)
-- Name: tsisatcartaasignacion tsisatcartaasignacion_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcartaasignacion
    ADD CONSTRAINT tsisatcartaasignacion_pkey PRIMARY KEY (cod_asignacion);


--
-- TOC entry 4158 (class 2606 OID 102888)
-- Name: tsisatcomentcosteo tsisatcomentarios_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcomentcosteo
    ADD CONSTRAINT tsisatcomentarios_pkey PRIMARY KEY (cod_comentcosteo);


--
-- TOC entry 4156 (class 2606 OID 102890)
-- Name: tsisatcomentcartaasignacion tsisatcomentcartaasignacion_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcomentcartaasignacion
    ADD CONSTRAINT tsisatcomentcartaasignacion_pkey PRIMARY KEY (cod_comentcartaasignacion);


--
-- TOC entry 4160 (class 2606 OID 102892)
-- Name: tsisatcomententrevista tsisatcomententrevista_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcomententrevista
    ADD CONSTRAINT tsisatcomententrevista_pkey PRIMARY KEY (cod_comententrevista);


--
-- TOC entry 4162 (class 2606 OID 102894)
-- Name: tsisatcomentvacantes tsisatcomentvacantes_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcomentvacantes
    ADD CONSTRAINT tsisatcomentvacantes_pkey PRIMARY KEY (cod_comentvacante);


--
-- TOC entry 4164 (class 2606 OID 102896)
-- Name: tsisatcontrataciones tsisatcontrataciones_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcontrataciones
    ADD CONSTRAINT tsisatcontrataciones_pkey PRIMARY KEY (cod_contratacion);


--
-- TOC entry 4166 (class 2606 OID 102898)
-- Name: tsisatcotizaciones tsisatcotizaciones_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcotizaciones
    ADD CONSTRAINT tsisatcotizaciones_pkey PRIMARY KEY (cod_cotizacion);


--
-- TOC entry 4168 (class 2606 OID 102900)
-- Name: tsisatcursosycertificados tsisatcursosycerticados_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcursosycertificados
    ADD CONSTRAINT tsisatcursosycerticados_pkey PRIMARY KEY (cod_curso);


--
-- TOC entry 4170 (class 2606 OID 102902)
-- Name: tsisatentrevistas tsisatentrevistas_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatentrevistas
    ADD CONSTRAINT tsisatentrevistas_pkey PRIMARY KEY (cod_entrevista);


--
-- TOC entry 4172 (class 2606 OID 102904)
-- Name: tsisatenviocorreos tsisatenviocorreos_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatenviocorreos
    ADD CONSTRAINT tsisatenviocorreos_pkey PRIMARY KEY (cod_envio);


--
-- TOC entry 4174 (class 2606 OID 102906)
-- Name: tsisatescolaridad tsisatescolaridad_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatescolaridad
    ADD CONSTRAINT tsisatescolaridad_pkey PRIMARY KEY (cod_escolaridad);


--
-- TOC entry 4176 (class 2606 OID 102908)
-- Name: tsisatexperienciaslaborales tsisatexperienciaslaborales_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatexperienciaslaborales
    ADD CONSTRAINT tsisatexperienciaslaborales_pkey PRIMARY KEY (cod_experiencia);


--
-- TOC entry 4178 (class 2606 OID 102910)
-- Name: tsisatfirmareqper tsisatfirmas_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatfirmareqper
    ADD CONSTRAINT tsisatfirmas_pkey PRIMARY KEY (cod_firma);


--
-- TOC entry 4180 (class 2606 OID 102912)
-- Name: tsisatframeworks tsisatframeworks_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatframeworks
    ADD CONSTRAINT tsisatframeworks_pkey PRIMARY KEY (cod_framework);


--
-- TOC entry 4182 (class 2606 OID 102914)
-- Name: tsisathabilidades tsisathabilidades_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisathabilidades
    ADD CONSTRAINT tsisathabilidades_pkey PRIMARY KEY (cod_habilidad);


--
-- TOC entry 4184 (class 2606 OID 102916)
-- Name: tsisatherramientas tsisatherramientas_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatherramientas
    ADD CONSTRAINT tsisatherramientas_pkey PRIMARY KEY (cod_herramientas);


--
-- TOC entry 4186 (class 2606 OID 102918)
-- Name: tsisatides tsisatides_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatides
    ADD CONSTRAINT tsisatides_pkey PRIMARY KEY (cod_ide);


--
-- TOC entry 4188 (class 2606 OID 102920)
-- Name: tsisatidiomas tsisatidiomas_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatidiomas
    ADD CONSTRAINT tsisatidiomas_pkey PRIMARY KEY (cod_idioma);


--
-- TOC entry 4190 (class 2606 OID 102922)
-- Name: tsisatlenguajes tsisatlenguajes_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatlenguajes
    ADD CONSTRAINT tsisatlenguajes_pkey PRIMARY KEY (cod_lenguaje);


--
-- TOC entry 4192 (class 2606 OID 102924)
-- Name: tsisatmaquetados tsisatmaquetados_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatmaquetados
    ADD CONSTRAINT tsisatmaquetados_pkey PRIMARY KEY (cod_maquetado);


--
-- TOC entry 4194 (class 2606 OID 102926)
-- Name: tsisatmetodologias tsisatmetodologias_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatmetodologias
    ADD CONSTRAINT tsisatmetodologias_pkey PRIMARY KEY (cod_metodologia);


--
-- TOC entry 4196 (class 2606 OID 102928)
-- Name: tsisatmodelados tsisatmodelados_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatmodelados
    ADD CONSTRAINT tsisatmodelados_pkey PRIMARY KEY (cod_modelado);


--
-- TOC entry 4198 (class 2606 OID 102930)
-- Name: tsisatordenservicio tsisatordenservicio_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatordenservicio
    ADD CONSTRAINT tsisatordenservicio_pkey PRIMARY KEY (cod_ordenservicio);


--
-- TOC entry 4200 (class 2606 OID 102932)
-- Name: tsisatpatrones tsisatpatrones_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatpatrones
    ADD CONSTRAINT tsisatpatrones_pkey PRIMARY KEY (cod_patron);


--
-- TOC entry 4202 (class 2606 OID 102934)
-- Name: tsisatprospectos tsisatprospectos_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatprospectos
    ADD CONSTRAINT tsisatprospectos_pkey PRIMARY KEY (cod_prospecto);


--
-- TOC entry 4206 (class 2606 OID 102936)
-- Name: tsisatprotocolos tsisatprotocolos_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatprotocolos
    ADD CONSTRAINT tsisatprotocolos_pkey PRIMARY KEY (cod_protocolo);


--
-- TOC entry 4208 (class 2606 OID 102938)
-- Name: tsisatproyectos tsisatproyectos_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatproyectos
    ADD CONSTRAINT tsisatproyectos_pkey PRIMARY KEY (cod_proyecto);


--
-- TOC entry 4210 (class 2606 OID 102940)
-- Name: tsisatqa tsisatqa_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatqa
    ADD CONSTRAINT tsisatqa_pkey PRIMARY KEY (cod_qa);


--
-- TOC entry 4212 (class 2606 OID 102942)
-- Name: tsisatrepositoriolibrerias tsisatrepositoriolibrerias_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatrepositoriolibrerias
    ADD CONSTRAINT tsisatrepositoriolibrerias_pkey PRIMARY KEY (cod_repositoriolibreria);


--
-- TOC entry 4214 (class 2606 OID 102944)
-- Name: tsisatrepositorios tsisatrepositorios_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatrepositorios
    ADD CONSTRAINT tsisatrepositorios_pkey PRIMARY KEY (cod_repositorio);


--
-- TOC entry 4216 (class 2606 OID 102946)
-- Name: tsisatsgbd tsisatsgbd_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatsgbd
    ADD CONSTRAINT tsisatsgbd_pkey PRIMARY KEY (cod_sgbd);


--
-- TOC entry 4218 (class 2606 OID 102948)
-- Name: tsisatso tsisatso_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatso
    ADD CONSTRAINT tsisatso_pkey PRIMARY KEY (cod_so);


--
-- TOC entry 4220 (class 2606 OID 102950)
-- Name: tsisatvacantes tsisatvacantes_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatvacantes
    ADD CONSTRAINT tsisatvacantes_pkey PRIMARY KEY (cod_vacante);


--
-- TOC entry 3985 (class 1259 OID 100465)
-- Name: fki_fk_cod_chat; Type: INDEX; Schema: sgrt; Owner: suite
--

CREATE INDEX fki_fk_cod_chat ON sgrt.tsgrtcompromisos USING btree (cod_chat);


--
-- TOC entry 4030 (class 1259 OID 100466)
-- Name: fki_fk_cod_empleado; Type: INDEX; Schema: sgrt; Owner: suite
--

CREATE INDEX fki_fk_cod_empleado ON sgrt.tsgrtinvitados USING btree (cod_empleado);


--
-- TOC entry 3982 (class 1259 OID 100467)
-- Name: fki_fk_cod_invitado; Type: INDEX; Schema: sgrt; Owner: suite
--

CREATE INDEX fki_fk_cod_invitado ON sgrt.tsgrtcomentariosreunion USING btree (cod_invitado);


--
-- TOC entry 3979 (class 1259 OID 100468)
-- Name: fki_fk_cod_invitados; Type: INDEX; Schema: sgrt; Owner: suite
--

CREATE INDEX fki_fk_cod_invitados ON sgrt.tsgrtcomentariosagenda USING btree (cod_invitado);


--
-- TOC entry 4507 (class 2620 OID 102540)
-- Name: tsgnomempleados historial_empleados; Type: TRIGGER; Schema: sgnom; Owner: suite
--

CREATE TRIGGER historial_empleados AFTER UPDATE OF imp_sueldoimss, imp_honorarios, cod_validaciones ON sgnom.tsgnomempleados FOR EACH ROW EXECUTE PROCEDURE sgnom.trigger_empleadoshst();

ALTER TABLE sgnom.tsgnomempleados DISABLE TRIGGER historial_empleados;


--
-- TOC entry 4508 (class 2620 OID 102495)
-- Name: tsgnomempleados nuevo_empleado; Type: TRIGGER; Schema: sgnom; Owner: suite
--

CREATE TRIGGER nuevo_empleado AFTER INSERT ON sgnom.tsgnomempleados FOR EACH ROW EXECUTE PROCEDURE sgnom.trigger_nuevoempleado();


--
-- TOC entry 4506 (class 2620 OID 100469)
-- Name: tsgrhencuesta tg_actualizarfecha; Type: TRIGGER; Schema: sgrh; Owner: suite
--

CREATE TRIGGER tg_actualizarfecha BEFORE UPDATE ON sgrh.tsgrhencuesta FOR EACH ROW EXECUTE PROCEDURE sgrh.factualizarfecha();


--
-- TOC entry 4224 (class 2606 OID 100470)
-- Name: tsgcousuarios fk_cod_empleado; Type: FK CONSTRAINT; Schema: sgco; Owner: suite
--

ALTER TABLE ONLY sgco.tsgcousuarios
    ADD CONSTRAINT fk_cod_empleado FOREIGN KEY (cod_empleado) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4221 (class 2606 OID 100475)
-- Name: tsgcotipousuario fk_tsgcotipousuario_tsgcosistemas_1; Type: FK CONSTRAINT; Schema: sgco; Owner: suite
--

ALTER TABLE ONLY sgco.tsgcotipousuario
    ADD CONSTRAINT fk_tsgcotipousuario_tsgcosistemas_1 FOREIGN KEY (cod_sistema) REFERENCES sgco.tsgcosistemas(cod_sistema);


--
-- TOC entry 4222 (class 2606 OID 100480)
-- Name: tsgcotipousuario fk_tsgcotipousuario_tsgcousuarios_1; Type: FK CONSTRAINT; Schema: sgco; Owner: suite
--

ALTER TABLE ONLY sgco.tsgcotipousuario
    ADD CONSTRAINT fk_tsgcotipousuario_tsgcousuarios_1 FOREIGN KEY (cod_usuario) REFERENCES sgco.tsgcousuarios(cod_usuario);


--
-- TOC entry 4223 (class 2606 OID 100485)
-- Name: tsgcotipousuario fk_tsgcotipousuario_tsgrhroles_1; Type: FK CONSTRAINT; Schema: sgco; Owner: suite
--

ALTER TABLE ONLY sgco.tsgcotipousuario
    ADD CONSTRAINT fk_tsgcotipousuario_tsgrhroles_1 FOREIGN KEY (cod_rol) REFERENCES sgrh.tsgrhroles(cod_rol);


--
-- TOC entry 4225 (class 2606 OID 100490)
-- Name: tsgcousuarios fk_tsgcousuarios_tsgrhempleados_1; Type: FK CONSTRAINT; Schema: sgco; Owner: suite
--

ALTER TABLE ONLY sgco.tsgcousuarios
    ADD CONSTRAINT fk_tsgcousuarios_tsgrhempleados_1 FOREIGN KEY (cod_empleado) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4400 (class 2606 OID 102170)
-- Name: tsgnomcncptoquincht concepto_quincena_id_copia_fk_conceptos_quincena; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomcncptoquincht
    ADD CONSTRAINT concepto_quincena_id_copia_fk_conceptos_quincena FOREIGN KEY (cod_conceptoid_fk) REFERENCES sgnom.tsgnomconcepto(cod_conceptoid) ON UPDATE CASCADE;


--
-- TOC entry 4398 (class 2606 OID 102175)
-- Name: tsgnomcncptoquinc concepto_quincena_id_fk_conceptos_quincena; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomcncptoquinc
    ADD CONSTRAINT concepto_quincena_id_fk_conceptos_quincena FOREIGN KEY (cod_conceptoid_fk) REFERENCES sgnom.tsgnomconcepto(cod_conceptoid) ON UPDATE CASCADE;


--
-- TOC entry 4401 (class 2606 OID 102180)
-- Name: tsgnomcncptoquincht empleado_concepto_id_copia_fk_conceptos_quincena; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomcncptoquincht
    ADD CONSTRAINT empleado_concepto_id_copia_fk_conceptos_quincena FOREIGN KEY (cod_empquincenaid_fk) REFERENCES sgnom.tsgnomempquincena(cod_empquincenaid) ON UPDATE CASCADE;


--
-- TOC entry 4399 (class 2606 OID 102185)
-- Name: tsgnomcncptoquinc empleado_concepto_id_fk_conceptos_quincena; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomcncptoquinc
    ADD CONSTRAINT empleado_concepto_id_fk_conceptos_quincena FOREIGN KEY (cod_empquincenaid_fk) REFERENCES sgnom.tsgnomempquincena(cod_empquincenaid) ON UPDATE CASCADE;


--
-- TOC entry 4422 (class 2606 OID 102470)
-- Name: tsgnomnominaimss fk_tsgnomnominaimss_tsgnomcabecera_1; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomnominaimss
    ADD CONSTRAINT fk_tsgnomnominaimss_tsgnomcabecera_1 FOREIGN KEY (cod_cabeceraid_fk) REFERENCES sgnom.tsgnomcabecera(cod_cabeceraid);


--
-- TOC entry 4409 (class 2606 OID 102190)
-- Name: tsgnomempquincena nom_cabecera_id_fk_empleados_quincena; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomempquincena
    ADD CONSTRAINT nom_cabecera_id_fk_empleados_quincena FOREIGN KEY (cod_cabeceraid_fk) REFERENCES sgnom.tsgnomcabecera(cod_cabeceraid) ON UPDATE CASCADE;


--
-- TOC entry 4411 (class 2606 OID 102195)
-- Name: tsgnomempquincenaht nom_cabecera_id_fk_empleados_quincena_copia; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomempquincenaht
    ADD CONSTRAINT nom_cabecera_id_fk_empleados_quincena_copia FOREIGN KEY (cod_cabeceraid_fk) REFERENCES sgnom.tsgnomcabecera(cod_cabeceraid) ON UPDATE CASCADE;


--
-- TOC entry 4402 (class 2606 OID 102200)
-- Name: tsgnomconcepto nom_cat_clasificador_id_fk_cat_conceptos; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomconcepto
    ADD CONSTRAINT nom_cat_clasificador_id_fk_cat_conceptos FOREIGN KEY (cod_clasificadorid_fk) REFERENCES sgnom.tsgnomclasificador(cod_clasificadorid) ON UPDATE CASCADE;


--
-- TOC entry 4417 (class 2606 OID 102205)
-- Name: tsgnommanterceros nom_cat_conceptos_fk_manuales_terceros; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnommanterceros
    ADD CONSTRAINT nom_cat_conceptos_fk_manuales_terceros FOREIGN KEY (cod_conceptoid_fk) REFERENCES sgnom.tsgnomconcepto(cod_conceptoid) ON UPDATE CASCADE;


--
-- TOC entry 4421 (class 2606 OID 102210)
-- Name: tsgnomquincena nom_cat_ejercicio_id_fk_cat_quincenas; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomquincena
    ADD CONSTRAINT nom_cat_ejercicio_id_fk_cat_quincenas FOREIGN KEY (cod_ejercicioid_fk) REFERENCES sgnom.tsgnomejercicio(cod_ejercicioid) ON UPDATE CASCADE;


--
-- TOC entry 4395 (class 2606 OID 102215)
-- Name: tsgnomcabeceraht nom_cat_estatus_nomina_id_copia_fk_cabeceras; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomcabeceraht
    ADD CONSTRAINT nom_cat_estatus_nomina_id_copia_fk_cabeceras FOREIGN KEY (cod_estatusnomid_fk) REFERENCES sgnom.tsgnomestatusnom(cod_estatusnomid) ON UPDATE CASCADE;


--
-- TOC entry 4392 (class 2606 OID 102220)
-- Name: tsgnomcabecera nom_cat_estatus_nomina_id_fk_cabeceras; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomcabecera
    ADD CONSTRAINT nom_cat_estatus_nomina_id_fk_cabeceras FOREIGN KEY (cod_estatusnomid_fk) REFERENCES sgnom.tsgnomestatusnom(cod_estatusnomid) ON UPDATE CASCADE;


--
-- TOC entry 4413 (class 2606 OID 102225)
-- Name: tsgnomincidencia nom_cat_incidencia_id_fk_incidencias; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomincidencia
    ADD CONSTRAINT nom_cat_incidencia_id_fk_incidencias FOREIGN KEY (cod_catincidenciaid_fk) REFERENCES sgnom.tsgnomcatincidencia(cod_catincidenciaid) ON UPDATE CASCADE;


--
-- TOC entry 4396 (class 2606 OID 102230)
-- Name: tsgnomcabeceraht nom_cat_quincena_id_copia_fk_cabeceras; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomcabeceraht
    ADD CONSTRAINT nom_cat_quincena_id_copia_fk_cabeceras FOREIGN KEY (cod_quincenaid_fk) REFERENCES sgnom.tsgnomquincena(cod_quincenaid) ON UPDATE CASCADE;


--
-- TOC entry 4393 (class 2606 OID 102235)
-- Name: tsgnomcabecera nom_cat_quincena_id_fk_cabeceras; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomcabecera
    ADD CONSTRAINT nom_cat_quincena_id_fk_cabeceras FOREIGN KEY (cod_quincenaid_fk) REFERENCES sgnom.tsgnomquincena(cod_quincenaid) ON UPDATE CASCADE;


--
-- TOC entry 4414 (class 2606 OID 102240)
-- Name: tsgnomincidencia nom_cat_quincena_id_fk_incidencias; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomincidencia
    ADD CONSTRAINT nom_cat_quincena_id_fk_incidencias FOREIGN KEY (cod_quincenaid_fk) REFERENCES sgnom.tsgnomquincena(cod_quincenaid) ON UPDATE CASCADE;


--
-- TOC entry 4418 (class 2606 OID 102245)
-- Name: tsgnommanterceros nom_cat_quincenas_fk_manuales_terceros_fin; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnommanterceros
    ADD CONSTRAINT nom_cat_quincenas_fk_manuales_terceros_fin FOREIGN KEY (cod_quincenafin_fk) REFERENCES sgnom.tsgnomquincena(cod_quincenaid) ON UPDATE CASCADE;


--
-- TOC entry 4419 (class 2606 OID 102250)
-- Name: tsgnommanterceros nom_cat_quincenas_fk_manuales_terceros_inicio; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnommanterceros
    ADD CONSTRAINT nom_cat_quincenas_fk_manuales_terceros_inicio FOREIGN KEY (cod_quincenainicio_fk) REFERENCES sgnom.tsgnomquincena(cod_quincenaid) ON UPDATE CASCADE;


--
-- TOC entry 4391 (class 2606 OID 102255)
-- Name: tsgnombitacora nom_cat_tabla_id_fk_nom_cat_tablas; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnombitacora
    ADD CONSTRAINT nom_cat_tabla_id_fk_nom_cat_tablas FOREIGN KEY (cod_tablaid_fk) REFERENCES sgnom.tsgnomhisttabla(cod_tablaid) ON UPDATE CASCADE;


--
-- TOC entry 4397 (class 2606 OID 102260)
-- Name: tsgnomcabeceraht nom_cat_tipo_nomina_id_copia_fk_cabeceras; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomcabeceraht
    ADD CONSTRAINT nom_cat_tipo_nomina_id_copia_fk_cabeceras FOREIGN KEY (cod_tiponominaid_fk) REFERENCES sgnom.tsgnomtiponomina(cod_tiponominaid) ON UPDATE CASCADE;


--
-- TOC entry 4394 (class 2606 OID 102265)
-- Name: tsgnomcabecera nom_cat_tipo_nomina_id_fk_cabeceras; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomcabecera
    ADD CONSTRAINT nom_cat_tipo_nomina_id_fk_cabeceras FOREIGN KEY (cod_tiponominaid_fk) REFERENCES sgnom.tsgnomtiponomina(cod_tiponominaid) ON UPDATE CASCADE;


--
-- TOC entry 4403 (class 2606 OID 102270)
-- Name: tsgnomconcepto nom_concepto_sat_id_fk_cat_conceptos; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomconcepto
    ADD CONSTRAINT nom_concepto_sat_id_fk_cat_conceptos FOREIGN KEY (cod_conceptosatid_fk) REFERENCES sgnom.tsgnomconceptosat(cod_conceptosatid) ON UPDATE CASCADE;


--
-- TOC entry 4415 (class 2606 OID 102275)
-- Name: tsgnomincidencia nom_emp_autoriza_fk_incidencias; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomincidencia
    ADD CONSTRAINT nom_emp_autoriza_fk_incidencias FOREIGN KEY (cod_empautoriza_fk) REFERENCES sgnom.tsgnomempleados(cod_empleadoid) ON UPDATE CASCADE;


--
-- TOC entry 4416 (class 2606 OID 102280)
-- Name: tsgnomincidencia nom_emp_reporta_fk_incidencias; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomincidencia
    ADD CONSTRAINT nom_emp_reporta_fk_incidencias FOREIGN KEY (cod_empreporta_fk) REFERENCES sgnom.tsgnomempleados(cod_empleadoid) ON UPDATE CASCADE;


--
-- TOC entry 4410 (class 2606 OID 102285)
-- Name: tsgnomempquincena nom_empleado_quincena_id_fk_empleados_quincena; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomempquincena
    ADD CONSTRAINT nom_empleado_quincena_id_fk_empleados_quincena FOREIGN KEY (cod_empleadoid_fk) REFERENCES sgnom.tsgnomempleados(cod_empleadoid) ON UPDATE CASCADE;


--
-- TOC entry 4412 (class 2606 OID 102290)
-- Name: tsgnomempquincenaht nom_empleado_quincena_id_fk_empleados_quincena_copia; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomempquincenaht
    ADD CONSTRAINT nom_empleado_quincena_id_fk_empleados_quincena_copia FOREIGN KEY (cod_empleadoid_fk) REFERENCES sgnom.tsgnomempleados(cod_empleadoid) ON UPDATE CASCADE;


--
-- TOC entry 4420 (class 2606 OID 102295)
-- Name: tsgnommanterceros nom_empleados_fk_manuales_terceros; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnommanterceros
    ADD CONSTRAINT nom_empleados_fk_manuales_terceros FOREIGN KEY (cod_empleadoid_fk) REFERENCES sgnom.tsgnomempleados(cod_empleadoid) ON UPDATE CASCADE;


--
-- TOC entry 4390 (class 2606 OID 102300)
-- Name: tsgnomaguinaldo nom_empleados_quincena_fk_aguinaldos; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomaguinaldo
    ADD CONSTRAINT nom_empleados_quincena_fk_aguinaldos FOREIGN KEY (cod_empquincenaid_fk) REFERENCES sgnom.tsgnomempquincena(cod_empquincenaid) ON UPDATE CASCADE;


--
-- TOC entry 4408 (class 2606 OID 102305)
-- Name: tsgnomconfpago nom_empleados_quincena_fk_conf_pago; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomconfpago
    ADD CONSTRAINT nom_empleados_quincena_fk_conf_pago FOREIGN KEY (cod_empquincenaid_fk) REFERENCES sgnom.tsgnomempquincena(cod_empquincenaid) ON UPDATE CASCADE;


--
-- TOC entry 4404 (class 2606 OID 102310)
-- Name: tsgnomconcepto nom_formula_id_fk_cat_conceptos; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomconcepto
    ADD CONSTRAINT nom_formula_id_fk_cat_conceptos FOREIGN KEY (cod_formulaid_fk) REFERENCES sgnom.tsgnomformula(cod_formulaid) ON UPDATE CASCADE;


--
-- TOC entry 4405 (class 2606 OID 102315)
-- Name: tsgnomconcepto nom_tipo_calculo_id_fk_cat_conceptos; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomconcepto
    ADD CONSTRAINT nom_tipo_calculo_id_fk_cat_conceptos FOREIGN KEY (cod_calculoid_fk) REFERENCES sgnom.tsgnomcalculo(cod_calculoid) ON UPDATE CASCADE;


--
-- TOC entry 4406 (class 2606 OID 102320)
-- Name: tsgnomconcepto nom_tipo_concepto_id_fk_cat_conceptos; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomconcepto
    ADD CONSTRAINT nom_tipo_concepto_id_fk_cat_conceptos FOREIGN KEY (cod_tipoconceptoid_fk) REFERENCES sgnom.tsgnomtipoconcepto(cod_tipoconceptoid) ON UPDATE CASCADE;


--
-- TOC entry 4407 (class 2606 OID 102325)
-- Name: tsgnomconcepto nom_tipo_nomina_id_fk_cat_conceptos; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomconcepto
    ADD CONSTRAINT nom_tipo_nomina_id_fk_cat_conceptos FOREIGN KEY (cod_tiponominaid_fk) REFERENCES sgnom.tsgnomtiponomina(cod_tiponominaid) ON UPDATE CASCADE;


--
-- TOC entry 4269 (class 2606 OID 100660)
-- Name: tsgrhlogistica cod_capacitacion_fk; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhlogistica
    ADD CONSTRAINT cod_capacitacion_fk FOREIGN KEY (cod_plancapacitacion) REFERENCES sgrh.tsgrhplancapacitacion(cod_plancapacitacion);


--
-- TOC entry 4272 (class 2606 OID 100665)
-- Name: tsgrhmodo cod_creadopor_fk; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhmodo
    ADD CONSTRAINT cod_creadopor_fk FOREIGN KEY (aud_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4286 (class 2606 OID 100670)
-- Name: tsgrhprocesos cod_creadopor_fk; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhprocesos
    ADD CONSTRAINT cod_creadopor_fk FOREIGN KEY (aud_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4288 (class 2606 OID 100675)
-- Name: tsgrhproveedores cod_creadopor_fk; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhproveedores
    ADD CONSTRAINT cod_creadopor_fk FOREIGN KEY (aud_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4308 (class 2606 OID 100680)
-- Name: tsgrhrolempleado cod_creadopor_fk; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhrolempleado
    ADD CONSTRAINT cod_creadopor_fk FOREIGN KEY (aud_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4311 (class 2606 OID 100685)
-- Name: tsgrhtipocapacitacion cod_creadopor_fk; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhtipocapacitacion
    ADD CONSTRAINT cod_creadopor_fk FOREIGN KEY (aud_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4257 (class 2606 OID 100690)
-- Name: tsgrhestatuscapacitacion cod_creadopor_fk; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhestatuscapacitacion
    ADD CONSTRAINT cod_creadopor_fk FOREIGN KEY (aud_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4274 (class 2606 OID 100695)
-- Name: tsgrhplancapacitacion cod_creadopor_fk; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhplancapacitacion
    ADD CONSTRAINT cod_creadopor_fk FOREIGN KEY (aud_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4291 (class 2606 OID 100700)
-- Name: tsgrhrelacionroles cod_creadopor_fk; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhrelacionroles
    ADD CONSTRAINT cod_creadopor_fk FOREIGN KEY (aud_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4270 (class 2606 OID 100705)
-- Name: tsgrhlogistica cod_creadopor_fk; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhlogistica
    ADD CONSTRAINT cod_creadopor_fk FOREIGN KEY (aud_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4259 (class 2606 OID 100710)
-- Name: tsgrhevacapacitacion cod_creadopor_fk; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhevacapacitacion
    ADD CONSTRAINT cod_creadopor_fk FOREIGN KEY (aud_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4273 (class 2606 OID 100715)
-- Name: tsgrhmodo cod_modificadopor_fk; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhmodo
    ADD CONSTRAINT cod_modificadopor_fk FOREIGN KEY (aud_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4287 (class 2606 OID 100720)
-- Name: tsgrhprocesos cod_modificadopor_fk; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhprocesos
    ADD CONSTRAINT cod_modificadopor_fk FOREIGN KEY (aud_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4289 (class 2606 OID 100725)
-- Name: tsgrhproveedores cod_modificadopor_fk; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhproveedores
    ADD CONSTRAINT cod_modificadopor_fk FOREIGN KEY (aud_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4309 (class 2606 OID 100730)
-- Name: tsgrhrolempleado cod_modificadopor_fk; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhrolempleado
    ADD CONSTRAINT cod_modificadopor_fk FOREIGN KEY (aud_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4312 (class 2606 OID 100735)
-- Name: tsgrhtipocapacitacion cod_modificadopor_fk; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhtipocapacitacion
    ADD CONSTRAINT cod_modificadopor_fk FOREIGN KEY (aud_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4258 (class 2606 OID 100740)
-- Name: tsgrhestatuscapacitacion cod_modificadopor_fk; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhestatuscapacitacion
    ADD CONSTRAINT cod_modificadopor_fk FOREIGN KEY (aud_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4275 (class 2606 OID 100745)
-- Name: tsgrhplancapacitacion cod_modificadopor_fk; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhplancapacitacion
    ADD CONSTRAINT cod_modificadopor_fk FOREIGN KEY (aud_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4292 (class 2606 OID 100750)
-- Name: tsgrhrelacionroles cod_modificadopor_fk; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhrelacionroles
    ADD CONSTRAINT cod_modificadopor_fk FOREIGN KEY (aud_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4271 (class 2606 OID 100755)
-- Name: tsgrhlogistica cod_modificadopor_fk; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhlogistica
    ADD CONSTRAINT cod_modificadopor_fk FOREIGN KEY (aud_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4260 (class 2606 OID 100760)
-- Name: tsgrhevacapacitacion cod_modificadopor_fk; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhevacapacitacion
    ADD CONSTRAINT cod_modificadopor_fk FOREIGN KEY (aud_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4293 (class 2606 OID 100765)
-- Name: tsgrhrelacionroles cod_plancapacitacion_fk; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhrelacionroles
    ADD CONSTRAINT cod_plancapacitacion_fk FOREIGN KEY (cod_plancapacitacion) REFERENCES sgrh.tsgrhplancapacitacion(cod_plancapacitacion);


--
-- TOC entry 4294 (class 2606 OID 100770)
-- Name: tsgrhrelacionroles cod_rolempleado_fk; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhrelacionroles
    ADD CONSTRAINT cod_rolempleado_fk FOREIGN KEY (cod_rolempleado) REFERENCES sgrh.tsgrhrolempleado(cod_rolempleado);


--
-- TOC entry 4233 (class 2606 OID 100775)
-- Name: tsgrhcartaasignacion fk_cod_ape; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhcartaasignacion
    ADD CONSTRAINT fk_cod_ape FOREIGN KEY (cod_ape) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4246 (class 2606 OID 100780)
-- Name: tsgrhempleados fk_cod_area; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhempleados
    ADD CONSTRAINT fk_cod_area FOREIGN KEY (cod_area) REFERENCES sgrh.tsgrhareas(cod_area) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4290 (class 2606 OID 100785)
-- Name: tsgrhpuestos fk_cod_area; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhpuestos
    ADD CONSTRAINT fk_cod_area FOREIGN KEY (cod_area) REFERENCES sgrh.tsgrhareas(cod_area) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4295 (class 2606 OID 100790)
-- Name: tsgrhrespuestasenc fk_cod_catrespuesta; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhrespuestasenc
    ADD CONSTRAINT fk_cod_catrespuesta FOREIGN KEY (cod_catrespuesta) REFERENCES sgrh.tsgrhcatrespuestas(cod_catrespuesta);


--
-- TOC entry 4234 (class 2606 OID 100795)
-- Name: tsgrhcartaasignacion fk_cod_cliente; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhcartaasignacion
    ADD CONSTRAINT fk_cod_cliente FOREIGN KEY (cod_cliente) REFERENCES sgrh.tsgrhclientes(cod_cliente) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4247 (class 2606 OID 100800)
-- Name: tsgrhempleados fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhempleados
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4244 (class 2606 OID 100805)
-- Name: tsgrhcontratos fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhcontratos
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4242 (class 2606 OID 100810)
-- Name: tsgrhcontrataciones fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhcontrataciones
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4249 (class 2606 OID 100815)
-- Name: tsgrhencuesta fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhencuesta
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4266 (class 2606 OID 100820)
-- Name: tsgrhevaluaciones fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhevaluaciones
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4281 (class 2606 OID 100825)
-- Name: tsgrhplanoperativo fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhplanoperativo
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4299 (class 2606 OID 100830)
-- Name: tsgrhrevplanoperativo fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhrevplanoperativo
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4235 (class 2606 OID 100835)
-- Name: tsgrhcartaasignacion fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhcartaasignacion
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4268 (class 2606 OID 100840)
-- Name: tsgrhexperienciaslaborales fk_cod_empleado; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhexperienciaslaborales
    ADD CONSTRAINT fk_cod_empleado FOREIGN KEY (cod_empleado) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4256 (class 2606 OID 100845)
-- Name: tsgrhescolaridad fk_cod_empleado; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhescolaridad
    ADD CONSTRAINT fk_cod_empleado FOREIGN KEY (cod_empleado) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4232 (class 2606 OID 100850)
-- Name: tsgrhcapacitaciones fk_cod_empleado; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhcapacitaciones
    ADD CONSTRAINT fk_cod_empleado FOREIGN KEY (cod_empleado) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4236 (class 2606 OID 100855)
-- Name: tsgrhcartaasignacion fk_cod_empleado; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhcartaasignacion
    ADD CONSTRAINT fk_cod_empleado FOREIGN KEY (cod_empleado) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4283 (class 2606 OID 100860)
-- Name: tsgrhpreguntasenc fk_cod_encuesta; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhpreguntasenc
    ADD CONSTRAINT fk_cod_encuesta FOREIGN KEY (cod_encuesta) REFERENCES sgrh.tsgrhencuesta(cod_encuesta) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4297 (class 2606 OID 100865)
-- Name: tsgrhrespuestaseva fk_cod_evacontestada; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhrespuestaseva
    ADD CONSTRAINT fk_cod_evacontestada FOREIGN KEY (cod_evacontestada) REFERENCES sgrh.tsgrhevacontestadas(cod_evacontestada) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4284 (class 2606 OID 100870)
-- Name: tsgrhpreguntaseva fk_cod_evaluacion; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhpreguntaseva
    ADD CONSTRAINT fk_cod_evaluacion FOREIGN KEY (cod_evaluacion) REFERENCES sgrh.tsgrhevaluaciones(cod_evaluacion) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4263 (class 2606 OID 100875)
-- Name: tsgrhevacontestadas fk_cod_evaluacion; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhevacontestadas
    ADD CONSTRAINT fk_cod_evaluacion FOREIGN KEY (cod_evaluacion) REFERENCES sgrh.tsgrhevaluaciones(cod_evaluacion) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4313 (class 2606 OID 100880)
-- Name: tsgrhvalidaevaluaciondes fk_cod_evaluacion; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhvalidaevaluaciondes
    ADD CONSTRAINT fk_cod_evaluacion FOREIGN KEY (cod_evaluacion) REFERENCES sgrh.tsgrhevaluaciones(cod_evaluacion) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4264 (class 2606 OID 100885)
-- Name: tsgrhevacontestadas fk_cod_evaluado; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhevacontestadas
    ADD CONSTRAINT fk_cod_evaluado FOREIGN KEY (cod_evaluado) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4314 (class 2606 OID 100890)
-- Name: tsgrhvalidaevaluaciondes fk_cod_evaluado; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhvalidaevaluaciondes
    ADD CONSTRAINT fk_cod_evaluado FOREIGN KEY (cod_evaluado) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4265 (class 2606 OID 100895)
-- Name: tsgrhevacontestadas fk_cod_evaluador; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhevacontestadas
    ADD CONSTRAINT fk_cod_evaluador FOREIGN KEY (cod_evaluador) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4315 (class 2606 OID 100900)
-- Name: tsgrhvalidaevaluaciondes fk_cod_evaluador; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhvalidaevaluaciondes
    ADD CONSTRAINT fk_cod_evaluador FOREIGN KEY (cod_evaluador) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4310 (class 2606 OID 100905)
-- Name: tsgrhsubfactoreseva fk_cod_factor; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhsubfactoreseva
    ADD CONSTRAINT fk_cod_factor FOREIGN KEY (cod_factor) REFERENCES sgrh.tsgrhfactoreseva(cod_factor) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4237 (class 2606 OID 100910)
-- Name: tsgrhcartaasignacion fk_cod_gpy; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhcartaasignacion
    ADD CONSTRAINT fk_cod_gpy FOREIGN KEY (cod_gpy) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4250 (class 2606 OID 100915)
-- Name: tsgrhencuesta fk_cod_lugar; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhencuesta
    ADD CONSTRAINT fk_cod_lugar FOREIGN KEY (cod_lugar) REFERENCES sgrt.tsgrtlugares(cod_lugar) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4316 (class 2606 OID 100920)
-- Name: tsgrhvalidaevaluaciondes fk_cod_lugar; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhvalidaevaluaciondes
    ADD CONSTRAINT fk_cod_lugar FOREIGN KEY (cod_lugar) REFERENCES sgrt.tsgrtlugares(cod_lugar) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4300 (class 2606 OID 100925)
-- Name: tsgrhrevplanoperativo fk_cod_lugar; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhrevplanoperativo
    ADD CONSTRAINT fk_cod_lugar FOREIGN KEY (cod_lugar) REFERENCES sgrt.tsgrtlugares(cod_lugar) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4248 (class 2606 OID 100930)
-- Name: tsgrhempleados fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhempleados
    ADD CONSTRAINT fk_cod_modificadopor FOREIGN KEY (cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4245 (class 2606 OID 100935)
-- Name: tsgrhcontratos fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhcontratos
    ADD CONSTRAINT fk_cod_modificadopor FOREIGN KEY (cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4243 (class 2606 OID 100940)
-- Name: tsgrhcontrataciones fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhcontrataciones
    ADD CONSTRAINT fk_cod_modificadopor FOREIGN KEY (cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4251 (class 2606 OID 100945)
-- Name: tsgrhencuesta fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhencuesta
    ADD CONSTRAINT fk_cod_modificadopor FOREIGN KEY (cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4267 (class 2606 OID 100950)
-- Name: tsgrhevaluaciones fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhevaluaciones
    ADD CONSTRAINT fk_cod_modificadopor FOREIGN KEY (cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4282 (class 2606 OID 100955)
-- Name: tsgrhplanoperativo fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhplanoperativo
    ADD CONSTRAINT fk_cod_modificadopor FOREIGN KEY (cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4301 (class 2606 OID 100960)
-- Name: tsgrhrevplanoperativo fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhrevplanoperativo
    ADD CONSTRAINT fk_cod_modificadopor FOREIGN KEY (cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4238 (class 2606 OID 100965)
-- Name: tsgrhcartaasignacion fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhcartaasignacion
    ADD CONSTRAINT fk_cod_modificadopor FOREIGN KEY (cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4317 (class 2606 OID 100970)
-- Name: tsgrhvalidaevaluaciondes fk_cod_participante1; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhvalidaevaluaciondes
    ADD CONSTRAINT fk_cod_participante1 FOREIGN KEY (cod_participante1) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4302 (class 2606 OID 100975)
-- Name: tsgrhrevplanoperativo fk_cod_participante1; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhrevplanoperativo
    ADD CONSTRAINT fk_cod_participante1 FOREIGN KEY (cod_participante1) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4318 (class 2606 OID 100980)
-- Name: tsgrhvalidaevaluaciondes fk_cod_participante2; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhvalidaevaluaciondes
    ADD CONSTRAINT fk_cod_participante2 FOREIGN KEY (cod_participante2) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4303 (class 2606 OID 100985)
-- Name: tsgrhrevplanoperativo fk_cod_participante2; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhrevplanoperativo
    ADD CONSTRAINT fk_cod_participante2 FOREIGN KEY (cod_participante2) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4319 (class 2606 OID 100990)
-- Name: tsgrhvalidaevaluaciondes fk_cod_participante3; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhvalidaevaluaciondes
    ADD CONSTRAINT fk_cod_participante3 FOREIGN KEY (cod_participante3) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4304 (class 2606 OID 100995)
-- Name: tsgrhrevplanoperativo fk_cod_participante3; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhrevplanoperativo
    ADD CONSTRAINT fk_cod_participante3 FOREIGN KEY (cod_participante3) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4320 (class 2606 OID 101000)
-- Name: tsgrhvalidaevaluaciondes fk_cod_participante4; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhvalidaevaluaciondes
    ADD CONSTRAINT fk_cod_participante4 FOREIGN KEY (cod_participante4) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4305 (class 2606 OID 101005)
-- Name: tsgrhrevplanoperativo fk_cod_participante4; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhrevplanoperativo
    ADD CONSTRAINT fk_cod_participante4 FOREIGN KEY (cod_participante4) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4306 (class 2606 OID 101010)
-- Name: tsgrhrevplanoperativo fk_cod_participante5; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhrevplanoperativo
    ADD CONSTRAINT fk_cod_participante5 FOREIGN KEY (cod_participante5) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4239 (class 2606 OID 101015)
-- Name: tsgrhcartaasignacion fk_cod_perfil; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhcartaasignacion
    ADD CONSTRAINT fk_cod_perfil FOREIGN KEY (cod_perfil) REFERENCES sgrh.tsgrhperfiles(cod_perfil) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4307 (class 2606 OID 101020)
-- Name: tsgrhrevplanoperativo fk_cod_planoperativo; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhrevplanoperativo
    ADD CONSTRAINT fk_cod_planoperativo FOREIGN KEY (cod_planoperativo) REFERENCES sgrh.tsgrhplanoperativo(cod_planoperativo) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4296 (class 2606 OID 101025)
-- Name: tsgrhrespuestasenc fk_cod_pregunta; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhrespuestasenc
    ADD CONSTRAINT fk_cod_pregunta FOREIGN KEY (cod_pregunta) REFERENCES sgrh.tsgrhpreguntasenc(cod_pregunta) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4298 (class 2606 OID 101030)
-- Name: tsgrhrespuestaseva fk_cod_pregunta; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhrespuestaseva
    ADD CONSTRAINT fk_cod_pregunta FOREIGN KEY (cod_pregunta) REFERENCES sgrh.tsgrhpreguntaseva(cod_pregunta) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4240 (class 2606 OID 101035)
-- Name: tsgrhcartaasignacion fk_cod_rhat; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhcartaasignacion
    ADD CONSTRAINT fk_cod_rhat FOREIGN KEY (cod_rhta) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4241 (class 2606 OID 101040)
-- Name: tsgrhcartaasignacion fk_cod_rys; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhcartaasignacion
    ADD CONSTRAINT fk_cod_rys FOREIGN KEY (cod_rys) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4285 (class 2606 OID 101045)
-- Name: tsgrhpreguntaseva fk_cod_subfactor; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhpreguntaseva
    ADD CONSTRAINT fk_cod_subfactor FOREIGN KEY (cod_subfactor) REFERENCES sgrh.tsgrhsubfactoreseva(cod_subfactor) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4228 (class 2606 OID 101050)
-- Name: tsgrhasignacionesemp fk_codasignadopor_asignacionesempleados; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhasignacionesemp
    ADD CONSTRAINT fk_codasignadopor_asignacionesempleados FOREIGN KEY (cod_asignadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4261 (class 2606 OID 101055)
-- Name: tsgrhevacapacitacion fk_codempleado_evacapacitacionesemp; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhevacapacitacion
    ADD CONSTRAINT fk_codempleado_evacapacitacionesemp FOREIGN KEY (cod_empleado) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4229 (class 2606 OID 101060)
-- Name: tsgrhasignacionesemp fk_codmodificadopor_asignacionesempleados; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhasignacionesemp
    ADD CONSTRAINT fk_codmodificadopor_asignacionesempleados FOREIGN KEY (cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4230 (class 2606 OID 101065)
-- Name: tsgrhasignacionesemp fk_codprospecto_asignacionesempleados; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhasignacionesemp
    ADD CONSTRAINT fk_codprospecto_asignacionesempleados FOREIGN KEY (cod_empleado) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4231 (class 2606 OID 101070)
-- Name: tsgrhasignacionesemp fk_codpuesto_asignacionesempleados; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhasignacionesemp
    ADD CONSTRAINT fk_codpuesto_asignacionesempleados FOREIGN KEY (cod_puesto) REFERENCES sgrh.tsgrhpuestos(cod_puesto);


--
-- TOC entry 4262 (class 2606 OID 101075)
-- Name: tsgrhevacapacitacion fk_plancapacitacion_evacapacitacionesemp; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhevacapacitacion
    ADD CONSTRAINT fk_plancapacitacion_evacapacitacionesemp FOREIGN KEY (cod_plancapacitacion) REFERENCES sgrh.tsgrhplancapacitacion(cod_plancapacitacion);


--
-- TOC entry 4276 (class 2606 OID 101080)
-- Name: tsgrhplancapacitacion plancap_tipocapacitacion_fk; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhplancapacitacion
    ADD CONSTRAINT plancap_tipocapacitacion_fk FOREIGN KEY (cod_tipocapacitacion) REFERENCES sgrh.tsgrhtipocapacitacion(cod_tipocapacitacion);


--
-- TOC entry 4277 (class 2606 OID 101085)
-- Name: tsgrhplancapacitacion plancapacitacion_estatus_fk; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhplancapacitacion
    ADD CONSTRAINT plancapacitacion_estatus_fk FOREIGN KEY (cod_estatus) REFERENCES sgrh.tsgrhestatuscapacitacion(cod_estatus);


--
-- TOC entry 4278 (class 2606 OID 101090)
-- Name: tsgrhplancapacitacion plancapacitacion_modo_fk; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhplancapacitacion
    ADD CONSTRAINT plancapacitacion_modo_fk FOREIGN KEY (cod_modo) REFERENCES sgrh.tsgrhmodo(cod_modo);


--
-- TOC entry 4279 (class 2606 OID 101095)
-- Name: tsgrhplancapacitacion plancapacitacion_proceso_fk; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhplancapacitacion
    ADD CONSTRAINT plancapacitacion_proceso_fk FOREIGN KEY (cod_proceso) REFERENCES sgrh.tsgrhprocesos(cod_proceso);


--
-- TOC entry 4280 (class 2606 OID 101100)
-- Name: tsgrhplancapacitacion plancapacitacion_proveedor_fk; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhplancapacitacion
    ADD CONSTRAINT plancapacitacion_proveedor_fk FOREIGN KEY (cod_proveedor) REFERENCES sgrh.tsgrhproveedores(cod_proveedor);


--
-- TOC entry 4226 (class 2606 OID 101105)
-- Name: tsgrhasignacion_encuesta tsgrhasignacion_encuesta_cod_empleado_fkey; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhasignacion_encuesta
    ADD CONSTRAINT tsgrhasignacion_encuesta_cod_empleado_fkey FOREIGN KEY (cod_empleado) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4227 (class 2606 OID 101110)
-- Name: tsgrhasignacion_encuesta tsgrhasignacion_encuesta_cod_encuesta_fkey; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhasignacion_encuesta
    ADD CONSTRAINT tsgrhasignacion_encuesta_cod_encuesta_fkey FOREIGN KEY (cod_encuesta) REFERENCES sgrh.tsgrhencuesta(cod_encuesta);


--
-- TOC entry 4252 (class 2606 OID 101115)
-- Name: tsgrhencuesta_participantes tsgrhencuesta_participantes_cod_empleado_fkey; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhencuesta_participantes
    ADD CONSTRAINT tsgrhencuesta_participantes_cod_empleado_fkey FOREIGN KEY (cod_empleado) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4253 (class 2606 OID 101120)
-- Name: tsgrhencuesta_participantes tsgrhencuesta_participantes_cod_encuesta_fkey; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhencuesta_participantes
    ADD CONSTRAINT tsgrhencuesta_participantes_cod_encuesta_fkey FOREIGN KEY (cod_encuesta) REFERENCES sgrh.tsgrhencuesta(cod_encuesta);


--
-- TOC entry 4254 (class 2606 OID 101125)
-- Name: tsgrhencuesta_participantes tsgrhencuesta_participantes_cod_pregunta_fkey; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhencuesta_participantes
    ADD CONSTRAINT tsgrhencuesta_participantes_cod_pregunta_fkey FOREIGN KEY (cod_pregunta) REFERENCES sgrh.tsgrhpreguntasenc(cod_pregunta);


--
-- TOC entry 4255 (class 2606 OID 101130)
-- Name: tsgrhencuesta_participantes tsgrhencuesta_participantes_cod_respuesta_fkey; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhencuesta_participantes
    ADD CONSTRAINT tsgrhencuesta_participantes_cod_respuesta_fkey FOREIGN KEY (cod_respuesta) REFERENCES sgrh.tsgrhrespuestasenc(cod_respuesta);


--
-- TOC entry 4334 (class 2606 OID 101135)
-- Name: tsgrtcomentariosagenda fk_cod_agenda; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtcomentariosagenda
    ADD CONSTRAINT fk_cod_agenda FOREIGN KEY (cod_agenda) REFERENCES sgrt.tsgrtagenda(cod_agenda) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4350 (class 2606 OID 101140)
-- Name: tsgrtfaq fk_cod_categoria; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtfaq
    ADD CONSTRAINT fk_cod_categoria FOREIGN KEY (cod_categoriafaq) REFERENCES sgrt.tsgrtcategoriafaq(cod_categoriafaq) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4338 (class 2606 OID 101145)
-- Name: tsgrtcompromisos fk_cod_chat; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtcompromisos
    ADD CONSTRAINT fk_cod_chat FOREIGN KEY (cod_chat) REFERENCES sgrt.tsgrtchat(cod_chat);


--
-- TOC entry 4357 (class 2606 OID 101150)
-- Name: tsgrtlugares fk_cod_ciudad; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtlugares
    ADD CONSTRAINT fk_cod_ciudad FOREIGN KEY (cod_ciudad) REFERENCES sgrt.tsgrtciudades(cod_ciudad) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4346 (class 2606 OID 101155)
-- Name: tsgrtdepartamento fk_cod_correo; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtdepartamento
    ADD CONSTRAINT fk_cod_correo FOREIGN KEY (cod_correo) REFERENCES sgrt.tsgrtcorreo(cod_correo) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4332 (class 2606 OID 101160)
-- Name: tsgrtcategoriafaq fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtcategoriafaq
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4340 (class 2606 OID 101165)
-- Name: tsgrtcorreo fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtcorreo
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4347 (class 2606 OID 101170)
-- Name: tsgrtdepartamento fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtdepartamento
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4351 (class 2606 OID 101175)
-- Name: tsgrtfaq fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtfaq
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4353 (class 2606 OID 101180)
-- Name: tsgrtgrupo fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtgrupo
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4328 (class 2606 OID 101185)
-- Name: tsgrtayudatopico fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtayudatopico
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4361 (class 2606 OID 101190)
-- Name: tsgrtnota fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtnota
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4365 (class 2606 OID 101195)
-- Name: tsgrtplantillacorreos fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtplantillacorreos
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4367 (class 2606 OID 101200)
-- Name: tsgrtprioridad fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtprioridad
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4372 (class 2606 OID 101205)
-- Name: tsgrtrespuesta fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtrespuesta
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4369 (class 2606 OID 101210)
-- Name: tsgrtresppredefinida fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtresppredefinida
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4384 (class 2606 OID 101215)
-- Name: tsgrtticket fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtticket
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4325 (class 2606 OID 101220)
-- Name: tsgrtattchticket fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtattchticket
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4358 (class 2606 OID 101225)
-- Name: tsgrtmsjticket fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtmsjticket
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4377 (class 2606 OID 101230)
-- Name: tsgrtreuniones fk_cod_creadorreunion; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtreuniones
    ADD CONSTRAINT fk_cod_creadorreunion FOREIGN KEY (cod_creadorreunion) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4341 (class 2606 OID 101235)
-- Name: tsgrtcorreo fk_cod_depto; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtcorreo
    ADD CONSTRAINT fk_cod_depto FOREIGN KEY (cod_depto) REFERENCES sgrt.tsgrtdepartamento(cod_depto) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4329 (class 2606 OID 101240)
-- Name: tsgrtayudatopico fk_cod_depto; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtayudatopico
    ADD CONSTRAINT fk_cod_depto FOREIGN KEY (cod_depto) REFERENCES sgrt.tsgrtdepartamento(cod_depto) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4370 (class 2606 OID 101245)
-- Name: tsgrtresppredefinida fk_cod_depto; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtresppredefinida
    ADD CONSTRAINT fk_cod_depto FOREIGN KEY (cod_depto) REFERENCES sgrt.tsgrtdepartamento(cod_depto) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4385 (class 2606 OID 101250)
-- Name: tsgrtticket fk_cod_depto; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtticket
    ADD CONSTRAINT fk_cod_depto FOREIGN KEY (cod_depto) REFERENCES sgrt.tsgrtdepartamento(cod_depto) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4343 (class 2606 OID 101255)
-- Name: tsgrtdatossolicitud fk_cod_edosolicitud; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtdatossolicitud
    ADD CONSTRAINT fk_cod_edosolicitud FOREIGN KEY (cod_edosolicitud) REFERENCES sgrt.tsgrtedosolicitudes(cod_edosolicitud) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4344 (class 2606 OID 101260)
-- Name: tsgrtdatossolicitud fk_cod_elemento; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtdatossolicitud
    ADD CONSTRAINT fk_cod_elemento FOREIGN KEY (cod_elemento) REFERENCES sgrt.tsgrtelementos(cod_elemento) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4323 (class 2606 OID 101265)
-- Name: tsgrtasistentes fk_cod_empleado; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtasistentes
    ADD CONSTRAINT fk_cod_empleado FOREIGN KEY (cod_empleado) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4362 (class 2606 OID 101270)
-- Name: tsgrtnota fk_cod_empleado; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtnota
    ADD CONSTRAINT fk_cod_empleado FOREIGN KEY (cod_empleado) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4373 (class 2606 OID 101275)
-- Name: tsgrtrespuesta fk_cod_empleado; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtrespuesta
    ADD CONSTRAINT fk_cod_empleado FOREIGN KEY (cod_empleado) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4381 (class 2606 OID 101280)
-- Name: tsgrtsolicitudservicios fk_cod_empleado; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtsolicitudservicios
    ADD CONSTRAINT fk_cod_empleado FOREIGN KEY (cod_empleado) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4386 (class 2606 OID 101285)
-- Name: tsgrtticket fk_cod_empleado; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtticket
    ADD CONSTRAINT fk_cod_empleado FOREIGN KEY (cod_empleado) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4355 (class 2606 OID 101290)
-- Name: tsgrtinvitados fk_cod_empleado; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtinvitados
    ADD CONSTRAINT fk_cod_empleado FOREIGN KEY (cod_empleado) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4324 (class 2606 OID 101295)
-- Name: tsgrtasistentes fk_cod_invitado; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtasistentes
    ADD CONSTRAINT fk_cod_invitado FOREIGN KEY (cod_invitado) REFERENCES sgrt.tsgrtinvitados(cod_invitado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4336 (class 2606 OID 101300)
-- Name: tsgrtcomentariosreunion fk_cod_invitado; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtcomentariosreunion
    ADD CONSTRAINT fk_cod_invitado FOREIGN KEY (cod_invitado) REFERENCES sgrt.tsgrtinvitados(cod_invitado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4335 (class 2606 OID 101305)
-- Name: tsgrtcomentariosagenda fk_cod_invitado; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtcomentariosagenda
    ADD CONSTRAINT fk_cod_invitado FOREIGN KEY (cod_invitado) REFERENCES sgrt.tsgrtinvitados(cod_invitado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4378 (class 2606 OID 101310)
-- Name: tsgrtreuniones fk_cod_lugar; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtreuniones
    ADD CONSTRAINT fk_cod_lugar FOREIGN KEY (cod_lugar) REFERENCES sgrt.tsgrtlugares(cod_lugar) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4374 (class 2606 OID 101315)
-- Name: tsgrtrespuesta fk_cod_mensaje; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtrespuesta
    ADD CONSTRAINT fk_cod_mensaje FOREIGN KEY (cod_mensaje) REFERENCES sgrt.tsgrtmsjticket(cod_mensaje) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4348 (class 2606 OID 101320)
-- Name: tsgrtdepartamento fk_cod_plantillacorreo; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtdepartamento
    ADD CONSTRAINT fk_cod_plantillacorreo FOREIGN KEY (cod_plantillacorreo) REFERENCES sgrt.tsgrtplantillacorreos(cod_plantillacorreo) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4330 (class 2606 OID 101325)
-- Name: tsgrtayudatopico fk_cod_prioridad; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtayudatopico
    ADD CONSTRAINT fk_cod_prioridad FOREIGN KEY (cod_prioridad) REFERENCES sgrt.tsgrtprioridad(cod_prioridad) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4387 (class 2606 OID 101330)
-- Name: tsgrtticket fk_cod_prioridad; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtticket
    ADD CONSTRAINT fk_cod_prioridad FOREIGN KEY (cod_prioridad) REFERENCES sgrt.tsgrtprioridad(cod_prioridad) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4379 (class 2606 OID 101335)
-- Name: tsgrtreuniones fk_cod_responsable; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtreuniones
    ADD CONSTRAINT fk_cod_responsable FOREIGN KEY (cod_responsable) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4339 (class 2606 OID 101340)
-- Name: tsgrtcompromisos fk_cod_reunion; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtcompromisos
    ADD CONSTRAINT fk_cod_reunion FOREIGN KEY (cod_reunion) REFERENCES sgrt.tsgrtreuniones(cod_reunion) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4322 (class 2606 OID 101345)
-- Name: tsgrtarchivos fk_cod_reunion; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtarchivos
    ADD CONSTRAINT fk_cod_reunion FOREIGN KEY (cod_reunion) REFERENCES sgrt.tsgrtreuniones(cod_reunion) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4356 (class 2606 OID 101350)
-- Name: tsgrtinvitados fk_cod_reunion; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtinvitados
    ADD CONSTRAINT fk_cod_reunion FOREIGN KEY (cod_reunion) REFERENCES sgrt.tsgrtreuniones(cod_reunion) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4337 (class 2606 OID 101355)
-- Name: tsgrtcomentariosreunion fk_cod_reunion; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtcomentariosreunion
    ADD CONSTRAINT fk_cod_reunion FOREIGN KEY (cod_reunion) REFERENCES sgrt.tsgrtreuniones(cod_reunion) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4321 (class 2606 OID 101360)
-- Name: tsgrtagenda fk_cod_reunion; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtagenda
    ADD CONSTRAINT fk_cod_reunion FOREIGN KEY (cod_reunion) REFERENCES sgrt.tsgrtreuniones(cod_reunion) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4380 (class 2606 OID 101365)
-- Name: tsgrtreuniones fk_cod_reunionanterior; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtreuniones
    ADD CONSTRAINT fk_cod_reunionanterior FOREIGN KEY (cod_proximareunion) REFERENCES sgrt.tsgrtreuniones(cod_reunion) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4382 (class 2606 OID 101370)
-- Name: tsgrtsolicitudservicios fk_cod_servicio; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtsolicitudservicios
    ADD CONSTRAINT fk_cod_servicio FOREIGN KEY (cod_servicio) REFERENCES sgrt.tsgrtservicios(cod_servicio) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4345 (class 2606 OID 101375)
-- Name: tsgrtdatossolicitud fk_cod_solicitud; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtdatossolicitud
    ADD CONSTRAINT fk_cod_solicitud FOREIGN KEY (cod_solicitud) REFERENCES sgrt.tsgrtsolicitudservicios(cod_solicitud) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4363 (class 2606 OID 101380)
-- Name: tsgrtnota fk_cod_ticket; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtnota
    ADD CONSTRAINT fk_cod_ticket FOREIGN KEY (cod_ticket) REFERENCES sgrt.tsgrtticket(cod_ticket) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4375 (class 2606 OID 101385)
-- Name: tsgrtrespuesta fk_cod_ticket; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtrespuesta
    ADD CONSTRAINT fk_cod_ticket FOREIGN KEY (cod_ticket) REFERENCES sgrt.tsgrtticket(cod_ticket) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4383 (class 2606 OID 101390)
-- Name: tsgrtsolicitudservicios fk_cod_ticket; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtsolicitudservicios
    ADD CONSTRAINT fk_cod_ticket FOREIGN KEY (cod_ticket) REFERENCES sgrt.tsgrtticket(cod_ticket) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4326 (class 2606 OID 101395)
-- Name: tsgrtattchticket fk_cod_ticket; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtattchticket
    ADD CONSTRAINT fk_cod_ticket FOREIGN KEY (cod_ticket) REFERENCES sgrt.tsgrtticket(cod_ticket) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4359 (class 2606 OID 101400)
-- Name: tsgrtmsjticket fk_cod_ticket; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtmsjticket
    ADD CONSTRAINT fk_cod_ticket FOREIGN KEY (cod_ticket) REFERENCES sgrt.tsgrtticket(cod_ticket) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4388 (class 2606 OID 101405)
-- Name: tsgrtticket fk_cod_topico; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtticket
    ADD CONSTRAINT fk_cod_topico FOREIGN KEY (cod_topico) REFERENCES sgrt.tsgrtayudatopico(cod_topico) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4342 (class 2606 OID 101410)
-- Name: tsgrtcorreo fk_cod_ultactualizacionpor; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtcorreo
    ADD CONSTRAINT fk_cod_ultactualizacionpor FOREIGN KEY (cod_ultactualizacionpor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4349 (class 2606 OID 101415)
-- Name: tsgrtdepartamento fk_cod_ultactualizacionpor; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtdepartamento
    ADD CONSTRAINT fk_cod_ultactualizacionpor FOREIGN KEY (cod_ultactualizacionpor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4352 (class 2606 OID 101420)
-- Name: tsgrtfaq fk_cod_ultactualizacionpor; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtfaq
    ADD CONSTRAINT fk_cod_ultactualizacionpor FOREIGN KEY (cod_ultactualizacionpor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4354 (class 2606 OID 101425)
-- Name: tsgrtgrupo fk_cod_ultactualizacionpor; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtgrupo
    ADD CONSTRAINT fk_cod_ultactualizacionpor FOREIGN KEY (cod_ultactualizacionpor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4331 (class 2606 OID 101430)
-- Name: tsgrtayudatopico fk_cod_ultactualizacionpor; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtayudatopico
    ADD CONSTRAINT fk_cod_ultactualizacionpor FOREIGN KEY (cod_ultactualizacionpor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4364 (class 2606 OID 101435)
-- Name: tsgrtnota fk_cod_ultactualizacionpor; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtnota
    ADD CONSTRAINT fk_cod_ultactualizacionpor FOREIGN KEY (cod_ultactualizacionpor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4366 (class 2606 OID 101440)
-- Name: tsgrtplantillacorreos fk_cod_ultactualizacionpor; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtplantillacorreos
    ADD CONSTRAINT fk_cod_ultactualizacionpor FOREIGN KEY (cod_ultactualizacionpor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4368 (class 2606 OID 101445)
-- Name: tsgrtprioridad fk_cod_ultactualizacionpor; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtprioridad
    ADD CONSTRAINT fk_cod_ultactualizacionpor FOREIGN KEY (cod_ultactualizacionpor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4376 (class 2606 OID 101450)
-- Name: tsgrtrespuesta fk_cod_ultactualizacionpor; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtrespuesta
    ADD CONSTRAINT fk_cod_ultactualizacionpor FOREIGN KEY (cod_ultactualizacionpor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4371 (class 2606 OID 101455)
-- Name: tsgrtresppredefinida fk_cod_ultactualizacionpor; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtresppredefinida
    ADD CONSTRAINT fk_cod_ultactualizacionpor FOREIGN KEY (cod_ultactualizacionpor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4389 (class 2606 OID 101460)
-- Name: tsgrtticket fk_cod_ultactualizacionpor; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtticket
    ADD CONSTRAINT fk_cod_ultactualizacionpor FOREIGN KEY (cod_ultactualizacionpor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4327 (class 2606 OID 101465)
-- Name: tsgrtattchticket fk_cod_ultactualizacionpor; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtattchticket
    ADD CONSTRAINT fk_cod_ultactualizacionpor FOREIGN KEY (cod_ultactualizacionpor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4360 (class 2606 OID 101470)
-- Name: tsgrtmsjticket fk_cod_ultactualizacionpor; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtmsjticket
    ADD CONSTRAINT fk_cod_ultactualizacionpor FOREIGN KEY (cod_ultactualizacionpor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4333 (class 2606 OID 101475)
-- Name: tsgrtciudades fk_estadorep; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtciudades
    ADD CONSTRAINT fk_estadorep FOREIGN KEY (cod_estadorep) REFERENCES sgrt.tsgrtestados(cod_estadorep) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4464 (class 2606 OID 102951)
-- Name: tsisatcotizaciones aud_fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcotizaciones
    ADD CONSTRAINT aud_fk_cod_creadopor FOREIGN KEY (aud_cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4484 (class 2606 OID 102956)
-- Name: tsisatordenservicio aud_fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatordenservicio
    ADD CONSTRAINT aud_fk_cod_creadopor FOREIGN KEY (aud_cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4465 (class 2606 OID 102961)
-- Name: tsisatcotizaciones aud_fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcotizaciones
    ADD CONSTRAINT aud_fk_cod_modificadopor FOREIGN KEY (aud_cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4485 (class 2606 OID 102966)
-- Name: tsisatordenservicio aud_fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatordenservicio
    ADD CONSTRAINT aud_fk_cod_modificadopor FOREIGN KEY (aud_cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4475 (class 2606 OID 102971)
-- Name: tsisatenviocorreos aud_fk_creadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatenviocorreos
    ADD CONSTRAINT aud_fk_creadopor FOREIGN KEY (aud_cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4476 (class 2606 OID 102976)
-- Name: tsisatenviocorreos aud_fk_modificadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatenviocorreos
    ADD CONSTRAINT aud_fk_modificadopor FOREIGN KEY (aud_cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4480 (class 2606 OID 102981)
-- Name: tsisatfirmareqper cod_autorizafk; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatfirmareqper
    ADD CONSTRAINT cod_autorizafk FOREIGN KEY (cod_autoriza) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE;


--
-- TOC entry 4481 (class 2606 OID 102986)
-- Name: tsisatfirmareqper cod_solicitafk; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatfirmareqper
    ADD CONSTRAINT cod_solicitafk FOREIGN KEY (cod_solicita) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE;


--
-- TOC entry 4482 (class 2606 OID 102991)
-- Name: tsisatfirmareqper cod_vacantefk; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatfirmareqper
    ADD CONSTRAINT cod_vacantefk FOREIGN KEY (cod_vacante) REFERENCES sisat.tsisatvacantes(cod_vacante) ON UPDATE CASCADE;


--
-- TOC entry 4439 (class 2606 OID 102996)
-- Name: tsisatcartaasignacion fk__cod_gpy; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcartaasignacion
    ADD CONSTRAINT fk__cod_gpy FOREIGN KEY (cod_gpy) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4496 (class 2606 OID 103001)
-- Name: tsisatproyectos fk_aud_cod_creadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatproyectos
    ADD CONSTRAINT fk_aud_cod_creadopor FOREIGN KEY (aud_cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4425 (class 2606 OID 103006)
-- Name: tsisatasignaciones fk_aud_cod_creadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatasignaciones
    ADD CONSTRAINT fk_aud_cod_creadopor FOREIGN KEY (aud_cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4497 (class 2606 OID 103011)
-- Name: tsisatproyectos fk_aud_cod_modificadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatproyectos
    ADD CONSTRAINT fk_aud_cod_modificadopor FOREIGN KEY (aud_cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4426 (class 2606 OID 103016)
-- Name: tsisatasignaciones fk_aud_cod_modificadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatasignaciones
    ADD CONSTRAINT fk_aud_cod_modificadopor FOREIGN KEY (aud_cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4491 (class 2606 OID 103021)
-- Name: tsisatprospectos fk_cod_administrador; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatprospectos
    ADD CONSTRAINT fk_cod_administrador FOREIGN KEY (cod_administrador) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4440 (class 2606 OID 103026)
-- Name: tsisatcartaasignacion fk_cod_ape; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcartaasignacion
    ADD CONSTRAINT fk_cod_ape FOREIGN KEY (cod_ape) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4452 (class 2606 OID 103031)
-- Name: tsisatcomentcosteo fk_cod_candidato; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcomentcosteo
    ADD CONSTRAINT fk_cod_candidato FOREIGN KEY (cod_candidato) REFERENCES sisat.tsisatcandidatos(cod_candidato);


--
-- TOC entry 4466 (class 2606 OID 103036)
-- Name: tsisatcotizaciones fk_cod_ciudad; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcotizaciones
    ADD CONSTRAINT fk_cod_ciudad FOREIGN KEY (cod_ciudad) REFERENCES sgrt.tsgrtciudades(cod_ciudad);


--
-- TOC entry 4486 (class 2606 OID 103041)
-- Name: tsisatordenservicio fk_cod_ciudad; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatordenservicio
    ADD CONSTRAINT fk_cod_ciudad FOREIGN KEY (cod_ciudad) REFERENCES sgrt.tsgrtciudades(cod_ciudad);


--
-- TOC entry 4441 (class 2606 OID 103046)
-- Name: tsisatcartaasignacion fk_cod_cliente; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcartaasignacion
    ADD CONSTRAINT fk_cod_cliente FOREIGN KEY (cod_cliente) REFERENCES sgrh.tsgrhclientes(cod_cliente) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4487 (class 2606 OID 103051)
-- Name: tsisatordenservicio fk_cod_cliente; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatordenservicio
    ADD CONSTRAINT fk_cod_cliente FOREIGN KEY (cod_cliente) REFERENCES sgrh.tsgrhclientes(cod_cliente) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4423 (class 2606 OID 103056)
-- Name: tsisatasignaciones fk_cod_cliente; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatasignaciones
    ADD CONSTRAINT fk_cod_cliente FOREIGN KEY (cod_cliente) REFERENCES sgrh.tsgrhclientes(cod_cliente) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4501 (class 2606 OID 103061)
-- Name: tsisatvacantes fk_cod_cliente; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatvacantes
    ADD CONSTRAINT fk_cod_cliente FOREIGN KEY (cod_cliente) REFERENCES sgrh.tsgrhclientes(cod_cliente);


--
-- TOC entry 4449 (class 2606 OID 103066)
-- Name: tsisatcomentcartaasignacion fk_cod_comentcartaasignacion; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcomentcartaasignacion
    ADD CONSTRAINT fk_cod_comentcartaasignacion FOREIGN KEY (cod_asignacion) REFERENCES sisat.tsisatcartaasignacion(cod_asignacion);


--
-- TOC entry 4502 (class 2606 OID 103071)
-- Name: tsisatvacantes fk_cod_contratacion; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatvacantes
    ADD CONSTRAINT fk_cod_contratacion FOREIGN KEY (cod_contratacion) REFERENCES sisat.tsisatcontrataciones(cod_contratacion) ON UPDATE CASCADE;


--
-- TOC entry 4503 (class 2606 OID 103076)
-- Name: tsisatvacantes fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatvacantes
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (aud_cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4434 (class 2606 OID 103081)
-- Name: tsisatcartaaceptacion fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcartaaceptacion
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (aud_cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4438 (class 2606 OID 103086)
-- Name: tsisatcartaasignacion fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcartaasignacion
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (aud_cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4463 (class 2606 OID 103091)
-- Name: tsisatcotizaciones fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcotizaciones
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (aud_cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4474 (class 2606 OID 103096)
-- Name: tsisatenviocorreos fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatenviocorreos
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (aud_cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4424 (class 2606 OID 103101)
-- Name: tsisatasignaciones fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatasignaciones
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (aud_cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4471 (class 2606 OID 103106)
-- Name: tsisatentrevistas fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatentrevistas
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (aud_cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4490 (class 2606 OID 103111)
-- Name: tsisatprospectos fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatprospectos
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (aud_cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4495 (class 2606 OID 103116)
-- Name: tsisatproyectos fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatproyectos
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (aud_cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4461 (class 2606 OID 103121)
-- Name: tsisatcontrataciones fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcontrataciones
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (aud_cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE;


--
-- TOC entry 4458 (class 2606 OID 103126)
-- Name: tsisatcomentvacantes fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcomentvacantes
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (aud_cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE;


--
-- TOC entry 4455 (class 2606 OID 103131)
-- Name: tsisatcomententrevista fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcomententrevista
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (aud_cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE;


--
-- TOC entry 4453 (class 2606 OID 103136)
-- Name: tsisatcomentcosteo fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcomentcosteo
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (aud_cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE;


--
-- TOC entry 4450 (class 2606 OID 103141)
-- Name: tsisatcomentcartaasignacion fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcomentcartaasignacion
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (aud_cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE;


--
-- TOC entry 4427 (class 2606 OID 103146)
-- Name: tsisatasignaciones fk_cod_empleado; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatasignaciones
    ADD CONSTRAINT fk_cod_empleado FOREIGN KEY (cod_empleado) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4456 (class 2606 OID 103151)
-- Name: tsisatcomententrevista fk_cod_entrevista; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcomententrevista
    ADD CONSTRAINT fk_cod_entrevista FOREIGN KEY (cod_entrevista) REFERENCES sisat.tsisatentrevistas(cod_entrevista);


--
-- TOC entry 4467 (class 2606 OID 103156)
-- Name: tsisatcotizaciones fk_cod_estado; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcotizaciones
    ADD CONSTRAINT fk_cod_estado FOREIGN KEY (cod_estado) REFERENCES sgrt.tsgrtestados(cod_estadorep);


--
-- TOC entry 4488 (class 2606 OID 103161)
-- Name: tsisatordenservicio fk_cod_estadorep; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatordenservicio
    ADD CONSTRAINT fk_cod_estadorep FOREIGN KEY (cod_estadorep) REFERENCES sgrt.tsgrtestados(cod_estadorep);


--
-- TOC entry 4442 (class 2606 OID 103166)
-- Name: tsisatcartaasignacion fk_cod_gpy; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcartaasignacion
    ADD CONSTRAINT fk_cod_gpy FOREIGN KEY (cod_gpy) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4489 (class 2606 OID 103171)
-- Name: tsisatordenservicio fk_cod_gpy; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatordenservicio
    ADD CONSTRAINT fk_cod_gpy FOREIGN KEY (cod_gpy) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4504 (class 2606 OID 103176)
-- Name: tsisatvacantes fk_cod_idioma; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatvacantes
    ADD CONSTRAINT fk_cod_idioma FOREIGN KEY (cod_idioma) REFERENCES sisat.tsisatidiomas(cod_idioma);


--
-- TOC entry 4493 (class 2606 OID 103181)
-- Name: tsisatprospectos_idiomas fk_cod_idioma; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatprospectos_idiomas
    ADD CONSTRAINT fk_cod_idioma FOREIGN KEY (cod_idioma) REFERENCES sisat.tsisatidiomas(cod_idioma);


--
-- TOC entry 4505 (class 2606 OID 103186)
-- Name: tsisatvacantes fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatvacantes
    ADD CONSTRAINT fk_cod_modificadopor FOREIGN KEY (aud_cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4435 (class 2606 OID 103191)
-- Name: tsisatcartaaceptacion fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcartaaceptacion
    ADD CONSTRAINT fk_cod_modificadopor FOREIGN KEY (aud_cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4443 (class 2606 OID 103196)
-- Name: tsisatcartaasignacion fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcartaasignacion
    ADD CONSTRAINT fk_cod_modificadopor FOREIGN KEY (aud_cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4468 (class 2606 OID 103201)
-- Name: tsisatcotizaciones fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcotizaciones
    ADD CONSTRAINT fk_cod_modificadopor FOREIGN KEY (aud_cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4477 (class 2606 OID 103206)
-- Name: tsisatenviocorreos fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatenviocorreos
    ADD CONSTRAINT fk_cod_modificadopor FOREIGN KEY (aud_cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4428 (class 2606 OID 103211)
-- Name: tsisatasignaciones fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatasignaciones
    ADD CONSTRAINT fk_cod_modificadopor FOREIGN KEY (aud_cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4472 (class 2606 OID 103216)
-- Name: tsisatentrevistas fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatentrevistas
    ADD CONSTRAINT fk_cod_modificadopor FOREIGN KEY (aud_cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4492 (class 2606 OID 103221)
-- Name: tsisatprospectos fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatprospectos
    ADD CONSTRAINT fk_cod_modificadopor FOREIGN KEY (aud_cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4498 (class 2606 OID 103226)
-- Name: tsisatproyectos fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatproyectos
    ADD CONSTRAINT fk_cod_modificadopor FOREIGN KEY (aud_cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4462 (class 2606 OID 103231)
-- Name: tsisatcontrataciones fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcontrataciones
    ADD CONSTRAINT fk_cod_modificadopor FOREIGN KEY (aud_cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE;


--
-- TOC entry 4459 (class 2606 OID 103236)
-- Name: tsisatcomentvacantes fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcomentvacantes
    ADD CONSTRAINT fk_cod_modificadopor FOREIGN KEY (aud_cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE;


--
-- TOC entry 4457 (class 2606 OID 103241)
-- Name: tsisatcomententrevista fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcomententrevista
    ADD CONSTRAINT fk_cod_modificadopor FOREIGN KEY (aud_cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE;


--
-- TOC entry 4454 (class 2606 OID 103246)
-- Name: tsisatcomentcosteo fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcomentcosteo
    ADD CONSTRAINT fk_cod_modificadopor FOREIGN KEY (aud_cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE;


--
-- TOC entry 4451 (class 2606 OID 103251)
-- Name: tsisatcomentcartaasignacion fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcomentcartaasignacion
    ADD CONSTRAINT fk_cod_modificadopor FOREIGN KEY (aud_cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE;


--
-- TOC entry 4444 (class 2606 OID 103256)
-- Name: tsisatcartaasignacion fk_cod_perfil; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcartaasignacion
    ADD CONSTRAINT fk_cod_perfil FOREIGN KEY (cod_perfil) REFERENCES sgrh.tsgrhperfiles(cod_perfil) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4431 (class 2606 OID 103261)
-- Name: tsisatcandidatos fk_cod_perfil; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcandidatos
    ADD CONSTRAINT fk_cod_perfil FOREIGN KEY (cod_perfil) REFERENCES sgrh.tsgrhperfiles(cod_perfil) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4429 (class 2606 OID 103266)
-- Name: tsisatasignaciones fk_cod_perfil; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatasignaciones
    ADD CONSTRAINT fk_cod_perfil FOREIGN KEY (cod_perfil) REFERENCES sgrh.tsgrhperfiles(cod_perfil) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4499 (class 2606 OID 103271)
-- Name: tsisatproyectos fk_cod_perfil; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatproyectos
    ADD CONSTRAINT fk_cod_perfil FOREIGN KEY (cod_perfil) REFERENCES sgrh.tsgrhperfiles(cod_perfil) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4445 (class 2606 OID 103276)
-- Name: tsisatcartaasignacion fk_cod_prospecto; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcartaasignacion
    ADD CONSTRAINT fk_cod_prospecto FOREIGN KEY (cod_prospecto) REFERENCES sisat.tsisatprospectos(cod_prospecto) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4479 (class 2606 OID 103281)
-- Name: tsisatexperienciaslaborales fk_cod_prospecto; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatexperienciaslaborales
    ADD CONSTRAINT fk_cod_prospecto FOREIGN KEY (cod_prospecto) REFERENCES sisat.tsisatprospectos(cod_prospecto) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4478 (class 2606 OID 103286)
-- Name: tsisatescolaridad fk_cod_prospecto; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatescolaridad
    ADD CONSTRAINT fk_cod_prospecto FOREIGN KEY (cod_prospecto) REFERENCES sisat.tsisatprospectos(cod_prospecto) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4470 (class 2606 OID 103291)
-- Name: tsisatcursosycertificados fk_cod_prospecto; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcursosycertificados
    ADD CONSTRAINT fk_cod_prospecto FOREIGN KEY (cod_prospecto) REFERENCES sisat.tsisatprospectos(cod_prospecto) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4483 (class 2606 OID 103296)
-- Name: tsisathabilidades fk_cod_prospecto; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisathabilidades
    ADD CONSTRAINT fk_cod_prospecto FOREIGN KEY (cod_prospecto) REFERENCES sisat.tsisatprospectos(cod_prospecto);


--
-- TOC entry 4500 (class 2606 OID 103301)
-- Name: tsisatproyectos fk_cod_prospecto; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatproyectos
    ADD CONSTRAINT fk_cod_prospecto FOREIGN KEY (cod_prospecto) REFERENCES sisat.tsisatprospectos(cod_prospecto) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4430 (class 2606 OID 103306)
-- Name: tsisatasignaciones fk_cod_prospecto; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatasignaciones
    ADD CONSTRAINT fk_cod_prospecto FOREIGN KEY (cod_prospecto) REFERENCES sisat.tsisatprospectos(cod_prospecto) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4473 (class 2606 OID 103311)
-- Name: tsisatentrevistas fk_cod_prospecto; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatentrevistas
    ADD CONSTRAINT fk_cod_prospecto FOREIGN KEY (cod_prospecto) REFERENCES sisat.tsisatprospectos(cod_prospecto);


--
-- TOC entry 4494 (class 2606 OID 103316)
-- Name: tsisatprospectos_idiomas fk_cod_prospecto; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatprospectos_idiomas
    ADD CONSTRAINT fk_cod_prospecto FOREIGN KEY (cod_prospecto) REFERENCES sisat.tsisatprospectos(cod_prospecto);


--
-- TOC entry 4469 (class 2606 OID 103321)
-- Name: tsisatcotizaciones fk_cod_puesto; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcotizaciones
    ADD CONSTRAINT fk_cod_puesto FOREIGN KEY (cod_puesto) REFERENCES sgrh.tsgrhpuestos(cod_puesto);


--
-- TOC entry 4446 (class 2606 OID 103326)
-- Name: tsisatcartaasignacion fk_cod_rhat; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcartaasignacion
    ADD CONSTRAINT fk_cod_rhat FOREIGN KEY (cod_rhta) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4447 (class 2606 OID 103331)
-- Name: tsisatcartaasignacion fk_cod_rhta; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcartaasignacion
    ADD CONSTRAINT fk_cod_rhta FOREIGN KEY (cod_rhta) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4448 (class 2606 OID 103336)
-- Name: tsisatcartaasignacion fk_cod_rys; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcartaasignacion
    ADD CONSTRAINT fk_cod_rys FOREIGN KEY (cod_rys) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4460 (class 2606 OID 103341)
-- Name: tsisatcomentvacantes fk_cod_vacante; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcomentvacantes
    ADD CONSTRAINT fk_cod_vacante FOREIGN KEY (cod_vacante) REFERENCES sisat.tsisatvacantes(cod_vacante);


--
-- TOC entry 4436 (class 2606 OID 103346)
-- Name: tsisatcartaaceptacion fk_creadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcartaaceptacion
    ADD CONSTRAINT fk_creadopor FOREIGN KEY (aud_cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4432 (class 2606 OID 103351)
-- Name: tsisatcandidatos fk_creadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcandidatos
    ADD CONSTRAINT fk_creadopor FOREIGN KEY (aud_cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4437 (class 2606 OID 103356)
-- Name: tsisatcartaaceptacion fk_modificadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcartaaceptacion
    ADD CONSTRAINT fk_modificadopor FOREIGN KEY (aud_cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4433 (class 2606 OID 103361)
-- Name: tsisatcandidatos fk_modificadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcandidatos
    ADD CONSTRAINT fk_modificadopor FOREIGN KEY (aud_cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4881 (class 0 OID 0)
-- Dependencies: 22
-- Name: SCHEMA sgco; Type: ACL; Schema: -; Owner: suite
--

REVOKE ALL ON SCHEMA sgco FROM suite;
GRANT ALL ON SCHEMA sgco TO suite WITH GRANT OPTION;


--
-- TOC entry 4883 (class 0 OID 0)
-- Dependencies: 26
-- Name: SCHEMA sgnom; Type: ACL; Schema: -; Owner: suite
--

REVOKE ALL ON SCHEMA sgnom FROM suite;
GRANT ALL ON SCHEMA sgnom TO suite WITH GRANT OPTION;


--
-- TOC entry 4885 (class 0 OID 0)
-- Dependencies: 13
-- Name: SCHEMA sgrh; Type: ACL; Schema: -; Owner: suite
--

REVOKE ALL ON SCHEMA sgrh FROM suite;
GRANT ALL ON SCHEMA sgrh TO suite WITH GRANT OPTION;


--
-- TOC entry 4889 (class 0 OID 0)
-- Dependencies: 1493
-- Name: TYPE edo_encuesta; Type: ACL; Schema: sgrh; Owner: suite
--

REVOKE ALL ON TYPE sgrh.edo_encuesta FROM suite;
GRANT ALL ON TYPE sgrh.edo_encuesta TO suite WITH GRANT OPTION;


--
-- TOC entry 4890 (class 0 OID 0)
-- Dependencies: 1494
-- Name: TYPE destinatario; Type: ACL; Schema: sgrt; Owner: suite
--

REVOKE ALL ON TYPE sgrt.destinatario FROM suite;
GRANT ALL ON TYPE sgrt.destinatario TO suite WITH GRANT OPTION;


--
-- TOC entry 4891 (class 0 OID 0)
-- Dependencies: 1495
-- Name: TYPE edoticket; Type: ACL; Schema: sgrt; Owner: suite
--

REVOKE ALL ON TYPE sgrt.edoticket FROM suite;
GRANT ALL ON TYPE sgrt.edoticket TO suite WITH GRANT OPTION;


--
-- TOC entry 4892 (class 0 OID 0)
-- Dependencies: 1496
-- Name: TYPE encriptacion; Type: ACL; Schema: sgrt; Owner: suite
--

REVOKE ALL ON TYPE sgrt.encriptacion FROM suite;
GRANT ALL ON TYPE sgrt.encriptacion TO suite WITH GRANT OPTION;


--
-- TOC entry 4893 (class 0 OID 0)
-- Dependencies: 1497
-- Name: TYPE estatus; Type: ACL; Schema: sgrt; Owner: suite
--

REVOKE ALL ON TYPE sgrt.estatus FROM suite;
GRANT ALL ON TYPE sgrt.estatus TO suite WITH GRANT OPTION;


--
-- TOC entry 4894 (class 0 OID 0)
-- Dependencies: 1498
-- Name: TYPE estatus_compromiso; Type: ACL; Schema: sgrt; Owner: suite
--

REVOKE ALL ON TYPE sgrt.estatus_compromiso FROM suite;
GRANT ALL ON TYPE sgrt.estatus_compromiso TO suite WITH GRANT OPTION;


--
-- TOC entry 4895 (class 0 OID 0)
-- Dependencies: 1499
-- Name: TYPE modulo; Type: ACL; Schema: sgrt; Owner: suite
--

REVOKE ALL ON TYPE sgrt.modulo FROM suite;
GRANT ALL ON TYPE sgrt.modulo TO suite WITH GRANT OPTION;


--
-- TOC entry 4896 (class 0 OID 0)
-- Dependencies: 1500
-- Name: TYPE origencontac; Type: ACL; Schema: sgrt; Owner: suite
--

REVOKE ALL ON TYPE sgrt.origencontac FROM suite;
GRANT ALL ON TYPE sgrt.origencontac TO suite WITH GRANT OPTION;


--
-- TOC entry 4897 (class 0 OID 0)
-- Dependencies: 1501
-- Name: TYPE prioridad; Type: ACL; Schema: sgrt; Owner: suite
--

REVOKE ALL ON TYPE sgrt.prioridad FROM suite;
GRANT ALL ON TYPE sgrt.prioridad TO suite WITH GRANT OPTION;


--
-- TOC entry 4898 (class 0 OID 0)
-- Dependencies: 1502
-- Name: TYPE protocolo; Type: ACL; Schema: sgrt; Owner: suite
--

REVOKE ALL ON TYPE sgrt.protocolo FROM suite;
GRANT ALL ON TYPE sgrt.protocolo TO suite WITH GRANT OPTION;


--
-- TOC entry 4899 (class 0 OID 0)
-- Dependencies: 1503
-- Name: TYPE tipo; Type: ACL; Schema: sgrt; Owner: suite
--

REVOKE ALL ON TYPE sgrt.tipo FROM suite;
GRANT ALL ON TYPE sgrt.tipo TO suite WITH GRANT OPTION;


--
-- TOC entry 4900 (class 0 OID 0)
-- Dependencies: 1504
-- Name: TYPE tipo_compromiso; Type: ACL; Schema: sgrt; Owner: suite
--

REVOKE ALL ON TYPE sgrt.tipo_compromiso FROM suite;
GRANT ALL ON TYPE sgrt.tipo_compromiso TO suite WITH GRANT OPTION;


--
-- TOC entry 4901 (class 0 OID 0)
-- Dependencies: 521
-- Name: FUNCTION actualizar_comentarios_incidencia(incidenciaid integer, comentarios text, reporta integer); Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON FUNCTION sgnom.actualizar_comentarios_incidencia(incidenciaid integer, comentarios text, reporta integer) FROM suite;
GRANT ALL ON FUNCTION sgnom.actualizar_comentarios_incidencia(incidenciaid integer, comentarios text, reporta integer) TO suite WITH GRANT OPTION;


--
-- TOC entry 4902 (class 0 OID 0)
-- Dependencies: 515
-- Name: FUNCTION actualizar_importe_incidencia(incidenciaid integer, importe numeric, reporta integer); Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON FUNCTION sgnom.actualizar_importe_incidencia(incidenciaid integer, importe numeric, reporta integer) FROM suite;
GRANT ALL ON FUNCTION sgnom.actualizar_importe_incidencia(incidenciaid integer, importe numeric, reporta integer) TO suite WITH GRANT OPTION;


--
-- TOC entry 4903 (class 0 OID 0)
-- Dependencies: 520
-- Name: FUNCTION actualizar_incidencia(incidenciaid integer, comentarios text, importe numeric, reporta integer); Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON FUNCTION sgnom.actualizar_incidencia(incidenciaid integer, comentarios text, importe numeric, reporta integer) FROM suite;
GRANT ALL ON FUNCTION sgnom.actualizar_incidencia(incidenciaid integer, comentarios text, importe numeric, reporta integer) TO suite WITH GRANT OPTION;


--
-- TOC entry 4904 (class 0 OID 0)
-- Dependencies: 522
-- Name: FUNCTION buscar_detalle_emp(cabecera integer); Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON FUNCTION sgnom.buscar_detalle_emp(cabecera integer) FROM suite;
GRANT ALL ON FUNCTION sgnom.buscar_detalle_emp(cabecera integer) TO suite WITH GRANT OPTION;


--
-- TOC entry 4905 (class 0 OID 0)
-- Dependencies: 523
-- Name: FUNCTION buscar_incidencias_por_empleado(idempleado integer); Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON FUNCTION sgnom.buscar_incidencias_por_empleado(idempleado integer) FROM suite;
GRANT ALL ON FUNCTION sgnom.buscar_incidencias_por_empleado(idempleado integer) TO suite WITH GRANT OPTION;


--
-- TOC entry 4906 (class 0 OID 0)
-- Dependencies: 525
-- Name: FUNCTION detalle_desglose_deduccion(cod_empleado integer, cod_cabecera integer); Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON FUNCTION sgnom.detalle_desglose_deduccion(cod_empleado integer, cod_cabecera integer) FROM suite;
GRANT ALL ON FUNCTION sgnom.detalle_desglose_deduccion(cod_empleado integer, cod_cabecera integer) TO suite WITH GRANT OPTION;


--
-- TOC entry 4907 (class 0 OID 0)
-- Dependencies: 526
-- Name: FUNCTION detalle_desglose_percepcion(cod_empleado integer, cod_cabecera integer); Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON FUNCTION sgnom.detalle_desglose_percepcion(cod_empleado integer, cod_cabecera integer) FROM suite;
GRANT ALL ON FUNCTION sgnom.detalle_desglose_percepcion(cod_empleado integer, cod_cabecera integer) TO suite WITH GRANT OPTION;


--
-- TOC entry 4908 (class 0 OID 0)
-- Dependencies: 527
-- Name: FUNCTION detallespersonal(); Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON FUNCTION sgnom.detallespersonal() FROM suite;
GRANT ALL ON FUNCTION sgnom.detallespersonal() TO suite WITH GRANT OPTION;


--
-- TOC entry 4909 (class 0 OID 0)
-- Dependencies: 508
-- Name: FUNCTION eliminar_incidencia_por_empleado(incidenciaid integer, reporta integer); Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON FUNCTION sgnom.eliminar_incidencia_por_empleado(incidenciaid integer, reporta integer) FROM suite;
GRANT ALL ON FUNCTION sgnom.eliminar_incidencia_por_empleado(incidenciaid integer, reporta integer) TO suite WITH GRANT OPTION;


--
-- TOC entry 4910 (class 0 OID 0)
-- Dependencies: 528
-- Name: FUNCTION empleado_confpago(cabecera integer, empleado integer); Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON FUNCTION sgnom.empleado_confpago(cabecera integer, empleado integer) FROM suite;
GRANT ALL ON FUNCTION sgnom.empleado_confpago(cabecera integer, empleado integer) TO suite WITH GRANT OPTION;


--
-- TOC entry 4911 (class 0 OID 0)
-- Dependencies: 529
-- Name: FUNCTION empleados_por_cabecera(idcabecera integer); Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON FUNCTION sgnom.empleados_por_cabecera(idcabecera integer) FROM suite;
GRANT ALL ON FUNCTION sgnom.empleados_por_cabecera(idcabecera integer) TO suite WITH GRANT OPTION;


--
-- TOC entry 4912 (class 0 OID 0)
-- Dependencies: 536
-- Name: FUNCTION fn_calcula_importes_nomina(idnomina integer, vempleados character varying, todos numeric); Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON FUNCTION sgnom.fn_calcula_importes_nomina(idnomina integer, vempleados character varying, todos numeric) FROM suite;
GRANT ALL ON FUNCTION sgnom.fn_calcula_importes_nomina(idnomina integer, vempleados character varying, todos numeric) TO suite WITH GRANT OPTION;


--
-- TOC entry 4913 (class 0 OID 0)
-- Dependencies: 538
-- Name: FUNCTION fn_calcula_nomina(vidnomina integer); Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON FUNCTION sgnom.fn_calcula_nomina(vidnomina integer) FROM suite;
GRANT ALL ON FUNCTION sgnom.fn_calcula_nomina(vidnomina integer) TO suite WITH GRANT OPTION;


--
-- TOC entry 4914 (class 0 OID 0)
-- Dependencies: 537
-- Name: FUNCTION fn_dias_laborados(empid integer, prmnumquincenacalculo integer); Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON FUNCTION sgnom.fn_dias_laborados(empid integer, prmnumquincenacalculo integer) FROM suite;
GRANT ALL ON FUNCTION sgnom.fn_dias_laborados(empid integer, prmnumquincenacalculo integer) TO suite WITH GRANT OPTION;


--
-- TOC entry 4915 (class 0 OID 0)
-- Dependencies: 539
-- Name: FUNCTION fn_insertacabecera(quincena integer, tipo integer, nombre character varying, credopor integer); Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON FUNCTION sgnom.fn_insertacabecera(quincena integer, tipo integer, nombre character varying, credopor integer) FROM suite;
GRANT ALL ON FUNCTION sgnom.fn_insertacabecera(quincena integer, tipo integer, nombre character varying, credopor integer) TO suite WITH GRANT OPTION;


--
-- TOC entry 4916 (class 0 OID 0)
-- Dependencies: 524
-- Name: FUNCTION fn_sueldo_base(empid integer); Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON FUNCTION sgnom.fn_sueldo_base(empid integer) FROM suite;
GRANT ALL ON FUNCTION sgnom.fn_sueldo_base(empid integer) TO suite WITH GRANT OPTION;


--
-- TOC entry 4917 (class 0 OID 0)
-- Dependencies: 540
-- Name: FUNCTION fn_validapagosnomina(cabecera integer); Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON FUNCTION sgnom.fn_validapagosnomina(cabecera integer) FROM suite;
GRANT ALL ON FUNCTION sgnom.fn_validapagosnomina(cabecera integer) TO suite WITH GRANT OPTION;


--
-- TOC entry 4918 (class 0 OID 0)
-- Dependencies: 541
-- Name: FUNCTION historialquincenasemp(empleado integer); Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON FUNCTION sgnom.historialquincenasemp(empleado integer) FROM suite;
GRANT ALL ON FUNCTION sgnom.historialquincenasemp(empleado integer) TO suite WITH GRANT OPTION;


--
-- TOC entry 4919 (class 0 OID 0)
-- Dependencies: 542
-- Name: FUNCTION incidencias_por_area(area integer); Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON FUNCTION sgnom.incidencias_por_area(area integer) FROM suite;
GRANT ALL ON FUNCTION sgnom.incidencias_por_area(area integer) TO suite WITH GRANT OPTION;


--
-- TOC entry 4920 (class 0 OID 0)
-- Dependencies: 544
-- Name: FUNCTION insertar_incidencia_por_empleado(incidenciaid integer, cantidad integer, actividad character varying, comentarios text, reporta integer, crea integer, monto numeric, fechas character varying); Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON FUNCTION sgnom.insertar_incidencia_por_empleado(incidenciaid integer, cantidad integer, actividad character varying, comentarios text, reporta integer, crea integer, monto numeric, fechas character varying) FROM suite;
GRANT ALL ON FUNCTION sgnom.insertar_incidencia_por_empleado(incidenciaid integer, cantidad integer, actividad character varying, comentarios text, reporta integer, crea integer, monto numeric, fechas character varying) TO suite WITH GRANT OPTION;


--
-- TOC entry 4921 (class 0 OID 0)
-- Dependencies: 545
-- Name: FUNCTION totalimpcab(id integer); Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON FUNCTION sgnom.totalimpcab(id integer) FROM suite;
GRANT ALL ON FUNCTION sgnom.totalimpcab(id integer) TO suite WITH GRANT OPTION;


--
-- TOC entry 4922 (class 0 OID 0)
-- Dependencies: 563
-- Name: FUNCTION validaraltas(); Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON FUNCTION sgnom.validaraltas() FROM suite;
GRANT ALL ON FUNCTION sgnom.validaraltas() TO suite WITH GRANT OPTION;


--
-- TOC entry 4923 (class 0 OID 0)
-- Dependencies: 564
-- Name: FUNCTION validarbajas(); Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON FUNCTION sgnom.validarbajas() FROM suite;
GRANT ALL ON FUNCTION sgnom.validarbajas() TO suite WITH GRANT OPTION;


--
-- TOC entry 4924 (class 0 OID 0)
-- Dependencies: 495
-- Name: FUNCTION buscar_plan(planid integer); Type: ACL; Schema: sgrh; Owner: suite
--

REVOKE ALL ON FUNCTION sgrh.buscar_plan(planid integer) FROM suite;
GRANT ALL ON FUNCTION sgrh.buscar_plan(planid integer) TO suite WITH GRANT OPTION;


--
-- TOC entry 4925 (class 0 OID 0)
-- Dependencies: 496
-- Name: FUNCTION crosstab_report_encuesta(integer); Type: ACL; Schema: sgrh; Owner: suite
--

REVOKE ALL ON FUNCTION sgrh.crosstab_report_encuesta(integer) FROM suite;
GRANT ALL ON FUNCTION sgrh.crosstab_report_encuesta(integer) TO suite WITH GRANT OPTION;


--
-- TOC entry 4926 (class 0 OID 0)
-- Dependencies: 491
-- Name: FUNCTION factualizarfecha(); Type: ACL; Schema: sgrh; Owner: suite
--

REVOKE ALL ON FUNCTION sgrh.factualizarfecha() FROM suite;
GRANT ALL ON FUNCTION sgrh.factualizarfecha() TO suite WITH GRANT OPTION;


--
-- TOC entry 4927 (class 0 OID 0)
-- Dependencies: 497
-- Name: FUNCTION buscar_asistentes_minuta(reunionid integer); Type: ACL; Schema: sgrt; Owner: suite
--

REVOKE ALL ON FUNCTION sgrt.buscar_asistentes_minuta(reunionid integer) FROM suite;
GRANT ALL ON FUNCTION sgrt.buscar_asistentes_minuta(reunionid integer) TO suite WITH GRANT OPTION;


--
-- TOC entry 4928 (class 0 OID 0)
-- Dependencies: 498
-- Name: FUNCTION buscar_compromisos_roles_list(reunionid integer); Type: ACL; Schema: sgrt; Owner: suite
--

REVOKE ALL ON FUNCTION sgrt.buscar_compromisos_roles_list(reunionid integer) FROM suite;
GRANT ALL ON FUNCTION sgrt.buscar_compromisos_roles_list(reunionid integer) TO suite WITH GRANT OPTION;


--
-- TOC entry 4929 (class 0 OID 0)
-- Dependencies: 499
-- Name: FUNCTION buscar_minutas_fechas(fecha_inicio text, fecha_fin text); Type: ACL; Schema: sgrt; Owner: suite
--

REVOKE ALL ON FUNCTION sgrt.buscar_minutas_fechas(fecha_inicio text, fecha_fin text) FROM suite;
GRANT ALL ON FUNCTION sgrt.buscar_minutas_fechas(fecha_inicio text, fecha_fin text) TO suite WITH GRANT OPTION;


--
-- TOC entry 4930 (class 0 OID 0)
-- Dependencies: 500
-- Name: FUNCTION buscar_proxima_reunion(reunionid integer, OUT cod_reunion integer, OUT des_nombre_reunion character varying, OUT fec_fecha text, OUT cod_lugar integer, OUT tim_hora character varying, OUT des_nombre_lugar character varying, OUT cod_ciudad integer, OUT des_nbciudad character varying, OUT cod_estadorep_ciudad integer, OUT des_nbestado character varying, OUT cod_estadorep_estado integer); Type: ACL; Schema: sgrt; Owner: suite
--

REVOKE ALL ON FUNCTION sgrt.buscar_proxima_reunion(reunionid integer, OUT cod_reunion integer, OUT des_nombre_reunion character varying, OUT fec_fecha text, OUT cod_lugar integer, OUT tim_hora character varying, OUT des_nombre_lugar character varying, OUT cod_ciudad integer, OUT des_nbciudad character varying, OUT cod_estadorep_ciudad integer, OUT des_nbestado character varying, OUT cod_estadorep_estado integer) FROM suite;
GRANT ALL ON FUNCTION sgrt.buscar_proxima_reunion(reunionid integer, OUT cod_reunion integer, OUT des_nombre_reunion character varying, OUT fec_fecha text, OUT cod_lugar integer, OUT tim_hora character varying, OUT des_nombre_lugar character varying, OUT cod_ciudad integer, OUT des_nbciudad character varying, OUT cod_estadorep_ciudad integer, OUT des_nbestado character varying, OUT cod_estadorep_estado integer) TO suite WITH GRANT OPTION;


--
-- TOC entry 4931 (class 0 OID 0)
-- Dependencies: 501
-- Name: FUNCTION compromisos_areas_fechas(fecha_inicio text, fecha_fin text); Type: ACL; Schema: sgrt; Owner: suite
--

REVOKE ALL ON FUNCTION sgrt.compromisos_areas_fechas(fecha_inicio text, fecha_fin text) FROM suite;
GRANT ALL ON FUNCTION sgrt.compromisos_areas_fechas(fecha_inicio text, fecha_fin text) TO suite WITH GRANT OPTION;


--
-- TOC entry 4932 (class 0 OID 0)
-- Dependencies: 502
-- Name: FUNCTION compromisos_dia(fechacompromiso text); Type: ACL; Schema: sgrt; Owner: suite
--

REVOKE ALL ON FUNCTION sgrt.compromisos_dia(fechacompromiso text) FROM suite;
GRANT ALL ON FUNCTION sgrt.compromisos_dia(fechacompromiso text) TO suite WITH GRANT OPTION;


--
-- TOC entry 4933 (class 0 OID 0)
-- Dependencies: 503
-- Name: FUNCTION compromisos_generales(); Type: ACL; Schema: sgrt; Owner: suite
--

REVOKE ALL ON FUNCTION sgrt.compromisos_generales() FROM suite;
GRANT ALL ON FUNCTION sgrt.compromisos_generales() TO suite WITH GRANT OPTION;


--
-- TOC entry 4934 (class 0 OID 0)
-- Dependencies: 504
-- Name: FUNCTION reporte_por_tema(reunionid integer); Type: ACL; Schema: sgrt; Owner: suite
--

REVOKE ALL ON FUNCTION sgrt.reporte_por_tema(reunionid integer) FROM suite;
GRANT ALL ON FUNCTION sgrt.reporte_por_tema(reunionid integer) TO suite WITH GRANT OPTION;


--
-- TOC entry 4935 (class 0 OID 0)
-- Dependencies: 583
-- Name: FUNCTION buscar_asignacion_recurso(asignacion_cod integer); Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON FUNCTION sisat.buscar_asignacion_recurso(asignacion_cod integer) FROM suite;
GRANT ALL ON FUNCTION sisat.buscar_asignacion_recurso(asignacion_cod integer) TO suite WITH GRANT OPTION;


--
-- TOC entry 4936 (class 0 OID 0)
-- Dependencies: 218
-- Name: TABLE rtsueldobase; Type: ACL; Schema: public; Owner: suite
--

REVOKE ALL ON TABLE public.rtsueldobase FROM suite;
GRANT ALL ON TABLE public.rtsueldobase TO suite WITH GRANT OPTION;


--
-- TOC entry 4937 (class 0 OID 0)
-- Dependencies: 219
-- Name: SEQUENCE seq_sistema; Type: ACL; Schema: sgco; Owner: suite
--

REVOKE ALL ON SEQUENCE sgco.seq_sistema FROM suite;
GRANT UPDATE ON SEQUENCE sgco.seq_sistema TO suite;
GRANT SELECT,USAGE ON SEQUENCE sgco.seq_sistema TO suite WITH GRANT OPTION;


--
-- TOC entry 4938 (class 0 OID 0)
-- Dependencies: 220
-- Name: SEQUENCE seq_tipousuario; Type: ACL; Schema: sgco; Owner: suite
--

REVOKE ALL ON SEQUENCE sgco.seq_tipousuario FROM suite;
GRANT UPDATE ON SEQUENCE sgco.seq_tipousuario TO suite;
GRANT SELECT,USAGE ON SEQUENCE sgco.seq_tipousuario TO suite WITH GRANT OPTION;


--
-- TOC entry 4939 (class 0 OID 0)
-- Dependencies: 221
-- Name: SEQUENCE seq_usuarios; Type: ACL; Schema: sgco; Owner: suite
--

REVOKE ALL ON SEQUENCE sgco.seq_usuarios FROM suite;
GRANT UPDATE ON SEQUENCE sgco.seq_usuarios TO suite;
GRANT SELECT,USAGE ON SEQUENCE sgco.seq_usuarios TO suite WITH GRANT OPTION;


--
-- TOC entry 4940 (class 0 OID 0)
-- Dependencies: 222
-- Name: TABLE tsgcosistemas; Type: ACL; Schema: sgco; Owner: suite
--

REVOKE ALL ON TABLE sgco.tsgcosistemas FROM suite;
GRANT ALL ON TABLE sgco.tsgcosistemas TO suite WITH GRANT OPTION;


--
-- TOC entry 4941 (class 0 OID 0)
-- Dependencies: 223
-- Name: TABLE tsgcotipousuario; Type: ACL; Schema: sgco; Owner: suite
--

REVOKE ALL ON TABLE sgco.tsgcotipousuario FROM suite;
GRANT ALL ON TABLE sgco.tsgcotipousuario TO suite WITH GRANT OPTION;


--
-- TOC entry 4942 (class 0 OID 0)
-- Dependencies: 224
-- Name: TABLE tsgcousuarios; Type: ACL; Schema: sgco; Owner: suite
--

REVOKE ALL ON TABLE sgco.tsgcousuarios FROM suite;
GRANT ALL ON TABLE sgco.tsgcousuarios TO suite WITH GRANT OPTION;


--
-- TOC entry 4943 (class 0 OID 0)
-- Dependencies: 367
-- Name: SEQUENCE seq_cabecera; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON SEQUENCE sgnom.seq_cabecera FROM suite;
GRANT UPDATE ON SEQUENCE sgnom.seq_cabecera TO suite;
GRANT SELECT,USAGE ON SEQUENCE sgnom.seq_cabecera TO suite WITH GRANT OPTION;


--
-- TOC entry 4944 (class 0 OID 0)
-- Dependencies: 368
-- Name: SEQUENCE seq_cncptoquinc; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON SEQUENCE sgnom.seq_cncptoquinc FROM suite;
GRANT UPDATE ON SEQUENCE sgnom.seq_cncptoquinc TO suite;
GRANT SELECT,USAGE ON SEQUENCE sgnom.seq_cncptoquinc TO suite WITH GRANT OPTION;


--
-- TOC entry 4945 (class 0 OID 0)
-- Dependencies: 369
-- Name: SEQUENCE seq_confpago; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON SEQUENCE sgnom.seq_confpago FROM suite;
GRANT UPDATE ON SEQUENCE sgnom.seq_confpago TO suite;
GRANT SELECT,USAGE ON SEQUENCE sgnom.seq_confpago TO suite WITH GRANT OPTION;


--
-- TOC entry 4946 (class 0 OID 0)
-- Dependencies: 370
-- Name: SEQUENCE seq_empquincena; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON SEQUENCE sgnom.seq_empquincena FROM suite;
GRANT UPDATE ON SEQUENCE sgnom.seq_empquincena TO suite;
GRANT SELECT,USAGE ON SEQUENCE sgnom.seq_empquincena TO suite WITH GRANT OPTION;


--
-- TOC entry 4947 (class 0 OID 0)
-- Dependencies: 371
-- Name: SEQUENCE seq_incidencia; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON SEQUENCE sgnom.seq_incidencia FROM suite;
GRANT UPDATE ON SEQUENCE sgnom.seq_incidencia TO suite;
GRANT SELECT,USAGE ON SEQUENCE sgnom.seq_incidencia TO suite WITH GRANT OPTION;


--
-- TOC entry 4949 (class 0 OID 0)
-- Dependencies: 373
-- Name: TABLE tsgnomaguinaldo; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnomaguinaldo FROM suite;
GRANT ALL ON TABLE sgnom.tsgnomaguinaldo TO suite WITH GRANT OPTION;


--
-- TOC entry 4950 (class 0 OID 0)
-- Dependencies: 374
-- Name: TABLE tsgnomargumento; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnomargumento FROM suite;
GRANT ALL ON TABLE sgnom.tsgnomargumento TO suite WITH GRANT OPTION;


--
-- TOC entry 4951 (class 0 OID 0)
-- Dependencies: 375
-- Name: TABLE tsgnombitacora; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnombitacora FROM suite;
GRANT ALL ON TABLE sgnom.tsgnombitacora TO suite WITH GRANT OPTION;


--
-- TOC entry 4952 (class 0 OID 0)
-- Dependencies: 376
-- Name: TABLE tsgnomcabecera; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnomcabecera FROM suite;
GRANT ALL ON TABLE sgnom.tsgnomcabecera TO suite WITH GRANT OPTION;


--
-- TOC entry 4953 (class 0 OID 0)
-- Dependencies: 377
-- Name: TABLE tsgnomcabeceraht; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnomcabeceraht FROM suite;
GRANT ALL ON TABLE sgnom.tsgnomcabeceraht TO suite WITH GRANT OPTION;


--
-- TOC entry 4955 (class 0 OID 0)
-- Dependencies: 378
-- Name: TABLE tsgnomcalculo; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnomcalculo FROM suite;
GRANT ALL ON TABLE sgnom.tsgnomcalculo TO suite WITH GRANT OPTION;


--
-- TOC entry 4957 (class 0 OID 0)
-- Dependencies: 379
-- Name: TABLE tsgnomcatincidencia; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnomcatincidencia FROM suite;
GRANT ALL ON TABLE sgnom.tsgnomcatincidencia TO suite WITH GRANT OPTION;


--
-- TOC entry 4958 (class 0 OID 0)
-- Dependencies: 380
-- Name: TABLE tsgnomclasificador; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnomclasificador FROM suite;
GRANT ALL ON TABLE sgnom.tsgnomclasificador TO suite WITH GRANT OPTION;


--
-- TOC entry 4959 (class 0 OID 0)
-- Dependencies: 381
-- Name: TABLE tsgnomcncptoquinc; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnomcncptoquinc FROM suite;
GRANT ALL ON TABLE sgnom.tsgnomcncptoquinc TO suite WITH GRANT OPTION;


--
-- TOC entry 4960 (class 0 OID 0)
-- Dependencies: 382
-- Name: TABLE tsgnomcncptoquincht; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnomcncptoquincht FROM suite;
GRANT ALL ON TABLE sgnom.tsgnomcncptoquincht TO suite WITH GRANT OPTION;


--
-- TOC entry 4962 (class 0 OID 0)
-- Dependencies: 383
-- Name: TABLE tsgnomconcepto; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnomconcepto FROM suite;
GRANT ALL ON TABLE sgnom.tsgnomconcepto TO suite WITH GRANT OPTION;


--
-- TOC entry 4964 (class 0 OID 0)
-- Dependencies: 384
-- Name: TABLE tsgnomconceptosat; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnomconceptosat FROM suite;
GRANT ALL ON TABLE sgnom.tsgnomconceptosat TO suite WITH GRANT OPTION;


--
-- TOC entry 4966 (class 0 OID 0)
-- Dependencies: 385
-- Name: TABLE tsgnomconfpago; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnomconfpago FROM suite;
GRANT ALL ON TABLE sgnom.tsgnomconfpago TO suite WITH GRANT OPTION;


--
-- TOC entry 4967 (class 0 OID 0)
-- Dependencies: 386
-- Name: TABLE tsgnomejercicio; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnomejercicio FROM suite;
GRANT ALL ON TABLE sgnom.tsgnomejercicio TO suite WITH GRANT OPTION;


--
-- TOC entry 4969 (class 0 OID 0)
-- Dependencies: 387
-- Name: TABLE tsgnomempleados; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnomempleados FROM suite;
GRANT ALL ON TABLE sgnom.tsgnomempleados TO suite WITH GRANT OPTION;


--
-- TOC entry 4971 (class 0 OID 0)
-- Dependencies: 388
-- Name: TABLE tsgnomempquincena; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnomempquincena FROM suite;
GRANT ALL ON TABLE sgnom.tsgnomempquincena TO suite WITH GRANT OPTION;


--
-- TOC entry 4972 (class 0 OID 0)
-- Dependencies: 389
-- Name: TABLE tsgnomempquincenaht; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnomempquincenaht FROM suite;
GRANT ALL ON TABLE sgnom.tsgnomempquincenaht TO suite WITH GRANT OPTION;


--
-- TOC entry 4974 (class 0 OID 0)
-- Dependencies: 390
-- Name: TABLE tsgnomestatusnom; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnomestatusnom FROM suite;
GRANT ALL ON TABLE sgnom.tsgnomestatusnom TO suite WITH GRANT OPTION;


--
-- TOC entry 4976 (class 0 OID 0)
-- Dependencies: 391
-- Name: TABLE tsgnomformula; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnomformula FROM suite;
GRANT ALL ON TABLE sgnom.tsgnomformula TO suite WITH GRANT OPTION;


--
-- TOC entry 4977 (class 0 OID 0)
-- Dependencies: 392
-- Name: TABLE tsgnomfuncion; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnomfuncion FROM suite;
GRANT ALL ON TABLE sgnom.tsgnomfuncion TO suite WITH GRANT OPTION;


--
-- TOC entry 4978 (class 0 OID 0)
-- Dependencies: 393
-- Name: TABLE tsgnomhisttabla; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnomhisttabla FROM suite;
GRANT ALL ON TABLE sgnom.tsgnomhisttabla TO suite WITH GRANT OPTION;


--
-- TOC entry 4982 (class 0 OID 0)
-- Dependencies: 394
-- Name: TABLE tsgnomincidencia; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnomincidencia FROM suite;
GRANT ALL ON TABLE sgnom.tsgnomincidencia TO suite WITH GRANT OPTION;


--
-- TOC entry 4983 (class 0 OID 0)
-- Dependencies: 395
-- Name: TABLE tsgnommanterceros; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnommanterceros FROM suite;
GRANT ALL ON TABLE sgnom.tsgnommanterceros TO suite WITH GRANT OPTION;


--
-- TOC entry 4984 (class 0 OID 0)
-- Dependencies: 396
-- Name: TABLE tsgnomquincena; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnomquincena FROM suite;
GRANT ALL ON TABLE sgnom.tsgnomquincena TO suite WITH GRANT OPTION;


--
-- TOC entry 4986 (class 0 OID 0)
-- Dependencies: 397
-- Name: TABLE tsgnomtipoconcepto; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnomtipoconcepto FROM suite;
GRANT ALL ON TABLE sgnom.tsgnomtipoconcepto TO suite WITH GRANT OPTION;


--
-- TOC entry 4988 (class 0 OID 0)
-- Dependencies: 398
-- Name: TABLE tsgnomtiponomina; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnomtiponomina FROM suite;
GRANT ALL ON TABLE sgnom.tsgnomtiponomina TO suite WITH GRANT OPTION;


--
-- TOC entry 4989 (class 0 OID 0)
-- Dependencies: 225
-- Name: SEQUENCE seq_area; Type: ACL; Schema: sgrh; Owner: suite
--

REVOKE ALL ON SEQUENCE sgrh.seq_area FROM suite;
GRANT UPDATE ON SEQUENCE sgrh.seq_area TO suite;
GRANT SELECT,USAGE ON SEQUENCE sgrh.seq_area TO suite WITH GRANT OPTION;


--
-- TOC entry 4990 (class 0 OID 0)
-- Dependencies: 226
-- Name: SEQUENCE seq_asignacion_encuesta; Type: ACL; Schema: sgrh; Owner: suite
--

REVOKE ALL ON SEQUENCE sgrh.seq_asignacion_encuesta FROM suite;
GRANT UPDATE ON SEQUENCE sgrh.seq_asignacion_encuesta TO suite;
GRANT SELECT,USAGE ON SEQUENCE sgrh.seq_asignacion_encuesta TO suite WITH GRANT OPTION;


--
-- TOC entry 4991 (class 0 OID 0)
-- Dependencies: 227
-- Name: SEQUENCE seq_asignacionesemp; Type: ACL; Schema: sgrh; Owner: suite
--

REVOKE ALL ON SEQUENCE sgrh.seq_asignacionesemp FROM suite;
GRANT UPDATE ON SEQUENCE sgrh.seq_asignacionesemp TO suite;
GRANT SELECT,USAGE ON SEQUENCE sgrh.seq_asignacionesemp TO suite WITH GRANT OPTION;


--
-- TOC entry 4992 (class 0 OID 0)
-- Dependencies: 228
-- Name: SEQUENCE seq_capacitaciones; Type: ACL; Schema: sgrh; Owner: suite
--

REVOKE ALL ON SEQUENCE sgrh.seq_capacitaciones FROM suite;
GRANT UPDATE ON SEQUENCE sgrh.seq_capacitaciones TO suite;
GRANT SELECT,USAGE ON SEQUENCE sgrh.seq_capacitaciones TO suite WITH GRANT OPTION;


--
-- TOC entry 4993 (class 0 OID 0)
-- Dependencies: 229
-- Name: SEQUENCE seq_cartaasignacion; Type: ACL; Schema: sgrh; Owner: suite
--

REVOKE ALL ON SEQUENCE sgrh.seq_cartaasignacion FROM suite;
GRANT UPDATE ON SEQUENCE sgrh.seq_cartaasignacion TO suite;
GRANT SELECT,USAGE ON SEQUENCE sgrh.seq_cartaasignacion TO suite WITH GRANT OPTION;


--
-- TOC entry 4994 (class 0 OID 0)
-- Dependencies: 230
-- Name: SEQUENCE seq_cat_encuesta_participantes; Type: ACL; Schema: sgrh; Owner: suite
--

REVOKE ALL ON SEQUENCE sgrh.seq_cat_encuesta_participantes FROM suite;
GRANT UPDATE ON SEQUENCE sgrh.seq_cat_encuesta_participantes TO suite;
GRANT SELECT,USAGE ON SEQUENCE sgrh.seq_cat_encuesta_participantes TO suite WITH GRANT OPTION;


--
-- TOC entry 4995 (class 0 OID 0)
-- Dependencies: 231
-- Name: SEQUENCE seq_catrespuestas; Type: ACL; Schema: sgrh; Owner: suite
--

REVOKE ALL ON SEQUENCE sgrh.seq_catrespuestas FROM suite;
GRANT UPDATE ON SEQUENCE sgrh.seq_catrespuestas TO suite;
GRANT SELECT,USAGE ON SEQUENCE sgrh.seq_catrespuestas TO suite WITH GRANT OPTION;


--
-- TOC entry 4996 (class 0 OID 0)
-- Dependencies: 232
-- Name: SEQUENCE seq_clientes; Type: ACL; Schema: sgrh; Owner: suite
--

REVOKE ALL ON SEQUENCE sgrh.seq_clientes FROM suite;
GRANT UPDATE ON SEQUENCE sgrh.seq_clientes TO suite;
GRANT SELECT,USAGE ON SEQUENCE sgrh.seq_clientes TO suite WITH GRANT OPTION;


--
-- TOC entry 4997 (class 0 OID 0)
-- Dependencies: 233
-- Name: SEQUENCE seq_contrataciones; Type: ACL; Schema: sgrh; Owner: suite
--

REVOKE ALL ON SEQUENCE sgrh.seq_contrataciones FROM suite;
GRANT UPDATE ON SEQUENCE sgrh.seq_contrataciones TO suite;
GRANT SELECT,USAGE ON SEQUENCE sgrh.seq_contrataciones TO suite WITH GRANT OPTION;


--
-- TOC entry 4998 (class 0 OID 0)
-- Dependencies: 234
-- Name: SEQUENCE seq_contratos; Type: ACL; Schema: sgrh; Owner: suite
--

REVOKE ALL ON SEQUENCE sgrh.seq_contratos FROM suite;
GRANT UPDATE ON SEQUENCE sgrh.seq_contratos TO suite;
GRANT SELECT,USAGE ON SEQUENCE sgrh.seq_contratos TO suite WITH GRANT OPTION;


--
-- TOC entry 4999 (class 0 OID 0)
-- Dependencies: 235
-- Name: SEQUENCE seq_empleado; Type: ACL; Schema: sgrh; Owner: suite
--

REVOKE ALL ON SEQUENCE sgrh.seq_empleado FROM suite;
GRANT UPDATE ON SEQUENCE sgrh.seq_empleado TO suite;
GRANT SELECT,USAGE ON SEQUENCE sgrh.seq_empleado TO suite WITH GRANT OPTION;


--
-- TOC entry 5000 (class 0 OID 0)
-- Dependencies: 236
-- Name: SEQUENCE seq_encuestas; Type: ACL; Schema: sgrh; Owner: suite
--

REVOKE ALL ON SEQUENCE sgrh.seq_encuestas FROM suite;
GRANT UPDATE ON SEQUENCE sgrh.seq_encuestas TO suite;
GRANT SELECT,USAGE ON SEQUENCE sgrh.seq_encuestas TO suite WITH GRANT OPTION;


--
-- TOC entry 5001 (class 0 OID 0)
-- Dependencies: 237
-- Name: SEQUENCE seq_escolaridad; Type: ACL; Schema: sgrh; Owner: suite
--

REVOKE ALL ON SEQUENCE sgrh.seq_escolaridad FROM suite;
GRANT UPDATE ON SEQUENCE sgrh.seq_escolaridad TO suite;
GRANT SELECT,USAGE ON SEQUENCE sgrh.seq_escolaridad TO suite WITH GRANT OPTION;


--
-- TOC entry 5002 (class 0 OID 0)
-- Dependencies: 238
-- Name: SEQUENCE seq_estatus; Type: ACL; Schema: sgrh; Owner: suite
--

REVOKE ALL ON SEQUENCE sgrh.seq_estatus FROM suite;
GRANT UPDATE ON SEQUENCE sgrh.seq_estatus TO suite;
GRANT SELECT,USAGE ON SEQUENCE sgrh.seq_estatus TO suite WITH GRANT OPTION;


--
-- TOC entry 5003 (class 0 OID 0)
-- Dependencies: 239
-- Name: SEQUENCE seq_evacapacitacion; Type: ACL; Schema: sgrh; Owner: suite
--

REVOKE ALL ON SEQUENCE sgrh.seq_evacapacitacion FROM suite;
GRANT UPDATE ON SEQUENCE sgrh.seq_evacapacitacion TO suite;
GRANT SELECT,USAGE ON SEQUENCE sgrh.seq_evacapacitacion TO suite WITH GRANT OPTION;


--
-- TOC entry 5004 (class 0 OID 0)
-- Dependencies: 240
-- Name: SEQUENCE seq_evacontestadas; Type: ACL; Schema: sgrh; Owner: suite
--

REVOKE ALL ON SEQUENCE sgrh.seq_evacontestadas FROM suite;
GRANT UPDATE ON SEQUENCE sgrh.seq_evacontestadas TO suite;
GRANT SELECT,USAGE ON SEQUENCE sgrh.seq_evacontestadas TO suite WITH GRANT OPTION;


--
-- TOC entry 5005 (class 0 OID 0)
-- Dependencies: 241
-- Name: SEQUENCE seq_evaluaciones; Type: ACL; Schema: sgrh; Owner: suite
--

REVOKE ALL ON SEQUENCE sgrh.seq_evaluaciones FROM suite;
GRANT UPDATE ON SEQUENCE sgrh.seq_evaluaciones TO suite;
GRANT SELECT,USAGE ON SEQUENCE sgrh.seq_evaluaciones TO suite WITH GRANT OPTION;


--
-- TOC entry 5006 (class 0 OID 0)
-- Dependencies: 242
-- Name: SEQUENCE seq_experiencialab; Type: ACL; Schema: sgrh; Owner: suite
--

REVOKE ALL ON SEQUENCE sgrh.seq_experiencialab FROM suite;
GRANT UPDATE ON SEQUENCE sgrh.seq_experiencialab TO suite;
GRANT SELECT,USAGE ON SEQUENCE sgrh.seq_experiencialab TO suite WITH GRANT OPTION;


--
-- TOC entry 5007 (class 0 OID 0)
-- Dependencies: 243
-- Name: SEQUENCE seq_factoreseva; Type: ACL; Schema: sgrh; Owner: suite
--

REVOKE ALL ON SEQUENCE sgrh.seq_factoreseva FROM suite;
GRANT UPDATE ON SEQUENCE sgrh.seq_factoreseva TO suite;
GRANT SELECT,USAGE ON SEQUENCE sgrh.seq_factoreseva TO suite WITH GRANT OPTION;


--
-- TOC entry 5008 (class 0 OID 0)
-- Dependencies: 244
-- Name: SEQUENCE seq_idiomas; Type: ACL; Schema: sgrh; Owner: suite
--

REVOKE ALL ON SEQUENCE sgrh.seq_idiomas FROM suite;
GRANT UPDATE ON SEQUENCE sgrh.seq_idiomas TO suite;
GRANT SELECT,USAGE ON SEQUENCE sgrh.seq_idiomas TO suite WITH GRANT OPTION;


--
-- TOC entry 5009 (class 0 OID 0)
-- Dependencies: 245
-- Name: SEQUENCE seq_logistica; Type: ACL; Schema: sgrh; Owner: suite
--

REVOKE ALL ON SEQUENCE sgrh.seq_logistica FROM suite;
GRANT UPDATE ON SEQUENCE sgrh.seq_logistica TO suite;
GRANT SELECT,USAGE ON SEQUENCE sgrh.seq_logistica TO suite WITH GRANT OPTION;


--
-- TOC entry 5010 (class 0 OID 0)
-- Dependencies: 246
-- Name: SEQUENCE seq_lugar; Type: ACL; Schema: sgrh; Owner: suite
--

REVOKE ALL ON SEQUENCE sgrh.seq_lugar FROM suite;
GRANT UPDATE ON SEQUENCE sgrh.seq_lugar TO suite;
GRANT SELECT,USAGE ON SEQUENCE sgrh.seq_lugar TO suite WITH GRANT OPTION;


--
-- TOC entry 5011 (class 0 OID 0)
-- Dependencies: 247
-- Name: SEQUENCE seq_modo; Type: ACL; Schema: sgrh; Owner: suite
--

REVOKE ALL ON SEQUENCE sgrh.seq_modo FROM suite;
GRANT UPDATE ON SEQUENCE sgrh.seq_modo TO suite;
GRANT SELECT,USAGE ON SEQUENCE sgrh.seq_modo TO suite WITH GRANT OPTION;


--
-- TOC entry 5012 (class 0 OID 0)
-- Dependencies: 248
-- Name: SEQUENCE seq_perfiles; Type: ACL; Schema: sgrh; Owner: suite
--

REVOKE ALL ON SEQUENCE sgrh.seq_perfiles FROM suite;
GRANT UPDATE ON SEQUENCE sgrh.seq_perfiles TO suite;
GRANT SELECT,USAGE ON SEQUENCE sgrh.seq_perfiles TO suite WITH GRANT OPTION;


--
-- TOC entry 5013 (class 0 OID 0)
-- Dependencies: 249
-- Name: SEQUENCE seq_plancapacitacion; Type: ACL; Schema: sgrh; Owner: suite
--

REVOKE ALL ON SEQUENCE sgrh.seq_plancapacitacion FROM suite;
GRANT UPDATE ON SEQUENCE sgrh.seq_plancapacitacion TO suite;
GRANT SELECT,USAGE ON SEQUENCE sgrh.seq_plancapacitacion TO suite WITH GRANT OPTION;


--
-- TOC entry 5014 (class 0 OID 0)
-- Dependencies: 250
-- Name: SEQUENCE seq_planesoperativos; Type: ACL; Schema: sgrh; Owner: suite
--

REVOKE ALL ON SEQUENCE sgrh.seq_planesoperativos FROM suite;
GRANT UPDATE ON SEQUENCE sgrh.seq_planesoperativos TO suite;
GRANT SELECT,USAGE ON SEQUENCE sgrh.seq_planesoperativos TO suite WITH GRANT OPTION;


--
-- TOC entry 5015 (class 0 OID 0)
-- Dependencies: 251
-- Name: SEQUENCE seq_preguntasenc; Type: ACL; Schema: sgrh; Owner: suite
--

REVOKE ALL ON SEQUENCE sgrh.seq_preguntasenc FROM suite;
GRANT UPDATE ON SEQUENCE sgrh.seq_preguntasenc TO suite;
GRANT SELECT,USAGE ON SEQUENCE sgrh.seq_preguntasenc TO suite WITH GRANT OPTION;


--
-- TOC entry 5016 (class 0 OID 0)
-- Dependencies: 252
-- Name: SEQUENCE seq_preguntaseva; Type: ACL; Schema: sgrh; Owner: suite
--

REVOKE ALL ON SEQUENCE sgrh.seq_preguntaseva FROM suite;
GRANT UPDATE ON SEQUENCE sgrh.seq_preguntaseva TO suite;
GRANT SELECT,USAGE ON SEQUENCE sgrh.seq_preguntaseva TO suite WITH GRANT OPTION;


--
-- TOC entry 5017 (class 0 OID 0)
-- Dependencies: 253
-- Name: SEQUENCE seq_proceso; Type: ACL; Schema: sgrh; Owner: suite
--

REVOKE ALL ON SEQUENCE sgrh.seq_proceso FROM suite;
GRANT UPDATE ON SEQUENCE sgrh.seq_proceso TO suite;
GRANT SELECT,USAGE ON SEQUENCE sgrh.seq_proceso TO suite WITH GRANT OPTION;


--
-- TOC entry 5018 (class 0 OID 0)
-- Dependencies: 254
-- Name: SEQUENCE seq_proveedor; Type: ACL; Schema: sgrh; Owner: suite
--

REVOKE ALL ON SEQUENCE sgrh.seq_proveedor FROM suite;
GRANT UPDATE ON SEQUENCE sgrh.seq_proveedor TO suite;
GRANT SELECT,USAGE ON SEQUENCE sgrh.seq_proveedor TO suite WITH GRANT OPTION;


--
-- TOC entry 5019 (class 0 OID 0)
-- Dependencies: 255
-- Name: SEQUENCE seq_puestos; Type: ACL; Schema: sgrh; Owner: suite
--

REVOKE ALL ON SEQUENCE sgrh.seq_puestos FROM suite;
GRANT UPDATE ON SEQUENCE sgrh.seq_puestos TO suite;
GRANT SELECT,USAGE ON SEQUENCE sgrh.seq_puestos TO suite WITH GRANT OPTION;


--
-- TOC entry 5020 (class 0 OID 0)
-- Dependencies: 256
-- Name: SEQUENCE seq_respuestasenc; Type: ACL; Schema: sgrh; Owner: suite
--

REVOKE ALL ON SEQUENCE sgrh.seq_respuestasenc FROM suite;
GRANT UPDATE ON SEQUENCE sgrh.seq_respuestasenc TO suite;
GRANT SELECT,USAGE ON SEQUENCE sgrh.seq_respuestasenc TO suite WITH GRANT OPTION;


--
-- TOC entry 5021 (class 0 OID 0)
-- Dependencies: 257
-- Name: SEQUENCE seq_respuestaseva; Type: ACL; Schema: sgrh; Owner: suite
--

REVOKE ALL ON SEQUENCE sgrh.seq_respuestaseva FROM suite;
GRANT UPDATE ON SEQUENCE sgrh.seq_respuestaseva TO suite;
GRANT SELECT,USAGE ON SEQUENCE sgrh.seq_respuestaseva TO suite WITH GRANT OPTION;


--
-- TOC entry 5022 (class 0 OID 0)
-- Dependencies: 258
-- Name: SEQUENCE seq_revplanesoperativos; Type: ACL; Schema: sgrh; Owner: suite
--

REVOKE ALL ON SEQUENCE sgrh.seq_revplanesoperativos FROM suite;
GRANT UPDATE ON SEQUENCE sgrh.seq_revplanesoperativos TO suite;
GRANT SELECT,USAGE ON SEQUENCE sgrh.seq_revplanesoperativos TO suite WITH GRANT OPTION;


--
-- TOC entry 5023 (class 0 OID 0)
-- Dependencies: 259
-- Name: SEQUENCE seq_rolempleado; Type: ACL; Schema: sgrh; Owner: suite
--

REVOKE ALL ON SEQUENCE sgrh.seq_rolempleado FROM suite;
GRANT UPDATE ON SEQUENCE sgrh.seq_rolempleado TO suite;
GRANT SELECT,USAGE ON SEQUENCE sgrh.seq_rolempleado TO suite WITH GRANT OPTION;


--
-- TOC entry 5024 (class 0 OID 0)
-- Dependencies: 260
-- Name: SEQUENCE seq_roles; Type: ACL; Schema: sgrh; Owner: suite
--

REVOKE ALL ON SEQUENCE sgrh.seq_roles FROM suite;
GRANT UPDATE ON SEQUENCE sgrh.seq_roles TO suite;
GRANT SELECT,USAGE ON SEQUENCE sgrh.seq_roles TO suite WITH GRANT OPTION;


--
-- TOC entry 5025 (class 0 OID 0)
-- Dependencies: 261
-- Name: SEQUENCE seq_subfactoreseva; Type: ACL; Schema: sgrh; Owner: suite
--

REVOKE ALL ON SEQUENCE sgrh.seq_subfactoreseva FROM suite;
GRANT UPDATE ON SEQUENCE sgrh.seq_subfactoreseva TO suite;
GRANT SELECT,USAGE ON SEQUENCE sgrh.seq_subfactoreseva TO suite WITH GRANT OPTION;


--
-- TOC entry 5026 (class 0 OID 0)
-- Dependencies: 262
-- Name: SEQUENCE seq_tipocapacitacion; Type: ACL; Schema: sgrh; Owner: suite
--

REVOKE ALL ON SEQUENCE sgrh.seq_tipocapacitacion FROM suite;
GRANT UPDATE ON SEQUENCE sgrh.seq_tipocapacitacion TO suite;
GRANT SELECT,USAGE ON SEQUENCE sgrh.seq_tipocapacitacion TO suite WITH GRANT OPTION;


--
-- TOC entry 5027 (class 0 OID 0)
-- Dependencies: 263
-- Name: SEQUENCE seq_tiposcapacitaciones; Type: ACL; Schema: sgrh; Owner: suite
--

REVOKE ALL ON SEQUENCE sgrh.seq_tiposcapacitaciones FROM suite;
GRANT UPDATE ON SEQUENCE sgrh.seq_tiposcapacitaciones TO suite;
GRANT SELECT,USAGE ON SEQUENCE sgrh.seq_tiposcapacitaciones TO suite WITH GRANT OPTION;


--
-- TOC entry 5028 (class 0 OID 0)
-- Dependencies: 264
-- Name: SEQUENCE seq_validaevaluaciones; Type: ACL; Schema: sgrh; Owner: suite
--

REVOKE ALL ON SEQUENCE sgrh.seq_validaevaluaciones FROM suite;
GRANT UPDATE ON SEQUENCE sgrh.seq_validaevaluaciones TO suite;
GRANT SELECT,USAGE ON SEQUENCE sgrh.seq_validaevaluaciones TO suite WITH GRANT OPTION;


--
-- TOC entry 5029 (class 0 OID 0)
-- Dependencies: 265
-- Name: TABLE tsgrhareas; Type: ACL; Schema: sgrh; Owner: suite
--

REVOKE ALL ON TABLE sgrh.tsgrhareas FROM suite;
GRANT ALL ON TABLE sgrh.tsgrhareas TO suite WITH GRANT OPTION;


--
-- TOC entry 5030 (class 0 OID 0)
-- Dependencies: 266
-- Name: TABLE tsgrhasignacion_encuesta; Type: ACL; Schema: sgrh; Owner: suite
--

REVOKE ALL ON TABLE sgrh.tsgrhasignacion_encuesta FROM suite;
GRANT ALL ON TABLE sgrh.tsgrhasignacion_encuesta TO suite WITH GRANT OPTION;


--
-- TOC entry 5031 (class 0 OID 0)
-- Dependencies: 267
-- Name: TABLE tsgrhasignacionesemp; Type: ACL; Schema: sgrh; Owner: suite
--

REVOKE ALL ON TABLE sgrh.tsgrhasignacionesemp FROM suite;
GRANT ALL ON TABLE sgrh.tsgrhasignacionesemp TO suite WITH GRANT OPTION;


--
-- TOC entry 5032 (class 0 OID 0)
-- Dependencies: 268
-- Name: TABLE tsgrhcapacitaciones; Type: ACL; Schema: sgrh; Owner: suite
--

REVOKE ALL ON TABLE sgrh.tsgrhcapacitaciones FROM suite;
GRANT ALL ON TABLE sgrh.tsgrhcapacitaciones TO suite WITH GRANT OPTION;


--
-- TOC entry 5033 (class 0 OID 0)
-- Dependencies: 269
-- Name: TABLE tsgrhcartaasignacion; Type: ACL; Schema: sgrh; Owner: suite
--

REVOKE ALL ON TABLE sgrh.tsgrhcartaasignacion FROM suite;
GRANT ALL ON TABLE sgrh.tsgrhcartaasignacion TO suite WITH GRANT OPTION;


--
-- TOC entry 5034 (class 0 OID 0)
-- Dependencies: 270
-- Name: TABLE tsgrhcatrespuestas; Type: ACL; Schema: sgrh; Owner: suite
--

REVOKE ALL ON TABLE sgrh.tsgrhcatrespuestas FROM suite;
GRANT ALL ON TABLE sgrh.tsgrhcatrespuestas TO suite WITH GRANT OPTION;


--
-- TOC entry 5035 (class 0 OID 0)
-- Dependencies: 271
-- Name: TABLE tsgrhclientes; Type: ACL; Schema: sgrh; Owner: suite
--

REVOKE ALL ON TABLE sgrh.tsgrhclientes FROM suite;
GRANT ALL ON TABLE sgrh.tsgrhclientes TO suite WITH GRANT OPTION;


--
-- TOC entry 5036 (class 0 OID 0)
-- Dependencies: 272
-- Name: TABLE tsgrhcontrataciones; Type: ACL; Schema: sgrh; Owner: suite
--

REVOKE ALL ON TABLE sgrh.tsgrhcontrataciones FROM suite;
GRANT ALL ON TABLE sgrh.tsgrhcontrataciones TO suite WITH GRANT OPTION;


--
-- TOC entry 5037 (class 0 OID 0)
-- Dependencies: 273
-- Name: TABLE tsgrhcontratos; Type: ACL; Schema: sgrh; Owner: suite
--

REVOKE ALL ON TABLE sgrh.tsgrhcontratos FROM suite;
GRANT ALL ON TABLE sgrh.tsgrhcontratos TO suite WITH GRANT OPTION;


--
-- TOC entry 5038 (class 0 OID 0)
-- Dependencies: 274
-- Name: TABLE tsgrhempleados; Type: ACL; Schema: sgrh; Owner: suite
--

REVOKE ALL ON TABLE sgrh.tsgrhempleados FROM suite;
GRANT ALL ON TABLE sgrh.tsgrhempleados TO suite WITH GRANT OPTION;


--
-- TOC entry 5039 (class 0 OID 0)
-- Dependencies: 275
-- Name: TABLE tsgrhencuesta; Type: ACL; Schema: sgrh; Owner: suite
--

REVOKE ALL ON TABLE sgrh.tsgrhencuesta FROM suite;
GRANT ALL ON TABLE sgrh.tsgrhencuesta TO suite WITH GRANT OPTION;


--
-- TOC entry 5040 (class 0 OID 0)
-- Dependencies: 276
-- Name: TABLE tsgrhencuesta_participantes; Type: ACL; Schema: sgrh; Owner: suite
--

REVOKE ALL ON TABLE sgrh.tsgrhencuesta_participantes FROM suite;
GRANT ALL ON TABLE sgrh.tsgrhencuesta_participantes TO suite WITH GRANT OPTION;


--
-- TOC entry 5041 (class 0 OID 0)
-- Dependencies: 277
-- Name: TABLE tsgrhescolaridad; Type: ACL; Schema: sgrh; Owner: suite
--

REVOKE ALL ON TABLE sgrh.tsgrhescolaridad FROM suite;
GRANT ALL ON TABLE sgrh.tsgrhescolaridad TO suite WITH GRANT OPTION;


--
-- TOC entry 5042 (class 0 OID 0)
-- Dependencies: 278
-- Name: TABLE tsgrhestatuscapacitacion; Type: ACL; Schema: sgrh; Owner: suite
--

REVOKE ALL ON TABLE sgrh.tsgrhestatuscapacitacion FROM suite;
GRANT ALL ON TABLE sgrh.tsgrhestatuscapacitacion TO suite WITH GRANT OPTION;


--
-- TOC entry 5043 (class 0 OID 0)
-- Dependencies: 279
-- Name: TABLE tsgrhevacapacitacion; Type: ACL; Schema: sgrh; Owner: suite
--

REVOKE ALL ON TABLE sgrh.tsgrhevacapacitacion FROM suite;
GRANT ALL ON TABLE sgrh.tsgrhevacapacitacion TO suite WITH GRANT OPTION;


--
-- TOC entry 5044 (class 0 OID 0)
-- Dependencies: 280
-- Name: TABLE tsgrhevacontestadas; Type: ACL; Schema: sgrh; Owner: suite
--

REVOKE ALL ON TABLE sgrh.tsgrhevacontestadas FROM suite;
GRANT ALL ON TABLE sgrh.tsgrhevacontestadas TO suite WITH GRANT OPTION;


--
-- TOC entry 5045 (class 0 OID 0)
-- Dependencies: 281
-- Name: TABLE tsgrhevaluaciones; Type: ACL; Schema: sgrh; Owner: suite
--

REVOKE ALL ON TABLE sgrh.tsgrhevaluaciones FROM suite;
GRANT ALL ON TABLE sgrh.tsgrhevaluaciones TO suite WITH GRANT OPTION;


--
-- TOC entry 5046 (class 0 OID 0)
-- Dependencies: 282
-- Name: TABLE tsgrhexperienciaslaborales; Type: ACL; Schema: sgrh; Owner: suite
--

REVOKE ALL ON TABLE sgrh.tsgrhexperienciaslaborales FROM suite;
GRANT ALL ON TABLE sgrh.tsgrhexperienciaslaborales TO suite WITH GRANT OPTION;


--
-- TOC entry 5047 (class 0 OID 0)
-- Dependencies: 283
-- Name: TABLE tsgrhfactoreseva; Type: ACL; Schema: sgrh; Owner: suite
--

REVOKE ALL ON TABLE sgrh.tsgrhfactoreseva FROM suite;
GRANT ALL ON TABLE sgrh.tsgrhfactoreseva TO suite WITH GRANT OPTION;


--
-- TOC entry 5048 (class 0 OID 0)
-- Dependencies: 284
-- Name: TABLE tsgrhidiomas; Type: ACL; Schema: sgrh; Owner: suite
--

REVOKE ALL ON TABLE sgrh.tsgrhidiomas FROM suite;
GRANT ALL ON TABLE sgrh.tsgrhidiomas TO suite WITH GRANT OPTION;


--
-- TOC entry 5049 (class 0 OID 0)
-- Dependencies: 285
-- Name: TABLE tsgrhlogistica; Type: ACL; Schema: sgrh; Owner: suite
--

REVOKE ALL ON TABLE sgrh.tsgrhlogistica FROM suite;
GRANT ALL ON TABLE sgrh.tsgrhlogistica TO suite WITH GRANT OPTION;


--
-- TOC entry 5050 (class 0 OID 0)
-- Dependencies: 286
-- Name: TABLE tsgrhmodo; Type: ACL; Schema: sgrh; Owner: suite
--

REVOKE ALL ON TABLE sgrh.tsgrhmodo FROM suite;
GRANT ALL ON TABLE sgrh.tsgrhmodo TO suite WITH GRANT OPTION;


--
-- TOC entry 5051 (class 0 OID 0)
-- Dependencies: 287
-- Name: TABLE tsgrhperfiles; Type: ACL; Schema: sgrh; Owner: suite
--

REVOKE ALL ON TABLE sgrh.tsgrhperfiles FROM suite;
GRANT ALL ON TABLE sgrh.tsgrhperfiles TO suite WITH GRANT OPTION;


--
-- TOC entry 5052 (class 0 OID 0)
-- Dependencies: 288
-- Name: TABLE tsgrhplancapacitacion; Type: ACL; Schema: sgrh; Owner: suite
--

REVOKE ALL ON TABLE sgrh.tsgrhplancapacitacion FROM suite;
GRANT ALL ON TABLE sgrh.tsgrhplancapacitacion TO suite WITH GRANT OPTION;


--
-- TOC entry 5053 (class 0 OID 0)
-- Dependencies: 289
-- Name: TABLE tsgrhplanoperativo; Type: ACL; Schema: sgrh; Owner: suite
--

REVOKE ALL ON TABLE sgrh.tsgrhplanoperativo FROM suite;
GRANT ALL ON TABLE sgrh.tsgrhplanoperativo TO suite WITH GRANT OPTION;


--
-- TOC entry 5054 (class 0 OID 0)
-- Dependencies: 290
-- Name: TABLE tsgrhpreguntasenc; Type: ACL; Schema: sgrh; Owner: suite
--

REVOKE ALL ON TABLE sgrh.tsgrhpreguntasenc FROM suite;
GRANT ALL ON TABLE sgrh.tsgrhpreguntasenc TO suite WITH GRANT OPTION;


--
-- TOC entry 5055 (class 0 OID 0)
-- Dependencies: 291
-- Name: TABLE tsgrhpreguntaseva; Type: ACL; Schema: sgrh; Owner: suite
--

REVOKE ALL ON TABLE sgrh.tsgrhpreguntaseva FROM suite;
GRANT ALL ON TABLE sgrh.tsgrhpreguntaseva TO suite WITH GRANT OPTION;


--
-- TOC entry 5056 (class 0 OID 0)
-- Dependencies: 292
-- Name: TABLE tsgrhprocesos; Type: ACL; Schema: sgrh; Owner: suite
--

REVOKE ALL ON TABLE sgrh.tsgrhprocesos FROM suite;
GRANT ALL ON TABLE sgrh.tsgrhprocesos TO suite WITH GRANT OPTION;


--
-- TOC entry 5057 (class 0 OID 0)
-- Dependencies: 293
-- Name: TABLE tsgrhproveedores; Type: ACL; Schema: sgrh; Owner: suite
--

REVOKE ALL ON TABLE sgrh.tsgrhproveedores FROM suite;
GRANT ALL ON TABLE sgrh.tsgrhproveedores TO suite WITH GRANT OPTION;


--
-- TOC entry 5058 (class 0 OID 0)
-- Dependencies: 294
-- Name: TABLE tsgrhpuestos; Type: ACL; Schema: sgrh; Owner: suite
--

REVOKE ALL ON TABLE sgrh.tsgrhpuestos FROM suite;
GRANT ALL ON TABLE sgrh.tsgrhpuestos TO suite WITH GRANT OPTION;


--
-- TOC entry 5059 (class 0 OID 0)
-- Dependencies: 295
-- Name: TABLE tsgrhrelacionroles; Type: ACL; Schema: sgrh; Owner: suite
--

REVOKE ALL ON TABLE sgrh.tsgrhrelacionroles FROM suite;
GRANT ALL ON TABLE sgrh.tsgrhrelacionroles TO suite WITH GRANT OPTION;


--
-- TOC entry 5060 (class 0 OID 0)
-- Dependencies: 296
-- Name: TABLE tsgrhrespuestasenc; Type: ACL; Schema: sgrh; Owner: suite
--

REVOKE ALL ON TABLE sgrh.tsgrhrespuestasenc FROM suite;
GRANT ALL ON TABLE sgrh.tsgrhrespuestasenc TO suite WITH GRANT OPTION;


--
-- TOC entry 5061 (class 0 OID 0)
-- Dependencies: 297
-- Name: TABLE tsgrhrespuestaseva; Type: ACL; Schema: sgrh; Owner: suite
--

REVOKE ALL ON TABLE sgrh.tsgrhrespuestaseva FROM suite;
GRANT ALL ON TABLE sgrh.tsgrhrespuestaseva TO suite WITH GRANT OPTION;


--
-- TOC entry 5062 (class 0 OID 0)
-- Dependencies: 298
-- Name: TABLE tsgrhrevplanoperativo; Type: ACL; Schema: sgrh; Owner: suite
--

REVOKE ALL ON TABLE sgrh.tsgrhrevplanoperativo FROM suite;
GRANT ALL ON TABLE sgrh.tsgrhrevplanoperativo TO suite WITH GRANT OPTION;


--
-- TOC entry 5063 (class 0 OID 0)
-- Dependencies: 299
-- Name: TABLE tsgrhrolempleado; Type: ACL; Schema: sgrh; Owner: suite
--

REVOKE ALL ON TABLE sgrh.tsgrhrolempleado FROM suite;
GRANT ALL ON TABLE sgrh.tsgrhrolempleado TO suite WITH GRANT OPTION;


--
-- TOC entry 5064 (class 0 OID 0)
-- Dependencies: 300
-- Name: TABLE tsgrhroles; Type: ACL; Schema: sgrh; Owner: suite
--

REVOKE ALL ON TABLE sgrh.tsgrhroles FROM suite;
GRANT ALL ON TABLE sgrh.tsgrhroles TO suite WITH GRANT OPTION;


--
-- TOC entry 5065 (class 0 OID 0)
-- Dependencies: 301
-- Name: TABLE tsgrhsubfactoreseva; Type: ACL; Schema: sgrh; Owner: suite
--

REVOKE ALL ON TABLE sgrh.tsgrhsubfactoreseva FROM suite;
GRANT ALL ON TABLE sgrh.tsgrhsubfactoreseva TO suite WITH GRANT OPTION;


--
-- TOC entry 5066 (class 0 OID 0)
-- Dependencies: 302
-- Name: TABLE tsgrhtipocapacitacion; Type: ACL; Schema: sgrh; Owner: suite
--

REVOKE ALL ON TABLE sgrh.tsgrhtipocapacitacion FROM suite;
GRANT ALL ON TABLE sgrh.tsgrhtipocapacitacion TO suite WITH GRANT OPTION;


--
-- TOC entry 5067 (class 0 OID 0)
-- Dependencies: 303
-- Name: TABLE tsgrhvalidaevaluaciondes; Type: ACL; Schema: sgrh; Owner: suite
--

REVOKE ALL ON TABLE sgrh.tsgrhvalidaevaluaciondes FROM suite;
GRANT ALL ON TABLE sgrh.tsgrhvalidaevaluaciondes TO suite WITH GRANT OPTION;


--
-- TOC entry 5068 (class 0 OID 0)
-- Dependencies: 304
-- Name: SEQUENCE seq_agenda; Type: ACL; Schema: sgrt; Owner: suite
--

REVOKE ALL ON SEQUENCE sgrt.seq_agenda FROM suite;
GRANT UPDATE ON SEQUENCE sgrt.seq_agenda TO suite;
GRANT SELECT,USAGE ON SEQUENCE sgrt.seq_agenda TO suite WITH GRANT OPTION;


--
-- TOC entry 5069 (class 0 OID 0)
-- Dependencies: 305
-- Name: SEQUENCE seq_archivo; Type: ACL; Schema: sgrt; Owner: suite
--

REVOKE ALL ON SEQUENCE sgrt.seq_archivo FROM suite;
GRANT UPDATE ON SEQUENCE sgrt.seq_archivo TO suite;
GRANT SELECT,USAGE ON SEQUENCE sgrt.seq_archivo TO suite WITH GRANT OPTION;


--
-- TOC entry 5070 (class 0 OID 0)
-- Dependencies: 306
-- Name: SEQUENCE seq_asistente; Type: ACL; Schema: sgrt; Owner: suite
--

REVOKE ALL ON SEQUENCE sgrt.seq_asistente FROM suite;
GRANT UPDATE ON SEQUENCE sgrt.seq_asistente TO suite;
GRANT SELECT,USAGE ON SEQUENCE sgrt.seq_asistente TO suite WITH GRANT OPTION;


--
-- TOC entry 5071 (class 0 OID 0)
-- Dependencies: 307
-- Name: SEQUENCE seq_attach; Type: ACL; Schema: sgrt; Owner: suite
--

REVOKE ALL ON SEQUENCE sgrt.seq_attach FROM suite;
GRANT UPDATE ON SEQUENCE sgrt.seq_attach TO suite;
GRANT SELECT,USAGE ON SEQUENCE sgrt.seq_attach TO suite WITH GRANT OPTION;


--
-- TOC entry 5072 (class 0 OID 0)
-- Dependencies: 308
-- Name: SEQUENCE seq_categoriafaq; Type: ACL; Schema: sgrt; Owner: suite
--

REVOKE ALL ON SEQUENCE sgrt.seq_categoriafaq FROM suite;
GRANT UPDATE ON SEQUENCE sgrt.seq_categoriafaq TO suite;
GRANT SELECT,USAGE ON SEQUENCE sgrt.seq_categoriafaq TO suite WITH GRANT OPTION;


--
-- TOC entry 5073 (class 0 OID 0)
-- Dependencies: 309
-- Name: SEQUENCE seq_chat; Type: ACL; Schema: sgrt; Owner: suite
--

REVOKE ALL ON SEQUENCE sgrt.seq_chat FROM suite;
GRANT UPDATE ON SEQUENCE sgrt.seq_chat TO suite;
GRANT SELECT,USAGE ON SEQUENCE sgrt.seq_chat TO suite WITH GRANT OPTION;


--
-- TOC entry 5074 (class 0 OID 0)
-- Dependencies: 310
-- Name: SEQUENCE seq_ciudad; Type: ACL; Schema: sgrt; Owner: suite
--

REVOKE ALL ON SEQUENCE sgrt.seq_ciudad FROM suite;
GRANT UPDATE ON SEQUENCE sgrt.seq_ciudad TO suite;
GRANT SELECT,USAGE ON SEQUENCE sgrt.seq_ciudad TO suite WITH GRANT OPTION;


--
-- TOC entry 5075 (class 0 OID 0)
-- Dependencies: 311
-- Name: SEQUENCE seq_comentsagenda; Type: ACL; Schema: sgrt; Owner: suite
--

REVOKE ALL ON SEQUENCE sgrt.seq_comentsagenda FROM suite;
GRANT UPDATE ON SEQUENCE sgrt.seq_comentsagenda TO suite;
GRANT SELECT,USAGE ON SEQUENCE sgrt.seq_comentsagenda TO suite WITH GRANT OPTION;


--
-- TOC entry 5076 (class 0 OID 0)
-- Dependencies: 312
-- Name: SEQUENCE seq_comentsreunion; Type: ACL; Schema: sgrt; Owner: suite
--

REVOKE ALL ON SEQUENCE sgrt.seq_comentsreunion FROM suite;
GRANT UPDATE ON SEQUENCE sgrt.seq_comentsreunion TO suite;
GRANT SELECT,USAGE ON SEQUENCE sgrt.seq_comentsreunion TO suite WITH GRANT OPTION;


--
-- TOC entry 5077 (class 0 OID 0)
-- Dependencies: 313
-- Name: SEQUENCE seq_compromiso; Type: ACL; Schema: sgrt; Owner: suite
--

REVOKE ALL ON SEQUENCE sgrt.seq_compromiso FROM suite;
GRANT UPDATE ON SEQUENCE sgrt.seq_compromiso TO suite;
GRANT SELECT,USAGE ON SEQUENCE sgrt.seq_compromiso TO suite WITH GRANT OPTION;


--
-- TOC entry 5078 (class 0 OID 0)
-- Dependencies: 314
-- Name: SEQUENCE seq_contacto; Type: ACL; Schema: sgrt; Owner: suite
--

REVOKE ALL ON SEQUENCE sgrt.seq_contacto FROM suite;
GRANT UPDATE ON SEQUENCE sgrt.seq_contacto TO suite;
GRANT SELECT,USAGE ON SEQUENCE sgrt.seq_contacto TO suite WITH GRANT OPTION;


--
-- TOC entry 5079 (class 0 OID 0)
-- Dependencies: 315
-- Name: SEQUENCE seq_correo; Type: ACL; Schema: sgrt; Owner: suite
--

REVOKE ALL ON SEQUENCE sgrt.seq_correo FROM suite;
GRANT UPDATE ON SEQUENCE sgrt.seq_correo TO suite;
GRANT SELECT,USAGE ON SEQUENCE sgrt.seq_correo TO suite WITH GRANT OPTION;


--
-- TOC entry 5080 (class 0 OID 0)
-- Dependencies: 316
-- Name: SEQUENCE seq_depto; Type: ACL; Schema: sgrt; Owner: suite
--

REVOKE ALL ON SEQUENCE sgrt.seq_depto FROM suite;
GRANT UPDATE ON SEQUENCE sgrt.seq_depto TO suite;
GRANT SELECT,USAGE ON SEQUENCE sgrt.seq_depto TO suite WITH GRANT OPTION;


--
-- TOC entry 5081 (class 0 OID 0)
-- Dependencies: 317
-- Name: SEQUENCE seq_edoacuerdo; Type: ACL; Schema: sgrt; Owner: suite
--

REVOKE ALL ON SEQUENCE sgrt.seq_edoacuerdo FROM suite;
GRANT UPDATE ON SEQUENCE sgrt.seq_edoacuerdo TO suite;
GRANT SELECT,USAGE ON SEQUENCE sgrt.seq_edoacuerdo TO suite WITH GRANT OPTION;


--
-- TOC entry 5082 (class 0 OID 0)
-- Dependencies: 318
-- Name: SEQUENCE seq_elemento; Type: ACL; Schema: sgrt; Owner: suite
--

REVOKE ALL ON SEQUENCE sgrt.seq_elemento FROM suite;
GRANT UPDATE ON SEQUENCE sgrt.seq_elemento TO suite;
GRANT SELECT,USAGE ON SEQUENCE sgrt.seq_elemento TO suite WITH GRANT OPTION;


--
-- TOC entry 5083 (class 0 OID 0)
-- Dependencies: 319
-- Name: SEQUENCE seq_estadorep; Type: ACL; Schema: sgrt; Owner: suite
--

REVOKE ALL ON SEQUENCE sgrt.seq_estadorep FROM suite;
GRANT UPDATE ON SEQUENCE sgrt.seq_estadorep TO suite;
GRANT SELECT,USAGE ON SEQUENCE sgrt.seq_estadorep TO suite WITH GRANT OPTION;


--
-- TOC entry 5084 (class 0 OID 0)
-- Dependencies: 320
-- Name: SEQUENCE seq_faq; Type: ACL; Schema: sgrt; Owner: suite
--

REVOKE ALL ON SEQUENCE sgrt.seq_faq FROM suite;
GRANT UPDATE ON SEQUENCE sgrt.seq_faq TO suite;
GRANT SELECT,USAGE ON SEQUENCE sgrt.seq_faq TO suite WITH GRANT OPTION;


--
-- TOC entry 5085 (class 0 OID 0)
-- Dependencies: 321
-- Name: SEQUENCE seq_grupo; Type: ACL; Schema: sgrt; Owner: suite
--

REVOKE ALL ON SEQUENCE sgrt.seq_grupo FROM suite;
GRANT UPDATE ON SEQUENCE sgrt.seq_grupo TO suite;
GRANT SELECT,USAGE ON SEQUENCE sgrt.seq_grupo TO suite WITH GRANT OPTION;


--
-- TOC entry 5086 (class 0 OID 0)
-- Dependencies: 322
-- Name: SEQUENCE seq_invitado; Type: ACL; Schema: sgrt; Owner: suite
--

REVOKE ALL ON SEQUENCE sgrt.seq_invitado FROM suite;
GRANT UPDATE ON SEQUENCE sgrt.seq_invitado TO suite;
GRANT SELECT,USAGE ON SEQUENCE sgrt.seq_invitado TO suite WITH GRANT OPTION;


--
-- TOC entry 5087 (class 0 OID 0)
-- Dependencies: 323
-- Name: SEQUENCE seq_lugar; Type: ACL; Schema: sgrt; Owner: suite
--

REVOKE ALL ON SEQUENCE sgrt.seq_lugar FROM suite;
GRANT UPDATE ON SEQUENCE sgrt.seq_lugar TO suite;
GRANT SELECT,USAGE ON SEQUENCE sgrt.seq_lugar TO suite WITH GRANT OPTION;


--
-- TOC entry 5088 (class 0 OID 0)
-- Dependencies: 324
-- Name: SEQUENCE seq_mensaje; Type: ACL; Schema: sgrt; Owner: suite
--

REVOKE ALL ON SEQUENCE sgrt.seq_mensaje FROM suite;
GRANT UPDATE ON SEQUENCE sgrt.seq_mensaje TO suite;
GRANT SELECT,USAGE ON SEQUENCE sgrt.seq_mensaje TO suite WITH GRANT OPTION;


--
-- TOC entry 5089 (class 0 OID 0)
-- Dependencies: 325
-- Name: SEQUENCE seq_nota; Type: ACL; Schema: sgrt; Owner: suite
--

REVOKE ALL ON SEQUENCE sgrt.seq_nota FROM suite;
GRANT UPDATE ON SEQUENCE sgrt.seq_nota TO suite;
GRANT SELECT,USAGE ON SEQUENCE sgrt.seq_nota TO suite WITH GRANT OPTION;


--
-- TOC entry 5090 (class 0 OID 0)
-- Dependencies: 326
-- Name: SEQUENCE seq_plantillacorreo; Type: ACL; Schema: sgrt; Owner: suite
--

REVOKE ALL ON SEQUENCE sgrt.seq_plantillacorreo FROM suite;
GRANT UPDATE ON SEQUENCE sgrt.seq_plantillacorreo TO suite;
GRANT SELECT,USAGE ON SEQUENCE sgrt.seq_plantillacorreo TO suite WITH GRANT OPTION;


--
-- TOC entry 5091 (class 0 OID 0)
-- Dependencies: 327
-- Name: SEQUENCE seq_prioridad; Type: ACL; Schema: sgrt; Owner: suite
--

REVOKE ALL ON SEQUENCE sgrt.seq_prioridad FROM suite;
GRANT UPDATE ON SEQUENCE sgrt.seq_prioridad TO suite;
GRANT SELECT,USAGE ON SEQUENCE sgrt.seq_prioridad TO suite WITH GRANT OPTION;


--
-- TOC entry 5092 (class 0 OID 0)
-- Dependencies: 328
-- Name: SEQUENCE seq_resp; Type: ACL; Schema: sgrt; Owner: suite
--

REVOKE ALL ON SEQUENCE sgrt.seq_resp FROM suite;
GRANT UPDATE ON SEQUENCE sgrt.seq_resp TO suite;
GRANT SELECT,USAGE ON SEQUENCE sgrt.seq_resp TO suite WITH GRANT OPTION;


--
-- TOC entry 5093 (class 0 OID 0)
-- Dependencies: 329
-- Name: SEQUENCE seq_respuesta; Type: ACL; Schema: sgrt; Owner: suite
--

REVOKE ALL ON SEQUENCE sgrt.seq_respuesta FROM suite;
GRANT UPDATE ON SEQUENCE sgrt.seq_respuesta TO suite;
GRANT SELECT,USAGE ON SEQUENCE sgrt.seq_respuesta TO suite WITH GRANT OPTION;


--
-- TOC entry 5094 (class 0 OID 0)
-- Dependencies: 330
-- Name: SEQUENCE seq_respuestas_participantes; Type: ACL; Schema: sgrt; Owner: suite
--

REVOKE ALL ON SEQUENCE sgrt.seq_respuestas_participantes FROM suite;
GRANT UPDATE ON SEQUENCE sgrt.seq_respuestas_participantes TO suite;
GRANT SELECT,USAGE ON SEQUENCE sgrt.seq_respuestas_participantes TO suite WITH GRANT OPTION;


--
-- TOC entry 5095 (class 0 OID 0)
-- Dependencies: 331
-- Name: SEQUENCE seq_reunion; Type: ACL; Schema: sgrt; Owner: suite
--

REVOKE ALL ON SEQUENCE sgrt.seq_reunion FROM suite;
GRANT UPDATE ON SEQUENCE sgrt.seq_reunion TO suite;
GRANT SELECT,USAGE ON SEQUENCE sgrt.seq_reunion TO suite WITH GRANT OPTION;


--
-- TOC entry 5096 (class 0 OID 0)
-- Dependencies: 332
-- Name: SEQUENCE seq_servicio; Type: ACL; Schema: sgrt; Owner: suite
--

REVOKE ALL ON SEQUENCE sgrt.seq_servicio FROM suite;
GRANT UPDATE ON SEQUENCE sgrt.seq_servicio TO suite;
GRANT SELECT,USAGE ON SEQUENCE sgrt.seq_servicio TO suite WITH GRANT OPTION;


--
-- TOC entry 5097 (class 0 OID 0)
-- Dependencies: 333
-- Name: SEQUENCE seq_solicitud; Type: ACL; Schema: sgrt; Owner: suite
--

REVOKE ALL ON SEQUENCE sgrt.seq_solicitud FROM suite;
GRANT UPDATE ON SEQUENCE sgrt.seq_solicitud TO suite;
GRANT SELECT,USAGE ON SEQUENCE sgrt.seq_solicitud TO suite WITH GRANT OPTION;


--
-- TOC entry 5098 (class 0 OID 0)
-- Dependencies: 334
-- Name: SEQUENCE seq_ticket; Type: ACL; Schema: sgrt; Owner: suite
--

REVOKE ALL ON SEQUENCE sgrt.seq_ticket FROM suite;
GRANT UPDATE ON SEQUENCE sgrt.seq_ticket TO suite;
GRANT SELECT,USAGE ON SEQUENCE sgrt.seq_ticket TO suite WITH GRANT OPTION;


--
-- TOC entry 5099 (class 0 OID 0)
-- Dependencies: 335
-- Name: SEQUENCE seq_topico; Type: ACL; Schema: sgrt; Owner: suite
--

REVOKE ALL ON SEQUENCE sgrt.seq_topico FROM suite;
GRANT UPDATE ON SEQUENCE sgrt.seq_topico TO suite;
GRANT SELECT,USAGE ON SEQUENCE sgrt.seq_topico TO suite WITH GRANT OPTION;


--
-- TOC entry 5100 (class 0 OID 0)
-- Dependencies: 336
-- Name: TABLE tsgrtagenda; Type: ACL; Schema: sgrt; Owner: suite
--

REVOKE ALL ON TABLE sgrt.tsgrtagenda FROM suite;
GRANT ALL ON TABLE sgrt.tsgrtagenda TO suite WITH GRANT OPTION;


--
-- TOC entry 5101 (class 0 OID 0)
-- Dependencies: 337
-- Name: TABLE tsgrtarchivos; Type: ACL; Schema: sgrt; Owner: suite
--

REVOKE ALL ON TABLE sgrt.tsgrtarchivos FROM suite;
GRANT ALL ON TABLE sgrt.tsgrtarchivos TO suite WITH GRANT OPTION;


--
-- TOC entry 5102 (class 0 OID 0)
-- Dependencies: 338
-- Name: TABLE tsgrtasistentes; Type: ACL; Schema: sgrt; Owner: suite
--

REVOKE ALL ON TABLE sgrt.tsgrtasistentes FROM suite;
GRANT ALL ON TABLE sgrt.tsgrtasistentes TO suite WITH GRANT OPTION;


--
-- TOC entry 5103 (class 0 OID 0)
-- Dependencies: 339
-- Name: TABLE tsgrtattchticket; Type: ACL; Schema: sgrt; Owner: suite
--

REVOKE ALL ON TABLE sgrt.tsgrtattchticket FROM suite;
GRANT ALL ON TABLE sgrt.tsgrtattchticket TO suite WITH GRANT OPTION;


--
-- TOC entry 5104 (class 0 OID 0)
-- Dependencies: 340
-- Name: TABLE tsgrtayudatopico; Type: ACL; Schema: sgrt; Owner: suite
--

REVOKE ALL ON TABLE sgrt.tsgrtayudatopico FROM suite;
GRANT ALL ON TABLE sgrt.tsgrtayudatopico TO suite WITH GRANT OPTION;


--
-- TOC entry 5105 (class 0 OID 0)
-- Dependencies: 341
-- Name: TABLE tsgrtcategoriafaq; Type: ACL; Schema: sgrt; Owner: suite
--

REVOKE ALL ON TABLE sgrt.tsgrtcategoriafaq FROM suite;
GRANT ALL ON TABLE sgrt.tsgrtcategoriafaq TO suite WITH GRANT OPTION;


--
-- TOC entry 5106 (class 0 OID 0)
-- Dependencies: 342
-- Name: TABLE tsgrtchat; Type: ACL; Schema: sgrt; Owner: suite
--

REVOKE ALL ON TABLE sgrt.tsgrtchat FROM suite;
GRANT ALL ON TABLE sgrt.tsgrtchat TO suite WITH GRANT OPTION;


--
-- TOC entry 5107 (class 0 OID 0)
-- Dependencies: 343
-- Name: TABLE tsgrtciudades; Type: ACL; Schema: sgrt; Owner: suite
--

REVOKE ALL ON TABLE sgrt.tsgrtciudades FROM suite;
GRANT ALL ON TABLE sgrt.tsgrtciudades TO suite WITH GRANT OPTION;


--
-- TOC entry 5108 (class 0 OID 0)
-- Dependencies: 344
-- Name: TABLE tsgrtcomentariosagenda; Type: ACL; Schema: sgrt; Owner: suite
--

REVOKE ALL ON TABLE sgrt.tsgrtcomentariosagenda FROM suite;
GRANT ALL ON TABLE sgrt.tsgrtcomentariosagenda TO suite WITH GRANT OPTION;


--
-- TOC entry 5109 (class 0 OID 0)
-- Dependencies: 345
-- Name: TABLE tsgrtcomentariosreunion; Type: ACL; Schema: sgrt; Owner: suite
--

REVOKE ALL ON TABLE sgrt.tsgrtcomentariosreunion FROM suite;
GRANT ALL ON TABLE sgrt.tsgrtcomentariosreunion TO suite WITH GRANT OPTION;


--
-- TOC entry 5110 (class 0 OID 0)
-- Dependencies: 346
-- Name: TABLE tsgrtcompromisos; Type: ACL; Schema: sgrt; Owner: suite
--

REVOKE ALL ON TABLE sgrt.tsgrtcompromisos FROM suite;
GRANT ALL ON TABLE sgrt.tsgrtcompromisos TO suite WITH GRANT OPTION;


--
-- TOC entry 5111 (class 0 OID 0)
-- Dependencies: 347
-- Name: TABLE tsgrtcorreo; Type: ACL; Schema: sgrt; Owner: suite
--

REVOKE ALL ON TABLE sgrt.tsgrtcorreo FROM suite;
GRANT ALL ON TABLE sgrt.tsgrtcorreo TO suite WITH GRANT OPTION;


--
-- TOC entry 5112 (class 0 OID 0)
-- Dependencies: 348
-- Name: TABLE tsgrtdatossolicitud; Type: ACL; Schema: sgrt; Owner: suite
--

REVOKE ALL ON TABLE sgrt.tsgrtdatossolicitud FROM suite;
GRANT ALL ON TABLE sgrt.tsgrtdatossolicitud TO suite WITH GRANT OPTION;


--
-- TOC entry 5113 (class 0 OID 0)
-- Dependencies: 349
-- Name: TABLE tsgrtdepartamento; Type: ACL; Schema: sgrt; Owner: suite
--

REVOKE ALL ON TABLE sgrt.tsgrtdepartamento FROM suite;
GRANT ALL ON TABLE sgrt.tsgrtdepartamento TO suite WITH GRANT OPTION;


--
-- TOC entry 5114 (class 0 OID 0)
-- Dependencies: 350
-- Name: TABLE tsgrtedosolicitudes; Type: ACL; Schema: sgrt; Owner: suite
--

REVOKE ALL ON TABLE sgrt.tsgrtedosolicitudes FROM suite;
GRANT ALL ON TABLE sgrt.tsgrtedosolicitudes TO suite WITH GRANT OPTION;


--
-- TOC entry 5115 (class 0 OID 0)
-- Dependencies: 351
-- Name: TABLE tsgrtelementos; Type: ACL; Schema: sgrt; Owner: suite
--

REVOKE ALL ON TABLE sgrt.tsgrtelementos FROM suite;
GRANT ALL ON TABLE sgrt.tsgrtelementos TO suite WITH GRANT OPTION;


--
-- TOC entry 5116 (class 0 OID 0)
-- Dependencies: 352
-- Name: TABLE tsgrtestados; Type: ACL; Schema: sgrt; Owner: suite
--

REVOKE ALL ON TABLE sgrt.tsgrtestados FROM suite;
GRANT ALL ON TABLE sgrt.tsgrtestados TO suite WITH GRANT OPTION;


--
-- TOC entry 5117 (class 0 OID 0)
-- Dependencies: 353
-- Name: TABLE tsgrtfaq; Type: ACL; Schema: sgrt; Owner: suite
--

REVOKE ALL ON TABLE sgrt.tsgrtfaq FROM suite;
GRANT ALL ON TABLE sgrt.tsgrtfaq TO suite WITH GRANT OPTION;


--
-- TOC entry 5118 (class 0 OID 0)
-- Dependencies: 354
-- Name: TABLE tsgrtgrupo; Type: ACL; Schema: sgrt; Owner: suite
--

REVOKE ALL ON TABLE sgrt.tsgrtgrupo FROM suite;
GRANT ALL ON TABLE sgrt.tsgrtgrupo TO suite WITH GRANT OPTION;


--
-- TOC entry 5119 (class 0 OID 0)
-- Dependencies: 355
-- Name: TABLE tsgrtinvitados; Type: ACL; Schema: sgrt; Owner: suite
--

REVOKE ALL ON TABLE sgrt.tsgrtinvitados FROM suite;
GRANT ALL ON TABLE sgrt.tsgrtinvitados TO suite WITH GRANT OPTION;


--
-- TOC entry 5120 (class 0 OID 0)
-- Dependencies: 356
-- Name: TABLE tsgrtlugares; Type: ACL; Schema: sgrt; Owner: suite
--

REVOKE ALL ON TABLE sgrt.tsgrtlugares FROM suite;
GRANT ALL ON TABLE sgrt.tsgrtlugares TO suite WITH GRANT OPTION;


--
-- TOC entry 5121 (class 0 OID 0)
-- Dependencies: 357
-- Name: TABLE tsgrtmsjticket; Type: ACL; Schema: sgrt; Owner: suite
--

REVOKE ALL ON TABLE sgrt.tsgrtmsjticket FROM suite;
GRANT ALL ON TABLE sgrt.tsgrtmsjticket TO suite WITH GRANT OPTION;


--
-- TOC entry 5122 (class 0 OID 0)
-- Dependencies: 358
-- Name: TABLE tsgrtnota; Type: ACL; Schema: sgrt; Owner: suite
--

REVOKE ALL ON TABLE sgrt.tsgrtnota FROM suite;
GRANT ALL ON TABLE sgrt.tsgrtnota TO suite WITH GRANT OPTION;


--
-- TOC entry 5123 (class 0 OID 0)
-- Dependencies: 359
-- Name: TABLE tsgrtplantillacorreos; Type: ACL; Schema: sgrt; Owner: suite
--

REVOKE ALL ON TABLE sgrt.tsgrtplantillacorreos FROM suite;
GRANT ALL ON TABLE sgrt.tsgrtplantillacorreos TO suite WITH GRANT OPTION;


--
-- TOC entry 5124 (class 0 OID 0)
-- Dependencies: 360
-- Name: TABLE tsgrtprioridad; Type: ACL; Schema: sgrt; Owner: suite
--

REVOKE ALL ON TABLE sgrt.tsgrtprioridad FROM suite;
GRANT ALL ON TABLE sgrt.tsgrtprioridad TO suite WITH GRANT OPTION;


--
-- TOC entry 5125 (class 0 OID 0)
-- Dependencies: 361
-- Name: TABLE tsgrtresppredefinida; Type: ACL; Schema: sgrt; Owner: suite
--

REVOKE ALL ON TABLE sgrt.tsgrtresppredefinida FROM suite;
GRANT ALL ON TABLE sgrt.tsgrtresppredefinida TO suite WITH GRANT OPTION;


--
-- TOC entry 5126 (class 0 OID 0)
-- Dependencies: 362
-- Name: TABLE tsgrtrespuesta; Type: ACL; Schema: sgrt; Owner: suite
--

REVOKE ALL ON TABLE sgrt.tsgrtrespuesta FROM suite;
GRANT ALL ON TABLE sgrt.tsgrtrespuesta TO suite WITH GRANT OPTION;


--
-- TOC entry 5127 (class 0 OID 0)
-- Dependencies: 363
-- Name: TABLE tsgrtreuniones; Type: ACL; Schema: sgrt; Owner: suite
--

REVOKE ALL ON TABLE sgrt.tsgrtreuniones FROM suite;
GRANT ALL ON TABLE sgrt.tsgrtreuniones TO suite WITH GRANT OPTION;


--
-- TOC entry 5128 (class 0 OID 0)
-- Dependencies: 364
-- Name: TABLE tsgrtservicios; Type: ACL; Schema: sgrt; Owner: suite
--

REVOKE ALL ON TABLE sgrt.tsgrtservicios FROM suite;
GRANT ALL ON TABLE sgrt.tsgrtservicios TO suite WITH GRANT OPTION;


--
-- TOC entry 5129 (class 0 OID 0)
-- Dependencies: 365
-- Name: TABLE tsgrtsolicitudservicios; Type: ACL; Schema: sgrt; Owner: suite
--

REVOKE ALL ON TABLE sgrt.tsgrtsolicitudservicios FROM suite;
GRANT ALL ON TABLE sgrt.tsgrtsolicitudservicios TO suite WITH GRANT OPTION;


--
-- TOC entry 5130 (class 0 OID 0)
-- Dependencies: 366
-- Name: TABLE tsgrtticket; Type: ACL; Schema: sgrt; Owner: suite
--

REVOKE ALL ON TABLE sgrt.tsgrtticket FROM suite;
GRANT ALL ON TABLE sgrt.tsgrtticket TO suite WITH GRANT OPTION;


--
-- TOC entry 5131 (class 0 OID 0)
-- Dependencies: 405
-- Name: SEQUENCE seq_aceptaciones; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON SEQUENCE sisat.seq_aceptaciones FROM suite;
GRANT UPDATE ON SEQUENCE sisat.seq_aceptaciones TO suite;
GRANT SELECT,USAGE ON SEQUENCE sisat.seq_aceptaciones TO suite WITH GRANT OPTION;


--
-- TOC entry 5132 (class 0 OID 0)
-- Dependencies: 406
-- Name: SEQUENCE seq_asignaciones; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON SEQUENCE sisat.seq_asignaciones FROM suite;
GRANT UPDATE ON SEQUENCE sisat.seq_asignaciones TO suite;
GRANT SELECT,USAGE ON SEQUENCE sisat.seq_asignaciones TO suite WITH GRANT OPTION;


--
-- TOC entry 5133 (class 0 OID 0)
-- Dependencies: 407
-- Name: SEQUENCE seq_candidatos; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON SEQUENCE sisat.seq_candidatos FROM suite;
GRANT UPDATE ON SEQUENCE sisat.seq_candidatos TO suite;
GRANT SELECT,USAGE ON SEQUENCE sisat.seq_candidatos TO suite WITH GRANT OPTION;


--
-- TOC entry 5134 (class 0 OID 0)
-- Dependencies: 408
-- Name: SEQUENCE seq_cartaasignaciones; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON SEQUENCE sisat.seq_cartaasignaciones FROM suite;
GRANT UPDATE ON SEQUENCE sisat.seq_cartaasignaciones TO suite;
GRANT SELECT,USAGE ON SEQUENCE sisat.seq_cartaasignaciones TO suite WITH GRANT OPTION;


--
-- TOC entry 5135 (class 0 OID 0)
-- Dependencies: 409
-- Name: SEQUENCE seq_comentcartaasignacion; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON SEQUENCE sisat.seq_comentcartaasignacion FROM suite;
GRANT UPDATE ON SEQUENCE sisat.seq_comentcartaasignacion TO suite;
GRANT SELECT,USAGE ON SEQUENCE sisat.seq_comentcartaasignacion TO suite WITH GRANT OPTION;


--
-- TOC entry 5136 (class 0 OID 0)
-- Dependencies: 410
-- Name: SEQUENCE seq_comentcosteo; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON SEQUENCE sisat.seq_comentcosteo FROM suite;
GRANT UPDATE ON SEQUENCE sisat.seq_comentcosteo TO suite;
GRANT SELECT,USAGE ON SEQUENCE sisat.seq_comentcosteo TO suite WITH GRANT OPTION;


--
-- TOC entry 5137 (class 0 OID 0)
-- Dependencies: 411
-- Name: SEQUENCE seq_comententrevista; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON SEQUENCE sisat.seq_comententrevista FROM suite;
GRANT UPDATE ON SEQUENCE sisat.seq_comententrevista TO suite;
GRANT SELECT,USAGE ON SEQUENCE sisat.seq_comententrevista TO suite WITH GRANT OPTION;


--
-- TOC entry 5138 (class 0 OID 0)
-- Dependencies: 412
-- Name: SEQUENCE seq_comentvacantes; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON SEQUENCE sisat.seq_comentvacantes FROM suite;
GRANT UPDATE ON SEQUENCE sisat.seq_comentvacantes TO suite;
GRANT SELECT,USAGE ON SEQUENCE sisat.seq_comentvacantes TO suite WITH GRANT OPTION;


--
-- TOC entry 5139 (class 0 OID 0)
-- Dependencies: 413
-- Name: SEQUENCE seq_contrataciones; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON SEQUENCE sisat.seq_contrataciones FROM suite;
GRANT UPDATE ON SEQUENCE sisat.seq_contrataciones TO suite;
GRANT SELECT,USAGE ON SEQUENCE sisat.seq_contrataciones TO suite WITH GRANT OPTION;


--
-- TOC entry 5140 (class 0 OID 0)
-- Dependencies: 414
-- Name: SEQUENCE seq_cotizaciones; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON SEQUENCE sisat.seq_cotizaciones FROM suite;
GRANT UPDATE ON SEQUENCE sisat.seq_cotizaciones TO suite;
GRANT SELECT,USAGE ON SEQUENCE sisat.seq_cotizaciones TO suite WITH GRANT OPTION;


--
-- TOC entry 5141 (class 0 OID 0)
-- Dependencies: 415
-- Name: SEQUENCE seq_cursos; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON SEQUENCE sisat.seq_cursos FROM suite;
GRANT UPDATE ON SEQUENCE sisat.seq_cursos TO suite;
GRANT SELECT,USAGE ON SEQUENCE sisat.seq_cursos TO suite WITH GRANT OPTION;


--
-- TOC entry 5142 (class 0 OID 0)
-- Dependencies: 416
-- Name: SEQUENCE seq_entrevistas; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON SEQUENCE sisat.seq_entrevistas FROM suite;
GRANT UPDATE ON SEQUENCE sisat.seq_entrevistas TO suite;
GRANT SELECT,USAGE ON SEQUENCE sisat.seq_entrevistas TO suite WITH GRANT OPTION;


--
-- TOC entry 5143 (class 0 OID 0)
-- Dependencies: 417
-- Name: SEQUENCE seq_envios; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON SEQUENCE sisat.seq_envios FROM suite;
GRANT UPDATE ON SEQUENCE sisat.seq_envios TO suite;
GRANT SELECT,USAGE ON SEQUENCE sisat.seq_envios TO suite WITH GRANT OPTION;


--
-- TOC entry 5144 (class 0 OID 0)
-- Dependencies: 418
-- Name: SEQUENCE seq_escolaridad; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON SEQUENCE sisat.seq_escolaridad FROM suite;
GRANT UPDATE ON SEQUENCE sisat.seq_escolaridad TO suite;
GRANT SELECT,USAGE ON SEQUENCE sisat.seq_escolaridad TO suite WITH GRANT OPTION;


--
-- TOC entry 5145 (class 0 OID 0)
-- Dependencies: 419
-- Name: SEQUENCE seq_experiencias; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON SEQUENCE sisat.seq_experiencias FROM suite;
GRANT UPDATE ON SEQUENCE sisat.seq_experiencias TO suite;
GRANT SELECT,USAGE ON SEQUENCE sisat.seq_experiencias TO suite WITH GRANT OPTION;


--
-- TOC entry 5146 (class 0 OID 0)
-- Dependencies: 420
-- Name: SEQUENCE seq_firmareqper; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON SEQUENCE sisat.seq_firmareqper FROM suite;
GRANT UPDATE ON SEQUENCE sisat.seq_firmareqper TO suite;
GRANT SELECT,USAGE ON SEQUENCE sisat.seq_firmareqper TO suite WITH GRANT OPTION;


--
-- TOC entry 5147 (class 0 OID 0)
-- Dependencies: 421
-- Name: SEQUENCE seq_firmas; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON SEQUENCE sisat.seq_firmas FROM suite;
GRANT UPDATE ON SEQUENCE sisat.seq_firmas TO suite;
GRANT SELECT,USAGE ON SEQUENCE sisat.seq_firmas TO suite WITH GRANT OPTION;


--
-- TOC entry 5148 (class 0 OID 0)
-- Dependencies: 422
-- Name: SEQUENCE seq_habilidades; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON SEQUENCE sisat.seq_habilidades FROM suite;
GRANT UPDATE ON SEQUENCE sisat.seq_habilidades TO suite;
GRANT SELECT,USAGE ON SEQUENCE sisat.seq_habilidades TO suite WITH GRANT OPTION;


--
-- TOC entry 5149 (class 0 OID 0)
-- Dependencies: 423
-- Name: SEQUENCE seq_idiomas; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON SEQUENCE sisat.seq_idiomas FROM suite;
GRANT UPDATE ON SEQUENCE sisat.seq_idiomas TO suite;
GRANT SELECT,USAGE ON SEQUENCE sisat.seq_idiomas TO suite WITH GRANT OPTION;


--
-- TOC entry 5150 (class 0 OID 0)
-- Dependencies: 424
-- Name: SEQUENCE seq_ordenservicios; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON SEQUENCE sisat.seq_ordenservicios FROM suite;
GRANT UPDATE ON SEQUENCE sisat.seq_ordenservicios TO suite;
GRANT SELECT,USAGE ON SEQUENCE sisat.seq_ordenservicios TO suite WITH GRANT OPTION;


--
-- TOC entry 5151 (class 0 OID 0)
-- Dependencies: 425
-- Name: SEQUENCE seq_prospectos; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON SEQUENCE sisat.seq_prospectos FROM suite;
GRANT UPDATE ON SEQUENCE sisat.seq_prospectos TO suite;
GRANT SELECT,USAGE ON SEQUENCE sisat.seq_prospectos TO suite WITH GRANT OPTION;


--
-- TOC entry 5152 (class 0 OID 0)
-- Dependencies: 426
-- Name: SEQUENCE seq_proyectos; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON SEQUENCE sisat.seq_proyectos FROM suite;
GRANT UPDATE ON SEQUENCE sisat.seq_proyectos TO suite;
GRANT SELECT,USAGE ON SEQUENCE sisat.seq_proyectos TO suite WITH GRANT OPTION;


--
-- TOC entry 5153 (class 0 OID 0)
-- Dependencies: 427
-- Name: SEQUENCE seq_vacantes; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON SEQUENCE sisat.seq_vacantes FROM suite;
GRANT UPDATE ON SEQUENCE sisat.seq_vacantes TO suite;
GRANT SELECT,USAGE ON SEQUENCE sisat.seq_vacantes TO suite WITH GRANT OPTION;


--
-- TOC entry 5154 (class 0 OID 0)
-- Dependencies: 428
-- Name: TABLE tsisatappservices; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisatappservices FROM suite;
GRANT ALL ON TABLE sisat.tsisatappservices TO suite WITH GRANT OPTION;


--
-- TOC entry 5155 (class 0 OID 0)
-- Dependencies: 429
-- Name: TABLE tsisatarquitecturas; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisatarquitecturas FROM suite;
GRANT ALL ON TABLE sisat.tsisatarquitecturas TO suite WITH GRANT OPTION;


--
-- TOC entry 5156 (class 0 OID 0)
-- Dependencies: 430
-- Name: TABLE tsisatasignaciones; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisatasignaciones FROM suite;
GRANT ALL ON TABLE sisat.tsisatasignaciones TO suite WITH GRANT OPTION;


--
-- TOC entry 5157 (class 0 OID 0)
-- Dependencies: 431
-- Name: TABLE tsisatcandidatos; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisatcandidatos FROM suite;
GRANT ALL ON TABLE sisat.tsisatcandidatos TO suite WITH GRANT OPTION;


--
-- TOC entry 5158 (class 0 OID 0)
-- Dependencies: 432
-- Name: TABLE tsisatcartaaceptacion; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisatcartaaceptacion FROM suite;
GRANT ALL ON TABLE sisat.tsisatcartaaceptacion TO suite WITH GRANT OPTION;


--
-- TOC entry 5159 (class 0 OID 0)
-- Dependencies: 433
-- Name: TABLE tsisatcartaasignacion; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisatcartaasignacion FROM suite;
GRANT ALL ON TABLE sisat.tsisatcartaasignacion TO suite WITH GRANT OPTION;


--
-- TOC entry 5160 (class 0 OID 0)
-- Dependencies: 434
-- Name: TABLE tsisatcomentcartaasignacion; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisatcomentcartaasignacion FROM suite;
GRANT ALL ON TABLE sisat.tsisatcomentcartaasignacion TO suite WITH GRANT OPTION;


--
-- TOC entry 5161 (class 0 OID 0)
-- Dependencies: 435
-- Name: TABLE tsisatcomentcosteo; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisatcomentcosteo FROM suite;
GRANT ALL ON TABLE sisat.tsisatcomentcosteo TO suite WITH GRANT OPTION;


--
-- TOC entry 5162 (class 0 OID 0)
-- Dependencies: 436
-- Name: TABLE tsisatcomententrevista; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisatcomententrevista FROM suite;
GRANT ALL ON TABLE sisat.tsisatcomententrevista TO suite WITH GRANT OPTION;


--
-- TOC entry 5163 (class 0 OID 0)
-- Dependencies: 437
-- Name: TABLE tsisatcomentvacantes; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisatcomentvacantes FROM suite;
GRANT ALL ON TABLE sisat.tsisatcomentvacantes TO suite WITH GRANT OPTION;


--
-- TOC entry 5164 (class 0 OID 0)
-- Dependencies: 438
-- Name: TABLE tsisatcontrataciones; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisatcontrataciones FROM suite;
GRANT ALL ON TABLE sisat.tsisatcontrataciones TO suite WITH GRANT OPTION;


--
-- TOC entry 5165 (class 0 OID 0)
-- Dependencies: 439
-- Name: TABLE tsisatcotizaciones; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisatcotizaciones FROM suite;
GRANT ALL ON TABLE sisat.tsisatcotizaciones TO suite WITH GRANT OPTION;


--
-- TOC entry 5166 (class 0 OID 0)
-- Dependencies: 440
-- Name: TABLE tsisatcursosycertificados; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisatcursosycertificados FROM suite;
GRANT ALL ON TABLE sisat.tsisatcursosycertificados TO suite WITH GRANT OPTION;


--
-- TOC entry 5167 (class 0 OID 0)
-- Dependencies: 441
-- Name: TABLE tsisatentrevistas; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisatentrevistas FROM suite;
GRANT ALL ON TABLE sisat.tsisatentrevistas TO suite WITH GRANT OPTION;


--
-- TOC entry 5168 (class 0 OID 0)
-- Dependencies: 442
-- Name: TABLE tsisatenviocorreos; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisatenviocorreos FROM suite;
GRANT ALL ON TABLE sisat.tsisatenviocorreos TO suite WITH GRANT OPTION;


--
-- TOC entry 5169 (class 0 OID 0)
-- Dependencies: 443
-- Name: TABLE tsisatescolaridad; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisatescolaridad FROM suite;
GRANT ALL ON TABLE sisat.tsisatescolaridad TO suite WITH GRANT OPTION;


--
-- TOC entry 5170 (class 0 OID 0)
-- Dependencies: 444
-- Name: TABLE tsisatexperienciaslaborales; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisatexperienciaslaborales FROM suite;
GRANT ALL ON TABLE sisat.tsisatexperienciaslaborales TO suite WITH GRANT OPTION;


--
-- TOC entry 5171 (class 0 OID 0)
-- Dependencies: 445
-- Name: TABLE tsisatfirmareqper; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisatfirmareqper FROM suite;
GRANT ALL ON TABLE sisat.tsisatfirmareqper TO suite WITH GRANT OPTION;


--
-- TOC entry 5172 (class 0 OID 0)
-- Dependencies: 446
-- Name: TABLE tsisatframeworks; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisatframeworks FROM suite;
GRANT ALL ON TABLE sisat.tsisatframeworks TO suite WITH GRANT OPTION;


--
-- TOC entry 5173 (class 0 OID 0)
-- Dependencies: 447
-- Name: TABLE tsisathabilidades; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisathabilidades FROM suite;
GRANT ALL ON TABLE sisat.tsisathabilidades TO suite WITH GRANT OPTION;


--
-- TOC entry 5174 (class 0 OID 0)
-- Dependencies: 448
-- Name: TABLE tsisatherramientas; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisatherramientas FROM suite;
GRANT ALL ON TABLE sisat.tsisatherramientas TO suite WITH GRANT OPTION;


--
-- TOC entry 5176 (class 0 OID 0)
-- Dependencies: 449
-- Name: SEQUENCE tsisatherramientas_cod_herramientas_seq; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON SEQUENCE sisat.tsisatherramientas_cod_herramientas_seq FROM suite;
GRANT UPDATE ON SEQUENCE sisat.tsisatherramientas_cod_herramientas_seq TO suite;
GRANT SELECT,USAGE ON SEQUENCE sisat.tsisatherramientas_cod_herramientas_seq TO suite WITH GRANT OPTION;


--
-- TOC entry 5177 (class 0 OID 0)
-- Dependencies: 450
-- Name: TABLE tsisatides; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisatides FROM suite;
GRANT ALL ON TABLE sisat.tsisatides TO suite WITH GRANT OPTION;


--
-- TOC entry 5178 (class 0 OID 0)
-- Dependencies: 451
-- Name: TABLE tsisatidiomas; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisatidiomas FROM suite;
GRANT ALL ON TABLE sisat.tsisatidiomas TO suite WITH GRANT OPTION;


--
-- TOC entry 5179 (class 0 OID 0)
-- Dependencies: 452
-- Name: TABLE tsisatlenguajes; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisatlenguajes FROM suite;
GRANT ALL ON TABLE sisat.tsisatlenguajes TO suite WITH GRANT OPTION;


--
-- TOC entry 5180 (class 0 OID 0)
-- Dependencies: 453
-- Name: TABLE tsisatmaquetados; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisatmaquetados FROM suite;
GRANT ALL ON TABLE sisat.tsisatmaquetados TO suite WITH GRANT OPTION;


--
-- TOC entry 5181 (class 0 OID 0)
-- Dependencies: 454
-- Name: TABLE tsisatmetodologias; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisatmetodologias FROM suite;
GRANT ALL ON TABLE sisat.tsisatmetodologias TO suite WITH GRANT OPTION;


--
-- TOC entry 5182 (class 0 OID 0)
-- Dependencies: 455
-- Name: TABLE tsisatmodelados; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisatmodelados FROM suite;
GRANT ALL ON TABLE sisat.tsisatmodelados TO suite WITH GRANT OPTION;


--
-- TOC entry 5183 (class 0 OID 0)
-- Dependencies: 456
-- Name: TABLE tsisatordenservicio; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisatordenservicio FROM suite;
GRANT ALL ON TABLE sisat.tsisatordenservicio TO suite WITH GRANT OPTION;


--
-- TOC entry 5184 (class 0 OID 0)
-- Dependencies: 457
-- Name: TABLE tsisatpatrones; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisatpatrones FROM suite;
GRANT ALL ON TABLE sisat.tsisatpatrones TO suite WITH GRANT OPTION;


--
-- TOC entry 5185 (class 0 OID 0)
-- Dependencies: 458
-- Name: TABLE tsisatprospectos; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisatprospectos FROM suite;
GRANT ALL ON TABLE sisat.tsisatprospectos TO suite WITH GRANT OPTION;


--
-- TOC entry 5186 (class 0 OID 0)
-- Dependencies: 459
-- Name: TABLE tsisatprospectos_idiomas; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisatprospectos_idiomas FROM suite;
GRANT ALL ON TABLE sisat.tsisatprospectos_idiomas TO suite WITH GRANT OPTION;


--
-- TOC entry 5187 (class 0 OID 0)
-- Dependencies: 460
-- Name: TABLE tsisatprotocolos; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisatprotocolos FROM suite;
GRANT ALL ON TABLE sisat.tsisatprotocolos TO suite WITH GRANT OPTION;


--
-- TOC entry 5188 (class 0 OID 0)
-- Dependencies: 461
-- Name: TABLE tsisatproyectos; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisatproyectos FROM suite;
GRANT ALL ON TABLE sisat.tsisatproyectos TO suite WITH GRANT OPTION;


--
-- TOC entry 5189 (class 0 OID 0)
-- Dependencies: 462
-- Name: TABLE tsisatqa; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisatqa FROM suite;
GRANT ALL ON TABLE sisat.tsisatqa TO suite WITH GRANT OPTION;


--
-- TOC entry 5190 (class 0 OID 0)
-- Dependencies: 463
-- Name: TABLE tsisatrepositoriolibrerias; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisatrepositoriolibrerias FROM suite;
GRANT ALL ON TABLE sisat.tsisatrepositoriolibrerias TO suite WITH GRANT OPTION;


--
-- TOC entry 5191 (class 0 OID 0)
-- Dependencies: 464
-- Name: TABLE tsisatrepositorios; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisatrepositorios FROM suite;
GRANT ALL ON TABLE sisat.tsisatrepositorios TO suite WITH GRANT OPTION;


--
-- TOC entry 5192 (class 0 OID 0)
-- Dependencies: 465
-- Name: TABLE tsisatsgbd; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisatsgbd FROM suite;
GRANT ALL ON TABLE sisat.tsisatsgbd TO suite WITH GRANT OPTION;


--
-- TOC entry 5193 (class 0 OID 0)
-- Dependencies: 466
-- Name: TABLE tsisatso; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisatso FROM suite;
GRANT ALL ON TABLE sisat.tsisatso TO suite WITH GRANT OPTION;


--
-- TOC entry 5194 (class 0 OID 0)
-- Dependencies: 467
-- Name: TABLE tsisatvacantes; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisatvacantes FROM suite;
GRANT ALL ON TABLE sisat.tsisatvacantes TO suite WITH GRANT OPTION;


--
-- TOC entry 2679 (class 826 OID 101917)
-- Name: DEFAULT PRIVILEGES FOR SEQUENCES; Type: DEFAULT ACL; Schema: sgco; Owner: suite
--

ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sgco REVOKE ALL ON SEQUENCES  FROM suite;
ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sgco GRANT SELECT,USAGE ON SEQUENCES  TO suite WITH GRANT OPTION;


--
-- TOC entry 2680 (class 826 OID 101918)
-- Name: DEFAULT PRIVILEGES FOR TYPES; Type: DEFAULT ACL; Schema: sgco; Owner: suite
--

ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sgco REVOKE ALL ON TYPES  FROM PUBLIC;
ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sgco REVOKE ALL ON TYPES  FROM suite;
ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sgco GRANT ALL ON TYPES  TO suite WITH GRANT OPTION;


--
-- TOC entry 2681 (class 826 OID 101919)
-- Name: DEFAULT PRIVILEGES FOR FUNCTIONS; Type: DEFAULT ACL; Schema: sgco; Owner: suite
--

ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sgco REVOKE ALL ON FUNCTIONS  FROM PUBLIC;
ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sgco REVOKE ALL ON FUNCTIONS  FROM suite;
ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sgco GRANT ALL ON FUNCTIONS  TO suite WITH GRANT OPTION;


--
-- TOC entry 2682 (class 826 OID 101920)
-- Name: DEFAULT PRIVILEGES FOR TABLES; Type: DEFAULT ACL; Schema: sgco; Owner: suite
--

ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sgco REVOKE ALL ON TABLES  FROM suite;
ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sgco GRANT ALL ON TABLES  TO suite WITH GRANT OPTION;


--
-- TOC entry 2695 (class 826 OID 102332)
-- Name: DEFAULT PRIVILEGES FOR SEQUENCES; Type: DEFAULT ACL; Schema: sgnom; Owner: suite
--

ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sgnom REVOKE ALL ON SEQUENCES  FROM suite;
ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sgnom GRANT SELECT,USAGE ON SEQUENCES  TO suite WITH GRANT OPTION;


--
-- TOC entry 2696 (class 826 OID 102333)
-- Name: DEFAULT PRIVILEGES FOR TYPES; Type: DEFAULT ACL; Schema: sgnom; Owner: suite
--

ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sgnom REVOKE ALL ON TYPES  FROM PUBLIC;
ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sgnom REVOKE ALL ON TYPES  FROM suite;
ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sgnom GRANT ALL ON TYPES  TO suite WITH GRANT OPTION;


--
-- TOC entry 2697 (class 826 OID 102334)
-- Name: DEFAULT PRIVILEGES FOR FUNCTIONS; Type: DEFAULT ACL; Schema: sgnom; Owner: suite
--

ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sgnom REVOKE ALL ON FUNCTIONS  FROM PUBLIC;
ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sgnom REVOKE ALL ON FUNCTIONS  FROM suite;
ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sgnom GRANT ALL ON FUNCTIONS  TO suite WITH GRANT OPTION;


--
-- TOC entry 2698 (class 826 OID 102335)
-- Name: DEFAULT PRIVILEGES FOR TABLES; Type: DEFAULT ACL; Schema: sgnom; Owner: suite
--

ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sgnom REVOKE ALL ON TABLES  FROM suite;
ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sgnom GRANT ALL ON TABLES  TO suite WITH GRANT OPTION;


--
-- TOC entry 2683 (class 826 OID 101925)
-- Name: DEFAULT PRIVILEGES FOR SEQUENCES; Type: DEFAULT ACL; Schema: sgrh; Owner: suite
--

ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sgrh REVOKE ALL ON SEQUENCES  FROM suite;
ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sgrh GRANT SELECT,USAGE ON SEQUENCES  TO suite WITH GRANT OPTION;


--
-- TOC entry 2684 (class 826 OID 101926)
-- Name: DEFAULT PRIVILEGES FOR TYPES; Type: DEFAULT ACL; Schema: sgrh; Owner: suite
--

ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sgrh REVOKE ALL ON TYPES  FROM PUBLIC;
ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sgrh REVOKE ALL ON TYPES  FROM suite;
ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sgrh GRANT ALL ON TYPES  TO suite WITH GRANT OPTION;


--
-- TOC entry 2685 (class 826 OID 101927)
-- Name: DEFAULT PRIVILEGES FOR FUNCTIONS; Type: DEFAULT ACL; Schema: sgrh; Owner: suite
--

ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sgrh REVOKE ALL ON FUNCTIONS  FROM PUBLIC;
ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sgrh REVOKE ALL ON FUNCTIONS  FROM suite;
ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sgrh GRANT ALL ON FUNCTIONS  TO suite WITH GRANT OPTION;


--
-- TOC entry 2686 (class 826 OID 101928)
-- Name: DEFAULT PRIVILEGES FOR TABLES; Type: DEFAULT ACL; Schema: sgrh; Owner: suite
--

ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sgrh REVOKE ALL ON TABLES  FROM suite;
ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sgrh GRANT ALL ON TABLES  TO suite WITH GRANT OPTION;


--
-- TOC entry 2687 (class 826 OID 101929)
-- Name: DEFAULT PRIVILEGES FOR SEQUENCES; Type: DEFAULT ACL; Schema: sgrt; Owner: suite
--

ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sgrt REVOKE ALL ON SEQUENCES  FROM suite;
ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sgrt GRANT SELECT,USAGE ON SEQUENCES  TO suite WITH GRANT OPTION;


--
-- TOC entry 2688 (class 826 OID 101930)
-- Name: DEFAULT PRIVILEGES FOR TYPES; Type: DEFAULT ACL; Schema: sgrt; Owner: suite
--

ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sgrt REVOKE ALL ON TYPES  FROM PUBLIC;
ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sgrt REVOKE ALL ON TYPES  FROM suite;
ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sgrt GRANT ALL ON TYPES  TO suite WITH GRANT OPTION;


--
-- TOC entry 2689 (class 826 OID 101931)
-- Name: DEFAULT PRIVILEGES FOR FUNCTIONS; Type: DEFAULT ACL; Schema: sgrt; Owner: suite
--

ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sgrt REVOKE ALL ON FUNCTIONS  FROM PUBLIC;
ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sgrt REVOKE ALL ON FUNCTIONS  FROM suite;
ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sgrt GRANT ALL ON FUNCTIONS  TO suite WITH GRANT OPTION;


--
-- TOC entry 2690 (class 826 OID 101932)
-- Name: DEFAULT PRIVILEGES FOR TABLES; Type: DEFAULT ACL; Schema: sgrt; Owner: suite
--

ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sgrt REVOKE ALL ON TABLES  FROM suite;
ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sgrt GRANT ALL ON TABLES  TO suite WITH GRANT OPTION;


--
-- TOC entry 2699 (class 826 OID 103366)
-- Name: DEFAULT PRIVILEGES FOR SEQUENCES; Type: DEFAULT ACL; Schema: sisat; Owner: suite
--

ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sisat REVOKE ALL ON SEQUENCES  FROM suite;
ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sisat GRANT SELECT,USAGE ON SEQUENCES  TO suite WITH GRANT OPTION;


--
-- TOC entry 2700 (class 826 OID 103367)
-- Name: DEFAULT PRIVILEGES FOR TYPES; Type: DEFAULT ACL; Schema: sisat; Owner: suite
--

ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sisat REVOKE ALL ON TYPES  FROM PUBLIC;
ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sisat REVOKE ALL ON TYPES  FROM suite;
ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sisat GRANT ALL ON TYPES  TO suite WITH GRANT OPTION;


--
-- TOC entry 2701 (class 826 OID 103368)
-- Name: DEFAULT PRIVILEGES FOR FUNCTIONS; Type: DEFAULT ACL; Schema: sisat; Owner: suite
--

ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sisat REVOKE ALL ON FUNCTIONS  FROM PUBLIC;
ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sisat REVOKE ALL ON FUNCTIONS  FROM suite;
ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sisat GRANT ALL ON FUNCTIONS  TO suite WITH GRANT OPTION;


--
-- TOC entry 2702 (class 826 OID 103369)
-- Name: DEFAULT PRIVILEGES FOR TABLES; Type: DEFAULT ACL; Schema: sisat; Owner: suite
--

ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sisat REVOKE ALL ON TABLES  FROM suite;
ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sisat GRANT ALL ON TABLES  TO suite WITH GRANT OPTION;


--
-- TOC entry 2691 (class 826 OID 101937)
-- Name: DEFAULT PRIVILEGES FOR SEQUENCES; Type: DEFAULT ACL; Schema: -; Owner: suite
--

ALTER DEFAULT PRIVILEGES FOR ROLE suite REVOKE ALL ON SEQUENCES  FROM suite;
ALTER DEFAULT PRIVILEGES FOR ROLE suite GRANT UPDATE ON SEQUENCES  TO suite;
ALTER DEFAULT PRIVILEGES FOR ROLE suite GRANT SELECT,USAGE ON SEQUENCES  TO suite WITH GRANT OPTION;


--
-- TOC entry 2692 (class 826 OID 101938)
-- Name: DEFAULT PRIVILEGES FOR TYPES; Type: DEFAULT ACL; Schema: -; Owner: suite
--

ALTER DEFAULT PRIVILEGES FOR ROLE suite REVOKE ALL ON TYPES  FROM suite;
ALTER DEFAULT PRIVILEGES FOR ROLE suite GRANT ALL ON TYPES  TO suite WITH GRANT OPTION;


--
-- TOC entry 2693 (class 826 OID 101939)
-- Name: DEFAULT PRIVILEGES FOR FUNCTIONS; Type: DEFAULT ACL; Schema: -; Owner: suite
--

ALTER DEFAULT PRIVILEGES FOR ROLE suite REVOKE ALL ON FUNCTIONS  FROM suite;
ALTER DEFAULT PRIVILEGES FOR ROLE suite GRANT ALL ON FUNCTIONS  TO suite WITH GRANT OPTION;


--
-- TOC entry 2694 (class 826 OID 101940)
-- Name: DEFAULT PRIVILEGES FOR TABLES; Type: DEFAULT ACL; Schema: -; Owner: suite
--

ALTER DEFAULT PRIVILEGES FOR ROLE suite REVOKE ALL ON TABLES  FROM suite;
ALTER DEFAULT PRIVILEGES FOR ROLE suite GRANT ALL ON TABLES  TO suite WITH GRANT OPTION;


-- Completed on 2019-12-10 16:28:14

--
-- PostgreSQL database dump complete
--

