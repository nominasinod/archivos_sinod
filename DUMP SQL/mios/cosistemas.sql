--
-- PostgreSQL database dump
--

-- Dumped from database version 11.5
-- Dumped by pg_dump version 11.5

-- Started on 2019-10-21 11:26:37

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 209 (class 1259 OID 46628)
-- Name: tsgcosistemas; Type: TABLE; Schema: sgco; Owner: suite
--

CREATE TABLE sgco.tsgcosistemas (
    cod_sistema integer NOT NULL,
    des_nbsistema character varying(50) NOT NULL,
    des_descripcion character varying(300)
);


ALTER TABLE sgco.tsgcosistemas OWNER TO suite;

--
-- TOC entry 3545 (class 0 OID 46628)
-- Dependencies: 209
-- Data for Name: tsgcosistemas; Type: TABLE DATA; Schema: sgco; Owner: suite
--

INSERT INTO sgco.tsgcosistemas (cod_sistema, des_nbsistema, des_descripcion) VALUES (1, 'SGRHAT', 'Sistema de Recursos Humanos y Ambiente de Trabajo');
INSERT INTO sgco.tsgcosistemas (cod_sistema, des_nbsistema, des_descripcion) VALUES (2, 'SISAT', 'Sistema de Selección y Administración de Talentos');
INSERT INTO sgco.tsgcosistemas (cod_sistema, des_nbsistema, des_descripcion) VALUES (3, 'SGNOM', 'Sistema de Nómina');


--
-- TOC entry 3421 (class 2606 OID 47465)
-- Name: tsgcosistemas cod_agenda; Type: CONSTRAINT; Schema: sgco; Owner: suite
--

ALTER TABLE ONLY sgco.tsgcosistemas
    ADD CONSTRAINT cod_agenda UNIQUE (cod_sistema);


--
-- TOC entry 3423 (class 2606 OID 47471)
-- Name: tsgcosistemas tsgcosistemas_pkey; Type: CONSTRAINT; Schema: sgco; Owner: suite
--

ALTER TABLE ONLY sgco.tsgcosistemas
    ADD CONSTRAINT tsgcosistemas_pkey PRIMARY KEY (cod_sistema);


--
-- TOC entry 3551 (class 0 OID 0)
-- Dependencies: 209
-- Name: TABLE tsgcosistemas; Type: ACL; Schema: sgco; Owner: suite
--

REVOKE ALL ON TABLE sgco.tsgcosistemas FROM suite;
GRANT ALL ON TABLE sgco.tsgcosistemas TO suite WITH GRANT OPTION;


-- Completed on 2019-10-21 11:26:37

--
-- PostgreSQL database dump complete
--

