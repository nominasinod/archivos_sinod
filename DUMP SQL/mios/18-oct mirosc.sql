--
-- PostgreSQL database dump
--

-- Dumped from database version 11.5
-- Dumped by pg_dump version 11.5

-- Started on 2019-10-18 16:14:45

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 7 (class 2615 OID 49524)
-- Name: armaz; Type: SCHEMA; Schema: -; Owner: mirosc
--

CREATE SCHEMA armaz;


ALTER SCHEMA armaz OWNER TO mirosc;

--
-- TOC entry 6 (class 2615 OID 49441)
-- Name: cat; Type: SCHEMA; Schema: -; Owner: mirosc
--

CREATE SCHEMA cat;


ALTER SCHEMA cat OWNER TO mirosc;

--
-- TOC entry 12 (class 2615 OID 49525)
-- Name: inven; Type: SCHEMA; Schema: -; Owner: mirosc
--

CREATE SCHEMA inven;


ALTER SCHEMA inven OWNER TO mirosc;

--
-- TOC entry 11 (class 2615 OID 49515)
-- Name: mica; Type: SCHEMA; Schema: -; Owner: mirosc
--

CREATE SCHEMA mica;


ALTER SCHEMA mica OWNER TO mirosc;

--
-- TOC entry 9 (class 2615 OID 49234)
-- Name: sght; Type: SCHEMA; Schema: -; Owner: mirosc
--

CREATE SCHEMA sght;


ALTER SCHEMA sght OWNER TO mirosc;

--
-- TOC entry 241 (class 1255 OID 49235)
-- Name: fu_obtener_edad(date); Type: FUNCTION; Schema: public; Owner: mirosc
--

CREATE FUNCTION public.fu_obtener_edad(pd_fecha_ini date, OUT pn_edad integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$
BEGIN
pn_edad := FLOOR(((DATE_PART('YEAR',current_date)-DATE_PART('YEAR',pd_fecha_ini))* 372 + (DATE_PART('MONTH',current_date) - DATE_PART('MONTH',pd_fecha_ini))*31 + (DATE_PART('DAY',current_date)-DATE_PART('DAY',pd_fecha_ini)))/372);
END;
$$;


ALTER FUNCTION public.fu_obtener_edad(pd_fecha_ini date, OUT pn_edad integer) OWNER TO mirosc;

--
-- TOC entry 242 (class 1255 OID 49236)
-- Name: fu_obtener_edad(date); Type: FUNCTION; Schema: sght; Owner: mirosc
--

CREATE FUNCTION sght.fu_obtener_edad(pd_fecha_ini date, OUT pn_edad integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$
BEGIN
pn_edad := FLOOR(((DATE_PART('YEAR',current_date)-DATE_PART('YEAR',pd_fecha_ini))* 372 + (DATE_PART('MONTH',current_date) - DATE_PART('MONTH',pd_fecha_ini))*31 + (DATE_PART('DAY',current_date)-DATE_PART('DAY',pd_fecha_ini)))/372);
END;
$$;


ALTER FUNCTION sght.fu_obtener_edad(pd_fecha_ini date, OUT pn_edad integer) OWNER TO mirosc;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 235 (class 1259 OID 49526)
-- Name: tarmazformamm; Type: TABLE; Schema: armaz; Owner: mirosc
--

CREATE TABLE armaz.tarmazformamm (
    cod_formammid integer NOT NULL,
    cnu_medida integer
);


ALTER TABLE armaz.tarmazformamm OWNER TO mirosc;

--
-- TOC entry 238 (class 1259 OID 49541)
-- Name: tarmazmaterial; Type: TABLE; Schema: armaz; Owner: mirosc
--

CREATE TABLE armaz.tarmazmaterial (
    cod_materialid integer NOT NULL,
    des_nbmaterial character varying(50)
);


ALTER TABLE armaz.tarmazmaterial OWNER TO mirosc;

--
-- TOC entry 236 (class 1259 OID 49531)
-- Name: tarmazpuentemm; Type: TABLE; Schema: armaz; Owner: mirosc
--

CREATE TABLE armaz.tarmazpuentemm (
    cod_puentemmid integer NOT NULL,
    cnu_medida integer
);


ALTER TABLE armaz.tarmazpuentemm OWNER TO mirosc;

--
-- TOC entry 237 (class 1259 OID 49536)
-- Name: tarmazterminalmm; Type: TABLE; Schema: armaz; Owner: mirosc
--

CREATE TABLE armaz.tarmazterminalmm (
    cod_terminalmmid integer NOT NULL,
    cnu_medida integer
);


ALTER TABLE armaz.tarmazterminalmm OWNER TO mirosc;

--
-- TOC entry 228 (class 1259 OID 49480)
-- Name: tcatcilindro; Type: TABLE; Schema: cat; Owner: mirosc
--

CREATE TABLE cat.tcatcilindro (
    cod_cilindroid integer NOT NULL,
    cnu_valor double precision
);


ALTER TABLE cat.tcatcilindro OWNER TO mirosc;

--
-- TOC entry 227 (class 1259 OID 49478)
-- Name: tcatcilindro_cod_cilindroid_seq; Type: SEQUENCE; Schema: cat; Owner: mirosc
--

CREATE SEQUENCE cat.tcatcilindro_cod_cilindroid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE cat.tcatcilindro_cod_cilindroid_seq OWNER TO mirosc;

--
-- TOC entry 3075 (class 0 OID 0)
-- Dependencies: 227
-- Name: tcatcilindro_cod_cilindroid_seq; Type: SEQUENCE OWNED BY; Schema: cat; Owner: mirosc
--

ALTER SEQUENCE cat.tcatcilindro_cod_cilindroid_seq OWNED BY cat.tcatcilindro.cod_cilindroid;


--
-- TOC entry 226 (class 1259 OID 49472)
-- Name: tcateje; Type: TABLE; Schema: cat; Owner: mirosc
--

CREATE TABLE cat.tcateje (
    cod_ejeid integer NOT NULL,
    cnu_valor integer
);


ALTER TABLE cat.tcateje OWNER TO mirosc;

--
-- TOC entry 225 (class 1259 OID 49470)
-- Name: tcateje_cod_ejeid_seq; Type: SEQUENCE; Schema: cat; Owner: mirosc
--

CREATE SEQUENCE cat.tcateje_cod_ejeid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE cat.tcateje_cod_ejeid_seq OWNER TO mirosc;

--
-- TOC entry 3076 (class 0 OID 0)
-- Dependencies: 225
-- Name: tcateje_cod_ejeid_seq; Type: SEQUENCE OWNED BY; Schema: cat; Owner: mirosc
--

ALTER SEQUENCE cat.tcateje_cod_ejeid_seq OWNED BY cat.tcateje.cod_ejeid;


--
-- TOC entry 230 (class 1259 OID 49488)
-- Name: tcatesferico; Type: TABLE; Schema: cat; Owner: mirosc
--

CREATE TABLE cat.tcatesferico (
    cod_esfericoid integer NOT NULL,
    cnu_valor double precision
);


ALTER TABLE cat.tcatesferico OWNER TO mirosc;

--
-- TOC entry 229 (class 1259 OID 49486)
-- Name: tcatesferico_cod_esfericoid_seq; Type: SEQUENCE; Schema: cat; Owner: mirosc
--

CREATE SEQUENCE cat.tcatesferico_cod_esfericoid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE cat.tcatesferico_cod_esfericoid_seq OWNER TO mirosc;

--
-- TOC entry 3077 (class 0 OID 0)
-- Dependencies: 229
-- Name: tcatesferico_cod_esfericoid_seq; Type: SEQUENCE OWNED BY; Schema: cat; Owner: mirosc
--

ALTER SEQUENCE cat.tcatesferico_cod_esfericoid_seq OWNED BY cat.tcatesferico.cod_esfericoid;


--
-- TOC entry 232 (class 1259 OID 49496)
-- Name: tcatvalorprueba; Type: TABLE; Schema: cat; Owner: mirosc
--

CREATE TABLE cat.tcatvalorprueba (
    cod_valorpruebaid integer NOT NULL,
    cnu_valor double precision
);


ALTER TABLE cat.tcatvalorprueba OWNER TO mirosc;

--
-- TOC entry 231 (class 1259 OID 49494)
-- Name: tcatvalorprueba_cod_valorpruebaid_seq; Type: SEQUENCE; Schema: cat; Owner: mirosc
--

CREATE SEQUENCE cat.tcatvalorprueba_cod_valorpruebaid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE cat.tcatvalorprueba_cod_valorpruebaid_seq OWNER TO mirosc;

--
-- TOC entry 3078 (class 0 OID 0)
-- Dependencies: 231
-- Name: tcatvalorprueba_cod_valorpruebaid_seq; Type: SEQUENCE OWNED BY; Schema: cat; Owner: mirosc
--

ALTER SEQUENCE cat.tcatvalorprueba_cod_valorpruebaid_seq OWNED BY cat.tcatvalorprueba.cod_valorpruebaid;


--
-- TOC entry 240 (class 1259 OID 49548)
-- Name: tinvenarmazon; Type: TABLE; Schema: inven; Owner: mirosc
--

CREATE TABLE inven.tinvenarmazon (
    cod_armazonid integer NOT NULL,
    des_nbmodelo character varying(20),
    des_nbsubmodelo character varying(20),
    imp_preciobase numeric(5,2),
    des_descripcorta text,
    des_descriplarga text,
    cod_formammid_fk integer,
    cod_puentemmid_fk integer,
    cod_terminammt_fk smallint
);


ALTER TABLE inven.tinvenarmazon OWNER TO mirosc;

--
-- TOC entry 239 (class 1259 OID 49546)
-- Name: tinvenarmazon_cod_armazonid_seq; Type: SEQUENCE; Schema: inven; Owner: mirosc
--

CREATE SEQUENCE inven.tinvenarmazon_cod_armazonid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE inven.tinvenarmazon_cod_armazonid_seq OWNER TO mirosc;

--
-- TOC entry 3079 (class 0 OID 0)
-- Dependencies: 239
-- Name: tinvenarmazon_cod_armazonid_seq; Type: SEQUENCE OWNED BY; Schema: inven; Owner: mirosc
--

ALTER SEQUENCE inven.tinvenarmazon_cod_armazonid_seq OWNED BY inven.tinvenarmazon.cod_armazonid;


--
-- TOC entry 234 (class 1259 OID 49518)
-- Name: tmicamaterial; Type: TABLE; Schema: mica; Owner: mirosc
--

CREATE TABLE mica.tmicamaterial (
    cod_materialid integer NOT NULL,
    des_nbmaterial bit varying(40)
);


ALTER TABLE mica.tmicamaterial OWNER TO mirosc;

--
-- TOC entry 233 (class 1259 OID 49516)
-- Name: tmicamaterial_cod_materialid_seq; Type: SEQUENCE; Schema: mica; Owner: mirosc
--

CREATE SEQUENCE mica.tmicamaterial_cod_materialid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE mica.tmicamaterial_cod_materialid_seq OWNER TO mirosc;

--
-- TOC entry 3080 (class 0 OID 0)
-- Dependencies: 233
-- Name: tmicamaterial_cod_materialid_seq; Type: SEQUENCE OWNED BY; Schema: mica; Owner: mirosc
--

ALTER SEQUENCE mica.tmicamaterial_cod_materialid_seq OWNED BY mica.tmicamaterial.cod_materialid;


--
-- TOC entry 201 (class 1259 OID 49237)
-- Name: tsghtantmedicofam; Type: TABLE; Schema: sght; Owner: mirosc
--

CREATE TABLE sght.tsghtantmedicofam (
    cod_antmedfam integer NOT NULL,
    cod_datogeneralid_fk integer,
    bol_diabetes boolean,
    bol_preionarter boolean,
    bol_glaucoma boolean,
    bol_cirugias boolean,
    bol_traumatismo boolean,
    bol_dolorcabeza boolean,
    bol_alergias boolean,
    bol_saludgeneral boolean,
    bol_status boolean DEFAULT true,
    cod_txid_fk integer
);


ALTER TABLE sght.tsghtantmedicofam OWNER TO mirosc;

--
-- TOC entry 202 (class 1259 OID 49241)
-- Name: tsghtantmedicofam_cod_antmedfam_seq; Type: SEQUENCE; Schema: sght; Owner: mirosc
--

CREATE SEQUENCE sght.tsghtantmedicofam_cod_antmedfam_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sght.tsghtantmedicofam_cod_antmedfam_seq OWNER TO mirosc;

--
-- TOC entry 3081 (class 0 OID 0)
-- Dependencies: 202
-- Name: tsghtantmedicofam_cod_antmedfam_seq; Type: SEQUENCE OWNED BY; Schema: sght; Owner: mirosc
--

ALTER SEQUENCE sght.tsghtantmedicofam_cod_antmedfam_seq OWNED BY sght.tsghtantmedicofam.cod_antmedfam;


--
-- TOC entry 203 (class 1259 OID 49243)
-- Name: tsghtantmedicopx_cod_antmedpxid_seq; Type: SEQUENCE; Schema: sght; Owner: mirosc
--

CREATE SEQUENCE sght.tsghtantmedicopx_cod_antmedpxid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;


ALTER TABLE sght.tsghtantmedicopx_cod_antmedpxid_seq OWNER TO mirosc;

--
-- TOC entry 204 (class 1259 OID 49245)
-- Name: tsghtantmedicopx; Type: TABLE; Schema: sght; Owner: mirosc
--

CREATE TABLE sght.tsghtantmedicopx (
    cod_datogeneralid_fk integer,
    des_fecultexammed character varying(50),
    bol_salud boolean,
    bol_diabetes boolean,
    bol_preionarter boolean,
    bol_glaucoma boolean,
    bol_cirugias boolean,
    txt_tx1 text,
    bol_traumatismo boolean,
    bol_dolorcabeza boolean,
    bol_alergias boolean,
    txt_tx2 text,
    bol_sermedicos boolean,
    txt_cuales text,
    bol_status boolean DEFAULT true,
    cod_antmedpxid integer DEFAULT nextval('sght.tsghtantmedicopx_cod_antmedpxid_seq'::regclass) NOT NULL
);


ALTER TABLE sght.tsghtantmedicopx OWNER TO mirosc;

--
-- TOC entry 205 (class 1259 OID 49253)
-- Name: tsghtantocularpx; Type: TABLE; Schema: sght; Owner: mirosc
--

CREATE TABLE sght.tsghtantocularpx (
    cod_antocularpxid integer NOT NULL,
    cod_datogeneralid_fk integer,
    bol_golpes boolean,
    txt_ubicacion1 text,
    txt_tx1 text,
    txt_ubicacion2 text,
    txt_tx2 text,
    txt_tx3 text,
    opcional text,
    bol_status boolean DEFAULT true,
    bol_infecciones boolean,
    bol_desviacion_o boolean
);


ALTER TABLE sght.tsghtantocularpx OWNER TO mirosc;

--
-- TOC entry 206 (class 1259 OID 49260)
-- Name: tsghtantocularpx_cod_antocularpxid_seq; Type: SEQUENCE; Schema: sght; Owner: mirosc
--

CREATE SEQUENCE sght.tsghtantocularpx_cod_antocularpxid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sght.tsghtantocularpx_cod_antocularpxid_seq OWNER TO mirosc;

--
-- TOC entry 3082 (class 0 OID 0)
-- Dependencies: 206
-- Name: tsghtantocularpx_cod_antocularpxid_seq; Type: SEQUENCE OWNED BY; Schema: sght; Owner: mirosc
--

ALTER SEQUENCE sght.tsghtantocularpx_cod_antocularpxid_seq OWNED BY sght.tsghtantocularpx.cod_antocularpxid;


--
-- TOC entry 207 (class 1259 OID 49262)
-- Name: tsghtantoculfam; Type: TABLE; Schema: sght; Owner: mirosc
--

CREATE TABLE sght.tsghtantoculfam (
    cod_antoculfamid integer NOT NULL,
    cod_datogeneralid_fk integer,
    bol_cataratas boolean,
    bol_glaucoma boolean,
    bol_cirugias boolean,
    bol_estrabismo boolean,
    bol_ametropias boolean,
    bol_queratocono boolean,
    bol_ceguera boolean,
    opcional text,
    bol_status boolean DEFAULT true,
    cod_txid_fk integer
);


ALTER TABLE sght.tsghtantoculfam OWNER TO mirosc;

--
-- TOC entry 208 (class 1259 OID 49269)
-- Name: tsghtantoculfam_cod_antoculfamid_seq; Type: SEQUENCE; Schema: sght; Owner: mirosc
--

CREATE SEQUENCE sght.tsghtantoculfam_cod_antoculfamid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sght.tsghtantoculfam_cod_antoculfamid_seq OWNER TO mirosc;

--
-- TOC entry 3083 (class 0 OID 0)
-- Dependencies: 208
-- Name: tsghtantoculfam_cod_antoculfamid_seq; Type: SEQUENCE OWNED BY; Schema: sght; Owner: mirosc
--

ALTER SEQUENCE sght.tsghtantoculfam_cod_antoculfamid_seq OWNED BY sght.tsghtantoculfam.cod_antoculfamid;


--
-- TOC entry 209 (class 1259 OID 49271)
-- Name: tsghtdatosgeneral; Type: TABLE; Schema: sght; Owner: mirosc
--

CREATE TABLE sght.tsghtdatosgeneral (
    cod_datogeneralid integer NOT NULL,
    cod_pacienteid_fk integer,
    fec_registro date,
    fec_nacimiento date,
    txt_hobbies text,
    des_calle character varying(50),
    cnu_numexterior integer,
    des_numinterior character varying(8),
    des_colonia character varying(20),
    cnu_cp integer,
    des_municipio character varying(40),
    des_estado character varying(20),
    email character varying(50),
    des_ocupacion character varying(50),
    txt_factores text,
    des_lugartrabajo character varying(30),
    des_opcional character varying(50),
    bol_status boolean DEFAULT true
);


ALTER TABLE sght.tsghtdatosgeneral OWNER TO mirosc;

--
-- TOC entry 210 (class 1259 OID 49278)
-- Name: tsghtdatosgeneral_cod_datogeneralid_seq; Type: SEQUENCE; Schema: sght; Owner: mirosc
--

CREATE SEQUENCE sght.tsghtdatosgeneral_cod_datogeneralid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sght.tsghtdatosgeneral_cod_datogeneralid_seq OWNER TO mirosc;

--
-- TOC entry 3084 (class 0 OID 0)
-- Dependencies: 210
-- Name: tsghtdatosgeneral_cod_datogeneralid_seq; Type: SEQUENCE OWNED BY; Schema: sght; Owner: mirosc
--

ALTER SEQUENCE sght.tsghtdatosgeneral_cod_datogeneralid_seq OWNED BY sght.tsghtdatosgeneral.cod_datogeneralid;


--
-- TOC entry 211 (class 1259 OID 49280)
-- Name: tsghtexamenpx; Type: TABLE; Schema: sght; Owner: mirosc
--

CREATE TABLE sght.tsghtexamenpx (
    cod_examenpxid integer NOT NULL,
    cod_datogeneralid_fk integer,
    des_ladoojo character(1),
    cnu_avl numeric(2,2),
    cnu_ph numeric(2,2),
    cnu_retinoscopia numeric(4,2),
    cnu_rx numeric(4,2),
    cnu_bicromatica numeric(2,2),
    cnu_reloj numeric(2,2),
    opcional integer,
    bol_status boolean DEFAULT true
);


ALTER TABLE sght.tsghtexamenpx OWNER TO mirosc;

--
-- TOC entry 212 (class 1259 OID 49284)
-- Name: tsghtexamenpx_cod_examenpxid_seq; Type: SEQUENCE; Schema: sght; Owner: mirosc
--

CREATE SEQUENCE sght.tsghtexamenpx_cod_examenpxid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sght.tsghtexamenpx_cod_examenpxid_seq OWNER TO mirosc;

--
-- TOC entry 3085 (class 0 OID 0)
-- Dependencies: 212
-- Name: tsghtexamenpx_cod_examenpxid_seq; Type: SEQUENCE OWNED BY; Schema: sght; Owner: mirosc
--

ALTER SEQUENCE sght.tsghtexamenpx_cod_examenpxid_seq OWNED BY sght.tsghtexamenpx.cod_examenpxid;


--
-- TOC entry 213 (class 1259 OID 49286)
-- Name: tsghtobservaciongen; Type: TABLE; Schema: sght; Owner: mirosc
--

CREATE TABLE sght.tsghtobservaciongen (
    cod_observaciongenid integer NOT NULL,
    cod_datogeneralid_fk integer,
    txt_anomalias text,
    txt_asimetrias text,
    txt_tx1 text,
    txt_obsocular text,
    txt_tx2 text,
    txt_comportamiento text,
    txt_otro text,
    opcional text,
    bol_status boolean DEFAULT true
);


ALTER TABLE sght.tsghtobservaciongen OWNER TO mirosc;

--
-- TOC entry 214 (class 1259 OID 49293)
-- Name: tsghtobservaciongen_cod_observaciongenid_seq; Type: SEQUENCE; Schema: sght; Owner: mirosc
--

CREATE SEQUENCE sght.tsghtobservaciongen_cod_observaciongenid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sght.tsghtobservaciongen_cod_observaciongenid_seq OWNER TO mirosc;

--
-- TOC entry 3086 (class 0 OID 0)
-- Dependencies: 214
-- Name: tsghtobservaciongen_cod_observaciongenid_seq; Type: SEQUENCE OWNED BY; Schema: sght; Owner: mirosc
--

ALTER SEQUENCE sght.tsghtobservaciongen_cod_observaciongenid_seq OWNED BY sght.tsghtobservaciongen.cod_observaciongenid;


--
-- TOC entry 215 (class 1259 OID 49295)
-- Name: tsghtpacientes; Type: TABLE; Schema: sght; Owner: mirosc
--

CREATE TABLE sght.tsghtpacientes (
    cod_pacienteid integer NOT NULL,
    des_nombre character varying(50) NOT NULL,
    des_paterno character varying(50),
    des_materno character varying(50),
    cnu_pais smallint DEFAULT 521,
    cnu_celular numeric,
    cnu_telcasa numeric,
    bol_whats boolean,
    bol_telcasa boolean,
    bol_activo boolean DEFAULT true
);


ALTER TABLE sght.tsghtpacientes OWNER TO mirosc;

--
-- TOC entry 216 (class 1259 OID 49303)
-- Name: tsghtpacientes_cod_pacienteid_seq; Type: SEQUENCE; Schema: sght; Owner: mirosc
--

CREATE SEQUENCE sght.tsghtpacientes_cod_pacienteid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sght.tsghtpacientes_cod_pacienteid_seq OWNER TO mirosc;

--
-- TOC entry 3087 (class 0 OID 0)
-- Dependencies: 216
-- Name: tsghtpacientes_cod_pacienteid_seq; Type: SEQUENCE OWNED BY; Schema: sght; Owner: mirosc
--

ALTER SEQUENCE sght.tsghtpacientes_cod_pacienteid_seq OWNED BY sght.tsghtpacientes.cod_pacienteid;


--
-- TOC entry 217 (class 1259 OID 49305)
-- Name: tsghtqueja; Type: TABLE; Schema: sght; Owner: mirosc
--

CREATE TABLE sght.tsghtqueja (
    cod_quejaid integer NOT NULL,
    cod_datogeneralid_fk integer,
    txt_causa text,
    txt_problema text,
    des_cuando character varying(50),
    bol_vlejana boolean,
    bol_vcercana boolean,
    bol_vdoble boolean,
    bol_ardor boolean,
    bol_comezon boolean,
    bol_secrecion boolean,
    bol_fatiga boolean,
    bol_epiforia boolean,
    bol_ojoseco boolean,
    bol_ojorojo boolean,
    bol_fotofobia boolean,
    bol_moscas boolean,
    bol_flashes boolean,
    bol_auras boolean,
    opcional integer,
    bol_status boolean DEFAULT true
);


ALTER TABLE sght.tsghtqueja OWNER TO mirosc;

--
-- TOC entry 218 (class 1259 OID 49312)
-- Name: tsghtqueja_cod_quejaid_seq; Type: SEQUENCE; Schema: sght; Owner: mirosc
--

CREATE SEQUENCE sght.tsghtqueja_cod_quejaid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sght.tsghtqueja_cod_quejaid_seq OWNER TO mirosc;

--
-- TOC entry 3088 (class 0 OID 0)
-- Dependencies: 218
-- Name: tsghtqueja_cod_quejaid_seq; Type: SEQUENCE OWNED BY; Schema: sght; Owner: mirosc
--

ALTER SEQUENCE sght.tsghtqueja_cod_quejaid_seq OWNED BY sght.tsghtqueja.cod_quejaid;


--
-- TOC entry 219 (class 1259 OID 49314)
-- Name: tsghtrxpaciente; Type: TABLE; Schema: sght; Owner: mirosc
--

CREATE TABLE sght.tsghtrxpaciente (
    cod_rxpaciente integer NOT NULL,
    des_ladoojo character(1),
    cnu_esferico numeric(2,2),
    cnu_cilindro numeric(2,2),
    cnu_eje integer,
    cnu_add integer,
    cnu_dnp integer,
    cnu_dip integer,
    cnu_altura integer,
    cnu_prisma numeric(4,2),
    opcional integer,
    bol_status boolean DEFAULT true,
    cod_datogeneralid_fk integer
);


ALTER TABLE sght.tsghtrxpaciente OWNER TO mirosc;

--
-- TOC entry 220 (class 1259 OID 49318)
-- Name: tsghtrxpaciente_cod_rxpaciente_seq; Type: SEQUENCE; Schema: sght; Owner: mirosc
--

CREATE SEQUENCE sght.tsghtrxpaciente_cod_rxpaciente_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sght.tsghtrxpaciente_cod_rxpaciente_seq OWNER TO mirosc;

--
-- TOC entry 3089 (class 0 OID 0)
-- Dependencies: 220
-- Name: tsghtrxpaciente_cod_rxpaciente_seq; Type: SEQUENCE OWNED BY; Schema: sght; Owner: mirosc
--

ALTER SEQUENCE sght.tsghtrxpaciente_cod_rxpaciente_seq OWNED BY sght.tsghtrxpaciente.cod_rxpaciente;


--
-- TOC entry 221 (class 1259 OID 49320)
-- Name: tsghtultimoexamen; Type: TABLE; Schema: sght; Owner: mirosc
--

CREATE TABLE sght.tsghtultimoexamen (
    cod_ultimoexamenid integer NOT NULL,
    cod_quejaid_fk integer,
    des_fechaaprox character varying(30),
    cnu_od numeric(4,2),
    cnu_oi numeric(4,2),
    cnu_add numeric(4,2),
    txt_anteojos text,
    bol_eficacia boolean,
    bol_comodidad boolean,
    bol_necesidad boolean,
    opcional character varying(50)
);


ALTER TABLE sght.tsghtultimoexamen OWNER TO mirosc;

--
-- TOC entry 222 (class 1259 OID 49326)
-- Name: tsghtultimoexamen_cod_ultimoexamenid_seq; Type: SEQUENCE; Schema: sght; Owner: mirosc
--

CREATE SEQUENCE sght.tsghtultimoexamen_cod_ultimoexamenid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sght.tsghtultimoexamen_cod_ultimoexamenid_seq OWNER TO mirosc;

--
-- TOC entry 3090 (class 0 OID 0)
-- Dependencies: 222
-- Name: tsghtultimoexamen_cod_ultimoexamenid_seq; Type: SEQUENCE OWNED BY; Schema: sght; Owner: mirosc
--

ALTER SEQUENCE sght.tsghtultimoexamen_cod_ultimoexamenid_seq OWNED BY sght.tsghtultimoexamen.cod_ultimoexamenid;


--
-- TOC entry 224 (class 1259 OID 49419)
-- Name: tsghtx; Type: TABLE; Schema: sght; Owner: mirosc
--

CREATE TABLE sght.tsghtx (
    cod_txid integer NOT NULL,
    txt_tx1 text,
    txt_tx2 text,
    txt_tx3 text,
    txt_tx4 text,
    txt_tx5 text,
    txt_tx6 text,
    txt_tx7 text,
    txt_tx8 text,
    txt_tx9 text,
    txt_tx10 text
);


ALTER TABLE sght.tsghtx OWNER TO mirosc;

--
-- TOC entry 223 (class 1259 OID 49417)
-- Name: tsghtx_cod_txid_seq; Type: SEQUENCE; Schema: sght; Owner: mirosc
--

CREATE SEQUENCE sght.tsghtx_cod_txid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sght.tsghtx_cod_txid_seq OWNER TO mirosc;

--
-- TOC entry 3091 (class 0 OID 0)
-- Dependencies: 223
-- Name: tsghtx_cod_txid_seq; Type: SEQUENCE OWNED BY; Schema: sght; Owner: mirosc
--

ALTER SEQUENCE sght.tsghtx_cod_txid_seq OWNED BY sght.tsghtx.cod_txid;


--
-- TOC entry 2845 (class 2604 OID 49483)
-- Name: tcatcilindro cod_cilindroid; Type: DEFAULT; Schema: cat; Owner: mirosc
--

ALTER TABLE ONLY cat.tcatcilindro ALTER COLUMN cod_cilindroid SET DEFAULT nextval('cat.tcatcilindro_cod_cilindroid_seq'::regclass);


--
-- TOC entry 2844 (class 2604 OID 49475)
-- Name: tcateje cod_ejeid; Type: DEFAULT; Schema: cat; Owner: mirosc
--

ALTER TABLE ONLY cat.tcateje ALTER COLUMN cod_ejeid SET DEFAULT nextval('cat.tcateje_cod_ejeid_seq'::regclass);


--
-- TOC entry 2846 (class 2604 OID 49491)
-- Name: tcatesferico cod_esfericoid; Type: DEFAULT; Schema: cat; Owner: mirosc
--

ALTER TABLE ONLY cat.tcatesferico ALTER COLUMN cod_esfericoid SET DEFAULT nextval('cat.tcatesferico_cod_esfericoid_seq'::regclass);


--
-- TOC entry 2847 (class 2604 OID 49499)
-- Name: tcatvalorprueba cod_valorpruebaid; Type: DEFAULT; Schema: cat; Owner: mirosc
--

ALTER TABLE ONLY cat.tcatvalorprueba ALTER COLUMN cod_valorpruebaid SET DEFAULT nextval('cat.tcatvalorprueba_cod_valorpruebaid_seq'::regclass);


--
-- TOC entry 2849 (class 2604 OID 49551)
-- Name: tinvenarmazon cod_armazonid; Type: DEFAULT; Schema: inven; Owner: mirosc
--

ALTER TABLE ONLY inven.tinvenarmazon ALTER COLUMN cod_armazonid SET DEFAULT nextval('inven.tinvenarmazon_cod_armazonid_seq'::regclass);


--
-- TOC entry 2848 (class 2604 OID 49521)
-- Name: tmicamaterial cod_materialid; Type: DEFAULT; Schema: mica; Owner: mirosc
--

ALTER TABLE ONLY mica.tmicamaterial ALTER COLUMN cod_materialid SET DEFAULT nextval('mica.tmicamaterial_cod_materialid_seq'::regclass);


--
-- TOC entry 2822 (class 2604 OID 49328)
-- Name: tsghtantmedicofam cod_antmedfam; Type: DEFAULT; Schema: sght; Owner: mirosc
--

ALTER TABLE ONLY sght.tsghtantmedicofam ALTER COLUMN cod_antmedfam SET DEFAULT nextval('sght.tsghtantmedicofam_cod_antmedfam_seq'::regclass);


--
-- TOC entry 2826 (class 2604 OID 49329)
-- Name: tsghtantocularpx cod_antocularpxid; Type: DEFAULT; Schema: sght; Owner: mirosc
--

ALTER TABLE ONLY sght.tsghtantocularpx ALTER COLUMN cod_antocularpxid SET DEFAULT nextval('sght.tsghtantocularpx_cod_antocularpxid_seq'::regclass);


--
-- TOC entry 2828 (class 2604 OID 49330)
-- Name: tsghtantoculfam cod_antoculfamid; Type: DEFAULT; Schema: sght; Owner: mirosc
--

ALTER TABLE ONLY sght.tsghtantoculfam ALTER COLUMN cod_antoculfamid SET DEFAULT nextval('sght.tsghtantoculfam_cod_antoculfamid_seq'::regclass);


--
-- TOC entry 2830 (class 2604 OID 49331)
-- Name: tsghtdatosgeneral cod_datogeneralid; Type: DEFAULT; Schema: sght; Owner: mirosc
--

ALTER TABLE ONLY sght.tsghtdatosgeneral ALTER COLUMN cod_datogeneralid SET DEFAULT nextval('sght.tsghtdatosgeneral_cod_datogeneralid_seq'::regclass);


--
-- TOC entry 2832 (class 2604 OID 49332)
-- Name: tsghtexamenpx cod_examenpxid; Type: DEFAULT; Schema: sght; Owner: mirosc
--

ALTER TABLE ONLY sght.tsghtexamenpx ALTER COLUMN cod_examenpxid SET DEFAULT nextval('sght.tsghtexamenpx_cod_examenpxid_seq'::regclass);


--
-- TOC entry 2834 (class 2604 OID 49333)
-- Name: tsghtobservaciongen cod_observaciongenid; Type: DEFAULT; Schema: sght; Owner: mirosc
--

ALTER TABLE ONLY sght.tsghtobservaciongen ALTER COLUMN cod_observaciongenid SET DEFAULT nextval('sght.tsghtobservaciongen_cod_observaciongenid_seq'::regclass);


--
-- TOC entry 2837 (class 2604 OID 49334)
-- Name: tsghtpacientes cod_pacienteid; Type: DEFAULT; Schema: sght; Owner: mirosc
--

ALTER TABLE ONLY sght.tsghtpacientes ALTER COLUMN cod_pacienteid SET DEFAULT nextval('sght.tsghtpacientes_cod_pacienteid_seq'::regclass);


--
-- TOC entry 2839 (class 2604 OID 49335)
-- Name: tsghtqueja cod_quejaid; Type: DEFAULT; Schema: sght; Owner: mirosc
--

ALTER TABLE ONLY sght.tsghtqueja ALTER COLUMN cod_quejaid SET DEFAULT nextval('sght.tsghtqueja_cod_quejaid_seq'::regclass);


--
-- TOC entry 2841 (class 2604 OID 49336)
-- Name: tsghtrxpaciente cod_rxpaciente; Type: DEFAULT; Schema: sght; Owner: mirosc
--

ALTER TABLE ONLY sght.tsghtrxpaciente ALTER COLUMN cod_rxpaciente SET DEFAULT nextval('sght.tsghtrxpaciente_cod_rxpaciente_seq'::regclass);


--
-- TOC entry 2842 (class 2604 OID 49337)
-- Name: tsghtultimoexamen cod_ultimoexamenid; Type: DEFAULT; Schema: sght; Owner: mirosc
--

ALTER TABLE ONLY sght.tsghtultimoexamen ALTER COLUMN cod_ultimoexamenid SET DEFAULT nextval('sght.tsghtultimoexamen_cod_ultimoexamenid_seq'::regclass);


--
-- TOC entry 2843 (class 2604 OID 49422)
-- Name: tsghtx cod_txid; Type: DEFAULT; Schema: sght; Owner: mirosc
--

ALTER TABLE ONLY sght.tsghtx ALTER COLUMN cod_txid SET DEFAULT nextval('sght.tsghtx_cod_txid_seq'::regclass);


--
-- TOC entry 3064 (class 0 OID 49526)
-- Dependencies: 235
-- Data for Name: tarmazformamm; Type: TABLE DATA; Schema: armaz; Owner: mirosc
--



--
-- TOC entry 3067 (class 0 OID 49541)
-- Dependencies: 238
-- Data for Name: tarmazmaterial; Type: TABLE DATA; Schema: armaz; Owner: mirosc
--



--
-- TOC entry 3065 (class 0 OID 49531)
-- Dependencies: 236
-- Data for Name: tarmazpuentemm; Type: TABLE DATA; Schema: armaz; Owner: mirosc
--



--
-- TOC entry 3066 (class 0 OID 49536)
-- Dependencies: 237
-- Data for Name: tarmazterminalmm; Type: TABLE DATA; Schema: armaz; Owner: mirosc
--



--
-- TOC entry 3057 (class 0 OID 49480)
-- Dependencies: 228
-- Data for Name: tcatcilindro; Type: TABLE DATA; Schema: cat; Owner: mirosc
--

INSERT INTO cat.tcatcilindro (cod_cilindroid, cnu_valor) VALUES (1, -8);
INSERT INTO cat.tcatcilindro (cod_cilindroid, cnu_valor) VALUES (2, -7.75);
INSERT INTO cat.tcatcilindro (cod_cilindroid, cnu_valor) VALUES (3, -7.5);
INSERT INTO cat.tcatcilindro (cod_cilindroid, cnu_valor) VALUES (4, -7.25);
INSERT INTO cat.tcatcilindro (cod_cilindroid, cnu_valor) VALUES (5, -7);
INSERT INTO cat.tcatcilindro (cod_cilindroid, cnu_valor) VALUES (6, -6.75);
INSERT INTO cat.tcatcilindro (cod_cilindroid, cnu_valor) VALUES (7, -6.5);
INSERT INTO cat.tcatcilindro (cod_cilindroid, cnu_valor) VALUES (8, -6.25);
INSERT INTO cat.tcatcilindro (cod_cilindroid, cnu_valor) VALUES (9, -5);
INSERT INTO cat.tcatcilindro (cod_cilindroid, cnu_valor) VALUES (10, -5.75);
INSERT INTO cat.tcatcilindro (cod_cilindroid, cnu_valor) VALUES (11, -5.5);
INSERT INTO cat.tcatcilindro (cod_cilindroid, cnu_valor) VALUES (12, -5.25);
INSERT INTO cat.tcatcilindro (cod_cilindroid, cnu_valor) VALUES (13, -4);


--
-- TOC entry 3055 (class 0 OID 49472)
-- Dependencies: 226
-- Data for Name: tcateje; Type: TABLE DATA; Schema: cat; Owner: mirosc
--

INSERT INTO cat.tcateje (cod_ejeid, cnu_valor) VALUES (1, 0);
INSERT INTO cat.tcateje (cod_ejeid, cnu_valor) VALUES (2, 5);
INSERT INTO cat.tcateje (cod_ejeid, cnu_valor) VALUES (3, 10);
INSERT INTO cat.tcateje (cod_ejeid, cnu_valor) VALUES (4, 15);
INSERT INTO cat.tcateje (cod_ejeid, cnu_valor) VALUES (5, 20);
INSERT INTO cat.tcateje (cod_ejeid, cnu_valor) VALUES (6, 25);
INSERT INTO cat.tcateje (cod_ejeid, cnu_valor) VALUES (7, 30);
INSERT INTO cat.tcateje (cod_ejeid, cnu_valor) VALUES (8, 35);
INSERT INTO cat.tcateje (cod_ejeid, cnu_valor) VALUES (9, 40);
INSERT INTO cat.tcateje (cod_ejeid, cnu_valor) VALUES (10, 45);


--
-- TOC entry 3059 (class 0 OID 49488)
-- Dependencies: 230
-- Data for Name: tcatesferico; Type: TABLE DATA; Schema: cat; Owner: mirosc
--



--
-- TOC entry 3061 (class 0 OID 49496)
-- Dependencies: 232
-- Data for Name: tcatvalorprueba; Type: TABLE DATA; Schema: cat; Owner: mirosc
--

INSERT INTO cat.tcatvalorprueba (cod_valorpruebaid, cnu_valor) VALUES (1, 0.0500000000000000028);
INSERT INTO cat.tcatvalorprueba (cod_valorpruebaid, cnu_valor) VALUES (2, 0.100000000000000006);
INSERT INTO cat.tcatvalorprueba (cod_valorpruebaid, cnu_valor) VALUES (3, 0.149999999999999994);
INSERT INTO cat.tcatvalorprueba (cod_valorpruebaid, cnu_valor) VALUES (4, 0.200000000000000011);
INSERT INTO cat.tcatvalorprueba (cod_valorpruebaid, cnu_valor) VALUES (5, 0.299999999999999989);
INSERT INTO cat.tcatvalorprueba (cod_valorpruebaid, cnu_valor) VALUES (6, 0.400000000000000022);
INSERT INTO cat.tcatvalorprueba (cod_valorpruebaid, cnu_valor) VALUES (7, 0.5);
INSERT INTO cat.tcatvalorprueba (cod_valorpruebaid, cnu_valor) VALUES (8, 0.599999999999999978);
INSERT INTO cat.tcatvalorprueba (cod_valorpruebaid, cnu_valor) VALUES (9, 0.699999999999999956);
INSERT INTO cat.tcatvalorprueba (cod_valorpruebaid, cnu_valor) VALUES (10, 0.800000000000000044);
INSERT INTO cat.tcatvalorprueba (cod_valorpruebaid, cnu_valor) VALUES (11, 0.900000000000000022);
INSERT INTO cat.tcatvalorprueba (cod_valorpruebaid, cnu_valor) VALUES (12, 1);
INSERT INTO cat.tcatvalorprueba (cod_valorpruebaid, cnu_valor) VALUES (13, 1.19999999999999996);
INSERT INTO cat.tcatvalorprueba (cod_valorpruebaid, cnu_valor) VALUES (14, 1.5);


--
-- TOC entry 3069 (class 0 OID 49548)
-- Dependencies: 240
-- Data for Name: tinvenarmazon; Type: TABLE DATA; Schema: inven; Owner: mirosc
--



--
-- TOC entry 3063 (class 0 OID 49518)
-- Dependencies: 234
-- Data for Name: tmicamaterial; Type: TABLE DATA; Schema: mica; Owner: mirosc
--



--
-- TOC entry 3030 (class 0 OID 49237)
-- Dependencies: 201
-- Data for Name: tsghtantmedicofam; Type: TABLE DATA; Schema: sght; Owner: mirosc
--

INSERT INTO sght.tsghtantmedicofam (cod_antmedfam, cod_datogeneralid_fk, bol_diabetes, bol_preionarter, bol_glaucoma, bol_cirugias, bol_traumatismo, bol_dolorcabeza, bol_alergias, bol_saludgeneral, bol_status, cod_txid_fk) VALUES (1, 23, NULL, true, true, NULL, NULL, NULL, true, NULL, true, NULL);
INSERT INTO sght.tsghtantmedicofam (cod_antmedfam, cod_datogeneralid_fk, bol_diabetes, bol_preionarter, bol_glaucoma, bol_cirugias, bol_traumatismo, bol_dolorcabeza, bol_alergias, bol_saludgeneral, bol_status, cod_txid_fk) VALUES (2, 24, NULL, true, true, NULL, NULL, true, true, NULL, true, NULL);
INSERT INTO sght.tsghtantmedicofam (cod_antmedfam, cod_datogeneralid_fk, bol_diabetes, bol_preionarter, bol_glaucoma, bol_cirugias, bol_traumatismo, bol_dolorcabeza, bol_alergias, bol_saludgeneral, bol_status, cod_txid_fk) VALUES (3, 27, true, NULL, NULL, NULL, NULL, NULL, NULL, NULL, true, NULL);


--
-- TOC entry 3033 (class 0 OID 49245)
-- Dependencies: 204
-- Data for Name: tsghtantmedicopx; Type: TABLE DATA; Schema: sght; Owner: mirosc
--

INSERT INTO sght.tsghtantmedicopx (cod_datogeneralid_fk, des_fecultexammed, bol_salud, bol_diabetes, bol_preionarter, bol_glaucoma, bol_cirugias, txt_tx1, bol_traumatismo, bol_dolorcabeza, bol_alergias, txt_tx2, bol_sermedicos, txt_cuales, bol_status, cod_antmedpxid) VALUES (23, '2019-07-10T05:00:00.000Z', NULL, true, true, NULL, NULL, NULL, true, NULL, NULL, NULL, NULL, NULL, true, 1);
INSERT INTO sght.tsghtantmedicopx (cod_datogeneralid_fk, des_fecultexammed, bol_salud, bol_diabetes, bol_preionarter, bol_glaucoma, bol_cirugias, txt_tx1, bol_traumatismo, bol_dolorcabeza, bol_alergias, txt_tx2, bol_sermedicos, txt_cuales, bol_status, cod_antmedpxid) VALUES (24, '2019-07-01T05:00:00.000Z', NULL, NULL, true, NULL, true, 'sdfs', NULL, NULL, true, 'fsdf', NULL, NULL, true, 2);
INSERT INTO sght.tsghtantmedicopx (cod_datogeneralid_fk, des_fecultexammed, bol_salud, bol_diabetes, bol_preionarter, bol_glaucoma, bol_cirugias, txt_tx1, bol_traumatismo, bol_dolorcabeza, bol_alergias, txt_tx2, bol_sermedicos, txt_cuales, bol_status, cod_antmedpxid) VALUES (27, '2019-04-04T06:00:00.000Z', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, true, 3);


--
-- TOC entry 3034 (class 0 OID 49253)
-- Dependencies: 205
-- Data for Name: tsghtantocularpx; Type: TABLE DATA; Schema: sght; Owner: mirosc
--

INSERT INTO sght.tsghtantocularpx (cod_antocularpxid, cod_datogeneralid_fk, bol_golpes, txt_ubicacion1, txt_tx1, txt_ubicacion2, txt_tx2, txt_tx3, opcional, bol_status, bol_infecciones, bol_desviacion_o) VALUES (1, 22, true, 'erv', 'erv', NULL, NULL, NULL, NULL, true, NULL, NULL);
INSERT INTO sght.tsghtantocularpx (cod_antocularpxid, cod_datogeneralid_fk, bol_golpes, txt_ubicacion1, txt_tx1, txt_ubicacion2, txt_tx2, txt_tx3, opcional, bol_status, bol_infecciones, bol_desviacion_o) VALUES (2, 22, true, 'erv', 'erv', NULL, NULL, NULL, NULL, true, NULL, NULL);
INSERT INTO sght.tsghtantocularpx (cod_antocularpxid, cod_datogeneralid_fk, bol_golpes, txt_ubicacion1, txt_tx1, txt_ubicacion2, txt_tx2, txt_tx3, opcional, bol_status, bol_infecciones, bol_desviacion_o) VALUES (3, 23, true, 'crec', 'crecc', NULL, NULL, 'recc', NULL, true, NULL, NULL);
INSERT INTO sght.tsghtantocularpx (cod_antocularpxid, cod_datogeneralid_fk, bol_golpes, txt_ubicacion1, txt_tx1, txt_ubicacion2, txt_tx2, txt_tx3, opcional, bol_status, bol_infecciones, bol_desviacion_o) VALUES (4, 24, true, 'sdfsd', 'sdfs', NULL, NULL, NULL, NULL, true, NULL, NULL);
INSERT INTO sght.tsghtantocularpx (cod_antocularpxid, cod_datogeneralid_fk, bol_golpes, txt_ubicacion1, txt_tx1, txt_ubicacion2, txt_tx2, txt_tx3, opcional, bol_status, bol_infecciones, bol_desviacion_o) VALUES (5, 27, false, NULL, NULL, NULL, NULL, NULL, NULL, true, NULL, NULL);


--
-- TOC entry 3036 (class 0 OID 49262)
-- Dependencies: 207
-- Data for Name: tsghtantoculfam; Type: TABLE DATA; Schema: sght; Owner: mirosc
--

INSERT INTO sght.tsghtantoculfam (cod_antoculfamid, cod_datogeneralid_fk, bol_cataratas, bol_glaucoma, bol_cirugias, bol_estrabismo, bol_ametropias, bol_queratocono, bol_ceguera, opcional, bol_status, cod_txid_fk) VALUES (1, 23, NULL, true, NULL, NULL, true, true, NULL, NULL, true, NULL);
INSERT INTO sght.tsghtantoculfam (cod_antoculfamid, cod_datogeneralid_fk, bol_cataratas, bol_glaucoma, bol_cirugias, bol_estrabismo, bol_ametropias, bol_queratocono, bol_ceguera, opcional, bol_status, cod_txid_fk) VALUES (2, 24, true, NULL, NULL, NULL, true, true, true, NULL, true, NULL);
INSERT INTO sght.tsghtantoculfam (cod_antoculfamid, cod_datogeneralid_fk, bol_cataratas, bol_glaucoma, bol_cirugias, bol_estrabismo, bol_ametropias, bol_queratocono, bol_ceguera, opcional, bol_status, cod_txid_fk) VALUES (3, 27, NULL, true, NULL, NULL, NULL, NULL, NULL, NULL, true, NULL);


--
-- TOC entry 3038 (class 0 OID 49271)
-- Dependencies: 209
-- Data for Name: tsghtdatosgeneral; Type: TABLE DATA; Schema: sght; Owner: mirosc
--

INSERT INTO sght.tsghtdatosgeneral (cod_datogeneralid, cod_pacienteid_fk, fec_registro, fec_nacimiento, txt_hobbies, des_calle, cnu_numexterior, des_numinterior, des_colonia, cnu_cp, des_municipio, des_estado, email, des_ocupacion, txt_factores, des_lugartrabajo, des_opcional, bol_status) VALUES (22, 3, '2019-07-24', '2019-07-03', 'efre', 'fsfd', 3443, 'efr', NULL, 34323, 'frdfd', 'rf', 'jfn@fdj.cd', 'crcr', 'crcre', NULL, NULL, true);
INSERT INTO sght.tsghtdatosgeneral (cod_datogeneralid, cod_pacienteid_fk, fec_registro, fec_nacimiento, txt_hobbies, des_calle, cnu_numexterior, des_numinterior, des_colonia, cnu_cp, des_municipio, des_estado, email, des_ocupacion, txt_factores, des_lugartrabajo, des_opcional, bol_status) VALUES (23, 4, '2019-07-24', '2019-07-10', 'efefs', 'ewf', 34, 'wef', NULL, 43343, 'refrefer', 'efrefre', 'fre@jn.comd', 'crecre', 'crecre', NULL, NULL, true);
INSERT INTO sght.tsghtdatosgeneral (cod_datogeneralid, cod_pacienteid_fk, fec_registro, fec_nacimiento, txt_hobbies, des_calle, cnu_numexterior, des_numinterior, des_colonia, cnu_cp, des_municipio, des_estado, email, des_ocupacion, txt_factores, des_lugartrabajo, des_opcional, bol_status) VALUES (24, 5, '2019-07-24', '2019-07-02', 'ddr', 'frf', 643534, '4r', NULL, 34545, 'frefe', 'erfe', 'mirosc@cb.comd', 'fc', 'rffdr', NULL, NULL, true);
INSERT INTO sght.tsghtdatosgeneral (cod_datogeneralid, cod_pacienteid_fk, fec_registro, fec_nacimiento, txt_hobbies, des_calle, cnu_numexterior, des_numinterior, des_colonia, cnu_cp, des_municipio, des_estado, email, des_ocupacion, txt_factores, des_lugartrabajo, des_opcional, bol_status) VALUES (25, 2, '2019-08-19', '2019-08-01', 'dcccd', 'scdc', 4, 'sd', NULL, 33414, 'dwed', 'edwwe', 'cre@crc.com', 'csd', 'cds', NULL, NULL, true);
INSERT INTO sght.tsghtdatosgeneral (cod_datogeneralid, cod_pacienteid_fk, fec_registro, fec_nacimiento, txt_hobbies, des_calle, cnu_numexterior, des_numinterior, des_colonia, cnu_cp, des_municipio, des_estado, email, des_ocupacion, txt_factores, des_lugartrabajo, des_opcional, bol_status) VALUES (26, 5, '2019-08-19', '2019-08-05', 'sds', 'sdc', 34, '4', NULL, 44544, 'dvcdf', 'dfc', 'dcfd@gd.com', 'dfc', 'dfc', NULL, NULL, true);
INSERT INTO sght.tsghtdatosgeneral (cod_datogeneralid, cod_pacienteid_fk, fec_registro, fec_nacimiento, txt_hobbies, des_calle, cnu_numexterior, des_numinterior, des_colonia, cnu_cp, des_municipio, des_estado, email, des_ocupacion, txt_factores, des_lugartrabajo, des_opcional, bol_status) VALUES (21, 2, '2019-07-24', '2007-12-04', 'rfrfr', 'erf', 34, '43', NULL, 34453, 'frrrrrrrrrrr', 'ferf', 'mfr@jfkmf.com', 'rjnv', 'jenj', NULL, NULL, true);
INSERT INTO sght.tsghtdatosgeneral (cod_datogeneralid, cod_pacienteid_fk, fec_registro, fec_nacimiento, txt_hobbies, des_calle, cnu_numexterior, des_numinterior, des_colonia, cnu_cp, des_municipio, des_estado, email, des_ocupacion, txt_factores, des_lugartrabajo, des_opcional, bol_status) VALUES (27, 6, '2019-08-24', '1990-02-07', 'jugar', 'av reforma', 7, NULL, NULL, 90000, 'tlaxcala', 'tlaxcala', 'micorreo@hotmail.com', 'trabajador domestico', 'diabetes', NULL, NULL, true);
INSERT INTO sght.tsghtdatosgeneral (cod_datogeneralid, cod_pacienteid_fk, fec_registro, fec_nacimiento, txt_hobbies, des_calle, cnu_numexterior, des_numinterior, des_colonia, cnu_cp, des_municipio, des_estado, email, des_ocupacion, txt_factores, des_lugartrabajo, des_opcional, bol_status) VALUES (28, 7, '2019-09-28', '1987-03-14', 'bailar', 'av juarez', 46, NULL, NULL, 90000, 'tlaxcala', 'tlaxcala', 'opticamiros@hotmail.com', 'empledo', 'montar a caballo', NULL, NULL, true);
INSERT INTO sght.tsghtdatosgeneral (cod_datogeneralid, cod_pacienteid_fk, fec_registro, fec_nacimiento, txt_hobbies, des_calle, cnu_numexterior, des_numinterior, des_colonia, cnu_cp, des_municipio, des_estado, email, des_ocupacion, txt_factores, des_lugartrabajo, des_opcional, bol_status) VALUES (52, 31, '2019-09-28', '2018-10-04', 'dvdsvs', 'vsdvsd', 121, NULL, NULL, 54123, 'sbdkks', 'sdkbvksb', 'jsdvbk@nsdkn.com', 'cksk', 'cdsbkbsd', NULL, NULL, true);
INSERT INTO sght.tsghtdatosgeneral (cod_datogeneralid, cod_pacienteid_fk, fec_registro, fec_nacimiento, txt_hobbies, des_calle, cnu_numexterior, des_numinterior, des_colonia, cnu_cp, des_municipio, des_estado, email, des_ocupacion, txt_factores, des_lugartrabajo, des_opcional, bol_status) VALUES (57, 36, '2019-09-28', '2007-07-28', 'c akjckj', 'jcnskjnk', 464, NULL, NULL, 56444, 'klcaklcn', 'snclsn', 'mslknc@nsjdvn.com', 'sjdnks', 'ckjdsbh', NULL, NULL, true);
INSERT INTO sght.tsghtdatosgeneral (cod_datogeneralid, cod_pacienteid_fk, fec_registro, fec_nacimiento, txt_hobbies, des_calle, cnu_numexterior, des_numinterior, des_colonia, cnu_cp, des_municipio, des_estado, email, des_ocupacion, txt_factores, des_lugartrabajo, des_opcional, bol_status) VALUES (58, 37, '2019-09-28', '2019-09-24', 'sbch', 'kjdbskb', 464, NULL, NULL, 64646, 'kjsckjsbk', 'sdknvj', 'ncsn@dj.co', 'sbdch', 'kjdsbk', NULL, NULL, true);
INSERT INTO sght.tsghtdatosgeneral (cod_datogeneralid, cod_pacienteid_fk, fec_registro, fec_nacimiento, txt_hobbies, des_calle, cnu_numexterior, des_numinterior, des_colonia, cnu_cp, des_municipio, des_estado, email, des_ocupacion, txt_factores, des_lugartrabajo, des_opcional, bol_status) VALUES (59, 38, '2019-10-01', '2019-10-07', 'fdssdvd', 'dfv', 43, NULL, NULL, 3242, 'cdc', 'crcs', 'njkdn@dvk.co', 'kmckd', 'jsjc', NULL, NULL, true);
INSERT INTO sght.tsghtdatosgeneral (cod_datogeneralid, cod_pacienteid_fk, fec_registro, fec_nacimiento, txt_hobbies, des_calle, cnu_numexterior, des_numinterior, des_colonia, cnu_cp, des_municipio, des_estado, email, des_ocupacion, txt_factores, des_lugartrabajo, des_opcional, bol_status) VALUES (60, 51, '2019-10-01', '2019-10-07', 'frrd', 'efre', 453534, NULL, NULL, 34534, 'frfdfd', 'dfrf', '53454@dg.c', 'frf', 'dfrrd', NULL, NULL, true);
INSERT INTO sght.tsghtdatosgeneral (cod_datogeneralid, cod_pacienteid_fk, fec_registro, fec_nacimiento, txt_hobbies, des_calle, cnu_numexterior, des_numinterior, des_colonia, cnu_cp, des_municipio, des_estado, email, des_ocupacion, txt_factores, des_lugartrabajo, des_opcional, bol_status) VALUES (61, 52, '2019-10-01', '2019-10-09', '34r', 'fwef34', 34, NULL, NULL, 34344, 'sfsef', 'sfef', 'sf@fs.c', 'fsdf', 'sfsf', NULL, NULL, true);
INSERT INTO sght.tsghtdatosgeneral (cod_datogeneralid, cod_pacienteid_fk, fec_registro, fec_nacimiento, txt_hobbies, des_calle, cnu_numexterior, des_numinterior, des_colonia, cnu_cp, des_municipio, des_estado, email, des_ocupacion, txt_factores, des_lugartrabajo, des_opcional, bol_status) VALUES (63, 54, '2019-10-01', '2019-10-09', '34r', 'fwef34', 34, NULL, NULL, 34344, 'sfsef', 'sfef', 'sf@fs.ca', 'fsdf', 'sfsf', NULL, NULL, true);


--
-- TOC entry 3040 (class 0 OID 49280)
-- Dependencies: 211
-- Data for Name: tsghtexamenpx; Type: TABLE DATA; Schema: sght; Owner: mirosc
--



--
-- TOC entry 3042 (class 0 OID 49286)
-- Dependencies: 213
-- Data for Name: tsghtobservaciongen; Type: TABLE DATA; Schema: sght; Owner: mirosc
--

INSERT INTO sght.tsghtobservaciongen (cod_observaciongenid, cod_datogeneralid_fk, txt_anomalias, txt_asimetrias, txt_tx1, txt_obsocular, txt_tx2, txt_comportamiento, txt_otro, opcional, bol_status) VALUES (1, 23, 'vf', 'dfvd', NULL, 'dfv', NULL, 'dfv', 'dfv', NULL, true);
INSERT INTO sght.tsghtobservaciongen (cod_observaciongenid, cod_datogeneralid_fk, txt_anomalias, txt_asimetrias, txt_tx1, txt_obsocular, txt_tx2, txt_comportamiento, txt_otro, opcional, bol_status) VALUES (2, 24, 'sdfsdf', 'fdsf', NULL, 'sdfs', NULL, 'sdf', 'sdfss', NULL, true);
INSERT INTO sght.tsghtobservaciongen (cod_observaciongenid, cod_datogeneralid_fk, txt_anomalias, txt_asimetrias, txt_tx1, txt_obsocular, txt_tx2, txt_comportamiento, txt_otro, opcional, bol_status) VALUES (3, 27, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, true);


--
-- TOC entry 3044 (class 0 OID 49295)
-- Dependencies: 215
-- Data for Name: tsghtpacientes; Type: TABLE DATA; Schema: sght; Owner: mirosc
--

INSERT INTO sght.tsghtpacientes (cod_pacienteid, des_nombre, des_paterno, des_materno, cnu_pais, cnu_celular, cnu_telcasa, bol_whats, bol_telcasa, bol_activo) VALUES (2, 'brenda', 'test', 'test2', 521, 324234324, 3454358593, true, true, true);
INSERT INTO sght.tsghtpacientes (cod_pacienteid, des_nombre, des_paterno, des_materno, cnu_pais, cnu_celular, cnu_telcasa, bol_whats, bol_telcasa, bol_activo) VALUES (3, 'bren', 'dew', 'ewd', 521, 3454353454, 3435345435, true, true, true);
INSERT INTO sght.tsghtpacientes (cod_pacienteid, des_nombre, des_paterno, des_materno, cnu_pais, cnu_celular, cnu_telcasa, bol_whats, bol_telcasa, bol_activo) VALUES (4, 'csc', 'dfv', 'sdvsd', 521, 3443434334, 43343434, true, true, true);
INSERT INTO sght.tsghtpacientes (cod_pacienteid, des_nombre, des_paterno, des_materno, cnu_pais, cnu_celular, cnu_telcasa, bol_whats, bol_telcasa, bol_activo) VALUES (5, 'mi', 'pe', 'lalo', 521, 3243243432, 4245235352, true, true, true);
INSERT INTO sght.tsghtpacientes (cod_pacienteid, des_nombre, des_paterno, des_materno, cnu_pais, cnu_celular, cnu_telcasa, bol_whats, bol_telcasa, bol_activo) VALUES (6, 'oscar', 'chavez', 'lopez', 521, 1234567890, 1234567890, true, true, true);
INSERT INTO sght.tsghtpacientes (cod_pacienteid, des_nombre, des_paterno, des_materno, cnu_pais, cnu_celular, cnu_telcasa, bol_whats, bol_telcasa, bol_activo) VALUES (7, 'maria linda', 'prueba', 'numero uno', 521, 2411255695, 2411245659, true, true, true);
INSERT INTO sght.tsghtpacientes (cod_pacienteid, des_nombre, des_paterno, des_materno, cnu_pais, cnu_celular, cnu_telcasa, bol_whats, bol_telcasa, bol_activo) VALUES (8, 'maria linda', 'prueba', 'numero uno', 521, 2411255695, 2411245659, true, true, true);
INSERT INTO sght.tsghtpacientes (cod_pacienteid, des_nombre, des_paterno, des_materno, cnu_pais, cnu_celular, cnu_telcasa, bol_whats, bol_telcasa, bol_activo) VALUES (9, 'maria linda', 'prueba', 'numero uno', 521, 2411255695, 2411245659, true, true, true);
INSERT INTO sght.tsghtpacientes (cod_pacienteid, des_nombre, des_paterno, des_materno, cnu_pais, cnu_celular, cnu_telcasa, bol_whats, bol_telcasa, bol_activo) VALUES (10, 'maria linda', 'prueba', 'numero uno', 521, 2411255695, 2411245659, true, true, true);
INSERT INTO sght.tsghtpacientes (cod_pacienteid, des_nombre, des_paterno, des_materno, cnu_pais, cnu_celular, cnu_telcasa, bol_whats, bol_telcasa, bol_activo) VALUES (11, 'maria linda', 'prueba', 'numero uno', 521, 2411255695, 2411245659, true, true, true);
INSERT INTO sght.tsghtpacientes (cod_pacienteid, des_nombre, des_paterno, des_materno, cnu_pais, cnu_celular, cnu_telcasa, bol_whats, bol_telcasa, bol_activo) VALUES (12, 'maria linda', 'prueba', 'numero uno', 521, 2411255695, 2411245659, true, true, true);
INSERT INTO sght.tsghtpacientes (cod_pacienteid, des_nombre, des_paterno, des_materno, cnu_pais, cnu_celular, cnu_telcasa, bol_whats, bol_telcasa, bol_activo) VALUES (13, 'maria linda', 'prueba', 'numero uno', 521, 2411255695, 2411245659, true, true, true);
INSERT INTO sght.tsghtpacientes (cod_pacienteid, des_nombre, des_paterno, des_materno, cnu_pais, cnu_celular, cnu_telcasa, bol_whats, bol_telcasa, bol_activo) VALUES (14, 'maria linda', 'prueba', 'numero uno', 521, 2411255695, 2411245659, true, true, true);
INSERT INTO sght.tsghtpacientes (cod_pacienteid, des_nombre, des_paterno, des_materno, cnu_pais, cnu_celular, cnu_telcasa, bol_whats, bol_telcasa, bol_activo) VALUES (15, 'maria linda', 'prueba', 'numero uno', 521, 2411255695, 2411245659, true, true, true);
INSERT INTO sght.tsghtpacientes (cod_pacienteid, des_nombre, des_paterno, des_materno, cnu_pais, cnu_celular, cnu_telcasa, bol_whats, bol_telcasa, bol_activo) VALUES (16, 'maria linda', 'prueba', 'numero uno', 521, 2411255695, 2411245659, true, true, true);
INSERT INTO sght.tsghtpacientes (cod_pacienteid, des_nombre, des_paterno, des_materno, cnu_pais, cnu_celular, cnu_telcasa, bol_whats, bol_telcasa, bol_activo) VALUES (17, 'maria linda', 'prueba', 'numero uno', 521, 2411255695, 2411245659, true, true, true);
INSERT INTO sght.tsghtpacientes (cod_pacienteid, des_nombre, des_paterno, des_materno, cnu_pais, cnu_celular, cnu_telcasa, bol_whats, bol_telcasa, bol_activo) VALUES (18, 'maria linda', 'prueba', 'numero uno', 521, 2411255695, 2411245659, true, true, true);
INSERT INTO sght.tsghtpacientes (cod_pacienteid, des_nombre, des_paterno, des_materno, cnu_pais, cnu_celular, cnu_telcasa, bol_whats, bol_telcasa, bol_activo) VALUES (19, 'maria linda', 'prueba', 'numero uno', 521, 2411255695, 2411245659, true, true, true);
INSERT INTO sght.tsghtpacientes (cod_pacienteid, des_nombre, des_paterno, des_materno, cnu_pais, cnu_celular, cnu_telcasa, bol_whats, bol_telcasa, bol_activo) VALUES (20, 'maria linda', 'prueba', 'numero uno', 521, 2411255695, 2411245659, true, true, true);
INSERT INTO sght.tsghtpacientes (cod_pacienteid, des_nombre, des_paterno, des_materno, cnu_pais, cnu_celular, cnu_telcasa, bol_whats, bol_telcasa, bol_activo) VALUES (21, 'maria linda', 'prueba', 'numero uno', 521, 2411255695, 2411245659, true, true, true);
INSERT INTO sght.tsghtpacientes (cod_pacienteid, des_nombre, des_paterno, des_materno, cnu_pais, cnu_celular, cnu_telcasa, bol_whats, bol_telcasa, bol_activo) VALUES (22, 'maria linda', 'prueba', 'numero uno', 521, 2411255695, 2411245659, true, true, true);
INSERT INTO sght.tsghtpacientes (cod_pacienteid, des_nombre, des_paterno, des_materno, cnu_pais, cnu_celular, cnu_telcasa, bol_whats, bol_telcasa, bol_activo) VALUES (23, 'maria linda', 'prueba', 'numero uno', 521, 2411255695, 2411245659, true, true, true);
INSERT INTO sght.tsghtpacientes (cod_pacienteid, des_nombre, des_paterno, des_materno, cnu_pais, cnu_celular, cnu_telcasa, bol_whats, bol_telcasa, bol_activo) VALUES (24, 'maria linda', 'prueba', 'numero uno', 521, 2411255695, 2411245659, true, true, true);
INSERT INTO sght.tsghtpacientes (cod_pacienteid, des_nombre, des_paterno, des_materno, cnu_pais, cnu_celular, cnu_telcasa, bol_whats, bol_telcasa, bol_activo) VALUES (25, 'maria linda', 'prueba', 'numero uno', 521, 2411255695, 2411245659, true, true, true);
INSERT INTO sght.tsghtpacientes (cod_pacienteid, des_nombre, des_paterno, des_materno, cnu_pais, cnu_celular, cnu_telcasa, bol_whats, bol_telcasa, bol_activo) VALUES (26, 'maria linda', 'prueba', 'numero uno', 521, 2411255695, 2411245659, true, true, true);
INSERT INTO sght.tsghtpacientes (cod_pacienteid, des_nombre, des_paterno, des_materno, cnu_pais, cnu_celular, cnu_telcasa, bol_whats, bol_telcasa, bol_activo) VALUES (27, 'maria linda', 'prueba', 'numero uno', 521, 2411255695, 2411245659, true, true, true);
INSERT INTO sght.tsghtpacientes (cod_pacienteid, des_nombre, des_paterno, des_materno, cnu_pais, cnu_celular, cnu_telcasa, bol_whats, bol_telcasa, bol_activo) VALUES (28, 'maria linda', 'prueba', 'numero uno', 521, 2411255695, 2411245659, true, true, true);
INSERT INTO sght.tsghtpacientes (cod_pacienteid, des_nombre, des_paterno, des_materno, cnu_pais, cnu_celular, cnu_telcasa, bol_whats, bol_telcasa, bol_activo) VALUES (29, 'maria linda', 'prueba', 'numero uno', 521, 2411255695, 2411245659, true, true, true);
INSERT INTO sght.tsghtpacientes (cod_pacienteid, des_nombre, des_paterno, des_materno, cnu_pais, cnu_celular, cnu_telcasa, bol_whats, bol_telcasa, bol_activo) VALUES (30, 'maria linda', 'prueba', 'numero uno', 521, 2411255695, 2411245659, true, true, true);
INSERT INTO sght.tsghtpacientes (cod_pacienteid, des_nombre, des_paterno, des_materno, cnu_pais, cnu_celular, cnu_telcasa, bol_whats, bol_telcasa, bol_activo) VALUES (31, 'jdnvu', 'cjui', 'bdsb', 521, 86464868, 4648646846, true, true, true);
INSERT INTO sght.tsghtpacientes (cod_pacienteid, des_nombre, des_paterno, des_materno, cnu_pais, cnu_celular, cnu_telcasa, bol_whats, bol_telcasa, bol_activo) VALUES (32, 'jdnvu', 'cjui', 'bdsb', 521, 86464868, 4648646846, true, true, true);
INSERT INTO sght.tsghtpacientes (cod_pacienteid, des_nombre, des_paterno, des_materno, cnu_pais, cnu_celular, cnu_telcasa, bol_whats, bol_telcasa, bol_activo) VALUES (33, 'jdnvu', 'cjui', 'bdsb', 521, 86464868, 4648646846, true, true, true);
INSERT INTO sght.tsghtpacientes (cod_pacienteid, des_nombre, des_paterno, des_materno, cnu_pais, cnu_celular, cnu_telcasa, bol_whats, bol_telcasa, bol_activo) VALUES (34, 'jdnvu', 'cjui', 'bdsb', 521, 86464868, 4648646846, true, true, true);
INSERT INTO sght.tsghtpacientes (cod_pacienteid, des_nombre, des_paterno, des_materno, cnu_pais, cnu_celular, cnu_telcasa, bol_whats, bol_telcasa, bol_activo) VALUES (35, 'jdnvu', 'cjui', 'bdsb', 521, 86464868, 4648646846, true, true, true);
INSERT INTO sght.tsghtpacientes (cod_pacienteid, des_nombre, des_paterno, des_materno, cnu_pais, cnu_celular, cnu_telcasa, bol_whats, bol_telcasa, bol_activo) VALUES (36, 'ndvn', 'lsnvdl', 'dsnvsl', 521, 5646465, 5465464, true, true, true);
INSERT INTO sght.tsghtpacientes (cod_pacienteid, des_nombre, des_paterno, des_materno, cnu_pais, cnu_celular, cnu_telcasa, bol_whats, bol_telcasa, bol_activo) VALUES (37, 'vksdbvkj', 'dsnvksdj', 'nsvn', 521, 465464, 5464646, true, true, true);
INSERT INTO sght.tsghtpacientes (cod_pacienteid, des_nombre, des_paterno, des_materno, cnu_pais, cnu_celular, cnu_telcasa, bol_whats, bol_telcasa, bol_activo) VALUES (38, 'vvd', 'dfv', 'fdv', 521, 234, 43, true, true, true);
INSERT INTO sght.tsghtpacientes (cod_pacienteid, des_nombre, des_paterno, des_materno, cnu_pais, cnu_celular, cnu_telcasa, bol_whats, bol_telcasa, bol_activo) VALUES (39, 'rdf', 'df', 'df', 521, 34, 34, true, true, true);
INSERT INTO sght.tsghtpacientes (cod_pacienteid, des_nombre, des_paterno, des_materno, cnu_pais, cnu_celular, cnu_telcasa, bol_whats, bol_telcasa, bol_activo) VALUES (40, 'rdf', 'df', 'df', 521, 34, 34, true, true, true);
INSERT INTO sght.tsghtpacientes (cod_pacienteid, des_nombre, des_paterno, des_materno, cnu_pais, cnu_celular, cnu_telcasa, bol_whats, bol_telcasa, bol_activo) VALUES (41, 'rdf', 'df', 'df', 521, 34, 34, true, true, true);
INSERT INTO sght.tsghtpacientes (cod_pacienteid, des_nombre, des_paterno, des_materno, cnu_pais, cnu_celular, cnu_telcasa, bol_whats, bol_telcasa, bol_activo) VALUES (42, 'rdf', 'df', 'df', 521, 34, 34, true, true, true);
INSERT INTO sght.tsghtpacientes (cod_pacienteid, des_nombre, des_paterno, des_materno, cnu_pais, cnu_celular, cnu_telcasa, bol_whats, bol_telcasa, bol_activo) VALUES (43, 'rdf', 'df', 'df', 521, 34, 34, true, true, true);
INSERT INTO sght.tsghtpacientes (cod_pacienteid, des_nombre, des_paterno, des_materno, cnu_pais, cnu_celular, cnu_telcasa, bol_whats, bol_telcasa, bol_activo) VALUES (44, 'rdf', 'df', 'df', 521, 34, 34, true, true, true);
INSERT INTO sght.tsghtpacientes (cod_pacienteid, des_nombre, des_paterno, des_materno, cnu_pais, cnu_celular, cnu_telcasa, bol_whats, bol_telcasa, bol_activo) VALUES (45, 'rdf', 'df', 'df', 521, 34, 34, true, true, true);
INSERT INTO sght.tsghtpacientes (cod_pacienteid, des_nombre, des_paterno, des_materno, cnu_pais, cnu_celular, cnu_telcasa, bol_whats, bol_telcasa, bol_activo) VALUES (46, 'rdf', 'df', 'df', 521, 34, 34, true, true, true);
INSERT INTO sght.tsghtpacientes (cod_pacienteid, des_nombre, des_paterno, des_materno, cnu_pais, cnu_celular, cnu_telcasa, bol_whats, bol_telcasa, bol_activo) VALUES (47, 'rdf', 'df', 'df', 521, 34, 34, true, true, true);
INSERT INTO sght.tsghtpacientes (cod_pacienteid, des_nombre, des_paterno, des_materno, cnu_pais, cnu_celular, cnu_telcasa, bol_whats, bol_telcasa, bol_activo) VALUES (48, 'rdf', 'df', 'df', 521, 34, 34, true, true, true);
INSERT INTO sght.tsghtpacientes (cod_pacienteid, des_nombre, des_paterno, des_materno, cnu_pais, cnu_celular, cnu_telcasa, bol_whats, bol_telcasa, bol_activo) VALUES (49, 'rdf', 'df', 'df', 521, 34, 34, true, true, true);
INSERT INTO sght.tsghtpacientes (cod_pacienteid, des_nombre, des_paterno, des_materno, cnu_pais, cnu_celular, cnu_telcasa, bol_whats, bol_telcasa, bol_activo) VALUES (50, 'rdf', 'df', 'df', 521, 34, 34, true, true, true);
INSERT INTO sght.tsghtpacientes (cod_pacienteid, des_nombre, des_paterno, des_materno, cnu_pais, cnu_celular, cnu_telcasa, bol_whats, bol_telcasa, bol_activo) VALUES (51, 'rdf', 'df', 'df', 521, 34, 34, true, true, true);
INSERT INTO sght.tsghtpacientes (cod_pacienteid, des_nombre, des_paterno, des_materno, cnu_pais, cnu_celular, cnu_telcasa, bol_whats, bol_telcasa, bol_activo) VALUES (52, 'dsdd', 'dfs', 'sfd3', 521, 234, 34, true, true, true);
INSERT INTO sght.tsghtpacientes (cod_pacienteid, des_nombre, des_paterno, des_materno, cnu_pais, cnu_celular, cnu_telcasa, bol_whats, bol_telcasa, bol_activo) VALUES (53, 'dsdd', 'dfs', 'sfd3', 521, 234, 34111111, true, true, true);
INSERT INTO sght.tsghtpacientes (cod_pacienteid, des_nombre, des_paterno, des_materno, cnu_pais, cnu_celular, cnu_telcasa, bol_whats, bol_telcasa, bol_activo) VALUES (54, 'dsdd', 'dfs', 'sfd3', 521, 234, 34111111, true, true, true);


--
-- TOC entry 3046 (class 0 OID 49305)
-- Dependencies: 217
-- Data for Name: tsghtqueja; Type: TABLE DATA; Schema: sght; Owner: mirosc
--

INSERT INTO sght.tsghtqueja (cod_quejaid, cod_datogeneralid_fk, txt_causa, txt_problema, des_cuando, bol_vlejana, bol_vcercana, bol_vdoble, bol_ardor, bol_comezon, bol_secrecion, bol_fatiga, bol_epiforia, bol_ojoseco, bol_ojorojo, bol_fotofobia, bol_moscas, bol_flashes, bol_auras, opcional, bol_status) VALUES (2, 21, NULL, 'rfe', 'tgtr', true, NULL, NULL, NULL, NULL, NULL, true, NULL, NULL, true, NULL, NULL, NULL, true, NULL, true);
INSERT INTO sght.tsghtqueja (cod_quejaid, cod_datogeneralid_fk, txt_causa, txt_problema, des_cuando, bol_vlejana, bol_vcercana, bol_vdoble, bol_ardor, bol_comezon, bol_secrecion, bol_fatiga, bol_epiforia, bol_ojoseco, bol_ojorojo, bol_fotofobia, bol_moscas, bol_flashes, bol_auras, opcional, bol_status) VALUES (3, 22, NULL, 'revre', 'revre', NULL, NULL, true, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, true, NULL, true);
INSERT INTO sght.tsghtqueja (cod_quejaid, cod_datogeneralid_fk, txt_causa, txt_problema, des_cuando, bol_vlejana, bol_vcercana, bol_vdoble, bol_ardor, bol_comezon, bol_secrecion, bol_fatiga, bol_epiforia, bol_ojoseco, bol_ojorojo, bol_fotofobia, bol_moscas, bol_flashes, bol_auras, opcional, bol_status) VALUES (4, 23, NULL, 'cerc', 'cerc', NULL, NULL, true, NULL, NULL, true, true, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, true);
INSERT INTO sght.tsghtqueja (cod_quejaid, cod_datogeneralid_fk, txt_causa, txt_problema, des_cuando, bol_vlejana, bol_vcercana, bol_vdoble, bol_ardor, bol_comezon, bol_secrecion, bol_fatiga, bol_epiforia, bol_ojoseco, bol_ojorojo, bol_fotofobia, bol_moscas, bol_flashes, bol_auras, opcional, bol_status) VALUES (5, 24, NULL, 'df', 'sdgs', NULL, NULL, true, NULL, NULL, true, true, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, true);
INSERT INTO sght.tsghtqueja (cod_quejaid, cod_datogeneralid_fk, txt_causa, txt_problema, des_cuando, bol_vlejana, bol_vcercana, bol_vdoble, bol_ardor, bol_comezon, bol_secrecion, bol_fatiga, bol_epiforia, bol_ojoseco, bol_ojorojo, bol_fotofobia, bol_moscas, bol_flashes, bol_auras, opcional, bol_status) VALUES (6, 27, NULL, 'ardor', '2 meses', true, NULL, NULL, true, true, NULL, NULL, NULL, NULL, true, NULL, NULL, NULL, NULL, NULL, true);


--
-- TOC entry 3048 (class 0 OID 49314)
-- Dependencies: 219
-- Data for Name: tsghtrxpaciente; Type: TABLE DATA; Schema: sght; Owner: mirosc
--



--
-- TOC entry 3050 (class 0 OID 49320)
-- Dependencies: 221
-- Data for Name: tsghtultimoexamen; Type: TABLE DATA; Schema: sght; Owner: mirosc
--



--
-- TOC entry 3053 (class 0 OID 49419)
-- Dependencies: 224
-- Data for Name: tsghtx; Type: TABLE DATA; Schema: sght; Owner: mirosc
--



--
-- TOC entry 3092 (class 0 OID 0)
-- Dependencies: 227
-- Name: tcatcilindro_cod_cilindroid_seq; Type: SEQUENCE SET; Schema: cat; Owner: mirosc
--

SELECT pg_catalog.setval('cat.tcatcilindro_cod_cilindroid_seq', 1, false);


--
-- TOC entry 3093 (class 0 OID 0)
-- Dependencies: 225
-- Name: tcateje_cod_ejeid_seq; Type: SEQUENCE SET; Schema: cat; Owner: mirosc
--

SELECT pg_catalog.setval('cat.tcateje_cod_ejeid_seq', 1, false);


--
-- TOC entry 3094 (class 0 OID 0)
-- Dependencies: 229
-- Name: tcatesferico_cod_esfericoid_seq; Type: SEQUENCE SET; Schema: cat; Owner: mirosc
--

SELECT pg_catalog.setval('cat.tcatesferico_cod_esfericoid_seq', 1, false);


--
-- TOC entry 3095 (class 0 OID 0)
-- Dependencies: 231
-- Name: tcatvalorprueba_cod_valorpruebaid_seq; Type: SEQUENCE SET; Schema: cat; Owner: mirosc
--

SELECT pg_catalog.setval('cat.tcatvalorprueba_cod_valorpruebaid_seq', 1, false);


--
-- TOC entry 3096 (class 0 OID 0)
-- Dependencies: 239
-- Name: tinvenarmazon_cod_armazonid_seq; Type: SEQUENCE SET; Schema: inven; Owner: mirosc
--

SELECT pg_catalog.setval('inven.tinvenarmazon_cod_armazonid_seq', 1, false);


--
-- TOC entry 3097 (class 0 OID 0)
-- Dependencies: 233
-- Name: tmicamaterial_cod_materialid_seq; Type: SEQUENCE SET; Schema: mica; Owner: mirosc
--

SELECT pg_catalog.setval('mica.tmicamaterial_cod_materialid_seq', 1, false);


--
-- TOC entry 3098 (class 0 OID 0)
-- Dependencies: 202
-- Name: tsghtantmedicofam_cod_antmedfam_seq; Type: SEQUENCE SET; Schema: sght; Owner: mirosc
--

SELECT pg_catalog.setval('sght.tsghtantmedicofam_cod_antmedfam_seq', 3, true);


--
-- TOC entry 3099 (class 0 OID 0)
-- Dependencies: 203
-- Name: tsghtantmedicopx_cod_antmedpxid_seq; Type: SEQUENCE SET; Schema: sght; Owner: mirosc
--

SELECT pg_catalog.setval('sght.tsghtantmedicopx_cod_antmedpxid_seq', 3, true);


--
-- TOC entry 3100 (class 0 OID 0)
-- Dependencies: 206
-- Name: tsghtantocularpx_cod_antocularpxid_seq; Type: SEQUENCE SET; Schema: sght; Owner: mirosc
--

SELECT pg_catalog.setval('sght.tsghtantocularpx_cod_antocularpxid_seq', 5, true);


--
-- TOC entry 3101 (class 0 OID 0)
-- Dependencies: 208
-- Name: tsghtantoculfam_cod_antoculfamid_seq; Type: SEQUENCE SET; Schema: sght; Owner: mirosc
--

SELECT pg_catalog.setval('sght.tsghtantoculfam_cod_antoculfamid_seq', 3, true);


--
-- TOC entry 3102 (class 0 OID 0)
-- Dependencies: 210
-- Name: tsghtdatosgeneral_cod_datogeneralid_seq; Type: SEQUENCE SET; Schema: sght; Owner: mirosc
--

SELECT pg_catalog.setval('sght.tsghtdatosgeneral_cod_datogeneralid_seq', 63, true);


--
-- TOC entry 3103 (class 0 OID 0)
-- Dependencies: 212
-- Name: tsghtexamenpx_cod_examenpxid_seq; Type: SEQUENCE SET; Schema: sght; Owner: mirosc
--

SELECT pg_catalog.setval('sght.tsghtexamenpx_cod_examenpxid_seq', 4, true);


--
-- TOC entry 3104 (class 0 OID 0)
-- Dependencies: 214
-- Name: tsghtobservaciongen_cod_observaciongenid_seq; Type: SEQUENCE SET; Schema: sght; Owner: mirosc
--

SELECT pg_catalog.setval('sght.tsghtobservaciongen_cod_observaciongenid_seq', 3, true);


--
-- TOC entry 3105 (class 0 OID 0)
-- Dependencies: 216
-- Name: tsghtpacientes_cod_pacienteid_seq; Type: SEQUENCE SET; Schema: sght; Owner: mirosc
--

SELECT pg_catalog.setval('sght.tsghtpacientes_cod_pacienteid_seq', 54, true);


--
-- TOC entry 3106 (class 0 OID 0)
-- Dependencies: 218
-- Name: tsghtqueja_cod_quejaid_seq; Type: SEQUENCE SET; Schema: sght; Owner: mirosc
--

SELECT pg_catalog.setval('sght.tsghtqueja_cod_quejaid_seq', 6, true);


--
-- TOC entry 3107 (class 0 OID 0)
-- Dependencies: 220
-- Name: tsghtrxpaciente_cod_rxpaciente_seq; Type: SEQUENCE SET; Schema: sght; Owner: mirosc
--

SELECT pg_catalog.setval('sght.tsghtrxpaciente_cod_rxpaciente_seq', 6, true);


--
-- TOC entry 3108 (class 0 OID 0)
-- Dependencies: 222
-- Name: tsghtultimoexamen_cod_ultimoexamenid_seq; Type: SEQUENCE SET; Schema: sght; Owner: mirosc
--

SELECT pg_catalog.setval('sght.tsghtultimoexamen_cod_ultimoexamenid_seq', 1, false);


--
-- TOC entry 3109 (class 0 OID 0)
-- Dependencies: 223
-- Name: tsghtx_cod_txid_seq; Type: SEQUENCE SET; Schema: sght; Owner: mirosc
--

SELECT pg_catalog.setval('sght.tsghtx_cod_txid_seq', 1, false);


--
-- TOC entry 2887 (class 2606 OID 49530)
-- Name: tarmazformamm tarmazformamm_pkey; Type: CONSTRAINT; Schema: armaz; Owner: mirosc
--

ALTER TABLE ONLY armaz.tarmazformamm
    ADD CONSTRAINT tarmazformamm_pkey PRIMARY KEY (cod_formammid);


--
-- TOC entry 2893 (class 2606 OID 49545)
-- Name: tarmazmaterial tarmazmaterial_pkey; Type: CONSTRAINT; Schema: armaz; Owner: mirosc
--

ALTER TABLE ONLY armaz.tarmazmaterial
    ADD CONSTRAINT tarmazmaterial_pkey PRIMARY KEY (cod_materialid);


--
-- TOC entry 2889 (class 2606 OID 49535)
-- Name: tarmazpuentemm tarmazpuentemm_pkey; Type: CONSTRAINT; Schema: armaz; Owner: mirosc
--

ALTER TABLE ONLY armaz.tarmazpuentemm
    ADD CONSTRAINT tarmazpuentemm_pkey PRIMARY KEY (cod_puentemmid);


--
-- TOC entry 2891 (class 2606 OID 49540)
-- Name: tarmazterminalmm tarmazterminalmm_pkey; Type: CONSTRAINT; Schema: armaz; Owner: mirosc
--

ALTER TABLE ONLY armaz.tarmazterminalmm
    ADD CONSTRAINT tarmazterminalmm_pkey PRIMARY KEY (cod_terminalmmid);


--
-- TOC entry 2879 (class 2606 OID 49485)
-- Name: tcatcilindro tcatcilindro_pkey; Type: CONSTRAINT; Schema: cat; Owner: mirosc
--

ALTER TABLE ONLY cat.tcatcilindro
    ADD CONSTRAINT tcatcilindro_pkey PRIMARY KEY (cod_cilindroid);


--
-- TOC entry 2877 (class 2606 OID 49477)
-- Name: tcateje tcateje_pkey; Type: CONSTRAINT; Schema: cat; Owner: mirosc
--

ALTER TABLE ONLY cat.tcateje
    ADD CONSTRAINT tcateje_pkey PRIMARY KEY (cod_ejeid);


--
-- TOC entry 2881 (class 2606 OID 49493)
-- Name: tcatesferico tcatesferico_pkey; Type: CONSTRAINT; Schema: cat; Owner: mirosc
--

ALTER TABLE ONLY cat.tcatesferico
    ADD CONSTRAINT tcatesferico_pkey PRIMARY KEY (cod_esfericoid);


--
-- TOC entry 2883 (class 2606 OID 49501)
-- Name: tcatvalorprueba tcatvalorprueba_pkey; Type: CONSTRAINT; Schema: cat; Owner: mirosc
--

ALTER TABLE ONLY cat.tcatvalorprueba
    ADD CONSTRAINT tcatvalorprueba_pkey PRIMARY KEY (cod_valorpruebaid);


--
-- TOC entry 2885 (class 2606 OID 49523)
-- Name: tmicamaterial tmicamaterial_pkey; Type: CONSTRAINT; Schema: mica; Owner: mirosc
--

ALTER TABLE ONLY mica.tmicamaterial
    ADD CONSTRAINT tmicamaterial_pkey PRIMARY KEY (cod_materialid);


--
-- TOC entry 2851 (class 2606 OID 49339)
-- Name: tsghtantmedicofam tsghtantmedicofam_pkey; Type: CONSTRAINT; Schema: sght; Owner: mirosc
--

ALTER TABLE ONLY sght.tsghtantmedicofam
    ADD CONSTRAINT tsghtantmedicofam_pkey PRIMARY KEY (cod_antmedfam);


--
-- TOC entry 2853 (class 2606 OID 49341)
-- Name: tsghtantmedicopx tsghtantmedicopx_pkey; Type: CONSTRAINT; Schema: sght; Owner: mirosc
--

ALTER TABLE ONLY sght.tsghtantmedicopx
    ADD CONSTRAINT tsghtantmedicopx_pkey PRIMARY KEY (cod_antmedpxid);


--
-- TOC entry 2855 (class 2606 OID 49343)
-- Name: tsghtantocularpx tsghtantocularpx_pkey; Type: CONSTRAINT; Schema: sght; Owner: mirosc
--

ALTER TABLE ONLY sght.tsghtantocularpx
    ADD CONSTRAINT tsghtantocularpx_pkey PRIMARY KEY (cod_antocularpxid);


--
-- TOC entry 2857 (class 2606 OID 49345)
-- Name: tsghtantoculfam tsghtantoculfam_pkey; Type: CONSTRAINT; Schema: sght; Owner: mirosc
--

ALTER TABLE ONLY sght.tsghtantoculfam
    ADD CONSTRAINT tsghtantoculfam_pkey PRIMARY KEY (cod_antoculfamid);


--
-- TOC entry 2859 (class 2606 OID 49347)
-- Name: tsghtdatosgeneral tsghtdatosgeneral_email_key; Type: CONSTRAINT; Schema: sght; Owner: mirosc
--

ALTER TABLE ONLY sght.tsghtdatosgeneral
    ADD CONSTRAINT tsghtdatosgeneral_email_key UNIQUE (email);


--
-- TOC entry 2861 (class 2606 OID 49349)
-- Name: tsghtdatosgeneral tsghtdatosgeneral_pkey; Type: CONSTRAINT; Schema: sght; Owner: mirosc
--

ALTER TABLE ONLY sght.tsghtdatosgeneral
    ADD CONSTRAINT tsghtdatosgeneral_pkey PRIMARY KEY (cod_datogeneralid);


--
-- TOC entry 2863 (class 2606 OID 49351)
-- Name: tsghtexamenpx tsghtexamenpx_pkey; Type: CONSTRAINT; Schema: sght; Owner: mirosc
--

ALTER TABLE ONLY sght.tsghtexamenpx
    ADD CONSTRAINT tsghtexamenpx_pkey PRIMARY KEY (cod_examenpxid);


--
-- TOC entry 2865 (class 2606 OID 49353)
-- Name: tsghtobservaciongen tsghtobservaciongen_pkey; Type: CONSTRAINT; Schema: sght; Owner: mirosc
--

ALTER TABLE ONLY sght.tsghtobservaciongen
    ADD CONSTRAINT tsghtobservaciongen_pkey PRIMARY KEY (cod_observaciongenid);


--
-- TOC entry 2867 (class 2606 OID 49355)
-- Name: tsghtpacientes tsghtpacientes_pkey; Type: CONSTRAINT; Schema: sght; Owner: mirosc
--

ALTER TABLE ONLY sght.tsghtpacientes
    ADD CONSTRAINT tsghtpacientes_pkey PRIMARY KEY (cod_pacienteid);


--
-- TOC entry 2869 (class 2606 OID 49357)
-- Name: tsghtqueja tsghtqueja_pkey; Type: CONSTRAINT; Schema: sght; Owner: mirosc
--

ALTER TABLE ONLY sght.tsghtqueja
    ADD CONSTRAINT tsghtqueja_pkey PRIMARY KEY (cod_quejaid);


--
-- TOC entry 2871 (class 2606 OID 49359)
-- Name: tsghtrxpaciente tsghtrxpaciente_pkey; Type: CONSTRAINT; Schema: sght; Owner: mirosc
--

ALTER TABLE ONLY sght.tsghtrxpaciente
    ADD CONSTRAINT tsghtrxpaciente_pkey PRIMARY KEY (cod_rxpaciente);


--
-- TOC entry 2873 (class 2606 OID 49361)
-- Name: tsghtultimoexamen tsghtultimoexamen_pkey; Type: CONSTRAINT; Schema: sght; Owner: mirosc
--

ALTER TABLE ONLY sght.tsghtultimoexamen
    ADD CONSTRAINT tsghtultimoexamen_pkey PRIMARY KEY (cod_ultimoexamenid);


--
-- TOC entry 2875 (class 2606 OID 49427)
-- Name: tsghtx tsghtx_pkey; Type: CONSTRAINT; Schema: sght; Owner: mirosc
--

ALTER TABLE ONLY sght.tsghtx
    ADD CONSTRAINT tsghtx_pkey PRIMARY KEY (cod_txid);


--
-- TOC entry 2908 (class 2606 OID 49565)
-- Name: tinvenarmazon fk_tinvenarmazon_tarmazforma_1; Type: FK CONSTRAINT; Schema: inven; Owner: mirosc
--

ALTER TABLE ONLY inven.tinvenarmazon
    ADD CONSTRAINT fk_tinvenarmazon_tarmazforma_1 FOREIGN KEY (cod_formammid_fk) REFERENCES armaz.tarmazformamm(cod_formammid);


--
-- TOC entry 2907 (class 2606 OID 49560)
-- Name: tinvenarmazon fk_tinvenarmazon_tarmazpuente_1; Type: FK CONSTRAINT; Schema: inven; Owner: mirosc
--

ALTER TABLE ONLY inven.tinvenarmazon
    ADD CONSTRAINT fk_tinvenarmazon_tarmazpuente_1 FOREIGN KEY (cod_puentemmid_fk) REFERENCES armaz.tarmazpuentemm(cod_puentemmid);


--
-- TOC entry 2906 (class 2606 OID 49555)
-- Name: tinvenarmazon fk_tinvenarmazon_tarmazterminal_1; Type: FK CONSTRAINT; Schema: inven; Owner: mirosc
--

ALTER TABLE ONLY inven.tinvenarmazon
    ADD CONSTRAINT fk_tinvenarmazon_tarmazterminal_1 FOREIGN KEY (cod_terminammt_fk) REFERENCES armaz.tarmazterminalmm(cod_terminalmmid);


--
-- TOC entry 2894 (class 2606 OID 49362)
-- Name: tsghtantmedicofam fk_tsghtantmedicofam_tsghtdatosgeneral_1; Type: FK CONSTRAINT; Schema: sght; Owner: mirosc
--

ALTER TABLE ONLY sght.tsghtantmedicofam
    ADD CONSTRAINT fk_tsghtantmedicofam_tsghtdatosgeneral_1 FOREIGN KEY (cod_datogeneralid_fk) REFERENCES sght.tsghtdatosgeneral(cod_datogeneralid);


--
-- TOC entry 2895 (class 2606 OID 49428)
-- Name: tsghtantmedicofam fk_tsghtantmedicofam_tsghtx_1; Type: FK CONSTRAINT; Schema: sght; Owner: mirosc
--

ALTER TABLE ONLY sght.tsghtantmedicofam
    ADD CONSTRAINT fk_tsghtantmedicofam_tsghtx_1 FOREIGN KEY (cod_txid_fk) REFERENCES sght.tsghtx(cod_txid);


--
-- TOC entry 2896 (class 2606 OID 49367)
-- Name: tsghtantmedicopx fk_tsghtantmedicopx_tsghtdatosgeneral_1; Type: FK CONSTRAINT; Schema: sght; Owner: mirosc
--

ALTER TABLE ONLY sght.tsghtantmedicopx
    ADD CONSTRAINT fk_tsghtantmedicopx_tsghtdatosgeneral_1 FOREIGN KEY (cod_datogeneralid_fk) REFERENCES sght.tsghtdatosgeneral(cod_datogeneralid) ON UPDATE CASCADE;


--
-- TOC entry 2897 (class 2606 OID 49372)
-- Name: tsghtantocularpx fk_tsghtantocularpx_tsghtdatosgeneral_1; Type: FK CONSTRAINT; Schema: sght; Owner: mirosc
--

ALTER TABLE ONLY sght.tsghtantocularpx
    ADD CONSTRAINT fk_tsghtantocularpx_tsghtdatosgeneral_1 FOREIGN KEY (cod_datogeneralid_fk) REFERENCES sght.tsghtdatosgeneral(cod_datogeneralid);


--
-- TOC entry 2898 (class 2606 OID 49377)
-- Name: tsghtantoculfam fk_tsghtantoculfam_tsghtdatosgeneral_1; Type: FK CONSTRAINT; Schema: sght; Owner: mirosc
--

ALTER TABLE ONLY sght.tsghtantoculfam
    ADD CONSTRAINT fk_tsghtantoculfam_tsghtdatosgeneral_1 FOREIGN KEY (cod_datogeneralid_fk) REFERENCES sght.tsghtdatosgeneral(cod_datogeneralid);


--
-- TOC entry 2899 (class 2606 OID 49433)
-- Name: tsghtantoculfam fk_tsghtantoculfam_tsghtx_1; Type: FK CONSTRAINT; Schema: sght; Owner: mirosc
--

ALTER TABLE ONLY sght.tsghtantoculfam
    ADD CONSTRAINT fk_tsghtantoculfam_tsghtx_1 FOREIGN KEY (cod_txid_fk) REFERENCES sght.tsghtx(cod_txid);


--
-- TOC entry 2900 (class 2606 OID 49382)
-- Name: tsghtdatosgeneral fk_tsghtdatosgeneral_tsghtpacientes_1; Type: FK CONSTRAINT; Schema: sght; Owner: mirosc
--

ALTER TABLE ONLY sght.tsghtdatosgeneral
    ADD CONSTRAINT fk_tsghtdatosgeneral_tsghtpacientes_1 FOREIGN KEY (cod_pacienteid_fk) REFERENCES sght.tsghtpacientes(cod_pacienteid);


--
-- TOC entry 2901 (class 2606 OID 49387)
-- Name: tsghtexamenpx fk_tsghtexamenpx_tsghtdatosgeneral_1; Type: FK CONSTRAINT; Schema: sght; Owner: mirosc
--

ALTER TABLE ONLY sght.tsghtexamenpx
    ADD CONSTRAINT fk_tsghtexamenpx_tsghtdatosgeneral_1 FOREIGN KEY (cod_datogeneralid_fk) REFERENCES sght.tsghtdatosgeneral(cod_datogeneralid);


--
-- TOC entry 2902 (class 2606 OID 49392)
-- Name: tsghtobservaciongen fk_tsghtobservaciongen_tsghtdatosgeneral_1; Type: FK CONSTRAINT; Schema: sght; Owner: mirosc
--

ALTER TABLE ONLY sght.tsghtobservaciongen
    ADD CONSTRAINT fk_tsghtobservaciongen_tsghtdatosgeneral_1 FOREIGN KEY (cod_datogeneralid_fk) REFERENCES sght.tsghtdatosgeneral(cod_datogeneralid);


--
-- TOC entry 2903 (class 2606 OID 49397)
-- Name: tsghtqueja fk_tsghtqueja_tsghtdatosgeneral_1; Type: FK CONSTRAINT; Schema: sght; Owner: mirosc
--

ALTER TABLE ONLY sght.tsghtqueja
    ADD CONSTRAINT fk_tsghtqueja_tsghtdatosgeneral_1 FOREIGN KEY (cod_datogeneralid_fk) REFERENCES sght.tsghtdatosgeneral(cod_datogeneralid);


--
-- TOC entry 2904 (class 2606 OID 49402)
-- Name: tsghtrxpaciente fk_tsghtrxpaciente_tsghtdatosgeneral_1; Type: FK CONSTRAINT; Schema: sght; Owner: mirosc
--

ALTER TABLE ONLY sght.tsghtrxpaciente
    ADD CONSTRAINT fk_tsghtrxpaciente_tsghtdatosgeneral_1 FOREIGN KEY (cod_datogeneralid_fk) REFERENCES sght.tsghtdatosgeneral(cod_datogeneralid);


--
-- TOC entry 2905 (class 2606 OID 49407)
-- Name: tsghtultimoexamen fk_tsghtultimoexamen_tsghtqueja_1; Type: FK CONSTRAINT; Schema: sght; Owner: mirosc
--

ALTER TABLE ONLY sght.tsghtultimoexamen
    ADD CONSTRAINT fk_tsghtultimoexamen_tsghtqueja_1 FOREIGN KEY (cod_quejaid_fk) REFERENCES sght.tsghtqueja(cod_quejaid);


-- Completed on 2019-10-18 16:14:46

--
-- PostgreSQL database dump complete
--

