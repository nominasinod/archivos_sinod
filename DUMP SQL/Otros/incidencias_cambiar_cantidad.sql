---------------------------------------CAMBIAR EL TIPO DE DATO DE LA COLUMNA
ALTER TABLE sgnom.tsgnomincidencia
ALTER COLUMN cnu_cantidad  TYPE NUMERIC(8,2);

--------------------------------------- FUNCION BUSCAR INCIDENCIAS POR EMPLEADO
DROP FUNCTION IF EXISTS sgnom.buscar_incidencias_por_empleado(integer);
CREATE OR REPLACE FUNCTION "sgnom"."buscar_incidencias_por_empleado"("idempleado" int4)
  RETURNS TABLE("idincidencia" int4, "fechaalta" date, "clave" varchar, "incidencia" varchar, "idtipo" bpchar, "desctipo" text, "cantidad" numeric, "actividad" varchar, "comentarios" text, "reportaid" int4, "reportanb" text, "autorizaid" int4, "autorizanb" text, "perfil" varchar, "detallefechas" text, "montoincidencia" numeric, "montopagado" numeric, "aceptacion" bool, "validacion" bool, "quincenaid" int4, "desquincena" varchar, "creaid" int4, "creanb" text) AS $BODY$

BEGIN 
RETURN QUERY 

	SELECT incidencias.cod_incidenciaid idincidencia,
		incidencias.aud_feccreacion fechaalta,
		catincidencias.cod_claveincidencia clave,
		catincidencias.cod_nbincidencia incidencia,
		catincidencias.cod_tipoincidencia idtipo,
		CASE 
			WHEN catincidencias.cod_tipoincidencia='1' THEN
			 'HORAS'
			WHEN catincidencias.cod_tipoincidencia='2' THEN
			 'DIAS' 
		  WHEN catincidencias.cod_tipoincidencia='3' THEN
			 'ACTIVIDAD' 
			ELSE
			 'NO DATA'
		END desc_tipo,
		incidencias.cnu_cantidad cantidad,
		incidencias.des_actividad actividad,
		incidencias.txt_comentarios comentarios,
		incidencias.cod_empreporta_fk reportaid,
		(SELECT CONCAT(rhempleados.des_nombre ||' ', rhempleados.des_nombres ,' ' || rhempleados.des_apepaterno || ' ' || rhempleados.des_apematerno) 
		FROM sgrh.tsgrhempleados rhempleados
		WHERE rhempleados.cod_empleado = (SELECT nom.cod_empleado_fk FROM sgnom.tsgnomempleados nom WHERE nom.cod_empleadoid = idempleado)
		) reportanb,
		incidencias.cod_empautoriza_fk autorizaid,
		(SELECT CONCAT(rhempleados.des_nombre ||' ', rhempleados.des_nombres ,' ' || rhempleados.des_apepaterno || ' ' || rhempleados.des_apematerno) 
		FROM sgrh.tsgrhempleados rhempleados
		WHERE rhempleados.cod_empleado = incidencias.cod_empautoriza_fk
		) autorizanb,
		catincidencias.cod_perfilincidencia perfil,
		CAST(incidencias.xml_detcantidad AS TEXT) detallefechas,
		catincidencias.imp_monto montoincidencia,
		incidencias.imp_monto montopagado,
		incidencias.bol_validacion validacion, --validacion rh
		incidencias.bol_aceptacion aceptacion, --aceptacion lider de celula
		incidencias.cod_quincenaid_fk quincena,
		(SELECT qui.des_quincena FROM sgnom.tsgnomquincena qui WHERE qui.cod_quincenaid = incidencias.cod_quincenaid_fk) desquincena,
		incidencias.aud_codcreadopor creaid,
		(SELECT CONCAT(rhempleados.des_nombre ||' ', rhempleados.des_nombres ,' ' || rhempleados.des_apepaterno || ' ' || rhempleados.des_apematerno) 
		FROM sgrh.tsgrhempleados rhempleados
		WHERE rhempleados.cod_empleado = incidencias.aud_codcreadopor
		) creanb
FROM sgnom.tsgnomincidencia incidencias
JOIN sgnom.tsgnomcatincidencia catincidencias 
ON incidencias.cod_catincidenciaid_fk = catincidencias.cod_catincidenciaid
WHERE incidencias.cod_empreporta_fk = idempleado 
	AND incidencias.cod_quincenaid_fk = (SELECT nomquincena.cod_quincenaid 
											FROM sgnom.tsgnomquincena nomquincena
											WHERE nomquincena.fec_inicio <= CURRENT_DATE AND nomquincena.fec_fin >= CURRENT_DATE)
	AND incidencias.bol_estatus = true 
ORDER BY 1 DESC, 2;

END;
$BODY$
  LANGUAGE plpgsql;

--------------------------------------- FUNCION INCIDENCIAS POR QUINCENA
DROP FUNCTION IF EXISTS sgnom.fn_incidencias_por_quincena(integer);

CREATE OR REPLACE FUNCTION "sgnom"."fn_incidencias_por_quincena"("quincena" int4)
  RETURNS TABLE("idincidencia" int4, "fechaalta" date, "clave" varchar, "incidencia" varchar, "idtipo" bpchar, "desctipo" text, "cantidad" numeric, "actividad" varchar, "comentarios" text, "reportaid" int4, "reportanb" text, "autorizaid" int4, "autorizanb" text, "perfil" varchar, "detallefechas" text, "montoincidencia" numeric, "montopagado" numeric, "validacion" bool, "aceptacion" bool, "autorizacion" bool, "quincenaid" int4, "desquincena" varchar, "creaid" int4, "creanb" text) AS $BODY$
BEGIN 
RETURN QUERY 
SELECT incidencias.cod_incidenciaid idincidencia,
		incidencias.aud_feccreacion fechaalta,
		catincidencias.cod_claveincidencia clave,
		catincidencias.cod_nbincidencia incidencia,
		catincidencias.cod_tipoincidencia idtipo,
		CASE 
			WHEN catincidencias.cod_tipoincidencia='1' THEN
			 'HORAS'
			WHEN catincidencias.cod_tipoincidencia='2' THEN
			 'DIAS' 
		  WHEN catincidencias.cod_tipoincidencia='3' THEN
			 'ACTIVIDAD' 
			ELSE
			 'NO DATA'
		END desc_tipo,
		incidencias.cnu_cantidad cantidad,
		incidencias.des_actividad actividad,
		incidencias.txt_comentarios comentarios,
		incidencias.cod_empreporta_fk reportaid,
		(SELECT CONCAT(rhempleados.des_nombre ||' ', rhempleados.des_nombres ,' ' || rhempleados.des_apepaterno || ' ' || rhempleados.des_apematerno) 
		FROM sgrh.tsgrhempleados rhempleados
		WHERE rhempleados.cod_empleado = (SELECT nom.cod_empleado_fk FROM sgnom.tsgnomempleados nom WHERE nom.cod_empleadoid = incidencias.cod_empreporta_fk)
		) reportanb,
		incidencias.cod_empautoriza_fk autorizaid,
		(SELECT CONCAT(rhempleados.des_nombre ||' ', rhempleados.des_nombres ,' ' || rhempleados.des_apepaterno || ' ' || rhempleados.des_apematerno) 
		FROM sgrh.tsgrhempleados rhempleados
		WHERE rhempleados.cod_empleado = incidencias.cod_empautoriza_fk
		) autorizanb,
		catincidencias.cod_perfilincidencia perfil,
		CAST(incidencias.xml_detcantidad AS TEXT) detallefechas,
		catincidencias.imp_monto montoincidencia,
		incidencias.imp_monto montopagado,
		incidencias.bol_validacion validacion, --validacion rh
		incidencias.bol_aceptacion aceptacion, --aceptacion lider de celula
		incidencias.bol_pago autorizacion, --aceptacion finanzas
		incidencias.cod_quincenaid_fk quincena,
		(SELECT qui.des_quincena FROM sgnom.tsgnomquincena qui WHERE qui.cod_quincenaid = incidencias.cod_quincenaid_fk) desquincena,
		incidencias.aud_codcreadopor creaid,
		(SELECT CONCAT(rhempleados.des_nombre ||' ', rhempleados.des_nombres ,' ' || rhempleados.des_apepaterno || ' ' || rhempleados.des_apematerno) 
		FROM sgrh.tsgrhempleados rhempleados
		WHERE rhempleados.cod_empleado = incidencias.aud_codcreadopor
		) creanb
FROM sgnom.tsgnomincidencia incidencias
JOIN sgnom.tsgnomcatincidencia catincidencias 
ON incidencias.cod_catincidenciaid_fk = catincidencias.cod_catincidenciaid
--WHERE incidencias.cod_empreporta_fk = idempleado 
	AND incidencias.cod_quincenaid_fk = quincena
	AND incidencias.bol_estatus = true 
ORDER BY 1 DESC, 2;

END;
$BODY$
  LANGUAGE plpgsql ;

--------------------------------------- FUNCION INCIDENCIAS QUINCENA ACTUAL
DROP FUNCTION IF EXISTS sgnom.incidencias_quincena();

CREATE OR REPLACE FUNCTION "sgnom"."incidencias_quincena"()
  RETURNS TABLE("idincidencia" int4, "clave" varchar, "incidencia" varchar, "idtipo" bpchar, "desctipo" text, "cantidad" numeric, "actividad" varchar, "detallefechas" text, "comentarios" text, "importe" numeric, "reporta" int4, "autoriza" int4, "nombre" text, "perfil" varchar, "aceptacion" bool, "validacion" bool, "modifica" int4, "autpago" bool, "quincenaid" int4, "desquincena" varchar) AS $BODY$
BEGIN 
RETURN QUERY 
SELECT incidencias.cod_incidenciaid idincidencia,
        catincidencias.cod_claveincidencia clave,
        catincidencias.cod_nbincidencia incidencia,
        catincidencias.cod_tipoincidencia idtipo,
        CASE 
            WHEN catincidencias.cod_tipoincidencia='1' THEN
             'HORAS'
            WHEN catincidencias.cod_tipoincidencia='2' THEN
             'DIAS' 
          WHEN catincidencias.cod_tipoincidencia='3' THEN
             'ACTIVIDAD' 
            ELSE
             'NO DATA'
        END desctipo,
        incidencias.cnu_cantidad cantidad,
        incidencias.des_actividad actividad,
				CAST(incidencias.xml_detcantidad AS TEXT) detallefechas,
        incidencias.txt_comentarios comentarios,
        incidencias.imp_monto importe,
        incidencias.cod_empreporta_fk reporta, --Codigo del empleado que reporta (sgnom)
				incidencias.cod_empautoriza_fk autoriza, --Codigo del empleado que autoriza (sgnom)
        CONCAT(rhempleados.des_nombre ||' ', rhempleados.des_nombres ,' ' || rhempleados.des_apepaterno || ' ' || rhempleados.des_apematerno), --Nombre del empleado (RH)
        catincidencias.cod_perfilincidencia perfil, --Perfil del empleado que reporta (Incidencia)
				incidencias.bol_aceptacion aceptacion,
        incidencias.bol_validacion validacion,
				incidencias.aud_codmodificadopor modifica,
				incidencias.bol_pago autpago,
				incidencias.cod_quincenaid_fk quincenaid,
				(SELECT qui.des_quincena FROM sgnom.tsgnomquincena qui WHERE qui.cod_quincenaid = incidencias.cod_quincenaid_fk) desquincena
FROM sgnom.tsgnomincidencia incidencias
JOIN sgnom.tsgnomcatincidencia catincidencias ON incidencias.cod_catincidenciaid_fk = catincidencias.cod_catincidenciaid
JOIN sgnom.tsgnomempleados nomemp ON incidencias.cod_empreporta_fk = nomemp.cod_empleadoid
JOIN sgrh.tsgrhempleados rhempleados ON rhempleados.cod_empleado = nomemp.cod_empleado_fk
WHERE incidencias.cod_quincenaid_fk = (SELECT nomquincena.cod_quincenaid FROM sgnom.tsgnomquincena nomquincena
WHERE nomquincena.fec_inicio <= CURRENT_DATE AND nomquincena.fec_fin >= CURRENT_DATE)AND incidencias.bol_estatus = true 
ORDER BY idincidencia;
END;
$BODY$
  LANGUAGE plpgsql ;

--------------------------------------- FUNCION INSERTAR INCIDENCIA---
--DROP FUNCTION IF EXISTS "sgnom"."insertar_incidencia_por_empleado"("incidenciaid" int4, "cantidad" int2, "actividad" varchar, "comentarios" text, "reporta" int4, "crea" int4, "monto" numeric, "fechas" varchar);

CREATE OR REPLACE FUNCTION "sgnom"."insertar_incidencia_por_empleado"("incidenciaid" int4, "cantidad" numeric, "actividad" varchar, "comentarios" text, "reporta" int4, "crea" int4, "monto" numeric, "fechas" varchar)
  RETURNS "pg_catalog"."bool" AS $BODY$

DECLARE vXML varchar(4000);
arregloFechas DATE[];

BEGIN 
arregloFechas = fechas::DATE[];
vXML :='<?xml version="1.0" encoding="UTF-8"?>                
		<DetalleFechas>';
FOR i IN 1 .. array_upper(arregloFechas, 1)
   LOOP
      vXML := vXML ||'<fecha>' || TO_CHAR(arregloFechas[i],'dd-MM-yyyy') || '</fecha>';
   END LOOP;
vXML := vXML || '</DetalleFechas>';

	INSERT INTO sgnom.tsgnomincidencia(
											"cod_incidenciaid",
											"cod_catincidenciaid_fk", 
											"cnu_cantidad", 
											"des_actividad", 
											"txt_comentarios", 
											"cod_empreporta_fk",
										  "imp_monto",
										  "xml_detcantidad",
											"bol_estatus", 
											"cod_quincenaid_fk",
											"aud_codcreadopor",
											"aud_feccreacion")
			VALUES (
											NEXTVAL('sgnom.seq_incidencia'::regclass), 
											incidenciaid, 
											cantidad, 
											actividad, 
											comentarios, 
											reporta,--nom
											monto,
											vXML :: XML,
											't',
											(SELECT nomquincena.cod_quincenaid 
												FROM sgnom.tsgnomquincena nomquincena
												WHERE nomquincena.fec_inicio <= CURRENT_DATE AND nomquincena.fec_fin >= CURRENT_DATE),
											crea,
											CURRENT_DATE
								);RETURN TRUE;
END;
$BODY$
  LANGUAGE plpgsql ;
