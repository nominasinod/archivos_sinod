/*
 Navicat Premium Data Transfer

 Source Server         : suite
 Source Server Type    : PostgreSQL
 Source Server Version : 100006
 Source Host           : localhost:5432
 Source Catalog        : suite
 Source Schema         : sgnom

 Target Server Type    : PostgreSQL
 Target Server Version : 100006
 File Encoding         : 65001

 Date: 19/09/2019 16:26:02
*/


-- ----------------------------
-- Table structure for tsgnomincidencia
-- ----------------------------
DROP TABLE IF EXISTS "sgnom"."tsgnomincidencia";
CREATE TABLE "sgnom"."tsgnomincidencia" (
  "cod_incidenciaid" int4 NOT NULL,
  "cod_catincidenciaid_fk" int4 NOT NULL,
  "cnu_cantidad" int2,
  "des_actividad" varchar(100) COLLATE "pg_catalog"."default",
  "txt_comentarios" text COLLATE "pg_catalog"."default",
  "cod_empreporta_fk" int4,
  "cod_empautoriza_fk" int4,
  "imp_monto" numeric(10,2),
  "xml_detcantidad" xml,
  "bol_estatus" bool,
  "cod_quincenaid_fk" int4 NOT NULL,
  "bol_validacion" bool,
  "fec_validacion" date,
  "aud_codcreadopor" int4 NOT NULL,
  "aud_feccreacion" date NOT NULL,
  "aud_codmodificadopor" int4,
  "aud_fecmodificacion" date,
  "bol_aceptacion" bool
)
;
COMMENT ON COLUMN "sgnom"."tsgnomincidencia"."bol_validacion" IS 'Validacion de la incidencia por parte de RH (pasa a finanzas)';
COMMENT ON COLUMN "sgnom"."tsgnomincidencia"."bol_aceptacion" IS 'Validacion de la incidencia por parte del lider de celula (pasa a RH)';
COMMENT ON TABLE "sgnom"."tsgnomincidencia" IS 'bol_estatus: (activo, inactivo) (1, 0)

validacion: (validar, denegar) (1, 0) 



';

-- ----------------------------
-- Records of tsgnomincidencia
-- ----------------------------
INSERT INTO "sgnom"."tsgnomincidencia" VALUES (9, 6, 1, NULL, 'extras', 13, NULL, NULL, NULL, 'f', 18, 't', NULL, 16, '2019-05-25', 13, '2019-08-26', NULL);
INSERT INTO "sgnom"."tsgnomincidencia" VALUES (15, 5, 3, '', 'ej. agosto', 13, NULL, NULL, NULL, 'f', 18, NULL, NULL, 13, '2019-08-26', 13, '2019-08-26', NULL);
INSERT INTO "sgnom"."tsgnomincidencia" VALUES (23, 10, 0, 'prueba agosto prueba agosto prueba agosto prueba agosto prueba agosto prueba agosto prueba agosto 00', 'entrega', 13, NULL, NULL, NULL, 'f', 18, NULL, NULL, 13, '2019-08-26', 13, '2019-08-27', NULL);
INSERT INTO "sgnom"."tsgnomincidencia" VALUES (43, 4, 1, '', 'fechas ARREGLO', 13, NULL, 0.00, '                
		<DetalleFechas><fecha>2019-08-18</fecha><fecha>2019-08-19</fecha><fecha>2019-08-20</fecha></DetalleFechas>', 'f', 18, NULL, NULL, 13, '2019-09-05', 13, '2019-09-05', NULL);
INSERT INTO "sgnom"."tsgnomincidencia" VALUES (41, 4, NULL, NULL, NULL, 13, NULL, NULL, '                
		<DetalleFechas><fecha>2019-08-20</fecha><fecha>2019-08-20</fecha><fecha>2019-08-21</fecha></DetalleFechas>', 'f', 18, NULL, NULL, 13, '2019-09-02', 13, '2019-09-04', NULL);
INSERT INTO "sgnom"."tsgnomincidencia" VALUES (1, 7, 1, NULL, 'CURSO ALUMNOS', 13, NULL, NULL, NULL, 'f', 18, NULL, NULL, 16, '2019-05-25', 16, '2019-07-16', NULL);
INSERT INTO "sgnom"."tsgnomincidencia" VALUES (2, 7, 1, NULL, 'CURSO ALUMNOS', 13, NULL, NULL, NULL, 'f', 18, NULL, NULL, 16, '2019-05-25', 16, '2019-07-16', NULL);
INSERT INTO "sgnom"."tsgnomincidencia" VALUES (3, 9, 0, '', '', 13, NULL, NULL, NULL, 'f', 18, NULL, NULL, 16, '2019-07-17', NULL, NULL, NULL);
INSERT INTO "sgnom"."tsgnomincidencia" VALUES (4, 6, 0, '', '', 13, NULL, NULL, NULL, 'f', 18, NULL, NULL, 16, '2019-07-17', NULL, NULL, NULL);
INSERT INTO "sgnom"."tsgnomincidencia" VALUES (5, 9, 0, '', '', 13, NULL, NULL, NULL, 'f', 18, NULL, NULL, 16, '2019-07-17', NULL, NULL, NULL);
INSERT INTO "sgnom"."tsgnomincidencia" VALUES (6, 6, 0, '', '', 13, NULL, NULL, NULL, 'f', 18, 'f', NULL, 16, '2019-07-17', 16, '2019-08-22', NULL);
INSERT INTO "sgnom"."tsgnomincidencia" VALUES (10, 7, 1, NULL, 'curso 5 alumnos', 13, NULL, NULL, NULL, 'f', 18, 't', NULL, 17, '2019-05-25', 13, '2019-08-26', NULL);
INSERT INTO "sgnom"."tsgnomincidencia" VALUES (11, 7, 1, NULL, 'curso 12 alumnos', 13, NULL, NULL, NULL, 'f', 18, 't', NULL, 17, '2019-05-25', 13, '2019-08-26', NULL);
INSERT INTO "sgnom"."tsgnomincidencia" VALUES (37, 4, 2, 'actividad', 'comentarios', 13, NULL, 0.00, '                
		<DetalleFechas><fecha>2019-08-20</fecha><fecha>2019-08-20</fecha><fecha>2019-08-21</fecha></DetalleFechas>', 'f', 18, NULL, NULL, 13, '2019-09-02', 13, '2019-09-05', NULL);
INSERT INTO "sgnom"."tsgnomincidencia" VALUES (39, 4, 2, NULL, NULL, 13, NULL, 0.00, '                
		<DetalleFechas><fecha>2019-08-20</fecha><fecha>2019-08-20</fecha><fecha>2019-08-21</fecha></DetalleFechas>', 'f', 18, NULL, NULL, 13, '2019-09-02', 13, '2019-09-05', NULL);
INSERT INTO "sgnom"."tsgnomincidencia" VALUES (8, 6, 2, NULL, 'EXTRAS', 13, NULL, NULL, NULL, 't', 18, 'f', NULL, 16, '2019-05-25', NULL, NULL, NULL);
INSERT INTO "sgnom"."tsgnomincidencia" VALUES (16, 10, 0, '3X3 CURSO', 'integrar', 13, 12, NULL, NULL, 't', 18, 'f', NULL, 13, '2019-08-26', NULL, NULL, NULL);
INSERT INTO "sgnom"."tsgnomincidencia" VALUES (31, 11, 7, '', '', 13, NULL, NULL, NULL, 't', 18, 'f', NULL, 13, '2019-08-27', NULL, NULL, NULL);
INSERT INTO "sgnom"."tsgnomincidencia" VALUES (46, 4, 1, '', 'ejemplo', 13, NULL, 0.00, '                
		<DetalleFechas><fecha>2019-08-18</fecha><fecha>2019-08-19</fecha><fecha>2019-08-20</fecha></DetalleFechas>', 'f', 18, NULL, NULL, 13, '2019-09-06', 13, '2019-09-09', NULL);
INSERT INTO "sgnom"."tsgnomincidencia" VALUES (48, 4, 1, '', 'INSERTAR CON STRING', 13, NULL, 0.00, '                
		<DetalleFechas><fecha>2019-08-18</fecha><fecha>2019-08-19</fecha><fecha>2019-08-20</fecha></DetalleFechas>', 'f', 18, NULL, NULL, 13, '2019-09-06', 13, '2019-09-09', NULL);
INSERT INTO "sgnom"."tsgnomincidencia" VALUES (47, 4, 1, '', 'ejemplo', 13, NULL, 0.00, '                
		<DetalleFechas><fecha>2019-08-18</fecha><fecha>2019-08-19</fecha><fecha>2019-08-20</fecha></DetalleFechas>', 'f', 18, NULL, NULL, 13, '2019-09-06', 13, '2019-09-10', NULL);
INSERT INTO "sgnom"."tsgnomincidencia" VALUES (28, 1, 0, '', ' coments coments coments  coments coments coments  coments coments coments  coments coments coments  coments coments coments  coments coments coments  coments coments coments  coments coments coments ', 13, NULL, NULL, NULL, 't', 18, 'f', NULL, 13, '2019-08-27', NULL, NULL, NULL);
INSERT INTO "sgnom"."tsgnomincidencia" VALUES (29, 1, 1, '', ' coments coments coments  coments coments coments  coments coments coments  coments coments coments  coments coments coments  coments coments coments  coments coments coments  coments coments coments ', 13, NULL, NULL, NULL, 't', 18, 'f', NULL, 13, '2019-08-27', NULL, NULL, NULL);
INSERT INTO "sgnom"."tsgnomincidencia" VALUES (36, 4, 1, '', 'ejemplo', 13, NULL, 0.00, '                
		<DetalleFechas><fecha>2019-08-18</fecha><fecha>2019-08-19</fecha><fecha>2019-08-20</fecha></DetalleFechas>', 't', 18, 'f', NULL, 13, '2019-08-28', NULL, NULL, NULL);
INSERT INTO "sgnom"."tsgnomincidencia" VALUES (42, 4, 2, '', NULL, 13, NULL, NULL, '                
		<DetalleFechas><fecha>2019-08-20</fecha><fecha>2019-08-20</fecha><fecha>2019-08-21</fecha></DetalleFechas>', 't', 18, 'f', NULL, 13, '2019-09-02', 13, '2019-09-06', NULL);
INSERT INTO "sgnom"."tsgnomincidencia" VALUES (44, 4, 1, '', 'fechas ARREGLO', 13, NULL, 0.00, '                
		<DetalleFechas><fecha>2019-08-18</fecha><fecha>2019-08-19</fecha><fecha>2019-08-20</fecha></DetalleFechas>', 't', 18, 'f', NULL, 13, '2019-09-05', NULL, NULL, NULL);
INSERT INTO "sgnom"."tsgnomincidencia" VALUES (49, 1, 2, '.', 'example', 13, NULL, 0.00, '                
		<DetalleFechas><fecha>2019-08-18</fecha><fecha>2019-08-19</fecha><fecha>2019-08-20</fecha></DetalleFechas>', 't', 18, 'f', NULL, 13, '2019-09-06', NULL, NULL, NULL);
INSERT INTO "sgnom"."tsgnomincidencia" VALUES (50, 8, 1, 'k', 'll', 13, NULL, 0.00, '                
		<DetalleFechas><fecha>2019-09-05</fecha><fecha>2019-09-03</fecha></DetalleFechas>', 't', 18, 'f', NULL, 13, '2019-09-06', NULL, NULL, NULL);
INSERT INTO "sgnom"."tsgnomincidencia" VALUES (52, 10, 0, 'prueba', 'l', 13, NULL, 0.00, '                
		<DetalleFechas><fecha>2019-09-04</fecha></DetalleFechas>', 't', 18, 'f', NULL, 13, '2019-09-09', NULL, NULL, NULL);
INSERT INTO "sgnom"."tsgnomincidencia" VALUES (38, 4, 2, 'actividad', NULL, 13, NULL, 0.00, '                
		<DetalleFechas><fecha>2019-08-20</fecha><fecha>2019-08-20</fecha><fecha>2019-08-21</fecha></DetalleFechas>', 'f', 18, NULL, NULL, 13, '2019-09-02', 13, '2019-09-09', NULL);
INSERT INTO "sgnom"."tsgnomincidencia" VALUES (45, 4, 1, '', 'ejemplo', 13, NULL, 0.00, '                
		<DetalleFechas><fecha>2019-08-18</fecha><fecha>2019-08-19</fecha><fecha>2019-08-20</fecha></DetalleFechas>', 't', 18, 't', NULL, 13, '2019-09-06', 13, '2019-09-09', NULL);
INSERT INTO "sgnom"."tsgnomincidencia" VALUES (54, 8, 3, '', 'prueba cantidad', 13, NULL, 0.00, '                
		<DetalleFechas><fecha>2019-09-06</fecha></DetalleFechas>', 't', 18, 'f', NULL, 13, '2019-09-09', NULL, NULL, NULL);
INSERT INTO "sgnom"."tsgnomincidencia" VALUES (20, 1, 1, '', 'agosto horas extra', 13, NULL, NULL, NULL, 't', 18, 'f', NULL, 13, '2019-08-26', NULL, NULL, NULL);
INSERT INTO "sgnom"."tsgnomincidencia" VALUES (22, 10, 0, 'entrega parcial de proyecto', 'actualizaci�n de requerimientos aprobada por el cliente', 13, NULL, NULL, NULL, 't', 18, 'f', NULL, 13, '2019-08-26', NULL, NULL, NULL);
INSERT INTO "sgnom"."tsgnomincidencia" VALUES (26, 1, 1, '', 'ejemplo', 13, NULL, NULL, NULL, 't', 18, 'f', NULL, 13, '2019-08-27', NULL, NULL, NULL);
INSERT INTO "sgnom"."tsgnomincidencia" VALUES (27, 1, 2, '', ' coments coments coments  coments coments coments  coments coments coments  coments coments coments  coments coments coments  coments coments coments  coments coments coments  coments coments coments ', 13, NULL, NULL, NULL, 't', 18, 'f', NULL, 13, '2019-08-27', NULL, NULL, NULL);
INSERT INTO "sgnom"."tsgnomincidencia" VALUES (19, 12, 2, '', 'clase de mvc', 13, 14, NULL, NULL, 't', 18, 'f', NULL, 13, '2019-08-26', NULL, NULL, NULL);
INSERT INTO "sgnom"."tsgnomincidencia" VALUES (30, 8, 2, '', 'horario completo', 13, NULL, NULL, NULL, 't', 18, 'f', NULL, 13, '2019-08-27', NULL, NULL, NULL);

-- ----------------------------
-- Primary Key structure for table tsgnomincidencia
-- ----------------------------
ALTER TABLE "sgnom"."tsgnomincidencia" ADD CONSTRAINT "nom_incidencia_id" PRIMARY KEY ("cod_incidenciaid");

-- ----------------------------
-- Foreign Keys structure for table tsgnomincidencia
-- ----------------------------
ALTER TABLE "sgnom"."tsgnomincidencia" ADD CONSTRAINT "nom_cat_incidencia_id_fk_incidencias" FOREIGN KEY ("cod_catincidenciaid_fk") REFERENCES "sgnom"."tsgnomcatincidencia" ("cod_catincidenciaid") ON DELETE NO ACTION ON UPDATE CASCADE;
ALTER TABLE "sgnom"."tsgnomincidencia" ADD CONSTRAINT "nom_cat_quincena_id_fk_incidencias" FOREIGN KEY ("cod_quincenaid_fk") REFERENCES "sgnom"."tsgnomquincena" ("cod_quincenaid") ON DELETE NO ACTION ON UPDATE CASCADE;
ALTER TABLE "sgnom"."tsgnomincidencia" ADD CONSTRAINT "nom_emp_autoriza_fk_incidencias" FOREIGN KEY ("cod_empautoriza_fk") REFERENCES "sgnom"."tsgnomempleados" ("cod_empleadoid") ON DELETE NO ACTION ON UPDATE CASCADE;
ALTER TABLE "sgnom"."tsgnomincidencia" ADD CONSTRAINT "nom_emp_reporta_fk_incidencias" FOREIGN KEY ("cod_empreporta_fk") REFERENCES "sgnom"."tsgnomempleados" ("cod_empleadoid") ON DELETE NO ACTION ON UPDATE CASCADE;
ALTER TABLE "sgnom"."tsgnomincidencia" ADD CONSTRAINT "tsgrh_empleados_id_fk_incidencias_c" FOREIGN KEY ("aud_codcreadopor") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "sgnom"."tsgnomincidencia" ADD CONSTRAINT "tsgrh_empleados_id_fk_incidencias_m" FOREIGN KEY ("aud_codmodificadopor") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE NO ACTION ON UPDATE NO ACTION;





-- ----------------------------
-- SEQUENCE structure for table tsgnomincidencia
-- ----------------------------

DROP SEQUENCE IF EXISTS sgnom.seq_incidencia;

CREATE SEQUENCE sgnom.seq_incidencia
    START WITH 60
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgnom.seq_incidencia OWNER TO suite;