CREATE OR REPLACE FUNCTION "sgnom"."insertar_incidencia_por_empleado"("incidenciaid" int4, "cantidad" int4, "actividad" varchar, "comentarios" text, "reporta" int4, "monto" numeric, "fechas" varchar)
  RETURNS "pg_catalog"."bool" AS $BODY$

DECLARE vXML varchar(4000);
arregloFechas DATE[];

BEGIN 
arregloFechas = fechas::DATE[];
vXML :='<?xml version="1.0" encoding="UTF-8"?>                
		<DetalleFechas>';
FOR i IN 1 .. array_upper(arregloFechas, 1)
   LOOP
      vXML := vXML ||'<fecha>' || TO_CHAR(arregloFechas[i],'dd-MM-yyyy') || '</fecha>';
   END LOOP;
vXML := vXML || '</DetalleFechas>';

	INSERT INTO sgnom.tsgnomincidencia(
											"cod_incidenciaid",
											"cod_catincidenciaid_fk", 
											"cnu_cantidad", 
											"des_actividad", 
											"txt_comentarios", 
											"cod_empreporta_fk",
										  "imp_monto",
										  "xml_detcantidad",
											"bol_estatus", 
											"cod_quincenaid_fk",
											"aud_codcreadopor",
											"aud_feccreacion")
			VALUES (
											NEXTVAL('sgnom.seq_incidencia'::regclass), 
											incidenciaid, 
											cantidad, 
											actividad, 
											comentarios, 
											(SELECT emp.cod_empleadoid
												FROM sgnom.tsgnomempleados emp 
												WHERE emp.cod_empleado_fk = reporta),--rh
											monto,
											vXML :: XML,
											't',
											(SELECT nomquincena.cod_quincenaid 
												FROM sgnom.tsgnomquincena nomquincena
												WHERE nomquincena.fec_inicio <= CURRENT_DATE AND nomquincena.fec_fin >= CURRENT_DATE),
											reporta,
											CURRENT_DATE
								);RETURN TRUE;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100