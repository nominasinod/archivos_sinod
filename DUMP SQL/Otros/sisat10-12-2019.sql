--
-- PostgreSQL database dump
--

-- Dumped from database version 10.6
-- Dumped by pg_dump version 10.6

-- Started on 2019-12-10 14:55:08

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 13 (class 2615 OID 59745)
-- Name: sisat; Type: SCHEMA; Schema: -; Owner: suite
--

CREATE SCHEMA sisat;


ALTER SCHEMA sisat OWNER TO suite;

--
-- TOC entry 3881 (class 0 OID 0)
-- Dependencies: 13
-- Name: SCHEMA sisat; Type: COMMENT; Schema: -; Owner: suite
--

COMMENT ON SCHEMA sisat IS 'Esquema que contiene las tablas del Sistema de Seleccion y Adquisicion de Talentos';


--
-- TOC entry 525 (class 1255 OID 59746)
-- Name: buscar_asignacion_recurso(integer); Type: FUNCTION; Schema: sisat; Owner: suite
--

CREATE FUNCTION sisat.buscar_asignacion_recurso(asignacion_cod integer) RETURNS TABLE(cod_asignacion integer, cod_prospecto integer, nombreprospecto text, des_perfil character varying, des_observacion character varying, des_actividades character varying, des_lugarsalida character varying, des_lugarllegada character varying, fec_llegada date, fec_salida date, cod_transporte character varying, des_lugarhopedaje character varying, fec_hospedaje date, des_computadora character varying, cod_telefono character varying, des_accesorios character varying, des_nbresponsable character varying, des_nbpuesto character varying, des_lugarresp character varying, cod_telefonoresp character varying, tim_horario time without time zone, fec_iniciocontra date, fec_terminocontra date, imp_sueldomensual numeric, imp_nominaimss numeric, imp_honorarios numeric, imp_otros numeric, cod_rfc character varying, des_razonsocial character varying, des_correo character varying, cod_cpostal integer, des_direccionfact character varying, des_nbcliente character varying, des_direccioncte character varying, des_nbcontactocte character varying, des_correocte character varying, cod_telefonocte character varying, audcodmodificacion integer, audfechamodificacion date, cod_empleado integer, codrhta text, codgpy text, codape text, codrys text)
    LANGUAGE plpgsql
    AS $$
BEGIN 
RETURN QUERY
SELECT c.cod_asignacion, p.cod_prospecto, p.des_nombre ||' '|| (case when p.des_nombres is NULL then '' else p.des_nombres::character varying(60) end) ||' '|| p.des_appaterno ||' '|| p.des_apmaterno AS NombreProspecto, perf.des_perfil, c.des_observacion, 
    c.des_actividades, c.des_lugarsalida, c.des_lugarllegada, c.fec_llegada,c.fec_salida, c.cod_transporte, c.des_lugarhopedaje, 
    c.fec_hospedaje, c.des_computadora, c.cod_telefono, c.des_accesorios, c.des_nbresponsable, c.des_nbpuesto, c.des_lugarresp, 
    c.cod_telefonoresp, c.tim_horario, c.fec_iniciocontra, c.fec_terminocontra, c.imp_sueldomensual, c.imp_nominaimss, 
    c.imp_honorarios, c.imp_otros,c.cod_rfc, c.des_razonsocial, c.des_correo, c.cod_cpostal, c.des_direccionfact, 
    cte.des_nbcliente, cte.des_direccioncte, cte.des_nbcontactocte, cte.des_correocte, cte.cod_telefonocte,c.aud_cod_modificadopor, c.aud_fecha_modificacion, emp.cod_empleado,
    emp.des_nombre ||' '|| (case when emp.des_nombres is NULL then '' else emp.des_nombres::character varying(60) end) ||' '|| emp.des_apepaterno ||' '|| emp.des_apematerno AS Cod_Rhta, (
    
    SELECT 
    empl.des_nombre ||' '|| (case when empl.des_nombres is NULL then '' else empl.des_nombres::character varying(60) end) ||' '|| empl.des_apepaterno ||' '|| empl.des_apematerno AS CodGpy
        
    FROM sisat.tsisatcartaasignacion ca
    inner join  sisat.tsisatprospectos pr on pr.cod_prospecto= ca.cod_prospecto
    inner join sgrh.tsgrhempleados empl on empl.cod_empleado = ca.cod_gpy 
    WHERE ca.cod_asignacion = c.cod_asignacion
    ), (
    SELECT 
    empl.des_nombre ||' '|| (case when empl.des_nombres is NULL then '' else empl.des_nombres::character varying(60) end) ||' '|| empl.des_apepaterno ||' '|| empl.des_apematerno AS CodApe
        
    FROM sisat.tsisatcartaasignacion ca
    inner join  sisat.tsisatprospectos pr on pr.cod_prospecto= ca.cod_prospecto
    inner join sgrh.tsgrhempleados empl on empl.cod_empleado = ca.cod_ape
    WHERE ca.cod_asignacion = c.cod_asignacion), (
    SELECT 
    empl.des_nombre ||' '|| (case when empl.des_nombres is NULL then '' else empl.des_nombres::character varying(60) end) ||' '|| empl.des_apepaterno ||' '|| empl.des_apematerno AS CodRys
        
    FROM sisat.tsisatcartaasignacion ca
    inner join  sisat.tsisatprospectos pr on pr.cod_prospecto= ca.cod_prospecto
    inner join sgrh.tsgrhempleados empl on empl.cod_empleado = ca.cod_rys 
    WHERE ca.cod_asignacion = c.cod_asignacion
    )
        
FROM sisat.tsisatcartaasignacion c
inner join  sgrh.tsgrhperfiles perf on perf.cod_perfil=c.cod_perfil
inner join sgrh.tsgrhclientes cte on cte.cod_cliente=c.cod_cliente
inner join  sisat.tsisatprospectos p on p.cod_prospecto= c.cod_prospecto
inner join sgrh.tsgrhempleados emp on emp.cod_empleado = c.cod_rhta
WHERE p.cod_prospecto= asignacion_cod
GROUP BY c.cod_asignacion, p.cod_prospecto, NombreProspecto, perf.des_perfil, c.des_observacion, 
    c.des_actividades, c.des_lugarsalida, c.des_lugarllegada, c.fec_llegada,c.fec_salida, c.cod_transporte, c.des_lugarhopedaje, 
    c.fec_hospedaje, c.des_computadora, c.cod_telefono, c.des_accesorios, c.des_nbresponsable, c.des_nbpuesto, c.des_lugarresp, 
    c.cod_telefonoresp, c.tim_horario, c.fec_iniciocontra, c.fec_terminocontra, c.imp_sueldomensual, c.imp_nominaimss, 
    c.imp_honorarios, c.imp_otros,c.cod_rfc, c.des_razonsocial, c.des_correo, c.cod_cpostal, c.des_direccionfact, 
    cte.des_nbcliente, cte.des_direccioncte, cte.des_nbcontactocte, cte.des_correocte, cte.cod_telefonocte,c.aud_cod_modificadopor, c.aud_fecha_modificacion, emp.cod_empleado, CodRhta 
ORDER BY cod_asignacion  asc;
END;
$$;


ALTER FUNCTION sisat.buscar_asignacion_recurso(asignacion_cod integer) OWNER TO suite;

--
-- TOC entry 522 (class 1255 OID 59747)
-- Name: buscar_clientes(integer); Type: FUNCTION; Schema: sisat; Owner: postgres
--

CREATE FUNCTION sisat.buscar_clientes(clienteid integer) RETURNS TABLE(cod_cliente integer, des_razonsocial character varying, cod_rfc character varying, des_direccioncte character varying, cod_cpostal integer, des_correocte character varying, cod_telefonocte character varying, des_nbcliente character varying)
    LANGUAGE plpgsql COST 1
    AS $$
BEGIN
RETURN QUERY
select 
cte.cod_cliente,
cart.des_razonsocial,
cart.cod_rfc,
cte.des_direccioncte,
cart.cod_cpostal,
cte.des_correocte,
cte.cod_telefonocte,
cte.des_nbcliente
from sgrh.tsgrhclientes cte join sisat.tsisatcartaasignacion cart  using(cod_cliente)
WHERE cte.cod_cliente = clienteid
group by cte.cod_cliente,cart.des_razonsocial,cart.cod_rfc,cte.des_direccioncte,cart.cod_cpostal,cte.des_correocte,cte.cod_telefonocte,cte.des_nbcliente  order by cod_cliente asc;
END;
$$;


ALTER FUNCTION sisat.buscar_clientes(clienteid integer) OWNER TO postgres;

--
-- TOC entry 523 (class 1255 OID 59748)
-- Name: buscar_prospectos(integer); Type: FUNCTION; Schema: sisat; Owner: postgres
--

CREATE FUNCTION sisat.buscar_prospectos(prospectoid integer) RETURNS TABLE(cod_prospecto integer, nombre_completo text, des_actividades character varying, imp_sueldomensual numeric, imp_nominaimss numeric, imp_honorarios numeric, imp_otros numeric)
    LANGUAGE plpgsql
    AS $$
BEGIN
RETURN QUERY
select
pros.cod_prospecto,
pros.des_nombre || ' '  ||
(case when pros.des_nombres is null then '' else pros.des_nombres::character varying(60) end) || ' '  ||
pros.des_appaterno || ' ' ||
pros.des_apmaterno as Nombre_completo,
cart.des_actividades,
cart.imp_sueldomensual,
cart.imp_nominaimss,
cart.imp_honorarios,
cart.imp_otros
from sisat.tsisatcartaasignacion cart join sisat.tsisatprospectos pros using(cod_prospecto)
WHERE pros.cod_prospecto = prospectoid
GROUP BY pros.cod_prospecto, Nombre_completo ,cart.des_actividades,cart.imp_sueldomensual,cart.imp_nominaimss,cart.imp_honorarios,cart.imp_otros
ORDER BY cod_prospecto  asc;
END;
$$;


ALTER FUNCTION sisat.buscar_prospectos(prospectoid integer) OWNER TO postgres;

--
-- TOC entry 524 (class 1255 OID 59749)
-- Name: requisicion_de_personal(integer); Type: FUNCTION; Schema: sisat; Owner: suite
--

CREATE FUNCTION sisat.requisicion_de_personal(cliente integer) RETURNS TABLE(codvacante integer, fecsolicitud date, fecentrega date, desrqvacante character varying, desescolaridad character varying, sexo character varying, cnuanexperiencia smallint, txtexperiencia text, txtconocimientostecno text, codnbidioma character varying, codnivelidioma character varying, fecinicio date, fectermino date, desesquema character varying, codsalarioestmin numeric, codsalarioestmax numeric, timjornada character varying, desnbcliente character varying, desdireccioncte character varying, codtelefonocte character varying, descorreocte character varying, nbempleado text, nbsolicita text)
    LANGUAGE plpgsql
    AS $$
BEGIN 
RETURN QUERY
SELECT v.cod_vacante, v.fec_solicitud, v.fec_entrega, v.des_rqvacante, 
		v.des_escolaridad, v.sexo, v.cnu_anexperiencia, v.txt_experiencia, 
		v.txt_conocimientostecno, i.cod_nbidioma, v.cod_nivelidioma, c.fec_inicio,
		c.fec_termino,c.des_esquema, c.cod_salarioestmin, c.cod_salarioestmax, 
		c.tim_jornada, cte.des_nbcliente, cte.des_direccioncte, cte.cod_telefonocte,
		cte.des_correocte, (SELECT 
    empl.des_nombre ||' '|| (case when empl.des_nombres is NULL then '' else empl.des_nombres::character varying(60) end) ||' '|| empl.des_apepaterno ||' '|| empl.des_apematerno AS nbempleado
        
    FROM sisat.tsisatfirmareqper f
    inner join  sisat.tsisatvacantes pr on pr.cod_vacante = f.cod_vacante
    inner join sgrh.tsgrhempleados empl on empl.cod_empleado = f.cod_solicita 
    WHERE f.cod_vacante = v.cod_vacante),(SELECT 
    empl.des_nombre ||' '|| (case when empl.des_nombres is NULL then '' else empl.des_nombres::character varying(60) end) ||' '|| empl.des_apepaterno ||' '|| empl.des_apematerno AS nbautoriza
        
    FROM sisat.tsisatfirmareqper f
    inner join  sisat.tsisatvacantes pr on pr.cod_vacante = f.cod_vacante
    inner join sgrh.tsgrhempleados empl on empl.cod_empleado = f.cod_autoriza
    WHERE f.cod_vacante = v.cod_vacante)
FROM sisat.tsisatvacantes v
INNER JOIN sisat.tsisatidiomas i ON i.cod_idioma = v.cod_idioma
INNER JOIN sisat.tsisatcontrataciones c ON c.cod_contratacion = v.cod_contratacion
INNER JOIN sgrh.tsgrhclientes cte on cte.cod_cliente=v.cod_cliente
WHERE v.cod_cliente = cliente
GROUP BY v.cod_vacante, v.fec_solicitud, v.fec_entrega, v.des_rqvacante, 
		v.des_escolaridad, v.sexo, v.cnu_anexperiencia, i.cod_nbidioma, v.cod_nivelidioma,
		v.txt_experiencia, v.txt_conocimientostecno, v.cod_cliente, c.fec_inicio, c.fec_termino, 
		c.des_esquema, c.cod_salarioestmin, c.cod_salarioestmax, c.tim_jornada,
		cte.des_nbcliente, cte.des_direccioncte, cte.cod_telefonocte,
		cte.des_correocte
ORDER BY fec_solicitud desc;
END;
	$$;


ALTER FUNCTION sisat.requisicion_de_personal(cliente integer) OWNER TO suite;

--
-- TOC entry 387 (class 1259 OID 59750)
-- Name: seq_aceptaciones; Type: SEQUENCE; Schema: sisat; Owner: suite
--

CREATE SEQUENCE sisat.seq_aceptaciones
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sisat.seq_aceptaciones OWNER TO suite;

--
-- TOC entry 388 (class 1259 OID 59752)
-- Name: seq_asignaciones; Type: SEQUENCE; Schema: sisat; Owner: suite
--

CREATE SEQUENCE sisat.seq_asignaciones
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sisat.seq_asignaciones OWNER TO suite;

--
-- TOC entry 389 (class 1259 OID 59754)
-- Name: seq_candidatos; Type: SEQUENCE; Schema: sisat; Owner: suite
--

CREATE SEQUENCE sisat.seq_candidatos
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sisat.seq_candidatos OWNER TO suite;

--
-- TOC entry 390 (class 1259 OID 59756)
-- Name: seq_cartaasignaciones; Type: SEQUENCE; Schema: sisat; Owner: suite
--

CREATE SEQUENCE sisat.seq_cartaasignaciones
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sisat.seq_cartaasignaciones OWNER TO suite;

--
-- TOC entry 391 (class 1259 OID 59758)
-- Name: seq_comentcartaasignacion; Type: SEQUENCE; Schema: sisat; Owner: suite
--

CREATE SEQUENCE sisat.seq_comentcartaasignacion
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sisat.seq_comentcartaasignacion OWNER TO suite;

--
-- TOC entry 392 (class 1259 OID 59760)
-- Name: seq_comentcosteo; Type: SEQUENCE; Schema: sisat; Owner: suite
--

CREATE SEQUENCE sisat.seq_comentcosteo
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sisat.seq_comentcosteo OWNER TO suite;

--
-- TOC entry 393 (class 1259 OID 59762)
-- Name: seq_comententrevista; Type: SEQUENCE; Schema: sisat; Owner: suite
--

CREATE SEQUENCE sisat.seq_comententrevista
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sisat.seq_comententrevista OWNER TO suite;

--
-- TOC entry 394 (class 1259 OID 59764)
-- Name: seq_comentvacantes; Type: SEQUENCE; Schema: sisat; Owner: suite
--

CREATE SEQUENCE sisat.seq_comentvacantes
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sisat.seq_comentvacantes OWNER TO suite;

--
-- TOC entry 395 (class 1259 OID 59766)
-- Name: seq_contrataciones; Type: SEQUENCE; Schema: sisat; Owner: suite
--

CREATE SEQUENCE sisat.seq_contrataciones
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sisat.seq_contrataciones OWNER TO suite;

--
-- TOC entry 396 (class 1259 OID 59768)
-- Name: seq_cotizaciones; Type: SEQUENCE; Schema: sisat; Owner: suite
--

CREATE SEQUENCE sisat.seq_cotizaciones
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sisat.seq_cotizaciones OWNER TO suite;

--
-- TOC entry 397 (class 1259 OID 59770)
-- Name: seq_cursos; Type: SEQUENCE; Schema: sisat; Owner: suite
--

CREATE SEQUENCE sisat.seq_cursos
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sisat.seq_cursos OWNER TO suite;

--
-- TOC entry 398 (class 1259 OID 59772)
-- Name: seq_entrevistas; Type: SEQUENCE; Schema: sisat; Owner: suite
--

CREATE SEQUENCE sisat.seq_entrevistas
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sisat.seq_entrevistas OWNER TO suite;

--
-- TOC entry 399 (class 1259 OID 59774)
-- Name: seq_envios; Type: SEQUENCE; Schema: sisat; Owner: suite
--

CREATE SEQUENCE sisat.seq_envios
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sisat.seq_envios OWNER TO suite;

--
-- TOC entry 400 (class 1259 OID 59776)
-- Name: seq_escolaridad; Type: SEQUENCE; Schema: sisat; Owner: suite
--

CREATE SEQUENCE sisat.seq_escolaridad
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sisat.seq_escolaridad OWNER TO suite;

--
-- TOC entry 401 (class 1259 OID 59778)
-- Name: seq_experiencias; Type: SEQUENCE; Schema: sisat; Owner: suite
--

CREATE SEQUENCE sisat.seq_experiencias
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sisat.seq_experiencias OWNER TO suite;

--
-- TOC entry 402 (class 1259 OID 59780)
-- Name: seq_firmareqper; Type: SEQUENCE; Schema: sisat; Owner: suite
--

CREATE SEQUENCE sisat.seq_firmareqper
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sisat.seq_firmareqper OWNER TO suite;

--
-- TOC entry 403 (class 1259 OID 59782)
-- Name: seq_firmas; Type: SEQUENCE; Schema: sisat; Owner: suite
--

CREATE SEQUENCE sisat.seq_firmas
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sisat.seq_firmas OWNER TO suite;

--
-- TOC entry 404 (class 1259 OID 59784)
-- Name: seq_habilidades; Type: SEQUENCE; Schema: sisat; Owner: suite
--

CREATE SEQUENCE sisat.seq_habilidades
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sisat.seq_habilidades OWNER TO suite;

--
-- TOC entry 405 (class 1259 OID 59786)
-- Name: seq_idiomas; Type: SEQUENCE; Schema: sisat; Owner: suite
--

CREATE SEQUENCE sisat.seq_idiomas
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sisat.seq_idiomas OWNER TO suite;

--
-- TOC entry 406 (class 1259 OID 59788)
-- Name: seq_ordenservicios; Type: SEQUENCE; Schema: sisat; Owner: suite
--

CREATE SEQUENCE sisat.seq_ordenservicios
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sisat.seq_ordenservicios OWNER TO suite;

--
-- TOC entry 407 (class 1259 OID 59790)
-- Name: seq_prospectos; Type: SEQUENCE; Schema: sisat; Owner: suite
--

CREATE SEQUENCE sisat.seq_prospectos
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sisat.seq_prospectos OWNER TO suite;

--
-- TOC entry 408 (class 1259 OID 59792)
-- Name: seq_proyectos; Type: SEQUENCE; Schema: sisat; Owner: suite
--

CREATE SEQUENCE sisat.seq_proyectos
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sisat.seq_proyectos OWNER TO suite;

--
-- TOC entry 409 (class 1259 OID 59794)
-- Name: seq_vacantes; Type: SEQUENCE; Schema: sisat; Owner: suite
--

CREATE SEQUENCE sisat.seq_vacantes
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sisat.seq_vacantes OWNER TO suite;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 410 (class 1259 OID 59796)
-- Name: tsisatappservices; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisatappservices (
    cod_appservice integer NOT NULL,
    des_appservice character varying(30) NOT NULL
);


ALTER TABLE sisat.tsisatappservices OWNER TO suite;

--
-- TOC entry 411 (class 1259 OID 59799)
-- Name: tsisatarquitecturas; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisatarquitecturas (
    cod_arquitectura integer NOT NULL,
    des_arquitectura character varying(30) NOT NULL
);


ALTER TABLE sisat.tsisatarquitecturas OWNER TO suite;

--
-- TOC entry 412 (class 1259 OID 59802)
-- Name: tsisatasignaciones; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisatasignaciones (
    cod_asignacion integer DEFAULT nextval('sisat.seq_asignaciones'::regclass) NOT NULL,
    cod_prospecto integer NOT NULL,
    cod_perfil integer NOT NULL,
    cod_cliente integer NOT NULL,
    des_correocte character varying(40) NOT NULL,
    cod_telefonocte character varying(16) NOT NULL,
    des_direccioncte character varying(200),
    cod_empleado integer,
    aud_cod_creadopor integer NOT NULL,
    aud_cod_modificadopor integer,
    aud_fec_creacion date NOT NULL,
    aud_fec_modificacion date
);


ALTER TABLE sisat.tsisatasignaciones OWNER TO suite;

--
-- TOC entry 413 (class 1259 OID 59806)
-- Name: tsisatcandidatos; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisatcandidatos (
    cod_candidato integer DEFAULT nextval('sisat.seq_candidatos'::regclass) NOT NULL,
    des_nombre character varying(50) NOT NULL,
    cod_perfil integer NOT NULL,
    imp_sueldo numeric(6,2),
    imp_sueldodia numeric(6,2),
    imp_nominaimss numeric(6,2),
    imp_honorarios numeric(6,2),
    imp_cargasocial numeric(6,2),
    imp_prestaciones numeric(6,2),
    imp_viaticos numeric(6,2),
    imp_subtotalcandidato numeric(6,2),
    imp_costoadmin numeric(6,2),
    cnu_financiamiento smallint,
    imp_isr numeric(6,2),
    imp_financiamiento numeric(6,2),
    imp_adicionales numeric(6,2),
    imp_subtotaladmin1 numeric(6,2),
    imp_comisiones numeric(6,2),
    imp_otrosgastos numeric(6,2),
    imp_subtotaladmin2 numeric(6,2),
    imp_total numeric(6,2),
    imp_iva numeric(6,2),
    por_utilidad numeric(4,2),
    imp_utilidad numeric(6,2),
    imp_tarifa numeric(6,2),
    aud_fecha_creacion date NOT NULL,
    aud_fecha_modificacion date,
    aud_cod_creadopor integer NOT NULL,
    aud_cod_modificadopor integer,
    imp_hospedaje numeric(6,2),
    imp_alimentacion numeric(6,2),
    imp_transporte numeric(6,2),
    imp_incentivos numeric(6,2),
    imp_eqcomputo numeric(6,2)
);


ALTER TABLE sisat.tsisatcandidatos OWNER TO suite;

--
-- TOC entry 414 (class 1259 OID 59810)
-- Name: tsisatcartaaceptacion; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisatcartaaceptacion (
    cod_aceptacion integer DEFAULT nextval('sisat.seq_aceptaciones'::regclass) NOT NULL,
    des_objetivo character varying(200) NOT NULL,
    txt_oferta text,
    des_esquema character varying(30) NOT NULL,
    tim_jornada time without time zone,
    txt_especificaciones text,
    aud_cod_creadopor integer NOT NULL,
    aud_cod_modificadopor integer,
    aud_fec_creacion date NOT NULL,
    aud_fec_modificacion date
);


ALTER TABLE sisat.tsisatcartaaceptacion OWNER TO suite;

--
-- TOC entry 415 (class 1259 OID 59817)
-- Name: tsisatcartaasignacion; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisatcartaasignacion (
    cod_asignacion integer DEFAULT nextval('sisat.seq_cartaasignaciones'::regclass) NOT NULL,
    cod_prospecto integer NOT NULL,
    cod_perfil integer NOT NULL,
    des_actividades character varying(200),
    des_lugarsalida character varying(100),
    des_lugarllegada character varying(100),
    fec_salida date,
    fec_llegada date,
    cod_transporte character varying(20),
    des_lugarhopedaje character varying(60),
    fec_hospedaje date,
    des_computadora character varying(150),
    cod_telefono character varying(16),
    des_accesorios character varying(150),
    des_nbresponsable character varying(50),
    des_nbpuesto character varying(50),
    des_lugarresp character varying(100),
    cod_telefonoresp character varying(16),
    tim_horario time without time zone,
    fec_iniciocontra date,
    fec_terminocontra date,
    imp_sueldomensual numeric(6,2),
    imp_nominaimss numeric(6,2),
    imp_honorarios numeric(6,2),
    imp_otros numeric(6,2),
    cod_rfc character varying(13),
    des_razonsocial character varying(45),
    des_correo character varying(50),
    cod_cpostal integer,
    des_direccionfact character varying(200),
    cod_cliente integer NOT NULL,
    cod_gpy integer,
    cod_rhta integer,
    cod_ape integer,
    cod_rys integer,
    des_observacion character varying(200),
    aud_cod_creadopor integer NOT NULL,
    aud_cod_modificadopor integer,
    aud_fecha_creacion date NOT NULL,
    aud_fecha_modificacion date
);


ALTER TABLE sisat.tsisatcartaasignacion OWNER TO suite;

--
-- TOC entry 449 (class 1259 OID 60559)
-- Name: tsisatcomentcartaasignacion; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisatcomentcartaasignacion (
    cod_comentcartaasignacion integer DEFAULT nextval('sisat.seq_comentcartaasignacion'::regclass) NOT NULL,
    des_comentcartaasignacion character varying(300),
    bol_validacion boolean NOT NULL,
    aud_fecha_creacion date NOT NULL,
    aud_fecha_modificacion date,
    aud_cod_creadopor integer NOT NULL,
    aud_cod_modificadopor integer,
    cod_asignacion integer NOT NULL
);


ALTER TABLE sisat.tsisatcomentcartaasignacion OWNER TO suite;

--
-- TOC entry 448 (class 1259 OID 60538)
-- Name: tsisatcomentcosteo; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisatcomentcosteo (
    cod_comentcosteo integer DEFAULT nextval('sisat.seq_comentcosteo'::regclass) NOT NULL,
    des_comentcosteo character varying(300),
    bol_validacion boolean NOT NULL,
    aud_fecha_creacion date NOT NULL,
    aud_fecha_modificacion date,
    aud_cod_creadopor integer NOT NULL,
    aud_cod_modificadopor integer,
    cod_candidato integer NOT NULL
);


ALTER TABLE sisat.tsisatcomentcosteo OWNER TO suite;

--
-- TOC entry 447 (class 1259 OID 60508)
-- Name: tsisatcomententrevista; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisatcomententrevista (
    cod_comententrevista integer DEFAULT nextval('sisat.seq_comententrevista'::regclass) NOT NULL,
    des_comententrevista character varying(500),
    aud_fecha_creacion date NOT NULL,
    aud_fecha_modificacion date,
    aud_cod_creadopor integer NOT NULL,
    aud_cod_modificadopor integer,
    cod_entrevista integer NOT NULL
);


ALTER TABLE sisat.tsisatcomententrevista OWNER TO suite;

--
-- TOC entry 446 (class 1259 OID 60487)
-- Name: tsisatcomentvacantes; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisatcomentvacantes (
    cod_comentvacante integer DEFAULT nextval('sisat.seq_comentvacantes'::regclass) NOT NULL,
    des_comentvacante character varying(300),
    bol_validacion boolean NOT NULL,
    aud_fecha_creacion date NOT NULL,
    aud_fecha_modificacion date,
    aud_cod_creadopor integer NOT NULL,
    aud_cod_modificadopor integer,
    cod_vacante integer NOT NULL
);


ALTER TABLE sisat.tsisatcomentvacantes OWNER TO suite;

--
-- TOC entry 416 (class 1259 OID 59839)
-- Name: tsisatcontrataciones; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisatcontrataciones (
    cod_contratacion integer DEFAULT nextval('sisat.seq_contrataciones'::regclass) NOT NULL,
    fec_inicio date,
    fec_termino date,
    des_esquema character varying(30),
    cod_salarioestmin numeric(6,2),
    cod_salarioestmax numeric(6,2),
    tim_jornada character varying(20),
    aud_cod_creadopor integer NOT NULL,
    aud_cod_modificadopor integer,
    aud_fec_creacion date NOT NULL,
    aud_fec_modificacion date
);


ALTER TABLE sisat.tsisatcontrataciones OWNER TO suite;

--
-- TOC entry 417 (class 1259 OID 59843)
-- Name: tsisatcotizaciones; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisatcotizaciones (
    cod_cotizacion integer NOT NULL,
    cod_ciudad integer,
    cod_estado integer,
    fec_fecha date NOT NULL,
    "des_nbcontacto " character varying(50) NOT NULL,
    cod_puesto integer,
    des_compania character varying(50) NOT NULL,
    des_nbservicio character varying(50) NOT NULL,
    cnu_cantidad smallint NOT NULL,
    txt_concepto text NOT NULL,
    imp_inversionhr numeric(6,2) NOT NULL,
    txt_condicionescomer text NOT NULL,
    des_nbatentamente character varying(60) NOT NULL,
    des_correoatentamente character varying(50) NOT NULL,
    aud_cod_creadopor integer NOT NULL,
    aud_cod_modificadopor integer,
    aud_fec_creacion date NOT NULL,
    aud_fec_modificacion date
);


ALTER TABLE sisat.tsisatcotizaciones OWNER TO suite;

--
-- TOC entry 418 (class 1259 OID 59849)
-- Name: tsisatcursosycertificados; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisatcursosycertificados (
    cod_curso integer DEFAULT nextval('sisat.seq_cursos'::regclass) NOT NULL,
    cod_prospecto integer NOT NULL,
    des_curso character varying(100) NOT NULL,
    des_institucion character varying(70) NOT NULL,
    fec_termino date NOT NULL,
    fec_inicio date NOT NULL
);


ALTER TABLE sisat.tsisatcursosycertificados OWNER TO suite;

--
-- TOC entry 419 (class 1259 OID 59853)
-- Name: tsisatentrevistas; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisatentrevistas (
    cod_entrevista integer DEFAULT nextval('sisat.seq_entrevistas'::regclass) NOT NULL,
    des_nbentrevistador character varying(90) NOT NULL,
    des_puesto character varying(50) NOT NULL,
    des_correoent character varying(40) NOT NULL,
    cod_telefonoent character varying(16) NOT NULL,
    des_direccionent character varying(200),
    tim_horarioent time without time zone,
    fec_fechaent date NOT NULL,
    comentarios character varying(700) NOT NULL,
    cod_prospecto integer NOT NULL,
    aud_cod_creadopor integer NOT NULL,
    aud_cod_modificadopor integer,
    aud_fec_creacion date NOT NULL,
    aud_fec_modificacion date
);


ALTER TABLE sisat.tsisatentrevistas OWNER TO suite;

--
-- TOC entry 420 (class 1259 OID 59860)
-- Name: tsisatenviocorreos; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisatenviocorreos (
    cod_envio integer DEFAULT nextval('sisat.seq_envios'::regclass) NOT NULL,
    des_destinatario character varying(90) NOT NULL,
    des_asunto character varying(50) NOT NULL,
    des_mensaje character varying(200) NOT NULL,
    bin_adjunto bytea,
    aud_cod_creadopor integer NOT NULL,
    aud_cod_modificadopor integer,
    aud_fec_creacion date NOT NULL,
    aud_fec_modificacion date
);


ALTER TABLE sisat.tsisatenviocorreos OWNER TO suite;

--
-- TOC entry 421 (class 1259 OID 59867)
-- Name: tsisatescolaridad; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisatescolaridad (
    cod_escolaridad integer DEFAULT nextval('sisat.seq_escolaridad'::regclass) NOT NULL,
    cod_prospecto integer NOT NULL,
    des_escolaridad character varying(45) NOT NULL,
    des_escuela character varying(70) NOT NULL,
    fec_inicio date NOT NULL,
    fec_termino date NOT NULL,
    cod_estatus character varying(20)
);


ALTER TABLE sisat.tsisatescolaridad OWNER TO suite;

--
-- TOC entry 422 (class 1259 OID 59871)
-- Name: tsisatexperienciaslaborales; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisatexperienciaslaborales (
    cod_experiencia integer DEFAULT nextval('sisat.seq_experiencias'::regclass) NOT NULL,
    cod_prospecto integer NOT NULL,
    des_empresa character varying(50) NOT NULL,
    des_puesto character varying(40) NOT NULL,
    fec_inicio date,
    fec_termino date,
    des_ubicacion character varying(70),
    txt_funciones text,
    des_nbcliente character varying(70),
    des_proyecto character varying(70),
    txt_logros character varying(300),
    txt_tecnologiasemple text NOT NULL
);


ALTER TABLE sisat.tsisatexperienciaslaborales OWNER TO suite;

--
-- TOC entry 423 (class 1259 OID 59878)
-- Name: tsisatfirmareqper; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisatfirmareqper (
    cod_firma integer DEFAULT nextval('sisat.seq_firmas'::regclass) NOT NULL,
    cod_solicita integer,
    cod_puestosolicita integer,
    cod_autoriza integer,
    cod_puestoautoriza integer,
    cod_vacante integer
);


ALTER TABLE sisat.tsisatfirmareqper OWNER TO suite;

--
-- TOC entry 424 (class 1259 OID 59882)
-- Name: tsisatframeworks; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisatframeworks (
    cod_framework integer NOT NULL,
    des_framework character varying(30) NOT NULL
);


ALTER TABLE sisat.tsisatframeworks OWNER TO suite;

--
-- TOC entry 425 (class 1259 OID 59885)
-- Name: tsisathabilidades; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisathabilidades (
    cod_habilidad integer DEFAULT nextval('sisat.seq_habilidades'::regclass) NOT NULL,
    cod_prospecto integer NOT NULL,
    des_habilidad character varying(50),
    des_dominio character varying(50)
);


ALTER TABLE sisat.tsisathabilidades OWNER TO suite;

--
-- TOC entry 426 (class 1259 OID 59889)
-- Name: tsisatherramientas; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisatherramientas (
    cod_herramientas integer NOT NULL,
    json_tecnologia json,
    cod_prospecto integer NOT NULL,
    des_nivel character varying(30) NOT NULL,
    cnu_experiencia numeric(5,1) NOT NULL
);


ALTER TABLE sisat.tsisatherramientas OWNER TO suite;

--
-- TOC entry 427 (class 1259 OID 59895)
-- Name: tsisatherramientas_cod_herramientas_seq; Type: SEQUENCE; Schema: sisat; Owner: suite
--

CREATE SEQUENCE sisat.tsisatherramientas_cod_herramientas_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sisat.tsisatherramientas_cod_herramientas_seq OWNER TO suite;

--
-- TOC entry 3927 (class 0 OID 0)
-- Dependencies: 427
-- Name: tsisatherramientas_cod_herramientas_seq; Type: SEQUENCE OWNED BY; Schema: sisat; Owner: suite
--

ALTER SEQUENCE sisat.tsisatherramientas_cod_herramientas_seq OWNED BY sisat.tsisatherramientas.cod_herramientas;


--
-- TOC entry 428 (class 1259 OID 59897)
-- Name: tsisatides; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisatides (
    cod_ide integer NOT NULL,
    des_ide character varying(30) NOT NULL
);


ALTER TABLE sisat.tsisatides OWNER TO suite;

--
-- TOC entry 429 (class 1259 OID 59900)
-- Name: tsisatidiomas; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisatidiomas (
    cod_idioma integer DEFAULT nextval('sisat.seq_idiomas'::regclass) NOT NULL,
    cod_nbidioma character varying(20) NOT NULL
);


ALTER TABLE sisat.tsisatidiomas OWNER TO suite;

--
-- TOC entry 430 (class 1259 OID 59904)
-- Name: tsisatlenguajes; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisatlenguajes (
    cod_lenguaje integer NOT NULL,
    des_lenguaje character varying(30) NOT NULL
);


ALTER TABLE sisat.tsisatlenguajes OWNER TO suite;

--
-- TOC entry 431 (class 1259 OID 59907)
-- Name: tsisatmaquetados; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisatmaquetados (
    cod_maquetado integer NOT NULL,
    des_maquetado character varying(50) NOT NULL
);


ALTER TABLE sisat.tsisatmaquetados OWNER TO suite;

--
-- TOC entry 432 (class 1259 OID 59910)
-- Name: tsisatmetodologias; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisatmetodologias (
    cod_metodologia integer NOT NULL,
    des_metodologia character varying(30) NOT NULL
);


ALTER TABLE sisat.tsisatmetodologias OWNER TO suite;

--
-- TOC entry 433 (class 1259 OID 59913)
-- Name: tsisatmodelados; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisatmodelados (
    cod_modelado integer NOT NULL,
    des_modelado character varying(30) NOT NULL
);


ALTER TABLE sisat.tsisatmodelados OWNER TO suite;

--
-- TOC entry 434 (class 1259 OID 59916)
-- Name: tsisatordenservicio; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisatordenservicio (
    cod_ordenservicio integer DEFAULT nextval('sisat.seq_ordenservicios'::regclass) NOT NULL,
    cod_estadorep integer NOT NULL,
    cod_ciudad integer NOT NULL,
    fec_fecha date NOT NULL,
    cod_puestocte character varying,
    des_nbservicio character varying(60),
    cnu_cantidad smallint,
    txt_concepto text,
    imp_inversionhr numeric(6,2),
    txt_condicionescomer text,
    des_ubcnconsultor character varying(100),
    fec_finservicio date,
    cod_gpy integer NOT NULL,
    cod_cliente integer NOT NULL,
    aud_cod_creadopor integer NOT NULL,
    aud_cod_modificadopor integer,
    aud_fec_creacion date NOT NULL,
    aud_fec_modificacion date
);


ALTER TABLE sisat.tsisatordenservicio OWNER TO suite;

--
-- TOC entry 435 (class 1259 OID 59923)
-- Name: tsisatpatrones; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisatpatrones (
    cod_patron integer NOT NULL,
    des_patron character varying(30) NOT NULL
);


ALTER TABLE sisat.tsisatpatrones OWNER TO suite;

--
-- TOC entry 436 (class 1259 OID 59926)
-- Name: tsisatprospectos; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisatprospectos (
    cod_prospecto integer DEFAULT nextval('sisat.seq_prospectos'::regclass) NOT NULL,
    des_nombre character varying(45) NOT NULL,
    des_nombres character varying(60),
    des_appaterno character varying(40) NOT NULL,
    des_apmaterno character varying(40),
    des_lugarnacimiento character varying(50) NOT NULL,
    fec_nacimiento date NOT NULL,
    cod_edad integer NOT NULL,
    cod_edocivil character varying(15) NOT NULL,
    des_nbpadre character varying(70),
    des_nbmadre character varying(70),
    cod_numhermanos integer,
    des_nbcalle character varying(60),
    cod_numcasa integer,
    des_colonia character varying(60),
    des_localidad character varying(60),
    des_municipio character varying(60),
    des_estado character varying(60),
    cod_cpostal integer,
    cod_tiposangre character varying(5),
    des_emailmbn character varying(40),
    des_emailpersonal character varying(40),
    des_pasatiempo character varying(200),
    cod_telefonocasa character varying(16) DEFAULT NULL::character varying,
    cod_telefonomovil character varying(16) DEFAULT NULL::character varying,
    cod_rfc character varying(13),
    cod_nss character varying(20),
    cod_curp character varying(18) NOT NULL,
    des_nacionalidad character varying(30),
    cod_administrador integer,
    fec_fechacoment date,
    txt_comentarios text,
    bin_foto bytea,
    cod_tipofoto character varying(30),
    cod_extensionfoto character varying(5),
    des_puestovacante character varying(50),
    anio_experiencia integer,
    aud_cod_creadopor integer NOT NULL,
    aud_cod_modificadopor integer,
    aud_fec_creacion date NOT NULL,
    aud_fec_modifiacion date
);


ALTER TABLE sisat.tsisatprospectos OWNER TO suite;

--
-- TOC entry 437 (class 1259 OID 59935)
-- Name: tsisatprospectos_idiomas; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisatprospectos_idiomas (
    cod_pros_idoma integer NOT NULL,
    cod_prospecto integer,
    cod_idioma integer,
    cod_nivel character varying(20),
    des_certificado character varying(40)
);


ALTER TABLE sisat.tsisatprospectos_idiomas OWNER TO suite;

--
-- TOC entry 438 (class 1259 OID 59938)
-- Name: tsisatprotocolos; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisatprotocolos (
    cod_protocolo integer NOT NULL,
    des_protocolo character varying(30) NOT NULL
);


ALTER TABLE sisat.tsisatprotocolos OWNER TO suite;

--
-- TOC entry 439 (class 1259 OID 59941)
-- Name: tsisatproyectos; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisatproyectos (
    cod_proyecto integer DEFAULT nextval('sisat.seq_proyectos'::regclass) NOT NULL,
    cod_prospecto integer NOT NULL,
    cod_perfil integer NOT NULL,
    des_nbcliente character varying(50) NOT NULL,
    des_nbresponsable character varying(50) NOT NULL,
    des_correo character varying(50) NOT NULL,
    cod_telefono character varying(16) NOT NULL,
    des_direccion character varying(200) NOT NULL,
    aud_cod_creadopor integer NOT NULL,
    aud_cod_modificadopor integer,
    aud_fec_creacion date NOT NULL,
    aud_fec_modificacion date
);


ALTER TABLE sisat.tsisatproyectos OWNER TO suite;

--
-- TOC entry 440 (class 1259 OID 59945)
-- Name: tsisatqa; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisatqa (
    cod_qa integer NOT NULL,
    des_qa character varying(30) NOT NULL
);


ALTER TABLE sisat.tsisatqa OWNER TO suite;

--
-- TOC entry 441 (class 1259 OID 59948)
-- Name: tsisatrepositoriolibrerias; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisatrepositoriolibrerias (
    cod_repositoriolibreria integer NOT NULL,
    des_repositoriolibreria character varying(30) NOT NULL
);


ALTER TABLE sisat.tsisatrepositoriolibrerias OWNER TO suite;

--
-- TOC entry 442 (class 1259 OID 59951)
-- Name: tsisatrepositorios; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisatrepositorios (
    cod_repositorio integer NOT NULL,
    des_repositorio character varying(30) NOT NULL
);


ALTER TABLE sisat.tsisatrepositorios OWNER TO suite;

--
-- TOC entry 443 (class 1259 OID 59954)
-- Name: tsisatsgbd; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisatsgbd (
    cod_sgbd integer NOT NULL,
    des_sgbd character varying(30) NOT NULL
);


ALTER TABLE sisat.tsisatsgbd OWNER TO suite;

--
-- TOC entry 444 (class 1259 OID 59957)
-- Name: tsisatso; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisatso (
    cod_so integer NOT NULL,
    des_so character varying(30) NOT NULL
);


ALTER TABLE sisat.tsisatso OWNER TO suite;

--
-- TOC entry 445 (class 1259 OID 59960)
-- Name: tsisatvacantes; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisatvacantes (
    cod_vacante integer DEFAULT nextval('sisat.seq_vacantes'::regclass) NOT NULL,
    des_rqvacante character varying(200) NOT NULL,
    cnu_anexperiencia smallint,
    txt_experiencia text,
    des_escolaridad character varying(50),
    txt_herramientas text,
    txt_habilidades text,
    des_lugartrabajo character varying(100),
    imp_sueldo numeric(6,2),
    cod_idioma integer,
    sexo character varying(11) NOT NULL,
    fec_solicitud date NOT NULL,
    fec_entrega date,
    cod_cliente integer NOT NULL,
    aud_cod_creadopor integer NOT NULL,
    aud_cod_modificadopor integer,
    aud_fec_creacion date NOT NULL,
    aud_fec_modificacion date,
    cod_contratacion integer NOT NULL,
    statusvacante boolean,
    txt_conocimientostecno text,
    cod_nivelidioma character varying(20)
);


ALTER TABLE sisat.tsisatvacantes OWNER TO suite;

--
-- TOC entry 3519 (class 2604 OID 59967)
-- Name: tsisatherramientas cod_herramientas; Type: DEFAULT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatherramientas ALTER COLUMN cod_herramientas SET DEFAULT nextval('sisat.tsisatherramientas_cod_herramientas_seq'::regclass);


--
-- TOC entry 3836 (class 0 OID 59796)
-- Dependencies: 410
-- Data for Name: tsisatappservices; Type: TABLE DATA; Schema: sisat; Owner: suite
--



--
-- TOC entry 3837 (class 0 OID 59799)
-- Dependencies: 411
-- Data for Name: tsisatarquitecturas; Type: TABLE DATA; Schema: sisat; Owner: suite
--



--
-- TOC entry 3838 (class 0 OID 59802)
-- Dependencies: 412
-- Data for Name: tsisatasignaciones; Type: TABLE DATA; Schema: sisat; Owner: suite
--



--
-- TOC entry 3839 (class 0 OID 59806)
-- Dependencies: 413
-- Data for Name: tsisatcandidatos; Type: TABLE DATA; Schema: sisat; Owner: suite
--



--
-- TOC entry 3840 (class 0 OID 59810)
-- Dependencies: 414
-- Data for Name: tsisatcartaaceptacion; Type: TABLE DATA; Schema: sisat; Owner: suite
--



--
-- TOC entry 3841 (class 0 OID 59817)
-- Dependencies: 415
-- Data for Name: tsisatcartaasignacion; Type: TABLE DATA; Schema: sisat; Owner: suite
--

INSERT INTO sisat.tsisatcartaasignacion (cod_asignacion, cod_prospecto, cod_perfil, des_actividades, des_lugarsalida, des_lugarllegada, fec_salida, fec_llegada, cod_transporte, des_lugarhopedaje, fec_hospedaje, des_computadora, cod_telefono, des_accesorios, des_nbresponsable, des_nbpuesto, des_lugarresp, cod_telefonoresp, tim_horario, fec_iniciocontra, fec_terminocontra, imp_sueldomensual, imp_nominaimss, imp_honorarios, imp_otros, cod_rfc, des_razonsocial, des_correo, cod_cpostal, des_direccionfact, cod_cliente, cod_gpy, cod_rhta, cod_ape, cod_rys, des_observacion, aud_cod_creadopor, aud_cod_modificadopor, aud_fecha_creacion, aud_fecha_modificacion) VALUES (2, 1, 2, 'DESARROLLO WEB', 'TLAXCALA', 'CD. DE MEXICO', '2019-06-07', '2019-06-07', 'PUBLICO', 'RENTA DE CASA', '2019-06-07', 'CLIENTE', 'PROPIO', 'PROPIO', 'RAMIRO MONTEZ', 'RESPONSABLE DE DESARROLLO', 'CD. DE MEXICO', '6789327689', '08:00:00', '2019-06-07', '2019-06-07', 1200.00, 1000.00, 500.00, 0.00, 'FNCKCCKFHVNC', 'PATITOS', 'PATITOSLOVER@GMAIL.COM', 67890, 'CALLE NICARAGUA', 3, 16, 10, 14, 15, 'SIN ', 10, 10, '2019-06-07', '2019-06-07');
INSERT INTO sisat.tsisatcartaasignacion (cod_asignacion, cod_prospecto, cod_perfil, des_actividades, des_lugarsalida, des_lugarllegada, fec_salida, fec_llegada, cod_transporte, des_lugarhopedaje, fec_hospedaje, des_computadora, cod_telefono, des_accesorios, des_nbresponsable, des_nbpuesto, des_lugarresp, cod_telefonoresp, tim_horario, fec_iniciocontra, fec_terminocontra, imp_sueldomensual, imp_nominaimss, imp_honorarios, imp_otros, cod_rfc, des_razonsocial, des_correo, cod_cpostal, des_direccionfact, cod_cliente, cod_gpy, cod_rhta, cod_ape, cod_rys, des_observacion, aud_cod_creadopor, aud_cod_modificadopor, aud_fecha_creacion, aud_fecha_modificacion) VALUES (4, 2, 3, 'PRUEBAS DE JAVA', 'APIZACO', 'PUEBLA', '2019-11-17', '2019-11-17', 'COLECTIVO', 'HOTEL', '2019-11-17', 'MBN', 'EMPRESA', 'PROPIO', 'PABLO DEL MONTE', 'GERENTE', 'CALLE MONTE', '(778)888-88-88', '08:00:00', '2019-01-18', '2019-01-27', 1600.00, 500.00, 400.00, 0.00, 'YTFG789NOY65F', 'PABLOMIX', 'JUANPABLITO@GMAIL.COM', 76868, 'CALLE BUENA VISTA', 4, 13, 10, 14, 12, NULL, 10, NULL, '2019-11-15', NULL);
INSERT INTO sisat.tsisatcartaasignacion (cod_asignacion, cod_prospecto, cod_perfil, des_actividades, des_lugarsalida, des_lugarllegada, fec_salida, fec_llegada, cod_transporte, des_lugarhopedaje, fec_hospedaje, des_computadora, cod_telefono, des_accesorios, des_nbresponsable, des_nbpuesto, des_lugarresp, cod_telefonoresp, tim_horario, fec_iniciocontra, fec_terminocontra, imp_sueldomensual, imp_nominaimss, imp_honorarios, imp_otros, cod_rfc, des_razonsocial, des_correo, cod_cpostal, des_direccionfact, cod_cliente, cod_gpy, cod_rhta, cod_ape, cod_rys, des_observacion, aud_cod_creadopor, aud_cod_modificadopor, aud_fecha_creacion, aud_fecha_modificacion) VALUES (5, 2, 3, 'PRUEBAS DE JAVA', 'APIZACO', 'PUEBLA', '2019-11-17', '2019-11-17', 'COLECTIVO', 'HOTEL', '2019-11-17', 'MBN', 'EMPRESA', 'PROPIO', 'PABLO DEL MONTE', 'GERENTE', 'CALLE MONTE', '(778)888-88-88', '08:00:00', '2019-01-18', '2019-12-27', 1600.00, 500.00, 400.00, 0.00, 'YTFG789NOY65F', 'PABLOMIX', 'JUANPABLITO@GMAIL.COM', 76868, 'CALLE BUENA VISTA', 4, 13, 10, 14, 12, NULL, 10, NULL, '2019-11-15', NULL);
INSERT INTO sisat.tsisatcartaasignacion (cod_asignacion, cod_prospecto, cod_perfil, des_actividades, des_lugarsalida, des_lugarllegada, fec_salida, fec_llegada, cod_transporte, des_lugarhopedaje, fec_hospedaje, des_computadora, cod_telefono, des_accesorios, des_nbresponsable, des_nbpuesto, des_lugarresp, cod_telefonoresp, tim_horario, fec_iniciocontra, fec_terminocontra, imp_sueldomensual, imp_nominaimss, imp_honorarios, imp_otros, cod_rfc, des_razonsocial, des_correo, cod_cpostal, des_direccionfact, cod_cliente, cod_gpy, cod_rhta, cod_ape, cod_rys, des_observacion, aud_cod_creadopor, aud_cod_modificadopor, aud_fecha_creacion, aud_fecha_modificacion) VALUES (1, 1, 1, 'DESARROLLAR', 'APIZACO', 'TLAXCALA', '2019-02-23', '2019-02-23', 'COLECTIVO', 'HOTEL', '2019-02-23', 'MBN', 'MBN', 'MBN', 'PEDRO PEREZ', 'RESPONSABLE', 'TLAXCALA', '4343535598', '08:00:00', '2019-02-23', '2019-12-07', 1600.00, 1000.00, 900.00, 100.00, 'BFCOBJVJKVBK', 'PEDRITOSPEDRAZA', 'JDSKHVCKS@GMAIL.COM', 56980, 'AVENIDA FLORES VAGON', 1, 16, 10, 14, 15, 'SIN OBSERVACION', 10, 10, '2019-02-23', '2019-02-23');
INSERT INTO sisat.tsisatcartaasignacion (cod_asignacion, cod_prospecto, cod_perfil, des_actividades, des_lugarsalida, des_lugarllegada, fec_salida, fec_llegada, cod_transporte, des_lugarhopedaje, fec_hospedaje, des_computadora, cod_telefono, des_accesorios, des_nbresponsable, des_nbpuesto, des_lugarresp, cod_telefonoresp, tim_horario, fec_iniciocontra, fec_terminocontra, imp_sueldomensual, imp_nominaimss, imp_honorarios, imp_otros, cod_rfc, des_razonsocial, des_correo, cod_cpostal, des_direccionfact, cod_cliente, cod_gpy, cod_rhta, cod_ape, cod_rys, des_observacion, aud_cod_creadopor, aud_cod_modificadopor, aud_fecha_creacion, aud_fecha_modificacion) VALUES (3, 2, 3, 'PRUEBAS ', 'APIZACO ', 'PUEBLA', '2019-11-15', '2019-11-29', 'COLECTIVO', 'HOTEL', '2019-11-15', 'MBN', 'EMPRESA', 'PROPIO', 'ANA KAREN PICHÓN SÁNCHEZ', 'LIDER', 'PUEBLA', '(246)989-89-98', '08:00:00', '2019-01-18', '2018-12-24', 16.00, 500.00, 400.00, 0.00, 'HGHXBDGDBY', 'ANNIKA', 'ANNIKA@GMAIL.COM', 90480, 'CALLE ANAKAREN', 2, 19, 10, 14, 15, NULL, 10, NULL, '2019-11-15', NULL);


--
-- TOC entry 3875 (class 0 OID 60559)
-- Dependencies: 449
-- Data for Name: tsisatcomentcartaasignacion; Type: TABLE DATA; Schema: sisat; Owner: suite
--

INSERT INTO sisat.tsisatcomentcartaasignacion (cod_comentcartaasignacion, des_comentcartaasignacion, bol_validacion, aud_fecha_creacion, aud_fecha_modificacion, aud_cod_creadopor, aud_cod_modificadopor, cod_asignacion) VALUES (4, 'EJEMPLO', true, '2019-11-28', NULL, 10, NULL, 1);


--
-- TOC entry 3874 (class 0 OID 60538)
-- Dependencies: 448
-- Data for Name: tsisatcomentcosteo; Type: TABLE DATA; Schema: sisat; Owner: suite
--



--
-- TOC entry 3873 (class 0 OID 60508)
-- Dependencies: 447
-- Data for Name: tsisatcomententrevista; Type: TABLE DATA; Schema: sisat; Owner: suite
--



--
-- TOC entry 3872 (class 0 OID 60487)
-- Dependencies: 446
-- Data for Name: tsisatcomentvacantes; Type: TABLE DATA; Schema: sisat; Owner: suite
--

INSERT INTO sisat.tsisatcomentvacantes (cod_comentvacante, des_comentvacante, bol_validacion, aud_fecha_creacion, aud_fecha_modificacion, aud_cod_creadopor, aud_cod_modificadopor, cod_vacante) VALUES (3, 'RECHAZADO', false, '2019-11-20', NULL, 10, NULL, 1);


--
-- TOC entry 3842 (class 0 OID 59839)
-- Dependencies: 416
-- Data for Name: tsisatcontrataciones; Type: TABLE DATA; Schema: sisat; Owner: suite
--

INSERT INTO sisat.tsisatcontrataciones (cod_contratacion, fec_inicio, fec_termino, des_esquema, cod_salarioestmin, cod_salarioestmax, tim_jornada, aud_cod_creadopor, aud_cod_modificadopor, aud_fec_creacion, aud_fec_modificacion) VALUES (1, '2019-11-14', '2020-02-28', '100% NÓMINA IMSS', 1000.00, 2500.00, '10 a 5 pm', 10, 10, '2019-11-14', '2019-11-14');
INSERT INTO sisat.tsisatcontrataciones (cod_contratacion, fec_inicio, fec_termino, des_esquema, cod_salarioestmin, cod_salarioestmax, tim_jornada, aud_cod_creadopor, aud_cod_modificadopor, aud_fec_creacion, aud_fec_modificacion) VALUES (2, '2019-11-14', '2020-01-16', 'ESQUEMA MIXTO', 1000.00, 2500.00, '10 a 5 pm', 10, 10, '2019-11-14', '2019-11-14');
INSERT INTO sisat.tsisatcontrataciones (cod_contratacion, fec_inicio, fec_termino, des_esquema, cod_salarioestmin, cod_salarioestmax, tim_jornada, aud_cod_creadopor, aud_cod_modificadopor, aud_fec_creacion, aud_fec_modificacion) VALUES (10, '2019-11-18', '2020-02-28', '100% NÓMINA IMSS', 4000.00, 6500.00, '10 a 5 pm', 10, 10, '2019-11-15', '2019-11-15');


--
-- TOC entry 3843 (class 0 OID 59843)
-- Dependencies: 417
-- Data for Name: tsisatcotizaciones; Type: TABLE DATA; Schema: sisat; Owner: suite
--



--
-- TOC entry 3844 (class 0 OID 59849)
-- Dependencies: 418
-- Data for Name: tsisatcursosycertificados; Type: TABLE DATA; Schema: sisat; Owner: suite
--



--
-- TOC entry 3845 (class 0 OID 59853)
-- Dependencies: 419
-- Data for Name: tsisatentrevistas; Type: TABLE DATA; Schema: sisat; Owner: suite
--



--
-- TOC entry 3846 (class 0 OID 59860)
-- Dependencies: 420
-- Data for Name: tsisatenviocorreos; Type: TABLE DATA; Schema: sisat; Owner: suite
--



--
-- TOC entry 3847 (class 0 OID 59867)
-- Dependencies: 421
-- Data for Name: tsisatescolaridad; Type: TABLE DATA; Schema: sisat; Owner: suite
--



--
-- TOC entry 3848 (class 0 OID 59871)
-- Dependencies: 422
-- Data for Name: tsisatexperienciaslaborales; Type: TABLE DATA; Schema: sisat; Owner: suite
--



--
-- TOC entry 3849 (class 0 OID 59878)
-- Dependencies: 423
-- Data for Name: tsisatfirmareqper; Type: TABLE DATA; Schema: sisat; Owner: suite
--

INSERT INTO sisat.tsisatfirmareqper (cod_firma, cod_solicita, cod_puestosolicita, cod_autoriza, cod_puestoautoriza, cod_vacante) VALUES (1, 17, 11, 24, 1, 1);
INSERT INTO sisat.tsisatfirmareqper (cod_firma, cod_solicita, cod_puestosolicita, cod_autoriza, cod_puestoautoriza, cod_vacante) VALUES (2, 17, 11, 34, 1, 2);
INSERT INTO sisat.tsisatfirmareqper (cod_firma, cod_solicita, cod_puestosolicita, cod_autoriza, cod_puestoautoriza, cod_vacante) VALUES (3, 17, 11, 10, 2, 3);


--
-- TOC entry 3850 (class 0 OID 59882)
-- Dependencies: 424
-- Data for Name: tsisatframeworks; Type: TABLE DATA; Schema: sisat; Owner: suite
--



--
-- TOC entry 3851 (class 0 OID 59885)
-- Dependencies: 425
-- Data for Name: tsisathabilidades; Type: TABLE DATA; Schema: sisat; Owner: suite
--



--
-- TOC entry 3852 (class 0 OID 59889)
-- Dependencies: 426
-- Data for Name: tsisatherramientas; Type: TABLE DATA; Schema: sisat; Owner: suite
--



--
-- TOC entry 3854 (class 0 OID 59897)
-- Dependencies: 428
-- Data for Name: tsisatides; Type: TABLE DATA; Schema: sisat; Owner: suite
--



--
-- TOC entry 3855 (class 0 OID 59900)
-- Dependencies: 429
-- Data for Name: tsisatidiomas; Type: TABLE DATA; Schema: sisat; Owner: suite
--

INSERT INTO sisat.tsisatidiomas (cod_idioma, cod_nbidioma) VALUES (1, 'INGLES');
INSERT INTO sisat.tsisatidiomas (cod_idioma, cod_nbidioma) VALUES (2, 'ALEMAN');
INSERT INTO sisat.tsisatidiomas (cod_idioma, cod_nbidioma) VALUES (3, 'PORTUGUES');


--
-- TOC entry 3856 (class 0 OID 59904)
-- Dependencies: 430
-- Data for Name: tsisatlenguajes; Type: TABLE DATA; Schema: sisat; Owner: suite
--



--
-- TOC entry 3857 (class 0 OID 59907)
-- Dependencies: 431
-- Data for Name: tsisatmaquetados; Type: TABLE DATA; Schema: sisat; Owner: suite
--



--
-- TOC entry 3858 (class 0 OID 59910)
-- Dependencies: 432
-- Data for Name: tsisatmetodologias; Type: TABLE DATA; Schema: sisat; Owner: suite
--



--
-- TOC entry 3859 (class 0 OID 59913)
-- Dependencies: 433
-- Data for Name: tsisatmodelados; Type: TABLE DATA; Schema: sisat; Owner: suite
--



--
-- TOC entry 3860 (class 0 OID 59916)
-- Dependencies: 434
-- Data for Name: tsisatordenservicio; Type: TABLE DATA; Schema: sisat; Owner: suite
--

INSERT INTO sisat.tsisatordenservicio (cod_ordenservicio, cod_estadorep, cod_ciudad, fec_fecha, cod_puestocte, des_nbservicio, cnu_cantidad, txt_concepto, imp_inversionhr, txt_condicionescomer, des_ubcnconsultor, fec_finservicio, cod_gpy, cod_cliente, aud_cod_creadopor, aud_cod_modificadopor, aud_fec_creacion, aud_fec_modificacion) VALUES (12, 1, 6, '2019-12-09', 'EJEMPLO', 'EJEMPLO', 20, 'EJEMPLO', 10.00, 'EJEMPLO', 'UBICACIÓN DE EJEMPLO', '2019-12-09', 16, 4, 10, NULL, '2019-12-09', NULL);
INSERT INTO sisat.tsisatordenservicio (cod_ordenservicio, cod_estadorep, cod_ciudad, fec_fecha, cod_puestocte, des_nbservicio, cnu_cantidad, txt_concepto, imp_inversionhr, txt_condicionescomer, des_ubcnconsultor, fec_finservicio, cod_gpy, cod_cliente, aud_cod_creadopor, aud_cod_modificadopor, aud_fec_creacion, aud_fec_modificacion) VALUES (13, 2, 11, '2019-12-09', 'EJEMPLO', 'EJEMPLO', 20, 'EJEMPLO', 10.00, 'EJEMPLO', 'UBICACIÓN DE EJEMPLO', '2019-12-09', 16, 2, 10, NULL, '2019-12-09', NULL);
INSERT INTO sisat.tsisatordenservicio (cod_ordenservicio, cod_estadorep, cod_ciudad, fec_fecha, cod_puestocte, des_nbservicio, cnu_cantidad, txt_concepto, imp_inversionhr, txt_condicionescomer, des_ubcnconsultor, fec_finservicio, cod_gpy, cod_cliente, aud_cod_creadopor, aud_cod_modificadopor, aud_fec_creacion, aud_fec_modificacion) VALUES (47, 1, 6, '2019-12-09', 'EJEMPLO', 'EJEMPLO', 1000, 'EJEMPLO', 20.00, 'EJEMPLO', 'EJEMPLO', '2019-12-09', 16, 2, 10, 10, '2019-12-09', '2019-12-09');
INSERT INTO sisat.tsisatordenservicio (cod_ordenservicio, cod_estadorep, cod_ciudad, fec_fecha, cod_puestocte, des_nbservicio, cnu_cantidad, txt_concepto, imp_inversionhr, txt_condicionescomer, des_ubcnconsultor, fec_finservicio, cod_gpy, cod_cliente, aud_cod_creadopor, aud_cod_modificadopor, aud_fec_creacion, aud_fec_modificacion) VALUES (48, 1, 6, '2019-12-09', 'EJEMPLO', 'EJEMPLO', 1000, 'EJEMPLO', 20.00, 'EJEMPLO', 'EJEMPLO', '2019-12-09', 16, 2, 10, NULL, '2019-12-09', NULL);
INSERT INTO sisat.tsisatordenservicio (cod_ordenservicio, cod_estadorep, cod_ciudad, fec_fecha, cod_puestocte, des_nbservicio, cnu_cantidad, txt_concepto, imp_inversionhr, txt_condicionescomer, des_ubcnconsultor, fec_finservicio, cod_gpy, cod_cliente, aud_cod_creadopor, aud_cod_modificadopor, aud_fec_creacion, aud_fec_modificacion) VALUES (49, 1, 6, '2019-12-09', 'EJEMPLO', 'EJEMPLO', 1000, 'EJEMPLO', 20.00, 'EJEMPLO', 'EJEMPLO', '2019-12-09', 16, 2, 10, NULL, '2019-12-09', NULL);
INSERT INTO sisat.tsisatordenservicio (cod_ordenservicio, cod_estadorep, cod_ciudad, fec_fecha, cod_puestocte, des_nbservicio, cnu_cantidad, txt_concepto, imp_inversionhr, txt_condicionescomer, des_ubcnconsultor, fec_finservicio, cod_gpy, cod_cliente, aud_cod_creadopor, aud_cod_modificadopor, aud_fec_creacion, aud_fec_modificacion) VALUES (50, 2, 10, '2019-12-10', 'EJEMPLO', 'EJEMPLO', 20, 'EJEMPLO', 10.00, 'EJEMPLO', 'UBICACIÓN DE EJEMPLO', '2019-12-10', 16, 1, 10, 10, '2019-12-10', '2019-12-10');
INSERT INTO sisat.tsisatordenservicio (cod_ordenservicio, cod_estadorep, cod_ciudad, fec_fecha, cod_puestocte, des_nbservicio, cnu_cantidad, txt_concepto, imp_inversionhr, txt_condicionescomer, des_ubcnconsultor, fec_finservicio, cod_gpy, cod_cliente, aud_cod_creadopor, aud_cod_modificadopor, aud_fec_creacion, aud_fec_modificacion) VALUES (51, 3, 9, '2019-12-10', 'EJEMPLO', 'EJEMPLO', 20, 'EJEMPLO', 10.00, 'EJEMPLO', 'UBICACIÓN DE EJEMPLO', '2019-12-10', 16, 2, 10, 10, '2019-12-10', '2019-12-10');
INSERT INTO sisat.tsisatordenservicio (cod_ordenservicio, cod_estadorep, cod_ciudad, fec_fecha, cod_puestocte, des_nbservicio, cnu_cantidad, txt_concepto, imp_inversionhr, txt_condicionescomer, des_ubcnconsultor, fec_finservicio, cod_gpy, cod_cliente, aud_cod_creadopor, aud_cod_modificadopor, aud_fec_creacion, aud_fec_modificacion) VALUES (52, 1, 6, '2019-12-10', 'EJEMPLO', 'EJEMPLO', 20, 'EJEMPLO', 10.00, 'EJEMPLO', 'UBICACIÓN DE EJEMPLO', '2019-12-10', 16, 1, 10, 10, '2019-12-10', '2019-12-10');
INSERT INTO sisat.tsisatordenservicio (cod_ordenservicio, cod_estadorep, cod_ciudad, fec_fecha, cod_puestocte, des_nbservicio, cnu_cantidad, txt_concepto, imp_inversionhr, txt_condicionescomer, des_ubcnconsultor, fec_finservicio, cod_gpy, cod_cliente, aud_cod_creadopor, aud_cod_modificadopor, aud_fec_creacion, aud_fec_modificacion) VALUES (53, 1, 6, '2019-12-10', 'EJEMPLO', 'EJEMPLO', 20, 'EJEMPLO', 10.00, 'EJEMPLO', 'UBICACIÓN DE EJEMPLO', '2019-12-10', 16, 1, 10, 10, '2019-12-10', '2019-12-10');
INSERT INTO sisat.tsisatordenservicio (cod_ordenservicio, cod_estadorep, cod_ciudad, fec_fecha, cod_puestocte, des_nbservicio, cnu_cantidad, txt_concepto, imp_inversionhr, txt_condicionescomer, des_ubcnconsultor, fec_finservicio, cod_gpy, cod_cliente, aud_cod_creadopor, aud_cod_modificadopor, aud_fec_creacion, aud_fec_modificacion) VALUES (54, 1, 6, '2019-12-10', 'EJEMPLO', 'EJEMPLO', 20, 'EJEMPLO', 10.00, 'EJEMPLO', 'UBICACIÓN DE EJEMPLO', '2019-12-10', 16, 1, 10, 10, '2019-12-10', '2019-12-10');


--
-- TOC entry 3861 (class 0 OID 59923)
-- Dependencies: 435
-- Data for Name: tsisatpatrones; Type: TABLE DATA; Schema: sisat; Owner: suite
--



--
-- TOC entry 3862 (class 0 OID 59926)
-- Dependencies: 436
-- Data for Name: tsisatprospectos; Type: TABLE DATA; Schema: sisat; Owner: suite
--

INSERT INTO sisat.tsisatprospectos (cod_prospecto, des_nombre, des_nombres, des_appaterno, des_apmaterno, des_lugarnacimiento, fec_nacimiento, cod_edad, cod_edocivil, des_nbpadre, des_nbmadre, cod_numhermanos, des_nbcalle, cod_numcasa, des_colonia, des_localidad, des_municipio, des_estado, cod_cpostal, cod_tiposangre, des_emailmbn, des_emailpersonal, des_pasatiempo, cod_telefonocasa, cod_telefonomovil, cod_rfc, cod_nss, cod_curp, des_nacionalidad, cod_administrador, fec_fechacoment, txt_comentarios, bin_foto, cod_tipofoto, cod_extensionfoto, des_puestovacante, anio_experiencia, aud_cod_creadopor, aud_cod_modificadopor, aud_fec_creacion, aud_fec_modifiacion) VALUES (2, 'FERNANDO', ' ', 'MORALES', 'MOZO', 'TLAXCALA', '1996-05-30', 23, 'SOLTERO', 'PEDRITO', 'JUANITA', 2, 'FRANCISCO I MADERO', 50, 'VENUSTIANO', 'XALOZTOC', 'XALOZTOC', 'TLAXCALA', 23578, 'O+', 'PABLITO.MBN@GMAIL.COM', 'JUANPABLITO@GMAIL.COM', 'GAMER', '54543', '4353678903', 'BBDFKGKDFJBVK', '5464647', 'LJFEVBKJBJF', 'MEXICANO', 1, '2019-02-23', 'USUSKIHDKBL', NULL, 'JPG', 'JPG', 'DESARROLLADOR', 1, 10, 10, '2019-02-23', '2019-02-23');
INSERT INTO sisat.tsisatprospectos (cod_prospecto, des_nombre, des_nombres, des_appaterno, des_apmaterno, des_lugarnacimiento, fec_nacimiento, cod_edad, cod_edocivil, des_nbpadre, des_nbmadre, cod_numhermanos, des_nbcalle, cod_numcasa, des_colonia, des_localidad, des_municipio, des_estado, cod_cpostal, cod_tiposangre, des_emailmbn, des_emailpersonal, des_pasatiempo, cod_telefonocasa, cod_telefonomovil, cod_rfc, cod_nss, cod_curp, des_nacionalidad, cod_administrador, fec_fechacoment, txt_comentarios, bin_foto, cod_tipofoto, cod_extensionfoto, des_puestovacante, anio_experiencia, aud_cod_creadopor, aud_cod_modificadopor, aud_fec_creacion, aud_fec_modifiacion) VALUES (1, 'JUAN', 'PABLO', 'VENEGAS', 'SANCHEZ', 'TLAXCALA', '1996-05-09', 23, 'SOLTERO', 'PEDRITO', 'JUANITA', 2, 'FRANCISCO I MADERO', 50, 'VENUSTIANO', 'XALOZTOC', 'XALOZTOC', 'TLAXCALA', 23578, 'O+', 'PABLITO.MBN@GMAIL.COM', 'FERNANDO.MORALES.MOZO@GMAIL.COM', 'GAMER', '54543', '4353678903', 'BBDFKGKDFJBVK', '5464647', 'LJFEVBKJBJF', 'MEXICANO', 1, '2019-02-23', 'USUSKIHDKBL', NULL, 'JPG', 'JPG', 'DESARROLLADOR', 1, 10, 10, '2019-02-23', '2019-02-23');


--
-- TOC entry 3863 (class 0 OID 59935)
-- Dependencies: 437
-- Data for Name: tsisatprospectos_idiomas; Type: TABLE DATA; Schema: sisat; Owner: suite
--



--
-- TOC entry 3864 (class 0 OID 59938)
-- Dependencies: 438
-- Data for Name: tsisatprotocolos; Type: TABLE DATA; Schema: sisat; Owner: suite
--



--
-- TOC entry 3865 (class 0 OID 59941)
-- Dependencies: 439
-- Data for Name: tsisatproyectos; Type: TABLE DATA; Schema: sisat; Owner: suite
--



--
-- TOC entry 3866 (class 0 OID 59945)
-- Dependencies: 440
-- Data for Name: tsisatqa; Type: TABLE DATA; Schema: sisat; Owner: suite
--



--
-- TOC entry 3867 (class 0 OID 59948)
-- Dependencies: 441
-- Data for Name: tsisatrepositoriolibrerias; Type: TABLE DATA; Schema: sisat; Owner: suite
--



--
-- TOC entry 3868 (class 0 OID 59951)
-- Dependencies: 442
-- Data for Name: tsisatrepositorios; Type: TABLE DATA; Schema: sisat; Owner: suite
--



--
-- TOC entry 3869 (class 0 OID 59954)
-- Dependencies: 443
-- Data for Name: tsisatsgbd; Type: TABLE DATA; Schema: sisat; Owner: suite
--



--
-- TOC entry 3870 (class 0 OID 59957)
-- Dependencies: 444
-- Data for Name: tsisatso; Type: TABLE DATA; Schema: sisat; Owner: suite
--



--
-- TOC entry 3871 (class 0 OID 59960)
-- Dependencies: 445
-- Data for Name: tsisatvacantes; Type: TABLE DATA; Schema: sisat; Owner: suite
--

INSERT INTO sisat.tsisatvacantes (cod_vacante, des_rqvacante, cnu_anexperiencia, txt_experiencia, des_escolaridad, txt_herramientas, txt_habilidades, des_lugartrabajo, imp_sueldo, cod_idioma, sexo, fec_solicitud, fec_entrega, cod_cliente, aud_cod_creadopor, aud_cod_modificadopor, aud_fec_creacion, aud_fec_modificacion, cod_contratacion, statusvacante, txt_conocimientostecno, cod_nivelidioma) VALUES (2, 'DESARROLLADOR DBA', 2, 'TRABAJO EN EQUIPO', 'ING. EN COMPUTACIÓN', NULL, NULL, NULL, NULL, 1, 'INDISTINTO', '2019-11-21', '2019-11-21', 3, 10, NULL, '2019-11-14', NULL, 2, NULL, 'SEGURIDAD Y MANTENIMIENTO DE UNA B.D', 'ALTO');
INSERT INTO sisat.tsisatvacantes (cod_vacante, des_rqvacante, cnu_anexperiencia, txt_experiencia, des_escolaridad, txt_herramientas, txt_habilidades, des_lugartrabajo, imp_sueldo, cod_idioma, sexo, fec_solicitud, fec_entrega, cod_cliente, aud_cod_creadopor, aud_cod_modificadopor, aud_fec_creacion, aud_fec_modificacion, cod_contratacion, statusvacante, txt_conocimientostecno, cod_nivelidioma) VALUES (1, 'DESARROLLADOR JAVA FULL STACK', 2, 'COMUNICACIÓN FLUIDA', 'ING. SITEMAS', NULL, NULL, NULL, NULL, 1, 'INDISTINTO', '2019-11-14', '2019-11-14', 3, 10, NULL, '2019-11-14', NULL, 1, NULL, 'TECNOLOGÍAS HTML5, JAVASCRIPT Y CSS
', 'MEDIO');
INSERT INTO sisat.tsisatvacantes (cod_vacante, des_rqvacante, cnu_anexperiencia, txt_experiencia, des_escolaridad, txt_herramientas, txt_habilidades, des_lugartrabajo, imp_sueldo, cod_idioma, sexo, fec_solicitud, fec_entrega, cod_cliente, aud_cod_creadopor, aud_cod_modificadopor, aud_fec_creacion, aud_fec_modificacion, cod_contratacion, statusvacante, txt_conocimientostecno, cod_nivelidioma) VALUES (3, 'DESARROLLADOR BACK-END', NULL, 'DESARROLLADOR DE APLICACIONES WEB ', 'ING. SITEMAS', NULL, NULL, NULL, NULL, 1, 'INDISTINTO', '2019-11-15', '2019-11-20', 4, 10, NULL, '2019-11-15', NULL, 10, NULL, 'INTERACCIÓN CON B.D, CONOCIMIENTO EN SQL SERVER, POSTGRESQL Y JAVA ', 'MEDIO');


--
-- TOC entry 3947 (class 0 OID 0)
-- Dependencies: 387
-- Name: seq_aceptaciones; Type: SEQUENCE SET; Schema: sisat; Owner: suite
--

SELECT pg_catalog.setval('sisat.seq_aceptaciones', 1, false);


--
-- TOC entry 3948 (class 0 OID 0)
-- Dependencies: 388
-- Name: seq_asignaciones; Type: SEQUENCE SET; Schema: sisat; Owner: suite
--

SELECT pg_catalog.setval('sisat.seq_asignaciones', 1, false);


--
-- TOC entry 3949 (class 0 OID 0)
-- Dependencies: 389
-- Name: seq_candidatos; Type: SEQUENCE SET; Schema: sisat; Owner: suite
--

SELECT pg_catalog.setval('sisat.seq_candidatos', 1, false);


--
-- TOC entry 3950 (class 0 OID 0)
-- Dependencies: 390
-- Name: seq_cartaasignaciones; Type: SEQUENCE SET; Schema: sisat; Owner: suite
--

SELECT pg_catalog.setval('sisat.seq_cartaasignaciones', 11, true);


--
-- TOC entry 3951 (class 0 OID 0)
-- Dependencies: 391
-- Name: seq_comentcartaasignacion; Type: SEQUENCE SET; Schema: sisat; Owner: suite
--

SELECT pg_catalog.setval('sisat.seq_comentcartaasignacion', 4, true);


--
-- TOC entry 3952 (class 0 OID 0)
-- Dependencies: 392
-- Name: seq_comentcosteo; Type: SEQUENCE SET; Schema: sisat; Owner: suite
--

SELECT pg_catalog.setval('sisat.seq_comentcosteo', 1, false);


--
-- TOC entry 3953 (class 0 OID 0)
-- Dependencies: 393
-- Name: seq_comententrevista; Type: SEQUENCE SET; Schema: sisat; Owner: suite
--

SELECT pg_catalog.setval('sisat.seq_comententrevista', 1, false);


--
-- TOC entry 3954 (class 0 OID 0)
-- Dependencies: 394
-- Name: seq_comentvacantes; Type: SEQUENCE SET; Schema: sisat; Owner: suite
--

SELECT pg_catalog.setval('sisat.seq_comentvacantes', 3, true);


--
-- TOC entry 3955 (class 0 OID 0)
-- Dependencies: 395
-- Name: seq_contrataciones; Type: SEQUENCE SET; Schema: sisat; Owner: suite
--

SELECT pg_catalog.setval('sisat.seq_contrataciones', 12, true);


--
-- TOC entry 3956 (class 0 OID 0)
-- Dependencies: 396
-- Name: seq_cotizaciones; Type: SEQUENCE SET; Schema: sisat; Owner: suite
--

SELECT pg_catalog.setval('sisat.seq_cotizaciones', 1, false);


--
-- TOC entry 3957 (class 0 OID 0)
-- Dependencies: 397
-- Name: seq_cursos; Type: SEQUENCE SET; Schema: sisat; Owner: suite
--

SELECT pg_catalog.setval('sisat.seq_cursos', 1, false);


--
-- TOC entry 3958 (class 0 OID 0)
-- Dependencies: 398
-- Name: seq_entrevistas; Type: SEQUENCE SET; Schema: sisat; Owner: suite
--

SELECT pg_catalog.setval('sisat.seq_entrevistas', 1, false);


--
-- TOC entry 3959 (class 0 OID 0)
-- Dependencies: 399
-- Name: seq_envios; Type: SEQUENCE SET; Schema: sisat; Owner: suite
--

SELECT pg_catalog.setval('sisat.seq_envios', 1, false);


--
-- TOC entry 3960 (class 0 OID 0)
-- Dependencies: 400
-- Name: seq_escolaridad; Type: SEQUENCE SET; Schema: sisat; Owner: suite
--

SELECT pg_catalog.setval('sisat.seq_escolaridad', 1, false);


--
-- TOC entry 3961 (class 0 OID 0)
-- Dependencies: 401
-- Name: seq_experiencias; Type: SEQUENCE SET; Schema: sisat; Owner: suite
--

SELECT pg_catalog.setval('sisat.seq_experiencias', 1, false);


--
-- TOC entry 3962 (class 0 OID 0)
-- Dependencies: 402
-- Name: seq_firmareqper; Type: SEQUENCE SET; Schema: sisat; Owner: suite
--

SELECT pg_catalog.setval('sisat.seq_firmareqper', 1, false);


--
-- TOC entry 3963 (class 0 OID 0)
-- Dependencies: 403
-- Name: seq_firmas; Type: SEQUENCE SET; Schema: sisat; Owner: suite
--

SELECT pg_catalog.setval('sisat.seq_firmas', 5, true);


--
-- TOC entry 3964 (class 0 OID 0)
-- Dependencies: 404
-- Name: seq_habilidades; Type: SEQUENCE SET; Schema: sisat; Owner: suite
--

SELECT pg_catalog.setval('sisat.seq_habilidades', 1, false);


--
-- TOC entry 3965 (class 0 OID 0)
-- Dependencies: 405
-- Name: seq_idiomas; Type: SEQUENCE SET; Schema: sisat; Owner: suite
--

SELECT pg_catalog.setval('sisat.seq_idiomas', 1, false);


--
-- TOC entry 3966 (class 0 OID 0)
-- Dependencies: 406
-- Name: seq_ordenservicios; Type: SEQUENCE SET; Schema: sisat; Owner: suite
--

SELECT pg_catalog.setval('sisat.seq_ordenservicios', 54, true);


--
-- TOC entry 3967 (class 0 OID 0)
-- Dependencies: 407
-- Name: seq_prospectos; Type: SEQUENCE SET; Schema: sisat; Owner: suite
--

SELECT pg_catalog.setval('sisat.seq_prospectos', 1, false);


--
-- TOC entry 3968 (class 0 OID 0)
-- Dependencies: 408
-- Name: seq_proyectos; Type: SEQUENCE SET; Schema: sisat; Owner: suite
--

SELECT pg_catalog.setval('sisat.seq_proyectos', 1, false);


--
-- TOC entry 3969 (class 0 OID 0)
-- Dependencies: 409
-- Name: seq_vacantes; Type: SEQUENCE SET; Schema: sisat; Owner: suite
--

SELECT pg_catalog.setval('sisat.seq_vacantes', 5, true);


--
-- TOC entry 3970 (class 0 OID 0)
-- Dependencies: 427
-- Name: tsisatherramientas_cod_herramientas_seq; Type: SEQUENCE SET; Schema: sisat; Owner: suite
--

SELECT pg_catalog.setval('sisat.tsisatherramientas_cod_herramientas_seq', 1, false);


--
-- TOC entry 3584 (class 2606 OID 59969)
-- Name: tsisatprospectos_idiomas pk_cod_pros_idioma; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatprospectos_idiomas
    ADD CONSTRAINT pk_cod_pros_idioma PRIMARY KEY (cod_pros_idoma);


--
-- TOC entry 3532 (class 2606 OID 59971)
-- Name: tsisatappservices tsisatappservices_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatappservices
    ADD CONSTRAINT tsisatappservices_pkey PRIMARY KEY (cod_appservice);


--
-- TOC entry 3534 (class 2606 OID 59973)
-- Name: tsisatarquitecturas tsisatarquitecturas_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatarquitecturas
    ADD CONSTRAINT tsisatarquitecturas_pkey PRIMARY KEY (cod_arquitectura);


--
-- TOC entry 3536 (class 2606 OID 59975)
-- Name: tsisatasignaciones tsisatasignaciones_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatasignaciones
    ADD CONSTRAINT tsisatasignaciones_pkey PRIMARY KEY (cod_asignacion);


--
-- TOC entry 3538 (class 2606 OID 59977)
-- Name: tsisatcandidatos tsisatcandidatos_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcandidatos
    ADD CONSTRAINT tsisatcandidatos_pkey PRIMARY KEY (cod_candidato);


--
-- TOC entry 3540 (class 2606 OID 59979)
-- Name: tsisatcartaaceptacion tsisatcartaaceptacion_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcartaaceptacion
    ADD CONSTRAINT tsisatcartaaceptacion_pkey PRIMARY KEY (cod_aceptacion);


--
-- TOC entry 3542 (class 2606 OID 59981)
-- Name: tsisatcartaasignacion tsisatcartaasignacion_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcartaasignacion
    ADD CONSTRAINT tsisatcartaasignacion_pkey PRIMARY KEY (cod_asignacion);


--
-- TOC entry 3606 (class 2606 OID 60543)
-- Name: tsisatcomentcosteo tsisatcomentarios_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcomentcosteo
    ADD CONSTRAINT tsisatcomentarios_pkey PRIMARY KEY (cod_comentcosteo);


--
-- TOC entry 3608 (class 2606 OID 60564)
-- Name: tsisatcomentcartaasignacion tsisatcomentcartaasignacion_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcomentcartaasignacion
    ADD CONSTRAINT tsisatcomentcartaasignacion_pkey PRIMARY KEY (cod_comentcartaasignacion);


--
-- TOC entry 3604 (class 2606 OID 60516)
-- Name: tsisatcomententrevista tsisatcomententrevista_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcomententrevista
    ADD CONSTRAINT tsisatcomententrevista_pkey PRIMARY KEY (cod_comententrevista);


--
-- TOC entry 3602 (class 2606 OID 60492)
-- Name: tsisatcomentvacantes tsisatcomentvacantes_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcomentvacantes
    ADD CONSTRAINT tsisatcomentvacantes_pkey PRIMARY KEY (cod_comentvacante);


--
-- TOC entry 3544 (class 2606 OID 59991)
-- Name: tsisatcontrataciones tsisatcontrataciones_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcontrataciones
    ADD CONSTRAINT tsisatcontrataciones_pkey PRIMARY KEY (cod_contratacion);


--
-- TOC entry 3546 (class 2606 OID 59993)
-- Name: tsisatcotizaciones tsisatcotizaciones_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcotizaciones
    ADD CONSTRAINT tsisatcotizaciones_pkey PRIMARY KEY (cod_cotizacion);


--
-- TOC entry 3548 (class 2606 OID 59995)
-- Name: tsisatcursosycertificados tsisatcursosycerticados_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcursosycertificados
    ADD CONSTRAINT tsisatcursosycerticados_pkey PRIMARY KEY (cod_curso);


--
-- TOC entry 3550 (class 2606 OID 59997)
-- Name: tsisatentrevistas tsisatentrevistas_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatentrevistas
    ADD CONSTRAINT tsisatentrevistas_pkey PRIMARY KEY (cod_entrevista);


--
-- TOC entry 3552 (class 2606 OID 59999)
-- Name: tsisatenviocorreos tsisatenviocorreos_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatenviocorreos
    ADD CONSTRAINT tsisatenviocorreos_pkey PRIMARY KEY (cod_envio);


--
-- TOC entry 3554 (class 2606 OID 60001)
-- Name: tsisatescolaridad tsisatescolaridad_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatescolaridad
    ADD CONSTRAINT tsisatescolaridad_pkey PRIMARY KEY (cod_escolaridad);


--
-- TOC entry 3556 (class 2606 OID 60003)
-- Name: tsisatexperienciaslaborales tsisatexperienciaslaborales_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatexperienciaslaborales
    ADD CONSTRAINT tsisatexperienciaslaborales_pkey PRIMARY KEY (cod_experiencia);


--
-- TOC entry 3558 (class 2606 OID 60005)
-- Name: tsisatfirmareqper tsisatfirmas_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatfirmareqper
    ADD CONSTRAINT tsisatfirmas_pkey PRIMARY KEY (cod_firma);


--
-- TOC entry 3560 (class 2606 OID 60007)
-- Name: tsisatframeworks tsisatframeworks_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatframeworks
    ADD CONSTRAINT tsisatframeworks_pkey PRIMARY KEY (cod_framework);


--
-- TOC entry 3562 (class 2606 OID 60009)
-- Name: tsisathabilidades tsisathabilidades_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisathabilidades
    ADD CONSTRAINT tsisathabilidades_pkey PRIMARY KEY (cod_habilidad);


--
-- TOC entry 3564 (class 2606 OID 60011)
-- Name: tsisatherramientas tsisatherramientas_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatherramientas
    ADD CONSTRAINT tsisatherramientas_pkey PRIMARY KEY (cod_herramientas);


--
-- TOC entry 3566 (class 2606 OID 60013)
-- Name: tsisatides tsisatides_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatides
    ADD CONSTRAINT tsisatides_pkey PRIMARY KEY (cod_ide);


--
-- TOC entry 3568 (class 2606 OID 60015)
-- Name: tsisatidiomas tsisatidiomas_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatidiomas
    ADD CONSTRAINT tsisatidiomas_pkey PRIMARY KEY (cod_idioma);


--
-- TOC entry 3570 (class 2606 OID 60017)
-- Name: tsisatlenguajes tsisatlenguajes_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatlenguajes
    ADD CONSTRAINT tsisatlenguajes_pkey PRIMARY KEY (cod_lenguaje);


--
-- TOC entry 3572 (class 2606 OID 60019)
-- Name: tsisatmaquetados tsisatmaquetados_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatmaquetados
    ADD CONSTRAINT tsisatmaquetados_pkey PRIMARY KEY (cod_maquetado);


--
-- TOC entry 3574 (class 2606 OID 60021)
-- Name: tsisatmetodologias tsisatmetodologias_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatmetodologias
    ADD CONSTRAINT tsisatmetodologias_pkey PRIMARY KEY (cod_metodologia);


--
-- TOC entry 3576 (class 2606 OID 60023)
-- Name: tsisatmodelados tsisatmodelados_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatmodelados
    ADD CONSTRAINT tsisatmodelados_pkey PRIMARY KEY (cod_modelado);


--
-- TOC entry 3578 (class 2606 OID 60025)
-- Name: tsisatordenservicio tsisatordenservicio_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatordenservicio
    ADD CONSTRAINT tsisatordenservicio_pkey PRIMARY KEY (cod_ordenservicio);


--
-- TOC entry 3580 (class 2606 OID 60027)
-- Name: tsisatpatrones tsisatpatrones_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatpatrones
    ADD CONSTRAINT tsisatpatrones_pkey PRIMARY KEY (cod_patron);


--
-- TOC entry 3582 (class 2606 OID 60029)
-- Name: tsisatprospectos tsisatprospectos_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatprospectos
    ADD CONSTRAINT tsisatprospectos_pkey PRIMARY KEY (cod_prospecto);


--
-- TOC entry 3586 (class 2606 OID 60031)
-- Name: tsisatprotocolos tsisatprotocolos_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatprotocolos
    ADD CONSTRAINT tsisatprotocolos_pkey PRIMARY KEY (cod_protocolo);


--
-- TOC entry 3588 (class 2606 OID 60033)
-- Name: tsisatproyectos tsisatproyectos_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatproyectos
    ADD CONSTRAINT tsisatproyectos_pkey PRIMARY KEY (cod_proyecto);


--
-- TOC entry 3590 (class 2606 OID 60035)
-- Name: tsisatqa tsisatqa_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatqa
    ADD CONSTRAINT tsisatqa_pkey PRIMARY KEY (cod_qa);


--
-- TOC entry 3592 (class 2606 OID 60037)
-- Name: tsisatrepositoriolibrerias tsisatrepositoriolibrerias_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatrepositoriolibrerias
    ADD CONSTRAINT tsisatrepositoriolibrerias_pkey PRIMARY KEY (cod_repositoriolibreria);


--
-- TOC entry 3594 (class 2606 OID 60039)
-- Name: tsisatrepositorios tsisatrepositorios_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatrepositorios
    ADD CONSTRAINT tsisatrepositorios_pkey PRIMARY KEY (cod_repositorio);


--
-- TOC entry 3596 (class 2606 OID 60041)
-- Name: tsisatsgbd tsisatsgbd_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatsgbd
    ADD CONSTRAINT tsisatsgbd_pkey PRIMARY KEY (cod_sgbd);


--
-- TOC entry 3598 (class 2606 OID 60043)
-- Name: tsisatso tsisatso_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatso
    ADD CONSTRAINT tsisatso_pkey PRIMARY KEY (cod_so);


--
-- TOC entry 3600 (class 2606 OID 60045)
-- Name: tsisatvacantes tsisatvacantes_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatvacantes
    ADD CONSTRAINT tsisatvacantes_pkey PRIMARY KEY (cod_vacante);


--
-- TOC entry 3637 (class 2606 OID 60046)
-- Name: tsisatcotizaciones aud_fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcotizaciones
    ADD CONSTRAINT aud_fk_cod_creadopor FOREIGN KEY (aud_cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 3658 (class 2606 OID 60051)
-- Name: tsisatordenservicio aud_fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatordenservicio
    ADD CONSTRAINT aud_fk_cod_creadopor FOREIGN KEY (aud_cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 3638 (class 2606 OID 60056)
-- Name: tsisatcotizaciones aud_fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcotizaciones
    ADD CONSTRAINT aud_fk_cod_modificadopor FOREIGN KEY (aud_cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 3663 (class 2606 OID 60587)
-- Name: tsisatordenservicio aud_fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatordenservicio
    ADD CONSTRAINT aud_fk_cod_modificadopor FOREIGN KEY (aud_cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 3648 (class 2606 OID 60066)
-- Name: tsisatenviocorreos aud_fk_creadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatenviocorreos
    ADD CONSTRAINT aud_fk_creadopor FOREIGN KEY (aud_cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 3649 (class 2606 OID 60071)
-- Name: tsisatenviocorreos aud_fk_modificadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatenviocorreos
    ADD CONSTRAINT aud_fk_modificadopor FOREIGN KEY (aud_cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 3654 (class 2606 OID 60076)
-- Name: tsisatfirmareqper cod_autorizafk; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatfirmareqper
    ADD CONSTRAINT cod_autorizafk FOREIGN KEY (cod_autoriza) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE;


--
-- TOC entry 3655 (class 2606 OID 60081)
-- Name: tsisatfirmareqper cod_solicitafk; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatfirmareqper
    ADD CONSTRAINT cod_solicitafk FOREIGN KEY (cod_solicita) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE;


--
-- TOC entry 3656 (class 2606 OID 60086)
-- Name: tsisatfirmareqper cod_vacantefk; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatfirmareqper
    ADD CONSTRAINT cod_vacantefk FOREIGN KEY (cod_vacante) REFERENCES sisat.tsisatvacantes(cod_vacante) ON UPDATE CASCADE;


--
-- TOC entry 3624 (class 2606 OID 60091)
-- Name: tsisatcartaasignacion fk__cod_gpy; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcartaasignacion
    ADD CONSTRAINT fk__cod_gpy FOREIGN KEY (cod_gpy) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 3669 (class 2606 OID 60096)
-- Name: tsisatproyectos fk_aud_cod_creadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatproyectos
    ADD CONSTRAINT fk_aud_cod_creadopor FOREIGN KEY (aud_cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 3609 (class 2606 OID 60101)
-- Name: tsisatasignaciones fk_aud_cod_creadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatasignaciones
    ADD CONSTRAINT fk_aud_cod_creadopor FOREIGN KEY (aud_cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 3670 (class 2606 OID 60106)
-- Name: tsisatproyectos fk_aud_cod_modificadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatproyectos
    ADD CONSTRAINT fk_aud_cod_modificadopor FOREIGN KEY (aud_cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 3610 (class 2606 OID 60111)
-- Name: tsisatasignaciones fk_aud_cod_modificadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatasignaciones
    ADD CONSTRAINT fk_aud_cod_modificadopor FOREIGN KEY (aud_cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 3664 (class 2606 OID 60116)
-- Name: tsisatprospectos fk_cod_administrador; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatprospectos
    ADD CONSTRAINT fk_cod_administrador FOREIGN KEY (cod_administrador) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3625 (class 2606 OID 60121)
-- Name: tsisatcartaasignacion fk_cod_ape; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcartaasignacion
    ADD CONSTRAINT fk_cod_ape FOREIGN KEY (cod_ape) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3686 (class 2606 OID 60544)
-- Name: tsisatcomentcosteo fk_cod_candidato; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcomentcosteo
    ADD CONSTRAINT fk_cod_candidato FOREIGN KEY (cod_candidato) REFERENCES sisat.tsisatcandidatos(cod_candidato);


--
-- TOC entry 3639 (class 2606 OID 60131)
-- Name: tsisatcotizaciones fk_cod_ciudad; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcotizaciones
    ADD CONSTRAINT fk_cod_ciudad FOREIGN KEY (cod_ciudad) REFERENCES sgrt.tsgrtciudades(cod_ciudad);


--
-- TOC entry 3659 (class 2606 OID 60136)
-- Name: tsisatordenservicio fk_cod_ciudad; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatordenservicio
    ADD CONSTRAINT fk_cod_ciudad FOREIGN KEY (cod_ciudad) REFERENCES sgrt.tsgrtciudades(cod_ciudad);


--
-- TOC entry 3626 (class 2606 OID 60141)
-- Name: tsisatcartaasignacion fk_cod_cliente; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcartaasignacion
    ADD CONSTRAINT fk_cod_cliente FOREIGN KEY (cod_cliente) REFERENCES sgrh.tsgrhclientes(cod_cliente) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3660 (class 2606 OID 60146)
-- Name: tsisatordenservicio fk_cod_cliente; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatordenservicio
    ADD CONSTRAINT fk_cod_cliente FOREIGN KEY (cod_cliente) REFERENCES sgrh.tsgrhclientes(cod_cliente) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3611 (class 2606 OID 60151)
-- Name: tsisatasignaciones fk_cod_cliente; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatasignaciones
    ADD CONSTRAINT fk_cod_cliente FOREIGN KEY (cod_cliente) REFERENCES sgrh.tsgrhclientes(cod_cliente) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3675 (class 2606 OID 60156)
-- Name: tsisatvacantes fk_cod_cliente; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatvacantes
    ADD CONSTRAINT fk_cod_cliente FOREIGN KEY (cod_cliente) REFERENCES sgrh.tsgrhclientes(cod_cliente);


--
-- TOC entry 3689 (class 2606 OID 60565)
-- Name: tsisatcomentcartaasignacion fk_cod_comentcartaasignacion; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcomentcartaasignacion
    ADD CONSTRAINT fk_cod_comentcartaasignacion FOREIGN KEY (cod_asignacion) REFERENCES sisat.tsisatcartaasignacion(cod_asignacion);


--
-- TOC entry 3676 (class 2606 OID 60166)
-- Name: tsisatvacantes fk_cod_contratacion; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatvacantes
    ADD CONSTRAINT fk_cod_contratacion FOREIGN KEY (cod_contratacion) REFERENCES sisat.tsisatcontrataciones(cod_contratacion) ON UPDATE CASCADE;


--
-- TOC entry 3677 (class 2606 OID 60171)
-- Name: tsisatvacantes fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatvacantes
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (aud_cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3620 (class 2606 OID 60176)
-- Name: tsisatcartaaceptacion fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcartaaceptacion
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (aud_cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3627 (class 2606 OID 60181)
-- Name: tsisatcartaasignacion fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcartaasignacion
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (aud_cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3640 (class 2606 OID 60186)
-- Name: tsisatcotizaciones fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcotizaciones
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (aud_cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3650 (class 2606 OID 60196)
-- Name: tsisatenviocorreos fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatenviocorreos
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (aud_cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3612 (class 2606 OID 60201)
-- Name: tsisatasignaciones fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatasignaciones
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (aud_cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3645 (class 2606 OID 60206)
-- Name: tsisatentrevistas fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatentrevistas
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (aud_cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3665 (class 2606 OID 60211)
-- Name: tsisatprospectos fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatprospectos
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (aud_cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3671 (class 2606 OID 60216)
-- Name: tsisatproyectos fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatproyectos
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (aud_cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3635 (class 2606 OID 60221)
-- Name: tsisatcontrataciones fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcontrataciones
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (aud_cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE;


--
-- TOC entry 3680 (class 2606 OID 60493)
-- Name: tsisatcomentvacantes fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcomentvacantes
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (aud_cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE;


--
-- TOC entry 3683 (class 2606 OID 60517)
-- Name: tsisatcomententrevista fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcomententrevista
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (aud_cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE;


--
-- TOC entry 3687 (class 2606 OID 60549)
-- Name: tsisatcomentcosteo fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcomentcosteo
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (aud_cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE;


--
-- TOC entry 3690 (class 2606 OID 60570)
-- Name: tsisatcomentcartaasignacion fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcomentcartaasignacion
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (aud_cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE;


--
-- TOC entry 3613 (class 2606 OID 60246)
-- Name: tsisatasignaciones fk_cod_empleado; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatasignaciones
    ADD CONSTRAINT fk_cod_empleado FOREIGN KEY (cod_empleado) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 3684 (class 2606 OID 60522)
-- Name: tsisatcomententrevista fk_cod_entrevista; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcomententrevista
    ADD CONSTRAINT fk_cod_entrevista FOREIGN KEY (cod_entrevista) REFERENCES sisat.tsisatentrevistas(cod_entrevista);


--
-- TOC entry 3641 (class 2606 OID 60256)
-- Name: tsisatcotizaciones fk_cod_estado; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcotizaciones
    ADD CONSTRAINT fk_cod_estado FOREIGN KEY (cod_estado) REFERENCES sgrt.tsgrtestados(cod_estadorep);


--
-- TOC entry 3661 (class 2606 OID 60261)
-- Name: tsisatordenservicio fk_cod_estadorep; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatordenservicio
    ADD CONSTRAINT fk_cod_estadorep FOREIGN KEY (cod_estadorep) REFERENCES sgrt.tsgrtestados(cod_estadorep);


--
-- TOC entry 3628 (class 2606 OID 60266)
-- Name: tsisatcartaasignacion fk_cod_gpy; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcartaasignacion
    ADD CONSTRAINT fk_cod_gpy FOREIGN KEY (cod_gpy) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3662 (class 2606 OID 60271)
-- Name: tsisatordenservicio fk_cod_gpy; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatordenservicio
    ADD CONSTRAINT fk_cod_gpy FOREIGN KEY (cod_gpy) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3678 (class 2606 OID 60276)
-- Name: tsisatvacantes fk_cod_idioma; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatvacantes
    ADD CONSTRAINT fk_cod_idioma FOREIGN KEY (cod_idioma) REFERENCES sisat.tsisatidiomas(cod_idioma);


--
-- TOC entry 3667 (class 2606 OID 60281)
-- Name: tsisatprospectos_idiomas fk_cod_idioma; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatprospectos_idiomas
    ADD CONSTRAINT fk_cod_idioma FOREIGN KEY (cod_idioma) REFERENCES sisat.tsisatidiomas(cod_idioma);


--
-- TOC entry 3679 (class 2606 OID 60286)
-- Name: tsisatvacantes fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatvacantes
    ADD CONSTRAINT fk_cod_modificadopor FOREIGN KEY (aud_cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3621 (class 2606 OID 60291)
-- Name: tsisatcartaaceptacion fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcartaaceptacion
    ADD CONSTRAINT fk_cod_modificadopor FOREIGN KEY (aud_cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3629 (class 2606 OID 60296)
-- Name: tsisatcartaasignacion fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcartaasignacion
    ADD CONSTRAINT fk_cod_modificadopor FOREIGN KEY (aud_cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3642 (class 2606 OID 60301)
-- Name: tsisatcotizaciones fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcotizaciones
    ADD CONSTRAINT fk_cod_modificadopor FOREIGN KEY (aud_cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3651 (class 2606 OID 60311)
-- Name: tsisatenviocorreos fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatenviocorreos
    ADD CONSTRAINT fk_cod_modificadopor FOREIGN KEY (aud_cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3614 (class 2606 OID 60316)
-- Name: tsisatasignaciones fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatasignaciones
    ADD CONSTRAINT fk_cod_modificadopor FOREIGN KEY (aud_cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3646 (class 2606 OID 60321)
-- Name: tsisatentrevistas fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatentrevistas
    ADD CONSTRAINT fk_cod_modificadopor FOREIGN KEY (aud_cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3666 (class 2606 OID 60326)
-- Name: tsisatprospectos fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatprospectos
    ADD CONSTRAINT fk_cod_modificadopor FOREIGN KEY (aud_cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3672 (class 2606 OID 60331)
-- Name: tsisatproyectos fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatproyectos
    ADD CONSTRAINT fk_cod_modificadopor FOREIGN KEY (aud_cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3636 (class 2606 OID 60336)
-- Name: tsisatcontrataciones fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcontrataciones
    ADD CONSTRAINT fk_cod_modificadopor FOREIGN KEY (aud_cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE;


--
-- TOC entry 3681 (class 2606 OID 60498)
-- Name: tsisatcomentvacantes fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcomentvacantes
    ADD CONSTRAINT fk_cod_modificadopor FOREIGN KEY (aud_cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE;


--
-- TOC entry 3685 (class 2606 OID 60527)
-- Name: tsisatcomententrevista fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcomententrevista
    ADD CONSTRAINT fk_cod_modificadopor FOREIGN KEY (aud_cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE;


--
-- TOC entry 3688 (class 2606 OID 60554)
-- Name: tsisatcomentcosteo fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcomentcosteo
    ADD CONSTRAINT fk_cod_modificadopor FOREIGN KEY (aud_cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE;


--
-- TOC entry 3691 (class 2606 OID 60575)
-- Name: tsisatcomentcartaasignacion fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcomentcartaasignacion
    ADD CONSTRAINT fk_cod_modificadopor FOREIGN KEY (aud_cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE;


--
-- TOC entry 3630 (class 2606 OID 60361)
-- Name: tsisatcartaasignacion fk_cod_perfil; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcartaasignacion
    ADD CONSTRAINT fk_cod_perfil FOREIGN KEY (cod_perfil) REFERENCES sgrh.tsgrhperfiles(cod_perfil) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3617 (class 2606 OID 60366)
-- Name: tsisatcandidatos fk_cod_perfil; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcandidatos
    ADD CONSTRAINT fk_cod_perfil FOREIGN KEY (cod_perfil) REFERENCES sgrh.tsgrhperfiles(cod_perfil) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3615 (class 2606 OID 60371)
-- Name: tsisatasignaciones fk_cod_perfil; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatasignaciones
    ADD CONSTRAINT fk_cod_perfil FOREIGN KEY (cod_perfil) REFERENCES sgrh.tsgrhperfiles(cod_perfil) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3673 (class 2606 OID 60376)
-- Name: tsisatproyectos fk_cod_perfil; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatproyectos
    ADD CONSTRAINT fk_cod_perfil FOREIGN KEY (cod_perfil) REFERENCES sgrh.tsgrhperfiles(cod_perfil) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3631 (class 2606 OID 60381)
-- Name: tsisatcartaasignacion fk_cod_prospecto; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcartaasignacion
    ADD CONSTRAINT fk_cod_prospecto FOREIGN KEY (cod_prospecto) REFERENCES sisat.tsisatprospectos(cod_prospecto) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3653 (class 2606 OID 60386)
-- Name: tsisatexperienciaslaborales fk_cod_prospecto; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatexperienciaslaborales
    ADD CONSTRAINT fk_cod_prospecto FOREIGN KEY (cod_prospecto) REFERENCES sisat.tsisatprospectos(cod_prospecto) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3652 (class 2606 OID 60391)
-- Name: tsisatescolaridad fk_cod_prospecto; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatescolaridad
    ADD CONSTRAINT fk_cod_prospecto FOREIGN KEY (cod_prospecto) REFERENCES sisat.tsisatprospectos(cod_prospecto) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3644 (class 2606 OID 60396)
-- Name: tsisatcursosycertificados fk_cod_prospecto; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcursosycertificados
    ADD CONSTRAINT fk_cod_prospecto FOREIGN KEY (cod_prospecto) REFERENCES sisat.tsisatprospectos(cod_prospecto) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3657 (class 2606 OID 60401)
-- Name: tsisathabilidades fk_cod_prospecto; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisathabilidades
    ADD CONSTRAINT fk_cod_prospecto FOREIGN KEY (cod_prospecto) REFERENCES sisat.tsisatprospectos(cod_prospecto);


--
-- TOC entry 3674 (class 2606 OID 60406)
-- Name: tsisatproyectos fk_cod_prospecto; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatproyectos
    ADD CONSTRAINT fk_cod_prospecto FOREIGN KEY (cod_prospecto) REFERENCES sisat.tsisatprospectos(cod_prospecto) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3616 (class 2606 OID 60411)
-- Name: tsisatasignaciones fk_cod_prospecto; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatasignaciones
    ADD CONSTRAINT fk_cod_prospecto FOREIGN KEY (cod_prospecto) REFERENCES sisat.tsisatprospectos(cod_prospecto) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3647 (class 2606 OID 60416)
-- Name: tsisatentrevistas fk_cod_prospecto; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatentrevistas
    ADD CONSTRAINT fk_cod_prospecto FOREIGN KEY (cod_prospecto) REFERENCES sisat.tsisatprospectos(cod_prospecto);


--
-- TOC entry 3668 (class 2606 OID 60421)
-- Name: tsisatprospectos_idiomas fk_cod_prospecto; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatprospectos_idiomas
    ADD CONSTRAINT fk_cod_prospecto FOREIGN KEY (cod_prospecto) REFERENCES sisat.tsisatprospectos(cod_prospecto);


--
-- TOC entry 3643 (class 2606 OID 60431)
-- Name: tsisatcotizaciones fk_cod_puesto; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcotizaciones
    ADD CONSTRAINT fk_cod_puesto FOREIGN KEY (cod_puesto) REFERENCES sgrh.tsgrhpuestos(cod_puesto);


--
-- TOC entry 3632 (class 2606 OID 60441)
-- Name: tsisatcartaasignacion fk_cod_rhat; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcartaasignacion
    ADD CONSTRAINT fk_cod_rhat FOREIGN KEY (cod_rhta) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3633 (class 2606 OID 60446)
-- Name: tsisatcartaasignacion fk_cod_rhta; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcartaasignacion
    ADD CONSTRAINT fk_cod_rhta FOREIGN KEY (cod_rhta) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 3634 (class 2606 OID 60451)
-- Name: tsisatcartaasignacion fk_cod_rys; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcartaasignacion
    ADD CONSTRAINT fk_cod_rys FOREIGN KEY (cod_rys) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 3682 (class 2606 OID 60503)
-- Name: tsisatcomentvacantes fk_cod_vacante; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcomentvacantes
    ADD CONSTRAINT fk_cod_vacante FOREIGN KEY (cod_vacante) REFERENCES sisat.tsisatvacantes(cod_vacante);


--
-- TOC entry 3622 (class 2606 OID 60461)
-- Name: tsisatcartaaceptacion fk_creadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcartaaceptacion
    ADD CONSTRAINT fk_creadopor FOREIGN KEY (aud_cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 3618 (class 2606 OID 60466)
-- Name: tsisatcandidatos fk_creadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcandidatos
    ADD CONSTRAINT fk_creadopor FOREIGN KEY (aud_cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 3623 (class 2606 OID 60471)
-- Name: tsisatcartaaceptacion fk_modificadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcartaaceptacion
    ADD CONSTRAINT fk_modificadopor FOREIGN KEY (aud_cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 3619 (class 2606 OID 60476)
-- Name: tsisatcandidatos fk_modificadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcandidatos
    ADD CONSTRAINT fk_modificadopor FOREIGN KEY (aud_cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 3882 (class 0 OID 0)
-- Dependencies: 525
-- Name: FUNCTION buscar_asignacion_recurso(asignacion_cod integer); Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON FUNCTION sisat.buscar_asignacion_recurso(asignacion_cod integer) FROM suite;
GRANT ALL ON FUNCTION sisat.buscar_asignacion_recurso(asignacion_cod integer) TO suite WITH GRANT OPTION;


--
-- TOC entry 3883 (class 0 OID 0)
-- Dependencies: 387
-- Name: SEQUENCE seq_aceptaciones; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON SEQUENCE sisat.seq_aceptaciones FROM suite;
GRANT UPDATE ON SEQUENCE sisat.seq_aceptaciones TO suite;
GRANT SELECT,USAGE ON SEQUENCE sisat.seq_aceptaciones TO suite WITH GRANT OPTION;


--
-- TOC entry 3884 (class 0 OID 0)
-- Dependencies: 388
-- Name: SEQUENCE seq_asignaciones; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON SEQUENCE sisat.seq_asignaciones FROM suite;
GRANT UPDATE ON SEQUENCE sisat.seq_asignaciones TO suite;
GRANT SELECT,USAGE ON SEQUENCE sisat.seq_asignaciones TO suite WITH GRANT OPTION;


--
-- TOC entry 3885 (class 0 OID 0)
-- Dependencies: 389
-- Name: SEQUENCE seq_candidatos; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON SEQUENCE sisat.seq_candidatos FROM suite;
GRANT UPDATE ON SEQUENCE sisat.seq_candidatos TO suite;
GRANT SELECT,USAGE ON SEQUENCE sisat.seq_candidatos TO suite WITH GRANT OPTION;


--
-- TOC entry 3886 (class 0 OID 0)
-- Dependencies: 390
-- Name: SEQUENCE seq_cartaasignaciones; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON SEQUENCE sisat.seq_cartaasignaciones FROM suite;
GRANT UPDATE ON SEQUENCE sisat.seq_cartaasignaciones TO suite;
GRANT SELECT,USAGE ON SEQUENCE sisat.seq_cartaasignaciones TO suite WITH GRANT OPTION;


--
-- TOC entry 3887 (class 0 OID 0)
-- Dependencies: 391
-- Name: SEQUENCE seq_comentcartaasignacion; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON SEQUENCE sisat.seq_comentcartaasignacion FROM suite;
GRANT UPDATE ON SEQUENCE sisat.seq_comentcartaasignacion TO suite;
GRANT SELECT,USAGE ON SEQUENCE sisat.seq_comentcartaasignacion TO suite WITH GRANT OPTION;


--
-- TOC entry 3888 (class 0 OID 0)
-- Dependencies: 392
-- Name: SEQUENCE seq_comentcosteo; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON SEQUENCE sisat.seq_comentcosteo FROM suite;
GRANT UPDATE ON SEQUENCE sisat.seq_comentcosteo TO suite;
GRANT SELECT,USAGE ON SEQUENCE sisat.seq_comentcosteo TO suite WITH GRANT OPTION;


--
-- TOC entry 3889 (class 0 OID 0)
-- Dependencies: 393
-- Name: SEQUENCE seq_comententrevista; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON SEQUENCE sisat.seq_comententrevista FROM suite;
GRANT UPDATE ON SEQUENCE sisat.seq_comententrevista TO suite;
GRANT SELECT,USAGE ON SEQUENCE sisat.seq_comententrevista TO suite WITH GRANT OPTION;


--
-- TOC entry 3890 (class 0 OID 0)
-- Dependencies: 394
-- Name: SEQUENCE seq_comentvacantes; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON SEQUENCE sisat.seq_comentvacantes FROM suite;
GRANT UPDATE ON SEQUENCE sisat.seq_comentvacantes TO suite;
GRANT SELECT,USAGE ON SEQUENCE sisat.seq_comentvacantes TO suite WITH GRANT OPTION;


--
-- TOC entry 3891 (class 0 OID 0)
-- Dependencies: 395
-- Name: SEQUENCE seq_contrataciones; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON SEQUENCE sisat.seq_contrataciones FROM suite;
GRANT UPDATE ON SEQUENCE sisat.seq_contrataciones TO suite;
GRANT SELECT,USAGE ON SEQUENCE sisat.seq_contrataciones TO suite WITH GRANT OPTION;


--
-- TOC entry 3892 (class 0 OID 0)
-- Dependencies: 396
-- Name: SEQUENCE seq_cotizaciones; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON SEQUENCE sisat.seq_cotizaciones FROM suite;
GRANT UPDATE ON SEQUENCE sisat.seq_cotizaciones TO suite;
GRANT SELECT,USAGE ON SEQUENCE sisat.seq_cotizaciones TO suite WITH GRANT OPTION;


--
-- TOC entry 3893 (class 0 OID 0)
-- Dependencies: 397
-- Name: SEQUENCE seq_cursos; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON SEQUENCE sisat.seq_cursos FROM suite;
GRANT UPDATE ON SEQUENCE sisat.seq_cursos TO suite;
GRANT SELECT,USAGE ON SEQUENCE sisat.seq_cursos TO suite WITH GRANT OPTION;


--
-- TOC entry 3894 (class 0 OID 0)
-- Dependencies: 398
-- Name: SEQUENCE seq_entrevistas; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON SEQUENCE sisat.seq_entrevistas FROM suite;
GRANT UPDATE ON SEQUENCE sisat.seq_entrevistas TO suite;
GRANT SELECT,USAGE ON SEQUENCE sisat.seq_entrevistas TO suite WITH GRANT OPTION;


--
-- TOC entry 3895 (class 0 OID 0)
-- Dependencies: 399
-- Name: SEQUENCE seq_envios; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON SEQUENCE sisat.seq_envios FROM suite;
GRANT UPDATE ON SEQUENCE sisat.seq_envios TO suite;
GRANT SELECT,USAGE ON SEQUENCE sisat.seq_envios TO suite WITH GRANT OPTION;


--
-- TOC entry 3896 (class 0 OID 0)
-- Dependencies: 400
-- Name: SEQUENCE seq_escolaridad; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON SEQUENCE sisat.seq_escolaridad FROM suite;
GRANT UPDATE ON SEQUENCE sisat.seq_escolaridad TO suite;
GRANT SELECT,USAGE ON SEQUENCE sisat.seq_escolaridad TO suite WITH GRANT OPTION;


--
-- TOC entry 3897 (class 0 OID 0)
-- Dependencies: 401
-- Name: SEQUENCE seq_experiencias; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON SEQUENCE sisat.seq_experiencias FROM suite;
GRANT UPDATE ON SEQUENCE sisat.seq_experiencias TO suite;
GRANT SELECT,USAGE ON SEQUENCE sisat.seq_experiencias TO suite WITH GRANT OPTION;


--
-- TOC entry 3898 (class 0 OID 0)
-- Dependencies: 402
-- Name: SEQUENCE seq_firmareqper; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON SEQUENCE sisat.seq_firmareqper FROM suite;
GRANT UPDATE ON SEQUENCE sisat.seq_firmareqper TO suite;
GRANT SELECT,USAGE ON SEQUENCE sisat.seq_firmareqper TO suite WITH GRANT OPTION;


--
-- TOC entry 3899 (class 0 OID 0)
-- Dependencies: 403
-- Name: SEQUENCE seq_firmas; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON SEQUENCE sisat.seq_firmas FROM suite;
GRANT UPDATE ON SEQUENCE sisat.seq_firmas TO suite;
GRANT SELECT,USAGE ON SEQUENCE sisat.seq_firmas TO suite WITH GRANT OPTION;


--
-- TOC entry 3900 (class 0 OID 0)
-- Dependencies: 404
-- Name: SEQUENCE seq_habilidades; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON SEQUENCE sisat.seq_habilidades FROM suite;
GRANT UPDATE ON SEQUENCE sisat.seq_habilidades TO suite;
GRANT SELECT,USAGE ON SEQUENCE sisat.seq_habilidades TO suite WITH GRANT OPTION;


--
-- TOC entry 3901 (class 0 OID 0)
-- Dependencies: 405
-- Name: SEQUENCE seq_idiomas; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON SEQUENCE sisat.seq_idiomas FROM suite;
GRANT UPDATE ON SEQUENCE sisat.seq_idiomas TO suite;
GRANT SELECT,USAGE ON SEQUENCE sisat.seq_idiomas TO suite WITH GRANT OPTION;


--
-- TOC entry 3902 (class 0 OID 0)
-- Dependencies: 406
-- Name: SEQUENCE seq_ordenservicios; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON SEQUENCE sisat.seq_ordenservicios FROM suite;
GRANT UPDATE ON SEQUENCE sisat.seq_ordenservicios TO suite;
GRANT SELECT,USAGE ON SEQUENCE sisat.seq_ordenservicios TO suite WITH GRANT OPTION;


--
-- TOC entry 3903 (class 0 OID 0)
-- Dependencies: 407
-- Name: SEQUENCE seq_prospectos; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON SEQUENCE sisat.seq_prospectos FROM suite;
GRANT UPDATE ON SEQUENCE sisat.seq_prospectos TO suite;
GRANT SELECT,USAGE ON SEQUENCE sisat.seq_prospectos TO suite WITH GRANT OPTION;


--
-- TOC entry 3904 (class 0 OID 0)
-- Dependencies: 408
-- Name: SEQUENCE seq_proyectos; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON SEQUENCE sisat.seq_proyectos FROM suite;
GRANT UPDATE ON SEQUENCE sisat.seq_proyectos TO suite;
GRANT SELECT,USAGE ON SEQUENCE sisat.seq_proyectos TO suite WITH GRANT OPTION;


--
-- TOC entry 3905 (class 0 OID 0)
-- Dependencies: 409
-- Name: SEQUENCE seq_vacantes; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON SEQUENCE sisat.seq_vacantes FROM suite;
GRANT UPDATE ON SEQUENCE sisat.seq_vacantes TO suite;
GRANT SELECT,USAGE ON SEQUENCE sisat.seq_vacantes TO suite WITH GRANT OPTION;


--
-- TOC entry 3906 (class 0 OID 0)
-- Dependencies: 410
-- Name: TABLE tsisatappservices; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisatappservices FROM suite;
GRANT ALL ON TABLE sisat.tsisatappservices TO suite WITH GRANT OPTION;


--
-- TOC entry 3907 (class 0 OID 0)
-- Dependencies: 411
-- Name: TABLE tsisatarquitecturas; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisatarquitecturas FROM suite;
GRANT ALL ON TABLE sisat.tsisatarquitecturas TO suite WITH GRANT OPTION;


--
-- TOC entry 3908 (class 0 OID 0)
-- Dependencies: 412
-- Name: TABLE tsisatasignaciones; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisatasignaciones FROM suite;
GRANT ALL ON TABLE sisat.tsisatasignaciones TO suite WITH GRANT OPTION;


--
-- TOC entry 3909 (class 0 OID 0)
-- Dependencies: 413
-- Name: TABLE tsisatcandidatos; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisatcandidatos FROM suite;
GRANT ALL ON TABLE sisat.tsisatcandidatos TO suite WITH GRANT OPTION;


--
-- TOC entry 3910 (class 0 OID 0)
-- Dependencies: 414
-- Name: TABLE tsisatcartaaceptacion; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisatcartaaceptacion FROM suite;
GRANT ALL ON TABLE sisat.tsisatcartaaceptacion TO suite WITH GRANT OPTION;


--
-- TOC entry 3911 (class 0 OID 0)
-- Dependencies: 415
-- Name: TABLE tsisatcartaasignacion; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisatcartaasignacion FROM suite;
GRANT ALL ON TABLE sisat.tsisatcartaasignacion TO suite WITH GRANT OPTION;


--
-- TOC entry 3912 (class 0 OID 0)
-- Dependencies: 449
-- Name: TABLE tsisatcomentcartaasignacion; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisatcomentcartaasignacion FROM suite;
GRANT ALL ON TABLE sisat.tsisatcomentcartaasignacion TO suite WITH GRANT OPTION;


--
-- TOC entry 3913 (class 0 OID 0)
-- Dependencies: 448
-- Name: TABLE tsisatcomentcosteo; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisatcomentcosteo FROM suite;
GRANT ALL ON TABLE sisat.tsisatcomentcosteo TO suite WITH GRANT OPTION;


--
-- TOC entry 3914 (class 0 OID 0)
-- Dependencies: 447
-- Name: TABLE tsisatcomententrevista; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisatcomententrevista FROM suite;
GRANT ALL ON TABLE sisat.tsisatcomententrevista TO suite WITH GRANT OPTION;


--
-- TOC entry 3915 (class 0 OID 0)
-- Dependencies: 446
-- Name: TABLE tsisatcomentvacantes; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisatcomentvacantes FROM suite;
GRANT ALL ON TABLE sisat.tsisatcomentvacantes TO suite WITH GRANT OPTION;


--
-- TOC entry 3916 (class 0 OID 0)
-- Dependencies: 416
-- Name: TABLE tsisatcontrataciones; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisatcontrataciones FROM suite;
GRANT ALL ON TABLE sisat.tsisatcontrataciones TO suite WITH GRANT OPTION;


--
-- TOC entry 3917 (class 0 OID 0)
-- Dependencies: 417
-- Name: TABLE tsisatcotizaciones; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisatcotizaciones FROM suite;
GRANT ALL ON TABLE sisat.tsisatcotizaciones TO suite WITH GRANT OPTION;


--
-- TOC entry 3918 (class 0 OID 0)
-- Dependencies: 418
-- Name: TABLE tsisatcursosycertificados; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisatcursosycertificados FROM suite;
GRANT ALL ON TABLE sisat.tsisatcursosycertificados TO suite WITH GRANT OPTION;


--
-- TOC entry 3919 (class 0 OID 0)
-- Dependencies: 419
-- Name: TABLE tsisatentrevistas; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisatentrevistas FROM suite;
GRANT ALL ON TABLE sisat.tsisatentrevistas TO suite WITH GRANT OPTION;


--
-- TOC entry 3920 (class 0 OID 0)
-- Dependencies: 420
-- Name: TABLE tsisatenviocorreos; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisatenviocorreos FROM suite;
GRANT ALL ON TABLE sisat.tsisatenviocorreos TO suite WITH GRANT OPTION;


--
-- TOC entry 3921 (class 0 OID 0)
-- Dependencies: 421
-- Name: TABLE tsisatescolaridad; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisatescolaridad FROM suite;
GRANT ALL ON TABLE sisat.tsisatescolaridad TO suite WITH GRANT OPTION;


--
-- TOC entry 3922 (class 0 OID 0)
-- Dependencies: 422
-- Name: TABLE tsisatexperienciaslaborales; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisatexperienciaslaborales FROM suite;
GRANT ALL ON TABLE sisat.tsisatexperienciaslaborales TO suite WITH GRANT OPTION;


--
-- TOC entry 3923 (class 0 OID 0)
-- Dependencies: 423
-- Name: TABLE tsisatfirmareqper; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisatfirmareqper FROM suite;
GRANT ALL ON TABLE sisat.tsisatfirmareqper TO suite WITH GRANT OPTION;


--
-- TOC entry 3924 (class 0 OID 0)
-- Dependencies: 424
-- Name: TABLE tsisatframeworks; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisatframeworks FROM suite;
GRANT ALL ON TABLE sisat.tsisatframeworks TO suite WITH GRANT OPTION;


--
-- TOC entry 3925 (class 0 OID 0)
-- Dependencies: 425
-- Name: TABLE tsisathabilidades; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisathabilidades FROM suite;
GRANT ALL ON TABLE sisat.tsisathabilidades TO suite WITH GRANT OPTION;


--
-- TOC entry 3926 (class 0 OID 0)
-- Dependencies: 426
-- Name: TABLE tsisatherramientas; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisatherramientas FROM suite;
GRANT ALL ON TABLE sisat.tsisatherramientas TO suite WITH GRANT OPTION;


--
-- TOC entry 3928 (class 0 OID 0)
-- Dependencies: 427
-- Name: SEQUENCE tsisatherramientas_cod_herramientas_seq; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON SEQUENCE sisat.tsisatherramientas_cod_herramientas_seq FROM suite;
GRANT UPDATE ON SEQUENCE sisat.tsisatherramientas_cod_herramientas_seq TO suite;
GRANT SELECT,USAGE ON SEQUENCE sisat.tsisatherramientas_cod_herramientas_seq TO suite WITH GRANT OPTION;


--
-- TOC entry 3929 (class 0 OID 0)
-- Dependencies: 428
-- Name: TABLE tsisatides; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisatides FROM suite;
GRANT ALL ON TABLE sisat.tsisatides TO suite WITH GRANT OPTION;


--
-- TOC entry 3930 (class 0 OID 0)
-- Dependencies: 429
-- Name: TABLE tsisatidiomas; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisatidiomas FROM suite;
GRANT ALL ON TABLE sisat.tsisatidiomas TO suite WITH GRANT OPTION;


--
-- TOC entry 3931 (class 0 OID 0)
-- Dependencies: 430
-- Name: TABLE tsisatlenguajes; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisatlenguajes FROM suite;
GRANT ALL ON TABLE sisat.tsisatlenguajes TO suite WITH GRANT OPTION;


--
-- TOC entry 3932 (class 0 OID 0)
-- Dependencies: 431
-- Name: TABLE tsisatmaquetados; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisatmaquetados FROM suite;
GRANT ALL ON TABLE sisat.tsisatmaquetados TO suite WITH GRANT OPTION;


--
-- TOC entry 3933 (class 0 OID 0)
-- Dependencies: 432
-- Name: TABLE tsisatmetodologias; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisatmetodologias FROM suite;
GRANT ALL ON TABLE sisat.tsisatmetodologias TO suite WITH GRANT OPTION;


--
-- TOC entry 3934 (class 0 OID 0)
-- Dependencies: 433
-- Name: TABLE tsisatmodelados; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisatmodelados FROM suite;
GRANT ALL ON TABLE sisat.tsisatmodelados TO suite WITH GRANT OPTION;


--
-- TOC entry 3935 (class 0 OID 0)
-- Dependencies: 434
-- Name: TABLE tsisatordenservicio; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisatordenservicio FROM suite;
GRANT ALL ON TABLE sisat.tsisatordenservicio TO suite WITH GRANT OPTION;


--
-- TOC entry 3936 (class 0 OID 0)
-- Dependencies: 435
-- Name: TABLE tsisatpatrones; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisatpatrones FROM suite;
GRANT ALL ON TABLE sisat.tsisatpatrones TO suite WITH GRANT OPTION;


--
-- TOC entry 3937 (class 0 OID 0)
-- Dependencies: 436
-- Name: TABLE tsisatprospectos; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisatprospectos FROM suite;
GRANT ALL ON TABLE sisat.tsisatprospectos TO suite WITH GRANT OPTION;


--
-- TOC entry 3938 (class 0 OID 0)
-- Dependencies: 437
-- Name: TABLE tsisatprospectos_idiomas; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisatprospectos_idiomas FROM suite;
GRANT ALL ON TABLE sisat.tsisatprospectos_idiomas TO suite WITH GRANT OPTION;


--
-- TOC entry 3939 (class 0 OID 0)
-- Dependencies: 438
-- Name: TABLE tsisatprotocolos; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisatprotocolos FROM suite;
GRANT ALL ON TABLE sisat.tsisatprotocolos TO suite WITH GRANT OPTION;


--
-- TOC entry 3940 (class 0 OID 0)
-- Dependencies: 439
-- Name: TABLE tsisatproyectos; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisatproyectos FROM suite;
GRANT ALL ON TABLE sisat.tsisatproyectos TO suite WITH GRANT OPTION;


--
-- TOC entry 3941 (class 0 OID 0)
-- Dependencies: 440
-- Name: TABLE tsisatqa; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisatqa FROM suite;
GRANT ALL ON TABLE sisat.tsisatqa TO suite WITH GRANT OPTION;


--
-- TOC entry 3942 (class 0 OID 0)
-- Dependencies: 441
-- Name: TABLE tsisatrepositoriolibrerias; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisatrepositoriolibrerias FROM suite;
GRANT ALL ON TABLE sisat.tsisatrepositoriolibrerias TO suite WITH GRANT OPTION;


--
-- TOC entry 3943 (class 0 OID 0)
-- Dependencies: 442
-- Name: TABLE tsisatrepositorios; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisatrepositorios FROM suite;
GRANT ALL ON TABLE sisat.tsisatrepositorios TO suite WITH GRANT OPTION;


--
-- TOC entry 3944 (class 0 OID 0)
-- Dependencies: 443
-- Name: TABLE tsisatsgbd; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisatsgbd FROM suite;
GRANT ALL ON TABLE sisat.tsisatsgbd TO suite WITH GRANT OPTION;


--
-- TOC entry 3945 (class 0 OID 0)
-- Dependencies: 444
-- Name: TABLE tsisatso; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisatso FROM suite;
GRANT ALL ON TABLE sisat.tsisatso TO suite WITH GRANT OPTION;


--
-- TOC entry 3946 (class 0 OID 0)
-- Dependencies: 445
-- Name: TABLE tsisatvacantes; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisatvacantes FROM suite;
GRANT ALL ON TABLE sisat.tsisatvacantes TO suite WITH GRANT OPTION;


--
-- TOC entry 2502 (class 826 OID 60481)
-- Name: DEFAULT PRIVILEGES FOR SEQUENCES; Type: DEFAULT ACL; Schema: sisat; Owner: suite
--

ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sisat REVOKE ALL ON SEQUENCES  FROM suite;
ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sisat GRANT SELECT,USAGE ON SEQUENCES  TO suite WITH GRANT OPTION;


--
-- TOC entry 2503 (class 826 OID 60482)
-- Name: DEFAULT PRIVILEGES FOR TYPES; Type: DEFAULT ACL; Schema: sisat; Owner: suite
--

ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sisat REVOKE ALL ON TYPES  FROM PUBLIC;
ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sisat REVOKE ALL ON TYPES  FROM suite;
ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sisat GRANT ALL ON TYPES  TO suite WITH GRANT OPTION;


--
-- TOC entry 2504 (class 826 OID 60483)
-- Name: DEFAULT PRIVILEGES FOR FUNCTIONS; Type: DEFAULT ACL; Schema: sisat; Owner: suite
--

ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sisat REVOKE ALL ON FUNCTIONS  FROM PUBLIC;
ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sisat REVOKE ALL ON FUNCTIONS  FROM suite;
ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sisat GRANT ALL ON FUNCTIONS  TO suite WITH GRANT OPTION;


--
-- TOC entry 2505 (class 826 OID 60484)
-- Name: DEFAULT PRIVILEGES FOR TABLES; Type: DEFAULT ACL; Schema: sisat; Owner: suite
--

ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sisat REVOKE ALL ON TABLES  FROM suite;
ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sisat GRANT ALL ON TABLES  TO suite WITH GRANT OPTION;


-- Completed on 2019-12-10 14:55:15

--
-- PostgreSQL database dump complete
--

