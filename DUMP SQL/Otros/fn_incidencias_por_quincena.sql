CREATE OR REPLACE FUNCTION sgnom.fn_incidencias_por_quincena("quincena" int4)
  RETURNS TABLE("idincidencia" int4, "fechaalta" date, "clave" varchar, "incidencia" varchar, "idtipo" bpchar, "desctipo" text, "cantidad" int2, "actividad" varchar, "comentarios" text, "reportaid" int4, "reportanb" text, "autorizaid" int4, "autorizanb" text, "perfil" varchar, "detallefechas" text, "montoincidencia" numeric, "montopagado" numeric, "validacion" bool, "aceptacion" bool, "quincenaid" int4, "desquincena" varchar, "creaid" int4, "creanb" text) AS $BODY$
BEGIN 
RETURN QUERY 
SELECT incidencias.cod_incidenciaid idincidencia,
		incidencias.aud_feccreacion fechaalta,
		catincidencias.cod_claveincidencia clave,
		catincidencias.cod_nbincidencia incidencia,
		catincidencias.cod_tipoincidencia idtipo,
		CASE 
			WHEN catincidencias.cod_tipoincidencia='1' THEN
			 'HORAS'
			WHEN catincidencias.cod_tipoincidencia='2' THEN
			 'DIAS' 
		  WHEN catincidencias.cod_tipoincidencia='3' THEN
			 'ACTIVIDAD' 
			ELSE
			 'NO DATA'
		END desc_tipo,
		incidencias.cnu_cantidad cantidad,
		incidencias.des_actividad actividad,
		incidencias.txt_comentarios comentarios,
		incidencias.cod_empreporta_fk reportaid,
		(SELECT CONCAT(rhempleados.des_nombre ||' ', rhempleados.des_nombres ,' ' || rhempleados.des_apepaterno || ' ' || rhempleados.des_apematerno) 
		FROM sgrh.tsgrhempleados rhempleados
		WHERE rhempleados.cod_empleado = (SELECT nom.cod_empleado_fk FROM sgnom.tsgnomempleados nom WHERE nom.cod_empleadoid = incidencias.cod_empreporta_fk)
		) reportanb,
		incidencias.cod_empautoriza_fk autorizaid,
		(SELECT CONCAT(rhempleados.des_nombre ||' ', rhempleados.des_nombres ,' ' || rhempleados.des_apepaterno || ' ' || rhempleados.des_apematerno) 
		FROM sgrh.tsgrhempleados rhempleados
		WHERE rhempleados.cod_empleado = incidencias.cod_empautoriza_fk
		) autorizanb,
		catincidencias.cod_perfilincidencia perfil,
		CAST(incidencias.xml_detcantidad AS TEXT) detallefechas,
		catincidencias.imp_monto montoincidencia,
		incidencias.imp_monto montopagado,
		incidencias.bol_validacion validacion, --validacion rh
		incidencias.bol_aceptacion aceptacion, --aceptacion lider de celula
		incidencias.cod_quincenaid_fk quincena,
		(SELECT qui.des_quincena FROM sgnom.tsgnomquincena qui WHERE qui.cod_quincenaid = incidencias.cod_quincenaid_fk) desquincena,
		incidencias.aud_codcreadopor creaid,
		(SELECT CONCAT(rhempleados.des_nombre ||' ', rhempleados.des_nombres ,' ' || rhempleados.des_apepaterno || ' ' || rhempleados.des_apematerno) 
		FROM sgrh.tsgrhempleados rhempleados
		WHERE rhempleados.cod_empleado = incidencias.aud_codcreadopor
		) creanb
FROM sgnom.tsgnomincidencia incidencias
JOIN sgnom.tsgnomcatincidencia catincidencias 
ON incidencias.cod_catincidenciaid_fk = catincidencias.cod_catincidenciaid
--WHERE incidencias.cod_empreporta_fk = idempleado 
	AND incidencias.cod_quincenaid_fk = quincena
	AND incidencias.bol_estatus = true 
ORDER BY 1 DESC, 2;

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000