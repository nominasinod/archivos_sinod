/*
 Navicat Premium Data Transfer

 Source Server         : suite
 Source Server Type    : PostgreSQL
 Source Server Version : 100006
 Source Host           : localhost:5432
 Source Catalog        : suite
 Source Schema         : sgrh

 Target Server Type    : PostgreSQL
 Target Server Version : 100006
 File Encoding         : 65001

 Date: 24/09/2019 19:53:07
*/


-- ----------------------------
-- Table structure for tsgrhroles
-- ----------------------------
DROP TABLE IF EXISTS "sgrh"."tsgrhroles";
CREATE TABLE "sgrh"."tsgrhroles" (
  "cod_rol" int4 NOT NULL,
  "des_nbrol" varchar(15) COLLATE "pg_catalog"."default" NOT NULL
)
;

-- ----------------------------
-- Records of tsgrhroles
-- ----------------------------
INSERT INTO "sgrh"."tsgrhroles" VALUES (1, 'Administrador');
INSERT INTO "sgrh"."tsgrhroles" VALUES (2, 'Responsable');
INSERT INTO "sgrh"."tsgrhroles" VALUES (3, 'Empleado');
