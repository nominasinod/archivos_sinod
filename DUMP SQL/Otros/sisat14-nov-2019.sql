--
-- PostgreSQL database dump
--

-- Dumped from database version 10.6
-- Dumped by pg_dump version 10.6

-- Started on 2019-11-14 12:43:45

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 8 (class 2615 OID 42571)
-- Name: sisat; Type: SCHEMA; Schema: -; Owner: suite
--

CREATE SCHEMA sisat;


ALTER SCHEMA sisat OWNER TO suite;

--
-- TOC entry 3867 (class 0 OID 0)
-- Dependencies: 8
-- Name: SCHEMA sisat; Type: COMMENT; Schema: -; Owner: suite
--

COMMENT ON SCHEMA sisat IS 'Esquema que contiene las tablas del Sistema de Seleccion y Adquisicion de Talentos';


--
-- TOC entry 506 (class 1255 OID 42740)
-- Name: buscar_asignacion_recurso(integer); Type: FUNCTION; Schema: sisat; Owner: suite
--

CREATE FUNCTION sisat.buscar_asignacion_recurso(asignacion_cod integer) RETURNS TABLE(cod_asignacion integer, cod_prospecto integer, nombreprospecto text, des_perfil character varying, des_observacion character varying, des_actividades character varying, des_lugarsalida character varying, des_lugarllegada character varying, fec_llegada date, fec_salida date, cod_transporte character varying, des_lugarhopedaje character varying, fec_hospedaje date, des_computadora character varying, cod_telefono character varying, des_accesorios character varying, des_nbresponsable character varying, des_nbpuesto character varying, des_lugarresp character varying, cod_telefonoresp character varying, tim_horario time without time zone, fec_iniciocontra date, fec_terminocontra date, imp_sueldomensual numeric, imp_nominaimss numeric, imp_honorarios numeric, imp_otros numeric, cod_rfc character varying, des_razonsocial character varying, des_correo character varying, cod_cpostal integer, des_direccionfact character varying, des_nbcliente character varying, des_direccioncte character varying, des_nbcontactocte character varying, des_correocte character varying, cod_telefonocte character varying, audcodmodificacion integer, audfechamodificacion date, cod_empleado integer, codrhta text, codgpy text, codape text, codrys text)
    LANGUAGE plpgsql
    AS $$
BEGIN 
RETURN QUERY
SELECT c.cod_asignacion, p.cod_prospecto, p.des_nombre ||' '|| (case when p.des_nombres is NULL then '' else p.des_nombres::character varying(60) end) ||' '|| p.des_appaterno ||' '|| p.des_apmaterno AS NombreProspecto, perf.des_perfil, c.des_observacion, 
    c.des_actividades, c.des_lugarsalida, c.des_lugarllegada, c.fec_llegada,c.fec_salida, c.cod_transporte, c.des_lugarhopedaje, 
    c.fec_hospedaje, c.des_computadora, c.cod_telefono, c.des_accesorios, c.des_nbresponsable, c.des_nbpuesto, c.des_lugarresp, 
    c.cod_telefonoresp, c.tim_horario, c.fec_iniciocontra, c.fec_terminocontra, c.imp_sueldomensual, c.imp_nominaimss, 
    c.imp_honorarios, c.imp_otros,c.cod_rfc, c.des_razonsocial, c.des_correo, c.cod_cpostal, c.des_direccionfact, 
    cte.des_nbcliente, cte.des_direccioncte, cte.des_nbcontactocte, cte.des_correocte, cte.cod_telefonocte,c.aud_cod_modificadopor, c.aud_fecha_modificacion, emp.cod_empleado,
    emp.des_nombre ||' '|| (case when emp.des_nombres is NULL then '' else emp.des_nombres::character varying(60) end) ||' '|| emp.des_apepaterno ||' '|| emp.des_apematerno AS Cod_Rhta, (
    
    SELECT 
    empl.des_nombre ||' '|| (case when empl.des_nombres is NULL then '' else empl.des_nombres::character varying(60) end) ||' '|| empl.des_apepaterno ||' '|| empl.des_apematerno AS CodGpy
        
    FROM sisat.tsisatcartaasignacion ca
    inner join  sisat.tsisatprospectos pr on pr.cod_prospecto= ca.cod_prospecto
    inner join sgrh.tsgrhempleados empl on empl.cod_empleado = ca.cod_gpy 
    WHERE ca.cod_asignacion = c.cod_asignacion
    ), (
    SELECT 
    empl.des_nombre ||' '|| (case when empl.des_nombres is NULL then '' else empl.des_nombres::character varying(60) end) ||' '|| empl.des_apepaterno ||' '|| empl.des_apematerno AS CodApe
        
    FROM sisat.tsisatcartaasignacion ca
    inner join  sisat.tsisatprospectos pr on pr.cod_prospecto= ca.cod_prospecto
    inner join sgrh.tsgrhempleados empl on empl.cod_empleado = ca.cod_ape
    WHERE ca.cod_asignacion = c.cod_asignacion), (
    SELECT 
    empl.des_nombre ||' '|| (case when empl.des_nombres is NULL then '' else empl.des_nombres::character varying(60) end) ||' '|| empl.des_apepaterno ||' '|| empl.des_apematerno AS CodRys
        
    FROM sisat.tsisatcartaasignacion ca
    inner join  sisat.tsisatprospectos pr on pr.cod_prospecto= ca.cod_prospecto
    inner join sgrh.tsgrhempleados empl on empl.cod_empleado = ca.cod_rys 
    WHERE ca.cod_asignacion = c.cod_asignacion
    )
        
FROM sisat.tsisatcartaasignacion c
inner join  sgrh.tsgrhperfiles perf on perf.cod_perfil=c.cod_perfil
inner join sgrh.tsgrhclientes cte on cte.cod_cliente=c.cod_cliente
inner join  sisat.tsisatprospectos p on p.cod_prospecto= c.cod_prospecto
inner join sgrh.tsgrhempleados emp on emp.cod_empleado = c.cod_rhta
WHERE p.cod_prospecto= asignacion_cod
GROUP BY c.cod_asignacion, p.cod_prospecto, NombreProspecto, perf.des_perfil, c.des_observacion, 
    c.des_actividades, c.des_lugarsalida, c.des_lugarllegada, c.fec_llegada,c.fec_salida, c.cod_transporte, c.des_lugarhopedaje, 
    c.fec_hospedaje, c.des_computadora, c.cod_telefono, c.des_accesorios, c.des_nbresponsable, c.des_nbpuesto, c.des_lugarresp, 
    c.cod_telefonoresp, c.tim_horario, c.fec_iniciocontra, c.fec_terminocontra, c.imp_sueldomensual, c.imp_nominaimss, 
    c.imp_honorarios, c.imp_otros,c.cod_rfc, c.des_razonsocial, c.des_correo, c.cod_cpostal, c.des_direccionfact, 
    cte.des_nbcliente, cte.des_direccioncte, cte.des_nbcontactocte, cte.des_correocte, cte.cod_telefonocte,c.aud_cod_modificadopor, c.aud_fecha_modificacion, emp.cod_empleado, CodRhta 
ORDER BY cod_asignacion  asc;
END;
$$;


ALTER FUNCTION sisat.buscar_asignacion_recurso(asignacion_cod integer) OWNER TO suite;

--
-- TOC entry 519 (class 1255 OID 45543)
-- Name: requisicion_de_personal(integer); Type: FUNCTION; Schema: sisat; Owner: postgres
--

CREATE FUNCTION sisat.requisicion_de_personal(cliente integer) RETURNS TABLE(codvacante integer, fecsolicitud date, fecentrega date, desrqvacante character varying, desescolaridad character varying, sexo character varying, cnuanexperiencia smallint, txtexperiencia text, txtconocimientostecno text, codnbidioma character varying, codnivelidioma character varying, fecinicio date, fectermino date, desesquema character varying, codsalarioestmin numeric, codsalarioestmax numeric, timjornada character varying, desnbcliente character varying, desdireccioncte character varying, codtelefonocte character varying, descorreocte character varying, nbempleado text, nbsolicita text)
    LANGUAGE plpgsql
    AS $$
BEGIN 
RETURN QUERY
SELECT v.cod_vacante, v.fec_solicitud, v.fec_entrega, v.des_rqvacante, 
		v.des_escolaridad, v.sexo, v.cnu_anexperiencia, v.txt_experiencia, 
		v.txt_conocimientostecno, i.cod_nbidioma, v.cod_nivelidioma, c.fec_inicio,
		c.fec_termino,c.des_esquema, c.cod_salarioestmin, c.cod_salarioestmax, 
		c.tim_jornada, cte.des_nbcliente, cte.des_direccioncte, cte.cod_telefonocte,
		cte.des_correocte, (SELECT 
    empl.des_nombre ||' '|| (case when empl.des_nombres is NULL then '' else empl.des_nombres::character varying(60) end) ||' '|| empl.des_apepaterno ||' '|| empl.des_apematerno AS nbempleado
        
    FROM sisat.tsisatfirmareqper f
    inner join  sisat.tsisatvacantes pr on pr.cod_vacante = f.cod_vacante
    inner join sgrh.tsgrhempleados empl on empl.cod_empleado = f.cod_solicita 
    WHERE f.cod_vacante = v.cod_vacante),(SELECT 
    empl.des_nombre ||' '|| (case when empl.des_nombres is NULL then '' else empl.des_nombres::character varying(60) end) ||' '|| empl.des_apepaterno ||' '|| empl.des_apematerno AS nbautoriza
        
    FROM sisat.tsisatfirmareqper f
    inner join  sisat.tsisatvacantes pr on pr.cod_vacante = f.cod_vacante
    inner join sgrh.tsgrhempleados empl on empl.cod_empleado = f.cod_autoriza
    WHERE f.cod_vacante = v.cod_vacante)
FROM sisat.tsisatvacantes v
INNER JOIN sisat.tsisatidiomas i ON i.cod_idioma = v.cod_idioma
INNER JOIN sisat.tsisatprospectos_idiomas pi ON pi.cod_idioma = i.cod_idioma
INNER JOIN sisat.tsisatcontrataciones c ON c.cod_contratacion = v.cod_contratacion
INNER JOIN sgrh.tsgrhclientes cte on cte.cod_cliente=v.cod_cliente
WHERE v.cod_cliente = cliente
GROUP BY v.cod_vacante, v.fec_solicitud, v.fec_entrega, v.des_rqvacante, 
		v.des_escolaridad, v.sexo, v.cnu_anexperiencia, i.cod_nbidioma, v.cod_nivelidioma,
		v.txt_experiencia, v.txt_conocimientostecno, v.cod_cliente, c.fec_inicio, c.fec_termino, 
		c.des_esquema, c.cod_salarioestmin, c.cod_salarioestmax, c.tim_jornada,
		cte.des_nbcliente, cte.des_direccioncte, cte.cod_telefonocte,
		cte.des_correocte
ORDER BY fec_solicitud desc;
END;
	$$;


ALTER FUNCTION sisat.requisicion_de_personal(cliente integer) OWNER TO postgres;

--
-- TOC entry 383 (class 1259 OID 43414)
-- Name: seq_aceptaciones; Type: SEQUENCE; Schema: sisat; Owner: suite
--

CREATE SEQUENCE sisat.seq_aceptaciones
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sisat.seq_aceptaciones OWNER TO suite;

--
-- TOC entry 384 (class 1259 OID 43416)
-- Name: seq_asignaciones; Type: SEQUENCE; Schema: sisat; Owner: suite
--

CREATE SEQUENCE sisat.seq_asignaciones
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sisat.seq_asignaciones OWNER TO suite;

--
-- TOC entry 385 (class 1259 OID 43418)
-- Name: seq_candidatos; Type: SEQUENCE; Schema: sisat; Owner: suite
--

CREATE SEQUENCE sisat.seq_candidatos
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sisat.seq_candidatos OWNER TO suite;

--
-- TOC entry 386 (class 1259 OID 43420)
-- Name: seq_cartaasignaciones; Type: SEQUENCE; Schema: sisat; Owner: suite
--

CREATE SEQUENCE sisat.seq_cartaasignaciones
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sisat.seq_cartaasignaciones OWNER TO suite;

--
-- TOC entry 387 (class 1259 OID 43422)
-- Name: seq_comentcartaasignacion; Type: SEQUENCE; Schema: sisat; Owner: suite
--

CREATE SEQUENCE sisat.seq_comentcartaasignacion
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sisat.seq_comentcartaasignacion OWNER TO suite;

--
-- TOC entry 388 (class 1259 OID 43424)
-- Name: seq_comentcosteo; Type: SEQUENCE; Schema: sisat; Owner: suite
--

CREATE SEQUENCE sisat.seq_comentcosteo
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sisat.seq_comentcosteo OWNER TO suite;

--
-- TOC entry 389 (class 1259 OID 43426)
-- Name: seq_comententrevista; Type: SEQUENCE; Schema: sisat; Owner: suite
--

CREATE SEQUENCE sisat.seq_comententrevista
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sisat.seq_comententrevista OWNER TO suite;

--
-- TOC entry 390 (class 1259 OID 43428)
-- Name: seq_comentvacantes; Type: SEQUENCE; Schema: sisat; Owner: suite
--

CREATE SEQUENCE sisat.seq_comentvacantes
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sisat.seq_comentvacantes OWNER TO suite;

--
-- TOC entry 391 (class 1259 OID 43430)
-- Name: seq_contrataciones; Type: SEQUENCE; Schema: sisat; Owner: suite
--

CREATE SEQUENCE sisat.seq_contrataciones
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sisat.seq_contrataciones OWNER TO suite;

--
-- TOC entry 392 (class 1259 OID 43432)
-- Name: seq_cotizaciones; Type: SEQUENCE; Schema: sisat; Owner: suite
--

CREATE SEQUENCE sisat.seq_cotizaciones
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sisat.seq_cotizaciones OWNER TO suite;

--
-- TOC entry 393 (class 1259 OID 43434)
-- Name: seq_cursos; Type: SEQUENCE; Schema: sisat; Owner: suite
--

CREATE SEQUENCE sisat.seq_cursos
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sisat.seq_cursos OWNER TO suite;

--
-- TOC entry 394 (class 1259 OID 43436)
-- Name: seq_entrevistas; Type: SEQUENCE; Schema: sisat; Owner: suite
--

CREATE SEQUENCE sisat.seq_entrevistas
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sisat.seq_entrevistas OWNER TO suite;

--
-- TOC entry 395 (class 1259 OID 43438)
-- Name: seq_envios; Type: SEQUENCE; Schema: sisat; Owner: suite
--

CREATE SEQUENCE sisat.seq_envios
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sisat.seq_envios OWNER TO suite;

--
-- TOC entry 396 (class 1259 OID 43440)
-- Name: seq_escolaridad; Type: SEQUENCE; Schema: sisat; Owner: suite
--

CREATE SEQUENCE sisat.seq_escolaridad
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sisat.seq_escolaridad OWNER TO suite;

--
-- TOC entry 397 (class 1259 OID 43442)
-- Name: seq_experiencias; Type: SEQUENCE; Schema: sisat; Owner: suite
--

CREATE SEQUENCE sisat.seq_experiencias
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sisat.seq_experiencias OWNER TO suite;

--
-- TOC entry 398 (class 1259 OID 43444)
-- Name: seq_firmareqper; Type: SEQUENCE; Schema: sisat; Owner: suite
--

CREATE SEQUENCE sisat.seq_firmareqper
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sisat.seq_firmareqper OWNER TO suite;

--
-- TOC entry 399 (class 1259 OID 43446)
-- Name: seq_firmas; Type: SEQUENCE; Schema: sisat; Owner: suite
--

CREATE SEQUENCE sisat.seq_firmas
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sisat.seq_firmas OWNER TO suite;

--
-- TOC entry 400 (class 1259 OID 43448)
-- Name: seq_habilidades; Type: SEQUENCE; Schema: sisat; Owner: suite
--

CREATE SEQUENCE sisat.seq_habilidades
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sisat.seq_habilidades OWNER TO suite;

--
-- TOC entry 401 (class 1259 OID 43450)
-- Name: seq_idiomas; Type: SEQUENCE; Schema: sisat; Owner: suite
--

CREATE SEQUENCE sisat.seq_idiomas
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sisat.seq_idiomas OWNER TO suite;

--
-- TOC entry 402 (class 1259 OID 43452)
-- Name: seq_ordenservicios; Type: SEQUENCE; Schema: sisat; Owner: suite
--

CREATE SEQUENCE sisat.seq_ordenservicios
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sisat.seq_ordenservicios OWNER TO suite;

--
-- TOC entry 403 (class 1259 OID 43454)
-- Name: seq_prospectos; Type: SEQUENCE; Schema: sisat; Owner: suite
--

CREATE SEQUENCE sisat.seq_prospectos
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sisat.seq_prospectos OWNER TO suite;

--
-- TOC entry 404 (class 1259 OID 43456)
-- Name: seq_proyectos; Type: SEQUENCE; Schema: sisat; Owner: suite
--

CREATE SEQUENCE sisat.seq_proyectos
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sisat.seq_proyectos OWNER TO suite;

--
-- TOC entry 405 (class 1259 OID 43458)
-- Name: seq_vacantes; Type: SEQUENCE; Schema: sisat; Owner: suite
--

CREATE SEQUENCE sisat.seq_vacantes
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sisat.seq_vacantes OWNER TO suite;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 406 (class 1259 OID 43460)
-- Name: tsisatappservices; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisatappservices (
    cod_appservice integer NOT NULL,
    des_appservice character varying(30) NOT NULL
);


ALTER TABLE sisat.tsisatappservices OWNER TO suite;

--
-- TOC entry 407 (class 1259 OID 43463)
-- Name: tsisatarquitecturas; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisatarquitecturas (
    cod_arquitectura integer NOT NULL,
    des_arquitectura character varying(30) NOT NULL
);


ALTER TABLE sisat.tsisatarquitecturas OWNER TO suite;

--
-- TOC entry 408 (class 1259 OID 43466)
-- Name: tsisatasignaciones; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisatasignaciones (
    cod_asignacion integer DEFAULT nextval('sisat.seq_asignaciones'::regclass) NOT NULL,
    cod_prospecto integer NOT NULL,
    cod_perfil integer NOT NULL,
    cod_cliente integer NOT NULL,
    des_correocte character varying(40) NOT NULL,
    cod_telefonocte character varying(16) NOT NULL,
    des_direccioncte character varying(200),
    cod_empleado integer,
    aud_cod_creadopor integer NOT NULL,
    aud_cod_modificadopor integer,
    aud_fec_creacion date NOT NULL,
    aud_fec_modificacion date
);


ALTER TABLE sisat.tsisatasignaciones OWNER TO suite;

--
-- TOC entry 409 (class 1259 OID 43470)
-- Name: tsisatcandidatos; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisatcandidatos (
    cod_candidato integer DEFAULT nextval('sisat.seq_candidatos'::regclass) NOT NULL,
    des_nombre character varying(50) NOT NULL,
    cod_perfil integer NOT NULL,
    imp_sueldo numeric(6,2),
    imp_sueldodia numeric(6,2),
    imp_nominaimss numeric(6,2),
    imp_honorarios numeric(6,2),
    imp_cargasocial numeric(6,2),
    imp_prestaciones numeric(6,2),
    imp_viaticos numeric(6,2),
    imp_subtotalcandidato numeric(6,2),
    imp_costoadmin numeric(6,2),
    cnu_financiamiento smallint,
    imp_isr numeric(6,2),
    imp_financiamiento numeric(6,2),
    imp_adicionales numeric(6,2),
    imp_subtotaladmin1 numeric(6,2),
    imp_comisiones numeric(6,2),
    imp_otrosgastos numeric(6,2),
    imp_subtotaladmin2 numeric(6,2),
    imp_total numeric(6,2),
    imp_iva numeric(6,2),
    por_utilidad numeric(4,2),
    imp_utilidad numeric(6,2),
    imp_tarifa numeric(6,2),
    aud_fecha_creacion date NOT NULL,
    aud_fecha_modificacion date,
    aud_cod_creadopor integer NOT NULL,
    aud_cod_modificadopor integer,
    imp_hospedaje numeric(6,2),
    imp_alimentacion numeric(6,2),
    imp_transporte numeric(6,2),
    imp_incentivos numeric(6,2),
    imp_eqcomputo numeric(6,2)
);


ALTER TABLE sisat.tsisatcandidatos OWNER TO suite;

--
-- TOC entry 410 (class 1259 OID 43474)
-- Name: tsisatcartaaceptacion; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisatcartaaceptacion (
    cod_aceptacion integer DEFAULT nextval('sisat.seq_aceptaciones'::regclass) NOT NULL,
    des_objetivo character varying(200) NOT NULL,
    txt_oferta text,
    des_esquema character varying(30) NOT NULL,
    tim_jornada time without time zone,
    txt_especificaciones text,
    aud_cod_creadopor integer NOT NULL,
    aud_cod_modificadopor integer,
    aud_fec_creacion date NOT NULL,
    aud_fec_modificacion date
);


ALTER TABLE sisat.tsisatcartaaceptacion OWNER TO suite;

--
-- TOC entry 411 (class 1259 OID 43481)
-- Name: tsisatcartaasignacion; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisatcartaasignacion (
    cod_asignacion integer DEFAULT nextval('sisat.seq_cartaasignaciones'::regclass) NOT NULL,
    cod_prospecto integer NOT NULL,
    cod_perfil integer NOT NULL,
    des_actividades character varying(200),
    des_lugarsalida character varying(100),
    des_lugarllegada character varying(100),
    fec_salida date,
    fec_llegada date,
    cod_transporte character varying(20),
    des_lugarhopedaje character varying(60),
    fec_hospedaje date,
    des_computadora character varying(150),
    cod_telefono character varying(16),
    des_accesorios character varying(150),
    des_nbresponsable character varying(50),
    des_nbpuesto character varying(50),
    des_lugarresp character varying(100),
    cod_telefonoresp character varying(16),
    tim_horario time without time zone,
    fec_iniciocontra date,
    fec_terminocontra date,
    imp_sueldomensual numeric(6,2),
    imp_nominaimss numeric(6,2),
    imp_honorarios numeric(6,2),
    imp_otros numeric(6,2),
    cod_rfc character varying(13),
    des_razonsocial character varying(45),
    des_correo character varying(50),
    cod_cpostal integer,
    des_direccionfact character varying(200),
    cod_cliente integer NOT NULL,
    cod_gpy integer,
    cod_rhta integer,
    cod_ape integer,
    cod_rys integer,
    des_observacion character varying(200) NOT NULL,
    aud_cod_creadopor integer NOT NULL,
    aud_cod_modificadopor integer,
    aud_fecha_creacion date NOT NULL,
    aud_fecha_modificacion date
);


ALTER TABLE sisat.tsisatcartaasignacion OWNER TO suite;

--
-- TOC entry 412 (class 1259 OID 43488)
-- Name: tsisatcomentcartaasignacion; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisatcomentcartaasignacion (
    cod_comentcartaasignacion integer NOT NULL,
    des_comentcartaasignacion character varying(300),
    bol_validacion boolean NOT NULL,
    aud_fecha_creacion date NOT NULL,
    aud_fecha_modificacion date,
    aud_cod_creadopor integer NOT NULL,
    aud_cod_modificadopor integer,
    cod_asignacion integer NOT NULL
);


ALTER TABLE sisat.tsisatcomentcartaasignacion OWNER TO suite;

--
-- TOC entry 413 (class 1259 OID 43491)
-- Name: tsisatcomentcosteo; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisatcomentcosteo (
    cod_comentcosteo integer NOT NULL,
    des_comentcosteo character varying(300),
    bol_validacion boolean NOT NULL,
    aud_fecha_creacion date NOT NULL,
    aud_fecha_modificacion date,
    aud_cod_creadopor integer NOT NULL,
    aud_cod_modificadopor integer,
    cod_candidato integer NOT NULL
);


ALTER TABLE sisat.tsisatcomentcosteo OWNER TO suite;

--
-- TOC entry 414 (class 1259 OID 43494)
-- Name: tsisatcomententrevista; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisatcomententrevista (
    cod_comententrevista integer NOT NULL,
    des_comententrevista character varying(500),
    aud_fecha_creacion date NOT NULL,
    aud_fecha_modificacion date,
    aud_cod_creadopor integer NOT NULL,
    aud_cod_modificadopor integer,
    cod_entrevista integer NOT NULL
);


ALTER TABLE sisat.tsisatcomententrevista OWNER TO suite;

--
-- TOC entry 415 (class 1259 OID 43500)
-- Name: tsisatcomentvacantes; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisatcomentvacantes (
    cod_comentvacante integer NOT NULL,
    des_comentvacante character varying(300),
    bol_validacion boolean NOT NULL,
    aud_fecha_creacion date NOT NULL,
    aud_fecha_modificacion date,
    aud_cod_creadopor integer NOT NULL,
    aud_cod_modificadopor integer,
    cod_vacante integer NOT NULL
);


ALTER TABLE sisat.tsisatcomentvacantes OWNER TO suite;

--
-- TOC entry 416 (class 1259 OID 43503)
-- Name: tsisatcontrataciones; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisatcontrataciones (
    cod_contratacion integer DEFAULT nextval('sisat.seq_contrataciones'::regclass) NOT NULL,
    fec_inicio date,
    fec_termino date,
    des_esquema character varying(30),
    cod_salarioestmin numeric(6,2),
    cod_salarioestmax numeric(6,2),
    tim_jornada character varying(20),
    cod_prospecto integer,
    aud_cod_creadopor integer NOT NULL,
    aud_cod_modificadopor integer,
    aud_fec_creacion date NOT NULL,
    aud_fec_modificacion date
);


ALTER TABLE sisat.tsisatcontrataciones OWNER TO suite;

--
-- TOC entry 417 (class 1259 OID 43507)
-- Name: tsisatcotizaciones; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisatcotizaciones (
    cod_cotizacion integer NOT NULL,
    cod_ciudad integer,
    cod_estado integer,
    fec_fecha date NOT NULL,
    "des_nbcontacto " character varying(50) NOT NULL,
    cod_puesto integer,
    des_compania character varying(50) NOT NULL,
    des_nbservicio character varying(50) NOT NULL,
    cnu_cantidad smallint NOT NULL,
    txt_concepto text NOT NULL,
    imp_inversionhr numeric(6,2) NOT NULL,
    txt_condicionescomer text NOT NULL,
    des_nbatentamente character varying(60) NOT NULL,
    des_correoatentamente character varying(50) NOT NULL,
    aud_cod_creadopor integer NOT NULL,
    aud_cod_modificadopor integer,
    aud_fec_creacion date NOT NULL,
    aud_fec_modificacion date
);


ALTER TABLE sisat.tsisatcotizaciones OWNER TO suite;

--
-- TOC entry 418 (class 1259 OID 43513)
-- Name: tsisatcursosycertificados; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisatcursosycertificados (
    cod_curso integer DEFAULT nextval('sisat.seq_cursos'::regclass) NOT NULL,
    cod_prospecto integer NOT NULL,
    des_curso character varying(100) NOT NULL,
    des_institucion character varying(70) NOT NULL,
    fec_termino date NOT NULL,
    fec_inicio date NOT NULL
);


ALTER TABLE sisat.tsisatcursosycertificados OWNER TO suite;

--
-- TOC entry 419 (class 1259 OID 43517)
-- Name: tsisatentrevistas; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisatentrevistas (
    cod_entrevista integer DEFAULT nextval('sisat.seq_entrevistas'::regclass) NOT NULL,
    des_nbentrevistador character varying(90) NOT NULL,
    des_puesto character varying(50) NOT NULL,
    des_correoent character varying(40) NOT NULL,
    cod_telefonoent character varying(16) NOT NULL,
    des_direccionent character varying(200),
    tim_horarioent time without time zone,
    fec_fechaent date NOT NULL,
    comentarios character varying(700) NOT NULL,
    cod_prospecto integer NOT NULL,
    aud_cod_creadopor integer NOT NULL,
    aud_cod_modificadopor integer,
    aud_fec_creacion date NOT NULL,
    aud_fec_modificacion date
);


ALTER TABLE sisat.tsisatentrevistas OWNER TO suite;

--
-- TOC entry 420 (class 1259 OID 43524)
-- Name: tsisatenviocorreos; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisatenviocorreos (
    cod_envio integer DEFAULT nextval('sisat.seq_envios'::regclass) NOT NULL,
    des_destinatario character varying(90) NOT NULL,
    des_asunto character varying(50) NOT NULL,
    des_mensaje character varying(200) NOT NULL,
    bin_adjunto bytea,
    aud_cod_creadopor integer NOT NULL,
    aud_cod_modificadopor integer,
    aud_fec_creacion date NOT NULL,
    aud_fec_modificacion date
);


ALTER TABLE sisat.tsisatenviocorreos OWNER TO suite;

--
-- TOC entry 421 (class 1259 OID 43531)
-- Name: tsisatescolaridad; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisatescolaridad (
    cod_escolaridad integer DEFAULT nextval('sisat.seq_escolaridad'::regclass) NOT NULL,
    cod_prospecto integer NOT NULL,
    des_escolaridad character varying(45) NOT NULL,
    des_escuela character varying(70) NOT NULL,
    fec_inicio date NOT NULL,
    fec_termino date NOT NULL,
    cod_estatus character varying(20)
);


ALTER TABLE sisat.tsisatescolaridad OWNER TO suite;

--
-- TOC entry 422 (class 1259 OID 43535)
-- Name: tsisatexperienciaslaborales; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisatexperienciaslaborales (
    cod_experiencia integer DEFAULT nextval('sisat.seq_experiencias'::regclass) NOT NULL,
    cod_prospecto integer NOT NULL,
    des_empresa character varying(50) NOT NULL,
    des_puesto character varying(40) NOT NULL,
    fec_inicio date,
    fec_termino date,
    des_ubicacion character varying(70),
    txt_funciones text,
    des_nbcliente character varying(70),
    des_proyecto character varying(70),
    txt_logros character varying(300),
    txt_tecnologiasemple text NOT NULL
);


ALTER TABLE sisat.tsisatexperienciaslaborales OWNER TO suite;

--
-- TOC entry 423 (class 1259 OID 43542)
-- Name: tsisatfirmareqper; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisatfirmareqper (
    cod_firma integer DEFAULT nextval('sisat.seq_firmas'::regclass) NOT NULL,
    cod_solicita integer,
    cod_puestosolicita integer,
    cod_autoriza integer,
    cod_puestoautoriza integer,
    cod_vacante integer
);


ALTER TABLE sisat.tsisatfirmareqper OWNER TO suite;

--
-- TOC entry 424 (class 1259 OID 43546)
-- Name: tsisatframeworks; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisatframeworks (
    cod_framework integer NOT NULL,
    des_framework character varying(30) NOT NULL
);


ALTER TABLE sisat.tsisatframeworks OWNER TO suite;

--
-- TOC entry 425 (class 1259 OID 43549)
-- Name: tsisathabilidades; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisathabilidades (
    cod_habilidad integer DEFAULT nextval('sisat.seq_habilidades'::regclass) NOT NULL,
    cod_prospecto integer NOT NULL,
    des_habilidad character varying(50),
    des_dominio character varying(50)
);


ALTER TABLE sisat.tsisathabilidades OWNER TO suite;

--
-- TOC entry 426 (class 1259 OID 43553)
-- Name: tsisatherramientas; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisatherramientas (
    cod_herramientas integer NOT NULL,
    json_tecnologia json,
    cod_prospecto integer NOT NULL,
    des_nivel character varying(30) NOT NULL,
    cnu_experiencia numeric(5,1) NOT NULL
);


ALTER TABLE sisat.tsisatherramientas OWNER TO suite;

--
-- TOC entry 427 (class 1259 OID 43559)
-- Name: tsisatherramientas_cod_herramientas_seq; Type: SEQUENCE; Schema: sisat; Owner: suite
--

CREATE SEQUENCE sisat.tsisatherramientas_cod_herramientas_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sisat.tsisatherramientas_cod_herramientas_seq OWNER TO suite;

--
-- TOC entry 3913 (class 0 OID 0)
-- Dependencies: 427
-- Name: tsisatherramientas_cod_herramientas_seq; Type: SEQUENCE OWNED BY; Schema: sisat; Owner: suite
--

ALTER SEQUENCE sisat.tsisatherramientas_cod_herramientas_seq OWNED BY sisat.tsisatherramientas.cod_herramientas;


--
-- TOC entry 428 (class 1259 OID 43561)
-- Name: tsisatides; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisatides (
    cod_ide integer NOT NULL,
    des_ide character varying(30) NOT NULL
);


ALTER TABLE sisat.tsisatides OWNER TO suite;

--
-- TOC entry 429 (class 1259 OID 43564)
-- Name: tsisatidiomas; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisatidiomas (
    cod_idioma integer DEFAULT nextval('sisat.seq_idiomas'::regclass) NOT NULL,
    cod_nbidioma character varying(20) NOT NULL
);


ALTER TABLE sisat.tsisatidiomas OWNER TO suite;

--
-- TOC entry 430 (class 1259 OID 43568)
-- Name: tsisatlenguajes; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisatlenguajes (
    cod_lenguaje integer NOT NULL,
    des_lenguaje character varying(30) NOT NULL
);


ALTER TABLE sisat.tsisatlenguajes OWNER TO suite;

--
-- TOC entry 431 (class 1259 OID 43571)
-- Name: tsisatmaquetados; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisatmaquetados (
    cod_maquetado integer NOT NULL,
    des_maquetado character varying(50) NOT NULL
);


ALTER TABLE sisat.tsisatmaquetados OWNER TO suite;

--
-- TOC entry 432 (class 1259 OID 43574)
-- Name: tsisatmetodologias; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisatmetodologias (
    cod_metodologia integer NOT NULL,
    des_metodologia character varying(30) NOT NULL
);


ALTER TABLE sisat.tsisatmetodologias OWNER TO suite;

--
-- TOC entry 433 (class 1259 OID 43577)
-- Name: tsisatmodelados; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisatmodelados (
    cod_modelado integer NOT NULL,
    des_modelado character varying(30) NOT NULL
);


ALTER TABLE sisat.tsisatmodelados OWNER TO suite;

--
-- TOC entry 434 (class 1259 OID 43580)
-- Name: tsisatordenservicio; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisatordenservicio (
    cod_ordenservicio integer DEFAULT nextval('sisat.seq_ordenservicios'::regclass) NOT NULL,
    cod_estadorep integer NOT NULL,
    cod_ciudad integer NOT NULL,
    fec_fecha date NOT NULL,
    des_nbcontacto character varying(50),
    cod_puesto integer,
    des_nbcompania character varying(50),
    des_nbservicio character varying(60),
    cnu_cantidad smallint,
    txt_concepto text,
    imp_inversionhr numeric(6,2),
    txt_condicionescomer text,
    des_ubcnconsultor character varying(100),
    fec_finservicio date,
    cod_gpy integer NOT NULL,
    des_correogpy character varying(50) NOT NULL,
    cod_cliente integer NOT NULL,
    des_correoclte character varying(50) NOT NULL,
    des_empresaclte character varying(50),
    aud_cod_creadopor integer NOT NULL,
    aud_cod_modificadopor integer NOT NULL,
    aud_fec_creacion date NOT NULL,
    aud_fec_modificacion date NOT NULL
);


ALTER TABLE sisat.tsisatordenservicio OWNER TO suite;

--
-- TOC entry 435 (class 1259 OID 43587)
-- Name: tsisatpatrones; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisatpatrones (
    cod_patron integer NOT NULL,
    des_patron character varying(30) NOT NULL
);


ALTER TABLE sisat.tsisatpatrones OWNER TO suite;

--
-- TOC entry 436 (class 1259 OID 43590)
-- Name: tsisatprospectos; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisatprospectos (
    cod_prospecto integer DEFAULT nextval('sisat.seq_prospectos'::regclass) NOT NULL,
    des_nombre character varying(45) NOT NULL,
    des_nombres character varying(60),
    des_appaterno character varying(40) NOT NULL,
    des_apmaterno character varying(40),
    des_lugarnacimiento character varying(50) NOT NULL,
    fec_nacimiento date NOT NULL,
    cod_edad integer NOT NULL,
    cod_edocivil character varying(15) NOT NULL,
    des_nbpadre character varying(70),
    des_nbmadre character varying(70),
    cod_numhermanos integer,
    des_nbcalle character varying(60),
    cod_numcasa integer,
    des_colonia character varying(60),
    des_localidad character varying(60),
    des_municipio character varying(60),
    des_estado character varying(60),
    cod_cpostal integer,
    cod_tiposangre character varying(5),
    des_emailmbn character varying(40),
    des_emailpersonal character varying(40),
    des_pasatiempo character varying(200),
    cod_telefonocasa character varying(16) DEFAULT NULL::character varying,
    cod_telefonomovil character varying(16) DEFAULT NULL::character varying,
    cod_rfc character varying(13),
    cod_nss character varying(20),
    cod_curp character varying(18) NOT NULL,
    des_nacionalidad character varying(30),
    cod_administrador integer,
    fec_fechacoment date,
    txt_comentarios text,
    bin_foto bytea,
    cod_tipofoto character varying(30),
    cod_extensionfoto character varying(5),
    des_puestovacante character varying(50),
    anio_experiencia integer,
    aud_cod_creadopor integer NOT NULL,
    aud_cod_modificadopor integer,
    aud_fec_creacion date NOT NULL,
    aud_fec_modifiacion date
);


ALTER TABLE sisat.tsisatprospectos OWNER TO suite;

--
-- TOC entry 437 (class 1259 OID 43599)
-- Name: tsisatprospectos_idiomas; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisatprospectos_idiomas (
    cod_pros_idoma integer NOT NULL,
    cod_prospecto integer,
    cod_idioma integer,
    cod_nivel character varying(20),
    des_certificado character varying(40)
);


ALTER TABLE sisat.tsisatprospectos_idiomas OWNER TO suite;

--
-- TOC entry 438 (class 1259 OID 43602)
-- Name: tsisatprotocolos; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisatprotocolos (
    cod_protocolo integer NOT NULL,
    des_protocolo character varying(30) NOT NULL
);


ALTER TABLE sisat.tsisatprotocolos OWNER TO suite;

--
-- TOC entry 439 (class 1259 OID 43605)
-- Name: tsisatproyectos; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisatproyectos (
    cod_proyecto integer DEFAULT nextval('sisat.seq_proyectos'::regclass) NOT NULL,
    cod_prospecto integer NOT NULL,
    cod_perfil integer NOT NULL,
    des_nbcliente character varying(50) NOT NULL,
    des_nbresponsable character varying(50) NOT NULL,
    des_correo character varying(50) NOT NULL,
    cod_telefono character varying(16) NOT NULL,
    des_direccion character varying(200) NOT NULL,
    aud_cod_creadopor integer NOT NULL,
    aud_cod_modificadopor integer,
    aud_fec_creacion date NOT NULL,
    aud_fec_modificacion date
);


ALTER TABLE sisat.tsisatproyectos OWNER TO suite;

--
-- TOC entry 440 (class 1259 OID 43609)
-- Name: tsisatqa; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisatqa (
    cod_qa integer NOT NULL,
    des_qa character varying(30) NOT NULL
);


ALTER TABLE sisat.tsisatqa OWNER TO suite;

--
-- TOC entry 441 (class 1259 OID 43612)
-- Name: tsisatrepositoriolibrerias; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisatrepositoriolibrerias (
    cod_repositoriolibreria integer NOT NULL,
    des_repositoriolibreria character varying(30) NOT NULL
);


ALTER TABLE sisat.tsisatrepositoriolibrerias OWNER TO suite;

--
-- TOC entry 442 (class 1259 OID 43615)
-- Name: tsisatrepositorios; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisatrepositorios (
    cod_repositorio integer NOT NULL,
    des_repositorio character varying(30) NOT NULL
);


ALTER TABLE sisat.tsisatrepositorios OWNER TO suite;

--
-- TOC entry 443 (class 1259 OID 43618)
-- Name: tsisatsgbd; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisatsgbd (
    cod_sgbd integer NOT NULL,
    des_sgbd character varying(30) NOT NULL
);


ALTER TABLE sisat.tsisatsgbd OWNER TO suite;

--
-- TOC entry 444 (class 1259 OID 43621)
-- Name: tsisatso; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisatso (
    cod_so integer NOT NULL,
    des_so character varying(30) NOT NULL
);


ALTER TABLE sisat.tsisatso OWNER TO suite;

--
-- TOC entry 445 (class 1259 OID 43624)
-- Name: tsisatvacantes; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisatvacantes (
    cod_vacante integer DEFAULT nextval('sisat.seq_vacantes'::regclass) NOT NULL,
    des_rqvacante character varying(200) NOT NULL,
    cnu_anexperiencia smallint,
    txt_experiencia text,
    des_escolaridad character varying(50),
    txt_herramientas text,
    txt_habilidades text,
    des_lugartrabajo character varying(100),
    imp_sueldo numeric(6,2),
    cod_idioma integer,
    sexo character varying(11) NOT NULL,
    fec_solicitud date NOT NULL,
    fec_entrega date,
    cod_cliente integer NOT NULL,
    aud_cod_creadopor integer NOT NULL,
    aud_cod_modificadopor integer,
    aud_fec_creacion date NOT NULL,
    aud_fec_modificacion date,
    cod_contratacion integer NOT NULL,
    statusvacante boolean,
    txt_conocimientostecno text,
    cod_nivelidioma character varying(20)
);


ALTER TABLE sisat.tsisatvacantes OWNER TO suite;

--
-- TOC entry 3507 (class 2604 OID 43631)
-- Name: tsisatherramientas cod_herramientas; Type: DEFAULT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatherramientas ALTER COLUMN cod_herramientas SET DEFAULT nextval('sisat.tsisatherramientas_cod_herramientas_seq'::regclass);


--
-- TOC entry 3822 (class 0 OID 43460)
-- Dependencies: 406
-- Data for Name: tsisatappservices; Type: TABLE DATA; Schema: sisat; Owner: suite
--



--
-- TOC entry 3823 (class 0 OID 43463)
-- Dependencies: 407
-- Data for Name: tsisatarquitecturas; Type: TABLE DATA; Schema: sisat; Owner: suite
--



--
-- TOC entry 3824 (class 0 OID 43466)
-- Dependencies: 408
-- Data for Name: tsisatasignaciones; Type: TABLE DATA; Schema: sisat; Owner: suite
--



--
-- TOC entry 3825 (class 0 OID 43470)
-- Dependencies: 409
-- Data for Name: tsisatcandidatos; Type: TABLE DATA; Schema: sisat; Owner: suite
--



--
-- TOC entry 3826 (class 0 OID 43474)
-- Dependencies: 410
-- Data for Name: tsisatcartaaceptacion; Type: TABLE DATA; Schema: sisat; Owner: suite
--



--
-- TOC entry 3827 (class 0 OID 43481)
-- Dependencies: 411
-- Data for Name: tsisatcartaasignacion; Type: TABLE DATA; Schema: sisat; Owner: suite
--

INSERT INTO sisat.tsisatcartaasignacion (cod_asignacion, cod_prospecto, cod_perfil, des_actividades, des_lugarsalida, des_lugarllegada, fec_salida, fec_llegada, cod_transporte, des_lugarhopedaje, fec_hospedaje, des_computadora, cod_telefono, des_accesorios, des_nbresponsable, des_nbpuesto, des_lugarresp, cod_telefonoresp, tim_horario, fec_iniciocontra, fec_terminocontra, imp_sueldomensual, imp_nominaimss, imp_honorarios, imp_otros, cod_rfc, des_razonsocial, des_correo, cod_cpostal, des_direccionfact, cod_cliente, cod_gpy, cod_rhta, cod_ape, cod_rys, des_observacion, aud_cod_creadopor, aud_cod_modificadopor, aud_fecha_creacion, aud_fecha_modificacion) VALUES (1, 1, 1, 'Desarrollar', 'Apizaco', 'Tlaxcala', '2019-02-23', '2019-02-23', 'Colectivo', 'hotel', '2019-02-23', 'MBN', 'MBN', 'MBN', 'Pedro Perez', 'Responsable', 'Tlaxcala', '4343535598', '08:00:00', '2019-02-23', '2019-09-23', 1600.00, 1000.00, 900.00, 100.00, 'BFCOBJVJKVBK', 'PedritosPedraza', 'jdskhvcks@gmail.com', 56980, 'Avenida Flores Vagon', 1, 16, 10, 14, 15, 'Sin Observacion', 10, 10, '2019-02-23', '2019-02-23');
INSERT INTO sisat.tsisatcartaasignacion (cod_asignacion, cod_prospecto, cod_perfil, des_actividades, des_lugarsalida, des_lugarllegada, fec_salida, fec_llegada, cod_transporte, des_lugarhopedaje, fec_hospedaje, des_computadora, cod_telefono, des_accesorios, des_nbresponsable, des_nbpuesto, des_lugarresp, cod_telefonoresp, tim_horario, fec_iniciocontra, fec_terminocontra, imp_sueldomensual, imp_nominaimss, imp_honorarios, imp_otros, cod_rfc, des_razonsocial, des_correo, cod_cpostal, des_direccionfact, cod_cliente, cod_gpy, cod_rhta, cod_ape, cod_rys, des_observacion, aud_cod_creadopor, aud_cod_modificadopor, aud_fecha_creacion, aud_fecha_modificacion) VALUES (2, 1, 2, 'Desarrollo Web', 'Tlaxcala', 'CD. de Mexico', '2019-06-07', '2019-06-07', 'Publico', 'Renta de casa', '2019-06-07', 'Cliente', 'Propio', 'Propio', 'Ramiro Montez', 'Responsable de desarrollo', 'CD. de Mexico', '6789327689', '08:00:00', '2019-06-07', '2019-06-07', 1200.00, 1000.00, 500.00, 0.00, 'FNCKCCKFHVNC', 'PATITOS', 'patitoslover@gmail.com', 67890, 'calle nicaragua', 3, 16, 10, 14, 15, 'sin ', 10, 10, '2019-06-07', '2019-06-07');
INSERT INTO sisat.tsisatcartaasignacion (cod_asignacion, cod_prospecto, cod_perfil, des_actividades, des_lugarsalida, des_lugarllegada, fec_salida, fec_llegada, cod_transporte, des_lugarhopedaje, fec_hospedaje, des_computadora, cod_telefono, des_accesorios, des_nbresponsable, des_nbpuesto, des_lugarresp, cod_telefonoresp, tim_horario, fec_iniciocontra, fec_terminocontra, imp_sueldomensual, imp_nominaimss, imp_honorarios, imp_otros, cod_rfc, des_razonsocial, des_correo, cod_cpostal, des_direccionfact, cod_cliente, cod_gpy, cod_rhta, cod_ape, cod_rys, des_observacion, aud_cod_creadopor, aud_cod_modificadopor, aud_fecha_creacion, aud_fecha_modificacion) VALUES (10, 1, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, 16, 10, 14, 15, '7', 10, NULL, '2019-11-06', NULL);
INSERT INTO sisat.tsisatcartaasignacion (cod_asignacion, cod_prospecto, cod_perfil, des_actividades, des_lugarsalida, des_lugarllegada, fec_salida, fec_llegada, cod_transporte, des_lugarhopedaje, fec_hospedaje, des_computadora, cod_telefono, des_accesorios, des_nbresponsable, des_nbpuesto, des_lugarresp, cod_telefonoresp, tim_horario, fec_iniciocontra, fec_terminocontra, imp_sueldomensual, imp_nominaimss, imp_honorarios, imp_otros, cod_rfc, des_razonsocial, des_correo, cod_cpostal, des_direccionfact, cod_cliente, cod_gpy, cod_rhta, cod_ape, cod_rys, des_observacion, aud_cod_creadopor, aud_cod_modificadopor, aud_fecha_creacion, aud_fecha_modificacion) VALUES (11, 1, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, 16, 10, 14, 15, '7', 10, NULL, '2019-11-06', NULL);
INSERT INTO sisat.tsisatcartaasignacion (cod_asignacion, cod_prospecto, cod_perfil, des_actividades, des_lugarsalida, des_lugarllegada, fec_salida, fec_llegada, cod_transporte, des_lugarhopedaje, fec_hospedaje, des_computadora, cod_telefono, des_accesorios, des_nbresponsable, des_nbpuesto, des_lugarresp, cod_telefonoresp, tim_horario, fec_iniciocontra, fec_terminocontra, imp_sueldomensual, imp_nominaimss, imp_honorarios, imp_otros, cod_rfc, des_razonsocial, des_correo, cod_cpostal, des_direccionfact, cod_cliente, cod_gpy, cod_rhta, cod_ape, cod_rys, des_observacion, aud_cod_creadopor, aud_cod_modificadopor, aud_fecha_creacion, aud_fecha_modificacion) VALUES (12, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, 16, 10, 14, 15, '7', 10, NULL, '2019-11-06', NULL);
INSERT INTO sisat.tsisatcartaasignacion (cod_asignacion, cod_prospecto, cod_perfil, des_actividades, des_lugarsalida, des_lugarllegada, fec_salida, fec_llegada, cod_transporte, des_lugarhopedaje, fec_hospedaje, des_computadora, cod_telefono, des_accesorios, des_nbresponsable, des_nbpuesto, des_lugarresp, cod_telefonoresp, tim_horario, fec_iniciocontra, fec_terminocontra, imp_sueldomensual, imp_nominaimss, imp_honorarios, imp_otros, cod_rfc, des_razonsocial, des_correo, cod_cpostal, des_direccionfact, cod_cliente, cod_gpy, cod_rhta, cod_ape, cod_rys, des_observacion, aud_cod_creadopor, aud_cod_modificadopor, aud_fecha_creacion, aud_fecha_modificacion) VALUES (13, 1, 3, NULL, 'hhj', 'vhvjhv', '2019-11-13', '2019-11-13', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 600.00, 500.00, 400.00, 100.00, NULL, NULL, NULL, NULL, NULL, 2, 16, 10, 14, 15, '8', 10, NULL, '2019-11-06', NULL);
INSERT INTO sisat.tsisatcartaasignacion (cod_asignacion, cod_prospecto, cod_perfil, des_actividades, des_lugarsalida, des_lugarllegada, fec_salida, fec_llegada, cod_transporte, des_lugarhopedaje, fec_hospedaje, des_computadora, cod_telefono, des_accesorios, des_nbresponsable, des_nbpuesto, des_lugarresp, cod_telefonoresp, tim_horario, fec_iniciocontra, fec_terminocontra, imp_sueldomensual, imp_nominaimss, imp_honorarios, imp_otros, cod_rfc, des_razonsocial, des_correo, cod_cpostal, des_direccionfact, cod_cliente, cod_gpy, cod_rhta, cod_ape, cod_rys, des_observacion, aud_cod_creadopor, aud_cod_modificadopor, aud_fecha_creacion, aud_fecha_modificacion) VALUES (14, 1, 3, 'Pruebas de java', 'San Pablo', 'Monte', '2019-11-14', '2019-11-21', 'Colectivo', 'Hotel', NULL, 'MBN', 'MBN', 'Propio', 'Pablo Del Monte', 'Gerente', 'Calle Monte', '(999)999-99-99', '08:00:00', NULL, NULL, 600.00, 500.00, 400.00, 100.00, 'YTFG789NOY65F', 'PabloMix', 'juanpablito@gmail.com', 89990, 'Calle Buena Vista', 2, 16, 10, 14, 15, '2', 10, NULL, '2019-11-06', NULL);
INSERT INTO sisat.tsisatcartaasignacion (cod_asignacion, cod_prospecto, cod_perfil, des_actividades, des_lugarsalida, des_lugarllegada, fec_salida, fec_llegada, cod_transporte, des_lugarhopedaje, fec_hospedaje, des_computadora, cod_telefono, des_accesorios, des_nbresponsable, des_nbpuesto, des_lugarresp, cod_telefonoresp, tim_horario, fec_iniciocontra, fec_terminocontra, imp_sueldomensual, imp_nominaimss, imp_honorarios, imp_otros, cod_rfc, des_razonsocial, des_correo, cod_cpostal, des_direccionfact, cod_cliente, cod_gpy, cod_rhta, cod_ape, cod_rys, des_observacion, aud_cod_creadopor, aud_cod_modificadopor, aud_fecha_creacion, aud_fecha_modificacion) VALUES (15, 1, 3, 'DesarrolloWeb', 'Apizaco', 'Xaloztoc', '2019-11-14', '2019-10-23', 'Colectivo', 'Hotel', NULL, 'Empresa', 'Empresa', 'Empresa', 'Pablo Del Monte', 'Gerente', 'Apizaco', '(999)999-99-99', '08:00:00', NULL, NULL, 1200.00, 500.00, 400.00, 100.00, 'YTFG789NOY65F', 'PabloMix', 'juanpablito@gmail.com', 56778, 'Calle Buena Vista', 4, 16, 10, 14, 15, '2', 10, NULL, '2019-11-06', NULL);
INSERT INTO sisat.tsisatcartaasignacion (cod_asignacion, cod_prospecto, cod_perfil, des_actividades, des_lugarsalida, des_lugarllegada, fec_salida, fec_llegada, cod_transporte, des_lugarhopedaje, fec_hospedaje, des_computadora, cod_telefono, des_accesorios, des_nbresponsable, des_nbpuesto, des_lugarresp, cod_telefonoresp, tim_horario, fec_iniciocontra, fec_terminocontra, imp_sueldomensual, imp_nominaimss, imp_honorarios, imp_otros, cod_rfc, des_razonsocial, des_correo, cod_cpostal, des_direccionfact, cod_cliente, cod_gpy, cod_rhta, cod_ape, cod_rys, des_observacion, aud_cod_creadopor, aud_cod_modificadopor, aud_fecha_creacion, aud_fecha_modificacion) VALUES (16, 1, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Propio', 'Propio', 'Propio', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, 16, 10, 14, 15, '2', 10, NULL, '2019-11-06', NULL);
INSERT INTO sisat.tsisatcartaasignacion (cod_asignacion, cod_prospecto, cod_perfil, des_actividades, des_lugarsalida, des_lugarllegada, fec_salida, fec_llegada, cod_transporte, des_lugarhopedaje, fec_hospedaje, des_computadora, cod_telefono, des_accesorios, des_nbresponsable, des_nbpuesto, des_lugarresp, cod_telefonoresp, tim_horario, fec_iniciocontra, fec_terminocontra, imp_sueldomensual, imp_nominaimss, imp_honorarios, imp_otros, cod_rfc, des_razonsocial, des_correo, cod_cpostal, des_direccionfact, cod_cliente, cod_gpy, cod_rhta, cod_ape, cod_rys, des_observacion, aud_cod_creadopor, aud_cod_modificadopor, aud_fecha_creacion, aud_fecha_modificacion) VALUES (17, 1, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Propio', 'Propio', 'Propio', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, 16, 10, 14, 15, '2', 10, NULL, '2019-11-06', NULL);
INSERT INTO sisat.tsisatcartaasignacion (cod_asignacion, cod_prospecto, cod_perfil, des_actividades, des_lugarsalida, des_lugarllegada, fec_salida, fec_llegada, cod_transporte, des_lugarhopedaje, fec_hospedaje, des_computadora, cod_telefono, des_accesorios, des_nbresponsable, des_nbpuesto, des_lugarresp, cod_telefonoresp, tim_horario, fec_iniciocontra, fec_terminocontra, imp_sueldomensual, imp_nominaimss, imp_honorarios, imp_otros, cod_rfc, des_razonsocial, des_correo, cod_cpostal, des_direccionfact, cod_cliente, cod_gpy, cod_rhta, cod_ape, cod_rys, des_observacion, aud_cod_creadopor, aud_cod_modificadopor, aud_fecha_creacion, aud_fecha_modificacion) VALUES (18, 1, 3, 'DesarrolloWeb', 'Apizaco', 'Xaloztoc', '2019-11-15', '2019-11-15', 'Colectivo', 'Hotel', NULL, 'Propio', 'Empresa', 'Propio', 'Pablo Del Monte', 'Gerente', 'Apizaco', '(999)999-99-99', '08:00:00', NULL, NULL, 600.00, 500.00, 100.00, 0.00, 'YTFG789NOY65F', 'PabloMix', 'juanpablito@gmail.com', 89990, 'Calle Buena Vista', 4, 16, 10, 14, 15, '2', 10, NULL, '2019-11-06', NULL);


--
-- TOC entry 3828 (class 0 OID 43488)
-- Dependencies: 412
-- Data for Name: tsisatcomentcartaasignacion; Type: TABLE DATA; Schema: sisat; Owner: suite
--



--
-- TOC entry 3829 (class 0 OID 43491)
-- Dependencies: 413
-- Data for Name: tsisatcomentcosteo; Type: TABLE DATA; Schema: sisat; Owner: suite
--



--
-- TOC entry 3830 (class 0 OID 43494)
-- Dependencies: 414
-- Data for Name: tsisatcomententrevista; Type: TABLE DATA; Schema: sisat; Owner: suite
--



--
-- TOC entry 3831 (class 0 OID 43500)
-- Dependencies: 415
-- Data for Name: tsisatcomentvacantes; Type: TABLE DATA; Schema: sisat; Owner: suite
--



--
-- TOC entry 3832 (class 0 OID 43503)
-- Dependencies: 416
-- Data for Name: tsisatcontrataciones; Type: TABLE DATA; Schema: sisat; Owner: suite
--



--
-- TOC entry 3833 (class 0 OID 43507)
-- Dependencies: 417
-- Data for Name: tsisatcotizaciones; Type: TABLE DATA; Schema: sisat; Owner: suite
--



--
-- TOC entry 3834 (class 0 OID 43513)
-- Dependencies: 418
-- Data for Name: tsisatcursosycertificados; Type: TABLE DATA; Schema: sisat; Owner: suite
--



--
-- TOC entry 3835 (class 0 OID 43517)
-- Dependencies: 419
-- Data for Name: tsisatentrevistas; Type: TABLE DATA; Schema: sisat; Owner: suite
--



--
-- TOC entry 3836 (class 0 OID 43524)
-- Dependencies: 420
-- Data for Name: tsisatenviocorreos; Type: TABLE DATA; Schema: sisat; Owner: suite
--



--
-- TOC entry 3837 (class 0 OID 43531)
-- Dependencies: 421
-- Data for Name: tsisatescolaridad; Type: TABLE DATA; Schema: sisat; Owner: suite
--



--
-- TOC entry 3838 (class 0 OID 43535)
-- Dependencies: 422
-- Data for Name: tsisatexperienciaslaborales; Type: TABLE DATA; Schema: sisat; Owner: suite
--



--
-- TOC entry 3839 (class 0 OID 43542)
-- Dependencies: 423
-- Data for Name: tsisatfirmareqper; Type: TABLE DATA; Schema: sisat; Owner: suite
--



--
-- TOC entry 3840 (class 0 OID 43546)
-- Dependencies: 424
-- Data for Name: tsisatframeworks; Type: TABLE DATA; Schema: sisat; Owner: suite
--



--
-- TOC entry 3841 (class 0 OID 43549)
-- Dependencies: 425
-- Data for Name: tsisathabilidades; Type: TABLE DATA; Schema: sisat; Owner: suite
--



--
-- TOC entry 3842 (class 0 OID 43553)
-- Dependencies: 426
-- Data for Name: tsisatherramientas; Type: TABLE DATA; Schema: sisat; Owner: suite
--



--
-- TOC entry 3844 (class 0 OID 43561)
-- Dependencies: 428
-- Data for Name: tsisatides; Type: TABLE DATA; Schema: sisat; Owner: suite
--



--
-- TOC entry 3845 (class 0 OID 43564)
-- Dependencies: 429
-- Data for Name: tsisatidiomas; Type: TABLE DATA; Schema: sisat; Owner: suite
--

INSERT INTO sisat.tsisatidiomas (cod_idioma, cod_nbidioma) VALUES (1, 'INGLES');
INSERT INTO sisat.tsisatidiomas (cod_idioma, cod_nbidioma) VALUES (2, 'ALEMAN');
INSERT INTO sisat.tsisatidiomas (cod_idioma, cod_nbidioma) VALUES (3, 'PORTUGUES');


--
-- TOC entry 3846 (class 0 OID 43568)
-- Dependencies: 430
-- Data for Name: tsisatlenguajes; Type: TABLE DATA; Schema: sisat; Owner: suite
--



--
-- TOC entry 3847 (class 0 OID 43571)
-- Dependencies: 431
-- Data for Name: tsisatmaquetados; Type: TABLE DATA; Schema: sisat; Owner: suite
--



--
-- TOC entry 3848 (class 0 OID 43574)
-- Dependencies: 432
-- Data for Name: tsisatmetodologias; Type: TABLE DATA; Schema: sisat; Owner: suite
--



--
-- TOC entry 3849 (class 0 OID 43577)
-- Dependencies: 433
-- Data for Name: tsisatmodelados; Type: TABLE DATA; Schema: sisat; Owner: suite
--



--
-- TOC entry 3850 (class 0 OID 43580)
-- Dependencies: 434
-- Data for Name: tsisatordenservicio; Type: TABLE DATA; Schema: sisat; Owner: suite
--



--
-- TOC entry 3851 (class 0 OID 43587)
-- Dependencies: 435
-- Data for Name: tsisatpatrones; Type: TABLE DATA; Schema: sisat; Owner: suite
--



--
-- TOC entry 3852 (class 0 OID 43590)
-- Dependencies: 436
-- Data for Name: tsisatprospectos; Type: TABLE DATA; Schema: sisat; Owner: suite
--

INSERT INTO sisat.tsisatprospectos (cod_prospecto, des_nombre, des_nombres, des_appaterno, des_apmaterno, des_lugarnacimiento, fec_nacimiento, cod_edad, cod_edocivil, des_nbpadre, des_nbmadre, cod_numhermanos, des_nbcalle, cod_numcasa, des_colonia, des_localidad, des_municipio, des_estado, cod_cpostal, cod_tiposangre, des_emailmbn, des_emailpersonal, des_pasatiempo, cod_telefonocasa, cod_telefonomovil, cod_rfc, cod_nss, cod_curp, des_nacionalidad, cod_administrador, fec_fechacoment, txt_comentarios, bin_foto, cod_tipofoto, cod_extensionfoto, des_puestovacante, anio_experiencia, aud_cod_creadopor, aud_cod_modificadopor, aud_fec_creacion, aud_fec_modifiacion) VALUES (1, 'Juan ', 'Pablo', 'Venegas', 'Sanchez', 'Tlaxcala', '1996-05-09', 23, 'Soltero', 'Pedrito', 'Juanita', 2, 'Francisco I Madero', 50, 'Venustiano', 'Xaloztoc', 'Xaloztoc', 'Tlaxcala', 23578, 'O+', 'pablito.mbn@gmail.com', 'juanpablito@gmail.com', 'gamer', '54543', '4353678903', 'BBDFKGKDFJBVK', '5464647', 'LJFEVBKJBJF', 'Mexicano', 1, '2019-02-23', 'ususkihdkbl', NULL, 'JPG', 'JPG', 'Desarrollador', 1, 10, 10, '2019-02-23', '2019-02-23');
INSERT INTO sisat.tsisatprospectos (cod_prospecto, des_nombre, des_nombres, des_appaterno, des_apmaterno, des_lugarnacimiento, fec_nacimiento, cod_edad, cod_edocivil, des_nbpadre, des_nbmadre, cod_numhermanos, des_nbcalle, cod_numcasa, des_colonia, des_localidad, des_municipio, des_estado, cod_cpostal, cod_tiposangre, des_emailmbn, des_emailpersonal, des_pasatiempo, cod_telefonocasa, cod_telefonomovil, cod_rfc, cod_nss, cod_curp, des_nacionalidad, cod_administrador, fec_fechacoment, txt_comentarios, bin_foto, cod_tipofoto, cod_extensionfoto, des_puestovacante, anio_experiencia, aud_cod_creadopor, aud_cod_modificadopor, aud_fec_creacion, aud_fec_modifiacion) VALUES (2, 'Fernando ', ' ', 'Morales', 'Mozo', 'Tlaxcala', '1996-05-30', 23, 'Soltero', 'Pedrito', 'Juanita', 2, 'Francisco I Madero', 50, 'Venustiano', 'Xaloztoc', 'Xaloztoc', 'Tlaxcala', 23578, 'O+', 'pablito.mbn@gmail.com', 'juanpablito@gmail.com', 'gamer', '54543', '4353678903', 'BBDFKGKDFJBVK', '5464647', 'LJFEVBKJBJF', 'Mexicano', 1, '2019-02-23', 'ususkihdkbl', NULL, 'JPG', 'JPG', 'Desarrollador', 1, 10, 10, '2019-02-23', '2019-02-23');


--
-- TOC entry 3853 (class 0 OID 43599)
-- Dependencies: 437
-- Data for Name: tsisatprospectos_idiomas; Type: TABLE DATA; Schema: sisat; Owner: suite
--



--
-- TOC entry 3854 (class 0 OID 43602)
-- Dependencies: 438
-- Data for Name: tsisatprotocolos; Type: TABLE DATA; Schema: sisat; Owner: suite
--



--
-- TOC entry 3855 (class 0 OID 43605)
-- Dependencies: 439
-- Data for Name: tsisatproyectos; Type: TABLE DATA; Schema: sisat; Owner: suite
--



--
-- TOC entry 3856 (class 0 OID 43609)
-- Dependencies: 440
-- Data for Name: tsisatqa; Type: TABLE DATA; Schema: sisat; Owner: suite
--



--
-- TOC entry 3857 (class 0 OID 43612)
-- Dependencies: 441
-- Data for Name: tsisatrepositoriolibrerias; Type: TABLE DATA; Schema: sisat; Owner: suite
--



--
-- TOC entry 3858 (class 0 OID 43615)
-- Dependencies: 442
-- Data for Name: tsisatrepositorios; Type: TABLE DATA; Schema: sisat; Owner: suite
--



--
-- TOC entry 3859 (class 0 OID 43618)
-- Dependencies: 443
-- Data for Name: tsisatsgbd; Type: TABLE DATA; Schema: sisat; Owner: suite
--



--
-- TOC entry 3860 (class 0 OID 43621)
-- Dependencies: 444
-- Data for Name: tsisatso; Type: TABLE DATA; Schema: sisat; Owner: suite
--



--
-- TOC entry 3861 (class 0 OID 43624)
-- Dependencies: 445
-- Data for Name: tsisatvacantes; Type: TABLE DATA; Schema: sisat; Owner: suite
--



--
-- TOC entry 3933 (class 0 OID 0)
-- Dependencies: 383
-- Name: seq_aceptaciones; Type: SEQUENCE SET; Schema: sisat; Owner: suite
--

SELECT pg_catalog.setval('sisat.seq_aceptaciones', 1, false);


--
-- TOC entry 3934 (class 0 OID 0)
-- Dependencies: 384
-- Name: seq_asignaciones; Type: SEQUENCE SET; Schema: sisat; Owner: suite
--

SELECT pg_catalog.setval('sisat.seq_asignaciones', 1, false);


--
-- TOC entry 3935 (class 0 OID 0)
-- Dependencies: 385
-- Name: seq_candidatos; Type: SEQUENCE SET; Schema: sisat; Owner: suite
--

SELECT pg_catalog.setval('sisat.seq_candidatos', 1, false);


--
-- TOC entry 3936 (class 0 OID 0)
-- Dependencies: 386
-- Name: seq_cartaasignaciones; Type: SEQUENCE SET; Schema: sisat; Owner: suite
--

SELECT pg_catalog.setval('sisat.seq_cartaasignaciones', 1, false);


--
-- TOC entry 3937 (class 0 OID 0)
-- Dependencies: 387
-- Name: seq_comentcartaasignacion; Type: SEQUENCE SET; Schema: sisat; Owner: suite
--

SELECT pg_catalog.setval('sisat.seq_comentcartaasignacion', 1, false);


--
-- TOC entry 3938 (class 0 OID 0)
-- Dependencies: 388
-- Name: seq_comentcosteo; Type: SEQUENCE SET; Schema: sisat; Owner: suite
--

SELECT pg_catalog.setval('sisat.seq_comentcosteo', 1, false);


--
-- TOC entry 3939 (class 0 OID 0)
-- Dependencies: 389
-- Name: seq_comententrevista; Type: SEQUENCE SET; Schema: sisat; Owner: suite
--

SELECT pg_catalog.setval('sisat.seq_comententrevista', 1, false);


--
-- TOC entry 3940 (class 0 OID 0)
-- Dependencies: 390
-- Name: seq_comentvacantes; Type: SEQUENCE SET; Schema: sisat; Owner: suite
--

SELECT pg_catalog.setval('sisat.seq_comentvacantes', 2, true);


--
-- TOC entry 3941 (class 0 OID 0)
-- Dependencies: 391
-- Name: seq_contrataciones; Type: SEQUENCE SET; Schema: sisat; Owner: suite
--

SELECT pg_catalog.setval('sisat.seq_contrataciones', 9, true);


--
-- TOC entry 3942 (class 0 OID 0)
-- Dependencies: 392
-- Name: seq_cotizaciones; Type: SEQUENCE SET; Schema: sisat; Owner: suite
--

SELECT pg_catalog.setval('sisat.seq_cotizaciones', 1, false);


--
-- TOC entry 3943 (class 0 OID 0)
-- Dependencies: 393
-- Name: seq_cursos; Type: SEQUENCE SET; Schema: sisat; Owner: suite
--

SELECT pg_catalog.setval('sisat.seq_cursos', 1, false);


--
-- TOC entry 3944 (class 0 OID 0)
-- Dependencies: 394
-- Name: seq_entrevistas; Type: SEQUENCE SET; Schema: sisat; Owner: suite
--

SELECT pg_catalog.setval('sisat.seq_entrevistas', 1, false);


--
-- TOC entry 3945 (class 0 OID 0)
-- Dependencies: 395
-- Name: seq_envios; Type: SEQUENCE SET; Schema: sisat; Owner: suite
--

SELECT pg_catalog.setval('sisat.seq_envios', 1, false);


--
-- TOC entry 3946 (class 0 OID 0)
-- Dependencies: 396
-- Name: seq_escolaridad; Type: SEQUENCE SET; Schema: sisat; Owner: suite
--

SELECT pg_catalog.setval('sisat.seq_escolaridad', 1, false);


--
-- TOC entry 3947 (class 0 OID 0)
-- Dependencies: 397
-- Name: seq_experiencias; Type: SEQUENCE SET; Schema: sisat; Owner: suite
--

SELECT pg_catalog.setval('sisat.seq_experiencias', 1, false);


--
-- TOC entry 3948 (class 0 OID 0)
-- Dependencies: 398
-- Name: seq_firmareqper; Type: SEQUENCE SET; Schema: sisat; Owner: suite
--

SELECT pg_catalog.setval('sisat.seq_firmareqper', 1, false);


--
-- TOC entry 3949 (class 0 OID 0)
-- Dependencies: 399
-- Name: seq_firmas; Type: SEQUENCE SET; Schema: sisat; Owner: suite
--

SELECT pg_catalog.setval('sisat.seq_firmas', 1, false);


--
-- TOC entry 3950 (class 0 OID 0)
-- Dependencies: 400
-- Name: seq_habilidades; Type: SEQUENCE SET; Schema: sisat; Owner: suite
--

SELECT pg_catalog.setval('sisat.seq_habilidades', 1, false);


--
-- TOC entry 3951 (class 0 OID 0)
-- Dependencies: 401
-- Name: seq_idiomas; Type: SEQUENCE SET; Schema: sisat; Owner: suite
--

SELECT pg_catalog.setval('sisat.seq_idiomas', 1, false);


--
-- TOC entry 3952 (class 0 OID 0)
-- Dependencies: 402
-- Name: seq_ordenservicios; Type: SEQUENCE SET; Schema: sisat; Owner: suite
--

SELECT pg_catalog.setval('sisat.seq_ordenservicios', 1, false);


--
-- TOC entry 3953 (class 0 OID 0)
-- Dependencies: 403
-- Name: seq_prospectos; Type: SEQUENCE SET; Schema: sisat; Owner: suite
--

SELECT pg_catalog.setval('sisat.seq_prospectos', 1, false);


--
-- TOC entry 3954 (class 0 OID 0)
-- Dependencies: 404
-- Name: seq_proyectos; Type: SEQUENCE SET; Schema: sisat; Owner: suite
--

SELECT pg_catalog.setval('sisat.seq_proyectos', 1, false);


--
-- TOC entry 3955 (class 0 OID 0)
-- Dependencies: 405
-- Name: seq_vacantes; Type: SEQUENCE SET; Schema: sisat; Owner: suite
--

SELECT pg_catalog.setval('sisat.seq_vacantes', 1, false);


--
-- TOC entry 3956 (class 0 OID 0)
-- Dependencies: 427
-- Name: tsisatherramientas_cod_herramientas_seq; Type: SEQUENCE SET; Schema: sisat; Owner: suite
--

SELECT pg_catalog.setval('sisat.tsisatherramientas_cod_herramientas_seq', 1, false);


--
-- TOC entry 3576 (class 2606 OID 43923)
-- Name: tsisatprospectos_idiomas pk_cod_pros_idioma; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatprospectos_idiomas
    ADD CONSTRAINT pk_cod_pros_idioma PRIMARY KEY (cod_pros_idoma);


--
-- TOC entry 3516 (class 2606 OID 43925)
-- Name: tsisatappservices tsisatappservices_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatappservices
    ADD CONSTRAINT tsisatappservices_pkey PRIMARY KEY (cod_appservice);


--
-- TOC entry 3518 (class 2606 OID 43927)
-- Name: tsisatarquitecturas tsisatarquitecturas_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatarquitecturas
    ADD CONSTRAINT tsisatarquitecturas_pkey PRIMARY KEY (cod_arquitectura);


--
-- TOC entry 3520 (class 2606 OID 43929)
-- Name: tsisatasignaciones tsisatasignaciones_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatasignaciones
    ADD CONSTRAINT tsisatasignaciones_pkey PRIMARY KEY (cod_asignacion);


--
-- TOC entry 3522 (class 2606 OID 43931)
-- Name: tsisatcandidatos tsisatcandidatos_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcandidatos
    ADD CONSTRAINT tsisatcandidatos_pkey PRIMARY KEY (cod_candidato);


--
-- TOC entry 3524 (class 2606 OID 43933)
-- Name: tsisatcartaaceptacion tsisatcartaaceptacion_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcartaaceptacion
    ADD CONSTRAINT tsisatcartaaceptacion_pkey PRIMARY KEY (cod_aceptacion);


--
-- TOC entry 3526 (class 2606 OID 43935)
-- Name: tsisatcartaasignacion tsisatcartaasignacion_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcartaasignacion
    ADD CONSTRAINT tsisatcartaasignacion_pkey PRIMARY KEY (cod_asignacion);


--
-- TOC entry 3530 (class 2606 OID 43937)
-- Name: tsisatcomentcosteo tsisatcomentarios_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcomentcosteo
    ADD CONSTRAINT tsisatcomentarios_pkey PRIMARY KEY (cod_comentcosteo);


--
-- TOC entry 3528 (class 2606 OID 43939)
-- Name: tsisatcomentcartaasignacion tsisatcomentcartaasignacion_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcomentcartaasignacion
    ADD CONSTRAINT tsisatcomentcartaasignacion_pkey PRIMARY KEY (cod_comentcartaasignacion);


--
-- TOC entry 3532 (class 2606 OID 43941)
-- Name: tsisatcomententrevista tsisatcomententrevista_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcomententrevista
    ADD CONSTRAINT tsisatcomententrevista_pkey PRIMARY KEY (cod_comententrevista);


--
-- TOC entry 3534 (class 2606 OID 43943)
-- Name: tsisatcomentvacantes tsisatcomentvacantes_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcomentvacantes
    ADD CONSTRAINT tsisatcomentvacantes_pkey PRIMARY KEY (cod_comentvacante);


--
-- TOC entry 3536 (class 2606 OID 43945)
-- Name: tsisatcontrataciones tsisatcontrataciones_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcontrataciones
    ADD CONSTRAINT tsisatcontrataciones_pkey PRIMARY KEY (cod_contratacion);


--
-- TOC entry 3538 (class 2606 OID 43947)
-- Name: tsisatcotizaciones tsisatcotizaciones_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcotizaciones
    ADD CONSTRAINT tsisatcotizaciones_pkey PRIMARY KEY (cod_cotizacion);


--
-- TOC entry 3540 (class 2606 OID 43949)
-- Name: tsisatcursosycertificados tsisatcursosycerticados_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcursosycertificados
    ADD CONSTRAINT tsisatcursosycerticados_pkey PRIMARY KEY (cod_curso);


--
-- TOC entry 3542 (class 2606 OID 43951)
-- Name: tsisatentrevistas tsisatentrevistas_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatentrevistas
    ADD CONSTRAINT tsisatentrevistas_pkey PRIMARY KEY (cod_entrevista);


--
-- TOC entry 3544 (class 2606 OID 43953)
-- Name: tsisatenviocorreos tsisatenviocorreos_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatenviocorreos
    ADD CONSTRAINT tsisatenviocorreos_pkey PRIMARY KEY (cod_envio);


--
-- TOC entry 3546 (class 2606 OID 43955)
-- Name: tsisatescolaridad tsisatescolaridad_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatescolaridad
    ADD CONSTRAINT tsisatescolaridad_pkey PRIMARY KEY (cod_escolaridad);


--
-- TOC entry 3548 (class 2606 OID 43957)
-- Name: tsisatexperienciaslaborales tsisatexperienciaslaborales_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatexperienciaslaborales
    ADD CONSTRAINT tsisatexperienciaslaborales_pkey PRIMARY KEY (cod_experiencia);


--
-- TOC entry 3550 (class 2606 OID 43959)
-- Name: tsisatfirmareqper tsisatfirmas_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatfirmareqper
    ADD CONSTRAINT tsisatfirmas_pkey PRIMARY KEY (cod_firma);


--
-- TOC entry 3552 (class 2606 OID 43961)
-- Name: tsisatframeworks tsisatframeworks_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatframeworks
    ADD CONSTRAINT tsisatframeworks_pkey PRIMARY KEY (cod_framework);


--
-- TOC entry 3554 (class 2606 OID 43963)
-- Name: tsisathabilidades tsisathabilidades_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisathabilidades
    ADD CONSTRAINT tsisathabilidades_pkey PRIMARY KEY (cod_habilidad);


--
-- TOC entry 3556 (class 2606 OID 43965)
-- Name: tsisatherramientas tsisatherramientas_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatherramientas
    ADD CONSTRAINT tsisatherramientas_pkey PRIMARY KEY (cod_herramientas);


--
-- TOC entry 3558 (class 2606 OID 43967)
-- Name: tsisatides tsisatides_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatides
    ADD CONSTRAINT tsisatides_pkey PRIMARY KEY (cod_ide);


--
-- TOC entry 3560 (class 2606 OID 43969)
-- Name: tsisatidiomas tsisatidiomas_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatidiomas
    ADD CONSTRAINT tsisatidiomas_pkey PRIMARY KEY (cod_idioma);


--
-- TOC entry 3562 (class 2606 OID 43971)
-- Name: tsisatlenguajes tsisatlenguajes_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatlenguajes
    ADD CONSTRAINT tsisatlenguajes_pkey PRIMARY KEY (cod_lenguaje);


--
-- TOC entry 3564 (class 2606 OID 43973)
-- Name: tsisatmaquetados tsisatmaquetados_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatmaquetados
    ADD CONSTRAINT tsisatmaquetados_pkey PRIMARY KEY (cod_maquetado);


--
-- TOC entry 3566 (class 2606 OID 43975)
-- Name: tsisatmetodologias tsisatmetodologias_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatmetodologias
    ADD CONSTRAINT tsisatmetodologias_pkey PRIMARY KEY (cod_metodologia);


--
-- TOC entry 3568 (class 2606 OID 43977)
-- Name: tsisatmodelados tsisatmodelados_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatmodelados
    ADD CONSTRAINT tsisatmodelados_pkey PRIMARY KEY (cod_modelado);


--
-- TOC entry 3570 (class 2606 OID 43979)
-- Name: tsisatordenservicio tsisatordenservicio_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatordenservicio
    ADD CONSTRAINT tsisatordenservicio_pkey PRIMARY KEY (cod_ordenservicio);


--
-- TOC entry 3572 (class 2606 OID 43981)
-- Name: tsisatpatrones tsisatpatrones_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatpatrones
    ADD CONSTRAINT tsisatpatrones_pkey PRIMARY KEY (cod_patron);


--
-- TOC entry 3574 (class 2606 OID 43983)
-- Name: tsisatprospectos tsisatprospectos_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatprospectos
    ADD CONSTRAINT tsisatprospectos_pkey PRIMARY KEY (cod_prospecto);


--
-- TOC entry 3578 (class 2606 OID 43985)
-- Name: tsisatprotocolos tsisatprotocolos_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatprotocolos
    ADD CONSTRAINT tsisatprotocolos_pkey PRIMARY KEY (cod_protocolo);


--
-- TOC entry 3580 (class 2606 OID 43987)
-- Name: tsisatproyectos tsisatproyectos_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatproyectos
    ADD CONSTRAINT tsisatproyectos_pkey PRIMARY KEY (cod_proyecto);


--
-- TOC entry 3582 (class 2606 OID 43989)
-- Name: tsisatqa tsisatqa_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatqa
    ADD CONSTRAINT tsisatqa_pkey PRIMARY KEY (cod_qa);


--
-- TOC entry 3584 (class 2606 OID 43991)
-- Name: tsisatrepositoriolibrerias tsisatrepositoriolibrerias_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatrepositoriolibrerias
    ADD CONSTRAINT tsisatrepositoriolibrerias_pkey PRIMARY KEY (cod_repositoriolibreria);


--
-- TOC entry 3586 (class 2606 OID 43993)
-- Name: tsisatrepositorios tsisatrepositorios_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatrepositorios
    ADD CONSTRAINT tsisatrepositorios_pkey PRIMARY KEY (cod_repositorio);


--
-- TOC entry 3588 (class 2606 OID 43995)
-- Name: tsisatsgbd tsisatsgbd_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatsgbd
    ADD CONSTRAINT tsisatsgbd_pkey PRIMARY KEY (cod_sgbd);


--
-- TOC entry 3590 (class 2606 OID 43997)
-- Name: tsisatso tsisatso_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatso
    ADD CONSTRAINT tsisatso_pkey PRIMARY KEY (cod_so);


--
-- TOC entry 3592 (class 2606 OID 43999)
-- Name: tsisatvacantes tsisatvacantes_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatvacantes
    ADD CONSTRAINT tsisatvacantes_pkey PRIMARY KEY (cod_vacante);


--
-- TOC entry 3633 (class 2606 OID 45045)
-- Name: tsisatcotizaciones aud_fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcotizaciones
    ADD CONSTRAINT aud_fk_cod_creadopor FOREIGN KEY (aud_cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 3654 (class 2606 OID 45050)
-- Name: tsisatordenservicio aud_fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatordenservicio
    ADD CONSTRAINT aud_fk_cod_creadopor FOREIGN KEY (aud_cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 3634 (class 2606 OID 45055)
-- Name: tsisatcotizaciones aud_fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcotizaciones
    ADD CONSTRAINT aud_fk_cod_modificadopor FOREIGN KEY (aud_cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 3655 (class 2606 OID 45060)
-- Name: tsisatordenservicio aud_fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatordenservicio
    ADD CONSTRAINT aud_fk_cod_modificadopor FOREIGN KEY (aud_cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 3644 (class 2606 OID 45065)
-- Name: tsisatenviocorreos aud_fk_creadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatenviocorreos
    ADD CONSTRAINT aud_fk_creadopor FOREIGN KEY (aud_cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 3645 (class 2606 OID 45070)
-- Name: tsisatenviocorreos aud_fk_modificadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatenviocorreos
    ADD CONSTRAINT aud_fk_modificadopor FOREIGN KEY (aud_cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 3650 (class 2606 OID 45075)
-- Name: tsisatfirmareqper cod_autorizafk; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatfirmareqper
    ADD CONSTRAINT cod_autorizafk FOREIGN KEY (cod_autoriza) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE;


--
-- TOC entry 3651 (class 2606 OID 45080)
-- Name: tsisatfirmareqper cod_solicitafk; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatfirmareqper
    ADD CONSTRAINT cod_solicitafk FOREIGN KEY (cod_solicita) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE;


--
-- TOC entry 3652 (class 2606 OID 45085)
-- Name: tsisatfirmareqper cod_vacantefk; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatfirmareqper
    ADD CONSTRAINT cod_vacantefk FOREIGN KEY (cod_vacante) REFERENCES sisat.tsisatvacantes(cod_vacante) ON UPDATE CASCADE;


--
-- TOC entry 3608 (class 2606 OID 45090)
-- Name: tsisatcartaasignacion fk__cod_gpy; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcartaasignacion
    ADD CONSTRAINT fk__cod_gpy FOREIGN KEY (cod_gpy) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 3667 (class 2606 OID 45095)
-- Name: tsisatproyectos fk_aud_cod_creadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatproyectos
    ADD CONSTRAINT fk_aud_cod_creadopor FOREIGN KEY (aud_cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 3593 (class 2606 OID 45100)
-- Name: tsisatasignaciones fk_aud_cod_creadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatasignaciones
    ADD CONSTRAINT fk_aud_cod_creadopor FOREIGN KEY (aud_cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 3668 (class 2606 OID 45105)
-- Name: tsisatproyectos fk_aud_cod_modificadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatproyectos
    ADD CONSTRAINT fk_aud_cod_modificadopor FOREIGN KEY (aud_cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 3594 (class 2606 OID 45110)
-- Name: tsisatasignaciones fk_aud_cod_modificadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatasignaciones
    ADD CONSTRAINT fk_aud_cod_modificadopor FOREIGN KEY (aud_cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 3609 (class 2606 OID 45115)
-- Name: tsisatcartaasignacion fk_cod_ape; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcartaasignacion
    ADD CONSTRAINT fk_cod_ape FOREIGN KEY (cod_ape) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3621 (class 2606 OID 45120)
-- Name: tsisatcomentcosteo fk_cod_candidato; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcomentcosteo
    ADD CONSTRAINT fk_cod_candidato FOREIGN KEY (cod_candidato) REFERENCES sisat.tsisatcandidatos(cod_candidato);


--
-- TOC entry 3635 (class 2606 OID 45125)
-- Name: tsisatcotizaciones fk_cod_ciudad; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcotizaciones
    ADD CONSTRAINT fk_cod_ciudad FOREIGN KEY (cod_ciudad) REFERENCES sgrt.tsgrtciudades(cod_ciudad);


--
-- TOC entry 3656 (class 2606 OID 45130)
-- Name: tsisatordenservicio fk_cod_ciudad; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatordenservicio
    ADD CONSTRAINT fk_cod_ciudad FOREIGN KEY (cod_ciudad) REFERENCES sgrt.tsgrtciudades(cod_ciudad);


--
-- TOC entry 3610 (class 2606 OID 45135)
-- Name: tsisatcartaasignacion fk_cod_cliente; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcartaasignacion
    ADD CONSTRAINT fk_cod_cliente FOREIGN KEY (cod_cliente) REFERENCES sgrh.tsgrhclientes(cod_cliente) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3657 (class 2606 OID 45140)
-- Name: tsisatordenservicio fk_cod_cliente; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatordenservicio
    ADD CONSTRAINT fk_cod_cliente FOREIGN KEY (cod_cliente) REFERENCES sgrh.tsgrhclientes(cod_cliente) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3595 (class 2606 OID 45145)
-- Name: tsisatasignaciones fk_cod_cliente; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatasignaciones
    ADD CONSTRAINT fk_cod_cliente FOREIGN KEY (cod_cliente) REFERENCES sgrh.tsgrhclientes(cod_cliente) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3673 (class 2606 OID 45150)
-- Name: tsisatvacantes fk_cod_cliente; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatvacantes
    ADD CONSTRAINT fk_cod_cliente FOREIGN KEY (cod_cliente) REFERENCES sgrh.tsgrhclientes(cod_cliente);


--
-- TOC entry 3618 (class 2606 OID 45155)
-- Name: tsisatcomentcartaasignacion fk_cod_comentcartaasignacion; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcomentcartaasignacion
    ADD CONSTRAINT fk_cod_comentcartaasignacion FOREIGN KEY (cod_asignacion) REFERENCES sisat.tsisatcartaasignacion(cod_asignacion);


--
-- TOC entry 3677 (class 2606 OID 45538)
-- Name: tsisatvacantes fk_cod_contratacion; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatvacantes
    ADD CONSTRAINT fk_cod_contratacion FOREIGN KEY (cod_contratacion) REFERENCES sisat.tsisatcontrataciones(cod_contratacion) ON UPDATE CASCADE;


--
-- TOC entry 3674 (class 2606 OID 45165)
-- Name: tsisatvacantes fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatvacantes
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (aud_cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3604 (class 2606 OID 45170)
-- Name: tsisatcartaaceptacion fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcartaaceptacion
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (aud_cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3611 (class 2606 OID 45175)
-- Name: tsisatcartaasignacion fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcartaasignacion
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (aud_cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3636 (class 2606 OID 45180)
-- Name: tsisatcotizaciones fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcotizaciones
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (aud_cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3658 (class 2606 OID 45185)
-- Name: tsisatordenservicio fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatordenservicio
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (aud_cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3646 (class 2606 OID 45190)
-- Name: tsisatenviocorreos fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatenviocorreos
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (aud_cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3596 (class 2606 OID 45195)
-- Name: tsisatasignaciones fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatasignaciones
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (aud_cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3641 (class 2606 OID 45200)
-- Name: tsisatentrevistas fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatentrevistas
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (aud_cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3663 (class 2606 OID 45205)
-- Name: tsisatprospectos fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatprospectos
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (aud_cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3669 (class 2606 OID 45210)
-- Name: tsisatproyectos fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatproyectos
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (aud_cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3630 (class 2606 OID 45215)
-- Name: tsisatcontrataciones fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcontrataciones
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (aud_cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE;


--
-- TOC entry 3619 (class 2606 OID 45220)
-- Name: tsisatcomentcartaasignacion fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcomentcartaasignacion
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (aud_cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE;


--
-- TOC entry 3622 (class 2606 OID 45225)
-- Name: tsisatcomentcosteo fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcomentcosteo
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (aud_cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE;


--
-- TOC entry 3624 (class 2606 OID 45230)
-- Name: tsisatcomententrevista fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcomententrevista
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (aud_cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE;


--
-- TOC entry 3627 (class 2606 OID 45235)
-- Name: tsisatcomentvacantes fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcomentvacantes
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (aud_cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE;


--
-- TOC entry 3597 (class 2606 OID 45240)
-- Name: tsisatasignaciones fk_cod_empleado; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatasignaciones
    ADD CONSTRAINT fk_cod_empleado FOREIGN KEY (cod_empleado) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 3625 (class 2606 OID 45245)
-- Name: tsisatcomententrevista fk_cod_entrevista; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcomententrevista
    ADD CONSTRAINT fk_cod_entrevista FOREIGN KEY (cod_entrevista) REFERENCES sisat.tsisatentrevistas(cod_entrevista);


--
-- TOC entry 3637 (class 2606 OID 45250)
-- Name: tsisatcotizaciones fk_cod_estado; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcotizaciones
    ADD CONSTRAINT fk_cod_estado FOREIGN KEY (cod_estado) REFERENCES sgrt.tsgrtestados(cod_estadorep);


--
-- TOC entry 3659 (class 2606 OID 45255)
-- Name: tsisatordenservicio fk_cod_estadorep; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatordenservicio
    ADD CONSTRAINT fk_cod_estadorep FOREIGN KEY (cod_estadorep) REFERENCES sgrt.tsgrtestados(cod_estadorep);


--
-- TOC entry 3612 (class 2606 OID 45260)
-- Name: tsisatcartaasignacion fk_cod_gpy; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcartaasignacion
    ADD CONSTRAINT fk_cod_gpy FOREIGN KEY (cod_gpy) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3660 (class 2606 OID 45265)
-- Name: tsisatordenservicio fk_cod_gpy; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatordenservicio
    ADD CONSTRAINT fk_cod_gpy FOREIGN KEY (cod_gpy) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3675 (class 2606 OID 45270)
-- Name: tsisatvacantes fk_cod_idioma; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatvacantes
    ADD CONSTRAINT fk_cod_idioma FOREIGN KEY (cod_idioma) REFERENCES sisat.tsisatidiomas(cod_idioma);


--
-- TOC entry 3665 (class 2606 OID 45275)
-- Name: tsisatprospectos_idiomas fk_cod_idioma; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatprospectos_idiomas
    ADD CONSTRAINT fk_cod_idioma FOREIGN KEY (cod_idioma) REFERENCES sisat.tsisatidiomas(cod_idioma);


--
-- TOC entry 3676 (class 2606 OID 45280)
-- Name: tsisatvacantes fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatvacantes
    ADD CONSTRAINT fk_cod_modificadopor FOREIGN KEY (aud_cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3605 (class 2606 OID 45285)
-- Name: tsisatcartaaceptacion fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcartaaceptacion
    ADD CONSTRAINT fk_cod_modificadopor FOREIGN KEY (aud_cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3613 (class 2606 OID 45290)
-- Name: tsisatcartaasignacion fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcartaasignacion
    ADD CONSTRAINT fk_cod_modificadopor FOREIGN KEY (aud_cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3638 (class 2606 OID 45295)
-- Name: tsisatcotizaciones fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcotizaciones
    ADD CONSTRAINT fk_cod_modificadopor FOREIGN KEY (aud_cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3661 (class 2606 OID 45300)
-- Name: tsisatordenservicio fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatordenservicio
    ADD CONSTRAINT fk_cod_modificadopor FOREIGN KEY (aud_cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3647 (class 2606 OID 45305)
-- Name: tsisatenviocorreos fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatenviocorreos
    ADD CONSTRAINT fk_cod_modificadopor FOREIGN KEY (aud_cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3598 (class 2606 OID 45310)
-- Name: tsisatasignaciones fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatasignaciones
    ADD CONSTRAINT fk_cod_modificadopor FOREIGN KEY (aud_cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3642 (class 2606 OID 45315)
-- Name: tsisatentrevistas fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatentrevistas
    ADD CONSTRAINT fk_cod_modificadopor FOREIGN KEY (aud_cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3664 (class 2606 OID 45320)
-- Name: tsisatprospectos fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatprospectos
    ADD CONSTRAINT fk_cod_modificadopor FOREIGN KEY (aud_cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3670 (class 2606 OID 45325)
-- Name: tsisatproyectos fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatproyectos
    ADD CONSTRAINT fk_cod_modificadopor FOREIGN KEY (aud_cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3631 (class 2606 OID 45330)
-- Name: tsisatcontrataciones fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcontrataciones
    ADD CONSTRAINT fk_cod_modificadopor FOREIGN KEY (aud_cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE;


--
-- TOC entry 3620 (class 2606 OID 45335)
-- Name: tsisatcomentcartaasignacion fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcomentcartaasignacion
    ADD CONSTRAINT fk_cod_modificadopor FOREIGN KEY (aud_cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE;


--
-- TOC entry 3623 (class 2606 OID 45340)
-- Name: tsisatcomentcosteo fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcomentcosteo
    ADD CONSTRAINT fk_cod_modificadopor FOREIGN KEY (aud_cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE;


--
-- TOC entry 3626 (class 2606 OID 45345)
-- Name: tsisatcomententrevista fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcomententrevista
    ADD CONSTRAINT fk_cod_modificadopor FOREIGN KEY (aud_cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE;


--
-- TOC entry 3628 (class 2606 OID 45350)
-- Name: tsisatcomentvacantes fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcomentvacantes
    ADD CONSTRAINT fk_cod_modificadopor FOREIGN KEY (aud_cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE;


--
-- TOC entry 3614 (class 2606 OID 45355)
-- Name: tsisatcartaasignacion fk_cod_perfil; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcartaasignacion
    ADD CONSTRAINT fk_cod_perfil FOREIGN KEY (cod_perfil) REFERENCES sgrh.tsgrhperfiles(cod_perfil) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3601 (class 2606 OID 45360)
-- Name: tsisatcandidatos fk_cod_perfil; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcandidatos
    ADD CONSTRAINT fk_cod_perfil FOREIGN KEY (cod_perfil) REFERENCES sgrh.tsgrhperfiles(cod_perfil) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3599 (class 2606 OID 45365)
-- Name: tsisatasignaciones fk_cod_perfil; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatasignaciones
    ADD CONSTRAINT fk_cod_perfil FOREIGN KEY (cod_perfil) REFERENCES sgrh.tsgrhperfiles(cod_perfil) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3671 (class 2606 OID 45370)
-- Name: tsisatproyectos fk_cod_perfil; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatproyectos
    ADD CONSTRAINT fk_cod_perfil FOREIGN KEY (cod_perfil) REFERENCES sgrh.tsgrhperfiles(cod_perfil) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3615 (class 2606 OID 45375)
-- Name: tsisatcartaasignacion fk_cod_prospecto; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcartaasignacion
    ADD CONSTRAINT fk_cod_prospecto FOREIGN KEY (cod_prospecto) REFERENCES sisat.tsisatprospectos(cod_prospecto) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3649 (class 2606 OID 45380)
-- Name: tsisatexperienciaslaborales fk_cod_prospecto; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatexperienciaslaborales
    ADD CONSTRAINT fk_cod_prospecto FOREIGN KEY (cod_prospecto) REFERENCES sisat.tsisatprospectos(cod_prospecto) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3648 (class 2606 OID 45385)
-- Name: tsisatescolaridad fk_cod_prospecto; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatescolaridad
    ADD CONSTRAINT fk_cod_prospecto FOREIGN KEY (cod_prospecto) REFERENCES sisat.tsisatprospectos(cod_prospecto) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3640 (class 2606 OID 45390)
-- Name: tsisatcursosycertificados fk_cod_prospecto; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcursosycertificados
    ADD CONSTRAINT fk_cod_prospecto FOREIGN KEY (cod_prospecto) REFERENCES sisat.tsisatprospectos(cod_prospecto) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3653 (class 2606 OID 45395)
-- Name: tsisathabilidades fk_cod_prospecto; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisathabilidades
    ADD CONSTRAINT fk_cod_prospecto FOREIGN KEY (cod_prospecto) REFERENCES sisat.tsisatprospectos(cod_prospecto);


--
-- TOC entry 3672 (class 2606 OID 45400)
-- Name: tsisatproyectos fk_cod_prospecto; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatproyectos
    ADD CONSTRAINT fk_cod_prospecto FOREIGN KEY (cod_prospecto) REFERENCES sisat.tsisatprospectos(cod_prospecto) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3600 (class 2606 OID 45405)
-- Name: tsisatasignaciones fk_cod_prospecto; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatasignaciones
    ADD CONSTRAINT fk_cod_prospecto FOREIGN KEY (cod_prospecto) REFERENCES sisat.tsisatprospectos(cod_prospecto) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3643 (class 2606 OID 45410)
-- Name: tsisatentrevistas fk_cod_prospecto; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatentrevistas
    ADD CONSTRAINT fk_cod_prospecto FOREIGN KEY (cod_prospecto) REFERENCES sisat.tsisatprospectos(cod_prospecto);


--
-- TOC entry 3666 (class 2606 OID 45415)
-- Name: tsisatprospectos_idiomas fk_cod_prospecto; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatprospectos_idiomas
    ADD CONSTRAINT fk_cod_prospecto FOREIGN KEY (cod_prospecto) REFERENCES sisat.tsisatprospectos(cod_prospecto);


--
-- TOC entry 3632 (class 2606 OID 45420)
-- Name: tsisatcontrataciones fk_cod_prospecto; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcontrataciones
    ADD CONSTRAINT fk_cod_prospecto FOREIGN KEY (cod_prospecto) REFERENCES sisat.tsisatprospectos(cod_prospecto) ON UPDATE CASCADE;


--
-- TOC entry 3639 (class 2606 OID 45425)
-- Name: tsisatcotizaciones fk_cod_puesto; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcotizaciones
    ADD CONSTRAINT fk_cod_puesto FOREIGN KEY (cod_puesto) REFERENCES sgrh.tsgrhpuestos(cod_puesto);


--
-- TOC entry 3662 (class 2606 OID 45430)
-- Name: tsisatordenservicio fk_cod_puesto; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatordenservicio
    ADD CONSTRAINT fk_cod_puesto FOREIGN KEY (cod_puesto) REFERENCES sgrh.tsgrhpuestos(cod_puesto);


--
-- TOC entry 3616 (class 2606 OID 45435)
-- Name: tsisatcartaasignacion fk_cod_rhta; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcartaasignacion
    ADD CONSTRAINT fk_cod_rhta FOREIGN KEY (cod_rhta) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 3617 (class 2606 OID 45440)
-- Name: tsisatcartaasignacion fk_cod_rys; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcartaasignacion
    ADD CONSTRAINT fk_cod_rys FOREIGN KEY (cod_rys) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 3629 (class 2606 OID 45445)
-- Name: tsisatcomentvacantes fk_cod_vacante; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcomentvacantes
    ADD CONSTRAINT fk_cod_vacante FOREIGN KEY (cod_vacante) REFERENCES sisat.tsisatvacantes(cod_vacante);


--
-- TOC entry 3606 (class 2606 OID 45450)
-- Name: tsisatcartaaceptacion fk_creadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcartaaceptacion
    ADD CONSTRAINT fk_creadopor FOREIGN KEY (aud_cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 3602 (class 2606 OID 45455)
-- Name: tsisatcandidatos fk_creadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcandidatos
    ADD CONSTRAINT fk_creadopor FOREIGN KEY (aud_cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 3607 (class 2606 OID 45460)
-- Name: tsisatcartaaceptacion fk_modificadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcartaaceptacion
    ADD CONSTRAINT fk_modificadopor FOREIGN KEY (aud_cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 3603 (class 2606 OID 45465)
-- Name: tsisatcandidatos fk_modificadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcandidatos
    ADD CONSTRAINT fk_modificadopor FOREIGN KEY (aud_cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 3868 (class 0 OID 0)
-- Dependencies: 506
-- Name: FUNCTION buscar_asignacion_recurso(asignacion_cod integer); Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON FUNCTION sisat.buscar_asignacion_recurso(asignacion_cod integer) FROM suite;
GRANT ALL ON FUNCTION sisat.buscar_asignacion_recurso(asignacion_cod integer) TO suite WITH GRANT OPTION;


--
-- TOC entry 3869 (class 0 OID 0)
-- Dependencies: 383
-- Name: SEQUENCE seq_aceptaciones; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON SEQUENCE sisat.seq_aceptaciones FROM suite;
GRANT UPDATE ON SEQUENCE sisat.seq_aceptaciones TO suite;
GRANT SELECT,USAGE ON SEQUENCE sisat.seq_aceptaciones TO suite WITH GRANT OPTION;


--
-- TOC entry 3870 (class 0 OID 0)
-- Dependencies: 384
-- Name: SEQUENCE seq_asignaciones; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON SEQUENCE sisat.seq_asignaciones FROM suite;
GRANT UPDATE ON SEQUENCE sisat.seq_asignaciones TO suite;
GRANT SELECT,USAGE ON SEQUENCE sisat.seq_asignaciones TO suite WITH GRANT OPTION;


--
-- TOC entry 3871 (class 0 OID 0)
-- Dependencies: 385
-- Name: SEQUENCE seq_candidatos; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON SEQUENCE sisat.seq_candidatos FROM suite;
GRANT UPDATE ON SEQUENCE sisat.seq_candidatos TO suite;
GRANT SELECT,USAGE ON SEQUENCE sisat.seq_candidatos TO suite WITH GRANT OPTION;


--
-- TOC entry 3872 (class 0 OID 0)
-- Dependencies: 386
-- Name: SEQUENCE seq_cartaasignaciones; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON SEQUENCE sisat.seq_cartaasignaciones FROM suite;
GRANT UPDATE ON SEQUENCE sisat.seq_cartaasignaciones TO suite;
GRANT SELECT,USAGE ON SEQUENCE sisat.seq_cartaasignaciones TO suite WITH GRANT OPTION;


--
-- TOC entry 3873 (class 0 OID 0)
-- Dependencies: 387
-- Name: SEQUENCE seq_comentcartaasignacion; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON SEQUENCE sisat.seq_comentcartaasignacion FROM suite;
GRANT UPDATE ON SEQUENCE sisat.seq_comentcartaasignacion TO suite;
GRANT SELECT,USAGE ON SEQUENCE sisat.seq_comentcartaasignacion TO suite WITH GRANT OPTION;


--
-- TOC entry 3874 (class 0 OID 0)
-- Dependencies: 388
-- Name: SEQUENCE seq_comentcosteo; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON SEQUENCE sisat.seq_comentcosteo FROM suite;
GRANT UPDATE ON SEQUENCE sisat.seq_comentcosteo TO suite;
GRANT SELECT,USAGE ON SEQUENCE sisat.seq_comentcosteo TO suite WITH GRANT OPTION;


--
-- TOC entry 3875 (class 0 OID 0)
-- Dependencies: 389
-- Name: SEQUENCE seq_comententrevista; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON SEQUENCE sisat.seq_comententrevista FROM suite;
GRANT UPDATE ON SEQUENCE sisat.seq_comententrevista TO suite;
GRANT SELECT,USAGE ON SEQUENCE sisat.seq_comententrevista TO suite WITH GRANT OPTION;


--
-- TOC entry 3876 (class 0 OID 0)
-- Dependencies: 390
-- Name: SEQUENCE seq_comentvacantes; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON SEQUENCE sisat.seq_comentvacantes FROM suite;
GRANT UPDATE ON SEQUENCE sisat.seq_comentvacantes TO suite;
GRANT SELECT,USAGE ON SEQUENCE sisat.seq_comentvacantes TO suite WITH GRANT OPTION;


--
-- TOC entry 3877 (class 0 OID 0)
-- Dependencies: 391
-- Name: SEQUENCE seq_contrataciones; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON SEQUENCE sisat.seq_contrataciones FROM suite;
GRANT UPDATE ON SEQUENCE sisat.seq_contrataciones TO suite;
GRANT SELECT,USAGE ON SEQUENCE sisat.seq_contrataciones TO suite WITH GRANT OPTION;


--
-- TOC entry 3878 (class 0 OID 0)
-- Dependencies: 392
-- Name: SEQUENCE seq_cotizaciones; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON SEQUENCE sisat.seq_cotizaciones FROM suite;
GRANT UPDATE ON SEQUENCE sisat.seq_cotizaciones TO suite;
GRANT SELECT,USAGE ON SEQUENCE sisat.seq_cotizaciones TO suite WITH GRANT OPTION;


--
-- TOC entry 3879 (class 0 OID 0)
-- Dependencies: 393
-- Name: SEQUENCE seq_cursos; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON SEQUENCE sisat.seq_cursos FROM suite;
GRANT UPDATE ON SEQUENCE sisat.seq_cursos TO suite;
GRANT SELECT,USAGE ON SEQUENCE sisat.seq_cursos TO suite WITH GRANT OPTION;


--
-- TOC entry 3880 (class 0 OID 0)
-- Dependencies: 394
-- Name: SEQUENCE seq_entrevistas; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON SEQUENCE sisat.seq_entrevistas FROM suite;
GRANT UPDATE ON SEQUENCE sisat.seq_entrevistas TO suite;
GRANT SELECT,USAGE ON SEQUENCE sisat.seq_entrevistas TO suite WITH GRANT OPTION;


--
-- TOC entry 3881 (class 0 OID 0)
-- Dependencies: 395
-- Name: SEQUENCE seq_envios; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON SEQUENCE sisat.seq_envios FROM suite;
GRANT UPDATE ON SEQUENCE sisat.seq_envios TO suite;
GRANT SELECT,USAGE ON SEQUENCE sisat.seq_envios TO suite WITH GRANT OPTION;


--
-- TOC entry 3882 (class 0 OID 0)
-- Dependencies: 396
-- Name: SEQUENCE seq_escolaridad; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON SEQUENCE sisat.seq_escolaridad FROM suite;
GRANT UPDATE ON SEQUENCE sisat.seq_escolaridad TO suite;
GRANT SELECT,USAGE ON SEQUENCE sisat.seq_escolaridad TO suite WITH GRANT OPTION;


--
-- TOC entry 3883 (class 0 OID 0)
-- Dependencies: 397
-- Name: SEQUENCE seq_experiencias; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON SEQUENCE sisat.seq_experiencias FROM suite;
GRANT UPDATE ON SEQUENCE sisat.seq_experiencias TO suite;
GRANT SELECT,USAGE ON SEQUENCE sisat.seq_experiencias TO suite WITH GRANT OPTION;


--
-- TOC entry 3884 (class 0 OID 0)
-- Dependencies: 398
-- Name: SEQUENCE seq_firmareqper; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON SEQUENCE sisat.seq_firmareqper FROM suite;
GRANT UPDATE ON SEQUENCE sisat.seq_firmareqper TO suite;
GRANT SELECT,USAGE ON SEQUENCE sisat.seq_firmareqper TO suite WITH GRANT OPTION;


--
-- TOC entry 3885 (class 0 OID 0)
-- Dependencies: 399
-- Name: SEQUENCE seq_firmas; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON SEQUENCE sisat.seq_firmas FROM suite;
GRANT UPDATE ON SEQUENCE sisat.seq_firmas TO suite;
GRANT SELECT,USAGE ON SEQUENCE sisat.seq_firmas TO suite WITH GRANT OPTION;


--
-- TOC entry 3886 (class 0 OID 0)
-- Dependencies: 400
-- Name: SEQUENCE seq_habilidades; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON SEQUENCE sisat.seq_habilidades FROM suite;
GRANT UPDATE ON SEQUENCE sisat.seq_habilidades TO suite;
GRANT SELECT,USAGE ON SEQUENCE sisat.seq_habilidades TO suite WITH GRANT OPTION;


--
-- TOC entry 3887 (class 0 OID 0)
-- Dependencies: 401
-- Name: SEQUENCE seq_idiomas; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON SEQUENCE sisat.seq_idiomas FROM suite;
GRANT UPDATE ON SEQUENCE sisat.seq_idiomas TO suite;
GRANT SELECT,USAGE ON SEQUENCE sisat.seq_idiomas TO suite WITH GRANT OPTION;


--
-- TOC entry 3888 (class 0 OID 0)
-- Dependencies: 402
-- Name: SEQUENCE seq_ordenservicios; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON SEQUENCE sisat.seq_ordenservicios FROM suite;
GRANT UPDATE ON SEQUENCE sisat.seq_ordenservicios TO suite;
GRANT SELECT,USAGE ON SEQUENCE sisat.seq_ordenservicios TO suite WITH GRANT OPTION;


--
-- TOC entry 3889 (class 0 OID 0)
-- Dependencies: 403
-- Name: SEQUENCE seq_prospectos; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON SEQUENCE sisat.seq_prospectos FROM suite;
GRANT UPDATE ON SEQUENCE sisat.seq_prospectos TO suite;
GRANT SELECT,USAGE ON SEQUENCE sisat.seq_prospectos TO suite WITH GRANT OPTION;


--
-- TOC entry 3890 (class 0 OID 0)
-- Dependencies: 404
-- Name: SEQUENCE seq_proyectos; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON SEQUENCE sisat.seq_proyectos FROM suite;
GRANT UPDATE ON SEQUENCE sisat.seq_proyectos TO suite;
GRANT SELECT,USAGE ON SEQUENCE sisat.seq_proyectos TO suite WITH GRANT OPTION;


--
-- TOC entry 3891 (class 0 OID 0)
-- Dependencies: 405
-- Name: SEQUENCE seq_vacantes; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON SEQUENCE sisat.seq_vacantes FROM suite;
GRANT UPDATE ON SEQUENCE sisat.seq_vacantes TO suite;
GRANT SELECT,USAGE ON SEQUENCE sisat.seq_vacantes TO suite WITH GRANT OPTION;


--
-- TOC entry 3892 (class 0 OID 0)
-- Dependencies: 406
-- Name: TABLE tsisatappservices; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisatappservices FROM suite;
GRANT ALL ON TABLE sisat.tsisatappservices TO suite WITH GRANT OPTION;


--
-- TOC entry 3893 (class 0 OID 0)
-- Dependencies: 407
-- Name: TABLE tsisatarquitecturas; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisatarquitecturas FROM suite;
GRANT ALL ON TABLE sisat.tsisatarquitecturas TO suite WITH GRANT OPTION;


--
-- TOC entry 3894 (class 0 OID 0)
-- Dependencies: 408
-- Name: TABLE tsisatasignaciones; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisatasignaciones FROM suite;
GRANT ALL ON TABLE sisat.tsisatasignaciones TO suite WITH GRANT OPTION;


--
-- TOC entry 3895 (class 0 OID 0)
-- Dependencies: 409
-- Name: TABLE tsisatcandidatos; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisatcandidatos FROM suite;
GRANT ALL ON TABLE sisat.tsisatcandidatos TO suite WITH GRANT OPTION;


--
-- TOC entry 3896 (class 0 OID 0)
-- Dependencies: 410
-- Name: TABLE tsisatcartaaceptacion; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisatcartaaceptacion FROM suite;
GRANT ALL ON TABLE sisat.tsisatcartaaceptacion TO suite WITH GRANT OPTION;


--
-- TOC entry 3897 (class 0 OID 0)
-- Dependencies: 411
-- Name: TABLE tsisatcartaasignacion; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisatcartaasignacion FROM suite;
GRANT ALL ON TABLE sisat.tsisatcartaasignacion TO suite WITH GRANT OPTION;


--
-- TOC entry 3898 (class 0 OID 0)
-- Dependencies: 412
-- Name: TABLE tsisatcomentcartaasignacion; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisatcomentcartaasignacion FROM suite;
GRANT ALL ON TABLE sisat.tsisatcomentcartaasignacion TO suite WITH GRANT OPTION;


--
-- TOC entry 3899 (class 0 OID 0)
-- Dependencies: 413
-- Name: TABLE tsisatcomentcosteo; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisatcomentcosteo FROM suite;
GRANT ALL ON TABLE sisat.tsisatcomentcosteo TO suite WITH GRANT OPTION;


--
-- TOC entry 3900 (class 0 OID 0)
-- Dependencies: 414
-- Name: TABLE tsisatcomententrevista; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisatcomententrevista FROM suite;
GRANT ALL ON TABLE sisat.tsisatcomententrevista TO suite WITH GRANT OPTION;


--
-- TOC entry 3901 (class 0 OID 0)
-- Dependencies: 415
-- Name: TABLE tsisatcomentvacantes; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisatcomentvacantes FROM suite;
GRANT ALL ON TABLE sisat.tsisatcomentvacantes TO suite WITH GRANT OPTION;


--
-- TOC entry 3902 (class 0 OID 0)
-- Dependencies: 416
-- Name: TABLE tsisatcontrataciones; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisatcontrataciones FROM suite;
GRANT ALL ON TABLE sisat.tsisatcontrataciones TO suite WITH GRANT OPTION;


--
-- TOC entry 3903 (class 0 OID 0)
-- Dependencies: 417
-- Name: TABLE tsisatcotizaciones; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisatcotizaciones FROM suite;
GRANT ALL ON TABLE sisat.tsisatcotizaciones TO suite WITH GRANT OPTION;


--
-- TOC entry 3904 (class 0 OID 0)
-- Dependencies: 418
-- Name: TABLE tsisatcursosycertificados; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisatcursosycertificados FROM suite;
GRANT ALL ON TABLE sisat.tsisatcursosycertificados TO suite WITH GRANT OPTION;


--
-- TOC entry 3905 (class 0 OID 0)
-- Dependencies: 419
-- Name: TABLE tsisatentrevistas; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisatentrevistas FROM suite;
GRANT ALL ON TABLE sisat.tsisatentrevistas TO suite WITH GRANT OPTION;


--
-- TOC entry 3906 (class 0 OID 0)
-- Dependencies: 420
-- Name: TABLE tsisatenviocorreos; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisatenviocorreos FROM suite;
GRANT ALL ON TABLE sisat.tsisatenviocorreos TO suite WITH GRANT OPTION;


--
-- TOC entry 3907 (class 0 OID 0)
-- Dependencies: 421
-- Name: TABLE tsisatescolaridad; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisatescolaridad FROM suite;
GRANT ALL ON TABLE sisat.tsisatescolaridad TO suite WITH GRANT OPTION;


--
-- TOC entry 3908 (class 0 OID 0)
-- Dependencies: 422
-- Name: TABLE tsisatexperienciaslaborales; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisatexperienciaslaborales FROM suite;
GRANT ALL ON TABLE sisat.tsisatexperienciaslaborales TO suite WITH GRANT OPTION;


--
-- TOC entry 3909 (class 0 OID 0)
-- Dependencies: 423
-- Name: TABLE tsisatfirmareqper; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisatfirmareqper FROM suite;
GRANT ALL ON TABLE sisat.tsisatfirmareqper TO suite WITH GRANT OPTION;


--
-- TOC entry 3910 (class 0 OID 0)
-- Dependencies: 424
-- Name: TABLE tsisatframeworks; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisatframeworks FROM suite;
GRANT ALL ON TABLE sisat.tsisatframeworks TO suite WITH GRANT OPTION;


--
-- TOC entry 3911 (class 0 OID 0)
-- Dependencies: 425
-- Name: TABLE tsisathabilidades; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisathabilidades FROM suite;
GRANT ALL ON TABLE sisat.tsisathabilidades TO suite WITH GRANT OPTION;


--
-- TOC entry 3912 (class 0 OID 0)
-- Dependencies: 426
-- Name: TABLE tsisatherramientas; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisatherramientas FROM suite;
GRANT ALL ON TABLE sisat.tsisatherramientas TO suite WITH GRANT OPTION;


--
-- TOC entry 3914 (class 0 OID 0)
-- Dependencies: 427
-- Name: SEQUENCE tsisatherramientas_cod_herramientas_seq; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON SEQUENCE sisat.tsisatherramientas_cod_herramientas_seq FROM suite;
GRANT UPDATE ON SEQUENCE sisat.tsisatherramientas_cod_herramientas_seq TO suite;
GRANT SELECT,USAGE ON SEQUENCE sisat.tsisatherramientas_cod_herramientas_seq TO suite WITH GRANT OPTION;


--
-- TOC entry 3915 (class 0 OID 0)
-- Dependencies: 428
-- Name: TABLE tsisatides; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisatides FROM suite;
GRANT ALL ON TABLE sisat.tsisatides TO suite WITH GRANT OPTION;


--
-- TOC entry 3916 (class 0 OID 0)
-- Dependencies: 429
-- Name: TABLE tsisatidiomas; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisatidiomas FROM suite;
GRANT ALL ON TABLE sisat.tsisatidiomas TO suite WITH GRANT OPTION;


--
-- TOC entry 3917 (class 0 OID 0)
-- Dependencies: 430
-- Name: TABLE tsisatlenguajes; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisatlenguajes FROM suite;
GRANT ALL ON TABLE sisat.tsisatlenguajes TO suite WITH GRANT OPTION;


--
-- TOC entry 3918 (class 0 OID 0)
-- Dependencies: 431
-- Name: TABLE tsisatmaquetados; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisatmaquetados FROM suite;
GRANT ALL ON TABLE sisat.tsisatmaquetados TO suite WITH GRANT OPTION;


--
-- TOC entry 3919 (class 0 OID 0)
-- Dependencies: 432
-- Name: TABLE tsisatmetodologias; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisatmetodologias FROM suite;
GRANT ALL ON TABLE sisat.tsisatmetodologias TO suite WITH GRANT OPTION;


--
-- TOC entry 3920 (class 0 OID 0)
-- Dependencies: 433
-- Name: TABLE tsisatmodelados; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisatmodelados FROM suite;
GRANT ALL ON TABLE sisat.tsisatmodelados TO suite WITH GRANT OPTION;


--
-- TOC entry 3921 (class 0 OID 0)
-- Dependencies: 434
-- Name: TABLE tsisatordenservicio; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisatordenservicio FROM suite;
GRANT ALL ON TABLE sisat.tsisatordenservicio TO suite WITH GRANT OPTION;


--
-- TOC entry 3922 (class 0 OID 0)
-- Dependencies: 435
-- Name: TABLE tsisatpatrones; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisatpatrones FROM suite;
GRANT ALL ON TABLE sisat.tsisatpatrones TO suite WITH GRANT OPTION;


--
-- TOC entry 3923 (class 0 OID 0)
-- Dependencies: 436
-- Name: TABLE tsisatprospectos; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisatprospectos FROM suite;
GRANT ALL ON TABLE sisat.tsisatprospectos TO suite WITH GRANT OPTION;


--
-- TOC entry 3924 (class 0 OID 0)
-- Dependencies: 437
-- Name: TABLE tsisatprospectos_idiomas; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisatprospectos_idiomas FROM suite;
GRANT ALL ON TABLE sisat.tsisatprospectos_idiomas TO suite WITH GRANT OPTION;


--
-- TOC entry 3925 (class 0 OID 0)
-- Dependencies: 438
-- Name: TABLE tsisatprotocolos; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisatprotocolos FROM suite;
GRANT ALL ON TABLE sisat.tsisatprotocolos TO suite WITH GRANT OPTION;


--
-- TOC entry 3926 (class 0 OID 0)
-- Dependencies: 439
-- Name: TABLE tsisatproyectos; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisatproyectos FROM suite;
GRANT ALL ON TABLE sisat.tsisatproyectos TO suite WITH GRANT OPTION;


--
-- TOC entry 3927 (class 0 OID 0)
-- Dependencies: 440
-- Name: TABLE tsisatqa; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisatqa FROM suite;
GRANT ALL ON TABLE sisat.tsisatqa TO suite WITH GRANT OPTION;


--
-- TOC entry 3928 (class 0 OID 0)
-- Dependencies: 441
-- Name: TABLE tsisatrepositoriolibrerias; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisatrepositoriolibrerias FROM suite;
GRANT ALL ON TABLE sisat.tsisatrepositoriolibrerias TO suite WITH GRANT OPTION;


--
-- TOC entry 3929 (class 0 OID 0)
-- Dependencies: 442
-- Name: TABLE tsisatrepositorios; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisatrepositorios FROM suite;
GRANT ALL ON TABLE sisat.tsisatrepositorios TO suite WITH GRANT OPTION;


--
-- TOC entry 3930 (class 0 OID 0)
-- Dependencies: 443
-- Name: TABLE tsisatsgbd; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisatsgbd FROM suite;
GRANT ALL ON TABLE sisat.tsisatsgbd TO suite WITH GRANT OPTION;


--
-- TOC entry 3931 (class 0 OID 0)
-- Dependencies: 444
-- Name: TABLE tsisatso; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisatso FROM suite;
GRANT ALL ON TABLE sisat.tsisatso TO suite WITH GRANT OPTION;


--
-- TOC entry 3932 (class 0 OID 0)
-- Dependencies: 445
-- Name: TABLE tsisatvacantes; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisatvacantes FROM suite;
GRANT ALL ON TABLE sisat.tsisatvacantes TO suite WITH GRANT OPTION;


--
-- TOC entry 2486 (class 826 OID 45494)
-- Name: DEFAULT PRIVILEGES FOR SEQUENCES; Type: DEFAULT ACL; Schema: sisat; Owner: suite
--

ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sisat REVOKE ALL ON SEQUENCES  FROM suite;
ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sisat GRANT SELECT,USAGE ON SEQUENCES  TO suite WITH GRANT OPTION;


--
-- TOC entry 2487 (class 826 OID 45495)
-- Name: DEFAULT PRIVILEGES FOR TYPES; Type: DEFAULT ACL; Schema: sisat; Owner: suite
--

ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sisat REVOKE ALL ON TYPES  FROM PUBLIC;
ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sisat REVOKE ALL ON TYPES  FROM suite;
ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sisat GRANT ALL ON TYPES  TO suite WITH GRANT OPTION;


--
-- TOC entry 2488 (class 826 OID 45496)
-- Name: DEFAULT PRIVILEGES FOR FUNCTIONS; Type: DEFAULT ACL; Schema: sisat; Owner: suite
--

ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sisat REVOKE ALL ON FUNCTIONS  FROM PUBLIC;
ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sisat REVOKE ALL ON FUNCTIONS  FROM suite;
ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sisat GRANT ALL ON FUNCTIONS  TO suite WITH GRANT OPTION;


--
-- TOC entry 2489 (class 826 OID 45497)
-- Name: DEFAULT PRIVILEGES FOR TABLES; Type: DEFAULT ACL; Schema: sisat; Owner: suite
--

ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sisat REVOKE ALL ON TABLES  FROM suite;
ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sisat GRANT ALL ON TABLES  TO suite WITH GRANT OPTION;


-- Completed on 2019-11-14 12:43:46

--
-- PostgreSQL database dump complete
--

