CREATE OR REPLACE FUNCTION "sgnom"."buscar_incidencias_por_empleado"("idempleado" int4)
  RETURNS TABLE("idincidencia" int4, "fechaalta" date, "clave" varchar, "incidencia" varchar, "idtipo" bpchar, "desctipo" text, "cantidad" int2, "actividad" varchar, "comentarios" text, "reportaid" int4, "reportanb" text, "autorizaid" int4, "autorizanb" text, "area" varchar, "perfil" varchar, "puesto" varchar, "detallefechas" text, "montoincidencia" numeric, "montopagado" numeric, "estatus" bool) AS $BODY$

BEGIN 
RETURN QUERY 

	SELECT incidencias.cod_incidenciaid idincidencia,
		incidencias.aud_feccreacion fechaalta,
		catincidencias.cod_claveincidencia clave,
		catincidencias.cod_nbincidencia incidencia,
		catincidencias.cod_tipoincidencia idtipo,
		CASE 
			WHEN catincidencias.cod_tipoincidencia='1' THEN
			 'HORAS'
			WHEN catincidencias.cod_tipoincidencia='2' THEN
			 'DIAS' 
		  WHEN catincidencias.cod_tipoincidencia='3' THEN
			 'ACTIVIDAD' 
			ELSE
			 'NO DATA'
		END desc_tipo,
		incidencias.cnu_cantidad cantidad,
		incidencias.des_actividad actividad,
		incidencias.txt_comentarios comentarios,
		incidencias.cod_empreporta_fk reportaid,
		(SELECT CONCAT(rhempleados.des_nombre ||' ', rhempleados.des_nombres ,' ' || rhempleados.des_apepaterno || ' ' || rhempleados.des_apematerno) 
		FROM sgrh.tsgrhempleados rhempleados
			JOIN sgrh.tsgrhareas rhareas
			USING(cod_area)
		WHERE rhempleados.cod_empleado = idempleado
		) reportanb,
		incidencias.cod_empautoriza_fk autorizaid,
		(SELECT CONCAT(rhempleados.des_nombre ||' ', rhempleados.des_nombres ,' ' || rhempleados.des_apepaterno || ' ' || rhempleados.des_apematerno) 
		FROM sgrh.tsgrhempleados rhempleados
			JOIN sgrh.tsgrhareas rhareas
			USING(cod_area)
		WHERE rhempleados.cod_empleado = incidencias.cod_empautoriza_fk
		) autorizanb,
		(SELECT rhareas.des_nbarea
		FROM sgrh.tsgrhempleados rhempleados
			JOIN sgrh.tsgrhareas rhareas
			USING(cod_area)
		WHERE rhempleados.cod_empleado = idempleado
		) area,
		catincidencias.cod_perfilincidencia perfil,
		(SELECT rhpuestos.des_puesto
			FROM sgrh.tsgrhempleados rhempleados
				JOIN sgrh.tsgrhpuestos rhpuestos
				USING(cod_puesto)
			WHERE rhempleados.cod_empleado = idempleado
		) puesto,
		CAST(incidencias.xml_detcantidad AS TEXT) detallefechas,
		catincidencias.imp_monto montoincidencia,
		incidencias.imp_monto montopagado,
		incidencias.bol_validacion_rh estatus --validacion
FROM sgnom.tsgnomincidencia incidencias
JOIN sgnom.tsgnomcatincidencia catincidencias 
ON incidencias.cod_catincidenciaid_fk = catincidencias.cod_catincidenciaid
WHERE incidencias.cod_empreporta_fk = (SELECT nomemp.cod_empleadoid 
											FROM sgnom.tsgnomempleados nomemp WHERE nomemp.cod_empleado_fk = idempleado) 
	AND incidencias.cod_quincenaid_fk = (SELECT nomquincena.cod_quincenaid 
											FROM sgnom.tsgnomquincena nomquincena
											WHERE nomquincena.fec_inicio < CURRENT_DATE AND nomquincena.fec_fin > CURRENT_DATE)
	AND incidencias.bol_estatus = true 
ORDER BY 1 DESC, 2;

END;
$BODY$
  LANGUAGE plpgsql