--
-- PostgreSQL database dump
--

-- Dumped from database version 11.4 (Debian 11.4-1.pgdg90+1)
-- Dumped by pg_dump version 11.4 (Debian 11.4-1.pgdg90+1)

-- Started on 2019-08-25 22:58:35 CDT

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 4718 (class 1262 OID 16387)
-- Name: suite; Type: DATABASE; Schema: -; Owner: suite
--

CREATE DATABASE suite WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'es_ES.UTF-8' LC_CTYPE = 'es_ES.UTF-8';


ALTER DATABASE suite OWNER TO suite;

\connect suite

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 13 (class 2615 OID 16449)
-- Name: sgco; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA sgco;


ALTER SCHEMA sgco OWNER TO postgres;

--
-- TOC entry 4719 (class 0 OID 0)
-- Dependencies: 13
-- Name: SCHEMA sgco; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON SCHEMA sgco IS 'Sistema de Gestion de Conocimiento de la Organizacion.';


--
-- TOC entry 10 (class 2615 OID 20872)
-- Name: sgnom; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA sgnom;


ALTER SCHEMA sgnom OWNER TO postgres;

--
-- TOC entry 4721 (class 0 OID 0)
-- Dependencies: 10
-- Name: SCHEMA sgnom; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON SCHEMA sgnom IS 'Sistema de Gestion de Nomina.';


--
-- TOC entry 11 (class 2615 OID 18794)
-- Name: sgrh; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA sgrh;


ALTER SCHEMA sgrh OWNER TO postgres;

--
-- TOC entry 4723 (class 0 OID 0)
-- Dependencies: 11
-- Name: SCHEMA sgrh; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON SCHEMA sgrh IS 'ESQUEMA QUE CONTIENE LAS TABLAS DE SISTEMA DE GESTION DE RECURSOS HUMANOS';


--
-- TOC entry 8 (class 2615 OID 16423)
-- Name: sgrt; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA sgrt;


ALTER SCHEMA sgrt OWNER TO postgres;

--
-- TOC entry 7 (class 2615 OID 21955)
-- Name: sisat; Type: SCHEMA; Schema: -; Owner: suite
--

CREATE SCHEMA sisat;


ALTER SCHEMA sisat OWNER TO suite;

--
-- TOC entry 4724 (class 0 OID 0)
-- Dependencies: 7
-- Name: SCHEMA sisat; Type: COMMENT; Schema: -; Owner: suite
--

COMMENT ON SCHEMA sisat IS 'Esquema que contiene las tablas del Sistema de Seleccion y Adquisicion de Talentos';


--
-- TOC entry 2 (class 3079 OID 16428)
-- Name: tablefunc; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS tablefunc WITH SCHEMA public;


--
-- TOC entry 4725 (class 0 OID 0)
-- Dependencies: 2
-- Name: EXTENSION tablefunc; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION tablefunc IS 'Functions that manipulate whole tables, including crosstab';


--
-- TOC entry 1119 (class 1247 OID 18796)
-- Name: edo_encuesta; Type: TYPE; Schema: sgrh; Owner: postgres
--

CREATE TYPE sgrh.edo_encuesta AS ENUM (
    '--',
    'En proceso',
    'Corregido',
    'Aceptado'
);


ALTER TYPE sgrh.edo_encuesta OWNER TO postgres;

--
-- TOC entry 1122 (class 1247 OID 16482)
-- Name: destinatario; Type: TYPE; Schema: sgrt; Owner: postgres
--

CREATE TYPE sgrt.destinatario AS ENUM (
    'USR',
    'EMPLEADO',
    'GRUPO',
    'DEPTO'
);


ALTER TYPE sgrt.destinatario OWNER TO postgres;

--
-- TOC entry 875 (class 1247 OID 16492)
-- Name: edoticket; Type: TYPE; Schema: sgrt; Owner: postgres
--

CREATE TYPE sgrt.edoticket AS ENUM (
    'Abierto',
    'Cerrado'
);


ALTER TYPE sgrt.edoticket OWNER TO postgres;

--
-- TOC entry 888 (class 1247 OID 16498)
-- Name: encriptacion; Type: TYPE; Schema: sgrt; Owner: postgres
--

CREATE TYPE sgrt.encriptacion AS ENUM (
    'NONE',
    'SSL'
);


ALTER TYPE sgrt.encriptacion OWNER TO postgres;

--
-- TOC entry 889 (class 1247 OID 16504)
-- Name: estatus; Type: TYPE; Schema: sgrt; Owner: postgres
--

CREATE TYPE sgrt.estatus AS ENUM (
    'Enviado',
    'Pendiente'
);


ALTER TYPE sgrt.estatus OWNER TO postgres;

--
-- TOC entry 892 (class 1247 OID 16510)
-- Name: estatus_compromiso; Type: TYPE; Schema: sgrt; Owner: postgres
--

CREATE TYPE sgrt.estatus_compromiso AS ENUM (
    'Pendiente',
    'Terminado'
);


ALTER TYPE sgrt.estatus_compromiso OWNER TO postgres;

--
-- TOC entry 895 (class 1247 OID 16516)
-- Name: modulo; Type: TYPE; Schema: sgrt; Owner: postgres
--

CREATE TYPE sgrt.modulo AS ENUM (
    'SGRT',
    'SSV'
);


ALTER TYPE sgrt.modulo OWNER TO postgres;

--
-- TOC entry 898 (class 1247 OID 16522)
-- Name: origencontac; Type: TYPE; Schema: sgrt; Owner: postgres
--

CREATE TYPE sgrt.origencontac AS ENUM (
    'Web',
    'Email',
    'Reunion',
    'Telefono',
    'Otro'
);


ALTER TYPE sgrt.origencontac OWNER TO postgres;

--
-- TOC entry 901 (class 1247 OID 16534)
-- Name: prioridad; Type: TYPE; Schema: sgrt; Owner: postgres
--

CREATE TYPE sgrt.prioridad AS ENUM (
    'Alta',
    'Media',
    'Baja'
);


ALTER TYPE sgrt.prioridad OWNER TO postgres;

--
-- TOC entry 904 (class 1247 OID 16542)
-- Name: protocolo; Type: TYPE; Schema: sgrt; Owner: postgres
--

CREATE TYPE sgrt.protocolo AS ENUM (
    'POP',
    'IMAP'
);


ALTER TYPE sgrt.protocolo OWNER TO postgres;

--
-- TOC entry 907 (class 1247 OID 16548)
-- Name: tipo; Type: TYPE; Schema: sgrt; Owner: postgres
--

CREATE TYPE sgrt.tipo AS ENUM (
    'ReunionAgendada',
    'ReunionCancelada',
    'NuevaSolicitud',
    'SolicitudPagada',
    'ViaticoCancelado'
);


ALTER TYPE sgrt.tipo OWNER TO postgres;

--
-- TOC entry 910 (class 1247 OID 16560)
-- Name: tipo_compromiso; Type: TYPE; Schema: sgrt; Owner: postgres
--

CREATE TYPE sgrt.tipo_compromiso AS ENUM (
    'Acuerdo',
    'Pendiente'
);


ALTER TYPE sgrt.tipo_compromiso OWNER TO postgres;

--
-- TOC entry 470 (class 1255 OID 39936)
-- Name: actualizar_comentarios_incidencia(integer, text, integer); Type: FUNCTION; Schema: sgnom; Owner: postgres
--

CREATE FUNCTION sgnom.actualizar_comentarios_incidencia(incidenciaid integer, comentarios text, reporta integer) RETURNS boolean
    LANGUAGE sql
    AS $$
		UPDATE sgnom.tsgnomincidencia 
			SET txt_comentarios = comentarios,
			aud_codmodificadopor = reporta,
			aud_fecmodificacion = CURRENT_DATE
		WHERE cod_incidenciaid = incidenciaid RETURNING TRUE; 
$$;


ALTER FUNCTION sgnom.actualizar_comentarios_incidencia(incidenciaid integer, comentarios text, reporta integer) OWNER TO postgres;

--
-- TOC entry 471 (class 1255 OID 39937)
-- Name: actualizar_importe_incidencia(integer, numeric, integer); Type: FUNCTION; Schema: sgnom; Owner: postgres
--

CREATE FUNCTION sgnom.actualizar_importe_incidencia(incidenciaid integer, importe numeric, reporta integer) RETURNS boolean
    LANGUAGE sql
    AS $$
		UPDATE sgnom.tsgnomincidencia 
			SET imp_monto = importe,
			aud_codmodificadopor = reporta,
			aud_fecmodificacion = CURRENT_DATE
		WHERE "cod_incidenciaid" = incidenciaid RETURNING TRUE; 
$$;


ALTER FUNCTION sgnom.actualizar_importe_incidencia(incidenciaid integer, importe numeric, reporta integer) OWNER TO postgres;

--
-- TOC entry 472 (class 1255 OID 39938)
-- Name: buscar_incidencias_por_empleado(integer); Type: FUNCTION; Schema: sgnom; Owner: postgres
--

CREATE FUNCTION sgnom.buscar_incidencias_por_empleado(idempleado integer) RETURNS TABLE(idincidencia integer, clave character varying, incidencia character varying, idtipo character, desctipo text, cantidad smallint, actividad character varying, comentarios text, reporta integer, empleado text, area character varying, perfil character varying, puesto character varying)
    LANGUAGE plpgsql
    AS $$

BEGIN 
RETURN QUERY 

	SELECT incidencias.cod_incidenciaid idincidencia,
		catincidencias.cod_claveincidencia clave,
		catincidencias.cod_nbincidencia incidencia,
		catincidencias.cod_tipoincidencia idtipo,
		CASE 
			WHEN catincidencias.cod_tipoincidencia='1' THEN
			 'HORAS'
			WHEN catincidencias.cod_tipoincidencia='2' THEN
			 'DIAS' 
		  WHEN catincidencias.cod_tipoincidencia='3' THEN
			 'ACTIVIDAD' 
			ELSE
			 'NO DATA'
		END desc_tipo,
		incidencias.cnu_cantidad cantidad,
		incidencias.des_actividad actividad,
		incidencias.txt_comentarios comentarios,
		incidencias.cod_empreporta_fk reporta,
		(SELECT CONCAT(rhempleados.des_nombre ||' ', rhempleados.des_nombres ,' ' || rhempleados.des_apepaterno || ' ' || rhempleados.des_apematerno) 
		FROM sgrh.tsgrhempleados rhempleados
			JOIN sgrh.tsgrhareas rhareas
			USING(cod_area)
		WHERE rhempleados.cod_empleado = 17
		) empleado,
		(SELECT rhareas.des_nbarea
		FROM sgrh.tsgrhempleados rhempleados
			JOIN sgrh.tsgrhareas rhareas
			USING(cod_area)
		WHERE rhempleados.cod_empleado = 17
		) area,
		catincidencias.cod_perfilincidencia perfil,
		(SELECT rhpuestos.des_puesto
			FROM sgrh.tsgrhempleados rhempleados
				JOIN sgrh.tsgrhpuestos rhpuestos
				USING(cod_puesto)
			WHERE rhempleados.cod_empleado = 17
		) puesto
FROM sgnom.tsgnomincidencia incidencias
JOIN sgnom.tsgnomcatincidencia catincidencias 
ON incidencias.cod_catincidenciaid_fk = catincidencias.cod_catincidenciaid
WHERE incidencias.cod_empreporta_fk = (SELECT nomemp.cod_empleadoid 
											FROM sgnom.tsgnomempleados nomemp WHERE nomemp.cod_empleado_fk = 17) 
	AND incidencias.cod_quincenaid_fk = (SELECT nomquincena.cod_quincenaid 
											FROM sgnom.tsgnomquincena nomquincena
											WHERE nomquincena.fec_inicio < CURRENT_DATE AND nomquincena.fec_fin > CURRENT_DATE)
	AND incidencias.bol_estatus = true 
ORDER BY idincidencia;

END;
$$;


ALTER FUNCTION sgnom.buscar_incidencias_por_empleado(idempleado integer) OWNER TO postgres;

--
-- TOC entry 473 (class 1255 OID 39939)
-- Name: eliminar_incidencia_por_empleado(integer, integer); Type: FUNCTION; Schema: sgnom; Owner: postgres
--

CREATE FUNCTION sgnom.eliminar_incidencia_por_empleado(incidenciaid integer, reporta integer) RETURNS boolean
    LANGUAGE sql
    AS $$
	--No es eliminacion fisica, actualiza el estatus a 'false'
	UPDATE sgnom.tsgnomincidencia 
		SET bol_estatus = 'f',
		aud_codmodificadopor = reporta,
		aud_fecmodificacion = CURRENT_DATE
	WHERE "cod_incidenciaid" = incidenciaid	RETURNING TRUE;

$$;


ALTER FUNCTION sgnom.eliminar_incidencia_por_empleado(incidenciaid integer, reporta integer) OWNER TO postgres;

--
-- TOC entry 478 (class 1255 OID 40097)
-- Name: fn_calcula_importes_nomina(integer, character varying, numeric); Type: FUNCTION; Schema: sgnom; Owner: postgres
--

CREATE FUNCTION sgnom.fn_calcula_importes_nomina(idnomina integer, vempleados character varying, todos numeric) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
    DECLARE
        vTotalPercepcion          NUMERIC(10,2):=0;
        vTotalDeduccion           NUMERIC(10,2):=0;
        vImporteTotal             NUMERIC(10,2):=0;
        vNumRegistros             INTEGER:=0;
        vNumRegistros2            INTEGER:=0;
        vBandera                  INTEGER;
        vIdEmpleadoNomina         INTEGER;
        vTotalPercepcionC         NUMERIC(10,2):=0;
        vTotalDeduccionC          NUMERIC(10,2):=0;
        vImporteTotalC            NUMERIC(10,2):=0;
        vNumeroEmpleados          INTEGER;
        vNomEmpleado              NUMERIC(10,2);
        vTotalEmpleadosNomina     INTEGER;
        vTotalEmpleadosCalculados INTEGER;
        ERR_CODE       INTEGER;
        ERR_MSG        VARCHAR(250);
        a_count INTEGER;
        b_count INTEGER;
        --CURSORES

        C5 CURSOR FOR
            SELECT EQ.cod_empquincenaid
            FROM sgnom.tsgnomempquincena EQ
            WHERE EQ.cod_cabeceraid_fk = idNomina
            ORDER BY EQ.cod_empquincenaid;

        C6 CURSOR FOR
            SELECT EQ.cod_empquincenaid
            FROM sgnom.tsgnomempquincena EQ
            WHERE EQ.cod_cabeceraid_fk = idNomina
            AND EQ.bol_estatusemp = 'f'
            ORDER BY EQ.cod_empquincenaid;

    BEGIN
        OPEN C5;
        RAISE INFO 'abre cursor C1';
        LOOP 
            FETCH C5 INTO vIdEmpleadoNomina;
            EXIT WHEN not found;
            RAISE INFO 'empleado %, idNomina %', vIdEmpleadoNomina,idNomina;
            --Se realiza la suma de los conceptos que son percepciones
            SELECT SUM(CQ.imp_concepto) INTO vTotalPercepcion
            FROM sgnom.tsgnomcncptoquinc CQ, sgnom.tsgnomconcepto CO, sgnom.tsgnomempquincena EQ
            WHERE CO.cod_tipoconceptoid_fk = 2
            AND EQ.cod_cabeceraid_fk = idNomina
            AND EQ.cod_empquincenaid = vIdEmpleadoNomina
            AND EQ.cod_empquincenaid = CQ.cod_empquincenaid_fk
            AND CO.cod_conceptoid = CQ.cod_conceptoid_fk;
            RAISE INFO 'vTotalPercepcion %', vTotalPercepcion;
            IF(vTotalPercepcion        IS NOT NULL) THEN
                vTotalPercepcionC        :=vTotalPercepcionC+vTotalPercepcion;
            ELSIF(vTotalPercepcion     IS NULL) THEN
                vTotalPercepcion         :=0;
                vTotalPercepcionC        :=vTotalPercepcionC+vTotalPercepcion;
            END IF;
            RAISE INFO 'vTotalPercepcionC %', vTotalPercepcionC;
            --Se realiza la suma de los conceptos que son deducciones
            SELECT SUM(CQ.imp_concepto) INTO vTotalDeduccion
            FROM sgnom.tsgnomcncptoquinc CQ, sgnom.tsgnomconcepto CO, sgnom.tsgnomempquincena EQ
            WHERE CO.cod_tipoconceptoid_fk = 2
            AND EQ.cod_cabeceraid_fk = idNomina
            AND EQ.cod_empleadoid_fk = vIdEmpleadoNomina
            AND EQ.cod_empquincenaid = CQ.cod_empquincenaid_fk
            AND CO.cod_conceptoid = CQ.cod_conceptoid_fk;
            RAISE INFO 'vTotalDeduccion %', vTotalDeduccion;
            IF(vTotalDeduccion         IS NOT NULL) THEN
                vTotalDeduccionC         :=vTotalDeduccionC+vTotalDeduccion;
            ELSIF (vTotalDeduccion     IS NULL) THEN
                vTotalDeduccion          :=0;
                vTotalDeduccionC         :=vTotalDeduccionC+vTotalDeduccion;
            END IF;
            RAISE INFO 'vTotalDeduccionC %', vTotalDeduccionC;
            IF(vTotalPercepcion IS NOT NULL AND vTotalDeduccion IS NOT NULL) THEN
                SELECT (vTotalPercepcion - vTotalDeduccion) INTO vImporteTotal ;
            ELSIF(vTotalPercepcion IS NULL AND vTotalDeduccion IS NOT NULL) THEN
                vImporteTotal        :=vTotalDeduccion;
            ELSIF(vTotalPercepcion IS NOT NULL AND vTotalDeduccion IS NULL) THEN
                vImporteTotal        :=vTotalPercepcion;
            END IF;
            
            IF(vImporteTotal IS NOT NULL) THEN
                vImporteTotalC :=vImporteTotalC+vImporteTotal;
            ELSE
                vImporteTotalC:=0;
            END IF;
            --Se actualizan los importes de cada empleado asi como el estatus
            UPDATE sgnom.tsgnomempquincena
            SET imp_totpercepcion     =vTotalPercepcion,
                imp_totdeduccion        =vTotalDeduccion,
                imp_totalemp          =vImporteTotal
                --bol_estatusemp ='B'
            WHERE cod_empquincenaid = vIdEmpleadoNomina
            AND cod_cabeceraid_fk = idNomina;
            GET DIAGNOSTICS a_count = ROW_COUNT;
			RAISE INFO 'a_count %', a_count;
            IF (a_count>0) THEN 
                vNumRegistros := vNumRegistros + 1 ;
				RAISE INFO 'vNumRegistros %', vNumRegistros;
            END IF;
            --COMMIT;
        END LOOP;
        CLOSE C5;
        UPDATE sgnom.tsgnomcabecera
        SET imp_totpercepcion = vTotalPercepcionC,
        imp_totdeduccion = vTotalDeduccionC,
        imp_totalemp = vImporteTotalC,
        cod_estatusnomid_fk = 2
        WHERE cod_cabeceraid = idNomina
        AND (cod_estatusnomid_fk=1 OR cod_estatusnomid_fk=2);
        GET DIAGNOSTICS b_count = ROW_COUNT;
        IF (b_count >0) THEN
            vNumRegistros2      := vNumRegistros2 + 1 ;
        END IF; 

        IF (vNumRegistros > 0 AND vNumRegistros2 > 0) THEN
            vBandera := 1;
        ELSIF (vNumRegistros = 0 OR vNumRegistros2 = 0) THEN
            vBandera := 0;
        ELSE
            vBandera:=2;
        END IF;
    RETURN vBandera;
		EXCEPTION
		WHEN NO_DATA_FOUND THEN
			RAISE EXCEPTION '% exp no data found', vIdEmpleado;
			RETURN NULL;
    END;
$$;


ALTER FUNCTION sgnom.fn_calcula_importes_nomina(idnomina integer, vempleados character varying, todos numeric) OWNER TO postgres;

--
-- TOC entry 467 (class 1255 OID 40091)
-- Name: fn_calcula_nomina(integer); Type: FUNCTION; Schema: sgnom; Owner: postgres
--

CREATE FUNCTION sgnom.fn_calcula_nomina(vidnomina integer) RETURNS integer
    LANGUAGE plpgsql
    AS $_$
    DECLARE
    --rtSueldoBase INTEGER;
    --vFormula CHARACTER VARYING; --valor de la formula
    --vArgumento CHARACTER VARYING; --valor del argumento
    --vNombreArgumento CHARACTER VARYING; --valor del argumento
    --vNumeroArgumentos INTEGER; --numero de argumentos
    --VARIABLES
    total INTEGER;
    vIdNomEmpPla01 INTEGER;
    vIdConEmpPla01 INTEGER;
    vIdConcepto01fk INTEGER;
    vNumRegistros INTEGER:=0;
    vBandera INTEGER;
    vFormula VARCHAR(150);
    vArgumento VARCHAR(100);
    vNumeroArgumentos INTEGER;
    vFuncionOracle VARCHAR(100);
    vDiasLaborados INTEGER;
    vCadenaEjecutar VARCHAR(500);
    vFormulaEjecutar VARCHAR(500);
    vTabulador INTEGER:=0;
    vIdEmpleado INTEGER;
    vIdPlaza INTEGER;
    vNumeroQuincena INTEGER;
    vSueldoBase NUMERIC(10,2):=0.0;
    vConstante NUMERIC(10,2);
    vFormula2 VARCHAR(150);
    vImporteActualizado NUMERIC(10,2);
    vCadenaImporte VARCHAR(250);
    vXML VARCHAR(4000);
    vNombreConcepto VARCHAR(150);
    vValorArgumento NUMERIC(10,2);
    vNombreArgumento VARCHAR(100);
    vCompensasionG INTEGER:=0;
    vNumeroEmpleados INTEGER;
    vNomEmpleado INTEGER;
    vBanderaImporte INTEGER:=0;
    vIdManTer INTEGER;
    vPorcentajeManTer INTEGER;
    vClaveConcepto VARCHAR(20);
    vClaveConcepto2 VARCHAR(20);
    vApoyoVehicular INTEGER;
    vAportacionSSIGF INTEGER;
    vPrimaVacacional INTEGER;
    vFormulaCICAS VARCHAR(150);
    vFormulaCICAC VARCHAR(150);
    vNumeroArgumentosCAS INTEGER;
    vSueldoDiario INTEGER:=0;
    vExisteAguinaldo INTEGER;
    vIdAguinaldo INTEGER;
    vIdQuincena INTEGER;
    vFaltasSueldoEst INTEGER:=0;
    vFaltasSueldoEv INTEGER:=0;
    vFaltasSueldoEstAA INTEGER:=0;
    vFaltasCompEst INTEGER:=0;
    vFaltasCompEv INTEGER:=0;
    vFaltasCompEstAA INTEGER:=0;
    vISRSueldo INTEGER;
    vISRCompGaran INTEGER;
    vISRSSI INTEGER;
    vISRApoyoVehic INTEGER;
    vISRPrimaVac INTEGER;
    vValorQuinquenio INTEGER:=0;
    vSueldoCompactado INTEGER:=0;
    vSueldoBimestral INTEGER:=0;
    vQuinquenioBimestral INTEGER:=0;
    vSumaSRCEAYV INTEGER;
    vSueldoBimestralEV INTEGER;
    vQuinquenioBimestralEV INTEGER;
    vSumaSRCEAYVEV INTEGER;
    vSueldoCompactadoEV INTEGER;
    vPensionAlimenticia INTEGER;
    vDespensa INTEGER:=0;
    xmlDesglose VARCHAR(3000);
    --cursorPrimaVacacional SYS_REFCURSOR;
    --xmlApoyoVehicular VARCHAR(3000);
    --cursorApoyoVehicular SYS_REFCURSOR;
    -- xmlISRSSI VARCHAR(3000);
    --cursorISRSSI SYS_REFCURSOR;
    vSumaSARP INTEGER;
    vSumaFOVP INTEGER;
    vSumaSRCEAYVPAT INTEGER;
    vPorcentajePension INTEGER;
    vISREstDesemp INTEGER;
    vISRHonorarios INTEGER;
    vValorEstimulo INTEGER;
    vNumeroFaltas INTEGER;
    vDiasLicencia INTEGER;
    ERR_CODE INTEGER;
    ERR_MSG VARCHAR(250);
    vAhorroSolidario INTEGER;
    DecimaImpar INTEGER;
    vNumeroQuincenaBIM INTEGER;
    vTipoNominaBIM INTEGER;
    vEjercicioBIM INTEGER;
    vBanderaSegBaja INTEGER;
    vValISREstDesempA INTEGER;
    vExisteAnt INTEGER;
    vSueldoAnt INTEGER;
    vCompAnt INTEGER;
    vDiasLabAnterior INTEGER;
    vFechaNomina INTEGER;
    vImpLicMedSueldo INTEGER;
    vBanderaEmpNeg INTEGER;
	a_count INTEGER;
    cncptoquincid INTEGER;
    ---- CURSORES
    C1 CURSOR FOR
        SELECT  EQ.cod_empquincenaid,EQ.cod_empleadoid_fk,Q.cnu_numquincena,Q.cod_quincenaid, --CQ.cod_conceptoid_fk
				CQ.cod_cncptoquincid, CQ.cod_empquincenaid_fk, CQ.cod_conceptoid_fk, CO.cod_nbconcepto, CO.cod_claveconcepto
        FROM sgnom.tsgnomempquincena EQ
        INNER JOIN sgnom.tsgnomcabecera C 
            ON C.cod_cabeceraid = EQ.cod_cabeceraid_fk
        INNER JOIN sgnom.tsgnomquincena Q
            ON Q.cod_quincenaid = C.cod_quincenaid_fk
		INNER JOIN sgnom.tsgnomcncptoquinc CQ 
			ON CQ.cod_empquincenaid_fk = EQ.cod_empquincenaid
		INNER JOIN sgnom.tsgnomconcepto CO 
		    ON CO.cod_conceptoid = CQ.cod_conceptoid_fk
        WHERE C.cod_cabeceraid = vIdNomina
		AND CO.cod_calculoid_fk = 2
        AND bol_estatusemp != 'f'
        ORDER BY CO.cod_tipoconceptoid_fk, CO.cnu_prioricalculo, EQ.cod_empquincenaid;
    
    C3 CURSOR FOR
        SELECT CQ.cod_cncptoquincid, CQ.cod_empquincenaid_fk, CQ.cod_conceptoid_fk, CO.cod_nbconcepto, CO.cod_claveconcepto
        FROM sgnom.tsgnomcncptoquinc CQ
        INNER JOIN sgnom.tsgnomconcepto CO ON CO.cod_conceptoid = CQ.cod_conceptoid_fk
        WHERE CO.cod_calculoid_fk = 2
        ORDER BY CO.cod_tipoconceptoid_fk, CO.cnu_prioricalculo;

    C4 CURSOR FOR
        SELECT des_formula
        FROM sgnom.tsgnomformula
        WHERE cod_formulaid =
                (SELECT CO.cod_formulaid_fk 
                FROM sgnom.tsgnomconcepto CO
                WHERE CO.cod_conceptoid = vIdConcepto01fk );

    C10 CURSOR FOR
        SELECT des_formula
        FROM sgnom.tsgnomformula
        WHERE cod_formulaid = 9;

    BEGIN
        OPEN C1;
        RAISE INFO 'abre cursor C1';
        LOOP 
            FETCH C1 INTO vIdNomEmpPla01, vIdEmpleado,vNumeroQuincena,vIdQuincena,cncptoquincid, vIdConEmpPla01,vIdConcepto01fk,vNombreConcepto,vClaveConcepto;
            EXIT WHEN not found;
            OPEN C4;
            RAISE INFO 'abre cursor C10 -> C4';
            LOOP
                FETCH C4 INTO vFormula;
                EXIT WHEN not found;
                vFormula2 :=vFormula;
                vNumeroArgumentos:=(SELECT COUNT(*)
                                    FROM regexp_matches(vFormula, ':[^#0-9/*+$%-:]+', 'g'));
                RAISE INFO 'vformula %', vFormula;
                RAISE INFO 'vNumeroArgumentos %', vNumeroArgumentos;
                FOR i IN 1 .. vNumeroArgumentos
                LOOP
                    SELECT regexp_replace(
                        (SELECT contenido
                        FROM
                        (SELECT contenido, row_number() OVER () AS indice --COUNT(*)
                        FROM 
                        regexp_matches(vFormula, ':[^#0-9/*+$%-:]+', 'g') AS contenido
                        ) AS todo
                        WHERE indice = i)::varchar, '[{:}"]', '', 'g') 
                        --WHERE indice = )::"varchar", '[[:punct:]]', '', 'g')
                        INTO vArgumento;
                    --Se obtiene el nombre del argumento
                    SELECT cod_nbargumento INTO vNombreArgumento
                    FROM sgnom.tsgnomargumento
                    WHERE cod_nbargumento = vArgumento;
                    RAISE INFO 'nomARG: % %', vArgumento, vNombreArgumento;
                    --Se obtiene la funci�n del argumento
                    SELECT des_funcionbd INTO vFuncionOracle
                    FROM sgnom.tsgnomargumento
                   	WHERE cod_nbargumento = vArgumento;
                    RAISE INFO 'fnARG: %, %', vArgumento,vFuncionOracle;
                    IF (vFuncionOracle IS NOT NULL) THEN
                        IF (vFuncionOracle = 'fn_dias_laborados') THEN 
                            vFormulaEjecutar :='sgnom.'||vFuncionOracle||'('||vIdEmpleado||','||vIdQuincena||')';
                            vCadenaEjecutar :='select ' || vFormulaEjecutar;
                            RAISE INFO 'cadena %', vCadenaEjecutar;
                            EXECUTE vCadenaEjecutar INTO vDiasLaborados;
                            vValorArgumento :=vDiasLaborados;
                            RAISE INFO 'RES laborados %', vValorArgumento;
						ELSIF (vFuncionOracle = 'fn_sueldo_base') THEN 
                            vFormulaEjecutar :='sgnom.'||vFuncionOracle||'('||vIdEmpleado||')';
                            vCadenaEjecutar :='select ' || vFormulaEjecutar;
                            RAISE INFO 'cadena %', vCadenaEjecutar;
                            EXECUTE vCadenaEjecutar INTO vSueldoBase;
                            vValorArgumento :=vSueldoBase;
                            RAISE INFO 'RES SB %', vValorArgumento;
                        END IF;
                    ELSE
                    	SELECT imp_valorconst INTO vConstante
						FROM sgnom.tsgnomargumento
                   		WHERE cod_nbargumento = vArgumento;
                    	vValorArgumento :=vConstante;
						RAISE INFO 'RES constante %', vConstante;
					END IF;
                    SELECT regexp_replace(vFormula2, vArgumento, vValorArgumento::"varchar", 'g') INTO vFormula2;
                    RAISE INFO 'RES vFormula2 %, vArgumento %, vValorArgumento % ', 
					vFormula2,vArgumento,vValorArgumento;
                    
                END LOOP;
				SELECT regexp_replace(vFormula2, '[:]', '', 'g') INTO vFormula2;
				vCadenaImporte:='SELECT '|| vFormula2 || '';
				RAISE INFO 'RES vCadenaImporte %',  vCadenaImporte;
                -- DBMS_OUTPUT.PUT_LINE(vFormula2);
                EXECUTE vCadenaImporte INTO vImporteActualizado;
                RAISE INFO 'RES vImporteActualizado %', vImporteActualizado;
                --COMMIT;
            END LOOP;
            CLOSE C4;
            OPEN C4;
            LOOP 
                FETCH C4 INTO vFormula;
                EXIT WHEN not found;
                vFormula2 :=vFormula;
                vXML :='<?xml version="1.0" encoding="ISO8859_1"?>                
                <mx.org.ift.serp.conceptoBindingXml.DetallesConcepto>                  
                <claveConcepto>' || vClaveConcepto || '</claveConcepto>                  
                <nombreConcepto>' || vNombreConcepto || '</nombreConcepto>';
                vXML :=vXML ||'<formula>' || vFormula || '</formula>                  
                <argumentos>';
                --Se obtiene el numero de argumentos que contiene la formula
                vNumeroArgumentos:=(SELECT COUNT(*)
                                    FROM regexp_matches(vFormula, ':[^#0-9/*+$%-:]+', 'g'));
                FOR i IN 1 .. vNumeroArgumentos
                LOOP
                    SELECT regexp_replace(
                        (SELECT contenido
                        FROM
                            (SELECT contenido, row_number() OVER () AS indice --COUNT(*)
                            FROM 
                            regexp_matches(vFormula, ':[^#0-9/*+$%-:]+', 'g') AS contenido
                            ) AS todo
                        WHERE indice = i)::varchar, '[{:}"]', '', 'g') 
                        --WHERE indice = )::"varchar", '[[:punct:]]', '', 'g')
                        INTO vArgumento;
                    --Se obtiene el nombre del argumento
                    SELECT cod_nbargumento INTO vNombreArgumento
                    FROM sgnom.tsgnomargumento
                    WHERE cod_nbargumento = vArgumento;
                    RAISE INFO 'nomARG: % %', vArgumento, vNombreArgumento;
                    --Se obtiene la funci�n del argumento
                    SELECT des_funcionbd INTO vFuncionOracle
                    FROM sgnom.tsgnomargumento
                    WHERE cod_nbargumento = vArgumento;
                    RAISE INFO 'fnARG: %, %', vArgumento,vFuncionOracle;
                    --Se calcula la funci�n del argumento
                    IF (vFuncionOracle IS NOT NULL) THEN
                        IF (vFuncionOracle = 'fn_dias_laborados') THEN 
                            vFormulaEjecutar :='sgnom.'||vFuncionOracle||'('||vIdEmpleado||','||vIdQuincena||')';
                            vCadenaEjecutar :='select ' || vFormulaEjecutar;
                            RAISE INFO 'cadena %', vCadenaEjecutar;
                            EXECUTE vCadenaEjecutar INTO vDiasLaborados;
                            vValorArgumento :=vDiasLaborados;
                            RAISE INFO 'RES laborados %', vValorArgumento;
                        ELSIF (vFuncionOracle = 'fn_sueldo_base') THEN 
                            vFormulaEjecutar :='sgnom.'||vFuncionOracle||'('||vIdEmpleado||')';
                            vCadenaEjecutar :='select ' || vFormulaEjecutar;
                            RAISE INFO 'cadena %', vCadenaEjecutar;
                            EXECUTE vCadenaEjecutar INTO vSueldoBase;
                            vValorArgumento :=vSueldoBase;
                            RAISE INFO 'RES SB %', vValorArgumento;
                        END IF;
                    ELSE
                        SELECT imp_valorconst INTO vConstante
                        FROM sgnom.tsgnomargumento
                        WHERE cod_nbargumento = vArgumento;
                        vValorArgumento :=vConstante;
                        RAISE INFO 'RES constante %', vConstante;
                    END IF;
                    IF(vValorArgumento IS NULL) THEN 
                        vValorArgumento :=0;
                    END IF;
                    SELECT regexp_replace(vFormula2, '[:]', '', 'g') INTO vFormula2
                    FROM (SELECT des_formula
                            FROM sgnom.tsgnomformula
                            WHERE cod_formulaid =
                                    (SELECT CO.cod_formulaid_fk 
                                    FROM sgnom.tsgnomconcepto CO
                                    WHERE CO.cod_conceptoid = vIdConcepto01fk)) form;
                    --Se continua la construccion del XML agregando cada argumento
                    vXML:= vXML || '<mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>' || vArgumento || '</nombre>                            
                    <valor>' || vValorArgumento || '</valor> <descripcion>' || vNombreArgumento || '</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento>';
                END LOOP;
                EXECUTE vCadenaImporte INTO vImporteActualizado;
                vXML :=vXML || '</argumentos>';
                IF(xmlDesglose IS NOT NULL) THEN 
                    vXML := vXML || xmlDesglose;
                    xmlDesglose :=NULL;
                END IF;
                --Se agrega el importe total al XML
                vXML:=vXML || '<importe>' || vImporteActualizado || '</importe> </mx.org.ift.serp.conceptoBindingXml.DetallesConcepto>  ';
                --Se guarda el importe del concepto y el XML generado
				RAISE INFO 'dentro C4 ACTUALIZANDO EL EQ %, id %', vIdConEmpPla01, vIdEmpleado;
                UPDATE sgnom.tsgnomcncptoquinc
				SET imp_concepto = vImporteActualizado, xml_desgloce = vXML::xml
				WHERE cod_empquincenaid_fk = vIdConEmpPla01;
                --AND cod_conceptoid_fk = vIdConcepto01fk
                --AND cod_cncptoquincid = cncptoquincid;
				GET DIAGNOSTICS a_count = ROW_COUNT;
				RAISE INFO 'a_count %', a_count;
                IF (a_count>0) THEN 
                    vNumRegistros := vNumRegistros + 1 ;
					RAISE INFO 'vNumRegistros %', vNumRegistros;
                END IF;
                --COMMIT; 
            END LOOP;
            CLOSE C4;
			RAISE INFO 'fuera C4 ACTUALIZANDO EL EQ %, id %', vIdConEmpPla01, vIdEmpleado;
            vSueldoBase :=0.0;
            vDiasLaborados :=0;
            vCompensasionG :=0;
            vDespensa :=0;
            vPorcentajeManTer :=0;
            vApoyoVehicular :=0;
            vAportacionSSIGF :=0;
            vPrimaVacacional :=0;
            vFaltasSueldoEst :=0;
            vFaltasSueldoEv :=0;
            vFaltasSueldoEstAA :=0;
            vFaltasCompEst :=0;
            vFaltasCompEv :=0;
            vFaltasCompEstAA :=0;
            vISRSueldo :=0;
            vISRCompGaran :=0;
            vISRSSI :=0;
            vISRApoyoVehic :=0;
            vISRPrimaVac :=0;
            vValorQuinquenio :=0;
            vSueldoCompactado :=0;
            vSueldoBimestral :=0;
            vQuinquenioBimestral :=0;
            vSumaSRCEAYV :=0;
            vSueldoBimestralEV :=0;
            vQuinquenioBimestralEV:=0;
            vSumaSRCEAYVEV :=0;
            vPensionAlimenticia :=0;
            vSueldoCompactado :=0;
            vSumaSARP :=0;
            vSumaFOVP :=0;
            vSumaSRCEAYVPAT :=0;
            vSumaSRCEAYVPAT :=0;
            vPorcentajePension :=0;
            vISREstDesemp :=0;
            vISRHonorarios :=0;
            vValorEstimulo :=0;
            vAhorroSolidario :=0;
            vConstante :=0;
            vImpLicMedSueldo :=0; 
        END LOOP;
		CLOSE C1;
        --Se llama la funcion que calcula los importes de cada concepto
        SELECT sgnom.fn_calcula_importes_nomina(vIdNomina,NULL,0) INTO vBanderaImporte;
		--RETURN vNumRegistros::varchar;
        IF vNumRegistros > 0 THEN 
            vBandera := 1;
        ELSIF vNumRegistros = 0 THEN 
            vBandera := 0;
        ELSE 
            vBandera:=2;
        END IF;
        RETURN vBandera;
		EXCEPTION
		WHEN NO_DATA_FOUND THEN
			RAISE EXCEPTION '% exp no data found', vIdEmpleado;
			RETURN NULL;
    END;
$_$;


ALTER FUNCTION sgnom.fn_calcula_nomina(vidnomina integer) OWNER TO postgres;

--
-- TOC entry 468 (class 1255 OID 39873)
-- Name: fn_calcula_nomina1(integer, integer); Type: FUNCTION; Schema: sgnom; Owner: postgres
--

CREATE FUNCTION sgnom.fn_calcula_nomina1(empid integer, vidnomina integer) RETURNS character varying
    LANGUAGE plpgsql
    AS $_$
    DECLARE
    --rtSueldoBase INTEGER;
    --vFormula CHARACTER VARYING; --valor de la formula
    --vArgumento CHARACTER VARYING; --valor del argumento
    --vNombreArgumento CHARACTER VARYING; --valor del argumento
    --vNumeroArgumentos INTEGER; --numero de argumentos
    --VARIABLES
    total INTEGER;
    vIdNomEmpPla01 INTEGER;
    vIdConEmpPla01 INTEGER;
    vIdConcepto01fk INTEGER;
    vNumRegistros INTEGER:=0;
    vBandera INTEGER;
    vFormula VARCHAR(150);
    vArgumento VARCHAR(100);
    vNumeroArgumentos INTEGER;
    vFuncionOracle VARCHAR(100);
    vDiasLaborados INTEGER;
    vCadenaEjecutar VARCHAR(500);
    vFormulaEjecutar VARCHAR(500);
    vTabulador INTEGER:=0;
    vIdEmpleado INTEGER;
    vIdPlaza INTEGER;
    vNumeroQuincena INTEGER;
    vSueldoBase NUMERIC(10,2):=0.0;
    vConstante NUMERIC(10,2);
    vFormula2 VARCHAR(150);
    vImporteActualizado NUMERIC(10,2);
    vCadenaImporte VARCHAR(250);
    vXML VARCHAR(4000);
    vNombreConcepto VARCHAR(150);
    vValorArgumento NUMERIC(10,2);
    vNombreArgumento VARCHAR(100);
    vCompensasionG INTEGER:=0;
    vNumeroEmpleados INTEGER;
    vNomEmpleado INTEGER;
    vBanderaImporte INTEGER:=0;
    vIdManTer INTEGER;
    vPorcentajeManTer INTEGER;
    vClaveConcepto VARCHAR(20);
    vClaveConcepto2 VARCHAR(20);
    vApoyoVehicular INTEGER;
    vAportacionSSIGF INTEGER;
    vPrimaVacacional INTEGER;
    vFormulaCICAS VARCHAR(150);
    vFormulaCICAC VARCHAR(150);
    vNumeroArgumentosCAS INTEGER;
    vSueldoDiario INTEGER:=0;
    vExisteAguinaldo INTEGER;
    vIdAguinaldo INTEGER;
    vIdQuincena INTEGER;
    vFaltasSueldoEst INTEGER:=0;
    vFaltasSueldoEv INTEGER:=0;
    vFaltasSueldoEstAA INTEGER:=0;
    vFaltasCompEst INTEGER:=0;
    vFaltasCompEv INTEGER:=0;
    vFaltasCompEstAA INTEGER:=0;
    vISRSueldo INTEGER;
    vISRCompGaran INTEGER;
    vISRSSI INTEGER;
    vISRApoyoVehic INTEGER;
    vISRPrimaVac INTEGER;
    vValorQuinquenio INTEGER:=0;
    vSueldoCompactado INTEGER:=0;
    vSueldoBimestral INTEGER:=0;
    vQuinquenioBimestral INTEGER:=0;
    vSumaSRCEAYV INTEGER;
    vSueldoBimestralEV INTEGER;
    vQuinquenioBimestralEV INTEGER;
    vSumaSRCEAYVEV INTEGER;
    vSueldoCompactadoEV INTEGER;
    vPensionAlimenticia INTEGER;
    vDespensa INTEGER:=0;
    xmlDesglose VARCHAR(3000);
    --cursorPrimaVacacional SYS_REFCURSOR;
    --xmlApoyoVehicular VARCHAR(3000);
    --cursorApoyoVehicular SYS_REFCURSOR;
    -- xmlISRSSI VARCHAR(3000);
    --cursorISRSSI SYS_REFCURSOR;
    vSumaSARP INTEGER;
    vSumaFOVP INTEGER;
    vSumaSRCEAYVPAT INTEGER;
    vPorcentajePension INTEGER;
    vISREstDesemp INTEGER;
    vISRHonorarios INTEGER;
    vValorEstimulo INTEGER;
    vNumeroFaltas INTEGER;
    vDiasLicencia INTEGER;
    ERR_CODE INTEGER;
    ERR_MSG VARCHAR(250);
    vAhorroSolidario INTEGER;
    DecimaImpar INTEGER;
    vNumeroQuincenaBIM INTEGER;
    vTipoNominaBIM INTEGER;
    vEjercicioBIM INTEGER;
    vBanderaSegBaja INTEGER;
    vValISREstDesempA INTEGER;
    vExisteAnt INTEGER;
    vSueldoAnt INTEGER;
    vCompAnt INTEGER;
    vDiasLabAnterior INTEGER;
    vFechaNomina INTEGER;
    vImpLicMedSueldo INTEGER;
    vBanderaEmpNeg INTEGER;
	a_count INTEGER;
    cncptoquincid INTEGER;
    ---- CURSORES
    C1 CURSOR FOR
        SELECT  EQ.cod_empquincenaid,EQ.cod_empleadoid_fk,Q.cnu_numquincena,Q.cod_quincenaid, --CQ.cod_conceptoid_fk
				CQ.cod_cncptoquincid, CQ.cod_empquincenaid_fk, CQ.cod_conceptoid_fk, CO.cod_nbconcepto, CO.cod_claveconcepto
        FROM sgnom.tsgnomempquincena EQ
        INNER JOIN sgnom.tsgnomcabecera C 
            ON C.cod_cabeceraid = EQ.cod_cabeceraid_fk
        INNER JOIN sgnom.tsgnomquincena Q
            ON Q.cod_quincenaid = C.cod_quincenaid_fk
		INNER JOIN sgnom.tsgnomcncptoquinc CQ 
			ON CQ.cod_empquincenaid_fk = EQ.cod_empquincenaid
		INNER JOIN sgnom.tsgnomconcepto CO 
		ON CO.cod_conceptoid = CQ.cod_conceptoid_fk
        WHERE C.cod_tiponominaid_fk = vIdNomina
		AND CO.cod_calculoid_fk = 2
        AND bol_estatusemp != 'f'
        ORDER BY CO.cod_tipoconceptoid_fk, CO.cnu_prioricalculo, EQ.cod_empquincenaid;
    
    C3 CURSOR FOR
        SELECT CQ.cod_cncptoquincid, CQ.cod_empquincenaid_fk, CQ.cod_conceptoid_fk, CO.cod_nbconcepto, CO.cod_claveconcepto
        FROM sgnom.tsgnomcncptoquinc CQ
        INNER JOIN sgnom.tsgnomconcepto CO ON CO.cod_conceptoid = CQ.cod_conceptoid_fk
        WHERE CO.cod_calculoid_fk = 2
        ORDER BY CO.cod_tipoconceptoid_fk, CO.cnu_prioricalculo;

    C4 CURSOR FOR
        SELECT des_formula
        FROM sgnom.tsgnomformula
        WHERE cod_formulaid =
                (SELECT CO.cod_formulaid_fk 
                FROM sgnom.tsgnomconcepto CO
                WHERE CO.cod_conceptoid = vIdConcepto01fk );

    C10 CURSOR FOR
        SELECT des_formula
        FROM sgnom.tsgnomformula
        WHERE cod_formulaid = 9;

    BEGIN
        OPEN C1;
        RAISE INFO 'abre cursor C1';
        LOOP 
            FETCH C1 INTO vIdNomEmpPla01, vIdEmpleado,vNumeroQuincena,vIdQuincena,cncptoquincid, vIdConEmpPla01,vIdConcepto01fk,vNombreConcepto,vClaveConcepto;
            EXIT WHEN not found;
            OPEN C4;
            RAISE INFO 'abre cursor C10 -> C4';
            LOOP
                FETCH C4 INTO vFormula;
                EXIT WHEN not found;
                vFormula2 :=vFormula;
                vNumeroArgumentos:=(SELECT COUNT(*)
                                    FROM regexp_matches(vFormula, ':[^#0-9/*+$%-:]+', 'g'));
                RAISE INFO 'vformula %', vFormula;
                RAISE INFO 'vNumeroArgumentos %', vNumeroArgumentos;
                FOR i IN 1 .. vNumeroArgumentos
                LOOP
                    SELECT regexp_replace(
                        (SELECT contenido
                        FROM
                        (SELECT contenido, row_number() OVER () AS indice --COUNT(*)
                        FROM 
                        regexp_matches(vFormula, ':[^#0-9/*+$%-:]+', 'g') AS contenido
                        ) AS todo
                        WHERE indice = i)::varchar, '[{:}"]', '', 'g') 
                        --WHERE indice = )::"varchar", '[[:punct:]]', '', 'g')
                        INTO vArgumento;
                    --Se obtiene el nombre del argumento
                    SELECT cod_nbargumento INTO vNombreArgumento
                    FROM sgnom.tsgnomargumento
                    WHERE cod_nbargumento = vArgumento;
                    RAISE INFO 'nomARG: % %', vArgumento, vNombreArgumento;
                    --Se obtiene la funci�n del argumento
                    SELECT des_funcionbd INTO vFuncionOracle
                    FROM sgnom.tsgnomargumento
                   	WHERE cod_nbargumento = vArgumento;
                    RAISE INFO 'fnARG: %, %', vArgumento,vFuncionOracle;
                    IF (vFuncionOracle IS NOT NULL) THEN
                        IF (vFuncionOracle = 'fn_dias_laborados') THEN 
                            vFormulaEjecutar :='sgnom.'||vFuncionOracle||'('||vIdEmpleado||','||vIdQuincena||')';
                            vCadenaEjecutar :='select ' || vFormulaEjecutar;
                            RAISE INFO 'cadena %', vCadenaEjecutar;
                            EXECUTE vCadenaEjecutar INTO vDiasLaborados;
                            vValorArgumento :=vDiasLaborados;
                            RAISE INFO 'RES laborados %', vValorArgumento;
						ELSIF (vFuncionOracle = 'fn_sueldo_base') THEN 
                            vFormulaEjecutar :='sgnom.'||vFuncionOracle||'('||vIdEmpleado||')';
                            vCadenaEjecutar :='select ' || vFormulaEjecutar;
                            RAISE INFO 'cadena %', vCadenaEjecutar;
                            EXECUTE vCadenaEjecutar INTO vSueldoBase;
                            vValorArgumento :=vSueldoBase;
                            RAISE INFO 'RES SB %', vValorArgumento;
                        END IF;
                    ELSE
                    	SELECT imp_valorconst INTO vConstante
						FROM sgnom.tsgnomargumento
                   		WHERE cod_nbargumento = vArgumento;
                    	vValorArgumento :=vConstante;
						RAISE INFO 'RES constante %', vConstante;
					END IF;
                    SELECT regexp_replace(vFormula2, vArgumento, vValorArgumento::"varchar", 'g') INTO vFormula2;
                    RAISE INFO 'RES vFormula2 %, vArgumento %, vValorArgumento % ', 
					vFormula2,vArgumento,vValorArgumento;
                    
                END LOOP;
				SELECT regexp_replace(vFormula2, '[:]', '', 'g') INTO vFormula2;
				vCadenaImporte:='SELECT '|| vFormula2 || '';
				RAISE INFO 'RES vCadenaImporte %',  vCadenaImporte;
                -- DBMS_OUTPUT.PUT_LINE(vFormula2);
                EXECUTE vCadenaImporte INTO vImporteActualizado;
                RAISE INFO 'RES vImporteActualizado %', vImporteActualizado;
                --COMMIT;
            END LOOP;
            CLOSE C4;
            --OPEN C3;
			--RAISE INFO 'abre C3 calculo';
            --LOOP
                --FETCH C3 INTO cncptoquincid, vIdConEmpPla01,vIdConcepto01fk,vNombreConcepto,vClaveConcepto ;
                --EXIT WHEN not found;
                OPEN C4;
                LOOP 
                    FETCH C4 INTO vFormula;
                    EXIT WHEN not found;
                    vFormula2 :=vFormula;
                    vXML :='<?xml version="1.0" encoding="ISO8859_1"?>                
                    <mx.org.ift.serp.conceptoBindingXml.DetallesConcepto>                  
                    <claveConcepto>' || vClaveConcepto || '</claveConcepto>                  
                    <nombreConcepto>' || vNombreConcepto || '</nombreConcepto>';
                     vXML :=vXML ||'<formula>' || vFormula || '</formula>                  
                    <argumentos>';
                    --Se obtiene el numero de argumentos que contiene la formula
                    vNumeroArgumentos:=(SELECT COUNT(*)
                                        FROM regexp_matches(vFormula, ':[^#0-9/*+$%-:]+', 'g'));
                    FOR i IN 1 .. vNumeroArgumentos
                    LOOP
                        SELECT regexp_replace(
                            (SELECT contenido
                            FROM
                            (SELECT contenido, row_number() OVER () AS indice --COUNT(*)
                            FROM 
                            regexp_matches(vFormula, ':[^#0-9/*+$%-:]+', 'g') AS contenido
                            ) AS todo
                            WHERE indice = i)::varchar, '[{:}"]', '', 'g') 
                            --WHERE indice = )::"varchar", '[[:punct:]]', '', 'g')
                            INTO vArgumento;
                        --Se obtiene el nombre del argumento
                        SELECT cod_nbargumento INTO vNombreArgumento
                        FROM sgnom.tsgnomargumento
                        WHERE cod_nbargumento = vArgumento;
                        RAISE INFO 'nomARG: % %', vArgumento, vNombreArgumento;
                        --Se obtiene la funci�n del argumento
                        SELECT des_funcionbd INTO vFuncionOracle
                        FROM sgnom.tsgnomargumento
                        WHERE cod_nbargumento = vArgumento;
                        RAISE INFO 'fnARG: %, %', vArgumento,vFuncionOracle;
                        --Se calcula la funci�n del argumento
                        IF (vFuncionOracle IS NOT NULL) THEN
                            IF (vFuncionOracle = 'fn_dias_laborados') THEN 
                                vFormulaEjecutar :='sgnom.'||vFuncionOracle||'('||vIdEmpleado||','||vIdQuincena||')';
                                vCadenaEjecutar :='select ' || vFormulaEjecutar;
                                RAISE INFO 'cadena %', vCadenaEjecutar;
                                EXECUTE vCadenaEjecutar INTO vDiasLaborados;
                                vValorArgumento :=vDiasLaborados;
                                RAISE INFO 'RES laborados %', vValorArgumento;
                            ELSIF (vFuncionOracle = 'fn_sueldo_base') THEN 
                                vFormulaEjecutar :='sgnom.'||vFuncionOracle||'('||vIdEmpleado||')';
                                vCadenaEjecutar :='select ' || vFormulaEjecutar;
                                RAISE INFO 'cadena %', vCadenaEjecutar;
                                EXECUTE vCadenaEjecutar INTO vSueldoBase;
                                vValorArgumento :=vSueldoBase;
                                RAISE INFO 'RES SB %', vValorArgumento;
                            END IF;
                        ELSE
                            SELECT imp_valorconst INTO vConstante
                            FROM sgnom.tsgnomargumento
                            WHERE cod_nbargumento = vArgumento;
                            vValorArgumento :=vConstante;
                            RAISE INFO 'RES constante %', vConstante;
                        END IF;
                        IF(vValorArgumento IS NULL) THEN 
                            vValorArgumento :=0;
                        END IF;
                        SELECT regexp_replace(vFormula2, '[:]', '', 'g') INTO vFormula2
                        FROM (SELECT des_formula
                                FROM sgnom.tsgnomformula
                                WHERE cod_formulaid =
                                        (SELECT CO.cod_formulaid_fk 
                                        FROM sgnom.tsgnomconcepto CO
                                        WHERE CO.cod_conceptoid = vIdConcepto01fk)) form;
                        --Se continua la construccion del XML agregando cada argumento
                        vXML:= vXML || '<mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>' || vArgumento || '</nombre>                            
                        <valor>' || vValorArgumento || '</valor> <descripcion>' || vNombreArgumento || '</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento>';
                    END LOOP;
                    EXECUTE vCadenaImporte INTO vImporteActualizado;
                    vXML :=vXML || '</argumentos>';
                    IF(xmlDesglose IS NOT NULL) THEN 
                        vXML := vXML || xmlDesglose;
                        xmlDesglose :=NULL;
                    END IF;
                    --Se agrega el importe total al XML
                    vXML:=vXML || '<importe>' || vImporteActualizado || '</importe> </mx.org.ift.serp.conceptoBindingXml.DetallesConcepto>  ';
                    --Se guarda el importe del concepto y el XML generado
					RAISE INFO 'dentro C4 ACTUALIZANDO EL EQ %, id %', vIdConEmpPla01, vIdEmpleado;
                    UPDATE sgnom.tsgnomcncptoquinc
					SET imp_concepto = vImporteActualizado, xml_desgloce = vXML::xml
					WHERE cod_empquincenaid_fk = vIdConEmpPla01
                    --AND cod_conceptoid_fk = vIdConcepto01fk
                    AND cod_cncptoquincid = cncptoquincid;
					GET DIAGNOSTICS a_count = ROW_COUNT;
					RAISE INFO 'a_count %', a_count;
                    IF (a_count>0) THEN 
                    	vNumRegistros := vNumRegistros + 1 ;
						RAISE INFO 'vNumRegistros %', vNumRegistros;
                    END IF;
                    --COMMIT; 
                END LOOP;
                CLOSE C4;
				RAISE INFO 'fuera C4 ACTUALIZANDO EL EQ %, id %', vIdConEmpPla01, vIdEmpleado;
            --END LOOP;
            --CLOSE C3;
            vSueldoBase :=0.0;
            vDiasLaborados :=0;
            vCompensasionG :=0;
            vDespensa :=0;
            vPorcentajeManTer :=0;
            vApoyoVehicular :=0;
            vAportacionSSIGF :=0;
            vPrimaVacacional :=0;
            vFaltasSueldoEst :=0;
            vFaltasSueldoEv :=0;
            vFaltasSueldoEstAA :=0;
            vFaltasCompEst :=0;
            vFaltasCompEv :=0;
            vFaltasCompEstAA :=0;
            vISRSueldo :=0;
            vISRCompGaran :=0;
            vISRSSI :=0;
            vISRApoyoVehic :=0;
            vISRPrimaVac :=0;
            vValorQuinquenio :=0;
            vSueldoCompactado :=0;
            vSueldoBimestral :=0;
            vQuinquenioBimestral :=0;
            vSumaSRCEAYV :=0;
            vSueldoBimestralEV :=0;
            vQuinquenioBimestralEV:=0;
            vSumaSRCEAYVEV :=0;
            vPensionAlimenticia :=0;
            vSueldoCompactado :=0;
            vSumaSARP :=0;
            vSumaFOVP :=0;
            vSumaSRCEAYVPAT :=0;
            vSumaSRCEAYVPAT :=0;
            vPorcentajePension :=0;
            vISREstDesemp :=0;
            vISRHonorarios :=0;
            vValorEstimulo :=0;
            vAhorroSolidario :=0;
            vConstante :=0;
            vImpLicMedSueldo :=0; 
        END LOOP;
		--CLOSE C1;
		RETURN vNumRegistros::varchar;
		EXCEPTION
		WHEN NO_DATA_FOUND THEN
			RAISE EXCEPTION '% exp no data found', empid;
			RETURN NULL;
    END;
$_$;


ALTER FUNCTION sgnom.fn_calcula_nomina1(empid integer, vidnomina integer) OWNER TO postgres;

--
-- TOC entry 477 (class 1255 OID 40063)
-- Name: fn_crearnomina(integer); Type: FUNCTION; Schema: sgnom; Owner: postgres
--

CREATE FUNCTION sgnom.fn_crearnomina(cabecera integer) RETURNS boolean
    LANGUAGE plpgsql
    AS $$
    DECLARE
    contador integer := 0;
    bandera boolean := false;
    rec RECORD;
    BEGIN
		FOR rec IN SELECT cod_empleadoid 
                        FROM sgnom.tsgnomempleados 
                        WHERE tsgnomempleados.fec_ingreso <= 
                                (SELECT tsgnomquincena.fec_fin
                                FROM sgnom.tsgnomquincena, sgnom.tsgnomcabecera
                                WHERE tsgnomquincena.cod_quincenaid = tsgnomcabecera.cod_quincenaid_fk
                                AND tsgnomcabecera.cod_cabeceraid = cabecera)
                        AND (tsgnomempleados.fec_salida >= 
                                (SELECT tsgnomquincena.fec_inicio
                                FROM sgnom.tsgnomquincena, sgnom.tsgnomcabecera
                                WHERE tsgnomquincena.cod_quincenaid = tsgnomcabecera.cod_quincenaid_fk
                                AND tsgnomcabecera.cod_cabeceraid = cabecera)
                        OR tsgnomempleados.fec_salida IS NULL)
                        AND tsgnomempleados.bol_estatus = 't'
                        ORDER BY 1
            LOOP
                INSERT INTO sgnom.tsgnomempquincena(cod_empleadoid_fk, cod_cabeceraid_fk, bol_estatusemp,
												   imp_totpercepcion, imp_totdeduccion, imp_totalemp)
                VALUES (rec.cod_empleadoid, cabecera, 't',
					   0.0, 0.0, 0.0);
                contador := contador + 1;
            END LOOP;
            IF (contador > 0) THEN
                bandera := true;
            ELSIF (contador = 0) THEN
                --ROLLBACK;
                bandera := false;
            END IF;
        IF bandera = false THEN
            --devuelve esta bandera
            --RAISE 'entra';
            DELETE FROM sgnom.tsgnomcabecera WHERE cod_cabeceraid = ultimoid;
            return bandera;
        END IF;
        --EXCEPTION 
        --WHEN OTHERS THEN 
            --ROLLBACK;
        RETURN bandera;
    END;
$$;


ALTER FUNCTION sgnom.fn_crearnomina(cabecera integer) OWNER TO postgres;

--
-- TOC entry 476 (class 1255 OID 39823)
-- Name: fn_dias_laborados(integer, integer); Type: FUNCTION; Schema: sgnom; Owner: postgres
--

CREATE FUNCTION sgnom.fn_dias_laborados(empid integer, prmnumquincenacalculo integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$

    DECLARE
    ----variables
    vFechaIngreso DATE;
    vFechaBaja DATE;
    vFechaIQuincena DATE;
    vFechaFQuincena DATE;
    vDiasLaborados INTEGER;
    vFecha1 INTEGER;
    vFecha2 INTEGER;
    vFecha3 INTEGER;
    vFecha1Dia INTEGER;
    vCadenaResultado VARCHAR(500);
    ERR_CODE INTEGER;
    ERR_MSG VARCHAR(250);

    ---cursores
    C11 CURSOR FOR 
        SELECT EMP.fec_ingreso, EMP.fec_salida
        FROM sgnom.tsgnomempleados EMP
        WHERE EMP.cod_empleadoid = empid;

    C12 CURSOR FOR 
        SELECT fec_inicio, fec_fin
        FROM sgnom.tsgnomquincena
        WHERE cod_quincenaid =
                    (SELECT cod_quincenaid
                    FROM sgnom.tsgnomquincena Q
                    INNER JOIN sgnom.tsgnomejercicio E ON Q.cod_ejercicioid_fk = E.cod_ejercicioid
                    WHERE cod_quincenaid = prmNumQuincenaCalculo
                    AND E.cnu_valorejercicio = (
                        SELECT MAX(cnu_valorejercicio)
                        FROM sgnom.tsgnomejercicio EJC
                        INNER JOIN sgnom.tsgnomquincena CQ ON EJC.cod_ejercicioid = CQ.cod_ejercicioid_fk
                        INNER JOIN sgnom.tsgnomcabecera CAB ON CQ.cod_quincenaid = CAB.cod_quincenaid_fk));

    BEGIN
		RAISE INFO 'inicia funcion laborales';
        OPEN C11; RAISE INFO 'inicia C11 laboral';
        LOOP
        FETCH C11 INTO vFechaIngreso, vFechaBaja;
			RAISE INFO 'contenido C11 % , %', vFechaIngreso,vFechaBaja;
            EXIT WHEN not found;
            OPEN C12; RAISE INFO 'inicia C12 laboral';
            LOOP
            FETCH C12 INTO vFechaIQuincena, vFechaFQuincena;
                RAISE INFO 'contenido C12 % , %', vFechaIQuincena,vFechaFQuincena;
                EXIT WHEN not found;
                -- DBMS_OUTPUT.PUT_LINE('vFechaIngreso '||vFechaIngreso||' vFechaBaja '||vFechaBaja||' vFechaIQuincena '||vFechaIQuincena||' vFechaFQuincena '||vFechaFQuincena );
                --Escenario1:trabajo la quincena completa,
                --ya sea porque ya llevaba tiempo trabajando o ingreso en inicio de quiincena
                IF(vFechaIngreso<=vFechaIQuincena AND vFechaBaja IS NULL) THEN
                    vDiasLaborados:= 15;
                --Escenario2:ingreso despues del inicio de la quincena,
                --se toma la fecha fin de la quincea y se le resta la fecha en la que entro,
                ELSIF(vFechaIngreso >vFechaIQuincena AND vFechaBaja IS NULL) THEN
                    vFecha1 :=TO_CHAR(vFechaFQuincena,'YYYYMMDD');
                    vFecha1Dia :=TO_CHAR(vFechaFQuincena,'DD');
                    vFecha2 :=TO_CHAR(vFechaIngreso,'YYYYMMDD');
                    IF(vFecha1!=vFecha2) THEN
                        IF(vFecha1Dia =28) THEN
                            vFecha1 :=vFecha1+2;
                        ELSIF(vFecha1Dia=29) THEN
                            vFecha1 :=vFecha1+1;
                        ELSIF(vFecha1Dia=31) THEN
                            vFecha1 :=vFecha1-1;
                        END IF;
                    END IF;
                    vDiasLaborados:= (vFecha1-vFecha2)+1;
                --Escenario3:se dio de baja antes de que termine la quincena, se toma la fecha de baja y se le resta la fecha de inicio de quincena
                ELSIF(vFechaIngreso<=vFechaIQuincena AND vFechaBaja IS NOT NULL) THEN
                    vFecha1 :=TO_CHAR(vFechaBaja,'YYYYMMDD');
                    vFecha2 :=TO_CHAR(vFechaIQuincena,'YYYYMMDD');
                    vDiasLaborados :=(vFecha1-vFecha2)+1;
                    IF(vDiasLaborados >15) THEN
                        vDiasLaborados :=15;
                    END IF;
                --Escenario4:se dio de alta despues del inicio de la quincena y se dio de baja antes que terminar la quincena,
                --se toma la fecha de baja y se le resta la fecha de ingreso
                ELSIF(vFechaIngreso>vFechaIQuincena AND vFechaBaja IS NOT NULL) THEN
                    vFecha1 :=TO_CHAR(vFechaBaja,'YYYYMMDD');
                    vFecha2 :=TO_CHAR(vFechaIngreso,'YYYYMMDD');
                    vFecha3 :=TO_CHAR(vFechaFQuincena,'YYYYMMDD');
                    vFecha1Dia :=TO_CHAR(vFechaFQuincena,'DD');
                    IF(vFecha1=vFecha3) THEN
                        IF(vFecha1Dia =28) THEN
                            vFecha1 :=vFecha1+2;
                        ELSIF(vFecha1Dia=29) THEN
                            vFecha1 :=vFecha1+1;
                        ELSIF(vFecha1Dia=31) THEN
                            vFecha1 :=vFecha1-1;
                        END IF;
                    END IF;
                    vDiasLaborados := (vFecha1-vFecha2)+1;
                END IF;
                RAISE INFO 'dentro loop laborados %', vDiasLaborados;
            END LOOP;
			RAISE INFO 'fin loop 2';
            CLOSE C12;
        END LOOP;
        RAISE INFO 'fin loop 1';
        CLOSE C11;
        --vCadenaResultado:=TO_CHAR(vDiasLaborados);
        RAISE INFO 'final laborados %', vDiasLaborados;
        --DBMS_OUTPUT.PUT_LINE(vCadenaResultado);
        RETURN vDiasLaborados;
		EXCEPTION
		WHEN NO_DATA_FOUND THEN
			RAISE EXCEPTION '% exp no data found', empid;
			RETURN NULL;
		WHEN OTHERS THEN
			--err_code := SQLCODE;
			--err_msg := SUBSTR(SQLERRM, 1, 200);
			RAISE EXCEPTION '% exp others', empid;
			--SPDERRORES('FN_DIAS_LABORADOS',err_code,err_msg,prmIdEmpleado);
			RETURN NULL;
    END;
$$;


ALTER FUNCTION sgnom.fn_dias_laborados(empid integer, prmnumquincenacalculo integer) OWNER TO postgres;

--
-- TOC entry 469 (class 1255 OID 31598)
-- Name: fn_sueldo_base(integer); Type: FUNCTION; Schema: sgnom; Owner: postgres
--

CREATE FUNCTION sgnom.fn_sueldo_base(empid integer) RETURNS numeric
    LANGUAGE plpgsql
    AS $$
    DECLARE
    rtSueldoBase numeric;
    BEGIN
        SELECT imp_honorarios 
        INTO rtSueldoBase
        FROM sgnom.tsgnomempleados
        WHERE cod_empleadoid = empid;
        RETURN rtSueldoBase;
    END;
$$;


ALTER FUNCTION sgnom.fn_sueldo_base(empid integer) OWNER TO postgres;

--
-- TOC entry 474 (class 1255 OID 39940)
-- Name: insertar_incidencia_por_empleado(integer, integer, character varying, text, integer); Type: FUNCTION; Schema: sgnom; Owner: postgres
--

CREATE FUNCTION sgnom.insertar_incidencia_por_empleado(incidenciaid integer, cantidad integer, actividad character varying, comentarios text, reporta integer) RETURNS boolean
    LANGUAGE sql
    AS $$

	INSERT INTO sgnom.tsgnomincidencia(
											"cod_incidenciaid",
											"cod_catincidenciaid_fk", 
											"cnu_cantidad", 
											"des_actividad", 
											"txt_comentarios", 
											"cod_empreporta_fk", 
											"bol_estatus", 
											"cod_quincenaid_fk",
											"aud_codcreadopor",
											"aud_feccreacion")
			VALUES (
											NEXTVAL('sgnom.seq_incidencia'::regclass), 
											incidenciaid, 
											cantidad, 
											actividad, 
											comentarios, 
											(SELECT emp.cod_empleadoid
												FROM sgnom.tsgnomempleados emp 
												WHERE emp.cod_empleado_fk = reporta),
											't',
											(SELECT nomquincena.cod_quincenaid 
												FROM sgnom.tsgnomquincena nomquincena
												WHERE nomquincena.fec_inicio <= CURRENT_DATE AND nomquincena.fec_fin >= CURRENT_DATE),
											reporta,
											CURRENT_DATE
								)RETURNING TRUE

$$;


ALTER FUNCTION sgnom.insertar_incidencia_por_empleado(incidenciaid integer, cantidad integer, actividad character varying, comentarios text, reporta integer) OWNER TO postgres;

--
-- TOC entry 458 (class 1255 OID 22457)
-- Name: totalimpcab(integer); Type: FUNCTION; Schema: sgnom; Owner: postgres
--

CREATE FUNCTION sgnom.totalimpcab(id integer) RETURNS boolean
    LANGUAGE plpgsql
    AS $$
DECLARE
 total numeric(10,2);
 BEGIN
   SELECT (imp_totpercepcion - imp_totdeduccion)
   INTO total
   FROM sgnom.tsgnomcabecera
   WHERE cod_cabeceraid = id;
  UPDATE sgnom.tsgnomcabecera SET imp_totalemp=total
   WHERE cod_cabeceraid = id;
RETURN true;
END;
$$;


ALTER FUNCTION sgnom.totalimpcab(id integer) OWNER TO postgres;

--
-- TOC entry 460 (class 1255 OID 18805)
-- Name: crosstab_report_encuesta(integer); Type: FUNCTION; Schema: sgrh; Owner: postgres
--

CREATE FUNCTION sgrh.crosstab_report_encuesta(integer) RETURNS TABLE(pregunta character varying, resp1 character varying, resp2 character varying, resp3 character varying, resp4 character varying, resp5 character varying)
    LANGUAGE sql
    AS $_$        
            SELECT * FROM crosstab(
                'SELECT p.des_pregunta AS rowid, 
                        cr.cod_ponderacion as attribute, 
                        cr.des_respuesta as value
                FROM sgrh.tsgrhpreguntasenc p
                INNER JOIN sgrh.tsgrhencuesta e ON p.cod_encuesta = e.cod_encuesta
                LEFT JOIN sgrh.tsgrhrespuestasenc r ON p.cod_pregunta = r.cod_pregunta
                LEFT JOIN sgrh.tsgrhcatrespuestas cr ON r.cod_catrespuesta = cr.cod_catrespuesta
                WHERE e.cod_encuesta = ' || $1
			) 
            AS (
                pregunta VARCHAR(200), 
                resp1 VARCHAR(200), 
                resp2 VARCHAR(200), 
                resp3 VARCHAR(200), 
                resp4 VARCHAR(200), 
                resp5 VARCHAR(200)
            );
    $_$;


ALTER FUNCTION sgrh.crosstab_report_encuesta(integer) OWNER TO postgres;

--
-- TOC entry 461 (class 1255 OID 18806)
-- Name: factualizarfecha(); Type: FUNCTION; Schema: sgrh; Owner: postgres
--

CREATE FUNCTION sgrh.factualizarfecha() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
declare begin
	new.fec_modificacion:=current_date;
	return new;

end;
$$;


ALTER FUNCTION sgrh.factualizarfecha() OWNER TO postgres;

--
-- TOC entry 462 (class 1255 OID 18473)
-- Name: buscar_asistentes_minuta(integer); Type: FUNCTION; Schema: sgrt; Owner: postgres
--

CREATE FUNCTION sgrt.buscar_asistentes_minuta(reunionid integer) RETURNS TABLE(nombre_asistente text, area_asistente character varying)
    LANGUAGE sql
    AS $$
	SELECT
	des_nombre as nombre_asistente,
	CONCAT((SELECT area.des_nbarea FROM sgrh.tsgrhempleados emp LEFT JOIN sgrh.tsgrhareas area ON area.cod_area=emp.cod_area WHERE emp.cod_empleado=invitado.cod_empleado),
	des_empresa) as area_asistente
	FROM sgrt.tsgrtinvitados invitado WHERE  invitado.cnu_asiste='1' and invitado.cod_reunion=reunionid;
	$$;


ALTER FUNCTION sgrt.buscar_asistentes_minuta(reunionid integer) OWNER TO postgres;

--
-- TOC entry 463 (class 1255 OID 18474)
-- Name: buscar_compromisos_roles_list(integer); Type: FUNCTION; Schema: sgrt; Owner: postgres
--

CREATE FUNCTION sgrt.buscar_compromisos_roles_list(reunionid integer) RETURNS TABLE(cod_compromiso integer, des_descripcion character varying, cod_estatus sgrt.estatus_compromiso, fec_solicitud text, fec_compromiso text, validador text, verificador text, ejecutor text)
    LANGUAGE plpgsql
    AS $$

	BEGIN
	RETURN QUERY
	select
	CAST(ROW_NUMBER() OVER(ORDER BY (SELECT 1)) as INTEGER) AS cod_compromiso,
	com.des_descripcion,
	com.cod_estatus,
	CAST(to_char(com.fec_solicitud, 'DD/MM/YYYY') as text) as fec_solicitud,
	CAST(to_char(com.fec_compromiso, 'DD/MM/YYYY') as text) as fec_compromiso,
	(select CONCAT(des_nombre, ' ', des_nombres, ' ', des_apepaterno, ' ', des_apematerno) from sgrh.tsgrhempleados where cod_empleado=com.cod_validador) AS validador,
	(select CONCAT(des_nombre, ' ', des_nombres, ' ', des_apepaterno, ' ', des_apematerno) from sgrh.tsgrhempleados where cod_empleado=com.cod_verificador) AS verificador,
	(select CONCAT(des_nombre, ' ', des_nombres, ' ', des_apepaterno, ' ', des_apematerno) from sgrh.tsgrhempleados where cod_empleado=com.cod_ejecutor) AS ejecutor
	from sgrt.tsgrtcompromisos com where com.cod_reunion=reunionid;

	END;
	$$;


ALTER FUNCTION sgrt.buscar_compromisos_roles_list(reunionid integer) OWNER TO postgres;

--
-- TOC entry 453 (class 1255 OID 18475)
-- Name: buscar_minutas_fechas(text, text); Type: FUNCTION; Schema: sgrt; Owner: postgres
--

CREATE FUNCTION sgrt.buscar_minutas_fechas(fecha_inicio text, fecha_fin text) RETURNS TABLE(cod_area integer, des_nbarea character varying, cantidad_minutas integer)
    LANGUAGE sql
    AS $$
	SELECT
	cod_area,
	cod_acronimo as des_nbarea,
	cast((SELECT
	COUNT(*)
	FROM sgrt.tsgrtreuniones reu
	INNER JOIN sgrh.tsgrhempleados emp
	ON reu.cod_responsable=emp.cod_empleado
	INNER JOIN sgrh.tsgrhareas area
	ON emp.cod_area=area.cod_area
	WHERE area.cod_area=a.cod_area and
	reu.fec_fecha >= cast(fecha_inicio as date)
	AND reu.fec_fecha <=  cast(fecha_fin as date)
	) as INTEGER) AS cantidad_minutas
	FROM sgrh.tsgrhareas a
	$$;


ALTER FUNCTION sgrt.buscar_minutas_fechas(fecha_inicio text, fecha_fin text) OWNER TO postgres;

--
-- TOC entry 454 (class 1255 OID 18476)
-- Name: buscar_proxima_reunion(integer); Type: FUNCTION; Schema: sgrt; Owner: postgres
--

CREATE FUNCTION sgrt.buscar_proxima_reunion(reunionid integer, OUT cod_reunion integer, OUT des_nombre_reunion character varying, OUT fec_fecha text, OUT cod_lugar integer, OUT tim_hora character varying, OUT des_nombre_lugar character varying, OUT cod_ciudad integer, OUT des_nbciudad character varying, OUT cod_estadorep_ciudad integer, OUT des_nbestado character varying, OUT cod_estadorep_estado integer) RETURNS SETOF record
    LANGUAGE sql
    AS $$
	  select
	  reunion.cod_reunion,
	  reunion.des_nombre,
	  CAST(to_char(reunion.fec_fecha, 'DD/MM/YYYY')as text) as fec_fecha,
	  reunion.cod_lugar,
	  CAST(reunion.tim_hora as text),
	  lugar.des_nombre,
	  lugar.cod_ciudad,
	ciudad.des_nbciudad,
	ciudad.cod_estadorep,
	estado.des_nbestado,
	estado.cod_estadorep
	from sgrt.tsgrtreuniones reunion inner join sgrt.tsgrtlugares lugar on reunion.cod_lugar=lugar.cod_lugar
	inner join sgrt.tsgrtciudades ciudad on lugar.cod_ciudad=ciudad.cod_ciudad
	inner join sgrt.tsgrtestados estado on ciudad.cod_estadorep=estado.cod_estadorep
	where cod_reunion=(select cod_proximareunion from sgrt.tsgrtreuniones where cod_reunion=reunionId);

	$$;


ALTER FUNCTION sgrt.buscar_proxima_reunion(reunionid integer, OUT cod_reunion integer, OUT des_nombre_reunion character varying, OUT fec_fecha text, OUT cod_lugar integer, OUT tim_hora character varying, OUT des_nombre_lugar character varying, OUT cod_ciudad integer, OUT des_nbciudad character varying, OUT cod_estadorep_ciudad integer, OUT des_nbestado character varying, OUT cod_estadorep_estado integer) OWNER TO postgres;

--
-- TOC entry 455 (class 1255 OID 18477)
-- Name: compromisos_areas_fechas(text, text); Type: FUNCTION; Schema: sgrt; Owner: postgres
--

CREATE FUNCTION sgrt.compromisos_areas_fechas(fecha_inicio text, fecha_fin text) RETURNS TABLE(cod_area integer, des_nbarea character varying, tipo text, num integer)
    LANGUAGE sql
    AS $$
	select
	cod_area,
	cod_acronimo as des_nbarea,
	CAST('Terminado' as text) as tipo,
	cast((SELECT
	COUNT(*)
	FROM sgrt.tsgrtcompromisos reu
	INNER JOIN sgrh.tsgrhempleados emp
	ON reu.cod_ejecutor=emp.cod_empleado
	INNER JOIN sgrh.tsgrhareas area
	ON emp.cod_area=area.cod_area
	WHERE reu.cod_estatus='Terminado' AND
	area.cod_area=a.cod_area AND
	reu.fec_compromiso >= cast(fecha_inicio as date)
	AND reu.fec_compromiso <= cast(fecha_fin as date)
	)as INTEGER) as num
	FROM
	sgrh.tsgrhareas a
	UNION
	select
	cod_area,
	cod_acronimo as des_nbarea,
	CAST('Pendiente' as text) as tipo,
	cast((SELECT
	COUNT(*)
	FROM sgrt.tsgrtcompromisos reu
	INNER JOIN sgrh.tsgrhempleados emp
	ON reu.cod_ejecutor=emp.cod_empleado
	INNER JOIN sgrh.tsgrhareas area
	ON emp.cod_area=area.cod_area
	WHERE reu.cod_estatus='Pendiente' and
	area.cod_area=a.cod_area AND
	reu.fec_compromiso >= cast(fecha_inicio as date)
	AND reu.fec_compromiso <= cast(fecha_fin as date))as INTEGER) as num
	FROM
	sgrh.tsgrhareas a
	$$;


ALTER FUNCTION sgrt.compromisos_areas_fechas(fecha_inicio text, fecha_fin text) OWNER TO postgres;

--
-- TOC entry 464 (class 1255 OID 18478)
-- Name: compromisos_dia(text); Type: FUNCTION; Schema: sgrt; Owner: postgres
--

CREATE FUNCTION sgrt.compromisos_dia(fechacompromiso text) RETURNS TABLE(cod_empleado integer, nombre text, area text, fec_compromiso text, des_descripcion text, cod_estatus text, tiempo_demora integer)
    LANGUAGE sql
    AS $$
	SELECT
	emp.cod_empleado,
	CONCAT(emp.des_nombre, ' ', emp.des_nombres, ' ', emp.des_apepaterno, ' ', emp.des_apematerno) as nombre,
	(SELECT cod_acronimo FROM sgrh.tsgrhareas WHERE cod_area=emp.cod_area) as area,
	cast(to_char(com.fec_compromiso,'DD/MM/YYYY') as text) fec_compromiso,
	com.des_descripcion,
	cast(com.cod_estatus as text),
	cast((select count(the_day) from
	    (select generate_series(com.fec_compromiso+1, com.fec_entrega, '1 day') as the_day) days
	where extract('dow' from the_day) not in (0,6))as INTEGER) as tiempo_demora
	FROM sgrt.tsgrtcompromisos com
	LEFT JOIN sgrh.tsgrhempleados emp ON com.cod_ejecutor=emp.cod_empleado or com.cod_validador=emp.cod_empleado or com.cod_verificador=emp.cod_empleado
	WHERE com.fec_compromiso=cast(fechaCompromiso as date);
	$$;


ALTER FUNCTION sgrt.compromisos_dia(fechacompromiso text) OWNER TO postgres;

--
-- TOC entry 456 (class 1255 OID 18479)
-- Name: compromisos_generales(); Type: FUNCTION; Schema: sgrt; Owner: postgres
--

CREATE FUNCTION sgrt.compromisos_generales() RETURNS TABLE(cod_empleado integer, cod_reunion integer, nombre text, rol text, area text, descripcion text, minuta text, estatus text, fec_registro text, fec_compromiso text, dias_habiles integer, tiempo_demora integer)
    LANGUAGE sql
    AS $$
	SELECT
	emp.cod_empleado,
	reunion.cod_reunion,
	CONCAT(emp.des_nombre, ' ', emp.des_nombres, ' ', emp.des_apepaterno, ' ', emp.des_apematerno) as nombre,
	CAST('Validador' as text) as rol,
	cast(area.cod_acronimo as text) as area,
	cast(com.des_descripcion as text) as descripcion,
	cast(reunion.des_nombre as text) as minuta,
	cast(com.cod_estatus as text)as estatus,
	cast(to_char(com.fec_solicitud,'DD/MM/YYYY') as text) as fec_registro,
	cast(to_char(com.fec_compromiso,'DD/MM/YYYY') as text),
	cast((select count(the_day) from
	    (select generate_series(com.fec_solicitud+1, com.fec_compromiso, '1 day') as the_day) days
	where extract('dow' from the_day) not in (0,6))as INTEGER) as dias_habiles,
	cast((select count(the_day) from
	    (select generate_series(com.fec_compromiso+1, com.fec_entrega, '1 day') as the_day) days
	where extract('dow' from the_day) not in (0,6))as INTEGER) as tiempo_demora
	FROM sgrt.tsgrtcompromisos com
	LEFT JOIN sgrt.tsgrtreuniones reunion ON reunion.cod_reunion=com.cod_reunion
	LEFT JOIN sgrh.tsgrhempleados emp ON emp.cod_empleado=com.cod_validador
	LEFT JOIN sgrh.tsgrhareas area ON area.cod_area=emp.cod_area
	WHERE emp.cod_empleado=com.cod_validador
	UNION
	SELECT
	emp.cod_empleado,
	reunion.cod_reunion,
	CONCAT(emp.des_nombre, ' ', emp.des_nombres, ' ', emp.des_apepaterno, ' ', emp.des_apematerno) as nombre,
	CAST('Verificador' as text) as rol,
	cast(area.cod_acronimo as text) as area,
	cast(com.des_descripcion as text) as descripcion,
	cast(reunion.des_nombre as text) as minuta,
	cast(com.cod_estatus as text)as estatus,
	cast(to_char(com.fec_solicitud,'DD/MM/YYYY') as text) as fec_registro,
	cast(to_char(com.fec_compromiso,'DD/MM/YYYY') as text),
	cast((select count(the_day) from
	    (select generate_series(com.fec_solicitud+1, com.fec_compromiso, '1 day') as the_day) days
	where extract('dow' from the_day) not in (0,6))as INTEGER) as dias_habiles,
	cast((select count(the_day) from
	    (select generate_series(com.fec_compromiso+1, com.fec_entrega, '1 day') as the_day) days
	where extract('dow' from the_day) not in (0,6))as INTEGER) as tiempo_demora
	FROM sgrt.tsgrtcompromisos com
	LEFT JOIN sgrt.tsgrtreuniones reunion ON reunion.cod_reunion=com.cod_reunion
	LEFT JOIN sgrh.tsgrhempleados emp ON emp.cod_empleado=com.cod_verificador
	LEFT JOIN sgrh.tsgrhareas area ON area.cod_area=emp.cod_area
	WHERE emp.cod_empleado=com.cod_verificador
	UNION
	SELECT
	emp.cod_empleado,
	reunion.cod_reunion,
	CONCAT(emp.des_nombre, ' ', emp.des_nombres, ' ', emp.des_apepaterno, ' ', emp.des_apematerno) as nombre,
	CAST('Ejecutor' as text) as rol,
	cast(area.cod_acronimo as text) as area,
	cast(com.des_descripcion as text) as descripcion,
	cast(reunion.des_nombre as text) as minuta,
	cast(com.cod_estatus as text)as estatus,
	cast(to_char(com.fec_solicitud,'DD/MM/YYYY') as text) as fec_registro,
	cast(to_char(com.fec_compromiso,'DD/MM/YYYY') as text),
	cast((select count(the_day) from
	    (select generate_series(com.fec_solicitud+1, com.fec_compromiso, '1 day') as the_day) days
	where extract('dow' from the_day) not in (0,6))as INTEGER) as dias_habiles,
	cast((select count(the_day) from
	    (select generate_series(com.fec_compromiso+1, com.fec_entrega, '1 day') as the_day) days
	where extract('dow' from the_day) not in (0,6))as INTEGER) as tiempo_demora
	FROM sgrt.tsgrtcompromisos com
	LEFT JOIN sgrt.tsgrtreuniones reunion ON reunion.cod_reunion=com.cod_reunion
	LEFT JOIN sgrh.tsgrhempleados emp ON emp.cod_empleado=com.cod_ejecutor
	LEFT JOIN sgrh.tsgrhareas area ON area.cod_area=emp.cod_area
	WHERE emp.cod_empleado=com.cod_ejecutor
	$$;


ALTER FUNCTION sgrt.compromisos_generales() OWNER TO postgres;

--
-- TOC entry 465 (class 1255 OID 18480)
-- Name: reporte_por_tema(integer); Type: FUNCTION; Schema: sgrt; Owner: postgres
--

CREATE FUNCTION sgrt.reporte_por_tema(reunionid integer) RETURNS TABLE(nombre_minuta character varying, responsable text, fecha text, acuerdos integer, pendientes integer, total integer, tiempo_invertido text)
    LANGUAGE plpgsql
    AS $$
	DECLARE
	    var_r record;
	BEGIN
	   FOR var_r IN(SELECT * FROM sgrt.tsgrtreuniones WHERE cod_reunion=reunionId OR cod_proximareunion=reunionId)
	     LOOP
	              RETURN QUERY
			select c.nombre_minuta, c.responsable, c.fecha,a.Acuerdos, b.Pendientes, cast(COALESCE(a.Acuerdos,0)+COALESCE(b.Pendientes,0)as int) AS Total, c.tiempo_invertido from
			(select cast(count(cod_tipocompromiso) as int) as Acuerdos from sgrt.tsgrtcompromisos where cod_tipocompromiso='Acuerdo' and cod_reunion=var_r.cod_reunion) a,
			(select cast(count(cod_tipocompromiso)as int) as Pendientes from sgrt.tsgrtcompromisos where cod_tipocompromiso='Pendiente' and cod_reunion=var_r.cod_reunion) b,
			(select reunion.des_nombre AS nombre_minuta, CONCAT(empleado.des_nombre, ' ', empleado.des_nombres, ' ', empleado.des_apepaterno, ' ', empleado.des_apematerno) AS responsable,
			CAST(to_char(reunion.fec_fecha, 'DD/MM/YYYY') as text) as fecha, CAST(to_char(reunion.tim_duracion,'HH24:MI') as text) as tiempo_invertido from
			sgrt.tsgrtreuniones reunion,
			sgrh.tsgrhempleados empleado
			WHERE reunion.cod_responsable=empleado.cod_empleado and
			cod_reunion=var_r.cod_reunion) c;
	            END LOOP;
	END; $$;


ALTER FUNCTION sgrt.reporte_por_tema(reunionid integer) OWNER TO postgres;

--
-- TOC entry 457 (class 1255 OID 21956)
-- Name: buscar_asignaciones(date); Type: FUNCTION; Schema: sisat; Owner: postgres
--

CREATE FUNCTION sisat.buscar_asignaciones(fec_creacion date) RETURNS TABLE(cliente character varying, fec_inicio date, fec_termino date, nombre character varying, nombres character varying, ap character varying, am character varying, ubicacion_asg character varying, hospedaje character varying, sueldo numeric)
    LANGUAGE plpgsql
    AS $$
BEGIN
RETURN QUERY
select 
cte.des_nbcliente, c.fec_iniciocontra, c. fec_terminocontra, p.des_nombre, p.des_nombres, p.des_appaterno, p.des_apmaterno, cte.des_direccioncte, c.des_lugarhopedaje, c.imp_sueldomensual  
from sisat.tsisatcartaasignacion c
inner join sgrh.tsgrhclientes cte
on c.cod_cliente=cte.cod_cliente
inner join sisat.tsisatprospectos p
on c.cod_prospecto=p.cod_prospecto
WHERE c.fec_iniciocontra=fec_creacion;

END;
$$;


ALTER FUNCTION sisat.buscar_asignaciones(fec_creacion date) OWNER TO postgres;

--
-- TOC entry 466 (class 1255 OID 31474)
-- Name: buscar_asignacionesporanio(integer); Type: FUNCTION; Schema: sisat; Owner: postgres
--

CREATE FUNCTION sisat.buscar_asignacionesporanio(anio integer) RETURNS TABLE(cliente character varying, fec_inicio date, fec_termino date, nombre character varying, nombres character varying, ap character varying, am character varying, ubicacion_asg character varying, hospedaje character varying, sueldo numeric)
    LANGUAGE plpgsql
    AS $$
BEGIN
RETURN QUERY
select 
cte.des_nbcliente, c.fec_iniciocontra, c.fec_terminocontra, p.des_nombre, p.des_nombres, p.des_appaterno, p.des_apmaterno, cte.des_direccioncte, c.des_lugarhopedaje, c.imp_sueldomensual  
from sisat.tsisatcartaasignacion c
inner join sgrh.tsgrhclientes cte
on c.cod_cliente=cte.cod_cliente
inner join sisat.tsisatprospectos p
on c.cod_prospecto=p.cod_prospecto
WHERE extract (year from c.fec_iniciocontra)=anio;
END;
$$;


ALTER FUNCTION sisat.buscar_asignacionesporanio(anio integer) OWNER TO postgres;

--
-- TOC entry 459 (class 1255 OID 31475)
-- Name: buscar_asignacionesporaniomes(integer, integer); Type: FUNCTION; Schema: sisat; Owner: postgres
--

CREATE FUNCTION sisat.buscar_asignacionesporaniomes(anio integer, mes integer) RETURNS TABLE(cliente character varying, fec_inicio date, fec_termino date, nombre character varying, nombres character varying, ap character varying, am character varying, ubicacion_asg character varying, hospedaje character varying, sueldo numeric)
    LANGUAGE plpgsql
    AS $$
BEGIN
RETURN QUERY
select 
cte.des_nbcliente, c.fec_iniciocontra, c.fec_terminocontra, p.des_nombre, p.des_nombres, p.des_appaterno, p.des_apmaterno, cte.des_direccioncte, c.des_lugarhopedaje, c.imp_sueldomensual  
from sisat.tsisatcartaasignacion c
inner join sgrh.tsgrhclientes cte
on c.cod_cliente=cte.cod_cliente
inner join sisat.tsisatprospectos p
on c.cod_prospecto=p.cod_prospecto
WHERE (extract (year from c.fec_iniciocontra)=anio) and (extract (month from c.fec_iniciocontra)=mes);
END;
$$;


ALTER FUNCTION sisat.buscar_asignacionesporaniomes(anio integer, mes integer) OWNER TO postgres;

--
-- TOC entry 475 (class 1255 OID 31589)
-- Name: buscar_carta_detalle_asignacion(integer); Type: FUNCTION; Schema: sisat; Owner: postgres
--

CREATE FUNCTION sisat.buscar_carta_detalle_asignacion(asignacion_cod integer) RETURNS TABLE(cod_asignacion integer, cod_prospecto integer, nombre_completo text, fec_creacion date, des_observacion character varying, des_actividades character varying)
    LANGUAGE plpgsql
    AS $$	
BEGIN 	
RETURN QUERY		
SELECT c.cod_asignacion,p.cod_prospecto, p.des_nombre ||' '|| p.des_appaterno ||' '|| p.des_apmaterno AS Nombre_Completo,
	   c.fec_creacion, c.des_observacion, c.des_actividades 	
FROM sisat.tsisatprospectos p 
inner join sisat.tsisatcartaasignacion c on p.cod_prospecto= c.cod_asignacion
WHERE c.cod_asignacion= asignacion_cod	
GROUP BY c.cod_asignacion,p.cod_prospecto, nombre_completo, c.fec_creacion, c.des_observacion,c.des_actividades 
ORDER BY cod_prospecto  asc;
END;
$$;


ALTER FUNCTION sisat.buscar_carta_detalle_asignacion(asignacion_cod integer) OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 428 (class 1259 OID 31595)
-- Name: rtsueldobase; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.rtsueldobase (
    imp_honorarios numeric(10,2)
);


ALTER TABLE public.rtsueldobase OWNER TO postgres;

--
-- TOC entry 205 (class 1259 OID 16454)
-- Name: seq_sistema; Type: SEQUENCE; Schema: sgco; Owner: postgres
--

CREATE SEQUENCE sgco.seq_sistema
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgco.seq_sistema OWNER TO postgres;

--
-- TOC entry 206 (class 1259 OID 16456)
-- Name: seq_tipousuario; Type: SEQUENCE; Schema: sgco; Owner: postgres
--

CREATE SEQUENCE sgco.seq_tipousuario
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgco.seq_tipousuario OWNER TO postgres;

--
-- TOC entry 207 (class 1259 OID 16458)
-- Name: seq_usuarios; Type: SEQUENCE; Schema: sgco; Owner: postgres
--

CREATE SEQUENCE sgco.seq_usuarios
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgco.seq_usuarios OWNER TO postgres;

--
-- TOC entry 208 (class 1259 OID 16460)
-- Name: tsgcosistemas; Type: TABLE; Schema: sgco; Owner: postgres
--

CREATE TABLE sgco.tsgcosistemas (
    cod_sistema integer NOT NULL,
    des_nbsistema character varying(50) NOT NULL,
    des_descripcion character varying(300)
);


ALTER TABLE sgco.tsgcosistemas OWNER TO postgres;

--
-- TOC entry 209 (class 1259 OID 16463)
-- Name: tsgcotipousuario; Type: TABLE; Schema: sgco; Owner: postgres
--

CREATE TABLE sgco.tsgcotipousuario (
    cod_tipousuario integer NOT NULL,
    cod_usuario integer NOT NULL,
    cod_sistema integer NOT NULL,
    cod_rol character varying(35) NOT NULL
);


ALTER TABLE sgco.tsgcotipousuario OWNER TO postgres;

--
-- TOC entry 210 (class 1259 OID 16466)
-- Name: tsgcousuarios; Type: TABLE; Schema: sgco; Owner: postgres
--

CREATE TABLE sgco.tsgcousuarios (
    cod_usuario integer NOT NULL,
    cod_empleado integer NOT NULL,
    des_correo character varying(60) NOT NULL,
    des_contrasenacorreo character varying(50) NOT NULL,
    cod_usuariosistema character varying(30) NOT NULL,
    des_contrasenasistema character varying(30) NOT NULL
);


ALTER TABLE sgco.tsgcousuarios OWNER TO postgres;

--
-- TOC entry 426 (class 1259 OID 22465)
-- Name: seq_cabecera; Type: SEQUENCE; Schema: sgnom; Owner: suite
--

CREATE SEQUENCE sgnom.seq_cabecera
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgnom.seq_cabecera OWNER TO suite;

--
-- TOC entry 427 (class 1259 OID 22468)
-- Name: seq_empquincena; Type: SEQUENCE; Schema: sgnom; Owner: suite
--

CREATE SEQUENCE sgnom.seq_empquincena
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgnom.seq_empquincena OWNER TO suite;

--
-- TOC entry 429 (class 1259 OID 39934)
-- Name: seq_incidencia; Type: SEQUENCE; Schema: sgnom; Owner: postgres
--

CREATE SEQUENCE sgnom.seq_incidencia
    START WITH 10
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgnom.seq_incidencia OWNER TO postgres;

--
-- TOC entry 348 (class 1259 OID 20873)
-- Name: tsgnomalguinaldo; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnomalguinaldo (
    cod_aguinaldoid integer NOT NULL,
    imp_aguinaldo numeric(10,2) NOT NULL,
    cod_tipoaguinaldo character(1) NOT NULL,
    cod_empquincenaid_fk integer NOT NULL,
    bol_estatus boolean NOT NULL,
    xml_desgloce xml
);


ALTER TABLE sgnom.tsgnomalguinaldo OWNER TO suite;

--
-- TOC entry 4755 (class 0 OID 0)
-- Dependencies: 348
-- Name: TABLE tsgnomalguinaldo; Type: COMMENT; Schema: sgnom; Owner: suite
--

COMMENT ON TABLE sgnom.tsgnomalguinaldo IS 'cod_tipoaguinaldo

(

i = imss,

h = honorarios

)';


--
-- TOC entry 349 (class 1259 OID 20879)
-- Name: tsgnomargumento; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnomargumento (
    cod_argumentoid integer NOT NULL,
    cod_nbargumento character varying(30) NOT NULL,
    cod_clavearg character varying(5) NOT NULL,
    imp_valorconst numeric(10,2),
    des_funcionbd character varying(60),
    cod_tipoargumento character(1),
    bol_estatus boolean NOT NULL,
    txt_descripcion text NOT NULL,
    aud_codcreadopor integer NOT NULL,
    aud_codmodificadopor integer,
    aud_feccreacion date NOT NULL,
    aud_fecmodificacion date
);


ALTER TABLE sgnom.tsgnomargumento OWNER TO suite;

--
-- TOC entry 350 (class 1259 OID 20885)
-- Name: tsgnombitacora; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnombitacora (
    cod_bitacoraid integer NOT NULL,
    xml_bitacora xml NOT NULL,
    cod_tablaid_fk integer NOT NULL
);


ALTER TABLE sgnom.tsgnombitacora OWNER TO suite;

--
-- TOC entry 351 (class 1259 OID 20891)
-- Name: tsgnomcabecera; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnomcabecera (
    cod_cabeceraid integer DEFAULT nextval('sgnom.seq_cabecera'::regclass) NOT NULL,
    cod_nbnomina character varying(40) NOT NULL,
    fec_creacion date NOT NULL,
    fec_ejecucion date,
    fec_cierre date,
    imp_totpercepcion numeric(10,2) NOT NULL,
    imp_totdeduccion numeric(10,2) NOT NULL,
    imp_totalemp numeric(10,2) NOT NULL,
    cod_quincenaid_fk integer NOT NULL,
    cod_tiponominaid_fk integer NOT NULL,
    cod_estatusnomid_fk integer NOT NULL,
    cnu_totalemp integer,
    aud_codcreadopor integer NOT NULL,
    aud_codmodificadopor integer,
    aud_feccreacion date NOT NULL,
    aud_fecmodificacion date
);


ALTER TABLE sgnom.tsgnomcabecera OWNER TO suite;

--
-- TOC entry 352 (class 1259 OID 20894)
-- Name: tsgnomcabeceraht; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnomcabeceraht (
    cod_cabeceraid integer NOT NULL,
    cod_nbnomina character varying(40) NOT NULL,
    fec_creacion date NOT NULL,
    fec_ejecucion date,
    fec_cierre date,
    imp_totpercepcion numeric(10,2),
    imp_totdeduccion numeric(10,2),
    imp_totalemp numeric(10,2),
    cod_quincenaid_fk integer NOT NULL,
    cod_tiponominaid_fk integer NOT NULL,
    cod_estatusnomid_fk integer NOT NULL,
    cnu_totalemp integer,
    aud_codcreadopor integer NOT NULL,
    aud_codmodificadopor integer,
    aud_feccreacion date NOT NULL,
    aud_fecmodificacion date
);


ALTER TABLE sgnom.tsgnomcabeceraht OWNER TO suite;

--
-- TOC entry 353 (class 1259 OID 20897)
-- Name: tsgnomcalculo; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnomcalculo (
    cod_calculoid integer NOT NULL,
    cod_tpcalculo character varying(25) NOT NULL,
    bol_estatus boolean NOT NULL
);


ALTER TABLE sgnom.tsgnomcalculo OWNER TO suite;

--
-- TOC entry 4761 (class 0 OID 0)
-- Dependencies: 353
-- Name: TABLE tsgnomcalculo; Type: COMMENT; Schema: sgnom; Owner: suite
--

COMMENT ON TABLE sgnom.tsgnomcalculo IS 'bol_estatus

(

activo, inactivo

)';


--
-- TOC entry 354 (class 1259 OID 20900)
-- Name: tsgnomcatincidencia; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnomcatincidencia (
    cod_catincidenciaid integer NOT NULL,
    cod_claveincidencia character varying(5) NOT NULL,
    cod_nbincidencia character varying(20),
    cod_perfilincidencia character varying(25),
    bol_estatus boolean,
    cod_tipoincidencia character(1),
    imp_monto numeric(10,2)
);


ALTER TABLE sgnom.tsgnomcatincidencia OWNER TO suite;

--
-- TOC entry 4763 (class 0 OID 0)
-- Dependencies: 354
-- Name: TABLE tsgnomcatincidencia; Type: COMMENT; Schema: sgnom; Owner: suite
--

COMMENT ON TABLE sgnom.tsgnomcatincidencia IS 'cod_tipoincidencia

(

1 = horas

2 = dias

3 = actividad

)';


--
-- TOC entry 355 (class 1259 OID 20903)
-- Name: tsgnomclasificador; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnomclasificador (
    cod_clasificadorid integer NOT NULL,
    cod_tpclasificador character varying(20),
    bol_estatus boolean
);


ALTER TABLE sgnom.tsgnomclasificador OWNER TO suite;

--
-- TOC entry 356 (class 1259 OID 20906)
-- Name: tsgnomcncptoquinc; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnomcncptoquinc (
    cod_cncptoquincid integer NOT NULL,
    cod_empquincenaid_fk integer NOT NULL,
    cod_conceptoid_fk integer NOT NULL,
    imp_concepto numeric(10,2) NOT NULL,
    imp_gravado numeric(10,2),
    imp_exento numeric(10,2),
    xml_desgloce xml
);


ALTER TABLE sgnom.tsgnomcncptoquinc OWNER TO suite;

--
-- TOC entry 357 (class 1259 OID 20912)
-- Name: tsgnomcncptoquincht; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnomcncptoquincht (
    cod_cncptoquinchtid integer NOT NULL,
    cod_empquincenaid_fk integer NOT NULL,
    cod_conceptoid_fk integer NOT NULL,
    imp_concepto numeric(10,2) NOT NULL,
    imp_gravado numeric(10,2),
    imp_exento numeric(10,2),
    xml_desgloce xml
);


ALTER TABLE sgnom.tsgnomcncptoquincht OWNER TO suite;

--
-- TOC entry 358 (class 1259 OID 20918)
-- Name: tsgnomconcepto; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnomconcepto (
    cod_conceptoid integer NOT NULL,
    cod_nbconcepto character varying(20) NOT NULL,
    cod_claveconcepto character varying(4) NOT NULL,
    cnu_prioricalculo integer NOT NULL,
    cnu_articulo integer NOT NULL,
    bol_estatus boolean NOT NULL,
    cod_formulaid_fk integer,
    cod_tipoconceptoid_fk integer NOT NULL,
    cod_calculoid_fk integer NOT NULL,
    cod_conceptosatid_fk integer NOT NULL,
    cod_frecuenciapago character varying(20) NOT NULL,
    cod_partidaprep integer NOT NULL,
    cnu_cuentacontable integer NOT NULL,
    cod_gravado character(1),
    cod_excento character(1),
    bol_aplicaisn boolean,
    bol_retroactividad boolean NOT NULL,
    cnu_topeex integer,
    cod_clasificadorid_fk integer NOT NULL,
    cod_tiponominaid_fk integer,
    aud_codcreadopor integer NOT NULL,
    aud_codmodificadopor integer,
    aud_feccreacion date NOT NULL,
    aud_fecmodificacion date
);


ALTER TABLE sgnom.tsgnomconcepto OWNER TO suite;

--
-- TOC entry 4768 (class 0 OID 0)
-- Dependencies: 358
-- Name: TABLE tsgnomconcepto; Type: COMMENT; Schema: sgnom; Owner: suite
--

COMMENT ON TABLE sgnom.tsgnomconcepto IS 'bol_estatus

(

activo, inactivo

)



gravado

(

1 = dias, 

2 = porcentaje, 

3 = no aplica

)



excento

(

activo, inactivo

)





validar tope_ex';


--
-- TOC entry 359 (class 1259 OID 20921)
-- Name: tsgnomconceptosat; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnomconceptosat (
    cod_conceptosatid integer NOT NULL,
    des_conceptosat character varying(51) NOT NULL,
    des_descconcepto character varying(51) NOT NULL,
    bol_estatus boolean NOT NULL
);


ALTER TABLE sgnom.tsgnomconceptosat OWNER TO suite;

--
-- TOC entry 4770 (class 0 OID 0)
-- Dependencies: 359
-- Name: TABLE tsgnomconceptosat; Type: COMMENT; Schema: sgnom; Owner: suite
--

COMMENT ON TABLE sgnom.tsgnomconceptosat IS 'bol_estatus

(

activo, inactivo

)';


--
-- TOC entry 360 (class 1259 OID 20924)
-- Name: tsgnomconfpago; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnomconfpago (
    cod_confpagoid integer NOT NULL,
    bol_pagoempleado boolean,
    bol_pagorh boolean,
    bol_pagofinanzas boolean,
    cod_empquincenaid_fk integer
);


ALTER TABLE sgnom.tsgnomconfpago OWNER TO suite;

--
-- TOC entry 4772 (class 0 OID 0)
-- Dependencies: 360
-- Name: TABLE tsgnomconfpago; Type: COMMENT; Schema: sgnom; Owner: suite
--

COMMENT ON TABLE sgnom.tsgnomconfpago IS 'bol_pagoempleado

(

confirmado, pendiente 

)



pago_rh

(

autorizado, pendiente

)



pago_fnzas

(

autorizado, pendiente

)';


--
-- TOC entry 361 (class 1259 OID 20927)
-- Name: tsgnomejercicio; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnomejercicio (
    cod_ejercicioid integer NOT NULL,
    cnu_valorejercicio integer NOT NULL,
    bol_estatus boolean NOT NULL
);


ALTER TABLE sgnom.tsgnomejercicio OWNER TO suite;

--
-- TOC entry 362 (class 1259 OID 20930)
-- Name: tsgnomempleados; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnomempleados (
    cod_empleadoid integer NOT NULL,
    fec_ingreso date NOT NULL,
    fec_salida date,
    bol_estatus boolean NOT NULL,
    cod_empleado_fk integer NOT NULL,
    imp_sueldoimss numeric(10,2),
    imp_honorarios numeric(10,2),
    imp_finiquito numeric(10,2),
    cod_tipoimss character(1),
    cod_tipohonorarios character(1),
    cod_banco character varying(50),
    cod_sucursal integer,
    cod_cuenta character varying(20),
    txt_descripcionbaja text,
    aud_codcreadopor integer NOT NULL,
    aud_feccreacion date NOT NULL,
    aud_codmodificadopor integer,
    aud_fecmodificacion date,
    cod_clabe character varying(18)
);


ALTER TABLE sgnom.tsgnomempleados OWNER TO suite;

--
-- TOC entry 4775 (class 0 OID 0)
-- Dependencies: 362
-- Name: TABLE tsgnomempleados; Type: COMMENT; Schema: sgnom; Owner: suite
--

COMMENT ON TABLE sgnom.tsgnomempleados IS 'cod_empleado_fk hace referencia al schema sgrh en la tabla tsgrhempleados';


--
-- TOC entry 363 (class 1259 OID 20936)
-- Name: tsgnomempquincena; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnomempquincena (
    cod_empquincenaid integer DEFAULT nextval('sgnom.seq_empquincena'::regclass) NOT NULL,
    cod_empleadoid_fk integer NOT NULL,
    cod_cabeceraid_fk integer NOT NULL,
    imp_totpercepcion numeric(10,2) NOT NULL,
    imp_totdeduccion numeric(10,2) NOT NULL,
    imp_totalemp numeric(10,2) NOT NULL,
    bol_estatusemp boolean NOT NULL
);


ALTER TABLE sgnom.tsgnomempquincena OWNER TO suite;

--
-- TOC entry 4777 (class 0 OID 0)
-- Dependencies: 363
-- Name: TABLE tsgnomempquincena; Type: COMMENT; Schema: sgnom; Owner: suite
--

COMMENT ON TABLE sgnom.tsgnomempquincena IS 'bol_estatusemp

(

activo, inactivo

)';


--
-- TOC entry 364 (class 1259 OID 20939)
-- Name: tsgnomempquincenaht; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnomempquincenaht (
    cod_empquincenahtid integer NOT NULL,
    cod_empleadoid_fk integer NOT NULL,
    cod_cabeceraid_fk integer NOT NULL,
    imp_totpercepcion numeric(10,2) NOT NULL,
    imp_totdeduccion numeric(10,2) NOT NULL,
    imp_totalemp numeric(10,2) NOT NULL,
    bol_estatusemp boolean NOT NULL
);


ALTER TABLE sgnom.tsgnomempquincenaht OWNER TO suite;

--
-- TOC entry 365 (class 1259 OID 20942)
-- Name: tsgnomestatusnom; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnomestatusnom (
    cod_estatusnomid integer NOT NULL,
    cod_estatusnomina character varying(15) NOT NULL,
    bol_estatus boolean NOT NULL
);


ALTER TABLE sgnom.tsgnomestatusnom OWNER TO suite;

--
-- TOC entry 4780 (class 0 OID 0)
-- Dependencies: 365
-- Name: TABLE tsgnomestatusnom; Type: COMMENT; Schema: sgnom; Owner: suite
--

COMMENT ON TABLE sgnom.tsgnomestatusnom IS 'cod_estatusnomid 

estatus

(

abierta,

calculada,

revision,

validada,

cerrada

)



nota: en caso de no ser validada entra en revision y posteriormente calcular nuevamente 

hasta ser validada';


--
-- TOC entry 366 (class 1259 OID 20945)
-- Name: tsgnomformula; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnomformula (
    cod_formulaid integer NOT NULL,
    des_nbformula character varying(60) NOT NULL,
    des_formula character varying(250) NOT NULL,
    bol_estatus boolean NOT NULL
);


ALTER TABLE sgnom.tsgnomformula OWNER TO suite;

--
-- TOC entry 4782 (class 0 OID 0)
-- Dependencies: 366
-- Name: TABLE tsgnomformula; Type: COMMENT; Schema: sgnom; Owner: suite
--

COMMENT ON TABLE sgnom.tsgnomformula IS 'bol_estatus

(

activo, inactivo

)';


--
-- TOC entry 367 (class 1259 OID 20948)
-- Name: tsgnomfuncion; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnomfuncion (
    cod_funcionid integer NOT NULL,
    cod_nbfuncion character varying(15) NOT NULL,
    bol_estatus boolean NOT NULL
);


ALTER TABLE sgnom.tsgnomfuncion OWNER TO suite;

--
-- TOC entry 368 (class 1259 OID 20951)
-- Name: tsgnomhisttabla; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnomhisttabla (
    cod_tablaid integer NOT NULL,
    cod_nbtabla character varying(18) NOT NULL,
    bol_estatus boolean NOT NULL
);


ALTER TABLE sgnom.tsgnomhisttabla OWNER TO suite;

--
-- TOC entry 369 (class 1259 OID 20954)
-- Name: tsgnomincidencia; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnomincidencia (
    cod_incidenciaid integer NOT NULL,
    cod_catincidenciaid_fk integer NOT NULL,
    cnu_cantidad smallint,
    des_actividad character varying(100),
    txt_comentarios text,
    cod_empreporta_fk integer,
    cod_empautoriza_fk integer,
    imp_monto numeric(10,2),
    xml_detcantidad xml,
    bol_estatus boolean,
    cod_quincenaid_fk integer NOT NULL,
    bol_validacion boolean,
    fec_validacion date,
    aud_codcreadopor integer NOT NULL,
    aud_feccreacion date NOT NULL,
    aud_codmodificadopor integer,
    aud_fecmodificacion date
);


ALTER TABLE sgnom.tsgnomincidencia OWNER TO suite;

--
-- TOC entry 4786 (class 0 OID 0)
-- Dependencies: 369
-- Name: TABLE tsgnomincidencia; Type: COMMENT; Schema: sgnom; Owner: suite
--

COMMENT ON TABLE sgnom.tsgnomincidencia IS 'bol_estatus: (activo, inactivo) (1, 0)

validacion: (validar, denegar) (1, 0) 



';


--
-- TOC entry 370 (class 1259 OID 20960)
-- Name: tsgnommanterceros; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnommanterceros (
    cod_mantercerosid integer NOT NULL,
    cod_conceptoid_fk integer,
    imp_monto numeric(10,2),
    cod_quincenainicio_fk integer,
    cod_quincenafin_fk integer,
    cod_empleadoid_fk integer,
    cod_frecuenciapago character varying(20),
    bol_estatus boolean,
    aud_codcreadopor integer NOT NULL,
    aud_codmodificadopor integer,
    aud_fecreacion date NOT NULL,
    aud_fecmodificacion date
);


ALTER TABLE sgnom.tsgnommanterceros OWNER TO suite;

--
-- TOC entry 371 (class 1259 OID 20963)
-- Name: tsgnomquincena; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnomquincena (
    cod_quincenaid integer NOT NULL,
    des_quincena character varying(70) NOT NULL,
    fec_inicio date NOT NULL,
    fec_fin date NOT NULL,
    fec_pago date NOT NULL,
    fec_dispersion date NOT NULL,
    cnu_numquincena integer NOT NULL,
    cod_ejercicioid_fk integer NOT NULL,
    bol_estatus boolean NOT NULL
);


ALTER TABLE sgnom.tsgnomquincena OWNER TO suite;

--
-- TOC entry 372 (class 1259 OID 20966)
-- Name: tsgnomtipoconcepto; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnomtipoconcepto (
    cod_tipoconceptoid integer NOT NULL,
    cod_tipoconcepto character varying(25) NOT NULL,
    bol_estatus boolean NOT NULL
);


ALTER TABLE sgnom.tsgnomtipoconcepto OWNER TO suite;

--
-- TOC entry 4790 (class 0 OID 0)
-- Dependencies: 372
-- Name: TABLE tsgnomtipoconcepto; Type: COMMENT; Schema: sgnom; Owner: suite
--

COMMENT ON TABLE sgnom.tsgnomtipoconcepto IS 'bol_estatus

(

activo, inactivo

)';


--
-- TOC entry 373 (class 1259 OID 20969)
-- Name: tsgnomtiponomina; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnomtiponomina (
    cod_tiponominaid integer NOT NULL,
    cod_nomina character varying(30) NOT NULL,
    bol_estatus boolean NOT NULL
);


ALTER TABLE sgnom.tsgnomtiponomina OWNER TO suite;

--
-- TOC entry 4792 (class 0 OID 0)
-- Dependencies: 373
-- Name: TABLE tsgnomtiponomina; Type: COMMENT; Schema: sgnom; Owner: suite
--

COMMENT ON TABLE sgnom.tsgnomtiponomina IS 'bol_estatus (activa, inactiva)';


--
-- TOC entry 273 (class 1259 OID 18807)
-- Name: seq_area; Type: SEQUENCE; Schema: sgrh; Owner: postgres
--

CREATE SEQUENCE sgrh.seq_area
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_area OWNER TO postgres;

--
-- TOC entry 274 (class 1259 OID 18809)
-- Name: seq_capacitaciones; Type: SEQUENCE; Schema: sgrh; Owner: postgres
--

CREATE SEQUENCE sgrh.seq_capacitaciones
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_capacitaciones OWNER TO postgres;

--
-- TOC entry 275 (class 1259 OID 18811)
-- Name: seq_cartaasignacion; Type: SEQUENCE; Schema: sgrh; Owner: postgres
--

CREATE SEQUENCE sgrh.seq_cartaasignacion
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_cartaasignacion OWNER TO postgres;

--
-- TOC entry 276 (class 1259 OID 18813)
-- Name: seq_cat_encuesta_participantes; Type: SEQUENCE; Schema: sgrh; Owner: postgres
--

CREATE SEQUENCE sgrh.seq_cat_encuesta_participantes
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_cat_encuesta_participantes OWNER TO postgres;

--
-- TOC entry 277 (class 1259 OID 18815)
-- Name: seq_catrespuestas; Type: SEQUENCE; Schema: sgrh; Owner: postgres
--

CREATE SEQUENCE sgrh.seq_catrespuestas
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_catrespuestas OWNER TO postgres;

--
-- TOC entry 278 (class 1259 OID 18817)
-- Name: seq_clientes; Type: SEQUENCE; Schema: sgrh; Owner: postgres
--

CREATE SEQUENCE sgrh.seq_clientes
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_clientes OWNER TO postgres;

--
-- TOC entry 279 (class 1259 OID 18819)
-- Name: seq_contrataciones; Type: SEQUENCE; Schema: sgrh; Owner: postgres
--

CREATE SEQUENCE sgrh.seq_contrataciones
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_contrataciones OWNER TO postgres;

--
-- TOC entry 280 (class 1259 OID 18821)
-- Name: seq_contratos; Type: SEQUENCE; Schema: sgrh; Owner: postgres
--

CREATE SEQUENCE sgrh.seq_contratos
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_contratos OWNER TO postgres;

--
-- TOC entry 281 (class 1259 OID 18823)
-- Name: seq_empleado; Type: SEQUENCE; Schema: sgrh; Owner: postgres
--

CREATE SEQUENCE sgrh.seq_empleado
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_empleado OWNER TO postgres;

--
-- TOC entry 282 (class 1259 OID 18825)
-- Name: seq_encuestas; Type: SEQUENCE; Schema: sgrh; Owner: postgres
--

CREATE SEQUENCE sgrh.seq_encuestas
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_encuestas OWNER TO postgres;

--
-- TOC entry 283 (class 1259 OID 18827)
-- Name: seq_escolaridad; Type: SEQUENCE; Schema: sgrh; Owner: postgres
--

CREATE SEQUENCE sgrh.seq_escolaridad
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_escolaridad OWNER TO postgres;

--
-- TOC entry 284 (class 1259 OID 18829)
-- Name: seq_estatus; Type: SEQUENCE; Schema: sgrh; Owner: postgres
--

CREATE SEQUENCE sgrh.seq_estatus
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_estatus OWNER TO postgres;

--
-- TOC entry 285 (class 1259 OID 18831)
-- Name: seq_evacapacitacion; Type: SEQUENCE; Schema: sgrh; Owner: postgres
--

CREATE SEQUENCE sgrh.seq_evacapacitacion
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_evacapacitacion OWNER TO postgres;

--
-- TOC entry 286 (class 1259 OID 18833)
-- Name: seq_evacontestadas; Type: SEQUENCE; Schema: sgrh; Owner: postgres
--

CREATE SEQUENCE sgrh.seq_evacontestadas
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_evacontestadas OWNER TO postgres;

--
-- TOC entry 287 (class 1259 OID 18835)
-- Name: seq_evaluaciones; Type: SEQUENCE; Schema: sgrh; Owner: postgres
--

CREATE SEQUENCE sgrh.seq_evaluaciones
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_evaluaciones OWNER TO postgres;

--
-- TOC entry 288 (class 1259 OID 18837)
-- Name: seq_experiencialab; Type: SEQUENCE; Schema: sgrh; Owner: postgres
--

CREATE SEQUENCE sgrh.seq_experiencialab
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_experiencialab OWNER TO postgres;

--
-- TOC entry 289 (class 1259 OID 18839)
-- Name: seq_factoreseva; Type: SEQUENCE; Schema: sgrh; Owner: postgres
--

CREATE SEQUENCE sgrh.seq_factoreseva
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_factoreseva OWNER TO postgres;

--
-- TOC entry 290 (class 1259 OID 18841)
-- Name: seq_idiomas; Type: SEQUENCE; Schema: sgrh; Owner: postgres
--

CREATE SEQUENCE sgrh.seq_idiomas
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_idiomas OWNER TO postgres;

--
-- TOC entry 291 (class 1259 OID 18843)
-- Name: seq_logistica; Type: SEQUENCE; Schema: sgrh; Owner: postgres
--

CREATE SEQUENCE sgrh.seq_logistica
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_logistica OWNER TO postgres;

--
-- TOC entry 292 (class 1259 OID 18845)
-- Name: seq_lugar; Type: SEQUENCE; Schema: sgrh; Owner: postgres
--

CREATE SEQUENCE sgrh.seq_lugar
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_lugar OWNER TO postgres;

--
-- TOC entry 293 (class 1259 OID 18847)
-- Name: seq_modo; Type: SEQUENCE; Schema: sgrh; Owner: postgres
--

CREATE SEQUENCE sgrh.seq_modo
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_modo OWNER TO postgres;

--
-- TOC entry 294 (class 1259 OID 18849)
-- Name: seq_perfiles; Type: SEQUENCE; Schema: sgrh; Owner: postgres
--

CREATE SEQUENCE sgrh.seq_perfiles
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_perfiles OWNER TO postgres;

--
-- TOC entry 295 (class 1259 OID 18851)
-- Name: seq_plancapacitacion; Type: SEQUENCE; Schema: sgrh; Owner: postgres
--

CREATE SEQUENCE sgrh.seq_plancapacitacion
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_plancapacitacion OWNER TO postgres;

--
-- TOC entry 296 (class 1259 OID 18853)
-- Name: seq_planesoperativos; Type: SEQUENCE; Schema: sgrh; Owner: postgres
--

CREATE SEQUENCE sgrh.seq_planesoperativos
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_planesoperativos OWNER TO postgres;

--
-- TOC entry 297 (class 1259 OID 18855)
-- Name: seq_preguntasenc; Type: SEQUENCE; Schema: sgrh; Owner: postgres
--

CREATE SEQUENCE sgrh.seq_preguntasenc
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_preguntasenc OWNER TO postgres;

--
-- TOC entry 298 (class 1259 OID 18857)
-- Name: seq_preguntaseva; Type: SEQUENCE; Schema: sgrh; Owner: postgres
--

CREATE SEQUENCE sgrh.seq_preguntaseva
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_preguntaseva OWNER TO postgres;

--
-- TOC entry 299 (class 1259 OID 18859)
-- Name: seq_proceso; Type: SEQUENCE; Schema: sgrh; Owner: postgres
--

CREATE SEQUENCE sgrh.seq_proceso
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_proceso OWNER TO postgres;

--
-- TOC entry 300 (class 1259 OID 18861)
-- Name: seq_proveedor; Type: SEQUENCE; Schema: sgrh; Owner: postgres
--

CREATE SEQUENCE sgrh.seq_proveedor
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_proveedor OWNER TO postgres;

--
-- TOC entry 301 (class 1259 OID 18863)
-- Name: seq_puestos; Type: SEQUENCE; Schema: sgrh; Owner: postgres
--

CREATE SEQUENCE sgrh.seq_puestos
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_puestos OWNER TO postgres;

--
-- TOC entry 302 (class 1259 OID 18865)
-- Name: seq_respuestasenc; Type: SEQUENCE; Schema: sgrh; Owner: postgres
--

CREATE SEQUENCE sgrh.seq_respuestasenc
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_respuestasenc OWNER TO postgres;

--
-- TOC entry 303 (class 1259 OID 18867)
-- Name: seq_respuestaseva; Type: SEQUENCE; Schema: sgrh; Owner: postgres
--

CREATE SEQUENCE sgrh.seq_respuestaseva
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_respuestaseva OWNER TO postgres;

--
-- TOC entry 304 (class 1259 OID 18869)
-- Name: seq_revplanesoperativos; Type: SEQUENCE; Schema: sgrh; Owner: postgres
--

CREATE SEQUENCE sgrh.seq_revplanesoperativos
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_revplanesoperativos OWNER TO postgres;

--
-- TOC entry 305 (class 1259 OID 18871)
-- Name: seq_rolempleado; Type: SEQUENCE; Schema: sgrh; Owner: postgres
--

CREATE SEQUENCE sgrh.seq_rolempleado
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_rolempleado OWNER TO postgres;

--
-- TOC entry 306 (class 1259 OID 18873)
-- Name: seq_subfactoreseva; Type: SEQUENCE; Schema: sgrh; Owner: postgres
--

CREATE SEQUENCE sgrh.seq_subfactoreseva
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_subfactoreseva OWNER TO postgres;

--
-- TOC entry 307 (class 1259 OID 18875)
-- Name: seq_tipocapacitacion; Type: SEQUENCE; Schema: sgrh; Owner: postgres
--

CREATE SEQUENCE sgrh.seq_tipocapacitacion
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_tipocapacitacion OWNER TO postgres;

--
-- TOC entry 308 (class 1259 OID 18877)
-- Name: seq_validaevaluaciones; Type: SEQUENCE; Schema: sgrh; Owner: postgres
--

CREATE SEQUENCE sgrh.seq_validaevaluaciones
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_validaevaluaciones OWNER TO postgres;

--
-- TOC entry 309 (class 1259 OID 18879)
-- Name: tsgrhareas; Type: TABLE; Schema: sgrh; Owner: postgres
--

CREATE TABLE sgrh.tsgrhareas (
    cod_area integer DEFAULT nextval('sgrh.seq_area'::regclass) NOT NULL,
    des_nbarea character varying(50) NOT NULL,
    cod_acronimo character varying(5) NOT NULL,
    cnu_activo boolean NOT NULL,
    cod_sistemasuite integer,
    cod_creadopor integer,
    cod_modificadopor integer,
    fec_creacion date,
    fec_modificacion date
);


ALTER TABLE sgrh.tsgrhareas OWNER TO postgres;

--
-- TOC entry 310 (class 1259 OID 18883)
-- Name: tsgrhasignacionesemp; Type: TABLE; Schema: sgrh; Owner: postgres
--

CREATE TABLE sgrh.tsgrhasignacionesemp (
    cod_asignacion integer NOT NULL,
    cod_empleado integer NOT NULL,
    cod_puesto integer NOT NULL,
    cod_asignadopor integer NOT NULL,
    cod_modificadopor integer,
    status character varying(10) NOT NULL,
    fec_creacion date NOT NULL,
    fec_modificacion date
);


ALTER TABLE sgrh.tsgrhasignacionesemp OWNER TO postgres;

--
-- TOC entry 311 (class 1259 OID 18886)
-- Name: tsgrhcapacitaciones; Type: TABLE; Schema: sgrh; Owner: postgres
--

CREATE TABLE sgrh.tsgrhcapacitaciones (
    cod_capacitacion integer DEFAULT nextval('sgrh.seq_capacitaciones'::regclass) NOT NULL,
    cod_empleado integer NOT NULL,
    cod_tipocurso character varying(40) NOT NULL,
    des_nbcurso character varying(50) NOT NULL,
    des_organismo character varying(50) NOT NULL,
    fec_termino date NOT NULL,
    des_duracion character varying(40) NOT NULL,
    bin_documento bytea
);


ALTER TABLE sgrh.tsgrhcapacitaciones OWNER TO postgres;

--
-- TOC entry 312 (class 1259 OID 18893)
-- Name: tsgrhcartaasignacion; Type: TABLE; Schema: sgrh; Owner: postgres
--

CREATE TABLE sgrh.tsgrhcartaasignacion (
    cod_asignacion integer DEFAULT nextval('sgrh.seq_cartaasignacion'::regclass) NOT NULL,
    cod_empleado integer NOT NULL,
    cod_perfil integer NOT NULL,
    des_actividades character varying(200),
    des_lugarsalida character varying(100),
    des_lugarllegada character varying(100),
    fec_salida date,
    fec_llegada date,
    cod_transporte character varying(20),
    des_lugarhopedaje character varying(60),
    fec_hospedaje date,
    des_computadora character varying(150),
    cod_telefono character varying(16),
    des_accesorios character varying(150),
    des_nbresponsable character varying(50),
    des_nbpuesto character varying(50),
    des_lugarresp character varying(100),
    cod_telefonoresp character varying(16),
    tim_horario time without time zone,
    fec_iniciocontra date,
    fec_terminocontra date,
    imp_sueldomensual numeric(6,2),
    imp_nominaimss numeric(6,2),
    imp_honorarios numeric(6,2),
    imp_otros numeric(6,2),
    cod_rfc character varying(13),
    des_razonsocial character varying(45),
    des_correo character varying(50),
    cod_cpostal integer,
    des_direccionfact character varying(200),
    cod_cliente integer NOT NULL,
    cod_gpy integer,
    cod_rhta integer,
    cod_ape integer,
    cod_rys integer,
    cod_creadopor integer NOT NULL,
    cod_modificadopor integer NOT NULL,
    fec_creacion date DEFAULT CURRENT_DATE NOT NULL,
    fec_modificacion date DEFAULT CURRENT_DATE NOT NULL
);


ALTER TABLE sgrh.tsgrhcartaasignacion OWNER TO postgres;

--
-- TOC entry 313 (class 1259 OID 18902)
-- Name: tsgrhcatrespuestas; Type: TABLE; Schema: sgrh; Owner: postgres
--

CREATE TABLE sgrh.tsgrhcatrespuestas (
    cod_catrespuesta integer DEFAULT nextval('sgrh.seq_catrespuestas'::regclass) NOT NULL,
    des_respuesta character varying(100) NOT NULL,
    cod_ponderacion integer NOT NULL
);


ALTER TABLE sgrh.tsgrhcatrespuestas OWNER TO postgres;

--
-- TOC entry 314 (class 1259 OID 18906)
-- Name: tsgrhclientes; Type: TABLE; Schema: sgrh; Owner: postgres
--

CREATE TABLE sgrh.tsgrhclientes (
    cod_cliente integer DEFAULT nextval('sgrh.seq_clientes'::regclass) NOT NULL,
    des_nbcliente character varying(90),
    des_direccioncte character varying(150) NOT NULL,
    des_nbcontactocte character varying(70) NOT NULL,
    des_correocte character varying(50) NOT NULL,
    cod_telefonocte character varying(16) DEFAULT NULL::character varying
);


ALTER TABLE sgrh.tsgrhclientes OWNER TO postgres;

--
-- TOC entry 315 (class 1259 OID 18911)
-- Name: tsgrhcontrataciones; Type: TABLE; Schema: sgrh; Owner: postgres
--

CREATE TABLE sgrh.tsgrhcontrataciones (
    cod_contratacion integer DEFAULT nextval('sgrh.seq_contrataciones'::regclass) NOT NULL,
    fec_inicio date NOT NULL,
    fec_termino date,
    des_esquema character varying(30) NOT NULL,
    cod_salarioestmin numeric(6,2) NOT NULL,
    cod_salarioestmax numeric(6,2),
    tim_jornada time without time zone,
    cod_creadopor integer NOT NULL,
    cod_modificadopor integer NOT NULL,
    fec_creacion date DEFAULT CURRENT_DATE NOT NULL,
    fec_modificacion date DEFAULT CURRENT_DATE NOT NULL
);


ALTER TABLE sgrh.tsgrhcontrataciones OWNER TO postgres;

--
-- TOC entry 316 (class 1259 OID 18917)
-- Name: tsgrhcontratos; Type: TABLE; Schema: sgrh; Owner: postgres
--

CREATE TABLE sgrh.tsgrhcontratos (
    cod_contrato integer DEFAULT nextval('sgrh.seq_contratos'::regclass) NOT NULL,
    des_nbconsultor character varying(45) NOT NULL,
    des_appaterno character varying(45) NOT NULL,
    des_apmaterno character varying(45) NOT NULL,
    cod_creadopor integer NOT NULL,
    cod_modificadopor integer NOT NULL,
    fec_creacion date DEFAULT CURRENT_DATE NOT NULL,
    fec_modificacion date DEFAULT CURRENT_DATE NOT NULL
);


ALTER TABLE sgrh.tsgrhcontratos OWNER TO postgres;

--
-- TOC entry 317 (class 1259 OID 18923)
-- Name: tsgrhempleados; Type: TABLE; Schema: sgrh; Owner: postgres
--

CREATE TABLE sgrh.tsgrhempleados (
    cod_empleado integer DEFAULT nextval('sgrh.seq_empleado'::regclass) NOT NULL,
    des_nombre character varying(45) NOT NULL,
    des_nombres character varying(60),
    des_apepaterno character varying(40) NOT NULL,
    des_apematerno character varying(40),
    des_direccion character varying(150) NOT NULL,
    fec_nacimiento date NOT NULL,
    des_lugarnacimiento character varying(50) NOT NULL,
    cod_edad integer NOT NULL,
    des_correo character varying(50),
    cod_tiposangre character varying(5) NOT NULL,
    cod_telefonocasa character varying(16),
    cod_telefonocelular character varying(16),
    cod_telemergencia character varying(16),
    bin_identificacion bytea,
    bin_pasaporte bytea,
    bin_visa bytea,
    cod_licenciamanejo character varying(20),
    fec_ingreso date NOT NULL,
    cod_rfc character varying(13),
    cod_nss character varying(20),
    cod_curp character varying(18) NOT NULL,
    bin_foto bytea,
    cod_tipofoto character varying(30),
    cod_extensionfoto character varying(5),
    cod_empleadoactivo boolean,
    cod_estatusempleado integer NOT NULL,
    cod_estadocivil integer NOT NULL,
    cod_rol integer,
    cod_puesto integer,
    cod_diasvacaciones integer,
    cod_sistemasuite integer,
    fec_creacion date DEFAULT CURRENT_DATE NOT NULL,
    fec_modificacion date DEFAULT CURRENT_DATE NOT NULL,
    cod_creadopor integer,
    cod_modificadopor integer,
    cod_area integer NOT NULL
);


ALTER TABLE sgrh.tsgrhempleados OWNER TO postgres;

--
-- TOC entry 318 (class 1259 OID 18932)
-- Name: tsgrhencuesta; Type: TABLE; Schema: sgrh; Owner: postgres
--

CREATE TABLE sgrh.tsgrhencuesta (
    cod_encuesta integer DEFAULT nextval('sgrh.seq_encuestas'::regclass) NOT NULL,
    des_nbencuesta character varying(50) NOT NULL,
    cod_edoencuesta character varying(20) NOT NULL,
    fec_fechaencuesta date NOT NULL,
    cod_lugar integer NOT NULL,
    tim_duracion time without time zone,
    des_elementosvalidar character varying(200),
    des_defectos character varying(200),
    des_introduccion character varying(200),
    cod_aceptado boolean,
    cod_edoeliminar boolean,
    cod_creadopor integer NOT NULL,
    cod_modificadopor integer NOT NULL,
    fec_creacion date DEFAULT CURRENT_DATE NOT NULL,
    fec_modificacion date DEFAULT CURRENT_DATE NOT NULL,
    cod_area integer NOT NULL,
    CONSTRAINT tsgrhencuesta_cod_edoencuesta_check CHECK (((cod_edoencuesta)::text = ANY (ARRAY[('En Proceso'::character varying)::text, ('Corregido'::character varying)::text, ('Aceptado'::character varying)::text])))
);


ALTER TABLE sgrh.tsgrhencuesta OWNER TO postgres;

--
-- TOC entry 319 (class 1259 OID 18942)
-- Name: tsgrhencuesta_participantes; Type: TABLE; Schema: sgrh; Owner: postgres
--

CREATE TABLE sgrh.tsgrhencuesta_participantes (
    cod_participantenc integer DEFAULT nextval('sgrh.seq_cat_encuesta_participantes'::regclass) NOT NULL,
    cod_empleado integer NOT NULL,
    cod_encuesta integer NOT NULL,
    cod_pregunta integer NOT NULL,
    cod_respuesta integer,
    respuesta_abierta text
);


ALTER TABLE sgrh.tsgrhencuesta_participantes OWNER TO postgres;

--
-- TOC entry 320 (class 1259 OID 18949)
-- Name: tsgrhescolaridad; Type: TABLE; Schema: sgrh; Owner: postgres
--

CREATE TABLE sgrh.tsgrhescolaridad (
    cod_escolaridad integer DEFAULT nextval('sgrh.seq_escolaridad'::regclass) NOT NULL,
    cod_empleado integer NOT NULL,
    des_nbinstitucion character varying(70) NOT NULL,
    des_nivelestudios character varying(30) NOT NULL,
    cod_titulo boolean NOT NULL,
    fec_inicio date NOT NULL,
    fec_termino date NOT NULL,
    bin_titulo bytea
);


ALTER TABLE sgrh.tsgrhescolaridad OWNER TO postgres;

--
-- TOC entry 321 (class 1259 OID 18956)
-- Name: tsgrhestatuscapacitacion; Type: TABLE; Schema: sgrh; Owner: postgres
--

CREATE TABLE sgrh.tsgrhestatuscapacitacion (
    cod_estatus integer DEFAULT nextval('sgrh.seq_estatus'::regclass) NOT NULL,
    des_estatus character varying(50) NOT NULL,
    aud_feccreacion date NOT NULL,
    aud_fecmodificacion date,
    aud_creadopor integer NOT NULL,
    aud_modificadopor integer
);


ALTER TABLE sgrh.tsgrhestatuscapacitacion OWNER TO postgres;

--
-- TOC entry 322 (class 1259 OID 18960)
-- Name: tsgrhevacapacitacion; Type: TABLE; Schema: sgrh; Owner: postgres
--

CREATE TABLE sgrh.tsgrhevacapacitacion (
    cod_evacapacitacion integer DEFAULT nextval('sgrh.seq_evacapacitacion'::regclass) NOT NULL,
    cod_plancapacitacion integer NOT NULL,
    cod_empleado integer,
    des_estado character varying(50) NOT NULL,
    des_evaluacion character varying(50),
    auf_feccreacion date NOT NULL,
    aud_fecmodificacion date,
    aud_creadopor integer NOT NULL,
    aud_modificadopor integer NOT NULL
);


ALTER TABLE sgrh.tsgrhevacapacitacion OWNER TO postgres;

--
-- TOC entry 323 (class 1259 OID 18964)
-- Name: tsgrhevacontestadas; Type: TABLE; Schema: sgrh; Owner: postgres
--

CREATE TABLE sgrh.tsgrhevacontestadas (
    cod_evacontestada integer DEFAULT nextval('sgrh.seq_evacontestadas'::regclass) NOT NULL,
    cod_evaluacion integer NOT NULL,
    cod_evaluador integer NOT NULL,
    cod_evaluado integer NOT NULL,
    cod_total integer NOT NULL,
    bin_reporte bytea
);


ALTER TABLE sgrh.tsgrhevacontestadas OWNER TO postgres;

--
-- TOC entry 324 (class 1259 OID 18971)
-- Name: tsgrhevaluaciones; Type: TABLE; Schema: sgrh; Owner: postgres
--

CREATE TABLE sgrh.tsgrhevaluaciones (
    cod_evaluacion integer DEFAULT nextval('sgrh.seq_evaluaciones'::regclass) NOT NULL,
    des_nbevaluacion character varying(60) NOT NULL,
    cod_creadopor integer NOT NULL,
    cod_modificadopor integer NOT NULL,
    fec_creacion date DEFAULT CURRENT_DATE NOT NULL,
    fec_modificacion date DEFAULT CURRENT_DATE NOT NULL,
    des_edoevaluacion character varying(30) DEFAULT '--'::character varying,
    cod_edoeliminar boolean,
    CONSTRAINT tsgrhevaluaciones_des_edoevaluacion_check CHECK (((des_edoevaluacion)::text = ANY (ARRAY[('--'::character varying)::text, ('En Proceso'::character varying)::text, ('Corregido'::character varying)::text, ('Aceptado'::character varying)::text])))
);


ALTER TABLE sgrh.tsgrhevaluaciones OWNER TO postgres;

--
-- TOC entry 325 (class 1259 OID 18979)
-- Name: tsgrhexperienciaslaborales; Type: TABLE; Schema: sgrh; Owner: postgres
--

CREATE TABLE sgrh.tsgrhexperienciaslaborales (
    cod_experiencia integer DEFAULT nextval('sgrh.seq_experiencialab'::regclass) NOT NULL,
    cod_empleado integer NOT NULL,
    des_nbempresa character varying(50) NOT NULL,
    des_nbpuesto character varying(50) NOT NULL,
    fec_inicio date NOT NULL,
    fec_termino date NOT NULL,
    txt_actividades text NOT NULL,
    des_ubicacion character varying(70),
    des_nbcliente character varying(70),
    des_proyecto character varying(70),
    txt_logros character varying(300)
);


ALTER TABLE sgrh.tsgrhexperienciaslaborales OWNER TO postgres;

--
-- TOC entry 326 (class 1259 OID 18986)
-- Name: tsgrhfactoreseva; Type: TABLE; Schema: sgrh; Owner: postgres
--

CREATE TABLE sgrh.tsgrhfactoreseva (
    cod_factor integer DEFAULT nextval('sgrh.seq_factoreseva'::regclass) NOT NULL,
    des_nbfactor character varying(60) NOT NULL,
    cod_edoeliminar boolean NOT NULL
);


ALTER TABLE sgrh.tsgrhfactoreseva OWNER TO postgres;

--
-- TOC entry 327 (class 1259 OID 18990)
-- Name: tsgrhidiomas; Type: TABLE; Schema: sgrh; Owner: postgres
--

CREATE TABLE sgrh.tsgrhidiomas (
    cod_idioma integer DEFAULT nextval('sgrh.seq_idiomas'::regclass) NOT NULL,
    des_nbidioma character varying(45) NOT NULL,
    por_dominiooral integer,
    por_dominioescrito integer,
    cod_empleado integer
);


ALTER TABLE sgrh.tsgrhidiomas OWNER TO postgres;

--
-- TOC entry 328 (class 1259 OID 18994)
-- Name: tsgrhlogistica; Type: TABLE; Schema: sgrh; Owner: postgres
--

CREATE TABLE sgrh.tsgrhlogistica (
    cod_logistica integer DEFAULT nextval('sgrh.seq_logistica'::regclass) NOT NULL,
    tim_horario integer NOT NULL,
    des_requerimientos character varying(200),
    fec_fecinicio date NOT NULL,
    fec_termino date NOT NULL,
    cod_capacitacion integer NOT NULL,
    aud_feccreacion date NOT NULL,
    aud_fecmodificacion date NOT NULL,
    aud_creadopor integer NOT NULL,
    aud_modificadopor integer NOT NULL
);


ALTER TABLE sgrh.tsgrhlogistica OWNER TO postgres;

--
-- TOC entry 329 (class 1259 OID 18998)
-- Name: tsgrhlugares; Type: TABLE; Schema: sgrh; Owner: postgres
--

CREATE TABLE sgrh.tsgrhlugares (
    cod_lugar integer DEFAULT nextval('sgrh.seq_lugar'::regclass) NOT NULL,
    des_lugar character varying(50) NOT NULL,
    aud_feccreacion date NOT NULL,
    aud_fecmodificacion date,
    aud_creadopor integer NOT NULL,
    aud_modificadopor integer
);


ALTER TABLE sgrh.tsgrhlugares OWNER TO postgres;

--
-- TOC entry 330 (class 1259 OID 19002)
-- Name: tsgrhmodo; Type: TABLE; Schema: sgrh; Owner: postgres
--

CREATE TABLE sgrh.tsgrhmodo (
    cod_modo integer DEFAULT nextval('sgrh.seq_modo'::regclass) NOT NULL,
    des_nbmodo character varying(50) NOT NULL,
    aud_feccreacion date NOT NULL,
    aud_fecmodificacion date,
    aud_creadopor integer NOT NULL,
    aud_modificadopor integer
);


ALTER TABLE sgrh.tsgrhmodo OWNER TO postgres;

--
-- TOC entry 331 (class 1259 OID 19006)
-- Name: tsgrhperfiles; Type: TABLE; Schema: sgrh; Owner: postgres
--

CREATE TABLE sgrh.tsgrhperfiles (
    cod_perfil integer DEFAULT nextval('sgrh.seq_perfiles'::regclass) NOT NULL,
    des_perfil character varying(100) NOT NULL
);


ALTER TABLE sgrh.tsgrhperfiles OWNER TO postgres;

--
-- TOC entry 332 (class 1259 OID 19010)
-- Name: tsgrhplancapacitacion; Type: TABLE; Schema: sgrh; Owner: postgres
--

CREATE TABLE sgrh.tsgrhplancapacitacion (
    cod_plancapacitacion integer DEFAULT nextval('sgrh.seq_plancapacitacion'::regclass) NOT NULL,
    des_nombre character varying(50) NOT NULL,
    cod_modo integer NOT NULL,
    cod_tipocapacitacion integer NOT NULL,
    des_criterios character varying(200) NOT NULL,
    cod_proceso integer NOT NULL,
    des_instructor character varying(50) NOT NULL,
    cod_proveedor integer NOT NULL,
    cod_estatus integer NOT NULL,
    des_comentarios character varying(200),
    des_evaluacion character varying(50),
    cod_lugar integer NOT NULL,
    aud_feccreacion date NOT NULL,
    aud_fecmodificacion date NOT NULL,
    aud_creadopor integer NOT NULL,
    aud_modificadopor integer NOT NULL
);


ALTER TABLE sgrh.tsgrhplancapacitacion OWNER TO postgres;

--
-- TOC entry 333 (class 1259 OID 19017)
-- Name: tsgrhplanoperativo; Type: TABLE; Schema: sgrh; Owner: postgres
--

CREATE TABLE sgrh.tsgrhplanoperativo (
    cod_planoperativo integer DEFAULT nextval('sgrh.seq_planesoperativos'::regclass) NOT NULL,
    des_nbplan character varying(100) NOT NULL,
    cod_version character varying(5) NOT NULL,
    cod_anio integer NOT NULL,
    cod_estatus character varying(20) NOT NULL,
    bin_planoperativo bytea,
    cod_creadopor integer NOT NULL,
    cod_modificadopor integer NOT NULL,
    fec_creacion date DEFAULT CURRENT_DATE NOT NULL,
    fec_modificacion date DEFAULT CURRENT_DATE NOT NULL
);


ALTER TABLE sgrh.tsgrhplanoperativo OWNER TO postgres;

--
-- TOC entry 334 (class 1259 OID 19026)
-- Name: tsgrhpreguntasenc; Type: TABLE; Schema: sgrh; Owner: postgres
--

CREATE TABLE sgrh.tsgrhpreguntasenc (
    cod_pregunta integer DEFAULT nextval('sgrh.seq_preguntasenc'::regclass) NOT NULL,
    des_pregunta character varying(200) NOT NULL,
    cod_tipopregunta boolean,
    cod_edoeliminar boolean,
    cod_encuesta integer NOT NULL
);


ALTER TABLE sgrh.tsgrhpreguntasenc OWNER TO postgres;

--
-- TOC entry 335 (class 1259 OID 19030)
-- Name: tsgrhpreguntaseva; Type: TABLE; Schema: sgrh; Owner: postgres
--

CREATE TABLE sgrh.tsgrhpreguntaseva (
    cod_pregunta integer DEFAULT nextval('sgrh.seq_preguntaseva'::regclass) NOT NULL,
    des_pregunta character varying(100) NOT NULL,
    cod_edoeliminar boolean NOT NULL,
    cod_evaluacion integer NOT NULL,
    cod_subfactor integer NOT NULL
);


ALTER TABLE sgrh.tsgrhpreguntaseva OWNER TO postgres;

--
-- TOC entry 336 (class 1259 OID 19034)
-- Name: tsgrhprocesos; Type: TABLE; Schema: sgrh; Owner: postgres
--

CREATE TABLE sgrh.tsgrhprocesos (
    cod_proceso integer DEFAULT nextval('sgrh.seq_proceso'::regclass) NOT NULL,
    des_nbproceso character varying(50) NOT NULL,
    aud_feccreacion date NOT NULL,
    aud_fecmodificacion date,
    aud_creadopor integer NOT NULL,
    aud_modificadopor integer
);


ALTER TABLE sgrh.tsgrhprocesos OWNER TO postgres;

--
-- TOC entry 337 (class 1259 OID 19038)
-- Name: tsgrhproveedores; Type: TABLE; Schema: sgrh; Owner: postgres
--

CREATE TABLE sgrh.tsgrhproveedores (
    cod_proveedor integer DEFAULT nextval('sgrh.seq_proveedor'::regclass) NOT NULL,
    des_nbproveedor character varying(50) NOT NULL,
    aud_feccreacion date NOT NULL,
    aud_fecmodificacion date,
    aud_creadopor integer NOT NULL,
    aud_modificadopor integer
);


ALTER TABLE sgrh.tsgrhproveedores OWNER TO postgres;

--
-- TOC entry 338 (class 1259 OID 19042)
-- Name: tsgrhpuestos; Type: TABLE; Schema: sgrh; Owner: postgres
--

CREATE TABLE sgrh.tsgrhpuestos (
    cod_puesto integer DEFAULT nextval('sgrh.seq_puestos'::regclass) NOT NULL,
    des_puesto character varying(100) NOT NULL,
    cod_area integer NOT NULL,
    cod_acronimo character varying(5)
);


ALTER TABLE sgrh.tsgrhpuestos OWNER TO postgres;

--
-- TOC entry 339 (class 1259 OID 19046)
-- Name: tsgrhrelacionroles; Type: TABLE; Schema: sgrh; Owner: postgres
--

CREATE TABLE sgrh.tsgrhrelacionroles (
    cod_plancapacitacion integer NOT NULL,
    cod_rolempleado integer NOT NULL,
    aud_feccreacion date NOT NULL,
    aud_fecmodificacion date,
    aud_creadopor integer NOT NULL,
    aud_modificadopor integer
);


ALTER TABLE sgrh.tsgrhrelacionroles OWNER TO postgres;

--
-- TOC entry 340 (class 1259 OID 19049)
-- Name: tsgrhrespuestasenc; Type: TABLE; Schema: sgrh; Owner: postgres
--

CREATE TABLE sgrh.tsgrhrespuestasenc (
    cod_respuesta integer DEFAULT nextval('sgrh.seq_respuestasenc'::regclass) NOT NULL,
    cod_catrespuesta integer,
    cod_pregunta integer NOT NULL,
    cod_edoeliminar boolean
);


ALTER TABLE sgrh.tsgrhrespuestasenc OWNER TO postgres;

--
-- TOC entry 341 (class 1259 OID 19053)
-- Name: tsgrhrespuestaseva; Type: TABLE; Schema: sgrh; Owner: postgres
--

CREATE TABLE sgrh.tsgrhrespuestaseva (
    cod_respuesta integer DEFAULT nextval('sgrh.seq_respuestaseva'::regclass) NOT NULL,
    des_respuesta character varying(200) NOT NULL,
    cod_pregunta integer NOT NULL,
    cod_evacontestada integer NOT NULL
);


ALTER TABLE sgrh.tsgrhrespuestaseva OWNER TO postgres;

--
-- TOC entry 342 (class 1259 OID 19057)
-- Name: tsgrhrevplanoperativo; Type: TABLE; Schema: sgrh; Owner: postgres
--

CREATE TABLE sgrh.tsgrhrevplanoperativo (
    cod_revplanoperativo integer DEFAULT nextval('sgrh.seq_revplanesoperativos'::regclass) NOT NULL,
    fec_creacion date DEFAULT CURRENT_DATE NOT NULL,
    cod_lugar integer NOT NULL,
    tim_duracion time without time zone,
    cod_participante1 integer,
    cod_participante2 integer,
    cod_participante3 integer,
    cod_participante4 integer,
    cod_participante5 integer,
    cod_planoperativo integer,
    des_puntosatratar character varying(250),
    des_acuerdosobtenidos character varying(500),
    cod_creadopor integer NOT NULL,
    cod_modificadopor integer NOT NULL,
    fec_modificacion date DEFAULT CURRENT_DATE NOT NULL
);


ALTER TABLE sgrh.tsgrhrevplanoperativo OWNER TO postgres;

--
-- TOC entry 343 (class 1259 OID 19066)
-- Name: tsgrhrolempleado; Type: TABLE; Schema: sgrh; Owner: postgres
--

CREATE TABLE sgrh.tsgrhrolempleado (
    cod_rolempleado integer DEFAULT nextval('sgrh.seq_rolempleado'::regclass) NOT NULL,
    des_nbrol character varying(50) NOT NULL,
    aud_feccreacion date NOT NULL,
    aud_fecmodificacion date,
    aud_creadopor integer NOT NULL,
    aud_modificadopor integer
);


ALTER TABLE sgrh.tsgrhrolempleado OWNER TO postgres;

--
-- TOC entry 344 (class 1259 OID 19070)
-- Name: tsgrhsubfactoreseva; Type: TABLE; Schema: sgrh; Owner: postgres
--

CREATE TABLE sgrh.tsgrhsubfactoreseva (
    cod_subfactor integer DEFAULT nextval('sgrh.seq_subfactoreseva'::regclass) NOT NULL,
    des_nbsubfactor character varying(60) NOT NULL,
    cod_factor integer NOT NULL,
    cod_edoeliminar boolean NOT NULL
);


ALTER TABLE sgrh.tsgrhsubfactoreseva OWNER TO postgres;

--
-- TOC entry 345 (class 1259 OID 19074)
-- Name: tsgrhtipocapacitacion; Type: TABLE; Schema: sgrh; Owner: postgres
--

CREATE TABLE sgrh.tsgrhtipocapacitacion (
    cod_tipocapacitacion integer DEFAULT nextval('sgrh.seq_tipocapacitacion'::regclass) NOT NULL,
    des_nbtipocapacitacion character varying(50) NOT NULL,
    aud_feccreacion date NOT NULL,
    aud_fecmodificacion date,
    aud_creadopor integer NOT NULL,
    aud_modificadopor integer
);


ALTER TABLE sgrh.tsgrhtipocapacitacion OWNER TO postgres;

--
-- TOC entry 346 (class 1259 OID 19078)
-- Name: tsgrhvalidaevaluaciondes; Type: TABLE; Schema: sgrh; Owner: postgres
--

CREATE TABLE sgrh.tsgrhvalidaevaluaciondes (
    cod_validacion integer DEFAULT nextval('sgrh.seq_validaevaluaciones'::regclass) NOT NULL,
    des_edoevaluacion character varying(30) NOT NULL,
    fec_validacion date,
    cod_lugar integer,
    tim_duracion time without time zone,
    des_defectosevalucacion character varying(1000),
    cod_edoeliminar boolean,
    cod_evaluacion integer,
    cod_participante1 integer NOT NULL,
    cod_participante2 integer,
    cod_participante3 integer,
    cod_participante4 integer,
    cod_evaluador integer,
    cod_evaluado integer,
    CONSTRAINT tsgrhvalidaevaluaciondes_des_edoevaluacion_check CHECK (((des_edoevaluacion)::text = ANY (ARRAY[('--'::character varying)::text, ('En Proceso'::character varying)::text, ('Corregido'::character varying)::text, ('Aceptado'::character varying)::text])))
);


ALTER TABLE sgrh.tsgrhvalidaevaluaciondes OWNER TO postgres;

--
-- TOC entry 211 (class 1259 OID 16565)
-- Name: seq_agenda; Type: SEQUENCE; Schema: sgrt; Owner: postgres
--

CREATE SEQUENCE sgrt.seq_agenda
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_agenda OWNER TO postgres;

--
-- TOC entry 212 (class 1259 OID 16567)
-- Name: seq_archivo; Type: SEQUENCE; Schema: sgrt; Owner: postgres
--

CREATE SEQUENCE sgrt.seq_archivo
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_archivo OWNER TO postgres;

--
-- TOC entry 213 (class 1259 OID 16569)
-- Name: seq_asistente; Type: SEQUENCE; Schema: sgrt; Owner: postgres
--

CREATE SEQUENCE sgrt.seq_asistente
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_asistente OWNER TO postgres;

--
-- TOC entry 214 (class 1259 OID 16571)
-- Name: seq_attach; Type: SEQUENCE; Schema: sgrt; Owner: postgres
--

CREATE SEQUENCE sgrt.seq_attach
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_attach OWNER TO postgres;

--
-- TOC entry 215 (class 1259 OID 16573)
-- Name: seq_categoriafaq; Type: SEQUENCE; Schema: sgrt; Owner: postgres
--

CREATE SEQUENCE sgrt.seq_categoriafaq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_categoriafaq OWNER TO postgres;

--
-- TOC entry 216 (class 1259 OID 16575)
-- Name: seq_chat; Type: SEQUENCE; Schema: sgrt; Owner: postgres
--

CREATE SEQUENCE sgrt.seq_chat
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 99999999999999
    CACHE 1;


ALTER TABLE sgrt.seq_chat OWNER TO postgres;

--
-- TOC entry 217 (class 1259 OID 16577)
-- Name: seq_ciudad; Type: SEQUENCE; Schema: sgrt; Owner: postgres
--

CREATE SEQUENCE sgrt.seq_ciudad
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_ciudad OWNER TO postgres;

--
-- TOC entry 218 (class 1259 OID 16579)
-- Name: seq_comentsagenda; Type: SEQUENCE; Schema: sgrt; Owner: postgres
--

CREATE SEQUENCE sgrt.seq_comentsagenda
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_comentsagenda OWNER TO postgres;

--
-- TOC entry 219 (class 1259 OID 16581)
-- Name: seq_comentsreunion; Type: SEQUENCE; Schema: sgrt; Owner: postgres
--

CREATE SEQUENCE sgrt.seq_comentsreunion
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_comentsreunion OWNER TO postgres;

--
-- TOC entry 220 (class 1259 OID 16583)
-- Name: seq_compromiso; Type: SEQUENCE; Schema: sgrt; Owner: postgres
--

CREATE SEQUENCE sgrt.seq_compromiso
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_compromiso OWNER TO postgres;

--
-- TOC entry 221 (class 1259 OID 16585)
-- Name: seq_contacto; Type: SEQUENCE; Schema: sgrt; Owner: postgres
--

CREATE SEQUENCE sgrt.seq_contacto
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_contacto OWNER TO postgres;

--
-- TOC entry 222 (class 1259 OID 16587)
-- Name: seq_correo; Type: SEQUENCE; Schema: sgrt; Owner: postgres
--

CREATE SEQUENCE sgrt.seq_correo
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_correo OWNER TO postgres;

--
-- TOC entry 223 (class 1259 OID 16589)
-- Name: seq_depto; Type: SEQUENCE; Schema: sgrt; Owner: postgres
--

CREATE SEQUENCE sgrt.seq_depto
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_depto OWNER TO postgres;

--
-- TOC entry 224 (class 1259 OID 16591)
-- Name: seq_edoacuerdo; Type: SEQUENCE; Schema: sgrt; Owner: postgres
--

CREATE SEQUENCE sgrt.seq_edoacuerdo
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_edoacuerdo OWNER TO postgres;

--
-- TOC entry 225 (class 1259 OID 16593)
-- Name: seq_elemento; Type: SEQUENCE; Schema: sgrt; Owner: postgres
--

CREATE SEQUENCE sgrt.seq_elemento
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_elemento OWNER TO postgres;

--
-- TOC entry 226 (class 1259 OID 16595)
-- Name: seq_estadorep; Type: SEQUENCE; Schema: sgrt; Owner: postgres
--

CREATE SEQUENCE sgrt.seq_estadorep
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_estadorep OWNER TO postgres;

--
-- TOC entry 227 (class 1259 OID 16597)
-- Name: seq_faq; Type: SEQUENCE; Schema: sgrt; Owner: postgres
--

CREATE SEQUENCE sgrt.seq_faq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_faq OWNER TO postgres;

--
-- TOC entry 228 (class 1259 OID 16599)
-- Name: seq_grupo; Type: SEQUENCE; Schema: sgrt; Owner: postgres
--

CREATE SEQUENCE sgrt.seq_grupo
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_grupo OWNER TO postgres;

--
-- TOC entry 229 (class 1259 OID 16601)
-- Name: seq_invitado; Type: SEQUENCE; Schema: sgrt; Owner: postgres
--

CREATE SEQUENCE sgrt.seq_invitado
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_invitado OWNER TO postgres;

--
-- TOC entry 230 (class 1259 OID 16603)
-- Name: seq_lugar; Type: SEQUENCE; Schema: sgrt; Owner: postgres
--

CREATE SEQUENCE sgrt.seq_lugar
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_lugar OWNER TO postgres;

--
-- TOC entry 231 (class 1259 OID 16605)
-- Name: seq_mensaje; Type: SEQUENCE; Schema: sgrt; Owner: postgres
--

CREATE SEQUENCE sgrt.seq_mensaje
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_mensaje OWNER TO postgres;

--
-- TOC entry 232 (class 1259 OID 16607)
-- Name: seq_nota; Type: SEQUENCE; Schema: sgrt; Owner: postgres
--

CREATE SEQUENCE sgrt.seq_nota
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_nota OWNER TO postgres;

--
-- TOC entry 233 (class 1259 OID 16609)
-- Name: seq_plantillacorreo; Type: SEQUENCE; Schema: sgrt; Owner: postgres
--

CREATE SEQUENCE sgrt.seq_plantillacorreo
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_plantillacorreo OWNER TO postgres;

--
-- TOC entry 234 (class 1259 OID 16611)
-- Name: seq_prioridad; Type: SEQUENCE; Schema: sgrt; Owner: postgres
--

CREATE SEQUENCE sgrt.seq_prioridad
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_prioridad OWNER TO postgres;

--
-- TOC entry 235 (class 1259 OID 16613)
-- Name: seq_resp; Type: SEQUENCE; Schema: sgrt; Owner: postgres
--

CREATE SEQUENCE sgrt.seq_resp
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_resp OWNER TO postgres;

--
-- TOC entry 236 (class 1259 OID 16615)
-- Name: seq_respuesta; Type: SEQUENCE; Schema: sgrt; Owner: postgres
--

CREATE SEQUENCE sgrt.seq_respuesta
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_respuesta OWNER TO postgres;

--
-- TOC entry 347 (class 1259 OID 19086)
-- Name: seq_respuestas_participantes; Type: SEQUENCE; Schema: sgrt; Owner: postgres
--

CREATE SEQUENCE sgrt.seq_respuestas_participantes
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_respuestas_participantes OWNER TO postgres;

--
-- TOC entry 237 (class 1259 OID 16617)
-- Name: seq_reunion; Type: SEQUENCE; Schema: sgrt; Owner: postgres
--

CREATE SEQUENCE sgrt.seq_reunion
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_reunion OWNER TO postgres;

--
-- TOC entry 238 (class 1259 OID 16619)
-- Name: seq_servicio; Type: SEQUENCE; Schema: sgrt; Owner: postgres
--

CREATE SEQUENCE sgrt.seq_servicio
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_servicio OWNER TO postgres;

--
-- TOC entry 239 (class 1259 OID 16621)
-- Name: seq_solicitud; Type: SEQUENCE; Schema: sgrt; Owner: postgres
--

CREATE SEQUENCE sgrt.seq_solicitud
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_solicitud OWNER TO postgres;

--
-- TOC entry 240 (class 1259 OID 16623)
-- Name: seq_ticket; Type: SEQUENCE; Schema: sgrt; Owner: postgres
--

CREATE SEQUENCE sgrt.seq_ticket
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_ticket OWNER TO postgres;

--
-- TOC entry 241 (class 1259 OID 16625)
-- Name: seq_topico; Type: SEQUENCE; Schema: sgrt; Owner: postgres
--

CREATE SEQUENCE sgrt.seq_topico
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_topico OWNER TO postgres;

--
-- TOC entry 242 (class 1259 OID 16627)
-- Name: tsgrtagenda; Type: TABLE; Schema: sgrt; Owner: postgres
--

CREATE TABLE sgrt.tsgrtagenda (
    cod_agenda integer DEFAULT nextval('sgrt.seq_agenda'::regclass) NOT NULL,
    des_texto character varying(200) NOT NULL,
    cnu_tratado smallint DEFAULT '0'::smallint,
    cod_reunion integer NOT NULL
);


ALTER TABLE sgrt.tsgrtagenda OWNER TO postgres;

--
-- TOC entry 243 (class 1259 OID 16632)
-- Name: tsgrtarchivos; Type: TABLE; Schema: sgrt; Owner: postgres
--

CREATE TABLE sgrt.tsgrtarchivos (
    cod_archivo integer DEFAULT nextval('sgrt.seq_archivo'::regclass) NOT NULL,
    bin_archivo bytea,
    cod_reunion integer NOT NULL
);


ALTER TABLE sgrt.tsgrtarchivos OWNER TO postgres;

--
-- TOC entry 244 (class 1259 OID 16639)
-- Name: tsgrtasistentes; Type: TABLE; Schema: sgrt; Owner: postgres
--

CREATE TABLE sgrt.tsgrtasistentes (
    cod_asistente integer DEFAULT nextval('sgrt.seq_asistente'::regclass) NOT NULL,
    cod_reunion integer NOT NULL,
    cod_empleado integer,
    cnu_asiste smallint NOT NULL,
    cod_invitado integer
);


ALTER TABLE sgrt.tsgrtasistentes OWNER TO postgres;

--
-- TOC entry 245 (class 1259 OID 16643)
-- Name: tsgrtattchticket; Type: TABLE; Schema: sgrt; Owner: postgres
--

CREATE TABLE sgrt.tsgrtattchticket (
    cod_attach integer DEFAULT nextval('sgrt.seq_attach'::regclass) NOT NULL,
    cod_ticket integer NOT NULL,
    cod_tamano character varying(20) NOT NULL,
    des_nombre character varying(128) NOT NULL,
    cod_sistemasuite integer NOT NULL,
    fec_ultactualizacion timestamp without time zone,
    cod_ultactualizacionpor integer NOT NULL,
    cod_creadopor integer NOT NULL,
    fec_creacion timestamp without time zone,
    bin_attach bytea
);


ALTER TABLE sgrt.tsgrtattchticket OWNER TO postgres;

--
-- TOC entry 246 (class 1259 OID 16650)
-- Name: tsgrtayudatopico; Type: TABLE; Schema: sgrt; Owner: postgres
--

CREATE TABLE sgrt.tsgrtayudatopico (
    cod_topico integer DEFAULT nextval('sgrt.seq_topico'::regclass) NOT NULL,
    cnu_activo smallint NOT NULL,
    cnu_autorespuesta smallint NOT NULL,
    cod_prioridad smallint NOT NULL,
    cod_depto integer NOT NULL,
    des_topico character varying(36) NOT NULL,
    cod_sistemasuite integer NOT NULL,
    fec_ultactualizacion timestamp without time zone,
    cod_ultactualizacionpor integer,
    cod_creadopor integer NOT NULL,
    fec_creacion timestamp without time zone
);


ALTER TABLE sgrt.tsgrtayudatopico OWNER TO postgres;

--
-- TOC entry 247 (class 1259 OID 16654)
-- Name: tsgrtcategoriafaq; Type: TABLE; Schema: sgrt; Owner: postgres
--

CREATE TABLE sgrt.tsgrtcategoriafaq (
    cod_categoriafaq integer DEFAULT nextval('sgrt.seq_categoriafaq'::regclass) NOT NULL,
    cnu_tipo smallint NOT NULL,
    des_categoria character varying(255) NOT NULL,
    des_descripcion character varying(255) NOT NULL,
    des_notas character varying(255) NOT NULL,
    tim_ultactualiza timestamp without time zone NOT NULL,
    fec_creacion timestamp without time zone NOT NULL,
    fec_ultactualizadopor integer NOT NULL,
    cod_creadopor integer NOT NULL
);


ALTER TABLE sgrt.tsgrtcategoriafaq OWNER TO postgres;

--
-- TOC entry 248 (class 1259 OID 16661)
-- Name: tsgrtchat; Type: TABLE; Schema: sgrt; Owner: postgres
--

CREATE TABLE sgrt.tsgrtchat (
    cod_chat integer DEFAULT nextval('sgrt.seq_chat'::regclass) NOT NULL,
    chat character varying NOT NULL
);


ALTER TABLE sgrt.tsgrtchat OWNER TO postgres;

--
-- TOC entry 249 (class 1259 OID 16668)
-- Name: tsgrtciudades; Type: TABLE; Schema: sgrt; Owner: postgres
--

CREATE TABLE sgrt.tsgrtciudades (
    cod_ciudad integer DEFAULT nextval('sgrt.seq_ciudad'::regclass) NOT NULL,
    des_nbciudad character varying(100) NOT NULL,
    cod_estadorep integer NOT NULL
);


ALTER TABLE sgrt.tsgrtciudades OWNER TO postgres;

--
-- TOC entry 250 (class 1259 OID 16672)
-- Name: tsgrtcomentariosagenda; Type: TABLE; Schema: sgrt; Owner: postgres
--

CREATE TABLE sgrt.tsgrtcomentariosagenda (
    cod_comentsagenda integer DEFAULT nextval('sgrt.seq_comentsagenda'::regclass) NOT NULL,
    des_comentario character varying(500) NOT NULL,
    cod_agenda integer NOT NULL,
    cod_invitado integer NOT NULL
);


ALTER TABLE sgrt.tsgrtcomentariosagenda OWNER TO postgres;

--
-- TOC entry 251 (class 1259 OID 16679)
-- Name: tsgrtcomentariosreunion; Type: TABLE; Schema: sgrt; Owner: postgres
--

CREATE TABLE sgrt.tsgrtcomentariosreunion (
    cod_commentsreunion integer DEFAULT nextval('sgrt.seq_comentsreunion'::regclass) NOT NULL,
    des_comentario character varying(500) NOT NULL,
    cod_invitado integer NOT NULL,
    cod_reunion integer NOT NULL
);


ALTER TABLE sgrt.tsgrtcomentariosreunion OWNER TO postgres;

--
-- TOC entry 252 (class 1259 OID 16686)
-- Name: tsgrtcompromisos; Type: TABLE; Schema: sgrt; Owner: postgres
--

CREATE TABLE sgrt.tsgrtcompromisos (
    cod_compromiso integer DEFAULT nextval('sgrt.seq_compromiso'::regclass) NOT NULL,
    des_descripcion character varying(200) NOT NULL,
    fec_solicitud date NOT NULL,
    fec_compromiso date NOT NULL,
    cod_reunion integer NOT NULL,
    cod_validador integer NOT NULL,
    cod_verificador integer NOT NULL,
    cod_estado integer,
    des_valor character varying(45),
    cod_ejecutor integer NOT NULL,
    cod_tipoejecutor character varying(10),
    cnu_revisado smallint,
    cod_estatus sgrt.estatus_compromiso DEFAULT 'Pendiente'::sgrt.estatus_compromiso NOT NULL,
    cod_tipocompromiso sgrt.tipo_compromiso DEFAULT 'Pendiente'::sgrt.tipo_compromiso NOT NULL,
    cod_chat integer,
    fec_entrega date
);


ALTER TABLE sgrt.tsgrtcompromisos OWNER TO postgres;

--
-- TOC entry 253 (class 1259 OID 16692)
-- Name: tsgrtcorreo; Type: TABLE; Schema: sgrt; Owner: postgres
--

CREATE TABLE sgrt.tsgrtcorreo (
    cod_correo integer DEFAULT nextval('sgrt.seq_correo'::regclass) NOT NULL,
    cnu_autorespuesta smallint NOT NULL,
    cod_prioridad smallint NOT NULL,
    cod_depto integer,
    des_nbusuario character varying(32) NOT NULL,
    des_correo character varying(50) NOT NULL,
    des_nombre character varying(70) NOT NULL,
    des_contrasena character varying(30) NOT NULL,
    cnu_activo smallint NOT NULL,
    des_dirhost character varying(125) NOT NULL,
    cod_protocolo sgrt.protocolo DEFAULT 'POP'::sgrt.protocolo NOT NULL,
    cod_encriptacion sgrt.encriptacion DEFAULT 'NONE'::sgrt.encriptacion NOT NULL,
    cod_puerto integer,
    cnu_frecsinc smallint NOT NULL,
    cnu_nummaxcorreo smallint NOT NULL,
    cnu_eliminar smallint NOT NULL,
    cnu_errores smallint NOT NULL,
    fec_ulterror timestamp without time zone,
    fec_ultsincr timestamp without time zone,
    cnu_smtpactivo smallint,
    des_smtphost character varying(125) NOT NULL,
    cod_smtpport integer,
    cnu_smtpsecure smallint NOT NULL,
    cnu_smtpauth smallint NOT NULL,
    cod_sistemasuite integer NOT NULL,
    fec_ultactualizacion timestamp without time zone,
    cod_ultactualizacionpor integer NOT NULL,
    cod_creadopor integer NOT NULL,
    fec_creacion timestamp without time zone,
    cod_usuario integer NOT NULL
);


ALTER TABLE sgrt.tsgrtcorreo OWNER TO postgres;

--
-- TOC entry 254 (class 1259 OID 16698)
-- Name: tsgrtdatossolicitud; Type: TABLE; Schema: sgrt; Owner: postgres
--

CREATE TABLE sgrt.tsgrtdatossolicitud (
    cod_datosolicitud integer NOT NULL,
    cod_elemento integer NOT NULL,
    des_descripcion character varying(45) NOT NULL,
    cod_solicitud integer NOT NULL,
    cod_edosolicitud integer NOT NULL
);


ALTER TABLE sgrt.tsgrtdatossolicitud OWNER TO postgres;

--
-- TOC entry 255 (class 1259 OID 16701)
-- Name: tsgrtdepartamento; Type: TABLE; Schema: sgrt; Owner: postgres
--

CREATE TABLE sgrt.tsgrtdepartamento (
    cod_depto integer DEFAULT nextval('sgrt.seq_depto'::regclass) NOT NULL,
    cod_plantillacorreo integer NOT NULL,
    cod_correo integer NOT NULL,
    cod_manager integer DEFAULT 0 NOT NULL,
    des_nombre character varying(50) NOT NULL,
    cod_ncorto character varying(10) NOT NULL,
    des_firma text NOT NULL,
    cnu_publico smallint NOT NULL,
    cod_sistemasuite integer DEFAULT 1 NOT NULL,
    fec_ultactualizacion timestamp without time zone,
    cod_ultactualizacionpor integer NOT NULL,
    cod_creadopor integer NOT NULL,
    fec_creacion timestamp without time zone
);


ALTER TABLE sgrt.tsgrtdepartamento OWNER TO postgres;

--
-- TOC entry 256 (class 1259 OID 16710)
-- Name: tsgrtedosolicitudes; Type: TABLE; Schema: sgrt; Owner: postgres
--

CREATE TABLE sgrt.tsgrtedosolicitudes (
    cod_edosolicitud integer NOT NULL,
    cod_nbedosolicitud integer NOT NULL
);


ALTER TABLE sgrt.tsgrtedosolicitudes OWNER TO postgres;

--
-- TOC entry 257 (class 1259 OID 16713)
-- Name: tsgrtelementos; Type: TABLE; Schema: sgrt; Owner: postgres
--

CREATE TABLE sgrt.tsgrtelementos (
    cod_elemento integer DEFAULT nextval('sgrt.seq_elemento'::regclass) NOT NULL,
    des_nombre character varying(50) NOT NULL,
    cnu_activo smallint NOT NULL
);


ALTER TABLE sgrt.tsgrtelementos OWNER TO postgres;

--
-- TOC entry 258 (class 1259 OID 16717)
-- Name: tsgrtestados; Type: TABLE; Schema: sgrt; Owner: postgres
--

CREATE TABLE sgrt.tsgrtestados (
    cod_estadorep integer DEFAULT nextval('sgrt.seq_estadorep'::regclass) NOT NULL,
    des_nbestado character varying(60) NOT NULL
);


ALTER TABLE sgrt.tsgrtestados OWNER TO postgres;

--
-- TOC entry 259 (class 1259 OID 16721)
-- Name: tsgrtfaq; Type: TABLE; Schema: sgrt; Owner: postgres
--

CREATE TABLE sgrt.tsgrtfaq (
    cod_faq integer DEFAULT nextval('sgrt.seq_faq'::regclass) NOT NULL,
    cod_categoriafaq integer NOT NULL,
    des_pregunta character varying(255) NOT NULL,
    cnu_activo smallint NOT NULL,
    des_respuesta character varying(255) NOT NULL,
    des_notasint character varying(255) NOT NULL,
    fec_ultactualizacion timestamp without time zone NOT NULL,
    fec_creacion timestamp without time zone NOT NULL,
    cod_creadopor integer NOT NULL,
    cod_ultactualizacionpor integer NOT NULL
);


ALTER TABLE sgrt.tsgrtfaq OWNER TO postgres;

--
-- TOC entry 260 (class 1259 OID 16728)
-- Name: tsgrtgrupo; Type: TABLE; Schema: sgrt; Owner: postgres
--

CREATE TABLE sgrt.tsgrtgrupo (
    cod_grupo integer DEFAULT nextval('sgrt.seq_grupo'::regclass) NOT NULL,
    cnu_activo smallint NOT NULL,
    des_nombre character varying(50) NOT NULL,
    cnu_crear smallint NOT NULL,
    cnu_editar smallint NOT NULL,
    cnu_borrar smallint NOT NULL,
    cnu_cerrar smallint NOT NULL,
    cnu_transferir smallint NOT NULL,
    cnu_prohibir smallint NOT NULL,
    cnu_administrar smallint NOT NULL,
    cod_sistemasuite integer DEFAULT 1 NOT NULL,
    fec_ultactualizacion timestamp without time zone,
    cod_ultactualizacionpor integer NOT NULL,
    cod_creadopor integer NOT NULL,
    fec_creacion timestamp without time zone
);


ALTER TABLE sgrt.tsgrtgrupo OWNER TO postgres;

--
-- TOC entry 261 (class 1259 OID 16733)
-- Name: tsgrtinvitados; Type: TABLE; Schema: sgrt; Owner: postgres
--

CREATE TABLE sgrt.tsgrtinvitados (
    cod_invitado integer DEFAULT nextval('sgrt.seq_invitado'::regclass) NOT NULL,
    cod_reunion integer NOT NULL,
    des_nombre character varying,
    des_correo character varying,
    cnu_invitacionenv smallint NOT NULL,
    cnu_asiste smallint NOT NULL,
    cod_empleado integer,
    des_empresa character varying
);


ALTER TABLE sgrt.tsgrtinvitados OWNER TO postgres;

--
-- TOC entry 262 (class 1259 OID 16740)
-- Name: tsgrtlugares; Type: TABLE; Schema: sgrt; Owner: postgres
--

CREATE TABLE sgrt.tsgrtlugares (
    cod_lugar integer DEFAULT nextval('sgrt.seq_lugar'::regclass) NOT NULL,
    des_nombre character varying(60) NOT NULL,
    cod_ciudad integer NOT NULL
);


ALTER TABLE sgrt.tsgrtlugares OWNER TO postgres;

--
-- TOC entry 263 (class 1259 OID 16744)
-- Name: tsgrtmsjticket; Type: TABLE; Schema: sgrt; Owner: postgres
--

CREATE TABLE sgrt.tsgrtmsjticket (
    cod_mensaje integer DEFAULT nextval('sgrt.seq_mensaje'::regclass) NOT NULL,
    cod_ticket integer NOT NULL,
    cod_usuario integer DEFAULT 0 NOT NULL,
    des_mensaje text NOT NULL,
    cod_fuente character varying(16) DEFAULT NULL::character varying,
    cod_sistemasuite integer DEFAULT 1 NOT NULL,
    fec_ultactualizacion timestamp without time zone,
    cod_ultactualizacionpor integer NOT NULL,
    cod_creadopor integer NOT NULL,
    fec_creacion timestamp without time zone
);


ALTER TABLE sgrt.tsgrtmsjticket OWNER TO postgres;

--
-- TOC entry 264 (class 1259 OID 16754)
-- Name: tsgrtnota; Type: TABLE; Schema: sgrt; Owner: postgres
--

CREATE TABLE sgrt.tsgrtnota (
    cod_nota integer DEFAULT nextval('sgrt.seq_nota'::regclass) NOT NULL,
    cod_ticket integer NOT NULL,
    cod_empleado integer NOT NULL,
    des_fuente character varying(32) NOT NULL,
    des_titulo character varying(255) DEFAULT 'Nota INTEGERerna Generica'::character varying NOT NULL,
    des_nota text NOT NULL,
    cod_sistemasuite integer NOT NULL,
    fec_ultactualizacion timestamp without time zone,
    cod_ultactualizacionpor integer NOT NULL,
    cod_creadopor integer NOT NULL,
    fec_creacion timestamp without time zone
);


ALTER TABLE sgrt.tsgrtnota OWNER TO postgres;

--
-- TOC entry 265 (class 1259 OID 16762)
-- Name: tsgrtplantillacorreos; Type: TABLE; Schema: sgrt; Owner: postgres
--

CREATE TABLE sgrt.tsgrtplantillacorreos (
    cod_plantillacorreo integer DEFAULT nextval('sgrt.seq_plantillacorreo'::regclass) NOT NULL,
    des_nombre character varying(32) NOT NULL,
    des_notas text,
    cod_tipodestinario sgrt.destinatario DEFAULT 'USR'::sgrt.destinatario,
    cod_sistemasuite integer NOT NULL,
    fec_ultactualizacion timestamp without time zone,
    cod_ultactualizacionpor integer NOT NULL,
    cod_creadopor integer NOT NULL,
    fec_creacion timestamp without time zone,
    des_asunto character varying(45),
    des_cuerpo character varying(255)
);


ALTER TABLE sgrt.tsgrtplantillacorreos OWNER TO postgres;

--
-- TOC entry 266 (class 1259 OID 16770)
-- Name: tsgrtprioridad; Type: TABLE; Schema: sgrt; Owner: postgres
--

CREATE TABLE sgrt.tsgrtprioridad (
    cod_prioridad integer DEFAULT nextval('sgrt.seq_prioridad'::regclass) NOT NULL,
    des_nombre character varying(60) NOT NULL,
    des_descripcion character varying(30) NOT NULL,
    cod_color character varying(7) NOT NULL,
    cnu_valprioridad smallint NOT NULL,
    cnu_publica smallint NOT NULL,
    cod_sistemasuite integer NOT NULL,
    fec_ultactualizacion timestamp without time zone,
    cod_ultactualizacionpor integer NOT NULL,
    cod_creadopor integer NOT NULL,
    fec_creacion timestamp without time zone
);


ALTER TABLE sgrt.tsgrtprioridad OWNER TO postgres;

--
-- TOC entry 267 (class 1259 OID 16774)
-- Name: tsgrtresppredefinida; Type: TABLE; Schema: sgrt; Owner: postgres
--

CREATE TABLE sgrt.tsgrtresppredefinida (
    cod_respuesta integer DEFAULT nextval('sgrt.seq_resp'::regclass) NOT NULL,
    cod_depto integer NOT NULL,
    cnu_activo smallint NOT NULL,
    des_titulo character varying(125) NOT NULL,
    des_respuesta text NOT NULL,
    cod_sistemasuite integer NOT NULL,
    fec_ultactualizacion timestamp without time zone,
    cod_ultactualizacionpor integer NOT NULL,
    cod_creadopor integer NOT NULL,
    fec_creacion timestamp without time zone
);


ALTER TABLE sgrt.tsgrtresppredefinida OWNER TO postgres;

--
-- TOC entry 268 (class 1259 OID 16781)
-- Name: tsgrtrespuesta; Type: TABLE; Schema: sgrt; Owner: postgres
--

CREATE TABLE sgrt.tsgrtrespuesta (
    cod_respuesta integer DEFAULT nextval('sgrt.seq_respuesta'::regclass) NOT NULL,
    cod_mensaje integer NOT NULL,
    cod_ticket integer NOT NULL,
    cod_empleado integer NOT NULL,
    des_respuesta text NOT NULL,
    cod_sistemasuite integer NOT NULL,
    fec_ultactualizacion timestamp without time zone,
    cod_ultactualizacionpor integer NOT NULL,
    cod_creadopor integer NOT NULL,
    fec_creacion timestamp without time zone
);


ALTER TABLE sgrt.tsgrtrespuesta OWNER TO postgres;

--
-- TOC entry 269 (class 1259 OID 16788)
-- Name: tsgrtreuniones; Type: TABLE; Schema: sgrt; Owner: postgres
--

CREATE TABLE sgrt.tsgrtreuniones (
    cod_reunion integer DEFAULT nextval('sgrt.seq_reunion'::regclass) NOT NULL,
    des_nombre character varying(45) NOT NULL,
    fec_fecha date NOT NULL,
    des_objetivo character varying(700) NOT NULL,
    cod_lugar integer NOT NULL,
    cod_responsable integer NOT NULL,
    cod_proximareunion integer,
    cod_creadorreunion integer NOT NULL,
    tim_duracion time without time zone,
    tim_hora time without time zone
);


ALTER TABLE sgrt.tsgrtreuniones OWNER TO postgres;

--
-- TOC entry 270 (class 1259 OID 16795)
-- Name: tsgrtservicios; Type: TABLE; Schema: sgrt; Owner: postgres
--

CREATE TABLE sgrt.tsgrtservicios (
    cod_servicio integer DEFAULT nextval('sgrt.seq_servicio'::regclass) NOT NULL,
    des_nombre_servicio character varying(45) NOT NULL,
    des_descripcion character varying(100) NOT NULL,
    fec_contratacion date NOT NULL
);


ALTER TABLE sgrt.tsgrtservicios OWNER TO postgres;

--
-- TOC entry 271 (class 1259 OID 16799)
-- Name: tsgrtsolicitudservicios; Type: TABLE; Schema: sgrt; Owner: postgres
--

CREATE TABLE sgrt.tsgrtsolicitudservicios (
    cod_solicitud integer DEFAULT nextval('sgrt.seq_solicitud'::regclass) NOT NULL,
    cod_empleado integer NOT NULL,
    cod_ticket integer NOT NULL,
    cod_servicio integer NOT NULL
);


ALTER TABLE sgrt.tsgrtsolicitudservicios OWNER TO postgres;

--
-- TOC entry 272 (class 1259 OID 16803)
-- Name: tsgrtticket; Type: TABLE; Schema: sgrt; Owner: postgres
--

CREATE TABLE sgrt.tsgrtticket (
    cod_ticket integer DEFAULT nextval('sgrt.seq_ticket'::regclass) NOT NULL,
    des_folio character varying(45) NOT NULL,
    cod_reunion integer,
    cod_acuerdo integer,
    cod_responsable integer,
    cod_validador integer,
    cod_depto integer NOT NULL,
    cod_prioridad smallint NOT NULL,
    cod_topico integer NOT NULL,
    cod_empleado integer NOT NULL,
    des_correo character varying(50) NOT NULL,
    des_nombre character varying(50) NOT NULL,
    des_tema character varying(64) DEFAULT '[Sin Asunto]'::character varying NOT NULL,
    des_temaayuda character varying(255) DEFAULT NULL::character varying,
    cod_telefono character varying(16) DEFAULT NULL::character varying,
    cod_extension character varying(8) DEFAULT NULL::character varying,
    cod_estadot sgrt.edoticket DEFAULT 'Abierto'::sgrt.edoticket NOT NULL,
    cod_origent sgrt.origencontac DEFAULT 'Otro'::sgrt.origencontac NOT NULL,
    cnu_expirado smallint NOT NULL,
    cnu_atendido smallint NOT NULL,
    fec_exp timestamp without time zone,
    fec_reap timestamp without time zone,
    fec_cierre timestamp without time zone,
    fec_ultimomsg timestamp without time zone,
    fec_ultimaresp timestamp without time zone,
    cod_sistemasuite integer DEFAULT 1 NOT NULL,
    fec_ultactualizacion timestamp without time zone,
    cod_ultactualizacionpor integer NOT NULL,
    cod_creadopor integer NOT NULL,
    fec_creacion timestamp without time zone,
    cod_ejecutor integer
);


ALTER TABLE sgrt.tsgrtticket OWNER TO postgres;

--
-- TOC entry 374 (class 1259 OID 21957)
-- Name: seq_aceptaciones; Type: SEQUENCE; Schema: sisat; Owner: suite
--

CREATE SEQUENCE sisat.seq_aceptaciones
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sisat.seq_aceptaciones OWNER TO suite;

--
-- TOC entry 375 (class 1259 OID 21959)
-- Name: seq_asignaciones; Type: SEQUENCE; Schema: sisat; Owner: suite
--

CREATE SEQUENCE sisat.seq_asignaciones
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sisat.seq_asignaciones OWNER TO suite;

--
-- TOC entry 376 (class 1259 OID 21961)
-- Name: seq_candidatos; Type: SEQUENCE; Schema: sisat; Owner: suite
--

CREATE SEQUENCE sisat.seq_candidatos
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sisat.seq_candidatos OWNER TO suite;

--
-- TOC entry 377 (class 1259 OID 21963)
-- Name: seq_cartaasignaciones; Type: SEQUENCE; Schema: sisat; Owner: suite
--

CREATE SEQUENCE sisat.seq_cartaasignaciones
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sisat.seq_cartaasignaciones OWNER TO suite;

--
-- TOC entry 378 (class 1259 OID 21965)
-- Name: seq_cotizaciones; Type: SEQUENCE; Schema: sisat; Owner: suite
--

CREATE SEQUENCE sisat.seq_cotizaciones
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sisat.seq_cotizaciones OWNER TO suite;

--
-- TOC entry 379 (class 1259 OID 21967)
-- Name: seq_cursos; Type: SEQUENCE; Schema: sisat; Owner: suite
--

CREATE SEQUENCE sisat.seq_cursos
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sisat.seq_cursos OWNER TO suite;

--
-- TOC entry 380 (class 1259 OID 21969)
-- Name: seq_entrevistas; Type: SEQUENCE; Schema: sisat; Owner: suite
--

CREATE SEQUENCE sisat.seq_entrevistas
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sisat.seq_entrevistas OWNER TO suite;

--
-- TOC entry 381 (class 1259 OID 21971)
-- Name: seq_envios; Type: SEQUENCE; Schema: sisat; Owner: suite
--

CREATE SEQUENCE sisat.seq_envios
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sisat.seq_envios OWNER TO suite;

--
-- TOC entry 382 (class 1259 OID 21973)
-- Name: seq_escolaridad; Type: SEQUENCE; Schema: sisat; Owner: suite
--

CREATE SEQUENCE sisat.seq_escolaridad
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sisat.seq_escolaridad OWNER TO suite;

--
-- TOC entry 383 (class 1259 OID 21975)
-- Name: seq_experiencias; Type: SEQUENCE; Schema: sisat; Owner: suite
--

CREATE SEQUENCE sisat.seq_experiencias
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sisat.seq_experiencias OWNER TO suite;

--
-- TOC entry 384 (class 1259 OID 21977)
-- Name: seq_firmas; Type: SEQUENCE; Schema: sisat; Owner: suite
--

CREATE SEQUENCE sisat.seq_firmas
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sisat.seq_firmas OWNER TO suite;

--
-- TOC entry 385 (class 1259 OID 21979)
-- Name: seq_habilidades; Type: SEQUENCE; Schema: sisat; Owner: suite
--

CREATE SEQUENCE sisat.seq_habilidades
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sisat.seq_habilidades OWNER TO suite;

--
-- TOC entry 386 (class 1259 OID 21981)
-- Name: seq_idiomas; Type: SEQUENCE; Schema: sisat; Owner: suite
--

CREATE SEQUENCE sisat.seq_idiomas
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sisat.seq_idiomas OWNER TO suite;

--
-- TOC entry 387 (class 1259 OID 21983)
-- Name: seq_ordenservicios; Type: SEQUENCE; Schema: sisat; Owner: suite
--

CREATE SEQUENCE sisat.seq_ordenservicios
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sisat.seq_ordenservicios OWNER TO suite;

--
-- TOC entry 388 (class 1259 OID 21985)
-- Name: seq_prospectos; Type: SEQUENCE; Schema: sisat; Owner: suite
--

CREATE SEQUENCE sisat.seq_prospectos
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sisat.seq_prospectos OWNER TO suite;

--
-- TOC entry 389 (class 1259 OID 21987)
-- Name: seq_proyectos; Type: SEQUENCE; Schema: sisat; Owner: suite
--

CREATE SEQUENCE sisat.seq_proyectos
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sisat.seq_proyectos OWNER TO suite;

--
-- TOC entry 390 (class 1259 OID 21989)
-- Name: seq_vacantes; Type: SEQUENCE; Schema: sisat; Owner: suite
--

CREATE SEQUENCE sisat.seq_vacantes
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sisat.seq_vacantes OWNER TO suite;

--
-- TOC entry 391 (class 1259 OID 21991)
-- Name: tsisatappservices; Type: TABLE; Schema: sisat; Owner: postgres
--

CREATE TABLE sisat.tsisatappservices (
    cod_appservice integer NOT NULL,
    des_appservice character varying(30) NOT NULL
);


ALTER TABLE sisat.tsisatappservices OWNER TO postgres;

--
-- TOC entry 392 (class 1259 OID 21994)
-- Name: tsisatarquitecturas; Type: TABLE; Schema: sisat; Owner: postgres
--

CREATE TABLE sisat.tsisatarquitecturas (
    cod_arquitectura integer NOT NULL,
    des_arquitectura character varying(30) NOT NULL
);


ALTER TABLE sisat.tsisatarquitecturas OWNER TO postgres;

--
-- TOC entry 393 (class 1259 OID 21997)
-- Name: tsisatasignaciones; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisatasignaciones (
    cod_asignacion integer DEFAULT nextval('sisat.seq_asignaciones'::regclass) NOT NULL,
    cod_prospecto integer NOT NULL,
    cod_perfil integer NOT NULL,
    cod_cliente integer NOT NULL,
    des_correocte character varying(40) NOT NULL,
    cod_telefonocte character varying(16) NOT NULL,
    des_direccioncte character varying(200),
    cod_empleado integer,
    cod_creadopor integer NOT NULL,
    cod_modificadopor integer NOT NULL,
    fec_creacion date NOT NULL,
    fec_modificacion date NOT NULL
);


ALTER TABLE sisat.tsisatasignaciones OWNER TO suite;

--
-- TOC entry 394 (class 1259 OID 22001)
-- Name: tsisatcandidatos; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisatcandidatos (
    cod_candidato integer DEFAULT nextval('sisat.seq_candidatos'::regclass) NOT NULL,
    des_nombre character varying(50) NOT NULL,
    cod_perfil integer NOT NULL,
    imp_sueldo numeric(6,2),
    imp_sueldodia numeric(6,2),
    imp_nominaimss numeric(6,2),
    imp_honorarios numeric(6,2),
    imp_cargasocial numeric(6,2),
    imp_prestaciones numeric(6,2),
    imp_viaticos numeric(6,2),
    imp_subtotalcandidato numeric(6,2),
    imp_costoadmin numeric(6,2),
    cnu_financiamiento smallint,
    imp_isr numeric(6,2),
    imp_financiamiento numeric(6,2),
    imp_adicionales numeric(6,2),
    imp_subtotaladmin1 numeric(6,2),
    imp_comisiones numeric(6,2),
    imp_otrosgastos numeric(6,2),
    imp_subtotaladmin2 numeric(6,2),
    imp_total numeric(6,2),
    imp_iva numeric(6,2),
    por_utilidad numeric(4,2),
    imp_utilidad numeric(6,2),
    imp_tarifa numeric(6,2),
    fec_creacion date NOT NULL,
    fec_modificacion date NOT NULL,
    cod_creadopor integer NOT NULL,
    cod_modificadopor integer NOT NULL
);


ALTER TABLE sisat.tsisatcandidatos OWNER TO suite;

--
-- TOC entry 395 (class 1259 OID 22005)
-- Name: tsisatcartaaceptacion; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisatcartaaceptacion (
    cod_aceptacion integer DEFAULT nextval('sisat.seq_aceptaciones'::regclass) NOT NULL,
    des_objetivo character varying(200) NOT NULL,
    txt_oferta text,
    des_esquema character varying(30) NOT NULL,
    tim_jornada time without time zone,
    txt_especificaciones text,
    cod_creadopor integer NOT NULL,
    cod_modificadopor integer NOT NULL,
    fec_creacion date NOT NULL,
    fec_modificacion date NOT NULL
);


ALTER TABLE sisat.tsisatcartaaceptacion OWNER TO suite;

--
-- TOC entry 396 (class 1259 OID 22012)
-- Name: tsisatcartaasignacion; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisatcartaasignacion (
    cod_asignacion integer DEFAULT nextval('sisat.seq_cartaasignaciones'::regclass) NOT NULL,
    cod_prospecto integer NOT NULL,
    cod_perfil integer NOT NULL,
    des_actividades character varying(200),
    des_lugarsalida character varying(100),
    des_lugarllegada character varying(100),
    fec_salida date,
    fec_llegada date,
    cod_transporte character varying(20),
    des_lugarhopedaje character varying(60),
    fec_hospedaje date,
    des_computadora character varying(150),
    cod_telefono character varying(16),
    des_accesorios character varying(150),
    des_nbresponsable character varying(50),
    des_nbpuesto character varying(50),
    des_lugarresp character varying(100),
    cod_telefonoresp character varying(16),
    tim_horario time without time zone,
    fec_iniciocontra date,
    fec_terminocontra date,
    imp_sueldomensual numeric(6,2),
    imp_nominaimss numeric(6,2),
    imp_honorarios numeric(6,2),
    imp_otros numeric(6,2),
    cod_rfc character varying(13),
    des_razonsocial character varying(45),
    des_correo character varying(50),
    cod_cpostal integer,
    des_direccionfact character varying(200),
    cod_cliente integer NOT NULL,
    cod_gpy integer,
    cod_rhta integer,
    cod_ape integer,
    cod_rys integer,
    cod_creadopor integer NOT NULL,
    cod_modificadopor integer NOT NULL,
    fec_creacion date NOT NULL,
    fec_modificacion date NOT NULL,
    des_observacion character varying(200) NOT NULL
);


ALTER TABLE sisat.tsisatcartaasignacion OWNER TO suite;

--
-- TOC entry 397 (class 1259 OID 22019)
-- Name: tsisatcomentarios; Type: TABLE; Schema: sisat; Owner: postgres
--

CREATE TABLE sisat.tsisatcomentarios (
    cod_comentario integer NOT NULL,
    des_comentario character varying(300) NOT NULL,
    cod_origen integer NOT NULL,
    validacion boolean NOT NULL,
    cod_cliente integer NOT NULL,
    cod_firma integer NOT NULL,
    cod_contratacion integer NOT NULL
);


ALTER TABLE sisat.tsisatcomentarios OWNER TO postgres;

--
-- TOC entry 398 (class 1259 OID 22022)
-- Name: tsisatcotizaciones; Type: TABLE; Schema: sisat; Owner: postgres
--

CREATE TABLE sisat.tsisatcotizaciones (
    cod_cotizacion integer NOT NULL,
    cod_ciudad integer,
    cod_estado integer,
    fec_fecha date NOT NULL,
    "des_nbcontacto " character varying(50) NOT NULL,
    cod_puesto integer,
    des_compania character varying(50) NOT NULL,
    des_nbservicio character varying(50) NOT NULL,
    cnu_cantidad smallint NOT NULL,
    txt_concepto text NOT NULL,
    imp_inversionhr numeric(6,2) NOT NULL,
    txt_condicionescomer text NOT NULL,
    des_nbatentamente character varying(60) NOT NULL,
    des_correoatentamente character varying(50) NOT NULL,
    cod_creadopor integer,
    cod_modificadopor integer,
    fec_creacion date,
    fec_modificacion date
);


ALTER TABLE sisat.tsisatcotizaciones OWNER TO postgres;

--
-- TOC entry 399 (class 1259 OID 22028)
-- Name: tsisatcursosycertificados; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisatcursosycertificados (
    cod_curso integer DEFAULT nextval('sisat.seq_cursos'::regclass) NOT NULL,
    cod_prospecto integer NOT NULL,
    des_curso character varying(100) NOT NULL,
    des_institucion character varying(70) NOT NULL,
    fec_termino date NOT NULL,
    fec_inicio date NOT NULL
);


ALTER TABLE sisat.tsisatcursosycertificados OWNER TO suite;

--
-- TOC entry 400 (class 1259 OID 22032)
-- Name: tsisatentrevistas; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisatentrevistas (
    cod_entrevista integer DEFAULT nextval('sisat.seq_entrevistas'::regclass) NOT NULL,
    des_nbentrevistador character varying(90) NOT NULL,
    des_puesto character varying(50) NOT NULL,
    des_correoent character varying(40) NOT NULL,
    cod_telefonoent character varying(16) NOT NULL,
    des_direccionent character varying(200),
    tim_horarioent time without time zone,
    fec_fechaent date NOT NULL,
    cod_creadopor integer NOT NULL,
    cod_modificadopor integer NOT NULL,
    fec_creacion date NOT NULL,
    fec_modificacion date NOT NULL,
    comentarios character varying(500) NOT NULL
);


ALTER TABLE sisat.tsisatentrevistas OWNER TO suite;

--
-- TOC entry 401 (class 1259 OID 22039)
-- Name: tsisatenviocorreos; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisatenviocorreos (
    cod_envio integer DEFAULT nextval('sisat.seq_envios'::regclass) NOT NULL,
    des_destinatario character varying(90) NOT NULL,
    des_asunto character varying(50) NOT NULL,
    des_mensaje character varying(200) NOT NULL,
    bin_adjunto bytea,
    cod_creadopor integer NOT NULL,
    cod_modificadopor integer NOT NULL,
    fec_creacion date NOT NULL,
    fec_modificacion date NOT NULL
);


ALTER TABLE sisat.tsisatenviocorreos OWNER TO suite;

--
-- TOC entry 402 (class 1259 OID 22046)
-- Name: tsisatescolaridad; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisatescolaridad (
    cod_escolaridad integer DEFAULT nextval('sisat.seq_escolaridad'::regclass) NOT NULL,
    cod_prospecto integer NOT NULL,
    des_escolaridad character varying(45) NOT NULL,
    des_escuela character varying(70) NOT NULL,
    fec_inicio date NOT NULL,
    fec_termino date NOT NULL,
    cod_estatus character varying(20)
);


ALTER TABLE sisat.tsisatescolaridad OWNER TO suite;

--
-- TOC entry 403 (class 1259 OID 22050)
-- Name: tsisatexperienciaslaborales; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisatexperienciaslaborales (
    cod_experiencia integer DEFAULT nextval('sisat.seq_experiencias'::regclass) NOT NULL,
    cod_prospecto integer NOT NULL,
    des_empresa character varying(50) NOT NULL,
    des_puesto character varying(40) NOT NULL,
    fec_inicio date,
    fec_termino date,
    des_ubicacion character varying(70),
    txt_funciones text,
    des_nbcliente character varying(70),
    des_proyecto character varying(70),
    txt_logros character varying(300),
    txt_tecnologiasemple text NOT NULL
);


ALTER TABLE sisat.tsisatexperienciaslaborales OWNER TO suite;

--
-- TOC entry 404 (class 1259 OID 22057)
-- Name: tsisatfirmas; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisatfirmas (
    cod_firma integer DEFAULT nextval('sisat.seq_firmas'::regclass) NOT NULL,
    cod_solicita integer,
    cod_puestosolicita integer,
    cod_autoriza integer,
    cod_puestoautoriza integer,
    cod_contratacion integer NOT NULL,
    cod_creadopor integer NOT NULL,
    cod_modificadopor integer NOT NULL,
    fec_creacion date NOT NULL,
    fec_modificacion date NOT NULL
);


ALTER TABLE sisat.tsisatfirmas OWNER TO suite;

--
-- TOC entry 405 (class 1259 OID 22061)
-- Name: tsisatframeworks; Type: TABLE; Schema: sisat; Owner: postgres
--

CREATE TABLE sisat.tsisatframeworks (
    cod_framework integer NOT NULL,
    des_framework character varying(30) NOT NULL
);


ALTER TABLE sisat.tsisatframeworks OWNER TO postgres;

--
-- TOC entry 406 (class 1259 OID 22064)
-- Name: tsisathabilidades; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisathabilidades (
    cod_habilidad integer DEFAULT nextval('sisat.seq_habilidades'::regclass) NOT NULL,
    cod_prospecto integer NOT NULL,
    des_habilidad character varying(50),
    des_dominio character varying(50)
);


ALTER TABLE sisat.tsisathabilidades OWNER TO suite;

--
-- TOC entry 407 (class 1259 OID 22068)
-- Name: tsisatherramientas; Type: TABLE; Schema: sisat; Owner: postgres
--

CREATE TABLE sisat.tsisatherramientas (
    cod_herramientas integer NOT NULL,
    json_tecnologia json NOT NULL,
    cod_prospecto integer NOT NULL,
    des_nivel character varying(30) NOT NULL,
    cnu_experiencia numeric(5,1) NOT NULL
);


ALTER TABLE sisat.tsisatherramientas OWNER TO postgres;

--
-- TOC entry 408 (class 1259 OID 22074)
-- Name: tsisatherramientas_cod_herramientas_seq; Type: SEQUENCE; Schema: sisat; Owner: postgres
--

CREATE SEQUENCE sisat.tsisatherramientas_cod_herramientas_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sisat.tsisatherramientas_cod_herramientas_seq OWNER TO postgres;

--
-- TOC entry 4903 (class 0 OID 0)
-- Dependencies: 408
-- Name: tsisatherramientas_cod_herramientas_seq; Type: SEQUENCE OWNED BY; Schema: sisat; Owner: postgres
--

ALTER SEQUENCE sisat.tsisatherramientas_cod_herramientas_seq OWNED BY sisat.tsisatherramientas.cod_herramientas;


--
-- TOC entry 409 (class 1259 OID 22076)
-- Name: tsisatides; Type: TABLE; Schema: sisat; Owner: postgres
--

CREATE TABLE sisat.tsisatides (
    cod_ide integer NOT NULL,
    des_ide character varying(30) NOT NULL
);


ALTER TABLE sisat.tsisatides OWNER TO postgres;

--
-- TOC entry 410 (class 1259 OID 22079)
-- Name: tsisatidiomas; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisatidiomas (
    cod_idioma integer DEFAULT nextval('sisat.seq_idiomas'::regclass) NOT NULL,
    cod_prospecto integer NOT NULL,
    cod_nbidioma character varying(20) NOT NULL,
    cod_nivel character varying(20) NOT NULL,
    des_certificado character varying(40)
);


ALTER TABLE sisat.tsisatidiomas OWNER TO suite;

--
-- TOC entry 411 (class 1259 OID 22083)
-- Name: tsisatlenguajes; Type: TABLE; Schema: sisat; Owner: postgres
--

CREATE TABLE sisat.tsisatlenguajes (
    cod_lenguaje integer NOT NULL,
    des_lenguaje character varying(30) NOT NULL
);


ALTER TABLE sisat.tsisatlenguajes OWNER TO postgres;

--
-- TOC entry 412 (class 1259 OID 22086)
-- Name: tsisatmaquetados; Type: TABLE; Schema: sisat; Owner: postgres
--

CREATE TABLE sisat.tsisatmaquetados (
    cod_maquetado integer NOT NULL,
    des_maquetado character varying(50) NOT NULL
);


ALTER TABLE sisat.tsisatmaquetados OWNER TO postgres;

--
-- TOC entry 413 (class 1259 OID 22089)
-- Name: tsisatmetodologias; Type: TABLE; Schema: sisat; Owner: postgres
--

CREATE TABLE sisat.tsisatmetodologias (
    cod_metodologia integer NOT NULL,
    des_metodologia character varying(30) NOT NULL
);


ALTER TABLE sisat.tsisatmetodologias OWNER TO postgres;

--
-- TOC entry 414 (class 1259 OID 22092)
-- Name: tsisatmodelados; Type: TABLE; Schema: sisat; Owner: postgres
--

CREATE TABLE sisat.tsisatmodelados (
    cod_modelado integer NOT NULL,
    des_modelado character varying(30) NOT NULL
);


ALTER TABLE sisat.tsisatmodelados OWNER TO postgres;

--
-- TOC entry 415 (class 1259 OID 22095)
-- Name: tsisatordenservicio; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisatordenservicio (
    cod_ordenservicio integer DEFAULT nextval('sisat.seq_ordenservicios'::regclass) NOT NULL,
    cod_estadorep integer NOT NULL,
    cod_ciudad integer NOT NULL,
    fec_fecha date NOT NULL,
    des_nbcontacto character varying(50),
    cod_puesto integer,
    des_nbcompania character varying(50),
    des_nbservicio character varying(60),
    cnu_cantidad smallint,
    txt_concepto text,
    imp_inversionhr numeric(6,2),
    txt_condicionescomer text,
    des_ubcnconsultor character varying(100),
    fec_finservicio date,
    cod_gpy integer NOT NULL,
    des_correogpy character varying(50) NOT NULL,
    cod_cliente integer NOT NULL,
    des_correoclte character varying(50) NOT NULL,
    des_empresaclte character varying(50),
    cod_creadopor integer NOT NULL,
    cod_modificadopor integer NOT NULL,
    fec_creacion date NOT NULL,
    fec_modificacion date NOT NULL
);


ALTER TABLE sisat.tsisatordenservicio OWNER TO suite;

--
-- TOC entry 416 (class 1259 OID 22102)
-- Name: tsisatpatrones; Type: TABLE; Schema: sisat; Owner: postgres
--

CREATE TABLE sisat.tsisatpatrones (
    cod_patron integer NOT NULL,
    des_patron character varying(30) NOT NULL
);


ALTER TABLE sisat.tsisatpatrones OWNER TO postgres;

--
-- TOC entry 417 (class 1259 OID 22105)
-- Name: tsisatprospectos; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisatprospectos (
    cod_prospecto integer DEFAULT nextval('sisat.seq_prospectos'::regclass) NOT NULL,
    des_nombre character varying(45) NOT NULL,
    des_nombres character varying(60),
    des_appaterno character varying(40) NOT NULL,
    des_apmaterno character varying(40),
    des_lugarnacimiento character varying(50) NOT NULL,
    fec_nacimiento date NOT NULL,
    cod_edad integer NOT NULL,
    cod_edocivil character varying(15) NOT NULL,
    des_nbpadre character varying(70),
    des_nbmadre character varying(70),
    cod_numhermanos integer,
    des_nbcalle character varying(60),
    cod_numcasa integer,
    des_colonia character varying(60),
    des_localidad character varying(60),
    des_municipio character varying(60),
    des_estado character varying(60),
    cod_cpostal integer,
    cod_tiposangre character varying(5),
    des_emailmbn character varying(40),
    des_emailpersonal character varying(40),
    des_pasatiempo character varying(200),
    cod_telefonocasa character varying(16) DEFAULT NULL::character varying,
    cod_telefonomovil character varying(16) DEFAULT NULL::character varying,
    cod_rfc character varying(13),
    cod_nss character varying(20),
    cod_curp character varying(18) NOT NULL,
    des_nacionalidad character varying(30),
    cod_administrador integer,
    fec_fechacoment date,
    txt_comentarios text,
    cod_creadopor integer NOT NULL,
    cod_modificadopor integer NOT NULL,
    fec_creacion date NOT NULL,
    fec_modificacion date NOT NULL,
    bin_foto bytea,
    cod_tipofoto character varying(30) NOT NULL,
    cod_extensionfoto character varying(5) NOT NULL,
    des_puestovacante character varying(50) NOT NULL,
    anio_experiencia integer,
    cod_entrevista integer
);


ALTER TABLE sisat.tsisatprospectos OWNER TO suite;

--
-- TOC entry 418 (class 1259 OID 22114)
-- Name: tsisatprotocolos; Type: TABLE; Schema: sisat; Owner: postgres
--

CREATE TABLE sisat.tsisatprotocolos (
    cod_protocolo integer NOT NULL,
    des_protocolo character varying(30) NOT NULL
);


ALTER TABLE sisat.tsisatprotocolos OWNER TO postgres;

--
-- TOC entry 419 (class 1259 OID 22117)
-- Name: tsisatproyectos; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisatproyectos (
    cod_proyecto integer DEFAULT nextval('sisat.seq_proyectos'::regclass) NOT NULL,
    cod_prospecto integer NOT NULL,
    cod_perfil integer NOT NULL,
    des_nbcliente character varying(50) NOT NULL,
    des_nbresponsable character varying(50) NOT NULL,
    des_correo character varying(50) NOT NULL,
    cod_telefono character varying(16) NOT NULL,
    des_direccion character varying(200) NOT NULL,
    cod_creadopor integer NOT NULL,
    cod_modificadopor integer NOT NULL,
    fec_creacion date NOT NULL,
    fec_modificacion date NOT NULL
);


ALTER TABLE sisat.tsisatproyectos OWNER TO suite;

--
-- TOC entry 420 (class 1259 OID 22121)
-- Name: tsisatqa; Type: TABLE; Schema: sisat; Owner: postgres
--

CREATE TABLE sisat.tsisatqa (
    cod_qa integer NOT NULL,
    des_qa character varying(30) NOT NULL
);


ALTER TABLE sisat.tsisatqa OWNER TO postgres;

--
-- TOC entry 421 (class 1259 OID 22124)
-- Name: tsisatrepositoriolibrerias; Type: TABLE; Schema: sisat; Owner: postgres
--

CREATE TABLE sisat.tsisatrepositoriolibrerias (
    cod_repositoriolibreria integer NOT NULL,
    des_repositoriolibreria character varying(30) NOT NULL
);


ALTER TABLE sisat.tsisatrepositoriolibrerias OWNER TO postgres;

--
-- TOC entry 422 (class 1259 OID 22127)
-- Name: tsisatrepositorios; Type: TABLE; Schema: sisat; Owner: postgres
--

CREATE TABLE sisat.tsisatrepositorios (
    cod_repositorio integer NOT NULL,
    des_repositorio character varying(30) NOT NULL
);


ALTER TABLE sisat.tsisatrepositorios OWNER TO postgres;

--
-- TOC entry 423 (class 1259 OID 22130)
-- Name: tsisatsgbd; Type: TABLE; Schema: sisat; Owner: postgres
--

CREATE TABLE sisat.tsisatsgbd (
    cod_sgbd integer NOT NULL,
    des_sgbd character varying(30) NOT NULL
);


ALTER TABLE sisat.tsisatsgbd OWNER TO postgres;

--
-- TOC entry 424 (class 1259 OID 22133)
-- Name: tsisatso; Type: TABLE; Schema: sisat; Owner: postgres
--

CREATE TABLE sisat.tsisatso (
    cod_so integer NOT NULL,
    des_so character varying(30) NOT NULL
);


ALTER TABLE sisat.tsisatso OWNER TO postgres;

--
-- TOC entry 425 (class 1259 OID 22136)
-- Name: tsisatvacantes; Type: TABLE; Schema: sisat; Owner: suite
--

CREATE TABLE sisat.tsisatvacantes (
    cod_vacante integer DEFAULT nextval('sisat.seq_vacantes'::regclass) NOT NULL,
    des_rqvacante character varying(200) NOT NULL,
    cnu_anexperiencia smallint,
    txt_experiencia text,
    des_escolaridad character varying(50),
    txt_herramientas text,
    txt_habilidades text,
    des_lugartrabajo character varying(100),
    imp_sueldo numeric(6,2),
    cod_creadopor integer NOT NULL,
    cod_modificadopor integer NOT NULL,
    fec_creacion date NOT NULL,
    fec_modificacion date NOT NULL,
    cod_idioma integer NOT NULL,
    sexo character varying(11) NOT NULL,
    fec_solicitud date,
    fec_entrega date,
    cod_cliente integer
);


ALTER TABLE sisat.tsisatvacantes OWNER TO suite;

--
-- TOC entry 3728 (class 2604 OID 22143)
-- Name: tsisatherramientas cod_herramientas; Type: DEFAULT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatherramientas ALTER COLUMN cod_herramientas SET DEFAULT nextval('sisat.tsisatherramientas_cod_herramientas_seq'::regclass);


--
-- TOC entry 4711 (class 0 OID 31595)
-- Dependencies: 428
-- Data for Name: rtsueldobase; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.rtsueldobase (imp_honorarios) VALUES (213.00);


--
-- TOC entry 4491 (class 0 OID 16460)
-- Dependencies: 208
-- Data for Name: tsgcosistemas; Type: TABLE DATA; Schema: sgco; Owner: postgres
--

INSERT INTO sgco.tsgcosistemas (cod_sistema, des_nbsistema, des_descripcion) VALUES (1, 'Sistema de Recursos Humanos y Ambiente de Trabajo', 'SGRHAT');
INSERT INTO sgco.tsgcosistemas (cod_sistema, des_nbsistema, des_descripcion) VALUES (2, 'Sistema de Integración', 'SISAT');
INSERT INTO sgco.tsgcosistemas (cod_sistema, des_nbsistema, des_descripcion) VALUES (4, 'sistat', 'sistema admnistracion y seleccion de talentos');
INSERT INTO sgco.tsgcosistemas (cod_sistema, des_nbsistema, des_descripcion) VALUES (3, 'Sistema de nomina
', 'SGNOM');


--
-- TOC entry 4492 (class 0 OID 16463)
-- Dependencies: 209
-- Data for Name: tsgcotipousuario; Type: TABLE DATA; Schema: sgco; Owner: postgres
--

INSERT INTO sgco.tsgcotipousuario (cod_tipousuario, cod_usuario, cod_sistema, cod_rol) VALUES (1, 1, 3, '1');


--
-- TOC entry 4493 (class 0 OID 16466)
-- Dependencies: 210
-- Data for Name: tsgcousuarios; Type: TABLE DATA; Schema: sgco; Owner: postgres
--

INSERT INTO sgco.tsgcousuarios (cod_usuario, cod_empleado, des_correo, des_contrasenacorreo, cod_usuariosistema, des_contrasenasistema) VALUES (2, 11, 'adrian.suarez@gmail.com', '123456789', 'adrian_suarez', '123456789');
INSERT INTO sgco.tsgcousuarios (cod_usuario, cod_empleado, des_correo, des_contrasenacorreo, cod_usuariosistema, des_contrasenasistema) VALUES (3, 12, 'carlos.antonio@gmail.com', 'abcdefg', 'carlos_antonio', 'abcdefg');
INSERT INTO sgco.tsgcousuarios (cod_usuario, cod_empleado, des_correo, des_contrasenacorreo, cod_usuariosistema, des_contrasenasistema) VALUES (4, 13, 'angel.roano@gmail.com', '123456', 'angel_roano', '123456');
INSERT INTO sgco.tsgcousuarios (cod_usuario, cod_empleado, des_correo, des_contrasenacorreo, cod_usuariosistema, des_contrasenasistema) VALUES (1, 10, 'mateorj96@gmail.com', '143100365m@teorj96', 'mateorj96', 'root');
INSERT INTO sgco.tsgcousuarios (cod_usuario, cod_empleado, des_correo, des_contrasenacorreo, cod_usuariosistema, des_contrasenasistema) VALUES (5, 1, 'mnflambr@gmail.com', '1234', 'ambro', '1234');


--
-- TOC entry 4631 (class 0 OID 20873)
-- Dependencies: 348
-- Data for Name: tsgnomalguinaldo; Type: TABLE DATA; Schema: sgnom; Owner: suite
--



--
-- TOC entry 4632 (class 0 OID 20879)
-- Dependencies: 349
-- Data for Name: tsgnomargumento; Type: TABLE DATA; Schema: sgnom; Owner: suite
--

INSERT INTO sgnom.tsgnomargumento (cod_argumentoid, cod_nbargumento, cod_clavearg, imp_valorconst, des_funcionbd, cod_tipoargumento, bol_estatus, txt_descripcion, aud_codcreadopor, aud_codmodificadopor, aud_feccreacion, aud_fecmodificacion) VALUES (3, 'dias laborados', 'DL', NULL, 'fn_dias_laborados', '0', true, 'dias laborados', 10, NULL, '2019-07-01', NULL);
INSERT INTO sgnom.tsgnomargumento (cod_argumentoid, cod_nbargumento, cod_clavearg, imp_valorconst, des_funcionbd, cod_tipoargumento, bol_estatus, txt_descripcion, aud_codcreadopor, aud_codmodificadopor, aud_feccreacion, aud_fecmodificacion) VALUES (1, 'Sueldo Base', 'SB', NULL, 'fn_sueldo_base', '0', true, 'obtiene sueldo base', 10, NULL, '2019-07-01', NULL);
INSERT INTO sgnom.tsgnomargumento (cod_argumentoid, cod_nbargumento, cod_clavearg, imp_valorconst, des_funcionbd, cod_tipoargumento, bol_estatus, txt_descripcion, aud_codcreadopor, aud_codmodificadopor, aud_feccreacion, aud_fecmodificacion) VALUES (2, 'dias del mes', 'DM', 30.00, NULL, '1', true, 'dias del mes', 10, NULL, '2019-07-01', NULL);
INSERT INTO sgnom.tsgnomargumento (cod_argumentoid, cod_nbargumento, cod_clavearg, imp_valorconst, des_funcionbd, cod_tipoargumento, bol_estatus, txt_descripcion, aud_codcreadopor, aud_codmodificadopor, aud_feccreacion, aud_fecmodificacion) VALUES (4, '1', '1', NULL, 'fn_sueldo_base', '0', true, '12', 13, NULL, '2019-07-15', NULL);


--
-- TOC entry 4633 (class 0 OID 20885)
-- Dependencies: 350
-- Data for Name: tsgnombitacora; Type: TABLE DATA; Schema: sgnom; Owner: suite
--



--
-- TOC entry 4634 (class 0 OID 20891)
-- Dependencies: 351
-- Data for Name: tsgnomcabecera; Type: TABLE DATA; Schema: sgnom; Owner: suite
--

INSERT INTO sgnom.tsgnomcabecera (cod_cabeceraid, cod_nbnomina, fec_creacion, fec_ejecucion, fec_cierre, imp_totpercepcion, imp_totdeduccion, imp_totalemp, cod_quincenaid_fk, cod_tiponominaid_fk, cod_estatusnomid_fk, cnu_totalemp, aud_codcreadopor, aud_codmodificadopor, aud_feccreacion, aud_fecmodificacion) VALUES (16, '1ra jul', '2019-07-08', NULL, NULL, 8319.50, 0.00, 8319.50, 13, 1, 2, NULL, 10, NULL, '2019-07-08', NULL);
INSERT INTO sgnom.tsgnomcabecera (cod_cabeceraid, cod_nbnomina, fec_creacion, fec_ejecucion, fec_cierre, imp_totpercepcion, imp_totdeduccion, imp_totalemp, cod_quincenaid_fk, cod_tiponominaid_fk, cod_estatusnomid_fk, cnu_totalemp, aud_codcreadopor, aud_codmodificadopor, aud_feccreacion, aud_fecmodificacion) VALUES (17, '2da jul', '2019-07-12', NULL, NULL, 6600.00, 0.00, 6600.00, 14, 1, 2, NULL, 10, NULL, '2019-07-12', NULL);


--
-- TOC entry 4635 (class 0 OID 20894)
-- Dependencies: 352
-- Data for Name: tsgnomcabeceraht; Type: TABLE DATA; Schema: sgnom; Owner: suite
--



--
-- TOC entry 4636 (class 0 OID 20897)
-- Dependencies: 353
-- Data for Name: tsgnomcalculo; Type: TABLE DATA; Schema: sgnom; Owner: suite
--

INSERT INTO sgnom.tsgnomcalculo (cod_calculoid, cod_tpcalculo, bol_estatus) VALUES (1, 'Importe', true);
INSERT INTO sgnom.tsgnomcalculo (cod_calculoid, cod_tpcalculo, bol_estatus) VALUES (2, 'Calculo', true);


--
-- TOC entry 4637 (class 0 OID 20900)
-- Dependencies: 354
-- Data for Name: tsgnomcatincidencia; Type: TABLE DATA; Schema: sgnom; Owner: suite
--

INSERT INTO sgnom.tsgnomcatincidencia (cod_catincidenciaid, cod_claveincidencia, cod_nbincidencia, cod_perfilincidencia, bol_estatus, cod_tipoincidencia, imp_monto) VALUES (4, 'M', 'M', 'M', true, '1', 1.00);
INSERT INTO sgnom.tsgnomcatincidencia (cod_catincidenciaid, cod_claveincidencia, cod_nbincidencia, cod_perfilincidencia, bol_estatus, cod_tipoincidencia, imp_monto) VALUES (1, 'HE', 'Horas extra', 'SF', true, '1', 100.00);
INSERT INTO sgnom.tsgnomcatincidencia (cod_catincidenciaid, cod_claveincidencia, cod_nbincidencia, cod_perfilincidencia, bol_estatus, cod_tipoincidencia, imp_monto) VALUES (6, 'HE2', 'horas extra 2', 'horas extra 2', true, '1', 1000.00);
INSERT INTO sgnom.tsgnomcatincidencia (cod_catincidenciaid, cod_claveincidencia, cod_nbincidencia, cod_perfilincidencia, bol_estatus, cod_tipoincidencia, imp_monto) VALUES (5, 'Z', 'Z', 'Z', true, '1', 1.00);
INSERT INTO sgnom.tsgnomcatincidencia (cod_catincidenciaid, cod_claveincidencia, cod_nbincidencia, cod_perfilincidencia, bol_estatus, cod_tipoincidencia, imp_monto) VALUES (3, 'd', 'd', '1', true, '1', 1.00);
INSERT INTO sgnom.tsgnomcatincidencia (cod_catincidenciaid, cod_claveincidencia, cod_nbincidencia, cod_perfilincidencia, bol_estatus, cod_tipoincidencia, imp_monto) VALUES (2, 'HS', 'g', 'fdv', true, '1', 3.00);


--
-- TOC entry 4638 (class 0 OID 20903)
-- Dependencies: 355
-- Data for Name: tsgnomclasificador; Type: TABLE DATA; Schema: sgnom; Owner: suite
--

INSERT INTO sgnom.tsgnomclasificador (cod_clasificadorid, cod_tpclasificador, bol_estatus) VALUES (1, 'clas1', true);
INSERT INTO sgnom.tsgnomclasificador (cod_clasificadorid, cod_tpclasificador, bol_estatus) VALUES (2, 'clas2', true);


--
-- TOC entry 4639 (class 0 OID 20906)
-- Dependencies: 356
-- Data for Name: tsgnomcncptoquinc; Type: TABLE DATA; Schema: sgnom; Owner: suite
--

INSERT INTO sgnom.tsgnomcncptoquinc (cod_cncptoquincid, cod_empquincenaid_fk, cod_conceptoid_fk, imp_concepto, imp_gravado, imp_exento, xml_desgloce) VALUES (3, 66, 1, 4100.00, 0.00, 0.00, '                
                <mx.org.ift.serp.conceptoBindingXml.DetallesConcepto>                  
                <claveConcepto>SQ</claveConcepto>                  
                <nombreConcepto>sueldo quincena</nombreConcepto><formula>:(:Sueldo Base:/:dias del mes:):*:dias laborados</formula>                  
                <argumentos><mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>Sueldo Base</nombre>                            
                    <valor>8200.00</valor> <descripcion>Sueldo Base</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento><mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>dias del mes</nombre>                            
                    <valor>30.00</valor> <descripcion>dias del mes</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento><mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>dias laborados</nombre>                            
                    <valor>15.00</valor> <descripcion>dias laborados</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento></argumentos><importe>4100.00</importe> </mx.org.ift.serp.conceptoBindingXml.DetallesConcepto>  ');
INSERT INTO sgnom.tsgnomcncptoquinc (cod_cncptoquincid, cod_empquincenaid_fk, cod_conceptoid_fk, imp_concepto, imp_gravado, imp_exento, xml_desgloce) VALUES (4, 67, 1, 106.50, 0.00, 0.00, '                
                <mx.org.ift.serp.conceptoBindingXml.DetallesConcepto>                  
                <claveConcepto>SQ</claveConcepto>                  
                <nombreConcepto>sueldo quincena</nombreConcepto><formula>:(:Sueldo Base:/:dias del mes:):*:dias laborados</formula>                  
                <argumentos><mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>Sueldo Base</nombre>                            
                    <valor>213.00</valor> <descripcion>Sueldo Base</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento><mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>dias del mes</nombre>                            
                    <valor>30.00</valor> <descripcion>dias del mes</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento><mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>dias laborados</nombre>                            
                    <valor>15.00</valor> <descripcion>dias laborados</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento></argumentos><importe>106.50</importe> </mx.org.ift.serp.conceptoBindingXml.DetallesConcepto>  ');
INSERT INTO sgnom.tsgnomcncptoquinc (cod_cncptoquincid, cod_empquincenaid_fk, cod_conceptoid_fk, imp_concepto, imp_gravado, imp_exento, xml_desgloce) VALUES (8, 79, 1, 2500.00, 0.00, 0.00, '                
                <mx.org.ift.serp.conceptoBindingXml.DetallesConcepto>                  
                <claveConcepto>SQ</claveConcepto>                  
                <nombreConcepto>sueldo quincena</nombreConcepto><formula>:(:Sueldo Base:/:dias del mes:):*:dias laborados</formula>                  
                <argumentos><mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>Sueldo Base</nombre>                            
                    <valor>5000.00</valor> <descripcion>Sueldo Base</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento><mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>dias del mes</nombre>                            
                    <valor>30.00</valor> <descripcion>dias del mes</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento><mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>dias laborados</nombre>                            
                    <valor>15.00</valor> <descripcion>dias laborados</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento></argumentos><importe>2500.00</importe> </mx.org.ift.serp.conceptoBindingXml.DetallesConcepto>  ');
INSERT INTO sgnom.tsgnomcncptoquinc (cod_cncptoquincid, cod_empquincenaid_fk, cod_conceptoid_fk, imp_concepto, imp_gravado, imp_exento, xml_desgloce) VALUES (9, 80, 1, 4100.00, 0.00, 0.00, '                
                <mx.org.ift.serp.conceptoBindingXml.DetallesConcepto>                  
                <claveConcepto>SQ</claveConcepto>                  
                <nombreConcepto>sueldo quincena</nombreConcepto><formula>:(:Sueldo Base:/:dias del mes:):*:dias laborados</formula>                  
                <argumentos><mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>Sueldo Base</nombre>                            
                    <valor>8200.00</valor> <descripcion>Sueldo Base</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento><mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>dias del mes</nombre>                            
                    <valor>30.00</valor> <descripcion>dias del mes</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento><mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>dias laborados</nombre>                            
                    <valor>15.00</valor> <descripcion>dias laborados</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento></argumentos><importe>4100.00</importe> </mx.org.ift.serp.conceptoBindingXml.DetallesConcepto>  ');
INSERT INTO sgnom.tsgnomcncptoquinc (cod_cncptoquincid, cod_empquincenaid_fk, cod_conceptoid_fk, imp_concepto, imp_gravado, imp_exento, xml_desgloce) VALUES (1, 65, 1, 2500.00, 0.00, 0.00, '                
                <mx.org.ift.serp.conceptoBindingXml.DetallesConcepto>                  
                <claveConcepto>SQ</claveConcepto>                  
                <nombreConcepto>sueldo quincena</nombreConcepto><formula>:(:Sueldo Base:/:dias del mes:):*:dias laborados</formula>                  
                <argumentos><mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>Sueldo Base</nombre>                            
                    <valor>5000.00</valor> <descripcion>Sueldo Base</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento><mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>dias del mes</nombre>                            
                    <valor>30.00</valor> <descripcion>dias del mes</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento><mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>dias laborados</nombre>                            
                    <valor>15.00</valor> <descripcion>dias laborados</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento></argumentos><importe>2500.00</importe> </mx.org.ift.serp.conceptoBindingXml.DetallesConcepto>  ');
INSERT INTO sgnom.tsgnomcncptoquinc (cod_cncptoquincid, cod_empquincenaid_fk, cod_conceptoid_fk, imp_concepto, imp_gravado, imp_exento, xml_desgloce) VALUES (2, 70, 1, 400.00, 0.00, 0.00, '                
                <mx.org.ift.serp.conceptoBindingXml.DetallesConcepto>                  
                <claveConcepto>SQ</claveConcepto>                  
                <nombreConcepto>sueldo quincena</nombreConcepto><formula>:(:Sueldo Base:/:dias del mes:):*:dias laborados</formula>                  
                <argumentos><mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>Sueldo Base</nombre>                            
                    <valor>1000.00</valor> <descripcion>Sueldo Base</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento><mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>dias del mes</nombre>                            
                    <valor>30.00</valor> <descripcion>dias del mes</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento><mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>dias laborados</nombre>                            
                    <valor>12.00</valor> <descripcion>dias laborados</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento></argumentos><importe>400.00</importe> </mx.org.ift.serp.conceptoBindingXml.DetallesConcepto>  ');
INSERT INTO sgnom.tsgnomcncptoquinc (cod_cncptoquincid, cod_empquincenaid_fk, cod_conceptoid_fk, imp_concepto, imp_gravado, imp_exento, xml_desgloce) VALUES (5, 68, 1, 106.50, 0.00, 0.00, '                
                <mx.org.ift.serp.conceptoBindingXml.DetallesConcepto>                  
                <claveConcepto>SQ</claveConcepto>                  
                <nombreConcepto>sueldo quincena</nombreConcepto><formula>:(:Sueldo Base:/:dias del mes:):*:dias laborados</formula>                  
                <argumentos><mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>Sueldo Base</nombre>                            
                    <valor>213.00</valor> <descripcion>Sueldo Base</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento><mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>dias del mes</nombre>                            
                    <valor>30.00</valor> <descripcion>dias del mes</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento><mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>dias laborados</nombre>                            
                    <valor>15.00</valor> <descripcion>dias laborados</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento></argumentos><importe>106.50</importe> </mx.org.ift.serp.conceptoBindingXml.DetallesConcepto>  ');
INSERT INTO sgnom.tsgnomcncptoquinc (cod_cncptoquincid, cod_empquincenaid_fk, cod_conceptoid_fk, imp_concepto, imp_gravado, imp_exento, xml_desgloce) VALUES (6, 69, 1, 106.50, 0.00, 0.00, '                
                <mx.org.ift.serp.conceptoBindingXml.DetallesConcepto>                  
                <claveConcepto>SQ</claveConcepto>                  
                <nombreConcepto>sueldo quincena</nombreConcepto><formula>:(:Sueldo Base:/:dias del mes:):*:dias laborados</formula>                  
                <argumentos><mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>Sueldo Base</nombre>                            
                    <valor>213.00</valor> <descripcion>Sueldo Base</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento><mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>dias del mes</nombre>                            
                    <valor>30.00</valor> <descripcion>dias del mes</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento><mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>dias laborados</nombre>                            
                    <valor>15.00</valor> <descripcion>dias laborados</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento></argumentos><importe>106.50</importe> </mx.org.ift.serp.conceptoBindingXml.DetallesConcepto>  ');
INSERT INTO sgnom.tsgnomcncptoquinc (cod_cncptoquincid, cod_empquincenaid_fk, cod_conceptoid_fk, imp_concepto, imp_gravado, imp_exento, xml_desgloce) VALUES (7, 71, 1, 1000.00, 0.00, 0.00, '                
                <mx.org.ift.serp.conceptoBindingXml.DetallesConcepto>                  
                <claveConcepto>SQ</claveConcepto>                  
                <nombreConcepto>sueldo quincena</nombreConcepto><formula>:(:Sueldo Base:/:dias del mes:):*:dias laborados</formula>                  
                <argumentos><mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>Sueldo Base</nombre>                            
                    <valor>2000.00</valor> <descripcion>Sueldo Base</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento><mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>dias del mes</nombre>                            
                    <valor>30.00</valor> <descripcion>dias del mes</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento><mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>dias laborados</nombre>                            
                    <valor>15.00</valor> <descripcion>dias laborados</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento></argumentos><importe>1000.00</importe> </mx.org.ift.serp.conceptoBindingXml.DetallesConcepto>  ');


--
-- TOC entry 4640 (class 0 OID 20912)
-- Dependencies: 357
-- Data for Name: tsgnomcncptoquincht; Type: TABLE DATA; Schema: sgnom; Owner: suite
--



--
-- TOC entry 4641 (class 0 OID 20918)
-- Dependencies: 358
-- Data for Name: tsgnomconcepto; Type: TABLE DATA; Schema: sgnom; Owner: suite
--

INSERT INTO sgnom.tsgnomconcepto (cod_conceptoid, cod_nbconcepto, cod_claveconcepto, cnu_prioricalculo, cnu_articulo, bol_estatus, cod_formulaid_fk, cod_tipoconceptoid_fk, cod_calculoid_fk, cod_conceptosatid_fk, cod_frecuenciapago, cod_partidaprep, cnu_cuentacontable, cod_gravado, cod_excento, bol_aplicaisn, bol_retroactividad, cnu_topeex, cod_clasificadorid_fk, cod_tiponominaid_fk, aud_codcreadopor, aud_codmodificadopor, aud_feccreacion, aud_fecmodificacion) VALUES (3, 'f', 'f', 2, 4, true, NULL, 2, 1, 1, 'Q', 4, 4, NULL, NULL, false, false, NULL, 1, 1, 13, NULL, '2019-07-14', NULL);
INSERT INTO sgnom.tsgnomconcepto (cod_conceptoid, cod_nbconcepto, cod_claveconcepto, cnu_prioricalculo, cnu_articulo, bol_estatus, cod_formulaid_fk, cod_tipoconceptoid_fk, cod_calculoid_fk, cod_conceptosatid_fk, cod_frecuenciapago, cod_partidaprep, cnu_cuentacontable, cod_gravado, cod_excento, bol_aplicaisn, bol_retroactividad, cnu_topeex, cod_clasificadorid_fk, cod_tiponominaid_fk, aud_codcreadopor, aud_codmodificadopor, aud_feccreacion, aud_fecmodificacion) VALUES (5, 'x', 'x', 3, 4, true, 13, 2, 2, 1, 'Q', 4, 4, NULL, NULL, false, false, NULL, 1, 1, 13, NULL, '2019-07-14', NULL);
INSERT INTO sgnom.tsgnomconcepto (cod_conceptoid, cod_nbconcepto, cod_claveconcepto, cnu_prioricalculo, cnu_articulo, bol_estatus, cod_formulaid_fk, cod_tipoconceptoid_fk, cod_calculoid_fk, cod_conceptosatid_fk, cod_frecuenciapago, cod_partidaprep, cnu_cuentacontable, cod_gravado, cod_excento, bol_aplicaisn, bol_retroactividad, cnu_topeex, cod_clasificadorid_fk, cod_tiponominaid_fk, aud_codcreadopor, aud_codmodificadopor, aud_feccreacion, aud_fecmodificacion) VALUES (4, 'e', 'e', 4, 3, true, 12, 2, 2, 1, 'Q', 3, 3, NULL, NULL, false, false, NULL, 1, 1, 13, NULL, '2019-07-14', NULL);
INSERT INTO sgnom.tsgnomconcepto (cod_conceptoid, cod_nbconcepto, cod_claveconcepto, cnu_prioricalculo, cnu_articulo, bol_estatus, cod_formulaid_fk, cod_tipoconceptoid_fk, cod_calculoid_fk, cod_conceptosatid_fk, cod_frecuenciapago, cod_partidaprep, cnu_cuentacontable, cod_gravado, cod_excento, bol_aplicaisn, bol_retroactividad, cnu_topeex, cod_clasificadorid_fk, cod_tiponominaid_fk, aud_codcreadopor, aud_codmodificadopor, aud_feccreacion, aud_fecmodificacion) VALUES (2, 'q', 'q', 1, 1, true, 11, 1, 2, 1, 'Q', 1, 1, NULL, NULL, false, false, NULL, 1, 1, 13, NULL, '2019-07-13', NULL);
INSERT INTO sgnom.tsgnomconcepto (cod_conceptoid, cod_nbconcepto, cod_claveconcepto, cnu_prioricalculo, cnu_articulo, bol_estatus, cod_formulaid_fk, cod_tipoconceptoid_fk, cod_calculoid_fk, cod_conceptosatid_fk, cod_frecuenciapago, cod_partidaprep, cnu_cuentacontable, cod_gravado, cod_excento, bol_aplicaisn, bol_retroactividad, cnu_topeex, cod_clasificadorid_fk, cod_tiponominaid_fk, aud_codcreadopor, aud_codmodificadopor, aud_feccreacion, aud_fecmodificacion) VALUES (12, 'TT', 'TTT', 2, 5, true, NULL, 1, 1, 1, 'Q', 5, 5, NULL, NULL, false, false, NULL, 1, 1, 13, NULL, '2019-07-14', NULL);
INSERT INTO sgnom.tsgnomconcepto (cod_conceptoid, cod_nbconcepto, cod_claveconcepto, cnu_prioricalculo, cnu_articulo, bol_estatus, cod_formulaid_fk, cod_tipoconceptoid_fk, cod_calculoid_fk, cod_conceptosatid_fk, cod_frecuenciapago, cod_partidaprep, cnu_cuentacontable, cod_gravado, cod_excento, bol_aplicaisn, bol_retroactividad, cnu_topeex, cod_clasificadorid_fk, cod_tiponominaid_fk, aud_codcreadopor, aud_codmodificadopor, aud_feccreacion, aud_fecmodificacion) VALUES (10, 'fff', 'fff', 3, 5, true, NULL, 1, 1, 1, 'Q', 5, 5, NULL, NULL, false, false, NULL, 1, 1, 13, NULL, '2019-07-14', NULL);
INSERT INTO sgnom.tsgnomconcepto (cod_conceptoid, cod_nbconcepto, cod_claveconcepto, cnu_prioricalculo, cnu_articulo, bol_estatus, cod_formulaid_fk, cod_tipoconceptoid_fk, cod_calculoid_fk, cod_conceptosatid_fk, cod_frecuenciapago, cod_partidaprep, cnu_cuentacontable, cod_gravado, cod_excento, bol_aplicaisn, bol_retroactividad, cnu_topeex, cod_clasificadorid_fk, cod_tiponominaid_fk, aud_codcreadopor, aud_codmodificadopor, aud_feccreacion, aud_fecmodificacion) VALUES (1, 'SUELDO QUINCENA', 'SQ', 1, 1, true, 9, 2, 2, 1, 'Q', 1, 1, '0', '0', false, false, NULL, 1, 1, 13, 13, '2019-07-08', '2019-08-19');


--
-- TOC entry 4642 (class 0 OID 20921)
-- Dependencies: 359
-- Data for Name: tsgnomconceptosat; Type: TABLE DATA; Schema: sgnom; Owner: suite
--

INSERT INTO sgnom.tsgnomconceptosat (cod_conceptosatid, des_conceptosat, des_descconcepto, bol_estatus) VALUES (1, 'ss1', 'sat1', true);
INSERT INTO sgnom.tsgnomconceptosat (cod_conceptosatid, des_conceptosat, des_descconcepto, bol_estatus) VALUES (2, 'ss2', 'sat2', true);


--
-- TOC entry 4643 (class 0 OID 20924)
-- Dependencies: 360
-- Data for Name: tsgnomconfpago; Type: TABLE DATA; Schema: sgnom; Owner: suite
--



--
-- TOC entry 4644 (class 0 OID 20927)
-- Dependencies: 361
-- Data for Name: tsgnomejercicio; Type: TABLE DATA; Schema: sgnom; Owner: suite
--

INSERT INTO sgnom.tsgnomejercicio (cod_ejercicioid, cnu_valorejercicio, bol_estatus) VALUES (1, 2014, true);
INSERT INTO sgnom.tsgnomejercicio (cod_ejercicioid, cnu_valorejercicio, bol_estatus) VALUES (2, 2015, true);
INSERT INTO sgnom.tsgnomejercicio (cod_ejercicioid, cnu_valorejercicio, bol_estatus) VALUES (3, 2016, true);
INSERT INTO sgnom.tsgnomejercicio (cod_ejercicioid, cnu_valorejercicio, bol_estatus) VALUES (4, 2017, true);
INSERT INTO sgnom.tsgnomejercicio (cod_ejercicioid, cnu_valorejercicio, bol_estatus) VALUES (5, 2018, true);
INSERT INTO sgnom.tsgnomejercicio (cod_ejercicioid, cnu_valorejercicio, bol_estatus) VALUES (6, 2019, true);
INSERT INTO sgnom.tsgnomejercicio (cod_ejercicioid, cnu_valorejercicio, bol_estatus) VALUES (7, 2020, true);
INSERT INTO sgnom.tsgnomejercicio (cod_ejercicioid, cnu_valorejercicio, bol_estatus) VALUES (8, 2021, true);


--
-- TOC entry 4645 (class 0 OID 20930)
-- Dependencies: 362
-- Data for Name: tsgnomempleados; Type: TABLE DATA; Schema: sgnom; Owner: suite
--

INSERT INTO sgnom.tsgnomempleados (cod_empleadoid, fec_ingreso, fec_salida, bol_estatus, cod_empleado_fk, imp_sueldoimss, imp_honorarios, imp_finiquito, cod_tipoimss, cod_tipohonorarios, cod_banco, cod_sucursal, cod_cuenta, txt_descripcionbaja, aud_codcreadopor, aud_feccreacion, aud_codmodificadopor, aud_fecmodificacion, cod_clabe) VALUES (13, '2019-05-07', '2019-05-10', true, 13, 123.00, 213.00, 123.00, '1', 'o', '123', 321, '12345', 'baja', 1, '2190-12-21', 1, '1990-12-21', '12345');
INSERT INTO sgnom.tsgnomempleados (cod_empleadoid, fec_ingreso, fec_salida, bol_estatus, cod_empleado_fk, imp_sueldoimss, imp_honorarios, imp_finiquito, cod_tipoimss, cod_tipohonorarios, cod_banco, cod_sucursal, cod_cuenta, txt_descripcionbaja, aud_codcreadopor, aud_feccreacion, aud_codmodificadopor, aud_fecmodificacion, cod_clabe) VALUES (14, '2019-05-07', NULL, true, 14, 123.00, 213.00, 123.00, '1', 'o', '123', 321, '12345', 'baja', 1, '2190-12-21', 1, '1990-12-21', '12345');
INSERT INTO sgnom.tsgnomempleados (cod_empleadoid, fec_ingreso, fec_salida, bol_estatus, cod_empleado_fk, imp_sueldoimss, imp_honorarios, imp_finiquito, cod_tipoimss, cod_tipohonorarios, cod_banco, cod_sucursal, cod_cuenta, txt_descripcionbaja, aud_codcreadopor, aud_feccreacion, aud_codmodificadopor, aud_fecmodificacion, cod_clabe) VALUES (16, '2019-05-07', NULL, true, 16, 123.00, 213.00, 123.00, '1', 'o', '123', 321, '12345', 'baja', 1, '2190-12-21', 1, '1990-12-21', '12345');
INSERT INTO sgnom.tsgnomempleados (cod_empleadoid, fec_ingreso, fec_salida, bol_estatus, cod_empleado_fk, imp_sueldoimss, imp_honorarios, imp_finiquito, cod_tipoimss, cod_tipohonorarios, cod_banco, cod_sucursal, cod_cuenta, txt_descripcionbaja, aud_codcreadopor, aud_feccreacion, aud_codmodificadopor, aud_fecmodificacion, cod_clabe) VALUES (18, '2019-05-07', '2019-06-10', false, 18, 123.00, 213.00, 123.00, '1', 'o', '123', 321, '12345', 'baja', 1, '2190-12-21', 1, '1990-12-21', '12345');
INSERT INTO sgnom.tsgnomempleados (cod_empleadoid, fec_ingreso, fec_salida, bol_estatus, cod_empleado_fk, imp_sueldoimss, imp_honorarios, imp_finiquito, cod_tipoimss, cod_tipohonorarios, cod_banco, cod_sucursal, cod_cuenta, txt_descripcionbaja, aud_codcreadopor, aud_feccreacion, aud_codmodificadopor, aud_fecmodificacion, cod_clabe) VALUES (12, '2019-05-07', NULL, true, 12, 123.00, 213.00, 123.00, '1', 'o', '123', 321, '12345', 'baja', 1, '2190-12-21', 1, '1990-12-21', '12345');
INSERT INTO sgnom.tsgnomempleados (cod_empleadoid, fec_ingreso, fec_salida, bol_estatus, cod_empleado_fk, imp_sueldoimss, imp_honorarios, imp_finiquito, cod_tipoimss, cod_tipohonorarios, cod_banco, cod_sucursal, cod_cuenta, txt_descripcionbaja, aud_codcreadopor, aud_feccreacion, aud_codmodificadopor, aud_fecmodificacion, cod_clabe) VALUES (15, '2019-05-07', '2019-05-10', true, 15, 123.00, 1000.00, 123.00, '1', 'o', '123', 321, '12345', 'baja', 1, '2190-12-21', 1, '1990-12-21', '12345');
INSERT INTO sgnom.tsgnomempleados (cod_empleadoid, fec_ingreso, fec_salida, bol_estatus, cod_empleado_fk, imp_sueldoimss, imp_honorarios, imp_finiquito, cod_tipoimss, cod_tipohonorarios, cod_banco, cod_sucursal, cod_cuenta, txt_descripcionbaja, aud_codcreadopor, aud_feccreacion, aud_codmodificadopor, aud_fecmodificacion, cod_clabe) VALUES (21, '2019-05-07', '2019-05-10', true, 21, 123.00, 2000.00, 123.00, '1', 'o', '123', 321, '12345', 'baja', 1, '2190-12-21', 1, '1990-12-21', '12345');
INSERT INTO sgnom.tsgnomempleados (cod_empleadoid, fec_ingreso, fec_salida, bol_estatus, cod_empleado_fk, imp_sueldoimss, imp_honorarios, imp_finiquito, cod_tipoimss, cod_tipohonorarios, cod_banco, cod_sucursal, cod_cuenta, txt_descripcionbaja, aud_codcreadopor, aud_feccreacion, aud_codmodificadopor, aud_fecmodificacion, cod_clabe) VALUES (17, '2019-05-07', '2019-05-10', true, 17, 123.00, 2213.00, 123.00, '1', 'o', '123', 321, '12345', 'baja', 1, '2190-12-21', 1, '1990-12-21', '12345');
INSERT INTO sgnom.tsgnomempleados (cod_empleadoid, fec_ingreso, fec_salida, bol_estatus, cod_empleado_fk, imp_sueldoimss, imp_honorarios, imp_finiquito, cod_tipoimss, cod_tipohonorarios, cod_banco, cod_sucursal, cod_cuenta, txt_descripcionbaja, aud_codcreadopor, aud_feccreacion, aud_codmodificadopor, aud_fecmodificacion, cod_clabe) VALUES (20, '2019-05-07', '2019-07-15', true, 20, 123.00, 2000.00, 123.00, '1', 'o', '123', 321, '12345', 'baja', 1, '2190-12-21', 1, '1990-12-21', '12345');
INSERT INTO sgnom.tsgnomempleados (cod_empleadoid, fec_ingreso, fec_salida, bol_estatus, cod_empleado_fk, imp_sueldoimss, imp_honorarios, imp_finiquito, cod_tipoimss, cod_tipohonorarios, cod_banco, cod_sucursal, cod_cuenta, txt_descripcionbaja, aud_codcreadopor, aud_feccreacion, aud_codmodificadopor, aud_fecmodificacion, cod_clabe) VALUES (11, '2019-05-07', '2019-08-10', true, 11, 123.00, 8200.00, 123.00, '1', 'o', '123', 321, '12345', 'baja', 1, '2190-12-21', 1, '1990-12-21', '12345');
INSERT INTO sgnom.tsgnomempleados (cod_empleadoid, fec_ingreso, fec_salida, bol_estatus, cod_empleado_fk, imp_sueldoimss, imp_honorarios, imp_finiquito, cod_tipoimss, cod_tipohonorarios, cod_banco, cod_sucursal, cod_cuenta, txt_descripcionbaja, aud_codcreadopor, aud_feccreacion, aud_codmodificadopor, aud_fecmodificacion, cod_clabe) VALUES (10, '2019-05-07', '2019-07-10', false, 10, 123.00, 20000.00, 123.00, '1', 'o', '123', 321, '12345', 'baja', 1, '2190-12-21', 1, '1990-12-21', '12345');
INSERT INTO sgnom.tsgnomempleados (cod_empleadoid, fec_ingreso, fec_salida, bol_estatus, cod_empleado_fk, imp_sueldoimss, imp_honorarios, imp_finiquito, cod_tipoimss, cod_tipohonorarios, cod_banco, cod_sucursal, cod_cuenta, txt_descripcionbaja, aud_codcreadopor, aud_feccreacion, aud_codmodificadopor, aud_fecmodificacion, cod_clabe) VALUES (19, '2019-05-07', '2019-07-12', true, 19, 10000.00, 1000.00, 123.00, '1', 'o', '123', 321, '12345', 'baja', 1, '2190-12-21', 1, '1990-12-21', '12345');
INSERT INTO sgnom.tsgnomempleados (cod_empleadoid, fec_ingreso, fec_salida, bol_estatus, cod_empleado_fk, imp_sueldoimss, imp_honorarios, imp_finiquito, cod_tipoimss, cod_tipohonorarios, cod_banco, cod_sucursal, cod_cuenta, txt_descripcionbaja, aud_codcreadopor, aud_feccreacion, aud_codmodificadopor, aud_fecmodificacion, cod_clabe) VALUES (1, '2019-05-07', NULL, true, 1, 12333.00, 5000.00, 123.00, '1', 'o', '123', 321, '12345', 'baja', 1, '2190-12-21', 1, '1990-12-21', '12345');


--
-- TOC entry 4646 (class 0 OID 20936)
-- Dependencies: 363
-- Data for Name: tsgnomempquincena; Type: TABLE DATA; Schema: sgnom; Owner: suite
--

INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (65, 1, 16, 2500.00, 0.00, 2500.00, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (66, 11, 16, 4100.00, 0.00, 4100.00, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (67, 12, 16, 106.50, 0.00, 106.50, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (68, 14, 16, 106.50, 0.00, 106.50, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (69, 16, 16, 106.50, 0.00, 106.50, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (70, 19, 16, 400.00, 0.00, 400.00, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (71, 20, 16, 1000.00, 0.00, 1000.00, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (79, 1, 17, 2500.00, 0.00, 2500.00, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (80, 11, 17, 4100.00, 0.00, 4100.00, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (81, 12, 17, 0.00, 0.00, 0.00, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (82, 14, 17, 0.00, 0.00, 0.00, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (83, 16, 17, 0.00, 0.00, 0.00, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (84, 19, 17, 0.00, 0.00, 0.00, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (85, 20, 17, 0.00, 0.00, 0.00, true);


--
-- TOC entry 4647 (class 0 OID 20939)
-- Dependencies: 364
-- Data for Name: tsgnomempquincenaht; Type: TABLE DATA; Schema: sgnom; Owner: suite
--



--
-- TOC entry 4648 (class 0 OID 20942)
-- Dependencies: 365
-- Data for Name: tsgnomestatusnom; Type: TABLE DATA; Schema: sgnom; Owner: suite
--

INSERT INTO sgnom.tsgnomestatusnom (cod_estatusnomid, cod_estatusnomina, bol_estatus) VALUES (2, 'calculada', true);
INSERT INTO sgnom.tsgnomestatusnom (cod_estatusnomid, cod_estatusnomina, bol_estatus) VALUES (1, 'abierta', true);


--
-- TOC entry 4649 (class 0 OID 20945)
-- Dependencies: 366
-- Data for Name: tsgnomformula; Type: TABLE DATA; Schema: sgnom; Owner: suite
--

INSERT INTO sgnom.tsgnomformula (cod_formulaid, des_nbformula, des_formula, bol_estatus) VALUES (1, 'sueldo', ':(:sueldo:/:diasM:):*:diasT', true);
INSERT INTO sgnom.tsgnomformula (cod_formulaid, des_nbformula, des_formula, bol_estatus) VALUES (2, 'test', ':d:*:diasM', true);
INSERT INTO sgnom.tsgnomformula (cod_formulaid, des_nbformula, des_formula, bol_estatus) VALUES (7, 'ukunb', ':diasM:/:diasP', false);
INSERT INTO sgnom.tsgnomformula (cod_formulaid, des_nbformula, des_formula, bol_estatus) VALUES (8, 'sueldo', ':(:diasM:/:d:):*:diasP', false);
INSERT INTO sgnom.tsgnomformula (cod_formulaid, des_nbformula, des_formula, bol_estatus) VALUES (10, 'Sueldo Quin 2', ':(:SB:/:DM:):*:DL', true);
INSERT INTO sgnom.tsgnomformula (cod_formulaid, des_nbformula, des_formula, bol_estatus) VALUES (11, 'q', ':dias del mes:*:dias laborados', false);
INSERT INTO sgnom.tsgnomformula (cod_formulaid, des_nbformula, des_formula, bol_estatus) VALUES (12, 'e', ':dias del mes:*:dias laborados', false);
INSERT INTO sgnom.tsgnomformula (cod_formulaid, des_nbformula, des_formula, bol_estatus) VALUES (13, 'r', ':dias del mes:*:dias laborados', false);
INSERT INTO sgnom.tsgnomformula (cod_formulaid, des_nbformula, des_formula, bol_estatus) VALUES (9, 'SUELDO QUINCENA', ':(:Sueldo Base:/:dias del mes:):*:dias laborados', true);


--
-- TOC entry 4650 (class 0 OID 20948)
-- Dependencies: 367
-- Data for Name: tsgnomfuncion; Type: TABLE DATA; Schema: sgnom; Owner: suite
--

INSERT INTO sgnom.tsgnomfuncion (cod_funcionid, cod_nbfuncion, bol_estatus) VALUES (1, 'fn_sueldo_base', true);
INSERT INTO sgnom.tsgnomfuncion (cod_funcionid, cod_nbfuncion, bol_estatus) VALUES (2, 'fn_dias_laborad', true);


--
-- TOC entry 4651 (class 0 OID 20951)
-- Dependencies: 368
-- Data for Name: tsgnomhisttabla; Type: TABLE DATA; Schema: sgnom; Owner: suite
--



--
-- TOC entry 4652 (class 0 OID 20954)
-- Dependencies: 369
-- Data for Name: tsgnomincidencia; Type: TABLE DATA; Schema: sgnom; Owner: suite
--



--
-- TOC entry 4653 (class 0 OID 20960)
-- Dependencies: 370
-- Data for Name: tsgnommanterceros; Type: TABLE DATA; Schema: sgnom; Owner: suite
--



--
-- TOC entry 4654 (class 0 OID 20963)
-- Dependencies: 371
-- Data for Name: tsgnomquincena; Type: TABLE DATA; Schema: sgnom; Owner: suite
--

INSERT INTO sgnom.tsgnomquincena (cod_quincenaid, des_quincena, fec_inicio, fec_fin, fec_pago, fec_dispersion, cnu_numquincena, cod_ejercicioid_fk, bol_estatus) VALUES (1, '1era de enero', '2019-01-01', '2019-01-15', '2019-01-01', '2019-01-15', 1, 6, true);
INSERT INTO sgnom.tsgnomquincena (cod_quincenaid, des_quincena, fec_inicio, fec_fin, fec_pago, fec_dispersion, cnu_numquincena, cod_ejercicioid_fk, bol_estatus) VALUES (2, '2da de enero', '2019-01-16', '2019-01-31', '2019-01-16', '2019-01-31', 2, 6, true);
INSERT INTO sgnom.tsgnomquincena (cod_quincenaid, des_quincena, fec_inicio, fec_fin, fec_pago, fec_dispersion, cnu_numquincena, cod_ejercicioid_fk, bol_estatus) VALUES (3, '1era de febrero', '2019-02-01', '2019-02-15', '2019-02-01', '2019-02-15', 3, 6, true);
INSERT INTO sgnom.tsgnomquincena (cod_quincenaid, des_quincena, fec_inicio, fec_fin, fec_pago, fec_dispersion, cnu_numquincena, cod_ejercicioid_fk, bol_estatus) VALUES (4, '2da de febrero', '2019-02-16', '2019-02-28', '2019-02-16', '2019-02-28', 4, 6, true);
INSERT INTO sgnom.tsgnomquincena (cod_quincenaid, des_quincena, fec_inicio, fec_fin, fec_pago, fec_dispersion, cnu_numquincena, cod_ejercicioid_fk, bol_estatus) VALUES (5, '1era de marzo', '2019-03-01', '2019-03-15', '2019-03-01', '2019-03-15', 5, 6, true);
INSERT INTO sgnom.tsgnomquincena (cod_quincenaid, des_quincena, fec_inicio, fec_fin, fec_pago, fec_dispersion, cnu_numquincena, cod_ejercicioid_fk, bol_estatus) VALUES (6, '2da de marzo', '2019-03-16', '2019-03-31', '2019-03-16', '2019-03-31', 6, 6, true);
INSERT INTO sgnom.tsgnomquincena (cod_quincenaid, des_quincena, fec_inicio, fec_fin, fec_pago, fec_dispersion, cnu_numquincena, cod_ejercicioid_fk, bol_estatus) VALUES (7, '1era de abril', '2019-04-01', '2019-04-15', '2019-04-01', '2019-04-15', 7, 6, true);
INSERT INTO sgnom.tsgnomquincena (cod_quincenaid, des_quincena, fec_inicio, fec_fin, fec_pago, fec_dispersion, cnu_numquincena, cod_ejercicioid_fk, bol_estatus) VALUES (8, '2da de abril', '2019-04-16', '2019-04-30', '2019-04-16', '2019-04-30', 8, 6, true);
INSERT INTO sgnom.tsgnomquincena (cod_quincenaid, des_quincena, fec_inicio, fec_fin, fec_pago, fec_dispersion, cnu_numquincena, cod_ejercicioid_fk, bol_estatus) VALUES (9, '1era de mayo', '2019-05-01', '2019-05-15', '2019-05-01', '2019-05-15', 9, 6, true);
INSERT INTO sgnom.tsgnomquincena (cod_quincenaid, des_quincena, fec_inicio, fec_fin, fec_pago, fec_dispersion, cnu_numquincena, cod_ejercicioid_fk, bol_estatus) VALUES (10, '2da de mayo', '2019-05-16', '2019-05-31', '2019-05-16', '2019-05-31', 10, 6, true);
INSERT INTO sgnom.tsgnomquincena (cod_quincenaid, des_quincena, fec_inicio, fec_fin, fec_pago, fec_dispersion, cnu_numquincena, cod_ejercicioid_fk, bol_estatus) VALUES (11, '1era de junio', '2019-06-01', '2019-06-15', '2019-06-01', '2019-06-15', 11, 6, true);
INSERT INTO sgnom.tsgnomquincena (cod_quincenaid, des_quincena, fec_inicio, fec_fin, fec_pago, fec_dispersion, cnu_numquincena, cod_ejercicioid_fk, bol_estatus) VALUES (12, '2da de junio', '2019-06-16', '2019-06-30', '2019-06-16', '2019-06-30', 12, 6, true);
INSERT INTO sgnom.tsgnomquincena (cod_quincenaid, des_quincena, fec_inicio, fec_fin, fec_pago, fec_dispersion, cnu_numquincena, cod_ejercicioid_fk, bol_estatus) VALUES (13, '1era de julio', '2019-07-01', '2019-07-15', '2019-07-01', '2019-07-15', 13, 6, true);
INSERT INTO sgnom.tsgnomquincena (cod_quincenaid, des_quincena, fec_inicio, fec_fin, fec_pago, fec_dispersion, cnu_numquincena, cod_ejercicioid_fk, bol_estatus) VALUES (14, '2da de julio', '2019-07-16', '2019-07-31', '2019-07-16', '2019-07-31', 14, 6, true);
INSERT INTO sgnom.tsgnomquincena (cod_quincenaid, des_quincena, fec_inicio, fec_fin, fec_pago, fec_dispersion, cnu_numquincena, cod_ejercicioid_fk, bol_estatus) VALUES (15, '1era de agosto', '2019-08-01', '2019-08-15', '2019-08-01', '2019-08-15', 15, 6, true);
INSERT INTO sgnom.tsgnomquincena (cod_quincenaid, des_quincena, fec_inicio, fec_fin, fec_pago, fec_dispersion, cnu_numquincena, cod_ejercicioid_fk, bol_estatus) VALUES (16, '2da de agosto', '2019-08-16', '2019-08-31', '2019-08-16', '2019-08-31', 16, 6, true);
INSERT INTO sgnom.tsgnomquincena (cod_quincenaid, des_quincena, fec_inicio, fec_fin, fec_pago, fec_dispersion, cnu_numquincena, cod_ejercicioid_fk, bol_estatus) VALUES (17, '1era de septiembre', '2019-09-01', '2019-09-15', '2019-09-01', '2019-09-15', 17, 6, true);
INSERT INTO sgnom.tsgnomquincena (cod_quincenaid, des_quincena, fec_inicio, fec_fin, fec_pago, fec_dispersion, cnu_numquincena, cod_ejercicioid_fk, bol_estatus) VALUES (18, '2da de septiembre', '2019-09-16', '2019-09-30', '2019-09-16', '2019-09-30', 18, 6, true);
INSERT INTO sgnom.tsgnomquincena (cod_quincenaid, des_quincena, fec_inicio, fec_fin, fec_pago, fec_dispersion, cnu_numquincena, cod_ejercicioid_fk, bol_estatus) VALUES (19, '1era de octubre', '2019-10-01', '2019-10-15', '2019-10-01', '2019-10-15', 19, 6, true);
INSERT INTO sgnom.tsgnomquincena (cod_quincenaid, des_quincena, fec_inicio, fec_fin, fec_pago, fec_dispersion, cnu_numquincena, cod_ejercicioid_fk, bol_estatus) VALUES (20, '2da de octubre', '2019-10-16', '2019-10-31', '2019-10-16', '2019-10-31', 20, 6, true);
INSERT INTO sgnom.tsgnomquincena (cod_quincenaid, des_quincena, fec_inicio, fec_fin, fec_pago, fec_dispersion, cnu_numquincena, cod_ejercicioid_fk, bol_estatus) VALUES (21, '1era de noviembre', '2019-11-01', '2019-11-15', '2019-11-01', '2019-11-15', 21, 6, true);
INSERT INTO sgnom.tsgnomquincena (cod_quincenaid, des_quincena, fec_inicio, fec_fin, fec_pago, fec_dispersion, cnu_numquincena, cod_ejercicioid_fk, bol_estatus) VALUES (22, '2da de noviembre', '2019-11-16', '2019-11-30', '2019-11-16', '2019-11-30', 22, 6, true);
INSERT INTO sgnom.tsgnomquincena (cod_quincenaid, des_quincena, fec_inicio, fec_fin, fec_pago, fec_dispersion, cnu_numquincena, cod_ejercicioid_fk, bol_estatus) VALUES (23, '1era de diciembre', '2019-12-01', '2019-12-15', '2019-12-01', '2019-12-15', 23, 6, true);
INSERT INTO sgnom.tsgnomquincena (cod_quincenaid, des_quincena, fec_inicio, fec_fin, fec_pago, fec_dispersion, cnu_numquincena, cod_ejercicioid_fk, bol_estatus) VALUES (24, '2da de diciembre', '2019-12-16', '2019-12-31', '2019-12-16', '2019-12-31', 24, 6, true);


--
-- TOC entry 4655 (class 0 OID 20966)
-- Dependencies: 372
-- Data for Name: tsgnomtipoconcepto; Type: TABLE DATA; Schema: sgnom; Owner: suite
--

INSERT INTO sgnom.tsgnomtipoconcepto (cod_tipoconceptoid, cod_tipoconcepto, bol_estatus) VALUES (1, 'Deducción', true);
INSERT INTO sgnom.tsgnomtipoconcepto (cod_tipoconceptoid, cod_tipoconcepto, bol_estatus) VALUES (2, 'Percepción', true);


--
-- TOC entry 4656 (class 0 OID 20969)
-- Dependencies: 373
-- Data for Name: tsgnomtiponomina; Type: TABLE DATA; Schema: sgnom; Owner: suite
--

INSERT INTO sgnom.tsgnomtiponomina (cod_tiponominaid, cod_nomina, bol_estatus) VALUES (1, 'nom1', true);


--
-- TOC entry 4592 (class 0 OID 18879)
-- Dependencies: 309
-- Data for Name: tsgrhareas; Type: TABLE DATA; Schema: sgrh; Owner: postgres
--

INSERT INTO sgrh.tsgrhareas (cod_area, des_nbarea, cod_acronimo, cnu_activo, cod_sistemasuite, cod_creadopor, cod_modificadopor, fec_creacion, fec_modificacion) VALUES (3, 'DISEÑO', 'DS', true, 1, 10, NULL, '2019-01-01', NULL);
INSERT INTO sgrh.tsgrhareas (cod_area, des_nbarea, cod_acronimo, cnu_activo, cod_sistemasuite, cod_creadopor, cod_modificadopor, fec_creacion, fec_modificacion) VALUES (1, 'BASE DE DATOS', 'DB', true, 2, 10, NULL, '2019-01-01', NULL);
INSERT INTO sgrh.tsgrhareas (cod_area, des_nbarea, cod_acronimo, cnu_activo, cod_sistemasuite, cod_creadopor, cod_modificadopor, fec_creacion, fec_modificacion) VALUES (4, 'SOPORTE TECNICO', 'ST', true, 1, 10, NULL, '2019-01-01', NULL);
INSERT INTO sgrh.tsgrhareas (cod_area, des_nbarea, cod_acronimo, cnu_activo, cod_sistemasuite, cod_creadopor, cod_modificadopor, fec_creacion, fec_modificacion) VALUES (2, 'FABRICA DE SOFTWARE', 'FS', true, 1, 10, NULL, '2019-01-01', NULL);
INSERT INTO sgrh.tsgrhareas (cod_area, des_nbarea, cod_acronimo, cnu_activo, cod_sistemasuite, cod_creadopor, cod_modificadopor, fec_creacion, fec_modificacion) VALUES (6, 'Marketing', 'mrg', true, 2, 10, NULL, '2019-01-01', NULL);
INSERT INTO sgrh.tsgrhareas (cod_area, des_nbarea, cod_acronimo, cnu_activo, cod_sistemasuite, cod_creadopor, cod_modificadopor, fec_creacion, fec_modificacion) VALUES (5, 'RECURSOS HUMANOS', 'RH', true, 1, 10, NULL, '2019-01-01', NULL);


--
-- TOC entry 4593 (class 0 OID 18883)
-- Dependencies: 310
-- Data for Name: tsgrhasignacionesemp; Type: TABLE DATA; Schema: sgrh; Owner: postgres
--



--
-- TOC entry 4594 (class 0 OID 18886)
-- Dependencies: 311
-- Data for Name: tsgrhcapacitaciones; Type: TABLE DATA; Schema: sgrh; Owner: postgres
--



--
-- TOC entry 4595 (class 0 OID 18893)
-- Dependencies: 312
-- Data for Name: tsgrhcartaasignacion; Type: TABLE DATA; Schema: sgrh; Owner: postgres
--



--
-- TOC entry 4596 (class 0 OID 18902)
-- Dependencies: 313
-- Data for Name: tsgrhcatrespuestas; Type: TABLE DATA; Schema: sgrh; Owner: postgres
--

INSERT INTO sgrh.tsgrhcatrespuestas (cod_catrespuesta, des_respuesta, cod_ponderacion) VALUES (1, 'Nunca', 1);
INSERT INTO sgrh.tsgrhcatrespuestas (cod_catrespuesta, des_respuesta, cod_ponderacion) VALUES (2, 'Algunas veces', 2);
INSERT INTO sgrh.tsgrhcatrespuestas (cod_catrespuesta, des_respuesta, cod_ponderacion) VALUES (3, 'Regular', 3);
INSERT INTO sgrh.tsgrhcatrespuestas (cod_catrespuesta, des_respuesta, cod_ponderacion) VALUES (4, 'Con frecuencia', 4);
INSERT INTO sgrh.tsgrhcatrespuestas (cod_catrespuesta, des_respuesta, cod_ponderacion) VALUES (5, 'Siempre', 5);
INSERT INTO sgrh.tsgrhcatrespuestas (cod_catrespuesta, des_respuesta, cod_ponderacion) VALUES (6, 'Muy malo', 1);
INSERT INTO sgrh.tsgrhcatrespuestas (cod_catrespuesta, des_respuesta, cod_ponderacion) VALUES (7, 'Malo', 2);
INSERT INTO sgrh.tsgrhcatrespuestas (cod_catrespuesta, des_respuesta, cod_ponderacion) VALUES (8, 'Bueno', 4);
INSERT INTO sgrh.tsgrhcatrespuestas (cod_catrespuesta, des_respuesta, cod_ponderacion) VALUES (9, 'Muy bueno', 5);
INSERT INTO sgrh.tsgrhcatrespuestas (cod_catrespuesta, des_respuesta, cod_ponderacion) VALUES (10, 'Muy bajo', 1);
INSERT INTO sgrh.tsgrhcatrespuestas (cod_catrespuesta, des_respuesta, cod_ponderacion) VALUES (11, 'Bajo', 2);
INSERT INTO sgrh.tsgrhcatrespuestas (cod_catrespuesta, des_respuesta, cod_ponderacion) VALUES (12, 'Alto', 3);
INSERT INTO sgrh.tsgrhcatrespuestas (cod_catrespuesta, des_respuesta, cod_ponderacion) VALUES (13, 'Muy alto', 4);
INSERT INTO sgrh.tsgrhcatrespuestas (cod_catrespuesta, des_respuesta, cod_ponderacion) VALUES (14, 'Muy incómodo', 1);
INSERT INTO sgrh.tsgrhcatrespuestas (cod_catrespuesta, des_respuesta, cod_ponderacion) VALUES (15, 'Incómodo', 2);
INSERT INTO sgrh.tsgrhcatrespuestas (cod_catrespuesta, des_respuesta, cod_ponderacion) VALUES (16, 'Soportable', 3);
INSERT INTO sgrh.tsgrhcatrespuestas (cod_catrespuesta, des_respuesta, cod_ponderacion) VALUES (17, 'Confortable', 4);
INSERT INTO sgrh.tsgrhcatrespuestas (cod_catrespuesta, des_respuesta, cod_ponderacion) VALUES (18, 'Muy confortable', 5);


--
-- TOC entry 4597 (class 0 OID 18906)
-- Dependencies: 314
-- Data for Name: tsgrhclientes; Type: TABLE DATA; Schema: sgrh; Owner: postgres
--

INSERT INTO sgrh.tsgrhclientes (cod_cliente, des_nbcliente, des_direccioncte, des_nbcontactocte, des_correocte, cod_telefonocte) VALUES (1, 'Pemex', 'Tamaulipas', 'Samuel Velzaco', 'samuel-@hotmail.com', '5578457687');
INSERT INTO sgrh.tsgrhclientes (cod_cliente, des_nbcliente, des_direccioncte, des_nbcontactocte, des_correocte, cod_telefonocte) VALUES (2, 'Cable vision', 'Mexico', 'Alfredo Gomez', 'alfgom-@hotmail.com', '5578457687');
INSERT INTO sgrh.tsgrhclientes (cod_cliente, des_nbcliente, des_direccioncte, des_nbcontactocte, des_correocte, cod_telefonocte) VALUES (3, 'Telecom', 'Mexico', 'Miguel Romero', 'romero@gmail.com', '55344344334');
INSERT INTO sgrh.tsgrhclientes (cod_cliente, des_nbcliente, des_direccioncte, des_nbcontactocte, des_correocte, cod_telefonocte) VALUES (4, 'Bancomer', 'Mexico', 'Jose Mauro', 'jose@bbva.com', '55344344334');
INSERT INTO sgrh.tsgrhclientes (cod_cliente, des_nbcliente, des_direccioncte, des_nbcontactocte, des_correocte, cod_telefonocte) VALUES (5, 'Nuevo', 'conocido', 'josue', 'dfd2@nuevo.com', '556322');


--
-- TOC entry 4598 (class 0 OID 18911)
-- Dependencies: 315
-- Data for Name: tsgrhcontrataciones; Type: TABLE DATA; Schema: sgrh; Owner: postgres
--



--
-- TOC entry 4599 (class 0 OID 18917)
-- Dependencies: 316
-- Data for Name: tsgrhcontratos; Type: TABLE DATA; Schema: sgrh; Owner: postgres
--



--
-- TOC entry 4600 (class 0 OID 18923)
-- Dependencies: 317
-- Data for Name: tsgrhempleados; Type: TABLE DATA; Schema: sgrh; Owner: postgres
--

INSERT INTO sgrh.tsgrhempleados (cod_empleado, des_nombre, des_nombres, des_apepaterno, des_apematerno, des_direccion, fec_nacimiento, des_lugarnacimiento, cod_edad, des_correo, cod_tiposangre, cod_telefonocasa, cod_telefonocelular, cod_telemergencia, bin_identificacion, bin_pasaporte, bin_visa, cod_licenciamanejo, fec_ingreso, cod_rfc, cod_nss, cod_curp, bin_foto, cod_tipofoto, cod_extensionfoto, cod_empleadoactivo, cod_estatusempleado, cod_estadocivil, cod_rol, cod_puesto, cod_diasvacaciones, cod_sistemasuite, fec_creacion, fec_modificacion, cod_creadopor, cod_modificadopor, cod_area) VALUES (10, 'MATEO', NULL, 'RODRIGUEZ', 'JUAREZ', 'DOMICILIO CONOCIDO', '1996-04-09', 'MECATLAN, VER.', 23, 'mateorj96@gmail.com', 'O+', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-14', NULL, NULL, 'ROJM960409HPZDAT22', NULL, NULL, NULL, true, 1, 0, NULL, 1, NULL, NULL, '2018-12-01', '2018-12-01', NULL, NULL, 1);
INSERT INTO sgrh.tsgrhempleados (cod_empleado, des_nombre, des_nombres, des_apepaterno, des_apematerno, des_direccion, fec_nacimiento, des_lugarnacimiento, cod_edad, des_correo, cod_tiposangre, cod_telefonocasa, cod_telefonocelular, cod_telemergencia, bin_identificacion, bin_pasaporte, bin_visa, cod_licenciamanejo, fec_ingreso, cod_rfc, cod_nss, cod_curp, bin_foto, cod_tipofoto, cod_extensionfoto, cod_empleadoactivo, cod_estatusempleado, cod_estadocivil, cod_rol, cod_puesto, cod_diasvacaciones, cod_sistemasuite, fec_creacion, fec_modificacion, cod_creadopor, cod_modificadopor, cod_area) VALUES (11, 'ADRIAN', NULL, 'SUAREZ', 'DE LA CRUZ', 'DOMICILIO CONOCIDO', '1996-06-14', 'AHUACATLAN, PUE.', 23, 'adrian.suarezc@gmail.com', 'O+', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-14', NULL, NULL, 'SDCA960614HPZDBT23', NULL, NULL, NULL, true, 1, 0, NULL, 1, NULL, NULL, '2018-12-01', '2018-12-01', 10, NULL, 1);
INSERT INTO sgrh.tsgrhempleados (cod_empleado, des_nombre, des_nombres, des_apepaterno, des_apematerno, des_direccion, fec_nacimiento, des_lugarnacimiento, cod_edad, des_correo, cod_tiposangre, cod_telefonocasa, cod_telefonocelular, cod_telemergencia, bin_identificacion, bin_pasaporte, bin_visa, cod_licenciamanejo, fec_ingreso, cod_rfc, cod_nss, cod_curp, bin_foto, cod_tipofoto, cod_extensionfoto, cod_empleadoactivo, cod_estatusempleado, cod_estadocivil, cod_rol, cod_puesto, cod_diasvacaciones, cod_sistemasuite, fec_creacion, fec_modificacion, cod_creadopor, cod_modificadopor, cod_area) VALUES (12, 'CARLOS', NULL, 'ANTONIO', 'TRINIDAD', 'DOMICILIO CONOCIDO', '1996-03-12', 'AHUACATLAN, PUE.', 23, 'trinidad.carlos@gmail.com', 'O+', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-14', NULL, NULL, 'ANTC960312HPZDCT24', NULL, NULL, NULL, true, 1, 0, NULL, 1, NULL, NULL, '2018-12-01', '2018-12-01', 10, NULL, 1);
INSERT INTO sgrh.tsgrhempleados (cod_empleado, des_nombre, des_nombres, des_apepaterno, des_apematerno, des_direccion, fec_nacimiento, des_lugarnacimiento, cod_edad, des_correo, cod_tiposangre, cod_telefonocasa, cod_telefonocelular, cod_telemergencia, bin_identificacion, bin_pasaporte, bin_visa, cod_licenciamanejo, fec_ingreso, cod_rfc, cod_nss, cod_curp, bin_foto, cod_tipofoto, cod_extensionfoto, cod_empleadoactivo, cod_estatusempleado, cod_estadocivil, cod_rol, cod_puesto, cod_diasvacaciones, cod_sistemasuite, fec_creacion, fec_modificacion, cod_creadopor, cod_modificadopor, cod_area) VALUES (13, 'ANGEL', 'ANTONIO', 'ROANO', 'ALVARADO', 'DOMICILIO CONOCIDO', '1995-02-19', 'TETELA, PUE.', 23, 'angel.antonio.roa@gmail.com', 'O+', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-14', NULL, NULL, 'ROAA950219HPZDDT25', NULL, NULL, NULL, true, 1, 0, NULL, 1, NULL, NULL, '2018-12-01', '2018-12-01', 10, NULL, 1);
INSERT INTO sgrh.tsgrhempleados (cod_empleado, des_nombre, des_nombres, des_apepaterno, des_apematerno, des_direccion, fec_nacimiento, des_lugarnacimiento, cod_edad, des_correo, cod_tiposangre, cod_telefonocasa, cod_telefonocelular, cod_telemergencia, bin_identificacion, bin_pasaporte, bin_visa, cod_licenciamanejo, fec_ingreso, cod_rfc, cod_nss, cod_curp, bin_foto, cod_tipofoto, cod_extensionfoto, cod_empleadoactivo, cod_estatusempleado, cod_estadocivil, cod_rol, cod_puesto, cod_diasvacaciones, cod_sistemasuite, fec_creacion, fec_modificacion, cod_creadopor, cod_modificadopor, cod_area) VALUES (14, 'JUAN', '', 'MARQUEZ', 'SAVEDO', 'DOMICILIO CONOCIDO', '1995-02-12', 'APIZACO, TLAX', 23, 'maito1.example@gmail.com', 'O-', '', '', '', NULL, NULL, NULL, '', '2019-02-14', '', '', 'MASJ950212HPZDET26', NULL, '', '', true, 1, 0, NULL, 1, NULL, NULL, '2019-02-14', '2019-02-14', 10, NULL, 1);
INSERT INTO sgrh.tsgrhempleados (cod_empleado, des_nombre, des_nombres, des_apepaterno, des_apematerno, des_direccion, fec_nacimiento, des_lugarnacimiento, cod_edad, des_correo, cod_tiposangre, cod_telefonocasa, cod_telefonocelular, cod_telemergencia, bin_identificacion, bin_pasaporte, bin_visa, cod_licenciamanejo, fec_ingreso, cod_rfc, cod_nss, cod_curp, bin_foto, cod_tipofoto, cod_extensionfoto, cod_empleadoactivo, cod_estatusempleado, cod_estadocivil, cod_rol, cod_puesto, cod_diasvacaciones, cod_sistemasuite, fec_creacion, fec_modificacion, cod_creadopor, cod_modificadopor, cod_area) VALUES (15, 'ANTONIO', '', 'HERRERA', 'CHAVEZ', 'DOMICILIO CONOCIDO', '1995-02-20', 'TLAXCALA, TLAX', 23, 'maito2.example@gmail.com', 'O-', '', '', '', NULL, NULL, NULL, '', '2019-02-14', '', '', 'HECA950220HPZDFT27', NULL, '', '', true, 1, 0, NULL, 2, NULL, NULL, '2019-02-14', '2019-02-14', 10, NULL, 2);
INSERT INTO sgrh.tsgrhempleados (cod_empleado, des_nombre, des_nombres, des_apepaterno, des_apematerno, des_direccion, fec_nacimiento, des_lugarnacimiento, cod_edad, des_correo, cod_tiposangre, cod_telefonocasa, cod_telefonocelular, cod_telemergencia, bin_identificacion, bin_pasaporte, bin_visa, cod_licenciamanejo, fec_ingreso, cod_rfc, cod_nss, cod_curp, bin_foto, cod_tipofoto, cod_extensionfoto, cod_empleadoactivo, cod_estatusempleado, cod_estadocivil, cod_rol, cod_puesto, cod_diasvacaciones, cod_sistemasuite, fec_creacion, fec_modificacion, cod_creadopor, cod_modificadopor, cod_area) VALUES (16, 'JAVIER', '', 'CHICHARITO', 'HERNANDEZ', 'DOMICILIO CONOCIDO', '1995-02-21', 'AHUCATLAN, PUE.', 23, 'maito3.example@gmail.com', 'O-', '', '', '', NULL, NULL, NULL, '', '2019-02-14', '', '', 'CHHJ950221HPZDGT28', NULL, '', '', true, 1, 0, NULL, 2, NULL, NULL, '2019-02-14', '2019-02-14', 10, NULL, 2);
INSERT INTO sgrh.tsgrhempleados (cod_empleado, des_nombre, des_nombres, des_apepaterno, des_apematerno, des_direccion, fec_nacimiento, des_lugarnacimiento, cod_edad, des_correo, cod_tiposangre, cod_telefonocasa, cod_telefonocelular, cod_telemergencia, bin_identificacion, bin_pasaporte, bin_visa, cod_licenciamanejo, fec_ingreso, cod_rfc, cod_nss, cod_curp, bin_foto, cod_tipofoto, cod_extensionfoto, cod_empleadoactivo, cod_estatusempleado, cod_estadocivil, cod_rol, cod_puesto, cod_diasvacaciones, cod_sistemasuite, fec_creacion, fec_modificacion, cod_creadopor, cod_modificadopor, cod_area) VALUES (17, 'MANUEL', '', 'GONZALEZ', 'PEREZ', 'DOMICILIO CONOCIDO', '1995-02-22', 'TEPANGO PUE.', 23, 'maito4.example@gmail.com', 'O-', '', '', '', NULL, NULL, NULL, '', '2019-02-14', '', '', 'GOPM950222HPZDHT29', NULL, '', '', true, 1, 0, NULL, 2, NULL, NULL, '2019-02-14', '2019-02-14', 10, NULL, 2);
INSERT INTO sgrh.tsgrhempleados (cod_empleado, des_nombre, des_nombres, des_apepaterno, des_apematerno, des_direccion, fec_nacimiento, des_lugarnacimiento, cod_edad, des_correo, cod_tiposangre, cod_telefonocasa, cod_telefonocelular, cod_telemergencia, bin_identificacion, bin_pasaporte, bin_visa, cod_licenciamanejo, fec_ingreso, cod_rfc, cod_nss, cod_curp, bin_foto, cod_tipofoto, cod_extensionfoto, cod_empleadoactivo, cod_estatusempleado, cod_estadocivil, cod_rol, cod_puesto, cod_diasvacaciones, cod_sistemasuite, fec_creacion, fec_modificacion, cod_creadopor, cod_modificadopor, cod_area) VALUES (18, 'SERGIO', '', 'RAMOS', 'FLORES', 'DOMICILIO CONOCIDO', '1995-03-01', 'ZAPOTITLAN, PUE.', 23, 'maito5.example@gmail.com', 'B+', '', '', '', NULL, NULL, NULL, '', '2019-02-14', '', '', 'RAFS950301HPZDIT30', NULL, '', '', true, 1, 0, NULL, 2, NULL, NULL, '2019-02-14', '2019-02-14', 10, NULL, 2);
INSERT INTO sgrh.tsgrhempleados (cod_empleado, des_nombre, des_nombres, des_apepaterno, des_apematerno, des_direccion, fec_nacimiento, des_lugarnacimiento, cod_edad, des_correo, cod_tiposangre, cod_telefonocasa, cod_telefonocelular, cod_telemergencia, bin_identificacion, bin_pasaporte, bin_visa, cod_licenciamanejo, fec_ingreso, cod_rfc, cod_nss, cod_curp, bin_foto, cod_tipofoto, cod_extensionfoto, cod_empleadoactivo, cod_estatusempleado, cod_estadocivil, cod_rol, cod_puesto, cod_diasvacaciones, cod_sistemasuite, fec_creacion, fec_modificacion, cod_creadopor, cod_modificadopor, cod_area) VALUES (19, 'CARINE', '', 'BENZEMA', 'ALVAREZ', 'DOMICILIO CONOCIDO', '1995-03-02', 'AQUIXTLA, PUE.', 23, 'maito6.example@gmail.com', 'B+', '', '', '', NULL, NULL, NULL, '', '2019-02-14', '', '', 'BEAC950302HPZDJT31', NULL, '', '', true, 1, 0, NULL, 2, NULL, NULL, '2019-02-14', '2019-02-14', 10, NULL, 2);
INSERT INTO sgrh.tsgrhempleados (cod_empleado, des_nombre, des_nombres, des_apepaterno, des_apematerno, des_direccion, fec_nacimiento, des_lugarnacimiento, cod_edad, des_correo, cod_tiposangre, cod_telefonocasa, cod_telefonocelular, cod_telemergencia, bin_identificacion, bin_pasaporte, bin_visa, cod_licenciamanejo, fec_ingreso, cod_rfc, cod_nss, cod_curp, bin_foto, cod_tipofoto, cod_extensionfoto, cod_empleadoactivo, cod_estatusempleado, cod_estadocivil, cod_rol, cod_puesto, cod_diasvacaciones, cod_sistemasuite, fec_creacion, fec_modificacion, cod_creadopor, cod_modificadopor, cod_area) VALUES (20, 'CAROLINA', '', 'JIMENEZ', 'VAZQUEZ', 'DOMICILIO CONOCIDO', '1995-03-03', 'ZACATLAN, PUE.', 23, 'maito7.example@gmail.com', 'B+', '', '', '', NULL, NULL, NULL, '', '2019-02-14', '', '', 'JIVC950303HPZDKT32', NULL, '', '', true, 1, 0, NULL, 3, NULL, NULL, '2019-02-14', '2019-02-14', 10, NULL, 3);
INSERT INTO sgrh.tsgrhempleados (cod_empleado, des_nombre, des_nombres, des_apepaterno, des_apematerno, des_direccion, fec_nacimiento, des_lugarnacimiento, cod_edad, des_correo, cod_tiposangre, cod_telefonocasa, cod_telefonocelular, cod_telemergencia, bin_identificacion, bin_pasaporte, bin_visa, cod_licenciamanejo, fec_ingreso, cod_rfc, cod_nss, cod_curp, bin_foto, cod_tipofoto, cod_extensionfoto, cod_empleadoactivo, cod_estatusempleado, cod_estadocivil, cod_rol, cod_puesto, cod_diasvacaciones, cod_sistemasuite, fec_creacion, fec_modificacion, cod_creadopor, cod_modificadopor, cod_area) VALUES (21, 'VERONICA', '', 'SANCHEZ', 'TRINIDAD', 'DOMICILIO CONOCIDO', '1995-03-04', 'CHIGNAHUAPAN, PUE.', 23, 'maito8.example@gmail.com', 'B+', '', '', '', NULL, NULL, NULL, '', '2019-02-14', '', '', 'SATV950304HPZDLT33', NULL, '', '', true, 1, 0, NULL, 3, NULL, NULL, '2019-02-14', '2019-02-14', 10, NULL, 3);
INSERT INTO sgrh.tsgrhempleados (cod_empleado, des_nombre, des_nombres, des_apepaterno, des_apematerno, des_direccion, fec_nacimiento, des_lugarnacimiento, cod_edad, des_correo, cod_tiposangre, cod_telefonocasa, cod_telefonocelular, cod_telemergencia, bin_identificacion, bin_pasaporte, bin_visa, cod_licenciamanejo, fec_ingreso, cod_rfc, cod_nss, cod_curp, bin_foto, cod_tipofoto, cod_extensionfoto, cod_empleadoactivo, cod_estatusempleado, cod_estadocivil, cod_rol, cod_puesto, cod_diasvacaciones, cod_sistemasuite, fec_creacion, fec_modificacion, cod_creadopor, cod_modificadopor, cod_area) VALUES (22, 'HEIDY', '', 'TRINIDAD', 'MORELOS', 'DOMICILIO CONOCIDO', '1995-03-05', 'TLAXCO, TLAX.', 23, 'maito9.example@gmail.com', 'A+', '', '', '', NULL, NULL, NULL, '', '2019-02-14', '', '', 'TRMH950305HPZDMT34', NULL, '', '', true, 1, 0, NULL, 3, NULL, NULL, '2019-02-14', '2019-02-14', 10, NULL, 3);
INSERT INTO sgrh.tsgrhempleados (cod_empleado, des_nombre, des_nombres, des_apepaterno, des_apematerno, des_direccion, fec_nacimiento, des_lugarnacimiento, cod_edad, des_correo, cod_tiposangre, cod_telefonocasa, cod_telefonocelular, cod_telemergencia, bin_identificacion, bin_pasaporte, bin_visa, cod_licenciamanejo, fec_ingreso, cod_rfc, cod_nss, cod_curp, bin_foto, cod_tipofoto, cod_extensionfoto, cod_empleadoactivo, cod_estatusempleado, cod_estadocivil, cod_rol, cod_puesto, cod_diasvacaciones, cod_sistemasuite, fec_creacion, fec_modificacion, cod_creadopor, cod_modificadopor, cod_area) VALUES (23, 'JESUS', 'MIGUEL', 'VELAZCO', 'MARQUEZ', 'DOMICILIO CONOCIDO', '1995-03-06', 'POZA RICA, VER.', 23, 'maito10.example@gmail.com', 'A+', '', '', '', NULL, NULL, NULL, '', '2019-02-14', '', '', 'MVMJ950306HPZDNT35', NULL, '', '', true, 1, 0, NULL, 3, NULL, NULL, '2019-02-14', '2019-02-14', 10, NULL, 3);
INSERT INTO sgrh.tsgrhempleados (cod_empleado, des_nombre, des_nombres, des_apepaterno, des_apematerno, des_direccion, fec_nacimiento, des_lugarnacimiento, cod_edad, des_correo, cod_tiposangre, cod_telefonocasa, cod_telefonocelular, cod_telemergencia, bin_identificacion, bin_pasaporte, bin_visa, cod_licenciamanejo, fec_ingreso, cod_rfc, cod_nss, cod_curp, bin_foto, cod_tipofoto, cod_extensionfoto, cod_empleadoactivo, cod_estatusempleado, cod_estadocivil, cod_rol, cod_puesto, cod_diasvacaciones, cod_sistemasuite, fec_creacion, fec_modificacion, cod_creadopor, cod_modificadopor, cod_area) VALUES (24, 'RAUL', '', 'ESPINOZA', 'MARTINEZ', 'DOMICILIO CONOCIDO', '1995-03-07', 'XALAPA, VER.', 23, 'maito11.example@gmail.com', 'A+', '', '', '', NULL, NULL, NULL, '', '2019-02-14', '', '', 'ESMR950307HPZDOT36', NULL, '', '', true, 1, 0, NULL, 3, NULL, NULL, '2019-02-14', '2019-02-14', 10, NULL, 3);
INSERT INTO sgrh.tsgrhempleados (cod_empleado, des_nombre, des_nombres, des_apepaterno, des_apematerno, des_direccion, fec_nacimiento, des_lugarnacimiento, cod_edad, des_correo, cod_tiposangre, cod_telefonocasa, cod_telefonocelular, cod_telemergencia, bin_identificacion, bin_pasaporte, bin_visa, cod_licenciamanejo, fec_ingreso, cod_rfc, cod_nss, cod_curp, bin_foto, cod_tipofoto, cod_extensionfoto, cod_empleadoactivo, cod_estatusempleado, cod_estadocivil, cod_rol, cod_puesto, cod_diasvacaciones, cod_sistemasuite, fec_creacion, fec_modificacion, cod_creadopor, cod_modificadopor, cod_area) VALUES (25, 'JOSE', 'EDUARDO', 'TRINIDAD', 'MORELOS', 'DOMICILIO CONOCIDO', '1995-03-08', 'COAHUILA, COAH.', 23, 'maito12.example@gmail.com', 'A+', '', '', '', NULL, NULL, NULL, '', '2019-02-14', '', '', 'ETMJ950308HPZDPT37', NULL, '', '', true, 1, 0, NULL, 4, NULL, NULL, '2019-02-14', '2019-02-14', 10, NULL, 4);
INSERT INTO sgrh.tsgrhempleados (cod_empleado, des_nombre, des_nombres, des_apepaterno, des_apematerno, des_direccion, fec_nacimiento, des_lugarnacimiento, cod_edad, des_correo, cod_tiposangre, cod_telefonocasa, cod_telefonocelular, cod_telemergencia, bin_identificacion, bin_pasaporte, bin_visa, cod_licenciamanejo, fec_ingreso, cod_rfc, cod_nss, cod_curp, bin_foto, cod_tipofoto, cod_extensionfoto, cod_empleadoactivo, cod_estatusempleado, cod_estadocivil, cod_rol, cod_puesto, cod_diasvacaciones, cod_sistemasuite, fec_creacion, fec_modificacion, cod_creadopor, cod_modificadopor, cod_area) VALUES (26, 'SARAHI', '', 'GONZALEZ', 'SUAREZ', 'DOMICILIO CONOCIDO', '1995-03-09', 'HUACHINANGO, PUE.', 23, 'maito13.example@gmail.com', 'A+', '', '', '', NULL, NULL, NULL, '', '2019-02-14', '', '', 'GOSS950309HPZDQT38', NULL, '', '', true, 1, 0, NULL, 4, NULL, NULL, '2019-02-14', '2019-02-14', 10, NULL, 4);
INSERT INTO sgrh.tsgrhempleados (cod_empleado, des_nombre, des_nombres, des_apepaterno, des_apematerno, des_direccion, fec_nacimiento, des_lugarnacimiento, cod_edad, des_correo, cod_tiposangre, cod_telefonocasa, cod_telefonocelular, cod_telemergencia, bin_identificacion, bin_pasaporte, bin_visa, cod_licenciamanejo, fec_ingreso, cod_rfc, cod_nss, cod_curp, bin_foto, cod_tipofoto, cod_extensionfoto, cod_empleadoactivo, cod_estatusempleado, cod_estadocivil, cod_rol, cod_puesto, cod_diasvacaciones, cod_sistemasuite, fec_creacion, fec_modificacion, cod_creadopor, cod_modificadopor, cod_area) VALUES (27, 'FEDERICO', '', 'GUZMAN', 'TRINIDAD', 'DOMICILIO CONOCIDO', '1995-03-10', 'TULANCINGO, HID.', 23, 'maito14.example@gmail.com', 'A+', '', '', '', NULL, NULL, NULL, '', '2019-02-14', '', '', 'GUTF950310HPZDRT39', NULL, '', '', true, 1, 0, NULL, 4, NULL, NULL, '2019-02-14', '2019-02-14', 10, NULL, 4);
INSERT INTO sgrh.tsgrhempleados (cod_empleado, des_nombre, des_nombres, des_apepaterno, des_apematerno, des_direccion, fec_nacimiento, des_lugarnacimiento, cod_edad, des_correo, cod_tiposangre, cod_telefonocasa, cod_telefonocelular, cod_telemergencia, bin_identificacion, bin_pasaporte, bin_visa, cod_licenciamanejo, fec_ingreso, cod_rfc, cod_nss, cod_curp, bin_foto, cod_tipofoto, cod_extensionfoto, cod_empleadoactivo, cod_estatusempleado, cod_estadocivil, cod_rol, cod_puesto, cod_diasvacaciones, cod_sistemasuite, fec_creacion, fec_modificacion, cod_creadopor, cod_modificadopor, cod_area) VALUES (28, 'JOSE', 'IVAN', 'VACILIO', 'SANCHEZ', 'DOMICILIO CONOCIDO', '1995-03-11', 'XOXONANCATLA, PUE.', 23, 'maito15.example@gmail.com', 'B-', '', '', '', NULL, NULL, NULL, '', '2019-02-14', '', '', 'IVSJ950311HPZDTT40', NULL, '', '', true, 1, 0, NULL, 4, NULL, NULL, '2019-02-14', '2019-02-14', 10, NULL, 4);
INSERT INTO sgrh.tsgrhempleados (cod_empleado, des_nombre, des_nombres, des_apepaterno, des_apematerno, des_direccion, fec_nacimiento, des_lugarnacimiento, cod_edad, des_correo, cod_tiposangre, cod_telefonocasa, cod_telefonocelular, cod_telemergencia, bin_identificacion, bin_pasaporte, bin_visa, cod_licenciamanejo, fec_ingreso, cod_rfc, cod_nss, cod_curp, bin_foto, cod_tipofoto, cod_extensionfoto, cod_empleadoactivo, cod_estatusempleado, cod_estadocivil, cod_rol, cod_puesto, cod_diasvacaciones, cod_sistemasuite, fec_creacion, fec_modificacion, cod_creadopor, cod_modificadopor, cod_area) VALUES (29, 'ERICA', '', 'ESPINOZA', 'CANDELARIA', 'DOMICILIO CONOCIDO', '1995-03-12', 'XICOTEPEC, PUE.', 23, 'maito16.example@gmail.com', 'B-', '', '', '', NULL, NULL, NULL, '', '2019-02-14', '', '', 'ESCE950312HPZDUT41', NULL, '', '', true, 1, 0, NULL, 5, NULL, NULL, '2019-02-14', '2019-02-14', 10, NULL, 5);
INSERT INTO sgrh.tsgrhempleados (cod_empleado, des_nombre, des_nombres, des_apepaterno, des_apematerno, des_direccion, fec_nacimiento, des_lugarnacimiento, cod_edad, des_correo, cod_tiposangre, cod_telefonocasa, cod_telefonocelular, cod_telemergencia, bin_identificacion, bin_pasaporte, bin_visa, cod_licenciamanejo, fec_ingreso, cod_rfc, cod_nss, cod_curp, bin_foto, cod_tipofoto, cod_extensionfoto, cod_empleadoactivo, cod_estatusempleado, cod_estadocivil, cod_rol, cod_puesto, cod_diasvacaciones, cod_sistemasuite, fec_creacion, fec_modificacion, cod_creadopor, cod_modificadopor, cod_area) VALUES (30, 'ROBERTO', '', 'ORTEGA', 'ALVAREZ', 'DOMICILIO CONOCIDO', '1995-03-13', 'JICOLAPA, PUE.', 23, 'maito17.example@gmail.com', 'A-', '', '', '', NULL, NULL, NULL, '', '2019-02-14', '', '', 'ORAR950313HPZDVT42', NULL, '', '', true, 1, 0, NULL, 5, NULL, NULL, '2019-02-14', '2019-02-14', 10, NULL, 5);
INSERT INTO sgrh.tsgrhempleados (cod_empleado, des_nombre, des_nombres, des_apepaterno, des_apematerno, des_direccion, fec_nacimiento, des_lugarnacimiento, cod_edad, des_correo, cod_tiposangre, cod_telefonocasa, cod_telefonocelular, cod_telemergencia, bin_identificacion, bin_pasaporte, bin_visa, cod_licenciamanejo, fec_ingreso, cod_rfc, cod_nss, cod_curp, bin_foto, cod_tipofoto, cod_extensionfoto, cod_empleadoactivo, cod_estatusempleado, cod_estadocivil, cod_rol, cod_puesto, cod_diasvacaciones, cod_sistemasuite, fec_creacion, fec_modificacion, cod_creadopor, cod_modificadopor, cod_area) VALUES (31, 'CESARIO', '', 'TELLEZ', 'REYES', 'DOMICILIO CONOCIDO', '1995-03-14', 'SANTA INES, PUE.', 23, 'maito18.example@gmail.com', 'A-', '', '', '', NULL, NULL, NULL, '', '2019-02-14', '', '', 'TERC950314HPZDWT43', NULL, '', '', true, 1, 0, NULL, 5, NULL, NULL, '2019-02-14', '2019-02-14', 10, NULL, 5);
INSERT INTO sgrh.tsgrhempleados (cod_empleado, des_nombre, des_nombres, des_apepaterno, des_apematerno, des_direccion, fec_nacimiento, des_lugarnacimiento, cod_edad, des_correo, cod_tiposangre, cod_telefonocasa, cod_telefonocelular, cod_telemergencia, bin_identificacion, bin_pasaporte, bin_visa, cod_licenciamanejo, fec_ingreso, cod_rfc, cod_nss, cod_curp, bin_foto, cod_tipofoto, cod_extensionfoto, cod_empleadoactivo, cod_estatusempleado, cod_estadocivil, cod_rol, cod_puesto, cod_diasvacaciones, cod_sistemasuite, fec_creacion, fec_modificacion, cod_creadopor, cod_modificadopor, cod_area) VALUES (32, 'CARLOS', 'EFREN', 'SANCHEZ', 'JUAN', 'DOMICILIO CONOCIDO', '1995-03-15', 'CHOLULA, PUE.', 23, 'maito19.example@gmail.com', 'A-', '', '', '', NULL, NULL, NULL, '', '2019-02-14', '', '', 'ESJC950315HPZDXT44', NULL, '', '', true, 1, 0, NULL, 5, NULL, NULL, '2019-02-14', '2019-02-14', 10, NULL, 5);
INSERT INTO sgrh.tsgrhempleados (cod_empleado, des_nombre, des_nombres, des_apepaterno, des_apematerno, des_direccion, fec_nacimiento, des_lugarnacimiento, cod_edad, des_correo, cod_tiposangre, cod_telefonocasa, cod_telefonocelular, cod_telemergencia, bin_identificacion, bin_pasaporte, bin_visa, cod_licenciamanejo, fec_ingreso, cod_rfc, cod_nss, cod_curp, bin_foto, cod_tipofoto, cod_extensionfoto, cod_empleadoactivo, cod_estatusempleado, cod_estadocivil, cod_rol, cod_puesto, cod_diasvacaciones, cod_sistemasuite, fec_creacion, fec_modificacion, cod_creadopor, cod_modificadopor, cod_area) VALUES (33, 'JOSE', '', 'DE LOS SANTOS', 'TRINIDAD', 'DOMICILIO CONOCIDO', '1995-03-16', 'DF, DF.', 23, 'maito20.example@gmail.com', 'A-', '', '', '', NULL, NULL, NULL, '', '2019-02-14', '', '', 'DETJ950316HPZDYT45', NULL, '', '', true, 1, 0, NULL, 5, NULL, NULL, '2019-02-14', '2019-02-14', 10, NULL, 5);
INSERT INTO sgrh.tsgrhempleados (cod_empleado, des_nombre, des_nombres, des_apepaterno, des_apematerno, des_direccion, fec_nacimiento, des_lugarnacimiento, cod_edad, des_correo, cod_tiposangre, cod_telefonocasa, cod_telefonocelular, cod_telemergencia, bin_identificacion, bin_pasaporte, bin_visa, cod_licenciamanejo, fec_ingreso, cod_rfc, cod_nss, cod_curp, bin_foto, cod_tipofoto, cod_extensionfoto, cod_empleadoactivo, cod_estatusempleado, cod_estadocivil, cod_rol, cod_puesto, cod_diasvacaciones, cod_sistemasuite, fec_creacion, fec_modificacion, cod_creadopor, cod_modificadopor, cod_area) VALUES (34, 'FIDEL', '', 'SANCHEZ', 'VAZQUEZ', 'DOMICILIO CONOCIDO', '0022-01-17', 'ZACATELCO, TLAX.', 0, 'maito21.example@gmail.com', 'A-', '', '757581', '', NULL, NULL, NULL, '', '0012-08-19', 'dfgt45', '', 'SAVF950317HPZDZT46', NULL, '', '', true, 1, 1, 2, 5, NULL, NULL, '0012-08-19', '2019-04-11', 10, 11, 5);
INSERT INTO sgrh.tsgrhempleados (cod_empleado, des_nombre, des_nombres, des_apepaterno, des_apematerno, des_direccion, fec_nacimiento, des_lugarnacimiento, cod_edad, des_correo, cod_tiposangre, cod_telefonocasa, cod_telefonocelular, cod_telemergencia, bin_identificacion, bin_pasaporte, bin_visa, cod_licenciamanejo, fec_ingreso, cod_rfc, cod_nss, cod_curp, bin_foto, cod_tipofoto, cod_extensionfoto, cod_empleadoactivo, cod_estatusempleado, cod_estadocivil, cod_rol, cod_puesto, cod_diasvacaciones, cod_sistemasuite, fec_creacion, fec_modificacion, cod_creadopor, cod_modificadopor, cod_area) VALUES (1, 'AMBRO', NULL, 'MN', 'FL', 'Domicilio', '1994-11-04', 'tlax', 24, 'mnflambr@gmail.com', 'o', NULL, '3242', NULL, NULL, NULL, NULL, NULL, '2019-01-01', NULL, NULL, 'coiejcoijcoic', NULL, NULL, NULL, true, 1, 1, 1, 1, NULL, NULL, '2019-01-01', '2019-06-08', 10, NULL, 1);


--
-- TOC entry 4601 (class 0 OID 18932)
-- Dependencies: 318
-- Data for Name: tsgrhencuesta; Type: TABLE DATA; Schema: sgrh; Owner: postgres
--

INSERT INTO sgrh.tsgrhencuesta (cod_encuesta, des_nbencuesta, cod_edoencuesta, fec_fechaencuesta, cod_lugar, tim_duracion, des_elementosvalidar, des_defectos, des_introduccion, cod_aceptado, cod_edoeliminar, cod_creadopor, cod_modificadopor, fec_creacion, fec_modificacion, cod_area) VALUES (5, 'ENCUESTA 00000001', 'Aceptado', '2018-12-10', 2, '00:15:00', NULL, NULL, NULL, true, false, 10, 10, '2018-12-10', '2018-12-10', 5);
INSERT INTO sgrh.tsgrhencuesta (cod_encuesta, des_nbencuesta, cod_edoencuesta, fec_fechaencuesta, cod_lugar, tim_duracion, des_elementosvalidar, des_defectos, des_introduccion, cod_aceptado, cod_edoeliminar, cod_creadopor, cod_modificadopor, fec_creacion, fec_modificacion, cod_area) VALUES (6, 'ENCUESTA 00000002', 'Aceptado', '2018-12-10', 2, '00:15:00', NULL, NULL, NULL, true, false, 10, 10, '2018-12-10', '2018-12-10', 5);
INSERT INTO sgrh.tsgrhencuesta (cod_encuesta, des_nbencuesta, cod_edoencuesta, fec_fechaencuesta, cod_lugar, tim_duracion, des_elementosvalidar, des_defectos, des_introduccion, cod_aceptado, cod_edoeliminar, cod_creadopor, cod_modificadopor, fec_creacion, fec_modificacion, cod_area) VALUES (8, 'ENCUESTA 00000004', 'Aceptado', '2019-02-10', 2, '00:10:00', NULL, NULL, NULL, true, false, 10, 10, '2019-02-10', '2019-02-10', 1);
INSERT INTO sgrh.tsgrhencuesta (cod_encuesta, des_nbencuesta, cod_edoencuesta, fec_fechaencuesta, cod_lugar, tim_duracion, des_elementosvalidar, des_defectos, des_introduccion, cod_aceptado, cod_edoeliminar, cod_creadopor, cod_modificadopor, fec_creacion, fec_modificacion, cod_area) VALUES (9, 'ENCUESTA 00000005', 'Aceptado', '2019-02-10', 2, '00:10:00', NULL, NULL, NULL, true, false, 10, 10, '2019-02-10', '2019-02-10', 1);
INSERT INTO sgrh.tsgrhencuesta (cod_encuesta, des_nbencuesta, cod_edoencuesta, fec_fechaencuesta, cod_lugar, tim_duracion, des_elementosvalidar, des_defectos, des_introduccion, cod_aceptado, cod_edoeliminar, cod_creadopor, cod_modificadopor, fec_creacion, fec_modificacion, cod_area) VALUES (10, 'ENCUESTA 00000006', 'Aceptado', '2019-02-10', 2, '00:10:00', NULL, NULL, NULL, true, false, 10, 10, '2019-02-10', '2019-02-10', 1);
INSERT INTO sgrh.tsgrhencuesta (cod_encuesta, des_nbencuesta, cod_edoencuesta, fec_fechaencuesta, cod_lugar, tim_duracion, des_elementosvalidar, des_defectos, des_introduccion, cod_aceptado, cod_edoeliminar, cod_creadopor, cod_modificadopor, fec_creacion, fec_modificacion, cod_area) VALUES (11, 'ENCUESTA 00000007', 'Aceptado', '2019-02-10', 2, '00:10:00', NULL, NULL, NULL, true, false, 10, 10, '2019-02-10', '2019-02-10', 1);
INSERT INTO sgrh.tsgrhencuesta (cod_encuesta, des_nbencuesta, cod_edoencuesta, fec_fechaencuesta, cod_lugar, tim_duracion, des_elementosvalidar, des_defectos, des_introduccion, cod_aceptado, cod_edoeliminar, cod_creadopor, cod_modificadopor, fec_creacion, fec_modificacion, cod_area) VALUES (12, 'ENCUESTA 00000008', 'Aceptado', '2019-02-10', 2, '00:10:00', NULL, NULL, NULL, true, false, 10, 10, '2019-02-10', '2019-02-10', 1);
INSERT INTO sgrh.tsgrhencuesta (cod_encuesta, des_nbencuesta, cod_edoencuesta, fec_fechaencuesta, cod_lugar, tim_duracion, des_elementosvalidar, des_defectos, des_introduccion, cod_aceptado, cod_edoeliminar, cod_creadopor, cod_modificadopor, fec_creacion, fec_modificacion, cod_area) VALUES (13, 'ENCUESTA 00000009', 'Aceptado', '2019-02-10', 2, '00:10:00', NULL, NULL, NULL, true, false, 10, 10, '2019-02-10', '2019-02-10', 2);
INSERT INTO sgrh.tsgrhencuesta (cod_encuesta, des_nbencuesta, cod_edoencuesta, fec_fechaencuesta, cod_lugar, tim_duracion, des_elementosvalidar, des_defectos, des_introduccion, cod_aceptado, cod_edoeliminar, cod_creadopor, cod_modificadopor, fec_creacion, fec_modificacion, cod_area) VALUES (14, 'ENCUESTA 00000010', 'Aceptado', '2019-02-10', 2, '00:10:00', NULL, NULL, NULL, true, false, 10, 10, '2019-02-10', '2019-02-10', 2);
INSERT INTO sgrh.tsgrhencuesta (cod_encuesta, des_nbencuesta, cod_edoencuesta, fec_fechaencuesta, cod_lugar, tim_duracion, des_elementosvalidar, des_defectos, des_introduccion, cod_aceptado, cod_edoeliminar, cod_creadopor, cod_modificadopor, fec_creacion, fec_modificacion, cod_area) VALUES (15, 'ENCUESTA 00000011', 'Aceptado', '2019-02-10', 2, '00:10:00', NULL, NULL, NULL, true, false, 10, 10, '2019-02-10', '2019-02-10', 2);
INSERT INTO sgrh.tsgrhencuesta (cod_encuesta, des_nbencuesta, cod_edoencuesta, fec_fechaencuesta, cod_lugar, tim_duracion, des_elementosvalidar, des_defectos, des_introduccion, cod_aceptado, cod_edoeliminar, cod_creadopor, cod_modificadopor, fec_creacion, fec_modificacion, cod_area) VALUES (16, 'ENCUESTA 00000012', 'Aceptado', '2019-02-10', 2, '00:10:00', NULL, NULL, NULL, true, false, 10, 10, '2019-02-10', '2019-02-10', 2);
INSERT INTO sgrh.tsgrhencuesta (cod_encuesta, des_nbencuesta, cod_edoencuesta, fec_fechaencuesta, cod_lugar, tim_duracion, des_elementosvalidar, des_defectos, des_introduccion, cod_aceptado, cod_edoeliminar, cod_creadopor, cod_modificadopor, fec_creacion, fec_modificacion, cod_area) VALUES (7, 'ENCUESTA 00000003', 'Aceptado', '2018-12-08', 2, '00:15:00', NULL, NULL, NULL, true, false, 10, 10, '2018-12-10', '2019-02-15', 1);


--
-- TOC entry 4602 (class 0 OID 18942)
-- Dependencies: 319
-- Data for Name: tsgrhencuesta_participantes; Type: TABLE DATA; Schema: sgrh; Owner: postgres
--

INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (1, 29, 5, 82, 477, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (2, 29, 5, 83, 482, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (3, 29, 5, 84, 488, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (4, 29, 5, 85, 493, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (5, 29, 5, 86, 498, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (6, 29, 5, 87, 502, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (7, 29, 5, 88, 507, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (8, 29, 5, 89, 512, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (9, 29, 5, 90, 516, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (10, 29, 5, 91, NULL, '');
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (11, 29, 5, 92, NULL, '');
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (12, 29, 5, 93, 523, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (13, 29, 5, 94, 528, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (14, 29, 5, 95, 533, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (15, 29, 5, 97, NULL, '');
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (16, 29, 5, 98, 539, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (17, 29, 5, 99, 543, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (18, 29, 5, 100, 548, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (19, 29, 5, 101, 553, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (20, 29, 5, 102, 557, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (21, 29, 5, 104, 564, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (22, 29, 5, 105, 568, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (23, 29, 5, 106, 574, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (24, 30, 5, 82, 477, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (25, 30, 5, 83, 482, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (26, 30, 5, 84, 489, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (27, 30, 5, 85, 494, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (28, 30, 5, 86, 498, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (29, 30, 5, 87, 502, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (30, 30, 5, 88, 507, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (31, 30, 5, 89, 512, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (32, 30, 5, 90, 515, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (33, 30, 5, 91, NULL, '');
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (34, 30, 5, 92, NULL, '');
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (35, 30, 5, 93, 522, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (36, 30, 5, 94, 529, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (37, 30, 5, 95, 533, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (38, 30, 5, 97, NULL, '');
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (39, 30, 5, 98, 539, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (40, 30, 5, 99, 543, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (41, 30, 5, 100, 548, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (42, 30, 5, 101, 553, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (43, 30, 5, 102, 557, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (44, 30, 5, 104, 563, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (45, 30, 5, 105, 568, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (46, 30, 5, 106, 573, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (47, 31, 5, 82, 479, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (48, 31, 5, 83, 484, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (49, 31, 5, 84, 487, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (50, 31, 5, 85, 492, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (51, 31, 5, 86, 499, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (52, 31, 5, 87, 502, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (53, 31, 5, 88, 507, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (54, 31, 5, 89, 512, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (55, 31, 5, 90, 517, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (56, 31, 5, 91, NULL, '');
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (57, 31, 5, 92, NULL, '');
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (58, 31, 5, 93, 524, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (59, 31, 5, 94, 529, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (60, 31, 5, 95, 533, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (61, 31, 5, 97, NULL, '');
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (62, 31, 5, 98, 537, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (63, 31, 5, 99, 544, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (64, 31, 5, 100, 548, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (65, 31, 5, 101, 553, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (66, 31, 5, 102, 558, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (67, 31, 5, 104, 564, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (68, 31, 5, 105, 570, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (69, 31, 5, 106, 574, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (70, 32, 5, 82, 478, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (71, 32, 5, 83, 483, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (72, 32, 5, 84, 488, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (73, 32, 5, 85, 492, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (74, 32, 5, 86, 498, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (75, 32, 5, 87, 502, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (76, 32, 5, 88, 508, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (77, 32, 5, 89, 512, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (78, 32, 5, 90, 517, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (79, 32, 5, 91, NULL, '');
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (80, 32, 5, 92, NULL, '');
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (81, 32, 5, 93, 523, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (82, 32, 5, 94, 529, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (83, 32, 5, 95, 533, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (84, 32, 5, 97, NULL, '');
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (85, 32, 5, 98, 539, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (86, 32, 5, 99, 543, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (87, 32, 5, 100, 548, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (88, 32, 5, 101, 553, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (89, 32, 5, 102, 557, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (90, 32, 5, 104, 565, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (91, 32, 5, 105, 569, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (92, 32, 5, 106, 575, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (93, 33, 5, 82, 477, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (94, 33, 5, 83, 482, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (95, 33, 5, 84, 488, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (96, 33, 5, 85, 493, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (97, 33, 5, 86, 499, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (98, 33, 5, 87, 502, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (99, 33, 5, 88, 507, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (100, 33, 5, 89, 512, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (101, 33, 5, 90, 518, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (102, 33, 5, 91, NULL, '');
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (103, 33, 5, 92, NULL, '');
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (104, 33, 5, 93, 523, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (105, 33, 5, 94, 529, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (106, 33, 5, 95, 532, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (107, 33, 5, 97, NULL, '');
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (108, 33, 5, 98, 538, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (109, 33, 5, 99, 543, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (110, 33, 5, 100, 548, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (111, 33, 5, 101, 553, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (112, 33, 5, 102, 558, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (113, 33, 5, 104, 565, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (114, 33, 5, 105, 570, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (115, 33, 5, 106, 575, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (116, 34, 5, 82, 477, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (117, 34, 5, 83, 482, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (118, 34, 5, 84, 488, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (119, 34, 5, 85, 492, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (120, 34, 5, 86, 499, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (121, 34, 5, 87, 502, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (122, 34, 5, 88, 508, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (123, 34, 5, 89, 512, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (124, 34, 5, 90, 518, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (125, 34, 5, 91, NULL, '');
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (126, 34, 5, 92, NULL, '');
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (127, 34, 5, 93, 523, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (128, 34, 5, 94, 527, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (129, 34, 5, 95, 532, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (130, 34, 5, 97, NULL, '');
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (131, 34, 5, 98, 538, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (132, 34, 5, 99, 543, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (133, 34, 5, 100, 549, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (134, 34, 5, 101, 553, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (135, 34, 5, 102, 559, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (136, 34, 5, 104, 563, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (137, 34, 5, 105, 569, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (138, 34, 5, 106, 575, NULL);


--
-- TOC entry 4603 (class 0 OID 18949)
-- Dependencies: 320
-- Data for Name: tsgrhescolaridad; Type: TABLE DATA; Schema: sgrh; Owner: postgres
--



--
-- TOC entry 4604 (class 0 OID 18956)
-- Dependencies: 321
-- Data for Name: tsgrhestatuscapacitacion; Type: TABLE DATA; Schema: sgrh; Owner: postgres
--

INSERT INTO sgrh.tsgrhestatuscapacitacion (cod_estatus, des_estatus, aud_feccreacion, aud_fecmodificacion, aud_creadopor, aud_modificadopor) VALUES (1, 'No aprobado', '2019-04-23', '2019-04-23', 10, 10);
INSERT INTO sgrh.tsgrhestatuscapacitacion (cod_estatus, des_estatus, aud_feccreacion, aud_fecmodificacion, aud_creadopor, aud_modificadopor) VALUES (2, 'En revisión', '2019-04-23', '2019-04-23', 10, 10);
INSERT INTO sgrh.tsgrhestatuscapacitacion (cod_estatus, des_estatus, aud_feccreacion, aud_fecmodificacion, aud_creadopor, aud_modificadopor) VALUES (3, 'Pendiente', '2019-04-23', '2019-04-23', 10, 10);
INSERT INTO sgrh.tsgrhestatuscapacitacion (cod_estatus, des_estatus, aud_feccreacion, aud_fecmodificacion, aud_creadopor, aud_modificadopor) VALUES (4, 'Detenido', '2019-04-23', '2019-04-23', 10, 10);
INSERT INTO sgrh.tsgrhestatuscapacitacion (cod_estatus, des_estatus, aud_feccreacion, aud_fecmodificacion, aud_creadopor, aud_modificadopor) VALUES (5, 'Aprobado', '2019-04-23', '2019-04-23', 10, 10);


--
-- TOC entry 4605 (class 0 OID 18960)
-- Dependencies: 322
-- Data for Name: tsgrhevacapacitacion; Type: TABLE DATA; Schema: sgrh; Owner: postgres
--



--
-- TOC entry 4606 (class 0 OID 18964)
-- Dependencies: 323
-- Data for Name: tsgrhevacontestadas; Type: TABLE DATA; Schema: sgrh; Owner: postgres
--



--
-- TOC entry 4607 (class 0 OID 18971)
-- Dependencies: 324
-- Data for Name: tsgrhevaluaciones; Type: TABLE DATA; Schema: sgrh; Owner: postgres
--



--
-- TOC entry 4608 (class 0 OID 18979)
-- Dependencies: 325
-- Data for Name: tsgrhexperienciaslaborales; Type: TABLE DATA; Schema: sgrh; Owner: postgres
--



--
-- TOC entry 4609 (class 0 OID 18986)
-- Dependencies: 326
-- Data for Name: tsgrhfactoreseva; Type: TABLE DATA; Schema: sgrh; Owner: postgres
--



--
-- TOC entry 4610 (class 0 OID 18990)
-- Dependencies: 327
-- Data for Name: tsgrhidiomas; Type: TABLE DATA; Schema: sgrh; Owner: postgres
--

INSERT INTO sgrh.tsgrhidiomas (cod_idioma, des_nbidioma, por_dominiooral, por_dominioescrito, cod_empleado) VALUES (1, 'Ingles', 30, 30, NULL);
INSERT INTO sgrh.tsgrhidiomas (cod_idioma, des_nbidioma, por_dominiooral, por_dominioescrito, cod_empleado) VALUES (2, 'Ingles', 30, 30, NULL);
INSERT INTO sgrh.tsgrhidiomas (cod_idioma, des_nbidioma, por_dominiooral, por_dominioescrito, cod_empleado) VALUES (3, 'Ingles', 30, 30, NULL);
INSERT INTO sgrh.tsgrhidiomas (cod_idioma, des_nbidioma, por_dominiooral, por_dominioescrito, cod_empleado) VALUES (4, 'ingles', 30, 30, NULL);
INSERT INTO sgrh.tsgrhidiomas (cod_idioma, des_nbidioma, por_dominiooral, por_dominioescrito, cod_empleado) VALUES (5, 'ingles', 30, 30, NULL);
INSERT INTO sgrh.tsgrhidiomas (cod_idioma, des_nbidioma, por_dominiooral, por_dominioescrito, cod_empleado) VALUES (6, 'ingles', 30, 30, NULL);
INSERT INTO sgrh.tsgrhidiomas (cod_idioma, des_nbidioma, por_dominiooral, por_dominioescrito, cod_empleado) VALUES (7, 'ingles', 30, 30, NULL);
INSERT INTO sgrh.tsgrhidiomas (cod_idioma, des_nbidioma, por_dominiooral, por_dominioescrito, cod_empleado) VALUES (8, 'ingles', 20, 20, NULL);
INSERT INTO sgrh.tsgrhidiomas (cod_idioma, des_nbidioma, por_dominiooral, por_dominioescrito, cod_empleado) VALUES (9, 'Ingles', 20, 20, 34);
INSERT INTO sgrh.tsgrhidiomas (cod_idioma, des_nbidioma, por_dominiooral, por_dominioescrito, cod_empleado) VALUES (10, 'Frances', 20, 20, 34);
INSERT INTO sgrh.tsgrhidiomas (cod_idioma, des_nbidioma, por_dominiooral, por_dominioescrito, cod_empleado) VALUES (11, 'Fran', 20, 20, NULL);
INSERT INTO sgrh.tsgrhidiomas (cod_idioma, des_nbidioma, por_dominiooral, por_dominioescrito, cod_empleado) VALUES (12, 'Fran', 20, 20, NULL);


--
-- TOC entry 4611 (class 0 OID 18994)
-- Dependencies: 328
-- Data for Name: tsgrhlogistica; Type: TABLE DATA; Schema: sgrh; Owner: postgres
--



--
-- TOC entry 4612 (class 0 OID 18998)
-- Dependencies: 329
-- Data for Name: tsgrhlugares; Type: TABLE DATA; Schema: sgrh; Owner: postgres
--



--
-- TOC entry 4613 (class 0 OID 19002)
-- Dependencies: 330
-- Data for Name: tsgrhmodo; Type: TABLE DATA; Schema: sgrh; Owner: postgres
--

INSERT INTO sgrh.tsgrhmodo (cod_modo, des_nbmodo, aud_feccreacion, aud_fecmodificacion, aud_creadopor, aud_modificadopor) VALUES (1, 'Interno', '2019-04-23', '2019-04-23', 10, 10);
INSERT INTO sgrh.tsgrhmodo (cod_modo, des_nbmodo, aud_feccreacion, aud_fecmodificacion, aud_creadopor, aud_modificadopor) VALUES (2, 'Externo', '2019-04-23', '2019-04-23', 10, 10);


--
-- TOC entry 4614 (class 0 OID 19006)
-- Dependencies: 331
-- Data for Name: tsgrhperfiles; Type: TABLE DATA; Schema: sgrh; Owner: postgres
--

INSERT INTO sgrh.tsgrhperfiles (cod_perfil, des_perfil) VALUES (1, 'Administrador');
INSERT INTO sgrh.tsgrhperfiles (cod_perfil, des_perfil) VALUES (2, 'Nuevo');
INSERT INTO sgrh.tsgrhperfiles (cod_perfil, des_perfil) VALUES (3, 'Prueba');
INSERT INTO sgrh.tsgrhperfiles (cod_perfil, des_perfil) VALUES (4, '.NET, java scrip');
INSERT INTO sgrh.tsgrhperfiles (cod_perfil, des_perfil) VALUES (5, 'Nuevo');
INSERT INTO sgrh.tsgrhperfiles (cod_perfil, des_perfil) VALUES (6, 'Nuevo');
INSERT INTO sgrh.tsgrhperfiles (cod_perfil, des_perfil) VALUES (7, 'luness');
INSERT INTO sgrh.tsgrhperfiles (cod_perfil, des_perfil) VALUES (8, 'nuevo');


--
-- TOC entry 4615 (class 0 OID 19010)
-- Dependencies: 332
-- Data for Name: tsgrhplancapacitacion; Type: TABLE DATA; Schema: sgrh; Owner: postgres
--



--
-- TOC entry 4616 (class 0 OID 19017)
-- Dependencies: 333
-- Data for Name: tsgrhplanoperativo; Type: TABLE DATA; Schema: sgrh; Owner: postgres
--



--
-- TOC entry 4617 (class 0 OID 19026)
-- Dependencies: 334
-- Data for Name: tsgrhpreguntasenc; Type: TABLE DATA; Schema: sgrh; Owner: postgres
--

INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (107, 'En su centro de trabajo las oportunidades de desarrollo laboral solo las reciben unas cuantas personas privilegiadas', false, false, 6);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (109, 'En  su centro de trabajo se cuenta con programas de capacitación en materia de igualdad laboral y no discriminación.', false, false, 6);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (111, 'En su centro de trabajo para lograr la contratación, una promoción o un ascenso cuentan más las recomendaciones que los conocimientos y capacidades de la persona.', false, false, 6);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (113, 'En su centro de trabajo la competencia por mejores puestos, condiciones laborales o salariales es justa y equitativa.', false, false, 6);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (115, 'En su centro de trabajo se cuenta con un sistema de evaluación de desempeño del personal.', false, false, 6);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (117, 'Usted siente que se le trata con respeto en su trabajo actual.', false, false, 7);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (119, 'En su centro de trabajo todas las personas que laboran obtienen un trato digno y decente.', false, false, 7);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (126, 'En su centro de trabajo existen campañas de difusión internas de promoción de la igualdad laboral y no discriminación.', false, false, 7);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (137, 'La organización cuenta con planes y acciones específicos destinados a mejorar mi trabajo.', false, false, 8);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (141, 'El nivel de compromiso por apoyar el trabajo de los demás en la organización es', false, false, 8);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (143, 'Mi jefe me respalda frente a sus superiores', false, false, 8);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (145, 'Participo de las actividades culturales y recreacionales que la organización realiza.', false, false, 8);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (147, 'Mi jefe me brinda la retroalimentación necesaria para reforzar mis puntos débiles según la evaluación de desempeño.', false, false, 8);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (149, 'Los jefes reconocen y valoran mi trabajo', false, false, 8);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (151, 'La distribución de la carga de trabajo que tiene mi área es', false, false, 8);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (153, '¿Cómo calificaría su nivel de satisfacción con el trabajo que realiza en la organización?', false, false, 8);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (155, 'Te agradeceremos nos hagas llegar algunos comentarios acerca de aspectos que ayudarían a mejorar nuestro ambiente de trabajo.', true, false, 8);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (157, 'Usted tiene el suficiente tiempo para realizar su trabajo habitual:', false, false, 9);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (159, '¿Considera que recibe una justa retribución económica por las labores desempeñadas?', false, false, 9);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (161, '¿Cómo calificaría su nivel de satisfacción por trabajar en la organización?', false, false, 9);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (121, 'En su centro de trabajo, en general hay personas que discriminan, tratan mal o le faltan el respeto a sus compañeras/os, colegas o subordinadas/os.', false, false, 7);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (124, 'En su centro de trabajo  las y los superiores reciben un trato mucho más respetuoso que subordinados(as) y personal administrativo.', false, false, 7);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (128, 'En su centro de trabajo las cargas de trabajo se distribuyen de acuerdo a la responsabilidad del cargo.', false, false, 7);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (130, 'En mi oficina se fomenta y desarrolla el trabajo en equipo', false, false, 8);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (132, 'Existe comunicación dentro de mi grupo de trabajo', false, false, 8);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (134, 'Siento que no me alcanza el tiempo para completar mi trabajo', false, false, 8);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (136, 'La relación entre compañeros de trabajo en la organización es', false, false, 8);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (138, 'La organización otorga buenos y equitativos beneficios a los trabajadores', false, false, 8);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (140, 'Las remuneraciones están al nivel de los sueldos de mis colegas en el mercado', false, false, 8);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (95, 'Soy responsable del trabajo que realizo', false, false, 5);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (94, 'Mi superior me motiva a cumplir con mi trabajo de la manera que yo considere mejor.', false, false, 5);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (93, 'Considero que necesito capacitación en alguna área de mi interés y que forma parte importante de mi desarrollo.', false, false, 5);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (142, 'Siento apoyo en mi jefe cuando me encuentro en dificultades', false, false, 8);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (91, '¿Cree que su trabajo es compatible con los objetivos de la empresa?', true, false, 5);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (90, 'Cuento con los materiales y equipos necesarios para realizar mi trabajo.', false, false, 5);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (89, 'Está conforme con la limpieza, higiene y salubridad en su lugar de trabajo.', false, false, 5);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (88, 'Si  hay  un  nuevo Plan  Estratégico, estoy dispuesto a servir de voluntario para iniciar los cambios.', false, false, 5);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (87, 'En esta Institución, la gente planifica cuidadosamente antes de tomar acción.', false, false, 5);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (144, 'Mi jefe me da autonomía para tomar las decisiones necesarias para el cumplimiento de mis responsabilidades.', false, false, 8);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (86, 'Yo aporto al proceso de planificación en mi área de trabajo.', false, false, 5);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (83, 'En mi organización está claramente definida su Misión y Visión.', false, false, 5);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (146, 'Mi jefe me proporciona información suficiente, adecuada para realizar bien mi trabajo', false, false, 8);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (148, 'El nivel de recursos (materiales, equipos e infraestructura) con los que cuento para realizar bien mi trabajo es', false, false, 8);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (150, 'Mi remuneración, comparada con lo que otros ganan y hacen en la organización, está acorde con las responsabilidades de mi cargo', false, false, 8);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (152, '¿Cómo calificaría su nivel de satisfacción por pertenecer a la organización?', false, false, 8);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (154, '¿Cómo calificaría su nivel de identificación con la organización?', false, false, 8);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (102, 'Siento que formo parte de un equipo que trabaja hacia una meta común', false, true, 5);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (101, 'En mi grupo de trabajo, solucionar el problema es más importante que encontrar algún culpable.', false, true, 5);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (100, 'Mis compañeros y yo trabajamos juntos de manera efectiva', false, true, 5);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (99, 'El horario de trabajo me permite atender mis necesidades personales', false, true, 5);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (98, 'Me siento comprometido para alcanzar las metas establecidas.', false, true, 5);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (156, 'En relación a las condiciones físicas de su puesto de trabajo (iluminación, temperatura, etc.)  usted considera que éste es:', false, false, 9);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (158, '¿Está usted de acuerdo en cómo está gestionado el departamento en el que trabaja respecto a las metas que éste tiene encomendadas?', false, false, 9);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (82, 'Me siento muy satisfecho con mi ambiente de trabajo.', false, false, 5);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (160, 'Considera que su remuneración está por encima de la media en su entorno social, fuera de la empresa?', false, false, 9);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (92, 'Considero que me pagan lo justo por mi trabajo.', true, false, 5);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (97, 'Conozco las exigencias de mi trabajo.', true, false, 5);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (108, 'En su centro de trabajo mujeres y hombres tienen por igual oportunidades de ascenso y capacitación.', false, false, 6);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (110, 'En los últimos 12 meses usted ha participado  en programas de capacitación.', false, false, 6);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (112, 'En su centro de trabajo se ha despedido a alguna mujer por embarazo u orillado a renunciar al regresar de su licencia de maternidad.', false, false, 6);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (114, 'En su centro de trabajo mujeres y hombres tienen las mismas oportunidades para ocupar puestos de decisión.', false, false, 6);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (116, 'En los últimos 12 meses le han realizado una evaluación de desempeño.', false, false, 6);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (118, 'En su centro de trabajo quienes realizan tareas personales para las y los jefes logran privilegios.', false, false, 7);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (120, 'En su centro de trabajo las valoraciones que se realizan a sus actividades dependen más de la calidad y responsabilidad que de cualquier otra cuestión personal.', false, false, 7);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (122, 'En su centro de trabajo debido a sus características personales hay personas que sufren un trato inferior o de burla.', false, false, 7);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (125, 'En su centro de trabajo las y los superiores están abiertos a la comunicación con el personal.', false, false, 7);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (127, 'En su centro de trabajo las funciones y tareas se transmiten de manera clara y precisa.', false, false, 7);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (85, 'Existe un plan para lograr los  objetivos de la organización.', false, false, 5);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (84, 'La  dirección manifiesta sus objetivos de tal forma que se crea un sentido común de misión e identidad entre sus miembros.', false, false, 5);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (129, 'Si manifiesto mi preocupación sobre algún asunto relacionado con la igualdad de género o prácticas discriminatorias, se le da seguimiento', false, false, 7);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (131, 'Para el desempeño de mis labores mi ambiente de trabajo es', false, false, 8);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (133, 'Existe comunicación fluida entre mi Región y la sede central.', false, false, 8);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (135, 'Los jefes en la organización se preocupan por mantener elevado el nivel de motivación del personal', false, false, 8);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (139, 'En la organización las funciones están claramente definidas', false, false, 8);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (205, 'preguntaejemplo', false, true, 5);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (106, 'Hay evidencia de que mi jefe me apoya utilizando mis ideas o propuestas para mejorar el trabajo', false, true, 5);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (105, 'Tengo mucho trabajo y poco tiempo para realizarlo', false, true, 5);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (104, 'Mi superior inmediato toma acciones que refuerzan el objetivo común de la Institución.', false, true, 5);


--
-- TOC entry 4618 (class 0 OID 19030)
-- Dependencies: 335
-- Data for Name: tsgrhpreguntaseva; Type: TABLE DATA; Schema: sgrh; Owner: postgres
--



--
-- TOC entry 4619 (class 0 OID 19034)
-- Dependencies: 336
-- Data for Name: tsgrhprocesos; Type: TABLE DATA; Schema: sgrh; Owner: postgres
--

INSERT INTO sgrh.tsgrhprocesos (cod_proceso, des_nbproceso, aud_feccreacion, aud_fecmodificacion, aud_creadopor, aud_modificadopor) VALUES (1, 'Plan Capacitacion GN', '2019-04-23', '2019-04-23', 10, 10);
INSERT INTO sgrh.tsgrhprocesos (cod_proceso, des_nbproceso, aud_feccreacion, aud_fecmodificacion, aud_creadopor, aud_modificadopor) VALUES (2, 'Plan Capacitacion GPR', '2019-04-23', '2019-04-23', 10, 10);
INSERT INTO sgrh.tsgrhprocesos (cod_proceso, des_nbproceso, aud_feccreacion, aud_fecmodificacion, aud_creadopor, aud_modificadopor) VALUES (3, 'Plan Capacitacion GR', '2019-04-23', '2019-04-23', 10, 10);
INSERT INTO sgrh.tsgrhprocesos (cod_proceso, des_nbproceso, aud_feccreacion, aud_fecmodificacion, aud_creadopor, aud_modificadopor) VALUES (4, 'Plan Capacitacion APE', '2019-04-23', '2019-04-23', 10, 10);


--
-- TOC entry 4620 (class 0 OID 19038)
-- Dependencies: 337
-- Data for Name: tsgrhproveedores; Type: TABLE DATA; Schema: sgrh; Owner: postgres
--



--
-- TOC entry 4621 (class 0 OID 19042)
-- Dependencies: 338
-- Data for Name: tsgrhpuestos; Type: TABLE DATA; Schema: sgrh; Owner: postgres
--

INSERT INTO sgrh.tsgrhpuestos (cod_puesto, des_puesto, cod_area, cod_acronimo) VALUES (1, 'DISEÑO Y MANTENIMIENTO DE BASE DE DATOS', 1, 'DBA');
INSERT INTO sgrh.tsgrhpuestos (cod_puesto, des_puesto, cod_area, cod_acronimo) VALUES (2, 'DESARROLLO BACKEND DE SOFTWARE COMERCIALIZABLE', 2, 'BACK');
INSERT INTO sgrh.tsgrhpuestos (cod_puesto, des_puesto, cod_area, cod_acronimo) VALUES (3, 'DISEÑO FRONTEND DE APLICATIVOS COMERCIALIZABLES', 3, 'FRONT');
INSERT INTO sgrh.tsgrhpuestos (cod_puesto, des_puesto, cod_area, cod_acronimo) VALUES (4, 'SOLUCION DE PROBLEMAS CON EL APLICATIVO DESARROLLADO Y MANTENIMIENTO DE CODIGO FUENTE', 4, 'QA');
INSERT INTO sgrh.tsgrhpuestos (cod_puesto, des_puesto, cod_area, cod_acronimo) VALUES (5, 'RESPONSABLE DE CAPACITACIÓN', 5, 'RC');
INSERT INTO sgrh.tsgrhpuestos (cod_puesto, des_puesto, cod_area, cod_acronimo) VALUES (6, 'RESPONSABLE DE RECURSOS HUMANOS Y AMBIENTE DE TRABAJO', 5, 'RRHAT');
INSERT INTO sgrh.tsgrhpuestos (cod_puesto, des_puesto, cod_area, cod_acronimo) VALUES (7, 'RESPOSABLE DE GESTIÓN DE RECURSOS ', 4, 'rdg');
INSERT INTO sgrh.tsgrhpuestos (cod_puesto, des_puesto, cod_area, cod_acronimo) VALUES (9, 'Soporte', 1, 'bds');
INSERT INTO sgrh.tsgrhpuestos (cod_puesto, des_puesto, cod_area, cod_acronimo) VALUES (8, 'Soporte', 6, 'bds');
INSERT INTO sgrh.tsgrhpuestos (cod_puesto, des_puesto, cod_area, cod_acronimo) VALUES (11, 'Prueba', 4, 'stp');


--
-- TOC entry 4622 (class 0 OID 19046)
-- Dependencies: 339
-- Data for Name: tsgrhrelacionroles; Type: TABLE DATA; Schema: sgrh; Owner: postgres
--



--
-- TOC entry 4623 (class 0 OID 19049)
-- Dependencies: 340
-- Data for Name: tsgrhrespuestasenc; Type: TABLE DATA; Schema: sgrh; Owner: postgres
--

INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (475, 1, 82, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (476, 2, 82, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (477, 3, 82, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (478, 4, 82, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (479, 5, 82, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (480, 1, 83, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (481, 2, 83, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (482, 3, 83, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (483, 4, 83, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (484, 5, 83, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (485, 1, 84, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (486, 2, 84, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (487, 3, 84, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (488, 4, 84, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (489, 5, 84, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (490, 1, 85, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (491, 2, 85, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (492, 3, 85, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (493, 4, 85, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (494, 5, 85, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (495, 1, 86, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (496, 2, 86, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (497, 3, 86, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (498, 4, 86, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (499, 5, 86, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (500, 1, 87, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (501, 2, 87, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (502, 3, 87, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (503, 4, 87, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (504, 5, 87, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (505, 1, 88, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (506, 2, 88, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (507, 3, 88, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (508, 4, 88, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (509, 5, 88, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (510, 1, 89, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (511, 2, 89, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (512, 3, 89, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (513, 4, 89, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (514, 5, 89, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (515, 1, 90, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (516, 2, 90, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (517, 3, 90, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (518, 4, 90, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (519, 5, 90, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (520, 1, 93, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (521, 2, 93, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (522, 3, 93, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (523, 4, 93, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (524, 5, 93, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (525, 1, 94, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (526, 2, 94, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (527, 3, 94, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (528, 4, 94, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (529, 5, 94, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (530, 1, 95, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (531, 2, 95, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (532, 3, 95, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (533, 4, 95, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (534, 5, 95, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (535, 1, 98, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (536, 2, 98, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (537, 3, 98, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (538, 4, 98, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (539, 5, 98, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (540, 1, 99, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (541, 2, 99, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (542, 3, 99, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (543, 4, 99, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (544, 5, 99, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (545, 1, 100, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (546, 2, 100, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (547, 3, 100, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (548, 4, 100, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (549, 5, 100, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (550, 1, 101, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (551, 2, 101, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (552, 3, 101, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (553, 4, 101, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (554, 5, 101, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (555, 1, 102, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (556, 2, 102, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (557, 3, 102, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (558, 4, 102, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (559, 5, 102, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (561, 1, 104, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (562, 2, 104, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (563, 3, 104, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (564, 4, 104, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (565, 5, 104, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (566, 1, 105, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (567, 2, 105, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (568, 3, 105, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (569, 4, 105, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (570, 5, 105, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (571, 1, 106, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (572, 2, 106, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (573, 3, 106, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (574, 4, 106, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (575, 5, 106, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (576, 1, 156, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (577, 2, 156, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (578, 3, 156, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (579, 4, 156, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (580, 5, 156, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (581, 1, 157, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (582, 2, 157, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (583, 3, 157, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (584, 4, 157, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (585, 5, 157, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (586, 1, 158, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (587, 2, 158, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (588, 3, 158, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (589, 4, 158, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (590, 5, 158, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (591, 1, 159, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (592, 2, 159, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (593, 3, 159, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (594, 4, 159, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (595, 5, 159, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (596, 1, 160, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (597, 2, 160, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (598, 3, 160, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (599, 4, 160, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (600, 5, 160, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (601, 1, 161, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (602, 2, 161, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (603, 3, 161, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (604, 4, 161, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (605, 5, 161, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (606, 1, 117, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (607, 2, 117, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (608, 3, 117, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (609, 4, 117, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (610, 5, 117, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (611, 1, 118, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (612, 2, 118, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (613, 3, 118, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (614, 4, 118, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (615, 5, 118, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (616, 1, 119, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (617, 2, 119, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (618, 3, 119, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (619, 4, 119, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (620, 5, 119, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (621, 1, 120, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (622, 2, 120, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (623, 3, 120, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (624, 4, 120, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (625, 5, 120, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (626, 1, 121, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (627, 2, 121, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (628, 3, 121, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (629, 4, 121, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (630, 5, 121, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (631, 1, 122, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (632, 2, 122, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (633, 3, 122, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (634, 4, 122, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (635, 5, 122, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (637, 1, 124, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (638, 2, 124, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (639, 3, 124, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (640, 4, 124, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (641, 5, 124, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (642, 1, 125, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (643, 2, 125, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (644, 3, 125, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (645, 4, 125, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (646, 5, 125, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (647, 1, 126, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (648, 2, 126, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (649, 3, 126, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (650, 4, 126, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (651, 5, 126, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (652, 1, 127, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (653, 2, 127, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (654, 3, 127, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (655, 4, 127, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (656, 5, 127, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (657, 1, 128, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (658, 2, 128, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (659, 3, 128, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (660, 4, 128, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (661, 5, 128, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (662, 1, 129, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (663, 2, 129, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (664, 3, 129, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (665, 4, 129, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (666, 5, 129, false);


--
-- TOC entry 4624 (class 0 OID 19053)
-- Dependencies: 341
-- Data for Name: tsgrhrespuestaseva; Type: TABLE DATA; Schema: sgrh; Owner: postgres
--



--
-- TOC entry 4625 (class 0 OID 19057)
-- Dependencies: 342
-- Data for Name: tsgrhrevplanoperativo; Type: TABLE DATA; Schema: sgrh; Owner: postgres
--



--
-- TOC entry 4626 (class 0 OID 19066)
-- Dependencies: 343
-- Data for Name: tsgrhrolempleado; Type: TABLE DATA; Schema: sgrh; Owner: postgres
--



--
-- TOC entry 4627 (class 0 OID 19070)
-- Dependencies: 344
-- Data for Name: tsgrhsubfactoreseva; Type: TABLE DATA; Schema: sgrh; Owner: postgres
--



--
-- TOC entry 4628 (class 0 OID 19074)
-- Dependencies: 345
-- Data for Name: tsgrhtipocapacitacion; Type: TABLE DATA; Schema: sgrh; Owner: postgres
--

INSERT INTO sgrh.tsgrhtipocapacitacion (cod_tipocapacitacion, des_nbtipocapacitacion, aud_feccreacion, aud_fecmodificacion, aud_creadopor, aud_modificadopor) VALUES (1, 'Curso', '2019-04-23', '2019-04-23', 10, 10);
INSERT INTO sgrh.tsgrhtipocapacitacion (cod_tipocapacitacion, des_nbtipocapacitacion, aud_feccreacion, aud_fecmodificacion, aud_creadopor, aud_modificadopor) VALUES (2, 'Taller', '2019-04-23', '2019-04-23', 10, 10);
INSERT INTO sgrh.tsgrhtipocapacitacion (cod_tipocapacitacion, des_nbtipocapacitacion, aud_feccreacion, aud_fecmodificacion, aud_creadopor, aud_modificadopor) VALUES (3, 'Seminario', '2019-04-23', '2019-04-23', 10, 10);
INSERT INTO sgrh.tsgrhtipocapacitacion (cod_tipocapacitacion, des_nbtipocapacitacion, aud_feccreacion, aud_fecmodificacion, aud_creadopor, aud_modificadopor) VALUES (4, 'Autoestudio', '2019-04-23', '2019-04-23', 10, 10);
INSERT INTO sgrh.tsgrhtipocapacitacion (cod_tipocapacitacion, des_nbtipocapacitacion, aud_feccreacion, aud_fecmodificacion, aud_creadopor, aud_modificadopor) VALUES (5, 'Mentoreo', '2019-04-23', '2019-04-23', 10, 10);
INSERT INTO sgrh.tsgrhtipocapacitacion (cod_tipocapacitacion, des_nbtipocapacitacion, aud_feccreacion, aud_fecmodificacion, aud_creadopor, aud_modificadopor) VALUES (6, 'Diplomado', '2019-04-23', '2019-04-23', 10, 10);
INSERT INTO sgrh.tsgrhtipocapacitacion (cod_tipocapacitacion, des_nbtipocapacitacion, aud_feccreacion, aud_fecmodificacion, aud_creadopor, aud_modificadopor) VALUES (7, 'Congreso', '2019-04-23', '2019-04-23', 10, 10);


--
-- TOC entry 4629 (class 0 OID 19078)
-- Dependencies: 346
-- Data for Name: tsgrhvalidaevaluaciondes; Type: TABLE DATA; Schema: sgrh; Owner: postgres
--



--
-- TOC entry 4525 (class 0 OID 16627)
-- Dependencies: 242
-- Data for Name: tsgrtagenda; Type: TABLE DATA; Schema: sgrt; Owner: postgres
--



--
-- TOC entry 4526 (class 0 OID 16632)
-- Dependencies: 243
-- Data for Name: tsgrtarchivos; Type: TABLE DATA; Schema: sgrt; Owner: postgres
--



--
-- TOC entry 4527 (class 0 OID 16639)
-- Dependencies: 244
-- Data for Name: tsgrtasistentes; Type: TABLE DATA; Schema: sgrt; Owner: postgres
--



--
-- TOC entry 4528 (class 0 OID 16643)
-- Dependencies: 245
-- Data for Name: tsgrtattchticket; Type: TABLE DATA; Schema: sgrt; Owner: postgres
--



--
-- TOC entry 4529 (class 0 OID 16650)
-- Dependencies: 246
-- Data for Name: tsgrtayudatopico; Type: TABLE DATA; Schema: sgrt; Owner: postgres
--



--
-- TOC entry 4530 (class 0 OID 16654)
-- Dependencies: 247
-- Data for Name: tsgrtcategoriafaq; Type: TABLE DATA; Schema: sgrt; Owner: postgres
--



--
-- TOC entry 4531 (class 0 OID 16661)
-- Dependencies: 248
-- Data for Name: tsgrtchat; Type: TABLE DATA; Schema: sgrt; Owner: postgres
--



--
-- TOC entry 4532 (class 0 OID 16668)
-- Dependencies: 249
-- Data for Name: tsgrtciudades; Type: TABLE DATA; Schema: sgrt; Owner: postgres
--



--
-- TOC entry 4533 (class 0 OID 16672)
-- Dependencies: 250
-- Data for Name: tsgrtcomentariosagenda; Type: TABLE DATA; Schema: sgrt; Owner: postgres
--



--
-- TOC entry 4534 (class 0 OID 16679)
-- Dependencies: 251
-- Data for Name: tsgrtcomentariosreunion; Type: TABLE DATA; Schema: sgrt; Owner: postgres
--



--
-- TOC entry 4535 (class 0 OID 16686)
-- Dependencies: 252
-- Data for Name: tsgrtcompromisos; Type: TABLE DATA; Schema: sgrt; Owner: postgres
--



--
-- TOC entry 4536 (class 0 OID 16692)
-- Dependencies: 253
-- Data for Name: tsgrtcorreo; Type: TABLE DATA; Schema: sgrt; Owner: postgres
--



--
-- TOC entry 4537 (class 0 OID 16698)
-- Dependencies: 254
-- Data for Name: tsgrtdatossolicitud; Type: TABLE DATA; Schema: sgrt; Owner: postgres
--



--
-- TOC entry 4538 (class 0 OID 16701)
-- Dependencies: 255
-- Data for Name: tsgrtdepartamento; Type: TABLE DATA; Schema: sgrt; Owner: postgres
--



--
-- TOC entry 4539 (class 0 OID 16710)
-- Dependencies: 256
-- Data for Name: tsgrtedosolicitudes; Type: TABLE DATA; Schema: sgrt; Owner: postgres
--



--
-- TOC entry 4540 (class 0 OID 16713)
-- Dependencies: 257
-- Data for Name: tsgrtelementos; Type: TABLE DATA; Schema: sgrt; Owner: postgres
--



--
-- TOC entry 4541 (class 0 OID 16717)
-- Dependencies: 258
-- Data for Name: tsgrtestados; Type: TABLE DATA; Schema: sgrt; Owner: postgres
--

INSERT INTO sgrt.tsgrtestados (cod_estadorep, des_nbestado) VALUES (1, 'PUEBLA');
INSERT INTO sgrt.tsgrtestados (cod_estadorep, des_nbestado) VALUES (2, 'TLAXCALA');
INSERT INTO sgrt.tsgrtestados (cod_estadorep, des_nbestado) VALUES (3, 'VERACRUZ');
INSERT INTO sgrt.tsgrtestados (cod_estadorep, des_nbestado) VALUES (4, 'DISTRITO FEDERAL');


--
-- TOC entry 4542 (class 0 OID 16721)
-- Dependencies: 259
-- Data for Name: tsgrtfaq; Type: TABLE DATA; Schema: sgrt; Owner: postgres
--



--
-- TOC entry 4543 (class 0 OID 16728)
-- Dependencies: 260
-- Data for Name: tsgrtgrupo; Type: TABLE DATA; Schema: sgrt; Owner: postgres
--



--
-- TOC entry 4544 (class 0 OID 16733)
-- Dependencies: 261
-- Data for Name: tsgrtinvitados; Type: TABLE DATA; Schema: sgrt; Owner: postgres
--



--
-- TOC entry 4545 (class 0 OID 16740)
-- Dependencies: 262
-- Data for Name: tsgrtlugares; Type: TABLE DATA; Schema: sgrt; Owner: postgres
--



--
-- TOC entry 4546 (class 0 OID 16744)
-- Dependencies: 263
-- Data for Name: tsgrtmsjticket; Type: TABLE DATA; Schema: sgrt; Owner: postgres
--



--
-- TOC entry 4547 (class 0 OID 16754)
-- Dependencies: 264
-- Data for Name: tsgrtnota; Type: TABLE DATA; Schema: sgrt; Owner: postgres
--



--
-- TOC entry 4548 (class 0 OID 16762)
-- Dependencies: 265
-- Data for Name: tsgrtplantillacorreos; Type: TABLE DATA; Schema: sgrt; Owner: postgres
--



--
-- TOC entry 4549 (class 0 OID 16770)
-- Dependencies: 266
-- Data for Name: tsgrtprioridad; Type: TABLE DATA; Schema: sgrt; Owner: postgres
--



--
-- TOC entry 4550 (class 0 OID 16774)
-- Dependencies: 267
-- Data for Name: tsgrtresppredefinida; Type: TABLE DATA; Schema: sgrt; Owner: postgres
--



--
-- TOC entry 4551 (class 0 OID 16781)
-- Dependencies: 268
-- Data for Name: tsgrtrespuesta; Type: TABLE DATA; Schema: sgrt; Owner: postgres
--



--
-- TOC entry 4552 (class 0 OID 16788)
-- Dependencies: 269
-- Data for Name: tsgrtreuniones; Type: TABLE DATA; Schema: sgrt; Owner: postgres
--



--
-- TOC entry 4553 (class 0 OID 16795)
-- Dependencies: 270
-- Data for Name: tsgrtservicios; Type: TABLE DATA; Schema: sgrt; Owner: postgres
--



--
-- TOC entry 4554 (class 0 OID 16799)
-- Dependencies: 271
-- Data for Name: tsgrtsolicitudservicios; Type: TABLE DATA; Schema: sgrt; Owner: postgres
--



--
-- TOC entry 4555 (class 0 OID 16803)
-- Dependencies: 272
-- Data for Name: tsgrtticket; Type: TABLE DATA; Schema: sgrt; Owner: postgres
--



--
-- TOC entry 4674 (class 0 OID 21991)
-- Dependencies: 391
-- Data for Name: tsisatappservices; Type: TABLE DATA; Schema: sisat; Owner: postgres
--



--
-- TOC entry 4675 (class 0 OID 21994)
-- Dependencies: 392
-- Data for Name: tsisatarquitecturas; Type: TABLE DATA; Schema: sisat; Owner: postgres
--



--
-- TOC entry 4676 (class 0 OID 21997)
-- Dependencies: 393
-- Data for Name: tsisatasignaciones; Type: TABLE DATA; Schema: sisat; Owner: suite
--



--
-- TOC entry 4677 (class 0 OID 22001)
-- Dependencies: 394
-- Data for Name: tsisatcandidatos; Type: TABLE DATA; Schema: sisat; Owner: suite
--



--
-- TOC entry 4678 (class 0 OID 22005)
-- Dependencies: 395
-- Data for Name: tsisatcartaaceptacion; Type: TABLE DATA; Schema: sisat; Owner: suite
--



--
-- TOC entry 4679 (class 0 OID 22012)
-- Dependencies: 396
-- Data for Name: tsisatcartaasignacion; Type: TABLE DATA; Schema: sisat; Owner: suite
--

INSERT INTO sisat.tsisatcartaasignacion (cod_asignacion, cod_prospecto, cod_perfil, des_actividades, des_lugarsalida, des_lugarllegada, fec_salida, fec_llegada, cod_transporte, des_lugarhopedaje, fec_hospedaje, des_computadora, cod_telefono, des_accesorios, des_nbresponsable, des_nbpuesto, des_lugarresp, cod_telefonoresp, tim_horario, fec_iniciocontra, fec_terminocontra, imp_sueldomensual, imp_nominaimss, imp_honorarios, imp_otros, cod_rfc, des_razonsocial, des_correo, cod_cpostal, des_direccionfact, cod_cliente, cod_gpy, cod_rhta, cod_ape, cod_rys, cod_creadopor, cod_modificadopor, fec_creacion, fec_modificacion, des_observacion) VALUES (1, 1, 2, 'desarrollar pagina web informativa', 'calle Francisco Sarabia #3', 'Adolfo López Mateos No.2448', '2019-05-11', '2019-05-11', 'Colectivo', 'Adolfo López Mateos No.2448', '2019-05-11', 'propia', 'propio', 'propio', NULL, NULL, NULL, NULL, NULL, '2019-05-12', '2019-08-30', 8500.60, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, NULL, NULL, 1, 1, '2019-05-03', '2019-05-03', 'no hay observacion');
INSERT INTO sisat.tsisatcartaasignacion (cod_asignacion, cod_prospecto, cod_perfil, des_actividades, des_lugarsalida, des_lugarllegada, fec_salida, fec_llegada, cod_transporte, des_lugarhopedaje, fec_hospedaje, des_computadora, cod_telefono, des_accesorios, des_nbresponsable, des_nbpuesto, des_lugarresp, cod_telefonoresp, tim_horario, fec_iniciocontra, fec_terminocontra, imp_sueldomensual, imp_nominaimss, imp_honorarios, imp_otros, cod_rfc, des_razonsocial, des_correo, cod_cpostal, des_direccionfact, cod_cliente, cod_gpy, cod_rhta, cod_ape, cod_rys, cod_creadopor, cod_modificadopor, fec_creacion, fec_modificacion, des_observacion) VALUES (2, 9, 1, 'desarrollar aplicación de escritorio', 'calle Independencia #24', 'Adolfo López Mateos No.2448', '2019-01-02', '2019-01-02', 'Particular', 'Adolfo López Mateos No.2448', '2019-01-02', 'cliente', 'propio', 'propio', NULL, NULL, NULL, NULL, NULL, '2019-01-05', '2019-05-30', 9999.80, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, NULL, NULL, 1, 1, '2018-12-28', '2018-12-28', 'no hay observacion');
INSERT INTO sisat.tsisatcartaasignacion (cod_asignacion, cod_prospecto, cod_perfil, des_actividades, des_lugarsalida, des_lugarllegada, fec_salida, fec_llegada, cod_transporte, des_lugarhopedaje, fec_hospedaje, des_computadora, cod_telefono, des_accesorios, des_nbresponsable, des_nbpuesto, des_lugarresp, cod_telefonoresp, tim_horario, fec_iniciocontra, fec_terminocontra, imp_sueldomensual, imp_nominaimss, imp_honorarios, imp_otros, cod_rfc, des_razonsocial, des_correo, cod_cpostal, des_direccionfact, cod_cliente, cod_gpy, cod_rhta, cod_ape, cod_rys, cod_creadopor, cod_modificadopor, fec_creacion, fec_modificacion, des_observacion) VALUES (3, 2, 3, 'analista', 'calle Florencio Espinoza', 'Adolfo López Mateos No.2448', '2018-10-15', '2018-10-15', 'Uber', 'Adolfo López Mateos No.2448', '2018-10-15', 'cliente', 'propio', 'propio', NULL, NULL, NULL, NULL, NULL, '2018-10-15', '2019-10-15', 9999.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, NULL, NULL, 1, 1, '2018-10-09', '2018-10-09', 'no hay observacion');
INSERT INTO sisat.tsisatcartaasignacion (cod_asignacion, cod_prospecto, cod_perfil, des_actividades, des_lugarsalida, des_lugarllegada, fec_salida, fec_llegada, cod_transporte, des_lugarhopedaje, fec_hospedaje, des_computadora, cod_telefono, des_accesorios, des_nbresponsable, des_nbpuesto, des_lugarresp, cod_telefonoresp, tim_horario, fec_iniciocontra, fec_terminocontra, imp_sueldomensual, imp_nominaimss, imp_honorarios, imp_otros, cod_rfc, des_razonsocial, des_correo, cod_cpostal, des_direccionfact, cod_cliente, cod_gpy, cod_rhta, cod_ape, cod_rys, cod_creadopor, cod_modificadopor, fec_creacion, fec_modificacion, des_observacion) VALUES (4, 7, 2, 'desarrollar una aplicación de tienda en linea', 'Avenida Insurgentes #345', 'Adolfo López Mateos No.2448', '2018-09-10', '2018-09-10', 'Colectivo', 'Adolfo López Mateos No.2448', '2018-09-10', 'MBN', 'cliente', 'propio', NULL, NULL, NULL, NULL, NULL, '2018-09-10', '2019-04-30', 9500.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, NULL, NULL, 1, 1, '2018-09-05', '2018-09-05', 'no hay observacion');
INSERT INTO sisat.tsisatcartaasignacion (cod_asignacion, cod_prospecto, cod_perfil, des_actividades, des_lugarsalida, des_lugarllegada, fec_salida, fec_llegada, cod_transporte, des_lugarhopedaje, fec_hospedaje, des_computadora, cod_telefono, des_accesorios, des_nbresponsable, des_nbpuesto, des_lugarresp, cod_telefonoresp, tim_horario, fec_iniciocontra, fec_terminocontra, imp_sueldomensual, imp_nominaimss, imp_honorarios, imp_otros, cod_rfc, des_razonsocial, des_correo, cod_cpostal, des_direccionfact, cod_cliente, cod_gpy, cod_rhta, cod_ape, cod_rys, cod_creadopor, cod_modificadopor, fec_creacion, fec_modificacion, des_observacion) VALUES (5, 8, 4, 'probar aplicación en producción', 'calle Alameda #29', 'Adolfo López Mateos No.2448', '2019-03-04', '2019-03-04', 'Colectivo', 'Adolfo López Mateos No.2448', '2019-03-04', 'propia', 'cliente', 'propio', NULL, NULL, NULL, NULL, NULL, '2019-03-05', '2019-06-30', 8563.60, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, NULL, NULL, 1, 1, '2019-02-25', '2019-02-25', 'no hay observacion');
INSERT INTO sisat.tsisatcartaasignacion (cod_asignacion, cod_prospecto, cod_perfil, des_actividades, des_lugarsalida, des_lugarllegada, fec_salida, fec_llegada, cod_transporte, des_lugarhopedaje, fec_hospedaje, des_computadora, cod_telefono, des_accesorios, des_nbresponsable, des_nbpuesto, des_lugarresp, cod_telefonoresp, tim_horario, fec_iniciocontra, fec_terminocontra, imp_sueldomensual, imp_nominaimss, imp_honorarios, imp_otros, cod_rfc, des_razonsocial, des_correo, cod_cpostal, des_direccionfact, cod_cliente, cod_gpy, cod_rhta, cod_ape, cod_rys, cod_creadopor, cod_modificadopor, fec_creacion, fec_modificacion, des_observacion) VALUES (6, 6, 2, 'desarrollar aplicación para venta de boletos de cine', 'Esteban de Antuñano #34', 'Adolfo López Mateos No.2448', '2019-04-13', '2019-04-13', 'Particular', 'Adolfo López Mateos No.2448', '2019-04-13', 'propia', 'cliente', 'propio', NULL, NULL, NULL, NULL, NULL, '2019-04-14', '2019-09-30', 8500.99, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, 1, 1, NULL, NULL, 1, 1, '2019-04-09', '2019-04-09', 'no hay observacion');
INSERT INTO sisat.tsisatcartaasignacion (cod_asignacion, cod_prospecto, cod_perfil, des_actividades, des_lugarsalida, des_lugarllegada, fec_salida, fec_llegada, cod_transporte, des_lugarhopedaje, fec_hospedaje, des_computadora, cod_telefono, des_accesorios, des_nbresponsable, des_nbpuesto, des_lugarresp, cod_telefonoresp, tim_horario, fec_iniciocontra, fec_terminocontra, imp_sueldomensual, imp_nominaimss, imp_honorarios, imp_otros, cod_rfc, des_razonsocial, des_correo, cod_cpostal, des_direccionfact, cod_cliente, cod_gpy, cod_rhta, cod_ape, cod_rys, cod_creadopor, cod_modificadopor, fec_creacion, fec_modificacion, des_observacion) VALUES (7, 5, 2, 'desarrollar aplicación para el manejo de finanzas', 'Niños Heroes #34', 'Adolfo López Mateos No.2448', '2019-05-05', '2019-05-05', 'Transporte de person', 'Adolfo López Mateos No.2448', '2019-05-05', 'MBN', 'MBN', 'propio', NULL, NULL, NULL, NULL, NULL, '2019-05-13', '2019-10-31', 9356.60, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, NULL, NULL, 1, 1, '2019-04-26', '2019-04-26', 'no hay observacion');
INSERT INTO sisat.tsisatcartaasignacion (cod_asignacion, cod_prospecto, cod_perfil, des_actividades, des_lugarsalida, des_lugarllegada, fec_salida, fec_llegada, cod_transporte, des_lugarhopedaje, fec_hospedaje, des_computadora, cod_telefono, des_accesorios, des_nbresponsable, des_nbpuesto, des_lugarresp, cod_telefonoresp, tim_horario, fec_iniciocontra, fec_terminocontra, imp_sueldomensual, imp_nominaimss, imp_honorarios, imp_otros, cod_rfc, des_razonsocial, des_correo, cod_cpostal, des_direccionfact, cod_cliente, cod_gpy, cod_rhta, cod_ape, cod_rys, cod_creadopor, cod_modificadopor, fec_creacion, fec_modificacion, des_observacion) VALUES (8, 4, 1, 'desarrollar aplicación para censar casas', 'Benito Juarez S/N', 'Adolfo López Mateos No.2448', '2018-11-11', '2018-11-11', 'Uber', 'Adolfo López Mateos No.2448', '2018-11-11', 'cliente', 'propio', 'propio', NULL, NULL, NULL, NULL, NULL, '2018-11-11', '2019-03-30', 8793.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, 1, 1, NULL, NULL, 1, 1, '2019-11-03', '2019-11-03', 'no hay observacion');
INSERT INTO sisat.tsisatcartaasignacion (cod_asignacion, cod_prospecto, cod_perfil, des_actividades, des_lugarsalida, des_lugarllegada, fec_salida, fec_llegada, cod_transporte, des_lugarhopedaje, fec_hospedaje, des_computadora, cod_telefono, des_accesorios, des_nbresponsable, des_nbpuesto, des_lugarresp, cod_telefonoresp, tim_horario, fec_iniciocontra, fec_terminocontra, imp_sueldomensual, imp_nominaimss, imp_honorarios, imp_otros, cod_rfc, des_razonsocial, des_correo, cod_cpostal, des_direccionfact, cod_cliente, cod_gpy, cod_rhta, cod_ape, cod_rys, cod_creadopor, cod_modificadopor, fec_creacion, fec_modificacion, des_observacion) VALUES (9, 10, 3, 'analista', 'Andador El Pipila', 'Adolfo López Mateos No.2448', '2019-02-02', '2019-02-02', 'Transporte de person', 'Adolfo López Mateos No.2448', '2019-02-02', 'cliente', 'propio', 'propio', NULL, NULL, NULL, NULL, NULL, '2019-02-09', '2019-10-31', 7356.60, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, NULL, NULL, 1, 1, '2019-01-20', '2019-01-20', 'no hay observacion');
INSERT INTO sisat.tsisatcartaasignacion (cod_asignacion, cod_prospecto, cod_perfil, des_actividades, des_lugarsalida, des_lugarllegada, fec_salida, fec_llegada, cod_transporte, des_lugarhopedaje, fec_hospedaje, des_computadora, cod_telefono, des_accesorios, des_nbresponsable, des_nbpuesto, des_lugarresp, cod_telefonoresp, tim_horario, fec_iniciocontra, fec_terminocontra, imp_sueldomensual, imp_nominaimss, imp_honorarios, imp_otros, cod_rfc, des_razonsocial, des_correo, cod_cpostal, des_direccionfact, cod_cliente, cod_gpy, cod_rhta, cod_ape, cod_rys, cod_creadopor, cod_modificadopor, fec_creacion, fec_modificacion, des_observacion) VALUES (10, 3, 4, 'probar aplicación en producción', 'Circuito Esteban de Antuñano', 'Adolfo López Mateos No.2448', '2019-02-05', '2019-02-05', 'Transporte de person', 'Adolfo López Mateos No.2448', '2019-02-05', 'MBN', 'cliente', 'propio', NULL, NULL, NULL, NULL, NULL, '2019-02-09', '2019-12-31', 7000.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, NULL, NULL, 1, 1, '2019-01-28', '2019-01-28', 'no hay observacion');


--
-- TOC entry 4680 (class 0 OID 22019)
-- Dependencies: 397
-- Data for Name: tsisatcomentarios; Type: TABLE DATA; Schema: sisat; Owner: postgres
--



--
-- TOC entry 4681 (class 0 OID 22022)
-- Dependencies: 398
-- Data for Name: tsisatcotizaciones; Type: TABLE DATA; Schema: sisat; Owner: postgres
--



--
-- TOC entry 4682 (class 0 OID 22028)
-- Dependencies: 399
-- Data for Name: tsisatcursosycertificados; Type: TABLE DATA; Schema: sisat; Owner: suite
--



--
-- TOC entry 4683 (class 0 OID 22032)
-- Dependencies: 400
-- Data for Name: tsisatentrevistas; Type: TABLE DATA; Schema: sisat; Owner: suite
--



--
-- TOC entry 4684 (class 0 OID 22039)
-- Dependencies: 401
-- Data for Name: tsisatenviocorreos; Type: TABLE DATA; Schema: sisat; Owner: suite
--



--
-- TOC entry 4685 (class 0 OID 22046)
-- Dependencies: 402
-- Data for Name: tsisatescolaridad; Type: TABLE DATA; Schema: sisat; Owner: suite
--



--
-- TOC entry 4686 (class 0 OID 22050)
-- Dependencies: 403
-- Data for Name: tsisatexperienciaslaborales; Type: TABLE DATA; Schema: sisat; Owner: suite
--



--
-- TOC entry 4687 (class 0 OID 22057)
-- Dependencies: 404
-- Data for Name: tsisatfirmas; Type: TABLE DATA; Schema: sisat; Owner: suite
--



--
-- TOC entry 4688 (class 0 OID 22061)
-- Dependencies: 405
-- Data for Name: tsisatframeworks; Type: TABLE DATA; Schema: sisat; Owner: postgres
--



--
-- TOC entry 4689 (class 0 OID 22064)
-- Dependencies: 406
-- Data for Name: tsisathabilidades; Type: TABLE DATA; Schema: sisat; Owner: suite
--



--
-- TOC entry 4690 (class 0 OID 22068)
-- Dependencies: 407
-- Data for Name: tsisatherramientas; Type: TABLE DATA; Schema: sisat; Owner: postgres
--



--
-- TOC entry 4692 (class 0 OID 22076)
-- Dependencies: 409
-- Data for Name: tsisatides; Type: TABLE DATA; Schema: sisat; Owner: postgres
--



--
-- TOC entry 4693 (class 0 OID 22079)
-- Dependencies: 410
-- Data for Name: tsisatidiomas; Type: TABLE DATA; Schema: sisat; Owner: suite
--



--
-- TOC entry 4694 (class 0 OID 22083)
-- Dependencies: 411
-- Data for Name: tsisatlenguajes; Type: TABLE DATA; Schema: sisat; Owner: postgres
--

INSERT INTO sisat.tsisatlenguajes (cod_lenguaje, des_lenguaje) VALUES (1, 'Java');
INSERT INTO sisat.tsisatlenguajes (cod_lenguaje, des_lenguaje) VALUES (2, 'PHP');
INSERT INTO sisat.tsisatlenguajes (cod_lenguaje, des_lenguaje) VALUES (3, 'Phyton');
INSERT INTO sisat.tsisatlenguajes (cod_lenguaje, des_lenguaje) VALUES (4, 'Visual Basic .Net');
INSERT INTO sisat.tsisatlenguajes (cod_lenguaje, des_lenguaje) VALUES (5, 'C#');
INSERT INTO sisat.tsisatlenguajes (cod_lenguaje, des_lenguaje) VALUES (6, 'C++');


--
-- TOC entry 4695 (class 0 OID 22086)
-- Dependencies: 412
-- Data for Name: tsisatmaquetados; Type: TABLE DATA; Schema: sisat; Owner: postgres
--



--
-- TOC entry 4696 (class 0 OID 22089)
-- Dependencies: 413
-- Data for Name: tsisatmetodologias; Type: TABLE DATA; Schema: sisat; Owner: postgres
--



--
-- TOC entry 4697 (class 0 OID 22092)
-- Dependencies: 414
-- Data for Name: tsisatmodelados; Type: TABLE DATA; Schema: sisat; Owner: postgres
--



--
-- TOC entry 4698 (class 0 OID 22095)
-- Dependencies: 415
-- Data for Name: tsisatordenservicio; Type: TABLE DATA; Schema: sisat; Owner: suite
--



--
-- TOC entry 4699 (class 0 OID 22102)
-- Dependencies: 416
-- Data for Name: tsisatpatrones; Type: TABLE DATA; Schema: sisat; Owner: postgres
--



--
-- TOC entry 4700 (class 0 OID 22105)
-- Dependencies: 417
-- Data for Name: tsisatprospectos; Type: TABLE DATA; Schema: sisat; Owner: suite
--

INSERT INTO sisat.tsisatprospectos (cod_prospecto, des_nombre, des_nombres, des_appaterno, des_apmaterno, des_lugarnacimiento, fec_nacimiento, cod_edad, cod_edocivil, des_nbpadre, des_nbmadre, cod_numhermanos, des_nbcalle, cod_numcasa, des_colonia, des_localidad, des_municipio, des_estado, cod_cpostal, cod_tiposangre, des_emailmbn, des_emailpersonal, des_pasatiempo, cod_telefonocasa, cod_telefonomovil, cod_rfc, cod_nss, cod_curp, des_nacionalidad, cod_administrador, fec_fechacoment, txt_comentarios, cod_creadopor, cod_modificadopor, fec_creacion, fec_modificacion, bin_foto, cod_tipofoto, cod_extensionfoto, des_puestovacante, anio_experiencia, cod_entrevista) VALUES (1, 'kenedy', 'johnatan', 'vazquez', 'mimientzi', 'ocotlan', '2019-08-02', 22, 'soltero', 'augusto', 'rosaura', 2, 'hidalgo', 12, 'guadalpe', 'ixcotla', 'santa ana chiautempan', 'tlaxcala ', 90810, 'o', 'vazquez@outlook.com', 'vazquezken@live.com
', 'leer', '2511122334', '2461102123', 'asdfghjkl単', 'asdfghjkl', 'asdfghjkl', 'mexicana', 1, '2019-08-02', 'asdfghjk', 1, 1, '2019-08-02', '2019-08-02', NULL, 'asdjk', 'asdfg', 'jr java', 2, NULL);
INSERT INTO sisat.tsisatprospectos (cod_prospecto, des_nombre, des_nombres, des_appaterno, des_apmaterno, des_lugarnacimiento, fec_nacimiento, cod_edad, cod_edocivil, des_nbpadre, des_nbmadre, cod_numhermanos, des_nbcalle, cod_numcasa, des_colonia, des_localidad, des_municipio, des_estado, cod_cpostal, cod_tiposangre, des_emailmbn, des_emailpersonal, des_pasatiempo, cod_telefonocasa, cod_telefonomovil, cod_rfc, cod_nss, cod_curp, des_nacionalidad, cod_administrador, fec_fechacoment, txt_comentarios, cod_creadopor, cod_modificadopor, fec_creacion, fec_modificacion, bin_foto, cod_tipofoto, cod_extensionfoto, des_puestovacante, anio_experiencia, cod_entrevista) VALUES (2, 'Arturo', NULL, 'Corona', 'Reyes', 'Atlixco, Puebla', '1990-09-17', 29, 'soltero', 'Arturo Corona', 'Margarita Sanchez', 1, 'Andador El pipila', 1, 'Agua Escondida', 'Ixtacuixtla', 'Ixtacuixtla', 'Tlaxcala', 7234, 'O+', 'arturo@mbn.com', 'arturo@gmail.com', 'escuchar música', '2484820331', '2461373813', 'MAT4567BHFD', 'MAT4567BHFD', 'MAT4567BHFD', 'mexicano', NULL, '2019-02-03', 'no hay comentarios', 1, 1, '2018-10-06', '2018-10-06', NULL, '35dfg', 'png', 'programador web', NULL, NULL);
INSERT INTO sisat.tsisatprospectos (cod_prospecto, des_nombre, des_nombres, des_appaterno, des_apmaterno, des_lugarnacimiento, fec_nacimiento, cod_edad, cod_edocivil, des_nbpadre, des_nbmadre, cod_numhermanos, des_nbcalle, cod_numcasa, des_colonia, des_localidad, des_municipio, des_estado, cod_cpostal, cod_tiposangre, des_emailmbn, des_emailpersonal, des_pasatiempo, cod_telefonocasa, cod_telefonomovil, cod_rfc, cod_nss, cod_curp, des_nacionalidad, cod_administrador, fec_fechacoment, txt_comentarios, cod_creadopor, cod_modificadopor, fec_creacion, fec_modificacion, bin_foto, cod_tipofoto, cod_extensionfoto, des_puestovacante, anio_experiencia, cod_entrevista) VALUES (3, 'David ', NULL, 'Barrera', 'Cortes', 'Huamantla, Tlaxcala', '1996-04-15', 23, 'casado', 'Mauro Barrera', 'Angela Cortes', 5, '16 de septiembre', 0, 'Chapultepec', 'Jilotepec', 'Calpan', 'Puebla', 3455, 'O+', 'david@mbn.com', 'david@gmail.com', 'escuchar música', '2484820331', '2461373813', 'MAT4567BHFD', 'MAT4567BHFD', 'MAT4567BHFD', 'mexicano', NULL, '2019-02-03', 'no hay comentarios', 2, 2, '2019-01-01', '2019-01-01', NULL, 'wsd45', 'png', 'programador junior', NULL, NULL);
INSERT INTO sisat.tsisatprospectos (cod_prospecto, des_nombre, des_nombres, des_appaterno, des_apmaterno, des_lugarnacimiento, fec_nacimiento, cod_edad, cod_edocivil, des_nbpadre, des_nbmadre, cod_numhermanos, des_nbcalle, cod_numcasa, des_colonia, des_localidad, des_municipio, des_estado, cod_cpostal, cod_tiposangre, des_emailmbn, des_emailpersonal, des_pasatiempo, cod_telefonocasa, cod_telefonomovil, cod_rfc, cod_nss, cod_curp, des_nacionalidad, cod_administrador, fec_fechacoment, txt_comentarios, cod_creadopor, cod_modificadopor, fec_creacion, fec_modificacion, bin_foto, cod_tipofoto, cod_extensionfoto, des_puestovacante, anio_experiencia, cod_entrevista) VALUES (4, 'Jorge', 'Adrian', 'Barragán ', 'Peréz', 'Texmelucan, Puebla', '1993-04-24', 26, 'soltero', 'Jorge Barragan', 'Leticia Peréz', 4, 'Libertad Norte', 1234, 'Condesa ', 'San Andres', 'Ixtacuixtla', 'Tlaxcala', 4456, 'O+', 'jorge@mbn.com', 'adrian@gmail.com', 'escuchar música', '2484820331', '2461373813', 'MAT4567BHFD', 'MAT4567BHFD', 'MAT4567BHFD', 'mexicano', NULL, '2019-02-03', 'no hay comentarios', 1, 1, '2019-04-05', '2019-04-05', NULL, 'der78', 'png', 'analista', NULL, NULL);
INSERT INTO sisat.tsisatprospectos (cod_prospecto, des_nombre, des_nombres, des_appaterno, des_apmaterno, des_lugarnacimiento, fec_nacimiento, cod_edad, cod_edocivil, des_nbpadre, des_nbmadre, cod_numhermanos, des_nbcalle, cod_numcasa, des_colonia, des_localidad, des_municipio, des_estado, cod_cpostal, cod_tiposangre, des_emailmbn, des_emailpersonal, des_pasatiempo, cod_telefonocasa, cod_telefonomovil, cod_rfc, cod_nss, cod_curp, des_nacionalidad, cod_administrador, fec_fechacoment, txt_comentarios, cod_creadopor, cod_modificadopor, fec_creacion, fec_modificacion, bin_foto, cod_tipofoto, cod_extensionfoto, des_puestovacante, anio_experiencia, cod_entrevista) VALUES (5, 'Brenda', NULL, 'Poblano', 'Poblano', 'San Mateo Calpultitlan, Huejotzingo, Puebla', '1995-11-14', 24, 'soltera', 'Gustavo Poblano', 'Amanda Poblano', 3, 'Avenida del trabajo', 34, 'Zaragoza', 'San Miguel Tianguistenco', 'San Salvador El Verde', 'Puebla', 7753, 'O+', 'brenda@mbn.com', 'brendap@gmail.com', 'escuchar música', '2484820331', '2461373813', 'MAT4567BHFD', 'MAT4567BHFD', 'MAT4567BHFD', 'mexicana', NULL, '2019-02-03', 'no hay comentarios', 1, 1, '2019-05-02', '2019-05-02', NULL, 'scv34', 'png', 'analista', NULL, NULL);
INSERT INTO sisat.tsisatprospectos (cod_prospecto, des_nombre, des_nombres, des_appaterno, des_apmaterno, des_lugarnacimiento, fec_nacimiento, cod_edad, cod_edocivil, des_nbpadre, des_nbmadre, cod_numhermanos, des_nbcalle, cod_numcasa, des_colonia, des_localidad, des_municipio, des_estado, cod_cpostal, cod_tiposangre, des_emailmbn, des_emailpersonal, des_pasatiempo, cod_telefonocasa, cod_telefonomovil, cod_rfc, cod_nss, cod_curp, des_nacionalidad, cod_administrador, fec_fechacoment, txt_comentarios, cod_creadopor, cod_modificadopor, fec_creacion, fec_modificacion, bin_foto, cod_tipofoto, cod_extensionfoto, des_puestovacante, anio_experiencia, cod_entrevista) VALUES (6, 'Gregorio', NULL, 'Muñoz', 'Hernandez', 'Naucalpan, Estado de México', '1996-07-26', 23, 'casado', 'Gregorio Muñoz', 'Cristina Hernandez', 2, 'Iztaccihuatl', 21, 'El Carmén', 'Tlacotepec', 'San Salvador El Verde', 'Puebla', 4586, 'O+', 'gregorio@mbn.com', 'gregor@gmail.com', 'escuchar música', '2484820331', '2461373813', 'MAT4567BHFD', 'MAT4567BHFD', 'MAT4567BHFD', 'mexicano', NULL, '2019-02-03', 'no hay comentarios', 2, 2, '2018-07-05', '2018-07-05', NULL, 'hhj98', 'png', 'programador junior', NULL, NULL);
INSERT INTO sisat.tsisatprospectos (cod_prospecto, des_nombre, des_nombres, des_appaterno, des_apmaterno, des_lugarnacimiento, fec_nacimiento, cod_edad, cod_edocivil, des_nbpadre, des_nbmadre, cod_numhermanos, des_nbcalle, cod_numcasa, des_colonia, des_localidad, des_municipio, des_estado, cod_cpostal, cod_tiposangre, des_emailmbn, des_emailpersonal, des_pasatiempo, cod_telefonocasa, cod_telefonomovil, cod_rfc, cod_nss, cod_curp, des_nacionalidad, cod_administrador, fec_fechacoment, txt_comentarios, cod_creadopor, cod_modificadopor, fec_creacion, fec_modificacion, bin_foto, cod_tipofoto, cod_extensionfoto, des_puestovacante, anio_experiencia, cod_entrevista) VALUES (7, 'Nohemi', NULL, 'Gonzalez', 'López', 'Huejotzingo, Puebla', '1995-02-24', 24, 'casada', 'Carlos Gonzalez', 'Cecilia López', 2, 'Esteban de Antuñano', 5, 'Cruz de Piedra', 'Zacatepec', 'Huejotzingo', 'Puebla', 7896, 'O+', 'nohemi@mbn.com', 'nohe@gmail.com', 'escuchar música', '2484820331', '2461373813', 'MAT4567BHFD', 'MAT4567BHFD', 'MAT4567BHFD', 'mexicana', NULL, '2019-02-03', 'no hay comentarios', 2, 2, '2017-04-03', '2017-04-03', NULL, 'gbh83', 'png', 'tester', NULL, NULL);
INSERT INTO sisat.tsisatprospectos (cod_prospecto, des_nombre, des_nombres, des_appaterno, des_apmaterno, des_lugarnacimiento, fec_nacimiento, cod_edad, cod_edocivil, des_nbpadre, des_nbmadre, cod_numhermanos, des_nbcalle, cod_numcasa, des_colonia, des_localidad, des_municipio, des_estado, cod_cpostal, cod_tiposangre, des_emailmbn, des_emailpersonal, des_pasatiempo, cod_telefonocasa, cod_telefonomovil, cod_rfc, cod_nss, cod_curp, des_nacionalidad, cod_administrador, fec_fechacoment, txt_comentarios, cod_creadopor, cod_modificadopor, fec_creacion, fec_modificacion, bin_foto, cod_tipofoto, cod_extensionfoto, des_puestovacante, anio_experiencia, cod_entrevista) VALUES (8, 'Karen ', 'Guadalupe', 'Rodriguez', 'Escobar', 'Texmelucan, Puebla', '1995-12-14', 24, 'soltera', 'Alfredo Rodriguez', 'Ruth Escobar', 2, 'Hermanos Serdan', 2345, 'Tercer Barrio', 'Santa Rita', 'Tlahuapan', 'Puebla', 7634, 'O+', 'karen@mbn.com', 'rodriguezKaren@gmail.com', 'escuchar música', '2484820331', '2461373813', 'MAT4567BHFD', 'MAT4567BHFD', 'MAT4567BHFD', 'mexicano', NULL, '2019-02-03', 'no hay comentarios', 2, 2, '2016-02-01', '2016-02-01', NULL, 'cvf23', 'png', 'tester', NULL, NULL);
INSERT INTO sisat.tsisatprospectos (cod_prospecto, des_nombre, des_nombres, des_appaterno, des_apmaterno, des_lugarnacimiento, fec_nacimiento, cod_edad, cod_edocivil, des_nbpadre, des_nbmadre, cod_numhermanos, des_nbcalle, cod_numcasa, des_colonia, des_localidad, des_municipio, des_estado, cod_cpostal, cod_tiposangre, des_emailmbn, des_emailpersonal, des_pasatiempo, cod_telefonocasa, cod_telefonomovil, cod_rfc, cod_nss, cod_curp, des_nacionalidad, cod_administrador, fec_fechacoment, txt_comentarios, cod_creadopor, cod_modificadopor, fec_creacion, fec_modificacion, bin_foto, cod_tipofoto, cod_extensionfoto, des_puestovacante, anio_experiencia, cod_entrevista) VALUES (9, 'Rodrigo', NULL, 'Fuentes', 'Fuentes', 'Ixtacuixtla, Tlaxcala', '1996-12-24', 23, 'soltera', 'Ricardo Fuentes', 'Magali Fuentes', 1, 'Resurrección ', 1, 'Ameyal', 'San Simón', 'San Felipe Teotlalzingo', 'Puebla', 3254, 'O+', 'rodrigo@mbn.com', 'roy@gmail.com', 'escuchar música', '2484820331', '2461373813', 'MAT4567BHFD', 'MAT4567BHFD', 'MAT4567BHFD', 'mexicano', NULL, '2019-02-03', 'no hay comentarios', 1, 1, '2018-12-05', '2018-12-05', NULL, '34fgg', 'png', 'programador web', NULL, NULL);
INSERT INTO sisat.tsisatprospectos (cod_prospecto, des_nombre, des_nombres, des_appaterno, des_apmaterno, des_lugarnacimiento, fec_nacimiento, cod_edad, cod_edocivil, des_nbpadre, des_nbmadre, cod_numhermanos, des_nbcalle, cod_numcasa, des_colonia, des_localidad, des_municipio, des_estado, cod_cpostal, cod_tiposangre, des_emailmbn, des_emailpersonal, des_pasatiempo, cod_telefonocasa, cod_telefonomovil, cod_rfc, cod_nss, cod_curp, des_nacionalidad, cod_administrador, fec_fechacoment, txt_comentarios, cod_creadopor, cod_modificadopor, fec_creacion, fec_modificacion, bin_foto, cod_tipofoto, cod_extensionfoto, des_puestovacante, anio_experiencia, cod_entrevista) VALUES (10, 'Zuriel', NULL, 'Trejo', 'Linares', 'Tlaxcala, Tlaxcala', '1991-01-06', 28, 'soltera', 'Ramiro Trejo', 'Sthephanie Linares', 3, 'Zavaleta', 3, 'Florencio Espinoza', 'Santa Cruz', 'Ixtacuixtla', 'Tlaxcala', 2466, 'O+', 'zuriel@mbn.com', 'zuri@gmail.com', 'escuchar música', '2484820331', '2461373813', 'MAT4567BHFD', 'MAT4567BHFD', 'MAT4567BHFD', 'mexicano', NULL, '2019-02-03', 'no hay comentarios', 2, 2, '2018-11-14', '2018-11-14', NULL, 'nmh', 'png', 'programador junior', NULL, NULL);


--
-- TOC entry 4701 (class 0 OID 22114)
-- Dependencies: 418
-- Data for Name: tsisatprotocolos; Type: TABLE DATA; Schema: sisat; Owner: postgres
--



--
-- TOC entry 4702 (class 0 OID 22117)
-- Dependencies: 419
-- Data for Name: tsisatproyectos; Type: TABLE DATA; Schema: sisat; Owner: suite
--



--
-- TOC entry 4703 (class 0 OID 22121)
-- Dependencies: 420
-- Data for Name: tsisatqa; Type: TABLE DATA; Schema: sisat; Owner: postgres
--



--
-- TOC entry 4704 (class 0 OID 22124)
-- Dependencies: 421
-- Data for Name: tsisatrepositoriolibrerias; Type: TABLE DATA; Schema: sisat; Owner: postgres
--



--
-- TOC entry 4705 (class 0 OID 22127)
-- Dependencies: 422
-- Data for Name: tsisatrepositorios; Type: TABLE DATA; Schema: sisat; Owner: postgres
--



--
-- TOC entry 4706 (class 0 OID 22130)
-- Dependencies: 423
-- Data for Name: tsisatsgbd; Type: TABLE DATA; Schema: sisat; Owner: postgres
--



--
-- TOC entry 4707 (class 0 OID 22133)
-- Dependencies: 424
-- Data for Name: tsisatso; Type: TABLE DATA; Schema: sisat; Owner: postgres
--



--
-- TOC entry 4708 (class 0 OID 22136)
-- Dependencies: 425
-- Data for Name: tsisatvacantes; Type: TABLE DATA; Schema: sisat; Owner: suite
--



--
-- TOC entry 4922 (class 0 OID 0)
-- Dependencies: 205
-- Name: seq_sistema; Type: SEQUENCE SET; Schema: sgco; Owner: postgres
--

SELECT pg_catalog.setval('sgco.seq_sistema', 4, false);


--
-- TOC entry 4923 (class 0 OID 0)
-- Dependencies: 206
-- Name: seq_tipousuario; Type: SEQUENCE SET; Schema: sgco; Owner: postgres
--

SELECT pg_catalog.setval('sgco.seq_tipousuario', 1, false);


--
-- TOC entry 4924 (class 0 OID 0)
-- Dependencies: 207
-- Name: seq_usuarios; Type: SEQUENCE SET; Schema: sgco; Owner: postgres
--

SELECT pg_catalog.setval('sgco.seq_usuarios', 1, false);


--
-- TOC entry 4925 (class 0 OID 0)
-- Dependencies: 426
-- Name: seq_cabecera; Type: SEQUENCE SET; Schema: sgnom; Owner: suite
--

SELECT pg_catalog.setval('sgnom.seq_cabecera', 16, true);


--
-- TOC entry 4926 (class 0 OID 0)
-- Dependencies: 427
-- Name: seq_empquincena; Type: SEQUENCE SET; Schema: sgnom; Owner: suite
--

SELECT pg_catalog.setval('sgnom.seq_empquincena', 85, true);


--
-- TOC entry 4927 (class 0 OID 0)
-- Dependencies: 429
-- Name: seq_incidencia; Type: SEQUENCE SET; Schema: sgnom; Owner: postgres
--

SELECT pg_catalog.setval('sgnom.seq_incidencia', 10, false);


--
-- TOC entry 4928 (class 0 OID 0)
-- Dependencies: 273
-- Name: seq_area; Type: SEQUENCE SET; Schema: sgrh; Owner: postgres
--

SELECT pg_catalog.setval('sgrh.seq_area', 6, true);


--
-- TOC entry 4929 (class 0 OID 0)
-- Dependencies: 274
-- Name: seq_capacitaciones; Type: SEQUENCE SET; Schema: sgrh; Owner: postgres
--

SELECT pg_catalog.setval('sgrh.seq_capacitaciones', 1, false);


--
-- TOC entry 4930 (class 0 OID 0)
-- Dependencies: 275
-- Name: seq_cartaasignacion; Type: SEQUENCE SET; Schema: sgrh; Owner: postgres
--

SELECT pg_catalog.setval('sgrh.seq_cartaasignacion', 1, false);


--
-- TOC entry 4931 (class 0 OID 0)
-- Dependencies: 276
-- Name: seq_cat_encuesta_participantes; Type: SEQUENCE SET; Schema: sgrh; Owner: postgres
--

SELECT pg_catalog.setval('sgrh.seq_cat_encuesta_participantes', 138, true);


--
-- TOC entry 4932 (class 0 OID 0)
-- Dependencies: 277
-- Name: seq_catrespuestas; Type: SEQUENCE SET; Schema: sgrh; Owner: postgres
--

SELECT pg_catalog.setval('sgrh.seq_catrespuestas', 200, false);


--
-- TOC entry 4933 (class 0 OID 0)
-- Dependencies: 278
-- Name: seq_clientes; Type: SEQUENCE SET; Schema: sgrh; Owner: postgres
--

SELECT pg_catalog.setval('sgrh.seq_clientes', 5, true);


--
-- TOC entry 4934 (class 0 OID 0)
-- Dependencies: 279
-- Name: seq_contrataciones; Type: SEQUENCE SET; Schema: sgrh; Owner: postgres
--

SELECT pg_catalog.setval('sgrh.seq_contrataciones', 1, false);


--
-- TOC entry 4935 (class 0 OID 0)
-- Dependencies: 280
-- Name: seq_contratos; Type: SEQUENCE SET; Schema: sgrh; Owner: postgres
--

SELECT pg_catalog.setval('sgrh.seq_contratos', 1, false);


--
-- TOC entry 4936 (class 0 OID 0)
-- Dependencies: 281
-- Name: seq_empleado; Type: SEQUENCE SET; Schema: sgrh; Owner: postgres
--

SELECT pg_catalog.setval('sgrh.seq_empleado', 14, true);


--
-- TOC entry 4937 (class 0 OID 0)
-- Dependencies: 282
-- Name: seq_encuestas; Type: SEQUENCE SET; Schema: sgrh; Owner: postgres
--

SELECT pg_catalog.setval('sgrh.seq_encuestas', 17, true);


--
-- TOC entry 4938 (class 0 OID 0)
-- Dependencies: 283
-- Name: seq_escolaridad; Type: SEQUENCE SET; Schema: sgrh; Owner: postgres
--

SELECT pg_catalog.setval('sgrh.seq_escolaridad', 1, false);


--
-- TOC entry 4939 (class 0 OID 0)
-- Dependencies: 284
-- Name: seq_estatus; Type: SEQUENCE SET; Schema: sgrh; Owner: postgres
--

SELECT pg_catalog.setval('sgrh.seq_estatus', 5, true);


--
-- TOC entry 4940 (class 0 OID 0)
-- Dependencies: 285
-- Name: seq_evacapacitacion; Type: SEQUENCE SET; Schema: sgrh; Owner: postgres
--

SELECT pg_catalog.setval('sgrh.seq_evacapacitacion', 1, false);


--
-- TOC entry 4941 (class 0 OID 0)
-- Dependencies: 286
-- Name: seq_evacontestadas; Type: SEQUENCE SET; Schema: sgrh; Owner: postgres
--

SELECT pg_catalog.setval('sgrh.seq_evacontestadas', 1, false);


--
-- TOC entry 4942 (class 0 OID 0)
-- Dependencies: 287
-- Name: seq_evaluaciones; Type: SEQUENCE SET; Schema: sgrh; Owner: postgres
--

SELECT pg_catalog.setval('sgrh.seq_evaluaciones', 1, false);


--
-- TOC entry 4943 (class 0 OID 0)
-- Dependencies: 288
-- Name: seq_experiencialab; Type: SEQUENCE SET; Schema: sgrh; Owner: postgres
--

SELECT pg_catalog.setval('sgrh.seq_experiencialab', 1, false);


--
-- TOC entry 4944 (class 0 OID 0)
-- Dependencies: 289
-- Name: seq_factoreseva; Type: SEQUENCE SET; Schema: sgrh; Owner: postgres
--

SELECT pg_catalog.setval('sgrh.seq_factoreseva', 1, false);


--
-- TOC entry 4945 (class 0 OID 0)
-- Dependencies: 290
-- Name: seq_idiomas; Type: SEQUENCE SET; Schema: sgrh; Owner: postgres
--

SELECT pg_catalog.setval('sgrh.seq_idiomas', 12, true);


--
-- TOC entry 4946 (class 0 OID 0)
-- Dependencies: 291
-- Name: seq_logistica; Type: SEQUENCE SET; Schema: sgrh; Owner: postgres
--

SELECT pg_catalog.setval('sgrh.seq_logistica', 1, false);


--
-- TOC entry 4947 (class 0 OID 0)
-- Dependencies: 292
-- Name: seq_lugar; Type: SEQUENCE SET; Schema: sgrh; Owner: postgres
--

SELECT pg_catalog.setval('sgrh.seq_lugar', 1, false);


--
-- TOC entry 4948 (class 0 OID 0)
-- Dependencies: 293
-- Name: seq_modo; Type: SEQUENCE SET; Schema: sgrh; Owner: postgres
--

SELECT pg_catalog.setval('sgrh.seq_modo', 2, true);


--
-- TOC entry 4949 (class 0 OID 0)
-- Dependencies: 294
-- Name: seq_perfiles; Type: SEQUENCE SET; Schema: sgrh; Owner: postgres
--

SELECT pg_catalog.setval('sgrh.seq_perfiles', 8, true);


--
-- TOC entry 4950 (class 0 OID 0)
-- Dependencies: 295
-- Name: seq_plancapacitacion; Type: SEQUENCE SET; Schema: sgrh; Owner: postgres
--

SELECT pg_catalog.setval('sgrh.seq_plancapacitacion', 1, false);


--
-- TOC entry 4951 (class 0 OID 0)
-- Dependencies: 296
-- Name: seq_planesoperativos; Type: SEQUENCE SET; Schema: sgrh; Owner: postgres
--

SELECT pg_catalog.setval('sgrh.seq_planesoperativos', 1, false);


--
-- TOC entry 4952 (class 0 OID 0)
-- Dependencies: 297
-- Name: seq_preguntasenc; Type: SEQUENCE SET; Schema: sgrh; Owner: postgres
--

SELECT pg_catalog.setval('sgrh.seq_preguntasenc', 205, true);


--
-- TOC entry 4953 (class 0 OID 0)
-- Dependencies: 298
-- Name: seq_preguntaseva; Type: SEQUENCE SET; Schema: sgrh; Owner: postgres
--

SELECT pg_catalog.setval('sgrh.seq_preguntaseva', 1, false);


--
-- TOC entry 4954 (class 0 OID 0)
-- Dependencies: 299
-- Name: seq_proceso; Type: SEQUENCE SET; Schema: sgrh; Owner: postgres
--

SELECT pg_catalog.setval('sgrh.seq_proceso', 4, true);


--
-- TOC entry 4955 (class 0 OID 0)
-- Dependencies: 300
-- Name: seq_proveedor; Type: SEQUENCE SET; Schema: sgrh; Owner: postgres
--

SELECT pg_catalog.setval('sgrh.seq_proveedor', 1, false);


--
-- TOC entry 4956 (class 0 OID 0)
-- Dependencies: 301
-- Name: seq_puestos; Type: SEQUENCE SET; Schema: sgrh; Owner: postgres
--

SELECT pg_catalog.setval('sgrh.seq_puestos', 11, true);


--
-- TOC entry 4957 (class 0 OID 0)
-- Dependencies: 302
-- Name: seq_respuestasenc; Type: SEQUENCE SET; Schema: sgrh; Owner: postgres
--

SELECT pg_catalog.setval('sgrh.seq_respuestasenc', 500, false);


--
-- TOC entry 4958 (class 0 OID 0)
-- Dependencies: 303
-- Name: seq_respuestaseva; Type: SEQUENCE SET; Schema: sgrh; Owner: postgres
--

SELECT pg_catalog.setval('sgrh.seq_respuestaseva', 1, false);


--
-- TOC entry 4959 (class 0 OID 0)
-- Dependencies: 304
-- Name: seq_revplanesoperativos; Type: SEQUENCE SET; Schema: sgrh; Owner: postgres
--

SELECT pg_catalog.setval('sgrh.seq_revplanesoperativos', 1, false);


--
-- TOC entry 4960 (class 0 OID 0)
-- Dependencies: 305
-- Name: seq_rolempleado; Type: SEQUENCE SET; Schema: sgrh; Owner: postgres
--

SELECT pg_catalog.setval('sgrh.seq_rolempleado', 1, false);


--
-- TOC entry 4961 (class 0 OID 0)
-- Dependencies: 306
-- Name: seq_subfactoreseva; Type: SEQUENCE SET; Schema: sgrh; Owner: postgres
--

SELECT pg_catalog.setval('sgrh.seq_subfactoreseva', 1, false);


--
-- TOC entry 4962 (class 0 OID 0)
-- Dependencies: 307
-- Name: seq_tipocapacitacion; Type: SEQUENCE SET; Schema: sgrh; Owner: postgres
--

SELECT pg_catalog.setval('sgrh.seq_tipocapacitacion', 7, true);


--
-- TOC entry 4963 (class 0 OID 0)
-- Dependencies: 308
-- Name: seq_validaevaluaciones; Type: SEQUENCE SET; Schema: sgrh; Owner: postgres
--

SELECT pg_catalog.setval('sgrh.seq_validaevaluaciones', 1, false);


--
-- TOC entry 4964 (class 0 OID 0)
-- Dependencies: 211
-- Name: seq_agenda; Type: SEQUENCE SET; Schema: sgrt; Owner: postgres
--

SELECT pg_catalog.setval('sgrt.seq_agenda', 1, false);


--
-- TOC entry 4965 (class 0 OID 0)
-- Dependencies: 212
-- Name: seq_archivo; Type: SEQUENCE SET; Schema: sgrt; Owner: postgres
--

SELECT pg_catalog.setval('sgrt.seq_archivo', 1, false);


--
-- TOC entry 4966 (class 0 OID 0)
-- Dependencies: 213
-- Name: seq_asistente; Type: SEQUENCE SET; Schema: sgrt; Owner: postgres
--

SELECT pg_catalog.setval('sgrt.seq_asistente', 1, false);


--
-- TOC entry 4967 (class 0 OID 0)
-- Dependencies: 214
-- Name: seq_attach; Type: SEQUENCE SET; Schema: sgrt; Owner: postgres
--

SELECT pg_catalog.setval('sgrt.seq_attach', 1, false);


--
-- TOC entry 4968 (class 0 OID 0)
-- Dependencies: 215
-- Name: seq_categoriafaq; Type: SEQUENCE SET; Schema: sgrt; Owner: postgres
--

SELECT pg_catalog.setval('sgrt.seq_categoriafaq', 1, false);


--
-- TOC entry 4969 (class 0 OID 0)
-- Dependencies: 216
-- Name: seq_chat; Type: SEQUENCE SET; Schema: sgrt; Owner: postgres
--

SELECT pg_catalog.setval('sgrt.seq_chat', 1, false);


--
-- TOC entry 4970 (class 0 OID 0)
-- Dependencies: 217
-- Name: seq_ciudad; Type: SEQUENCE SET; Schema: sgrt; Owner: postgres
--

SELECT pg_catalog.setval('sgrt.seq_ciudad', 11, true);


--
-- TOC entry 4971 (class 0 OID 0)
-- Dependencies: 218
-- Name: seq_comentsagenda; Type: SEQUENCE SET; Schema: sgrt; Owner: postgres
--

SELECT pg_catalog.setval('sgrt.seq_comentsagenda', 1, false);


--
-- TOC entry 4972 (class 0 OID 0)
-- Dependencies: 219
-- Name: seq_comentsreunion; Type: SEQUENCE SET; Schema: sgrt; Owner: postgres
--

SELECT pg_catalog.setval('sgrt.seq_comentsreunion', 1, false);


--
-- TOC entry 4973 (class 0 OID 0)
-- Dependencies: 220
-- Name: seq_compromiso; Type: SEQUENCE SET; Schema: sgrt; Owner: postgres
--

SELECT pg_catalog.setval('sgrt.seq_compromiso', 1, false);


--
-- TOC entry 4974 (class 0 OID 0)
-- Dependencies: 221
-- Name: seq_contacto; Type: SEQUENCE SET; Schema: sgrt; Owner: postgres
--

SELECT pg_catalog.setval('sgrt.seq_contacto', 1, false);


--
-- TOC entry 4975 (class 0 OID 0)
-- Dependencies: 222
-- Name: seq_correo; Type: SEQUENCE SET; Schema: sgrt; Owner: postgres
--

SELECT pg_catalog.setval('sgrt.seq_correo', 1, false);


--
-- TOC entry 4976 (class 0 OID 0)
-- Dependencies: 223
-- Name: seq_depto; Type: SEQUENCE SET; Schema: sgrt; Owner: postgres
--

SELECT pg_catalog.setval('sgrt.seq_depto', 1, false);


--
-- TOC entry 4977 (class 0 OID 0)
-- Dependencies: 224
-- Name: seq_edoacuerdo; Type: SEQUENCE SET; Schema: sgrt; Owner: postgres
--

SELECT pg_catalog.setval('sgrt.seq_edoacuerdo', 1, false);


--
-- TOC entry 4978 (class 0 OID 0)
-- Dependencies: 225
-- Name: seq_elemento; Type: SEQUENCE SET; Schema: sgrt; Owner: postgres
--

SELECT pg_catalog.setval('sgrt.seq_elemento', 1, false);


--
-- TOC entry 4979 (class 0 OID 0)
-- Dependencies: 226
-- Name: seq_estadorep; Type: SEQUENCE SET; Schema: sgrt; Owner: postgres
--

SELECT pg_catalog.setval('sgrt.seq_estadorep', 4, true);


--
-- TOC entry 4980 (class 0 OID 0)
-- Dependencies: 227
-- Name: seq_faq; Type: SEQUENCE SET; Schema: sgrt; Owner: postgres
--

SELECT pg_catalog.setval('sgrt.seq_faq', 1, false);


--
-- TOC entry 4981 (class 0 OID 0)
-- Dependencies: 228
-- Name: seq_grupo; Type: SEQUENCE SET; Schema: sgrt; Owner: postgres
--

SELECT pg_catalog.setval('sgrt.seq_grupo', 1, false);


--
-- TOC entry 4982 (class 0 OID 0)
-- Dependencies: 229
-- Name: seq_invitado; Type: SEQUENCE SET; Schema: sgrt; Owner: postgres
--

SELECT pg_catalog.setval('sgrt.seq_invitado', 1, false);


--
-- TOC entry 4983 (class 0 OID 0)
-- Dependencies: 230
-- Name: seq_lugar; Type: SEQUENCE SET; Schema: sgrt; Owner: postgres
--

SELECT pg_catalog.setval('sgrt.seq_lugar', 4, true);


--
-- TOC entry 4984 (class 0 OID 0)
-- Dependencies: 231
-- Name: seq_mensaje; Type: SEQUENCE SET; Schema: sgrt; Owner: postgres
--

SELECT pg_catalog.setval('sgrt.seq_mensaje', 1, false);


--
-- TOC entry 4985 (class 0 OID 0)
-- Dependencies: 232
-- Name: seq_nota; Type: SEQUENCE SET; Schema: sgrt; Owner: postgres
--

SELECT pg_catalog.setval('sgrt.seq_nota', 1, false);


--
-- TOC entry 4986 (class 0 OID 0)
-- Dependencies: 233
-- Name: seq_plantillacorreo; Type: SEQUENCE SET; Schema: sgrt; Owner: postgres
--

SELECT pg_catalog.setval('sgrt.seq_plantillacorreo', 1, false);


--
-- TOC entry 4987 (class 0 OID 0)
-- Dependencies: 234
-- Name: seq_prioridad; Type: SEQUENCE SET; Schema: sgrt; Owner: postgres
--

SELECT pg_catalog.setval('sgrt.seq_prioridad', 1, false);


--
-- TOC entry 4988 (class 0 OID 0)
-- Dependencies: 235
-- Name: seq_resp; Type: SEQUENCE SET; Schema: sgrt; Owner: postgres
--

SELECT pg_catalog.setval('sgrt.seq_resp', 1, false);


--
-- TOC entry 4989 (class 0 OID 0)
-- Dependencies: 236
-- Name: seq_respuesta; Type: SEQUENCE SET; Schema: sgrt; Owner: postgres
--

SELECT pg_catalog.setval('sgrt.seq_respuesta', 1, false);


--
-- TOC entry 4990 (class 0 OID 0)
-- Dependencies: 347
-- Name: seq_respuestas_participantes; Type: SEQUENCE SET; Schema: sgrt; Owner: postgres
--

SELECT pg_catalog.setval('sgrt.seq_respuestas_participantes', 1, false);


--
-- TOC entry 4991 (class 0 OID 0)
-- Dependencies: 237
-- Name: seq_reunion; Type: SEQUENCE SET; Schema: sgrt; Owner: postgres
--

SELECT pg_catalog.setval('sgrt.seq_reunion', 1, false);


--
-- TOC entry 4992 (class 0 OID 0)
-- Dependencies: 238
-- Name: seq_servicio; Type: SEQUENCE SET; Schema: sgrt; Owner: postgres
--

SELECT pg_catalog.setval('sgrt.seq_servicio', 1, false);


--
-- TOC entry 4993 (class 0 OID 0)
-- Dependencies: 239
-- Name: seq_solicitud; Type: SEQUENCE SET; Schema: sgrt; Owner: postgres
--

SELECT pg_catalog.setval('sgrt.seq_solicitud', 1, false);


--
-- TOC entry 4994 (class 0 OID 0)
-- Dependencies: 240
-- Name: seq_ticket; Type: SEQUENCE SET; Schema: sgrt; Owner: postgres
--

SELECT pg_catalog.setval('sgrt.seq_ticket', 1, false);


--
-- TOC entry 4995 (class 0 OID 0)
-- Dependencies: 241
-- Name: seq_topico; Type: SEQUENCE SET; Schema: sgrt; Owner: postgres
--

SELECT pg_catalog.setval('sgrt.seq_topico', 1, false);


--
-- TOC entry 4996 (class 0 OID 0)
-- Dependencies: 374
-- Name: seq_aceptaciones; Type: SEQUENCE SET; Schema: sisat; Owner: suite
--

SELECT pg_catalog.setval('sisat.seq_aceptaciones', 1, false);


--
-- TOC entry 4997 (class 0 OID 0)
-- Dependencies: 375
-- Name: seq_asignaciones; Type: SEQUENCE SET; Schema: sisat; Owner: suite
--

SELECT pg_catalog.setval('sisat.seq_asignaciones', 1, false);


--
-- TOC entry 4998 (class 0 OID 0)
-- Dependencies: 376
-- Name: seq_candidatos; Type: SEQUENCE SET; Schema: sisat; Owner: suite
--

SELECT pg_catalog.setval('sisat.seq_candidatos', 1, false);


--
-- TOC entry 4999 (class 0 OID 0)
-- Dependencies: 377
-- Name: seq_cartaasignaciones; Type: SEQUENCE SET; Schema: sisat; Owner: suite
--

SELECT pg_catalog.setval('sisat.seq_cartaasignaciones', 1, false);


--
-- TOC entry 5000 (class 0 OID 0)
-- Dependencies: 378
-- Name: seq_cotizaciones; Type: SEQUENCE SET; Schema: sisat; Owner: suite
--

SELECT pg_catalog.setval('sisat.seq_cotizaciones', 1, false);


--
-- TOC entry 5001 (class 0 OID 0)
-- Dependencies: 379
-- Name: seq_cursos; Type: SEQUENCE SET; Schema: sisat; Owner: suite
--

SELECT pg_catalog.setval('sisat.seq_cursos', 1, false);


--
-- TOC entry 5002 (class 0 OID 0)
-- Dependencies: 380
-- Name: seq_entrevistas; Type: SEQUENCE SET; Schema: sisat; Owner: suite
--

SELECT pg_catalog.setval('sisat.seq_entrevistas', 1, false);


--
-- TOC entry 5003 (class 0 OID 0)
-- Dependencies: 381
-- Name: seq_envios; Type: SEQUENCE SET; Schema: sisat; Owner: suite
--

SELECT pg_catalog.setval('sisat.seq_envios', 1, false);


--
-- TOC entry 5004 (class 0 OID 0)
-- Dependencies: 382
-- Name: seq_escolaridad; Type: SEQUENCE SET; Schema: sisat; Owner: suite
--

SELECT pg_catalog.setval('sisat.seq_escolaridad', 1, false);


--
-- TOC entry 5005 (class 0 OID 0)
-- Dependencies: 383
-- Name: seq_experiencias; Type: SEQUENCE SET; Schema: sisat; Owner: suite
--

SELECT pg_catalog.setval('sisat.seq_experiencias', 1, false);


--
-- TOC entry 5006 (class 0 OID 0)
-- Dependencies: 384
-- Name: seq_firmas; Type: SEQUENCE SET; Schema: sisat; Owner: suite
--

SELECT pg_catalog.setval('sisat.seq_firmas', 1, false);


--
-- TOC entry 5007 (class 0 OID 0)
-- Dependencies: 385
-- Name: seq_habilidades; Type: SEQUENCE SET; Schema: sisat; Owner: suite
--

SELECT pg_catalog.setval('sisat.seq_habilidades', 1, false);


--
-- TOC entry 5008 (class 0 OID 0)
-- Dependencies: 386
-- Name: seq_idiomas; Type: SEQUENCE SET; Schema: sisat; Owner: suite
--

SELECT pg_catalog.setval('sisat.seq_idiomas', 1, false);


--
-- TOC entry 5009 (class 0 OID 0)
-- Dependencies: 387
-- Name: seq_ordenservicios; Type: SEQUENCE SET; Schema: sisat; Owner: suite
--

SELECT pg_catalog.setval('sisat.seq_ordenservicios', 1, false);


--
-- TOC entry 5010 (class 0 OID 0)
-- Dependencies: 388
-- Name: seq_prospectos; Type: SEQUENCE SET; Schema: sisat; Owner: suite
--

SELECT pg_catalog.setval('sisat.seq_prospectos', 1, false);


--
-- TOC entry 5011 (class 0 OID 0)
-- Dependencies: 389
-- Name: seq_proyectos; Type: SEQUENCE SET; Schema: sisat; Owner: suite
--

SELECT pg_catalog.setval('sisat.seq_proyectos', 1, false);


--
-- TOC entry 5012 (class 0 OID 0)
-- Dependencies: 390
-- Name: seq_vacantes; Type: SEQUENCE SET; Schema: sisat; Owner: suite
--

SELECT pg_catalog.setval('sisat.seq_vacantes', 1, false);


--
-- TOC entry 5013 (class 0 OID 0)
-- Dependencies: 408
-- Name: tsisatherramientas_cod_herramientas_seq; Type: SEQUENCE SET; Schema: sisat; Owner: postgres
--

SELECT pg_catalog.setval('sisat.tsisatherramientas_cod_herramientas_seq', 1, false);


--
-- TOC entry 3737 (class 2606 OID 16470)
-- Name: tsgcosistemas cod_agenda; Type: CONSTRAINT; Schema: sgco; Owner: postgres
--

ALTER TABLE ONLY sgco.tsgcosistemas
    ADD CONSTRAINT cod_agenda UNIQUE (cod_sistema);


--
-- TOC entry 3741 (class 2606 OID 16472)
-- Name: tsgcotipousuario cod_archivo; Type: CONSTRAINT; Schema: sgco; Owner: postgres
--

ALTER TABLE ONLY sgco.tsgcotipousuario
    ADD CONSTRAINT cod_archivo UNIQUE (cod_tipousuario);


--
-- TOC entry 3745 (class 2606 OID 16474)
-- Name: tsgcousuarios cod_asistente; Type: CONSTRAINT; Schema: sgco; Owner: postgres
--

ALTER TABLE ONLY sgco.tsgcousuarios
    ADD CONSTRAINT cod_asistente UNIQUE (cod_usuario);


--
-- TOC entry 3739 (class 2606 OID 16476)
-- Name: tsgcosistemas tsgcosistemas_pkey; Type: CONSTRAINT; Schema: sgco; Owner: postgres
--

ALTER TABLE ONLY sgco.tsgcosistemas
    ADD CONSTRAINT tsgcosistemas_pkey PRIMARY KEY (cod_sistema);


--
-- TOC entry 3743 (class 2606 OID 16478)
-- Name: tsgcotipousuario tsgcotipousuario_pkey; Type: CONSTRAINT; Schema: sgco; Owner: postgres
--

ALTER TABLE ONLY sgco.tsgcotipousuario
    ADD CONSTRAINT tsgcotipousuario_pkey PRIMARY KEY (cod_tipousuario);


--
-- TOC entry 3747 (class 2606 OID 16480)
-- Name: tsgcousuarios tsgcousuarios_pkey; Type: CONSTRAINT; Schema: sgco; Owner: postgres
--

ALTER TABLE ONLY sgco.tsgcousuarios
    ADD CONSTRAINT tsgcousuarios_pkey PRIMARY KEY (cod_usuario);


--
-- TOC entry 3987 (class 2606 OID 20973)
-- Name: tsgnomcatincidencia cat_incidencia_id; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomcatincidencia
    ADD CONSTRAINT cat_incidencia_id PRIMARY KEY (cod_catincidenciaid);


--
-- TOC entry 3975 (class 2606 OID 20975)
-- Name: tsgnomalguinaldo nom_aguinaldo_id; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomalguinaldo
    ADD CONSTRAINT nom_aguinaldo_id PRIMARY KEY (cod_aguinaldoid);


--
-- TOC entry 3979 (class 2606 OID 20977)
-- Name: tsgnombitacora nom_bitacora_id; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnombitacora
    ADD CONSTRAINT nom_bitacora_id PRIMARY KEY (cod_bitacoraid);


--
-- TOC entry 3983 (class 2606 OID 20979)
-- Name: tsgnomcabeceraht nom_cabecera_copia_id; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomcabeceraht
    ADD CONSTRAINT nom_cabecera_copia_id PRIMARY KEY (cod_cabeceraid);


--
-- TOC entry 3981 (class 2606 OID 20981)
-- Name: tsgnomcabecera nom_cabecera_id; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomcabecera
    ADD CONSTRAINT nom_cabecera_id PRIMARY KEY (cod_cabeceraid);


--
-- TOC entry 3977 (class 2606 OID 20983)
-- Name: tsgnomargumento nom_cat_argumento_id; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomargumento
    ADD CONSTRAINT nom_cat_argumento_id PRIMARY KEY (cod_argumentoid);


--
-- TOC entry 3995 (class 2606 OID 20985)
-- Name: tsgnomconcepto nom_cat_concepto_id; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomconcepto
    ADD CONSTRAINT nom_cat_concepto_id PRIMARY KEY (cod_conceptoid);


--
-- TOC entry 3997 (class 2606 OID 20987)
-- Name: tsgnomconceptosat nom_cat_concepto_sat_id; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomconceptosat
    ADD CONSTRAINT nom_cat_concepto_sat_id PRIMARY KEY (cod_conceptosatid);


--
-- TOC entry 4001 (class 2606 OID 20989)
-- Name: tsgnomejercicio nom_cat_ejercicio_id; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomejercicio
    ADD CONSTRAINT nom_cat_ejercicio_id PRIMARY KEY (cod_ejercicioid);


--
-- TOC entry 4009 (class 2606 OID 20991)
-- Name: tsgnomestatusnom nom_cat_estatus_nomina_id; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomestatusnom
    ADD CONSTRAINT nom_cat_estatus_nomina_id PRIMARY KEY (cod_estatusnomid);


--
-- TOC entry 4011 (class 2606 OID 20993)
-- Name: tsgnomformula nom_cat_formula_id; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomformula
    ADD CONSTRAINT nom_cat_formula_id PRIMARY KEY (cod_formulaid);


--
-- TOC entry 4013 (class 2606 OID 20995)
-- Name: tsgnomfuncion nom_cat_funcion_id; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomfuncion
    ADD CONSTRAINT nom_cat_funcion_id PRIMARY KEY (cod_funcionid);


--
-- TOC entry 4021 (class 2606 OID 20997)
-- Name: tsgnomquincena nom_cat_quincena_id; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomquincena
    ADD CONSTRAINT nom_cat_quincena_id PRIMARY KEY (cod_quincenaid);


--
-- TOC entry 4015 (class 2606 OID 20999)
-- Name: tsgnomhisttabla nom_cat_tabla_id; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomhisttabla
    ADD CONSTRAINT nom_cat_tabla_id PRIMARY KEY (cod_tablaid);


--
-- TOC entry 3985 (class 2606 OID 21001)
-- Name: tsgnomcalculo nom_cat_tipo_calculo_id; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomcalculo
    ADD CONSTRAINT nom_cat_tipo_calculo_id PRIMARY KEY (cod_calculoid);


--
-- TOC entry 3989 (class 2606 OID 21003)
-- Name: tsgnomclasificador nom_cat_tipo_clasificador_id; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomclasificador
    ADD CONSTRAINT nom_cat_tipo_clasificador_id PRIMARY KEY (cod_clasificadorid);


--
-- TOC entry 4023 (class 2606 OID 21005)
-- Name: tsgnomtipoconcepto nom_cat_tipo_conepto_id; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomtipoconcepto
    ADD CONSTRAINT nom_cat_tipo_conepto_id PRIMARY KEY (cod_tipoconceptoid);


--
-- TOC entry 4025 (class 2606 OID 21007)
-- Name: tsgnomtiponomina nom_cat_tipo_nomina_id; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomtiponomina
    ADD CONSTRAINT nom_cat_tipo_nomina_id PRIMARY KEY (cod_tiponominaid);


--
-- TOC entry 3993 (class 2606 OID 21009)
-- Name: tsgnomcncptoquincht nom_conceptos_quincena_copia_id; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomcncptoquincht
    ADD CONSTRAINT nom_conceptos_quincena_copia_id PRIMARY KEY (cod_cncptoquinchtid);


--
-- TOC entry 3991 (class 2606 OID 21011)
-- Name: tsgnomcncptoquinc nom_conceptos_quincena_id; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomcncptoquinc
    ADD CONSTRAINT nom_conceptos_quincena_id PRIMARY KEY (cod_cncptoquincid);


--
-- TOC entry 3999 (class 2606 OID 21013)
-- Name: tsgnomconfpago nom_conf_pago_pkey; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomconfpago
    ADD CONSTRAINT nom_conf_pago_pkey PRIMARY KEY (cod_confpagoid);


--
-- TOC entry 4003 (class 2606 OID 21015)
-- Name: tsgnomempleados nom_empleado_id; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomempleados
    ADD CONSTRAINT nom_empleado_id PRIMARY KEY (cod_empleadoid);


--
-- TOC entry 4007 (class 2606 OID 21017)
-- Name: tsgnomempquincenaht nom_empleado_quincena_copia_id; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomempquincenaht
    ADD CONSTRAINT nom_empleado_quincena_copia_id PRIMARY KEY (cod_empquincenahtid);


--
-- TOC entry 4005 (class 2606 OID 21019)
-- Name: tsgnomempquincena nom_empleado_quincena_id; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomempquincena
    ADD CONSTRAINT nom_empleado_quincena_id PRIMARY KEY (cod_empquincenaid);


--
-- TOC entry 4017 (class 2606 OID 21021)
-- Name: tsgnomincidencia nom_incidencia_id; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomincidencia
    ADD CONSTRAINT nom_incidencia_id PRIMARY KEY (cod_incidenciaid);


--
-- TOC entry 4019 (class 2606 OID 21023)
-- Name: tsgnommanterceros nom_manuales_terceros_pkey; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnommanterceros
    ADD CONSTRAINT nom_manuales_terceros_pkey PRIMARY KEY (cod_mantercerosid);


--
-- TOC entry 3903 (class 2606 OID 19090)
-- Name: tsgrhcatrespuestas catrespuestas_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhcatrespuestas
    ADD CONSTRAINT catrespuestas_pkey PRIMARY KEY (cod_catrespuesta);


--
-- TOC entry 3941 (class 2606 OID 19092)
-- Name: tsgrhmodo cod_capacitacion_pk; Type: CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhmodo
    ADD CONSTRAINT cod_capacitacion_pk PRIMARY KEY (cod_modo);


--
-- TOC entry 3921 (class 2606 OID 19094)
-- Name: tsgrhestatuscapacitacion cod_estatus_pk; Type: CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhestatuscapacitacion
    ADD CONSTRAINT cod_estatus_pk PRIMARY KEY (cod_estatus);


--
-- TOC entry 3939 (class 2606 OID 19096)
-- Name: tsgrhlugares cod_lugar_pk; Type: CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhlugares
    ADD CONSTRAINT cod_lugar_pk PRIMARY KEY (cod_lugar);


--
-- TOC entry 3953 (class 2606 OID 19098)
-- Name: tsgrhprocesos cod_procesos_pk; Type: CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhprocesos
    ADD CONSTRAINT cod_procesos_pk PRIMARY KEY (cod_proceso);


--
-- TOC entry 3955 (class 2606 OID 19100)
-- Name: tsgrhproveedores cod_proveedor_pk; Type: CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhproveedores
    ADD CONSTRAINT cod_proveedor_pk PRIMARY KEY (cod_proveedor);


--
-- TOC entry 3959 (class 2606 OID 19102)
-- Name: tsgrhrelacionroles cod_relacion_pk; Type: CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhrelacionroles
    ADD CONSTRAINT cod_relacion_pk PRIMARY KEY (cod_plancapacitacion, cod_rolempleado);


--
-- TOC entry 3967 (class 2606 OID 19104)
-- Name: tsgrhrolempleado cod_rolempleado_pk; Type: CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhrolempleado
    ADD CONSTRAINT cod_rolempleado_pk PRIMARY KEY (cod_rolempleado);


--
-- TOC entry 3971 (class 2606 OID 19106)
-- Name: tsgrhtipocapacitacion cod_tipocapacitacion_pk; Type: CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhtipocapacitacion
    ADD CONSTRAINT cod_tipocapacitacion_pk PRIMARY KEY (cod_tipocapacitacion);


--
-- TOC entry 3897 (class 2606 OID 19108)
-- Name: tsgrhasignacionesemp pk_cod_asignacion_asignacionesempleados; Type: CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhasignacionesemp
    ADD CONSTRAINT pk_cod_asignacion_asignacionesempleados PRIMARY KEY (cod_asignacion);


--
-- TOC entry 3923 (class 2606 OID 19110)
-- Name: tsgrhevacapacitacion pk_cod_evaluacioncap_evacapacitacionesemp; Type: CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhevacapacitacion
    ADD CONSTRAINT pk_cod_evaluacioncap_evacapacitacionesemp PRIMARY KEY (cod_evacapacitacion);


--
-- TOC entry 3895 (class 2606 OID 19112)
-- Name: tsgrhareas tsgrhareas_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhareas
    ADD CONSTRAINT tsgrhareas_pkey PRIMARY KEY (cod_area);


--
-- TOC entry 3899 (class 2606 OID 19114)
-- Name: tsgrhcapacitaciones tsgrhcapacitaciones_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhcapacitaciones
    ADD CONSTRAINT tsgrhcapacitaciones_pkey PRIMARY KEY (cod_capacitacion);


--
-- TOC entry 3901 (class 2606 OID 19116)
-- Name: tsgrhcartaasignacion tsgrhcartaasignacion_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhcartaasignacion
    ADD CONSTRAINT tsgrhcartaasignacion_pkey PRIMARY KEY (cod_asignacion);


--
-- TOC entry 3905 (class 2606 OID 19118)
-- Name: tsgrhclientes tsgrhclientes_des_correocte_key; Type: CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhclientes
    ADD CONSTRAINT tsgrhclientes_des_correocte_key UNIQUE (des_correocte);


--
-- TOC entry 3907 (class 2606 OID 19120)
-- Name: tsgrhclientes tsgrhclientes_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhclientes
    ADD CONSTRAINT tsgrhclientes_pkey PRIMARY KEY (cod_cliente);


--
-- TOC entry 3909 (class 2606 OID 19122)
-- Name: tsgrhcontrataciones tsgrhcontrataciones_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhcontrataciones
    ADD CONSTRAINT tsgrhcontrataciones_pkey PRIMARY KEY (cod_contratacion);


--
-- TOC entry 3911 (class 2606 OID 19124)
-- Name: tsgrhcontratos tsgrhcontratos_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhcontratos
    ADD CONSTRAINT tsgrhcontratos_pkey PRIMARY KEY (cod_contrato);


--
-- TOC entry 3913 (class 2606 OID 19126)
-- Name: tsgrhempleados tsgrhempleados_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhempleados
    ADD CONSTRAINT tsgrhempleados_pkey PRIMARY KEY (cod_empleado);


--
-- TOC entry 3917 (class 2606 OID 19128)
-- Name: tsgrhencuesta_participantes tsgrhencuesta_participantes_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhencuesta_participantes
    ADD CONSTRAINT tsgrhencuesta_participantes_pkey PRIMARY KEY (cod_participantenc);


--
-- TOC entry 3915 (class 2606 OID 19130)
-- Name: tsgrhencuesta tsgrhencuesta_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhencuesta
    ADD CONSTRAINT tsgrhencuesta_pkey PRIMARY KEY (cod_encuesta);


--
-- TOC entry 3919 (class 2606 OID 19132)
-- Name: tsgrhescolaridad tsgrhescolaridad_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhescolaridad
    ADD CONSTRAINT tsgrhescolaridad_pkey PRIMARY KEY (cod_escolaridad);


--
-- TOC entry 3925 (class 2606 OID 19134)
-- Name: tsgrhevacontestadas tsgrhevacontestadas_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhevacontestadas
    ADD CONSTRAINT tsgrhevacontestadas_pkey PRIMARY KEY (cod_evacontestada);


--
-- TOC entry 3927 (class 2606 OID 19136)
-- Name: tsgrhevaluaciones tsgrhevaluaciones_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhevaluaciones
    ADD CONSTRAINT tsgrhevaluaciones_pkey PRIMARY KEY (cod_evaluacion);


--
-- TOC entry 3929 (class 2606 OID 19138)
-- Name: tsgrhexperienciaslaborales tsgrhexperienciaslaborales_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhexperienciaslaborales
    ADD CONSTRAINT tsgrhexperienciaslaborales_pkey PRIMARY KEY (cod_experiencia);


--
-- TOC entry 3931 (class 2606 OID 19140)
-- Name: tsgrhfactoreseva tsgrhfactoreseva_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhfactoreseva
    ADD CONSTRAINT tsgrhfactoreseva_pkey PRIMARY KEY (cod_factor);


--
-- TOC entry 3933 (class 2606 OID 19142)
-- Name: tsgrhidiomas tsgrhidiomas_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhidiomas
    ADD CONSTRAINT tsgrhidiomas_pkey PRIMARY KEY (cod_idioma);


--
-- TOC entry 3943 (class 2606 OID 19144)
-- Name: tsgrhperfiles tsgrhperfiles_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhperfiles
    ADD CONSTRAINT tsgrhperfiles_pkey PRIMARY KEY (cod_perfil);


--
-- TOC entry 3945 (class 2606 OID 19146)
-- Name: tsgrhplancapacitacion tsgrhplancapacitacion_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhplancapacitacion
    ADD CONSTRAINT tsgrhplancapacitacion_pkey PRIMARY KEY (cod_plancapacitacion);


--
-- TOC entry 3947 (class 2606 OID 19148)
-- Name: tsgrhplanoperativo tsgrhplanoperativo_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhplanoperativo
    ADD CONSTRAINT tsgrhplanoperativo_pkey PRIMARY KEY (cod_planoperativo);


--
-- TOC entry 3949 (class 2606 OID 19150)
-- Name: tsgrhpreguntasenc tsgrhpreguntasenc_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhpreguntasenc
    ADD CONSTRAINT tsgrhpreguntasenc_pkey PRIMARY KEY (cod_pregunta);


--
-- TOC entry 3951 (class 2606 OID 19152)
-- Name: tsgrhpreguntaseva tsgrhpreguntaseva_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhpreguntaseva
    ADD CONSTRAINT tsgrhpreguntaseva_pkey PRIMARY KEY (cod_pregunta);


--
-- TOC entry 3957 (class 2606 OID 19154)
-- Name: tsgrhpuestos tsgrhpuestos_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhpuestos
    ADD CONSTRAINT tsgrhpuestos_pkey PRIMARY KEY (cod_puesto);


--
-- TOC entry 3935 (class 2606 OID 19156)
-- Name: tsgrhlogistica tsgrhreglogistica_cod_capacitacion_key; Type: CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhlogistica
    ADD CONSTRAINT tsgrhreglogistica_cod_capacitacion_key UNIQUE (cod_capacitacion);


--
-- TOC entry 3937 (class 2606 OID 19158)
-- Name: tsgrhlogistica tsgrhreglogistica_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhlogistica
    ADD CONSTRAINT tsgrhreglogistica_pkey PRIMARY KEY (cod_logistica);


--
-- TOC entry 3961 (class 2606 OID 19160)
-- Name: tsgrhrespuestasenc tsgrhrespuestasenc_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhrespuestasenc
    ADD CONSTRAINT tsgrhrespuestasenc_pkey PRIMARY KEY (cod_respuesta);


--
-- TOC entry 3963 (class 2606 OID 19162)
-- Name: tsgrhrespuestaseva tsgrhrespuestaseva_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhrespuestaseva
    ADD CONSTRAINT tsgrhrespuestaseva_pkey PRIMARY KEY (cod_respuesta);


--
-- TOC entry 3965 (class 2606 OID 19164)
-- Name: tsgrhrevplanoperativo tsgrhrevplanoperativo_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhrevplanoperativo
    ADD CONSTRAINT tsgrhrevplanoperativo_pkey PRIMARY KEY (cod_revplanoperativo);


--
-- TOC entry 3969 (class 2606 OID 19166)
-- Name: tsgrhsubfactoreseva tsgrhsubfactoreseva_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhsubfactoreseva
    ADD CONSTRAINT tsgrhsubfactoreseva_pkey PRIMARY KEY (cod_subfactor);


--
-- TOC entry 3973 (class 2606 OID 19168)
-- Name: tsgrhvalidaevaluaciondes tsgrhvalidaevaluaciondes_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhvalidaevaluaciondes
    ADD CONSTRAINT tsgrhvalidaevaluaciondes_pkey PRIMARY KEY (cod_validacion);


--
-- TOC entry 3749 (class 2606 OID 16818)
-- Name: tsgrtagenda cod_agenda; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtagenda
    ADD CONSTRAINT cod_agenda UNIQUE (cod_agenda);


--
-- TOC entry 3753 (class 2606 OID 16820)
-- Name: tsgrtarchivos cod_archivo; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtarchivos
    ADD CONSTRAINT cod_archivo UNIQUE (cod_archivo);


--
-- TOC entry 3759 (class 2606 OID 16822)
-- Name: tsgrtasistentes cod_asistente; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtasistentes
    ADD CONSTRAINT cod_asistente UNIQUE (cod_asistente);


--
-- TOC entry 3763 (class 2606 OID 16824)
-- Name: tsgrtattchticket cod_attach; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtattchticket
    ADD CONSTRAINT cod_attach UNIQUE (cod_attach);


--
-- TOC entry 3824 (class 2606 OID 16826)
-- Name: tsgrtfaq cod_categofaq; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtfaq
    ADD CONSTRAINT cod_categofaq UNIQUE (cod_categoriafaq);


--
-- TOC entry 3775 (class 2606 OID 16828)
-- Name: tsgrtcategoriafaq cod_categoriafaq; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtcategoriafaq
    ADD CONSTRAINT cod_categoriafaq UNIQUE (cod_categoriafaq);


--
-- TOC entry 3783 (class 2606 OID 16830)
-- Name: tsgrtcomentariosagenda cod_comentsagenda; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtcomentariosagenda
    ADD CONSTRAINT cod_comentsagenda UNIQUE (cod_comentsagenda);


--
-- TOC entry 3796 (class 2606 OID 16832)
-- Name: tsgrtcorreo cod_correo; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtcorreo
    ADD CONSTRAINT cod_correo UNIQUE (cod_correo);


--
-- TOC entry 3802 (class 2606 OID 16834)
-- Name: tsgrtdatossolicitud cod_datosolicitud; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtdatossolicitud
    ADD CONSTRAINT cod_datosolicitud UNIQUE (cod_datosolicitud);


--
-- TOC entry 3806 (class 2606 OID 16836)
-- Name: tsgrtdepartamento cod_depto; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtdepartamento
    ADD CONSTRAINT cod_depto UNIQUE (cod_depto);


--
-- TOC entry 3883 (class 2606 OID 16838)
-- Name: tsgrtticket cod_deptoticket; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtticket
    ADD CONSTRAINT cod_deptoticket UNIQUE (cod_depto);


--
-- TOC entry 3812 (class 2606 OID 16840)
-- Name: tsgrtedosolicitudes cod_edosolicitud; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtedosolicitudes
    ADD CONSTRAINT cod_edosolicitud UNIQUE (cod_edosolicitud);


--
-- TOC entry 3816 (class 2606 OID 16842)
-- Name: tsgrtelementos cod_elemento; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtelementos
    ADD CONSTRAINT cod_elemento UNIQUE (cod_elemento);


--
-- TOC entry 3885 (class 2606 OID 16844)
-- Name: tsgrtticket cod_empleado; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtticket
    ADD CONSTRAINT cod_empleado UNIQUE (cod_empleado);


--
-- TOC entry 3820 (class 2606 OID 16846)
-- Name: tsgrtestados cod_estadorep; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtestados
    ADD CONSTRAINT cod_estadorep UNIQUE (cod_estadorep);


--
-- TOC entry 3826 (class 2606 OID 16848)
-- Name: tsgrtfaq cod_faq; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtfaq
    ADD CONSTRAINT cod_faq UNIQUE (cod_faq);


--
-- TOC entry 3830 (class 2606 OID 16850)
-- Name: tsgrtgrupo cod_grupo; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtgrupo
    ADD CONSTRAINT cod_grupo UNIQUE (cod_grupo);


--
-- TOC entry 3834 (class 2606 OID 16852)
-- Name: tsgrtinvitados cod_invitado; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtinvitados
    ADD CONSTRAINT cod_invitado UNIQUE (cod_invitado);


--
-- TOC entry 3839 (class 2606 OID 16854)
-- Name: tsgrtlugares cod_lugar; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtlugares
    ADD CONSTRAINT cod_lugar UNIQUE (cod_lugar);


--
-- TOC entry 3843 (class 2606 OID 16856)
-- Name: tsgrtmsjticket cod_mensaje; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtmsjticket
    ADD CONSTRAINT cod_mensaje UNIQUE (cod_mensaje);


--
-- TOC entry 3847 (class 2606 OID 16858)
-- Name: tsgrtnota cod_nota; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtnota
    ADD CONSTRAINT cod_nota UNIQUE (cod_nota);


--
-- TOC entry 3851 (class 2606 OID 16860)
-- Name: tsgrtplantillacorreos cod_plantillacorreo; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtplantillacorreos
    ADD CONSTRAINT cod_plantillacorreo UNIQUE (cod_plantillacorreo);


--
-- TOC entry 3855 (class 2606 OID 16862)
-- Name: tsgrtprioridad cod_prioridad; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtprioridad
    ADD CONSTRAINT cod_prioridad UNIQUE (cod_prioridad);


--
-- TOC entry 3867 (class 2606 OID 16864)
-- Name: tsgrtrespuesta cod_respuesta; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtrespuesta
    ADD CONSTRAINT cod_respuesta UNIQUE (cod_respuesta);


--
-- TOC entry 3861 (class 2606 OID 16866)
-- Name: tsgrtresppredefinida cod_respuestapredf; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtresppredefinida
    ADD CONSTRAINT cod_respuestapredf UNIQUE (cod_respuesta);


--
-- TOC entry 3755 (class 2606 OID 16868)
-- Name: tsgrtarchivos cod_reunionarchivos; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtarchivos
    ADD CONSTRAINT cod_reunionarchivos UNIQUE (cod_reunion);


--
-- TOC entry 3871 (class 2606 OID 16870)
-- Name: tsgrtreuniones cod_reuniones; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtreuniones
    ADD CONSTRAINT cod_reuniones UNIQUE (cod_reunion);


--
-- TOC entry 3875 (class 2606 OID 16872)
-- Name: tsgrtservicios cod_servicio; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtservicios
    ADD CONSTRAINT cod_servicio UNIQUE (cod_servicio);


--
-- TOC entry 3879 (class 2606 OID 16874)
-- Name: tsgrtsolicitudservicios cod_solicitud; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtsolicitudservicios
    ADD CONSTRAINT cod_solicitud UNIQUE (cod_solicitud);


--
-- TOC entry 3887 (class 2606 OID 16876)
-- Name: tsgrtticket cod_ticket; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtticket
    ADD CONSTRAINT cod_ticket UNIQUE (cod_ticket);


--
-- TOC entry 3765 (class 2606 OID 16878)
-- Name: tsgrtattchticket cod_ticketattach; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtattchticket
    ADD CONSTRAINT cod_ticketattach UNIQUE (cod_ticket);


--
-- TOC entry 3769 (class 2606 OID 16880)
-- Name: tsgrtayudatopico cod_topico; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtayudatopico
    ADD CONSTRAINT cod_topico UNIQUE (cod_topico);


--
-- TOC entry 3751 (class 2606 OID 16898)
-- Name: tsgrtagenda tsgrtagenda_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtagenda
    ADD CONSTRAINT tsgrtagenda_pkey PRIMARY KEY (cod_agenda);


--
-- TOC entry 3757 (class 2606 OID 16900)
-- Name: tsgrtarchivos tsgrtarchivos_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtarchivos
    ADD CONSTRAINT tsgrtarchivos_pkey PRIMARY KEY (cod_archivo);


--
-- TOC entry 3761 (class 2606 OID 16902)
-- Name: tsgrtasistentes tsgrtasistentes_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtasistentes
    ADD CONSTRAINT tsgrtasistentes_pkey PRIMARY KEY (cod_asistente);


--
-- TOC entry 3767 (class 2606 OID 16904)
-- Name: tsgrtattchticket tsgrtattchticket_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtattchticket
    ADD CONSTRAINT tsgrtattchticket_pkey PRIMARY KEY (cod_attach);


--
-- TOC entry 3771 (class 2606 OID 16884)
-- Name: tsgrtayudatopico tsgrtayudatopico_des_topico_key; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtayudatopico
    ADD CONSTRAINT tsgrtayudatopico_des_topico_key UNIQUE (des_topico);


--
-- TOC entry 3773 (class 2606 OID 16906)
-- Name: tsgrtayudatopico tsgrtayudatopico_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtayudatopico
    ADD CONSTRAINT tsgrtayudatopico_pkey PRIMARY KEY (cod_topico);


--
-- TOC entry 3777 (class 2606 OID 16908)
-- Name: tsgrtcategoriafaq tsgrtcategoriafaq_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtcategoriafaq
    ADD CONSTRAINT tsgrtcategoriafaq_pkey PRIMARY KEY (cod_categoriafaq);


--
-- TOC entry 3779 (class 2606 OID 16910)
-- Name: tsgrtchat tsgrtchat_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtchat
    ADD CONSTRAINT tsgrtchat_pkey PRIMARY KEY (cod_chat);


--
-- TOC entry 3781 (class 2606 OID 16912)
-- Name: tsgrtciudades tsgrtciudades_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtciudades
    ADD CONSTRAINT tsgrtciudades_pkey PRIMARY KEY (cod_ciudad);


--
-- TOC entry 3786 (class 2606 OID 16914)
-- Name: tsgrtcomentariosagenda tsgrtcomentariosagenda_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtcomentariosagenda
    ADD CONSTRAINT tsgrtcomentariosagenda_pkey PRIMARY KEY (cod_comentsagenda);


--
-- TOC entry 3789 (class 2606 OID 16916)
-- Name: tsgrtcomentariosreunion tsgrtcomentariosreunion_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtcomentariosreunion
    ADD CONSTRAINT tsgrtcomentariosreunion_pkey PRIMARY KEY (cod_commentsreunion);


--
-- TOC entry 3792 (class 2606 OID 16918)
-- Name: tsgrtcompromisos tsgrtcompromisos_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtcompromisos
    ADD CONSTRAINT tsgrtcompromisos_pkey PRIMARY KEY (cod_compromiso);


--
-- TOC entry 3798 (class 2606 OID 16882)
-- Name: tsgrtcorreo tsgrtcorreo_des_correo_key; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtcorreo
    ADD CONSTRAINT tsgrtcorreo_des_correo_key UNIQUE (des_correo);


--
-- TOC entry 3800 (class 2606 OID 16942)
-- Name: tsgrtcorreo tsgrtcorreo_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtcorreo
    ADD CONSTRAINT tsgrtcorreo_pkey PRIMARY KEY (cod_correo);


--
-- TOC entry 3804 (class 2606 OID 16944)
-- Name: tsgrtdatossolicitud tsgrtdatossolicitud_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtdatossolicitud
    ADD CONSTRAINT tsgrtdatossolicitud_pkey PRIMARY KEY (cod_datosolicitud);


--
-- TOC entry 3808 (class 2606 OID 16886)
-- Name: tsgrtdepartamento tsgrtdepartamento_des_nombre_key; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtdepartamento
    ADD CONSTRAINT tsgrtdepartamento_des_nombre_key UNIQUE (des_nombre);


--
-- TOC entry 3810 (class 2606 OID 16920)
-- Name: tsgrtdepartamento tsgrtdepartamento_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtdepartamento
    ADD CONSTRAINT tsgrtdepartamento_pkey PRIMARY KEY (cod_depto);


--
-- TOC entry 3814 (class 2606 OID 16922)
-- Name: tsgrtedosolicitudes tsgrtedosolicitudes_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtedosolicitudes
    ADD CONSTRAINT tsgrtedosolicitudes_pkey PRIMARY KEY (cod_edosolicitud);


--
-- TOC entry 3818 (class 2606 OID 16924)
-- Name: tsgrtelementos tsgrtelementos_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtelementos
    ADD CONSTRAINT tsgrtelementos_pkey PRIMARY KEY (cod_elemento);


--
-- TOC entry 3822 (class 2606 OID 16926)
-- Name: tsgrtestados tsgrtestados_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtestados
    ADD CONSTRAINT tsgrtestados_pkey PRIMARY KEY (cod_estadorep);


--
-- TOC entry 3828 (class 2606 OID 16928)
-- Name: tsgrtfaq tsgrtfaq_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtfaq
    ADD CONSTRAINT tsgrtfaq_pkey PRIMARY KEY (cod_faq);


--
-- TOC entry 3832 (class 2606 OID 16930)
-- Name: tsgrtgrupo tsgrtgrupo_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtgrupo
    ADD CONSTRAINT tsgrtgrupo_pkey PRIMARY KEY (cod_grupo);


--
-- TOC entry 3837 (class 2606 OID 16932)
-- Name: tsgrtinvitados tsgrtinvitados_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtinvitados
    ADD CONSTRAINT tsgrtinvitados_pkey PRIMARY KEY (cod_invitado);


--
-- TOC entry 3841 (class 2606 OID 16934)
-- Name: tsgrtlugares tsgrtlugares_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtlugares
    ADD CONSTRAINT tsgrtlugares_pkey PRIMARY KEY (cod_lugar);


--
-- TOC entry 3845 (class 2606 OID 16936)
-- Name: tsgrtmsjticket tsgrtmsjticket_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtmsjticket
    ADD CONSTRAINT tsgrtmsjticket_pkey PRIMARY KEY (cod_mensaje);


--
-- TOC entry 3849 (class 2606 OID 16938)
-- Name: tsgrtnota tsgrtnota_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtnota
    ADD CONSTRAINT tsgrtnota_pkey PRIMARY KEY (cod_nota);


--
-- TOC entry 3853 (class 2606 OID 16940)
-- Name: tsgrtplantillacorreos tsgrtplantillacorreos_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtplantillacorreos
    ADD CONSTRAINT tsgrtplantillacorreos_pkey PRIMARY KEY (cod_plantillacorreo);


--
-- TOC entry 3857 (class 2606 OID 16888)
-- Name: tsgrtprioridad tsgrtprioridad_des_nombre_key; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtprioridad
    ADD CONSTRAINT tsgrtprioridad_des_nombre_key UNIQUE (des_nombre);


--
-- TOC entry 3859 (class 2606 OID 16946)
-- Name: tsgrtprioridad tsgrtprioridad_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtprioridad
    ADD CONSTRAINT tsgrtprioridad_pkey PRIMARY KEY (cod_prioridad);


--
-- TOC entry 3863 (class 2606 OID 16890)
-- Name: tsgrtresppredefinida tsgrtresppredefinida_des_titulo_key; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtresppredefinida
    ADD CONSTRAINT tsgrtresppredefinida_des_titulo_key UNIQUE (des_titulo);


--
-- TOC entry 3865 (class 2606 OID 16948)
-- Name: tsgrtresppredefinida tsgrtresppredefinida_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtresppredefinida
    ADD CONSTRAINT tsgrtresppredefinida_pkey PRIMARY KEY (cod_respuesta);


--
-- TOC entry 3869 (class 2606 OID 16950)
-- Name: tsgrtrespuesta tsgrtrespuesta_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtrespuesta
    ADD CONSTRAINT tsgrtrespuesta_pkey PRIMARY KEY (cod_respuesta);


--
-- TOC entry 3873 (class 2606 OID 16952)
-- Name: tsgrtreuniones tsgrtreuniones_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtreuniones
    ADD CONSTRAINT tsgrtreuniones_pkey PRIMARY KEY (cod_reunion);


--
-- TOC entry 3877 (class 2606 OID 16954)
-- Name: tsgrtservicios tsgrtservicios_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtservicios
    ADD CONSTRAINT tsgrtservicios_pkey PRIMARY KEY (cod_servicio);


--
-- TOC entry 3881 (class 2606 OID 16956)
-- Name: tsgrtsolicitudservicios tsgrtsolicitudservicios_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtsolicitudservicios
    ADD CONSTRAINT tsgrtsolicitudservicios_pkey PRIMARY KEY (cod_solicitud);


--
-- TOC entry 3889 (class 2606 OID 16892)
-- Name: tsgrtticket tsgrtticket_des_correo_key; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtticket
    ADD CONSTRAINT tsgrtticket_des_correo_key UNIQUE (des_correo);


--
-- TOC entry 3891 (class 2606 OID 16894)
-- Name: tsgrtticket tsgrtticket_des_folio_key; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtticket
    ADD CONSTRAINT tsgrtticket_des_folio_key UNIQUE (des_folio);


--
-- TOC entry 3893 (class 2606 OID 16958)
-- Name: tsgrtticket tsgrtticket_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtticket
    ADD CONSTRAINT tsgrtticket_pkey PRIMARY KEY (cod_ticket);


--
-- TOC entry 3794 (class 2606 OID 16896)
-- Name: tsgrtcompromisos unique_cod_chat; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtcompromisos
    ADD CONSTRAINT unique_cod_chat UNIQUE (cod_chat);


--
-- TOC entry 4027 (class 2606 OID 22145)
-- Name: tsisatappservices tsisatappservices_pkey; Type: CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatappservices
    ADD CONSTRAINT tsisatappservices_pkey PRIMARY KEY (cod_appservice);


--
-- TOC entry 4029 (class 2606 OID 22147)
-- Name: tsisatarquitecturas tsisatarquitecturas_pkey; Type: CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatarquitecturas
    ADD CONSTRAINT tsisatarquitecturas_pkey PRIMARY KEY (cod_arquitectura);


--
-- TOC entry 4031 (class 2606 OID 22149)
-- Name: tsisatasignaciones tsisatasignaciones_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatasignaciones
    ADD CONSTRAINT tsisatasignaciones_pkey PRIMARY KEY (cod_asignacion);


--
-- TOC entry 4033 (class 2606 OID 22151)
-- Name: tsisatcandidatos tsisatcandidatos_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcandidatos
    ADD CONSTRAINT tsisatcandidatos_pkey PRIMARY KEY (cod_candidato);


--
-- TOC entry 4035 (class 2606 OID 22153)
-- Name: tsisatcartaaceptacion tsisatcartaaceptacion_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcartaaceptacion
    ADD CONSTRAINT tsisatcartaaceptacion_pkey PRIMARY KEY (cod_aceptacion);


--
-- TOC entry 4037 (class 2606 OID 22155)
-- Name: tsisatcartaasignacion tsisatcartaasignacion_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcartaasignacion
    ADD CONSTRAINT tsisatcartaasignacion_pkey PRIMARY KEY (cod_asignacion);


--
-- TOC entry 4039 (class 2606 OID 22157)
-- Name: tsisatcomentarios tsisatcomentarios_pkey; Type: CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatcomentarios
    ADD CONSTRAINT tsisatcomentarios_pkey PRIMARY KEY (cod_comentario);


--
-- TOC entry 4041 (class 2606 OID 22159)
-- Name: tsisatcotizaciones tsisatcotizaciones_pkey; Type: CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatcotizaciones
    ADD CONSTRAINT tsisatcotizaciones_pkey PRIMARY KEY (cod_cotizacion);


--
-- TOC entry 4043 (class 2606 OID 22161)
-- Name: tsisatcursosycertificados tsisatcursosycerticados_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcursosycertificados
    ADD CONSTRAINT tsisatcursosycerticados_pkey PRIMARY KEY (cod_curso);


--
-- TOC entry 4045 (class 2606 OID 22163)
-- Name: tsisatentrevistas tsisatentrevistas_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatentrevistas
    ADD CONSTRAINT tsisatentrevistas_pkey PRIMARY KEY (cod_entrevista);


--
-- TOC entry 4047 (class 2606 OID 22165)
-- Name: tsisatenviocorreos tsisatenviocorreos_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatenviocorreos
    ADD CONSTRAINT tsisatenviocorreos_pkey PRIMARY KEY (cod_envio);


--
-- TOC entry 4049 (class 2606 OID 22167)
-- Name: tsisatescolaridad tsisatescolaridad_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatescolaridad
    ADD CONSTRAINT tsisatescolaridad_pkey PRIMARY KEY (cod_escolaridad);


--
-- TOC entry 4051 (class 2606 OID 22169)
-- Name: tsisatexperienciaslaborales tsisatexperienciaslaborales_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatexperienciaslaborales
    ADD CONSTRAINT tsisatexperienciaslaborales_pkey PRIMARY KEY (cod_experiencia);


--
-- TOC entry 4053 (class 2606 OID 22171)
-- Name: tsisatfirmas tsisatfirmas_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatfirmas
    ADD CONSTRAINT tsisatfirmas_pkey PRIMARY KEY (cod_firma);


--
-- TOC entry 4055 (class 2606 OID 22173)
-- Name: tsisatframeworks tsisatframeworks_pkey; Type: CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatframeworks
    ADD CONSTRAINT tsisatframeworks_pkey PRIMARY KEY (cod_framework);


--
-- TOC entry 4057 (class 2606 OID 22175)
-- Name: tsisathabilidades tsisathabilidades_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisathabilidades
    ADD CONSTRAINT tsisathabilidades_pkey PRIMARY KEY (cod_habilidad);


--
-- TOC entry 4059 (class 2606 OID 22177)
-- Name: tsisatherramientas tsisatherramientas_pkey; Type: CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatherramientas
    ADD CONSTRAINT tsisatherramientas_pkey PRIMARY KEY (cod_herramientas);


--
-- TOC entry 4061 (class 2606 OID 22179)
-- Name: tsisatides tsisatides_pkey; Type: CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatides
    ADD CONSTRAINT tsisatides_pkey PRIMARY KEY (cod_ide);


--
-- TOC entry 4063 (class 2606 OID 22181)
-- Name: tsisatidiomas tsisatidiomas_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatidiomas
    ADD CONSTRAINT tsisatidiomas_pkey PRIMARY KEY (cod_idioma);


--
-- TOC entry 4065 (class 2606 OID 22183)
-- Name: tsisatlenguajes tsisatlenguajes_pkey; Type: CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatlenguajes
    ADD CONSTRAINT tsisatlenguajes_pkey PRIMARY KEY (cod_lenguaje);


--
-- TOC entry 4067 (class 2606 OID 22185)
-- Name: tsisatmaquetados tsisatmaquetados_pkey; Type: CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatmaquetados
    ADD CONSTRAINT tsisatmaquetados_pkey PRIMARY KEY (cod_maquetado);


--
-- TOC entry 4069 (class 2606 OID 22187)
-- Name: tsisatmetodologias tsisatmetodologias_pkey; Type: CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatmetodologias
    ADD CONSTRAINT tsisatmetodologias_pkey PRIMARY KEY (cod_metodologia);


--
-- TOC entry 4071 (class 2606 OID 22189)
-- Name: tsisatmodelados tsisatmodelados_pkey; Type: CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatmodelados
    ADD CONSTRAINT tsisatmodelados_pkey PRIMARY KEY (cod_modelado);


--
-- TOC entry 4073 (class 2606 OID 22191)
-- Name: tsisatordenservicio tsisatordenservicio_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatordenservicio
    ADD CONSTRAINT tsisatordenservicio_pkey PRIMARY KEY (cod_ordenservicio);


--
-- TOC entry 4075 (class 2606 OID 22193)
-- Name: tsisatpatrones tsisatpatrones_pkey; Type: CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatpatrones
    ADD CONSTRAINT tsisatpatrones_pkey PRIMARY KEY (cod_patron);


--
-- TOC entry 4077 (class 2606 OID 22195)
-- Name: tsisatprospectos tsisatprospectos_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatprospectos
    ADD CONSTRAINT tsisatprospectos_pkey PRIMARY KEY (cod_prospecto);


--
-- TOC entry 4079 (class 2606 OID 22197)
-- Name: tsisatprotocolos tsisatprotocolos_pkey; Type: CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatprotocolos
    ADD CONSTRAINT tsisatprotocolos_pkey PRIMARY KEY (cod_protocolo);


--
-- TOC entry 4081 (class 2606 OID 22199)
-- Name: tsisatproyectos tsisatproyectos_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatproyectos
    ADD CONSTRAINT tsisatproyectos_pkey PRIMARY KEY (cod_proyecto);


--
-- TOC entry 4083 (class 2606 OID 22201)
-- Name: tsisatqa tsisatqa_pkey; Type: CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatqa
    ADD CONSTRAINT tsisatqa_pkey PRIMARY KEY (cod_qa);


--
-- TOC entry 4085 (class 2606 OID 22203)
-- Name: tsisatrepositoriolibrerias tsisatrepositoriolibrerias_pkey; Type: CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatrepositoriolibrerias
    ADD CONSTRAINT tsisatrepositoriolibrerias_pkey PRIMARY KEY (cod_repositoriolibreria);


--
-- TOC entry 4087 (class 2606 OID 22205)
-- Name: tsisatrepositorios tsisatrepositorios_pkey; Type: CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatrepositorios
    ADD CONSTRAINT tsisatrepositorios_pkey PRIMARY KEY (cod_repositorio);


--
-- TOC entry 4089 (class 2606 OID 22207)
-- Name: tsisatsgbd tsisatsgbd_pkey; Type: CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatsgbd
    ADD CONSTRAINT tsisatsgbd_pkey PRIMARY KEY (cod_sgbd);


--
-- TOC entry 4091 (class 2606 OID 22209)
-- Name: tsisatso tsisatso_pkey; Type: CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatso
    ADD CONSTRAINT tsisatso_pkey PRIMARY KEY (cod_so);


--
-- TOC entry 4093 (class 2606 OID 22211)
-- Name: tsisatvacantes tsisatvacantes_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatvacantes
    ADD CONSTRAINT tsisatvacantes_pkey PRIMARY KEY (cod_vacante);


--
-- TOC entry 3790 (class 1259 OID 16959)
-- Name: fki_fk_cod_chat; Type: INDEX; Schema: sgrt; Owner: postgres
--

CREATE INDEX fki_fk_cod_chat ON sgrt.tsgrtcompromisos USING btree (cod_chat);


--
-- TOC entry 3835 (class 1259 OID 16960)
-- Name: fki_fk_cod_empleado; Type: INDEX; Schema: sgrt; Owner: postgres
--

CREATE INDEX fki_fk_cod_empleado ON sgrt.tsgrtinvitados USING btree (cod_empleado);


--
-- TOC entry 3787 (class 1259 OID 16961)
-- Name: fki_fk_cod_invitado; Type: INDEX; Schema: sgrt; Owner: postgres
--

CREATE INDEX fki_fk_cod_invitado ON sgrt.tsgrtcomentariosreunion USING btree (cod_invitado);


--
-- TOC entry 3784 (class 1259 OID 16962)
-- Name: fki_fk_cod_invitados; Type: INDEX; Schema: sgrt; Owner: postgres
--

CREATE INDEX fki_fk_cod_invitados ON sgrt.tsgrtcomentariosagenda USING btree (cod_invitado);


--
-- TOC entry 4366 (class 2620 OID 19169)
-- Name: tsgrhencuesta tg_actualizarfecha; Type: TRIGGER; Schema: sgrh; Owner: postgres
--

CREATE TRIGGER tg_actualizarfecha BEFORE UPDATE ON sgrh.tsgrhencuesta FOR EACH ROW EXECUTE PROCEDURE sgrh.factualizarfecha();


--
-- TOC entry 4094 (class 2606 OID 19170)
-- Name: tsgcousuarios fk_cod_empleado; Type: FK CONSTRAINT; Schema: sgco; Owner: postgres
--

ALTER TABLE ONLY sgco.tsgcousuarios
    ADD CONSTRAINT fk_cod_empleado FOREIGN KEY (cod_empleado) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4290 (class 2606 OID 21024)
-- Name: tsgnomcncptoquincht concepto_quincena_id_copia_fk_conceptos_quincena; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomcncptoquincht
    ADD CONSTRAINT concepto_quincena_id_copia_fk_conceptos_quincena FOREIGN KEY (cod_conceptoid_fk) REFERENCES sgnom.tsgnomconcepto(cod_conceptoid) ON UPDATE CASCADE;


--
-- TOC entry 4288 (class 2606 OID 21029)
-- Name: tsgnomcncptoquinc concepto_quincena_id_fk_conceptos_quincena; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomcncptoquinc
    ADD CONSTRAINT concepto_quincena_id_fk_conceptos_quincena FOREIGN KEY (cod_conceptoid_fk) REFERENCES sgnom.tsgnomconcepto(cod_conceptoid) ON UPDATE CASCADE;


--
-- TOC entry 4291 (class 2606 OID 21034)
-- Name: tsgnomcncptoquincht empleado_concepto_id_copia_fk_conceptos_quincena; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomcncptoquincht
    ADD CONSTRAINT empleado_concepto_id_copia_fk_conceptos_quincena FOREIGN KEY (cod_empquincenaid_fk) REFERENCES sgnom.tsgnomempquincena(cod_empquincenaid) ON UPDATE CASCADE;


--
-- TOC entry 4289 (class 2606 OID 21039)
-- Name: tsgnomcncptoquinc empleado_concepto_id_fk_conceptos_quincena; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomcncptoquinc
    ADD CONSTRAINT empleado_concepto_id_fk_conceptos_quincena FOREIGN KEY (cod_empquincenaid_fk) REFERENCES sgnom.tsgnomempquincena(cod_empquincenaid) ON UPDATE CASCADE;


--
-- TOC entry 4304 (class 2606 OID 21044)
-- Name: tsgnomempquincena nom_cabecera_id_fk_empleados_quincena; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomempquincena
    ADD CONSTRAINT nom_cabecera_id_fk_empleados_quincena FOREIGN KEY (cod_cabeceraid_fk) REFERENCES sgnom.tsgnomcabecera(cod_cabeceraid) ON UPDATE CASCADE;


--
-- TOC entry 4306 (class 2606 OID 21049)
-- Name: tsgnomempquincenaht nom_cabecera_id_fk_empleados_quincena_copia; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomempquincenaht
    ADD CONSTRAINT nom_cabecera_id_fk_empleados_quincena_copia FOREIGN KEY (cod_cabeceraid_fk) REFERENCES sgnom.tsgnomcabecera(cod_cabeceraid) ON UPDATE CASCADE;


--
-- TOC entry 4292 (class 2606 OID 21054)
-- Name: tsgnomconcepto nom_cat_clasificador_id_fk_cat_conceptos; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomconcepto
    ADD CONSTRAINT nom_cat_clasificador_id_fk_cat_conceptos FOREIGN KEY (cod_clasificadorid_fk) REFERENCES sgnom.tsgnomclasificador(cod_clasificadorid) ON UPDATE CASCADE;


--
-- TOC entry 4314 (class 2606 OID 21059)
-- Name: tsgnommanterceros nom_cat_conceptos_fk_manuales_terceros; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnommanterceros
    ADD CONSTRAINT nom_cat_conceptos_fk_manuales_terceros FOREIGN KEY (cod_conceptoid_fk) REFERENCES sgnom.tsgnomconcepto(cod_conceptoid) ON UPDATE CASCADE;


--
-- TOC entry 4320 (class 2606 OID 21064)
-- Name: tsgnomquincena nom_cat_ejercicio_id_fk_cat_quincenas; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomquincena
    ADD CONSTRAINT nom_cat_ejercicio_id_fk_cat_quincenas FOREIGN KEY (cod_ejercicioid_fk) REFERENCES sgnom.tsgnomejercicio(cod_ejercicioid) ON UPDATE CASCADE;


--
-- TOC entry 4283 (class 2606 OID 21069)
-- Name: tsgnomcabeceraht nom_cat_estatus_nomina_id_copia_fk_cabeceras; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomcabeceraht
    ADD CONSTRAINT nom_cat_estatus_nomina_id_copia_fk_cabeceras FOREIGN KEY (cod_estatusnomid_fk) REFERENCES sgnom.tsgnomestatusnom(cod_estatusnomid) ON UPDATE CASCADE;


--
-- TOC entry 4278 (class 2606 OID 21074)
-- Name: tsgnomcabecera nom_cat_estatus_nomina_id_fk_cabeceras; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomcabecera
    ADD CONSTRAINT nom_cat_estatus_nomina_id_fk_cabeceras FOREIGN KEY (cod_estatusnomid_fk) REFERENCES sgnom.tsgnomestatusnom(cod_estatusnomid) ON UPDATE CASCADE;


--
-- TOC entry 4308 (class 2606 OID 21079)
-- Name: tsgnomincidencia nom_cat_incidencia_id_fk_incidencias; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomincidencia
    ADD CONSTRAINT nom_cat_incidencia_id_fk_incidencias FOREIGN KEY (cod_catincidenciaid_fk) REFERENCES sgnom.tsgnomcatincidencia(cod_catincidenciaid) ON UPDATE CASCADE;


--
-- TOC entry 4284 (class 2606 OID 21084)
-- Name: tsgnomcabeceraht nom_cat_quincena_id_copia_fk_cabeceras; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomcabeceraht
    ADD CONSTRAINT nom_cat_quincena_id_copia_fk_cabeceras FOREIGN KEY (cod_quincenaid_fk) REFERENCES sgnom.tsgnomquincena(cod_quincenaid) ON UPDATE CASCADE;


--
-- TOC entry 4279 (class 2606 OID 21089)
-- Name: tsgnomcabecera nom_cat_quincena_id_fk_cabeceras; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomcabecera
    ADD CONSTRAINT nom_cat_quincena_id_fk_cabeceras FOREIGN KEY (cod_quincenaid_fk) REFERENCES sgnom.tsgnomquincena(cod_quincenaid) ON UPDATE CASCADE;


--
-- TOC entry 4309 (class 2606 OID 21094)
-- Name: tsgnomincidencia nom_cat_quincena_id_fk_incidencias; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomincidencia
    ADD CONSTRAINT nom_cat_quincena_id_fk_incidencias FOREIGN KEY (cod_quincenaid_fk) REFERENCES sgnom.tsgnomquincena(cod_quincenaid) ON UPDATE CASCADE;


--
-- TOC entry 4315 (class 2606 OID 21099)
-- Name: tsgnommanterceros nom_cat_quincenas_fk_manuales_terceros_fin; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnommanterceros
    ADD CONSTRAINT nom_cat_quincenas_fk_manuales_terceros_fin FOREIGN KEY (cod_quincenafin_fk) REFERENCES sgnom.tsgnomquincena(cod_quincenaid) ON UPDATE CASCADE;


--
-- TOC entry 4316 (class 2606 OID 21104)
-- Name: tsgnommanterceros nom_cat_quincenas_fk_manuales_terceros_inicio; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnommanterceros
    ADD CONSTRAINT nom_cat_quincenas_fk_manuales_terceros_inicio FOREIGN KEY (cod_quincenainicio_fk) REFERENCES sgnom.tsgnomquincena(cod_quincenaid) ON UPDATE CASCADE;


--
-- TOC entry 4277 (class 2606 OID 21109)
-- Name: tsgnombitacora nom_cat_tabla_id_fk_nom_cat_tablas; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnombitacora
    ADD CONSTRAINT nom_cat_tabla_id_fk_nom_cat_tablas FOREIGN KEY (cod_tablaid_fk) REFERENCES sgnom.tsgnomhisttabla(cod_tablaid) ON UPDATE CASCADE;


--
-- TOC entry 4285 (class 2606 OID 21114)
-- Name: tsgnomcabeceraht nom_cat_tipo_nomina_id_copia_fk_cabeceras; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomcabeceraht
    ADD CONSTRAINT nom_cat_tipo_nomina_id_copia_fk_cabeceras FOREIGN KEY (cod_tiponominaid_fk) REFERENCES sgnom.tsgnomtiponomina(cod_tiponominaid) ON UPDATE CASCADE;


--
-- TOC entry 4280 (class 2606 OID 21119)
-- Name: tsgnomcabecera nom_cat_tipo_nomina_id_fk_cabeceras; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomcabecera
    ADD CONSTRAINT nom_cat_tipo_nomina_id_fk_cabeceras FOREIGN KEY (cod_tiponominaid_fk) REFERENCES sgnom.tsgnomtiponomina(cod_tiponominaid) ON UPDATE CASCADE;


--
-- TOC entry 4293 (class 2606 OID 21124)
-- Name: tsgnomconcepto nom_concepto_sat_id_fk_cat_conceptos; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomconcepto
    ADD CONSTRAINT nom_concepto_sat_id_fk_cat_conceptos FOREIGN KEY (cod_conceptosatid_fk) REFERENCES sgnom.tsgnomconceptosat(cod_conceptosatid) ON UPDATE CASCADE;


--
-- TOC entry 4310 (class 2606 OID 21129)
-- Name: tsgnomincidencia nom_emp_autoriza_fk_incidencias; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomincidencia
    ADD CONSTRAINT nom_emp_autoriza_fk_incidencias FOREIGN KEY (cod_empautoriza_fk) REFERENCES sgnom.tsgnomempleados(cod_empleadoid) ON UPDATE CASCADE;


--
-- TOC entry 4311 (class 2606 OID 21134)
-- Name: tsgnomincidencia nom_emp_reporta_fk_incidencias; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomincidencia
    ADD CONSTRAINT nom_emp_reporta_fk_incidencias FOREIGN KEY (cod_empreporta_fk) REFERENCES sgnom.tsgnomempleados(cod_empleadoid) ON UPDATE CASCADE;


--
-- TOC entry 4305 (class 2606 OID 21139)
-- Name: tsgnomempquincena nom_empleado_quincena_id_fk_empleados_quincena; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomempquincena
    ADD CONSTRAINT nom_empleado_quincena_id_fk_empleados_quincena FOREIGN KEY (cod_empleadoid_fk) REFERENCES sgnom.tsgnomempleados(cod_empleadoid) ON UPDATE CASCADE;


--
-- TOC entry 4307 (class 2606 OID 21144)
-- Name: tsgnomempquincenaht nom_empleado_quincena_id_fk_empleados_quincena_copia; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomempquincenaht
    ADD CONSTRAINT nom_empleado_quincena_id_fk_empleados_quincena_copia FOREIGN KEY (cod_empleadoid_fk) REFERENCES sgnom.tsgnomempleados(cod_empleadoid) ON UPDATE CASCADE;


--
-- TOC entry 4317 (class 2606 OID 21149)
-- Name: tsgnommanterceros nom_empleados_fk_manuales_terceros; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnommanterceros
    ADD CONSTRAINT nom_empleados_fk_manuales_terceros FOREIGN KEY (cod_empleadoid_fk) REFERENCES sgnom.tsgnomempleados(cod_empleadoid) ON UPDATE CASCADE;


--
-- TOC entry 4274 (class 2606 OID 21154)
-- Name: tsgnomalguinaldo nom_empleados_quincena_fk_aguinaldos; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomalguinaldo
    ADD CONSTRAINT nom_empleados_quincena_fk_aguinaldos FOREIGN KEY (cod_empquincenaid_fk) REFERENCES sgnom.tsgnomempquincena(cod_empquincenaid) ON UPDATE CASCADE;


--
-- TOC entry 4300 (class 2606 OID 21159)
-- Name: tsgnomconfpago nom_empleados_quincena_fk_conf_pago; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomconfpago
    ADD CONSTRAINT nom_empleados_quincena_fk_conf_pago FOREIGN KEY (cod_empquincenaid_fk) REFERENCES sgnom.tsgnomempquincena(cod_empquincenaid) ON UPDATE CASCADE;


--
-- TOC entry 4294 (class 2606 OID 21164)
-- Name: tsgnomconcepto nom_formula_id_fk_cat_conceptos; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomconcepto
    ADD CONSTRAINT nom_formula_id_fk_cat_conceptos FOREIGN KEY (cod_formulaid_fk) REFERENCES sgnom.tsgnomformula(cod_formulaid) ON UPDATE CASCADE;


--
-- TOC entry 4295 (class 2606 OID 21169)
-- Name: tsgnomconcepto nom_tipo_calculo_id_fk_cat_conceptos; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomconcepto
    ADD CONSTRAINT nom_tipo_calculo_id_fk_cat_conceptos FOREIGN KEY (cod_calculoid_fk) REFERENCES sgnom.tsgnomcalculo(cod_calculoid) ON UPDATE CASCADE;


--
-- TOC entry 4296 (class 2606 OID 21174)
-- Name: tsgnomconcepto nom_tipo_concepto_id_fk_cat_conceptos; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomconcepto
    ADD CONSTRAINT nom_tipo_concepto_id_fk_cat_conceptos FOREIGN KEY (cod_tipoconceptoid_fk) REFERENCES sgnom.tsgnomtipoconcepto(cod_tipoconceptoid) ON UPDATE CASCADE;


--
-- TOC entry 4297 (class 2606 OID 21179)
-- Name: tsgnomconcepto nom_tipo_nomina_id_fk_cat_conceptos; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomconcepto
    ADD CONSTRAINT nom_tipo_nomina_id_fk_cat_conceptos FOREIGN KEY (cod_tiponominaid_fk) REFERENCES sgnom.tsgnomtiponomina(cod_tiponominaid) ON UPDATE CASCADE;


--
-- TOC entry 4286 (class 2606 OID 21184)
-- Name: tsgnomcabeceraht tsgrh_empleados_id_copia_fk_cabeceras_c; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomcabeceraht
    ADD CONSTRAINT tsgrh_empleados_id_copia_fk_cabeceras_c FOREIGN KEY (aud_codcreadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4287 (class 2606 OID 21189)
-- Name: tsgnomcabeceraht tsgrh_empleados_id_copia_fk_cabeceras_m; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomcabeceraht
    ADD CONSTRAINT tsgrh_empleados_id_copia_fk_cabeceras_m FOREIGN KEY (aud_codmodificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4275 (class 2606 OID 21194)
-- Name: tsgnomargumento tsgrh_empleados_id_fk_argumento_c; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomargumento
    ADD CONSTRAINT tsgrh_empleados_id_fk_argumento_c FOREIGN KEY (aud_codcreadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4276 (class 2606 OID 21199)
-- Name: tsgnomargumento tsgrh_empleados_id_fk_argumento_m; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomargumento
    ADD CONSTRAINT tsgrh_empleados_id_fk_argumento_m FOREIGN KEY (aud_codmodificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4281 (class 2606 OID 21204)
-- Name: tsgnomcabecera tsgrh_empleados_id_fk_cabeceras_c; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomcabecera
    ADD CONSTRAINT tsgrh_empleados_id_fk_cabeceras_c FOREIGN KEY (aud_codcreadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4282 (class 2606 OID 21209)
-- Name: tsgnomcabecera tsgrh_empleados_id_fk_cabeceras_m; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomcabecera
    ADD CONSTRAINT tsgrh_empleados_id_fk_cabeceras_m FOREIGN KEY (aud_codmodificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4301 (class 2606 OID 21214)
-- Name: tsgnomempleados tsgrh_empleados_id_fk_cabeceras_m; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomempleados
    ADD CONSTRAINT tsgrh_empleados_id_fk_cabeceras_m FOREIGN KEY (aud_codmodificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4298 (class 2606 OID 21219)
-- Name: tsgnomconcepto tsgrh_empleados_id_fk_cat_conceptos_c; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomconcepto
    ADD CONSTRAINT tsgrh_empleados_id_fk_cat_conceptos_c FOREIGN KEY (aud_codcreadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4299 (class 2606 OID 21224)
-- Name: tsgnomconcepto tsgrh_empleados_id_fk_cat_conceptos_m; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomconcepto
    ADD CONSTRAINT tsgrh_empleados_id_fk_cat_conceptos_m FOREIGN KEY (aud_codmodificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4302 (class 2606 OID 21229)
-- Name: tsgnomempleados tsgrh_empleados_id_fk_empleados; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomempleados
    ADD CONSTRAINT tsgrh_empleados_id_fk_empleados FOREIGN KEY (cod_empleado_fk) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4303 (class 2606 OID 21234)
-- Name: tsgnomempleados tsgrh_empleados_id_fk_empleados_c; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomempleados
    ADD CONSTRAINT tsgrh_empleados_id_fk_empleados_c FOREIGN KEY (aud_codcreadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4312 (class 2606 OID 21239)
-- Name: tsgnomincidencia tsgrh_empleados_id_fk_incidencias_c; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomincidencia
    ADD CONSTRAINT tsgrh_empleados_id_fk_incidencias_c FOREIGN KEY (aud_codcreadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4313 (class 2606 OID 21244)
-- Name: tsgnomincidencia tsgrh_empleados_id_fk_incidencias_m; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomincidencia
    ADD CONSTRAINT tsgrh_empleados_id_fk_incidencias_m FOREIGN KEY (aud_codmodificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4318 (class 2606 OID 21249)
-- Name: tsgnommanterceros tsgrh_empleados_id_fk_manuales_terceros_c; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnommanterceros
    ADD CONSTRAINT tsgrh_empleados_id_fk_manuales_terceros_c FOREIGN KEY (aud_codcreadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4319 (class 2606 OID 21254)
-- Name: tsgnommanterceros tsgrh_empleados_id_fk_manuales_terceros_m; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnommanterceros
    ADD CONSTRAINT tsgrh_empleados_id_fk_manuales_terceros_m FOREIGN KEY (aud_codmodificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4219 (class 2606 OID 19175)
-- Name: tsgrhlogistica cod_capacitacion_fk; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhlogistica
    ADD CONSTRAINT cod_capacitacion_fk FOREIGN KEY (cod_capacitacion) REFERENCES sgrh.tsgrhplancapacitacion(cod_plancapacitacion);


--
-- TOC entry 4224 (class 2606 OID 19180)
-- Name: tsgrhmodo cod_creadopor_fk; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhmodo
    ADD CONSTRAINT cod_creadopor_fk FOREIGN KEY (aud_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4261 (class 2606 OID 19185)
-- Name: tsgrhrolempleado cod_creadopor_fk; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhrolempleado
    ADD CONSTRAINT cod_creadopor_fk FOREIGN KEY (aud_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4239 (class 2606 OID 19190)
-- Name: tsgrhprocesos cod_creadopor_fk; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhprocesos
    ADD CONSTRAINT cod_creadopor_fk FOREIGN KEY (aud_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4241 (class 2606 OID 19195)
-- Name: tsgrhproveedores cod_creadopor_fk; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhproveedores
    ADD CONSTRAINT cod_creadopor_fk FOREIGN KEY (aud_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4222 (class 2606 OID 19200)
-- Name: tsgrhlugares cod_creadopor_fk; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhlugares
    ADD CONSTRAINT cod_creadopor_fk FOREIGN KEY (aud_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4207 (class 2606 OID 19205)
-- Name: tsgrhestatuscapacitacion cod_creadopor_fk; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhestatuscapacitacion
    ADD CONSTRAINT cod_creadopor_fk FOREIGN KEY (aud_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4264 (class 2606 OID 19210)
-- Name: tsgrhtipocapacitacion cod_creadopor_fk; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhtipocapacitacion
    ADD CONSTRAINT cod_creadopor_fk FOREIGN KEY (aud_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4226 (class 2606 OID 19215)
-- Name: tsgrhplancapacitacion cod_creadopor_fk; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhplancapacitacion
    ADD CONSTRAINT cod_creadopor_fk FOREIGN KEY (aud_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4220 (class 2606 OID 19220)
-- Name: tsgrhlogistica cod_creadopor_fk; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhlogistica
    ADD CONSTRAINT cod_creadopor_fk FOREIGN KEY (aud_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4244 (class 2606 OID 19225)
-- Name: tsgrhrelacionroles cod_creadopor_fk; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhrelacionroles
    ADD CONSTRAINT cod_creadopor_fk FOREIGN KEY (aud_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4209 (class 2606 OID 19230)
-- Name: tsgrhevacapacitacion cod_creadopor_fk; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhevacapacitacion
    ADD CONSTRAINT cod_creadopor_fk FOREIGN KEY (aud_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4225 (class 2606 OID 19235)
-- Name: tsgrhmodo cod_modificadopor_fk; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhmodo
    ADD CONSTRAINT cod_modificadopor_fk FOREIGN KEY (aud_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4262 (class 2606 OID 19240)
-- Name: tsgrhrolempleado cod_modificadopor_fk; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhrolempleado
    ADD CONSTRAINT cod_modificadopor_fk FOREIGN KEY (aud_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4240 (class 2606 OID 19245)
-- Name: tsgrhprocesos cod_modificadopor_fk; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhprocesos
    ADD CONSTRAINT cod_modificadopor_fk FOREIGN KEY (aud_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4242 (class 2606 OID 19250)
-- Name: tsgrhproveedores cod_modificadopor_fk; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhproveedores
    ADD CONSTRAINT cod_modificadopor_fk FOREIGN KEY (aud_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4223 (class 2606 OID 19255)
-- Name: tsgrhlugares cod_modificadopor_fk; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhlugares
    ADD CONSTRAINT cod_modificadopor_fk FOREIGN KEY (aud_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4208 (class 2606 OID 19260)
-- Name: tsgrhestatuscapacitacion cod_modificadopor_fk; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhestatuscapacitacion
    ADD CONSTRAINT cod_modificadopor_fk FOREIGN KEY (aud_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4265 (class 2606 OID 19265)
-- Name: tsgrhtipocapacitacion cod_modificadopor_fk; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhtipocapacitacion
    ADD CONSTRAINT cod_modificadopor_fk FOREIGN KEY (aud_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4227 (class 2606 OID 19270)
-- Name: tsgrhplancapacitacion cod_modificadopor_fk; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhplancapacitacion
    ADD CONSTRAINT cod_modificadopor_fk FOREIGN KEY (aud_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4221 (class 2606 OID 19275)
-- Name: tsgrhlogistica cod_modificadopor_fk; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhlogistica
    ADD CONSTRAINT cod_modificadopor_fk FOREIGN KEY (aud_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4245 (class 2606 OID 19280)
-- Name: tsgrhrelacionroles cod_modificadopor_fk; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhrelacionroles
    ADD CONSTRAINT cod_modificadopor_fk FOREIGN KEY (aud_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4210 (class 2606 OID 19285)
-- Name: tsgrhevacapacitacion cod_modificadopor_fk; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhevacapacitacion
    ADD CONSTRAINT cod_modificadopor_fk FOREIGN KEY (aud_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4246 (class 2606 OID 19290)
-- Name: tsgrhrelacionroles cod_plancapacitacion_fk; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhrelacionroles
    ADD CONSTRAINT cod_plancapacitacion_fk FOREIGN KEY (cod_plancapacitacion) REFERENCES sgrh.tsgrhplancapacitacion(cod_plancapacitacion);


--
-- TOC entry 4247 (class 2606 OID 19295)
-- Name: tsgrhrelacionroles cod_rolempleado_fk; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhrelacionroles
    ADD CONSTRAINT cod_rolempleado_fk FOREIGN KEY (cod_rolempleado) REFERENCES sgrh.tsgrhrolempleado(cod_rolempleado);


--
-- TOC entry 4182 (class 2606 OID 19300)
-- Name: tsgrhcartaasignacion fk_cod_ape; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhcartaasignacion
    ADD CONSTRAINT fk_cod_ape FOREIGN KEY (cod_ape) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4195 (class 2606 OID 19305)
-- Name: tsgrhempleados fk_cod_area; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhempleados
    ADD CONSTRAINT fk_cod_area FOREIGN KEY (cod_area) REFERENCES sgrh.tsgrhareas(cod_area) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4243 (class 2606 OID 19310)
-- Name: tsgrhpuestos fk_cod_area; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhpuestos
    ADD CONSTRAINT fk_cod_area FOREIGN KEY (cod_area) REFERENCES sgrh.tsgrhareas(cod_area) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4248 (class 2606 OID 19315)
-- Name: tsgrhrespuestasenc fk_cod_catrespuesta; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhrespuestasenc
    ADD CONSTRAINT fk_cod_catrespuesta FOREIGN KEY (cod_catrespuesta) REFERENCES sgrh.tsgrhcatrespuestas(cod_catrespuesta);


--
-- TOC entry 4183 (class 2606 OID 19320)
-- Name: tsgrhcartaasignacion fk_cod_cliente; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhcartaasignacion
    ADD CONSTRAINT fk_cod_cliente FOREIGN KEY (cod_cliente) REFERENCES sgrh.tsgrhclientes(cod_cliente) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4196 (class 2606 OID 19325)
-- Name: tsgrhempleados fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhempleados
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4193 (class 2606 OID 19330)
-- Name: tsgrhcontratos fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhcontratos
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4191 (class 2606 OID 19335)
-- Name: tsgrhcontrataciones fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhcontrataciones
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4200 (class 2606 OID 19340)
-- Name: tsgrhencuesta fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhencuesta
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4216 (class 2606 OID 19345)
-- Name: tsgrhevaluaciones fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhevaluaciones
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4234 (class 2606 OID 19350)
-- Name: tsgrhplanoperativo fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhplanoperativo
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4252 (class 2606 OID 19355)
-- Name: tsgrhrevplanoperativo fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhrevplanoperativo
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4184 (class 2606 OID 19360)
-- Name: tsgrhcartaasignacion fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhcartaasignacion
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4218 (class 2606 OID 19365)
-- Name: tsgrhexperienciaslaborales fk_cod_empleado; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhexperienciaslaborales
    ADD CONSTRAINT fk_cod_empleado FOREIGN KEY (cod_empleado) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4206 (class 2606 OID 19370)
-- Name: tsgrhescolaridad fk_cod_empleado; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhescolaridad
    ADD CONSTRAINT fk_cod_empleado FOREIGN KEY (cod_empleado) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4181 (class 2606 OID 19375)
-- Name: tsgrhcapacitaciones fk_cod_empleado; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhcapacitaciones
    ADD CONSTRAINT fk_cod_empleado FOREIGN KEY (cod_empleado) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4185 (class 2606 OID 19380)
-- Name: tsgrhcartaasignacion fk_cod_empleado; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhcartaasignacion
    ADD CONSTRAINT fk_cod_empleado FOREIGN KEY (cod_empleado) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4236 (class 2606 OID 19385)
-- Name: tsgrhpreguntasenc fk_cod_encuesta; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhpreguntasenc
    ADD CONSTRAINT fk_cod_encuesta FOREIGN KEY (cod_encuesta) REFERENCES sgrh.tsgrhencuesta(cod_encuesta) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4250 (class 2606 OID 19390)
-- Name: tsgrhrespuestaseva fk_cod_evacontestada; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhrespuestaseva
    ADD CONSTRAINT fk_cod_evacontestada FOREIGN KEY (cod_evacontestada) REFERENCES sgrh.tsgrhevacontestadas(cod_evacontestada) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4237 (class 2606 OID 19395)
-- Name: tsgrhpreguntaseva fk_cod_evaluacion; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhpreguntaseva
    ADD CONSTRAINT fk_cod_evaluacion FOREIGN KEY (cod_evaluacion) REFERENCES sgrh.tsgrhevaluaciones(cod_evaluacion) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4213 (class 2606 OID 19400)
-- Name: tsgrhevacontestadas fk_cod_evaluacion; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhevacontestadas
    ADD CONSTRAINT fk_cod_evaluacion FOREIGN KEY (cod_evaluacion) REFERENCES sgrh.tsgrhevaluaciones(cod_evaluacion) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4266 (class 2606 OID 19405)
-- Name: tsgrhvalidaevaluaciondes fk_cod_evaluacion; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhvalidaevaluaciondes
    ADD CONSTRAINT fk_cod_evaluacion FOREIGN KEY (cod_evaluacion) REFERENCES sgrh.tsgrhevaluaciones(cod_evaluacion) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4214 (class 2606 OID 19410)
-- Name: tsgrhevacontestadas fk_cod_evaluado; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhevacontestadas
    ADD CONSTRAINT fk_cod_evaluado FOREIGN KEY (cod_evaluado) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4267 (class 2606 OID 19415)
-- Name: tsgrhvalidaevaluaciondes fk_cod_evaluado; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhvalidaevaluaciondes
    ADD CONSTRAINT fk_cod_evaluado FOREIGN KEY (cod_evaluado) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4215 (class 2606 OID 19420)
-- Name: tsgrhevacontestadas fk_cod_evaluador; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhevacontestadas
    ADD CONSTRAINT fk_cod_evaluador FOREIGN KEY (cod_evaluador) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4268 (class 2606 OID 19425)
-- Name: tsgrhvalidaevaluaciondes fk_cod_evaluador; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhvalidaevaluaciondes
    ADD CONSTRAINT fk_cod_evaluador FOREIGN KEY (cod_evaluador) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4263 (class 2606 OID 19430)
-- Name: tsgrhsubfactoreseva fk_cod_factor; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhsubfactoreseva
    ADD CONSTRAINT fk_cod_factor FOREIGN KEY (cod_factor) REFERENCES sgrh.tsgrhfactoreseva(cod_factor) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4186 (class 2606 OID 19435)
-- Name: tsgrhcartaasignacion fk_cod_gpy; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhcartaasignacion
    ADD CONSTRAINT fk_cod_gpy FOREIGN KEY (cod_gpy) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4269 (class 2606 OID 19445)
-- Name: tsgrhvalidaevaluaciondes fk_cod_lugar; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhvalidaevaluaciondes
    ADD CONSTRAINT fk_cod_lugar FOREIGN KEY (cod_lugar) REFERENCES sgrt.tsgrtlugares(cod_lugar) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4253 (class 2606 OID 19450)
-- Name: tsgrhrevplanoperativo fk_cod_lugar; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhrevplanoperativo
    ADD CONSTRAINT fk_cod_lugar FOREIGN KEY (cod_lugar) REFERENCES sgrt.tsgrtlugares(cod_lugar) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4197 (class 2606 OID 19455)
-- Name: tsgrhempleados fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhempleados
    ADD CONSTRAINT fk_cod_modificadopor FOREIGN KEY (cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4194 (class 2606 OID 19460)
-- Name: tsgrhcontratos fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhcontratos
    ADD CONSTRAINT fk_cod_modificadopor FOREIGN KEY (cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4192 (class 2606 OID 19465)
-- Name: tsgrhcontrataciones fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhcontrataciones
    ADD CONSTRAINT fk_cod_modificadopor FOREIGN KEY (cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4201 (class 2606 OID 19470)
-- Name: tsgrhencuesta fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhencuesta
    ADD CONSTRAINT fk_cod_modificadopor FOREIGN KEY (cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4217 (class 2606 OID 19475)
-- Name: tsgrhevaluaciones fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhevaluaciones
    ADD CONSTRAINT fk_cod_modificadopor FOREIGN KEY (cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4235 (class 2606 OID 19480)
-- Name: tsgrhplanoperativo fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhplanoperativo
    ADD CONSTRAINT fk_cod_modificadopor FOREIGN KEY (cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4254 (class 2606 OID 19485)
-- Name: tsgrhrevplanoperativo fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhrevplanoperativo
    ADD CONSTRAINT fk_cod_modificadopor FOREIGN KEY (cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4187 (class 2606 OID 19490)
-- Name: tsgrhcartaasignacion fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhcartaasignacion
    ADD CONSTRAINT fk_cod_modificadopor FOREIGN KEY (cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4270 (class 2606 OID 19495)
-- Name: tsgrhvalidaevaluaciondes fk_cod_participante1; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhvalidaevaluaciondes
    ADD CONSTRAINT fk_cod_participante1 FOREIGN KEY (cod_participante1) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4255 (class 2606 OID 19500)
-- Name: tsgrhrevplanoperativo fk_cod_participante1; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhrevplanoperativo
    ADD CONSTRAINT fk_cod_participante1 FOREIGN KEY (cod_participante1) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4271 (class 2606 OID 19505)
-- Name: tsgrhvalidaevaluaciondes fk_cod_participante2; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhvalidaevaluaciondes
    ADD CONSTRAINT fk_cod_participante2 FOREIGN KEY (cod_participante2) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4256 (class 2606 OID 19510)
-- Name: tsgrhrevplanoperativo fk_cod_participante2; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhrevplanoperativo
    ADD CONSTRAINT fk_cod_participante2 FOREIGN KEY (cod_participante2) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4272 (class 2606 OID 19515)
-- Name: tsgrhvalidaevaluaciondes fk_cod_participante3; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhvalidaevaluaciondes
    ADD CONSTRAINT fk_cod_participante3 FOREIGN KEY (cod_participante3) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4257 (class 2606 OID 19520)
-- Name: tsgrhrevplanoperativo fk_cod_participante3; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhrevplanoperativo
    ADD CONSTRAINT fk_cod_participante3 FOREIGN KEY (cod_participante3) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4273 (class 2606 OID 19525)
-- Name: tsgrhvalidaevaluaciondes fk_cod_participante4; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhvalidaevaluaciondes
    ADD CONSTRAINT fk_cod_participante4 FOREIGN KEY (cod_participante4) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4258 (class 2606 OID 19530)
-- Name: tsgrhrevplanoperativo fk_cod_participante4; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhrevplanoperativo
    ADD CONSTRAINT fk_cod_participante4 FOREIGN KEY (cod_participante4) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4259 (class 2606 OID 19535)
-- Name: tsgrhrevplanoperativo fk_cod_participante5; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhrevplanoperativo
    ADD CONSTRAINT fk_cod_participante5 FOREIGN KEY (cod_participante5) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4188 (class 2606 OID 19540)
-- Name: tsgrhcartaasignacion fk_cod_perfil; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhcartaasignacion
    ADD CONSTRAINT fk_cod_perfil FOREIGN KEY (cod_perfil) REFERENCES sgrh.tsgrhperfiles(cod_perfil) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4260 (class 2606 OID 19545)
-- Name: tsgrhrevplanoperativo fk_cod_planoperativo; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhrevplanoperativo
    ADD CONSTRAINT fk_cod_planoperativo FOREIGN KEY (cod_planoperativo) REFERENCES sgrh.tsgrhplanoperativo(cod_planoperativo) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4249 (class 2606 OID 19550)
-- Name: tsgrhrespuestasenc fk_cod_pregunta; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhrespuestasenc
    ADD CONSTRAINT fk_cod_pregunta FOREIGN KEY (cod_pregunta) REFERENCES sgrh.tsgrhpreguntasenc(cod_pregunta) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4251 (class 2606 OID 19555)
-- Name: tsgrhrespuestaseva fk_cod_pregunta; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhrespuestaseva
    ADD CONSTRAINT fk_cod_pregunta FOREIGN KEY (cod_pregunta) REFERENCES sgrh.tsgrhpreguntaseva(cod_pregunta) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4198 (class 2606 OID 19560)
-- Name: tsgrhempleados fk_cod_puesto; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhempleados
    ADD CONSTRAINT fk_cod_puesto FOREIGN KEY (cod_puesto) REFERENCES sgrh.tsgrhpuestos(cod_puesto) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4189 (class 2606 OID 19565)
-- Name: tsgrhcartaasignacion fk_cod_rhat; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhcartaasignacion
    ADD CONSTRAINT fk_cod_rhat FOREIGN KEY (cod_rhta) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4190 (class 2606 OID 19570)
-- Name: tsgrhcartaasignacion fk_cod_rys; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhcartaasignacion
    ADD CONSTRAINT fk_cod_rys FOREIGN KEY (cod_rys) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4177 (class 2606 OID 19575)
-- Name: tsgrhareas fk_cod_sistemasuite; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhareas
    ADD CONSTRAINT fk_cod_sistemasuite FOREIGN KEY (cod_sistemasuite) REFERENCES sgco.tsgcosistemas(cod_sistema) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4199 (class 2606 OID 19580)
-- Name: tsgrhempleados fk_cod_sistemasuite; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhempleados
    ADD CONSTRAINT fk_cod_sistemasuite FOREIGN KEY (cod_sistemasuite) REFERENCES sgco.tsgcosistemas(cod_sistema) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4238 (class 2606 OID 19585)
-- Name: tsgrhpreguntaseva fk_cod_subfactor; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhpreguntaseva
    ADD CONSTRAINT fk_cod_subfactor FOREIGN KEY (cod_subfactor) REFERENCES sgrh.tsgrhsubfactoreseva(cod_subfactor) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4178 (class 2606 OID 19590)
-- Name: tsgrhasignacionesemp fk_codasignadopor_asignacionesempleados; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhasignacionesemp
    ADD CONSTRAINT fk_codasignadopor_asignacionesempleados FOREIGN KEY (cod_asignadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4211 (class 2606 OID 19595)
-- Name: tsgrhevacapacitacion fk_codempleado_evacapacitacionesemp; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhevacapacitacion
    ADD CONSTRAINT fk_codempleado_evacapacitacionesemp FOREIGN KEY (cod_empleado) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4179 (class 2606 OID 19600)
-- Name: tsgrhasignacionesemp fk_codprospecto_asignacionesempleados; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhasignacionesemp
    ADD CONSTRAINT fk_codprospecto_asignacionesempleados FOREIGN KEY (cod_empleado) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4180 (class 2606 OID 19605)
-- Name: tsgrhasignacionesemp fk_codpuesto_asignacionesempleados; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhasignacionesemp
    ADD CONSTRAINT fk_codpuesto_asignacionesempleados FOREIGN KEY (cod_puesto) REFERENCES sgrh.tsgrhpuestos(cod_puesto);


--
-- TOC entry 4212 (class 2606 OID 19610)
-- Name: tsgrhevacapacitacion fk_plancapacitacion_evacapacitacionesemp; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhevacapacitacion
    ADD CONSTRAINT fk_plancapacitacion_evacapacitacionesemp FOREIGN KEY (cod_plancapacitacion) REFERENCES sgrh.tsgrhplancapacitacion(cod_plancapacitacion);


--
-- TOC entry 4228 (class 2606 OID 19615)
-- Name: tsgrhplancapacitacion plancap_tipocapacitacion_fk; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhplancapacitacion
    ADD CONSTRAINT plancap_tipocapacitacion_fk FOREIGN KEY (cod_tipocapacitacion) REFERENCES sgrh.tsgrhtipocapacitacion(cod_tipocapacitacion);


--
-- TOC entry 4229 (class 2606 OID 19620)
-- Name: tsgrhplancapacitacion plancapacitacion_estatus_fk; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhplancapacitacion
    ADD CONSTRAINT plancapacitacion_estatus_fk FOREIGN KEY (cod_estatus) REFERENCES sgrh.tsgrhestatuscapacitacion(cod_estatus);


--
-- TOC entry 4230 (class 2606 OID 19625)
-- Name: tsgrhplancapacitacion plancapacitacion_lugar_fk; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhplancapacitacion
    ADD CONSTRAINT plancapacitacion_lugar_fk FOREIGN KEY (cod_lugar) REFERENCES sgrh.tsgrhlugares(cod_lugar);


--
-- TOC entry 4231 (class 2606 OID 19630)
-- Name: tsgrhplancapacitacion plancapacitacion_modo_fk; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhplancapacitacion
    ADD CONSTRAINT plancapacitacion_modo_fk FOREIGN KEY (cod_modo) REFERENCES sgrh.tsgrhmodo(cod_modo);


--
-- TOC entry 4232 (class 2606 OID 19635)
-- Name: tsgrhplancapacitacion plancapacitacion_proceso_fk; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhplancapacitacion
    ADD CONSTRAINT plancapacitacion_proceso_fk FOREIGN KEY (cod_proceso) REFERENCES sgrh.tsgrhprocesos(cod_proceso);


--
-- TOC entry 4233 (class 2606 OID 19640)
-- Name: tsgrhplancapacitacion plancapacitacion_proveedor_fk; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhplancapacitacion
    ADD CONSTRAINT plancapacitacion_proveedor_fk FOREIGN KEY (cod_proveedor) REFERENCES sgrh.tsgrhproveedores(cod_proveedor);


--
-- TOC entry 4202 (class 2606 OID 19645)
-- Name: tsgrhencuesta_participantes tsgrhencuesta_participantes_cod_empleado_fkey; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhencuesta_participantes
    ADD CONSTRAINT tsgrhencuesta_participantes_cod_empleado_fkey FOREIGN KEY (cod_empleado) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4203 (class 2606 OID 19650)
-- Name: tsgrhencuesta_participantes tsgrhencuesta_participantes_cod_encuesta_fkey; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhencuesta_participantes
    ADD CONSTRAINT tsgrhencuesta_participantes_cod_encuesta_fkey FOREIGN KEY (cod_encuesta) REFERENCES sgrh.tsgrhencuesta(cod_encuesta);


--
-- TOC entry 4204 (class 2606 OID 19655)
-- Name: tsgrhencuesta_participantes tsgrhencuesta_participantes_cod_pregunta_fkey; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhencuesta_participantes
    ADD CONSTRAINT tsgrhencuesta_participantes_cod_pregunta_fkey FOREIGN KEY (cod_pregunta) REFERENCES sgrh.tsgrhpreguntasenc(cod_pregunta);


--
-- TOC entry 4205 (class 2606 OID 19660)
-- Name: tsgrhencuesta_participantes tsgrhencuesta_participantes_cod_respuesta_fkey; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhencuesta_participantes
    ADD CONSTRAINT tsgrhencuesta_participantes_cod_respuesta_fkey FOREIGN KEY (cod_respuesta) REFERENCES sgrh.tsgrhrespuestasenc(cod_respuesta);


--
-- TOC entry 4110 (class 2606 OID 17806)
-- Name: tsgrtcomentariosagenda fk_cod_agenda; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtcomentariosagenda
    ADD CONSTRAINT fk_cod_agenda FOREIGN KEY (cod_agenda) REFERENCES sgrt.tsgrtagenda(cod_agenda) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4129 (class 2606 OID 17811)
-- Name: tsgrtfaq fk_cod_categoria; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtfaq
    ADD CONSTRAINT fk_cod_categoria FOREIGN KEY (cod_categoriafaq) REFERENCES sgrt.tsgrtcategoriafaq(cod_categoriafaq) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4114 (class 2606 OID 17816)
-- Name: tsgrtcompromisos fk_cod_chat; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtcompromisos
    ADD CONSTRAINT fk_cod_chat FOREIGN KEY (cod_chat) REFERENCES sgrt.tsgrtchat(cod_chat);


--
-- TOC entry 4137 (class 2606 OID 17821)
-- Name: tsgrtlugares fk_cod_ciudad; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtlugares
    ADD CONSTRAINT fk_cod_ciudad FOREIGN KEY (cod_ciudad) REFERENCES sgrt.tsgrtciudades(cod_ciudad) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4124 (class 2606 OID 17826)
-- Name: tsgrtdepartamento fk_cod_correo; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtdepartamento
    ADD CONSTRAINT fk_cod_correo FOREIGN KEY (cod_correo) REFERENCES sgrt.tsgrtcorreo(cod_correo) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4108 (class 2606 OID 19665)
-- Name: tsgrtcategoriafaq fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtcategoriafaq
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4119 (class 2606 OID 19670)
-- Name: tsgrtcorreo fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtcorreo
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4127 (class 2606 OID 19675)
-- Name: tsgrtdepartamento fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtdepartamento
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4130 (class 2606 OID 19680)
-- Name: tsgrtfaq fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtfaq
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4133 (class 2606 OID 19685)
-- Name: tsgrtgrupo fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtgrupo
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4106 (class 2606 OID 19690)
-- Name: tsgrtayudatopico fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtayudatopico
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4144 (class 2606 OID 19695)
-- Name: tsgrtnota fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtnota
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4148 (class 2606 OID 19700)
-- Name: tsgrtplantillacorreos fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtplantillacorreos
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4151 (class 2606 OID 19705)
-- Name: tsgrtprioridad fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtprioridad
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4160 (class 2606 OID 19710)
-- Name: tsgrtrespuesta fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtrespuesta
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4155 (class 2606 OID 19715)
-- Name: tsgrtresppredefinida fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtresppredefinida
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4174 (class 2606 OID 19720)
-- Name: tsgrtticket fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtticket
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4101 (class 2606 OID 19725)
-- Name: tsgrtattchticket fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtattchticket
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4140 (class 2606 OID 19730)
-- Name: tsgrtmsjticket fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtmsjticket
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4165 (class 2606 OID 19735)
-- Name: tsgrtreuniones fk_cod_creadorreunion; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtreuniones
    ADD CONSTRAINT fk_cod_creadorreunion FOREIGN KEY (cod_creadorreunion) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4116 (class 2606 OID 17906)
-- Name: tsgrtcorreo fk_cod_depto; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtcorreo
    ADD CONSTRAINT fk_cod_depto FOREIGN KEY (cod_depto) REFERENCES sgrt.tsgrtdepartamento(cod_depto) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4103 (class 2606 OID 17911)
-- Name: tsgrtayudatopico fk_cod_depto; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtayudatopico
    ADD CONSTRAINT fk_cod_depto FOREIGN KEY (cod_depto) REFERENCES sgrt.tsgrtdepartamento(cod_depto) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4153 (class 2606 OID 17916)
-- Name: tsgrtresppredefinida fk_cod_depto; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtresppredefinida
    ADD CONSTRAINT fk_cod_depto FOREIGN KEY (cod_depto) REFERENCES sgrt.tsgrtdepartamento(cod_depto) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4170 (class 2606 OID 17921)
-- Name: tsgrtticket fk_cod_depto; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtticket
    ADD CONSTRAINT fk_cod_depto FOREIGN KEY (cod_depto) REFERENCES sgrt.tsgrtdepartamento(cod_depto) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4121 (class 2606 OID 17926)
-- Name: tsgrtdatossolicitud fk_cod_edosolicitud; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtdatossolicitud
    ADD CONSTRAINT fk_cod_edosolicitud FOREIGN KEY (cod_edosolicitud) REFERENCES sgrt.tsgrtedosolicitudes(cod_edosolicitud) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4122 (class 2606 OID 17931)
-- Name: tsgrtdatossolicitud fk_cod_elemento; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtdatossolicitud
    ADD CONSTRAINT fk_cod_elemento FOREIGN KEY (cod_elemento) REFERENCES sgrt.tsgrtelementos(cod_elemento) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4098 (class 2606 OID 19740)
-- Name: tsgrtasistentes fk_cod_empleado; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtasistentes
    ADD CONSTRAINT fk_cod_empleado FOREIGN KEY (cod_empleado) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4145 (class 2606 OID 19745)
-- Name: tsgrtnota fk_cod_empleado; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtnota
    ADD CONSTRAINT fk_cod_empleado FOREIGN KEY (cod_empleado) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4161 (class 2606 OID 19750)
-- Name: tsgrtrespuesta fk_cod_empleado; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtrespuesta
    ADD CONSTRAINT fk_cod_empleado FOREIGN KEY (cod_empleado) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4169 (class 2606 OID 19755)
-- Name: tsgrtsolicitudservicios fk_cod_empleado; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtsolicitudservicios
    ADD CONSTRAINT fk_cod_empleado FOREIGN KEY (cod_empleado) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4175 (class 2606 OID 19760)
-- Name: tsgrtticket fk_cod_empleado; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtticket
    ADD CONSTRAINT fk_cod_empleado FOREIGN KEY (cod_empleado) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4136 (class 2606 OID 19765)
-- Name: tsgrtinvitados fk_cod_empleado; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtinvitados
    ADD CONSTRAINT fk_cod_empleado FOREIGN KEY (cod_empleado) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4097 (class 2606 OID 17966)
-- Name: tsgrtasistentes fk_cod_invitado; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtasistentes
    ADD CONSTRAINT fk_cod_invitado FOREIGN KEY (cod_invitado) REFERENCES sgrt.tsgrtinvitados(cod_invitado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4112 (class 2606 OID 17971)
-- Name: tsgrtcomentariosreunion fk_cod_invitado; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtcomentariosreunion
    ADD CONSTRAINT fk_cod_invitado FOREIGN KEY (cod_invitado) REFERENCES sgrt.tsgrtinvitados(cod_invitado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4111 (class 2606 OID 17976)
-- Name: tsgrtcomentariosagenda fk_cod_invitado; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtcomentariosagenda
    ADD CONSTRAINT fk_cod_invitado FOREIGN KEY (cod_invitado) REFERENCES sgrt.tsgrtinvitados(cod_invitado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4163 (class 2606 OID 17981)
-- Name: tsgrtreuniones fk_cod_lugar; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtreuniones
    ADD CONSTRAINT fk_cod_lugar FOREIGN KEY (cod_lugar) REFERENCES sgrt.tsgrtlugares(cod_lugar) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4157 (class 2606 OID 17986)
-- Name: tsgrtrespuesta fk_cod_mensaje; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtrespuesta
    ADD CONSTRAINT fk_cod_mensaje FOREIGN KEY (cod_mensaje) REFERENCES sgrt.tsgrtmsjticket(cod_mensaje) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4125 (class 2606 OID 17991)
-- Name: tsgrtdepartamento fk_cod_plantillacorreo; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtdepartamento
    ADD CONSTRAINT fk_cod_plantillacorreo FOREIGN KEY (cod_plantillacorreo) REFERENCES sgrt.tsgrtplantillacorreos(cod_plantillacorreo) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4104 (class 2606 OID 17996)
-- Name: tsgrtayudatopico fk_cod_prioridad; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtayudatopico
    ADD CONSTRAINT fk_cod_prioridad FOREIGN KEY (cod_prioridad) REFERENCES sgrt.tsgrtprioridad(cod_prioridad) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4171 (class 2606 OID 18001)
-- Name: tsgrtticket fk_cod_prioridad; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtticket
    ADD CONSTRAINT fk_cod_prioridad FOREIGN KEY (cod_prioridad) REFERENCES sgrt.tsgrtprioridad(cod_prioridad) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4166 (class 2606 OID 19770)
-- Name: tsgrtreuniones fk_cod_responsable; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtreuniones
    ADD CONSTRAINT fk_cod_responsable FOREIGN KEY (cod_responsable) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4115 (class 2606 OID 18011)
-- Name: tsgrtcompromisos fk_cod_reunion; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtcompromisos
    ADD CONSTRAINT fk_cod_reunion FOREIGN KEY (cod_reunion) REFERENCES sgrt.tsgrtreuniones(cod_reunion) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4096 (class 2606 OID 18016)
-- Name: tsgrtarchivos fk_cod_reunion; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtarchivos
    ADD CONSTRAINT fk_cod_reunion FOREIGN KEY (cod_reunion) REFERENCES sgrt.tsgrtreuniones(cod_reunion) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4135 (class 2606 OID 18021)
-- Name: tsgrtinvitados fk_cod_reunion; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtinvitados
    ADD CONSTRAINT fk_cod_reunion FOREIGN KEY (cod_reunion) REFERENCES sgrt.tsgrtreuniones(cod_reunion) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4113 (class 2606 OID 18026)
-- Name: tsgrtcomentariosreunion fk_cod_reunion; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtcomentariosreunion
    ADD CONSTRAINT fk_cod_reunion FOREIGN KEY (cod_reunion) REFERENCES sgrt.tsgrtreuniones(cod_reunion) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4095 (class 2606 OID 18031)
-- Name: tsgrtagenda fk_cod_reunion; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtagenda
    ADD CONSTRAINT fk_cod_reunion FOREIGN KEY (cod_reunion) REFERENCES sgrt.tsgrtreuniones(cod_reunion) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4164 (class 2606 OID 18036)
-- Name: tsgrtreuniones fk_cod_reunionanterior; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtreuniones
    ADD CONSTRAINT fk_cod_reunionanterior FOREIGN KEY (cod_proximareunion) REFERENCES sgrt.tsgrtreuniones(cod_reunion) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4167 (class 2606 OID 18041)
-- Name: tsgrtsolicitudservicios fk_cod_servicio; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtsolicitudservicios
    ADD CONSTRAINT fk_cod_servicio FOREIGN KEY (cod_servicio) REFERENCES sgrt.tsgrtservicios(cod_servicio) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4117 (class 2606 OID 18046)
-- Name: tsgrtcorreo fk_cod_sistemasuite; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtcorreo
    ADD CONSTRAINT fk_cod_sistemasuite FOREIGN KEY (cod_sistemasuite) REFERENCES sgco.tsgcosistemas(cod_sistema) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4126 (class 2606 OID 18051)
-- Name: tsgrtdepartamento fk_cod_sistemasuite; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtdepartamento
    ADD CONSTRAINT fk_cod_sistemasuite FOREIGN KEY (cod_sistemasuite) REFERENCES sgco.tsgcosistemas(cod_sistema) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4132 (class 2606 OID 18056)
-- Name: tsgrtgrupo fk_cod_sistemasuite; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtgrupo
    ADD CONSTRAINT fk_cod_sistemasuite FOREIGN KEY (cod_sistemasuite) REFERENCES sgco.tsgcosistemas(cod_sistema) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4105 (class 2606 OID 18061)
-- Name: tsgrtayudatopico fk_cod_sistemasuite; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtayudatopico
    ADD CONSTRAINT fk_cod_sistemasuite FOREIGN KEY (cod_sistemasuite) REFERENCES sgco.tsgcosistemas(cod_sistema) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4142 (class 2606 OID 18066)
-- Name: tsgrtnota fk_cod_sistemasuite; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtnota
    ADD CONSTRAINT fk_cod_sistemasuite FOREIGN KEY (cod_sistemasuite) REFERENCES sgco.tsgcosistemas(cod_sistema) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4147 (class 2606 OID 18071)
-- Name: tsgrtplantillacorreos fk_cod_sistemasuite; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtplantillacorreos
    ADD CONSTRAINT fk_cod_sistemasuite FOREIGN KEY (cod_sistemasuite) REFERENCES sgco.tsgcosistemas(cod_sistema) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4150 (class 2606 OID 18076)
-- Name: tsgrtprioridad fk_cod_sistemasuite; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtprioridad
    ADD CONSTRAINT fk_cod_sistemasuite FOREIGN KEY (cod_sistemasuite) REFERENCES sgco.tsgcosistemas(cod_sistema) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4158 (class 2606 OID 18081)
-- Name: tsgrtrespuesta fk_cod_sistemasuite; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtrespuesta
    ADD CONSTRAINT fk_cod_sistemasuite FOREIGN KEY (cod_sistemasuite) REFERENCES sgco.tsgcosistemas(cod_sistema) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4154 (class 2606 OID 18086)
-- Name: tsgrtresppredefinida fk_cod_sistemasuite; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtresppredefinida
    ADD CONSTRAINT fk_cod_sistemasuite FOREIGN KEY (cod_sistemasuite) REFERENCES sgco.tsgcosistemas(cod_sistema) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4172 (class 2606 OID 18091)
-- Name: tsgrtticket fk_cod_sistemasuite; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtticket
    ADD CONSTRAINT fk_cod_sistemasuite FOREIGN KEY (cod_sistemasuite) REFERENCES sgco.tsgcosistemas(cod_sistema) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4099 (class 2606 OID 18096)
-- Name: tsgrtattchticket fk_cod_sistemasuite; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtattchticket
    ADD CONSTRAINT fk_cod_sistemasuite FOREIGN KEY (cod_sistemasuite) REFERENCES sgco.tsgcosistemas(cod_sistema) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4138 (class 2606 OID 18101)
-- Name: tsgrtmsjticket fk_cod_sistemasuite; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtmsjticket
    ADD CONSTRAINT fk_cod_sistemasuite FOREIGN KEY (cod_sistemasuite) REFERENCES sgco.tsgcosistemas(cod_sistema) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4123 (class 2606 OID 18106)
-- Name: tsgrtdatossolicitud fk_cod_solicitud; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtdatossolicitud
    ADD CONSTRAINT fk_cod_solicitud FOREIGN KEY (cod_solicitud) REFERENCES sgrt.tsgrtsolicitudservicios(cod_solicitud) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4143 (class 2606 OID 18111)
-- Name: tsgrtnota fk_cod_ticket; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtnota
    ADD CONSTRAINT fk_cod_ticket FOREIGN KEY (cod_ticket) REFERENCES sgrt.tsgrtticket(cod_ticket) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4159 (class 2606 OID 18116)
-- Name: tsgrtrespuesta fk_cod_ticket; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtrespuesta
    ADD CONSTRAINT fk_cod_ticket FOREIGN KEY (cod_ticket) REFERENCES sgrt.tsgrtticket(cod_ticket) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4168 (class 2606 OID 18121)
-- Name: tsgrtsolicitudservicios fk_cod_ticket; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtsolicitudservicios
    ADD CONSTRAINT fk_cod_ticket FOREIGN KEY (cod_ticket) REFERENCES sgrt.tsgrtticket(cod_ticket) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4100 (class 2606 OID 18126)
-- Name: tsgrtattchticket fk_cod_ticket; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtattchticket
    ADD CONSTRAINT fk_cod_ticket FOREIGN KEY (cod_ticket) REFERENCES sgrt.tsgrtticket(cod_ticket) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4139 (class 2606 OID 18131)
-- Name: tsgrtmsjticket fk_cod_ticket; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtmsjticket
    ADD CONSTRAINT fk_cod_ticket FOREIGN KEY (cod_ticket) REFERENCES sgrt.tsgrtticket(cod_ticket) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4173 (class 2606 OID 18136)
-- Name: tsgrtticket fk_cod_topico; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtticket
    ADD CONSTRAINT fk_cod_topico FOREIGN KEY (cod_topico) REFERENCES sgrt.tsgrtayudatopico(cod_topico) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4120 (class 2606 OID 19775)
-- Name: tsgrtcorreo fk_cod_ultactualizacionpor; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtcorreo
    ADD CONSTRAINT fk_cod_ultactualizacionpor FOREIGN KEY (cod_ultactualizacionpor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4128 (class 2606 OID 19780)
-- Name: tsgrtdepartamento fk_cod_ultactualizacionpor; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtdepartamento
    ADD CONSTRAINT fk_cod_ultactualizacionpor FOREIGN KEY (cod_ultactualizacionpor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4131 (class 2606 OID 19785)
-- Name: tsgrtfaq fk_cod_ultactualizacionpor; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtfaq
    ADD CONSTRAINT fk_cod_ultactualizacionpor FOREIGN KEY (cod_ultactualizacionpor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4134 (class 2606 OID 19790)
-- Name: tsgrtgrupo fk_cod_ultactualizacionpor; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtgrupo
    ADD CONSTRAINT fk_cod_ultactualizacionpor FOREIGN KEY (cod_ultactualizacionpor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4107 (class 2606 OID 19795)
-- Name: tsgrtayudatopico fk_cod_ultactualizacionpor; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtayudatopico
    ADD CONSTRAINT fk_cod_ultactualizacionpor FOREIGN KEY (cod_ultactualizacionpor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4146 (class 2606 OID 19800)
-- Name: tsgrtnota fk_cod_ultactualizacionpor; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtnota
    ADD CONSTRAINT fk_cod_ultactualizacionpor FOREIGN KEY (cod_ultactualizacionpor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4149 (class 2606 OID 19805)
-- Name: tsgrtplantillacorreos fk_cod_ultactualizacionpor; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtplantillacorreos
    ADD CONSTRAINT fk_cod_ultactualizacionpor FOREIGN KEY (cod_ultactualizacionpor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4152 (class 2606 OID 19810)
-- Name: tsgrtprioridad fk_cod_ultactualizacionpor; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtprioridad
    ADD CONSTRAINT fk_cod_ultactualizacionpor FOREIGN KEY (cod_ultactualizacionpor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4162 (class 2606 OID 19815)
-- Name: tsgrtrespuesta fk_cod_ultactualizacionpor; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtrespuesta
    ADD CONSTRAINT fk_cod_ultactualizacionpor FOREIGN KEY (cod_ultactualizacionpor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4156 (class 2606 OID 19820)
-- Name: tsgrtresppredefinida fk_cod_ultactualizacionpor; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtresppredefinida
    ADD CONSTRAINT fk_cod_ultactualizacionpor FOREIGN KEY (cod_ultactualizacionpor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4176 (class 2606 OID 19825)
-- Name: tsgrtticket fk_cod_ultactualizacionpor; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtticket
    ADD CONSTRAINT fk_cod_ultactualizacionpor FOREIGN KEY (cod_ultactualizacionpor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4102 (class 2606 OID 19830)
-- Name: tsgrtattchticket fk_cod_ultactualizacionpor; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtattchticket
    ADD CONSTRAINT fk_cod_ultactualizacionpor FOREIGN KEY (cod_ultactualizacionpor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4141 (class 2606 OID 19835)
-- Name: tsgrtmsjticket fk_cod_ultactualizacionpor; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtmsjticket
    ADD CONSTRAINT fk_cod_ultactualizacionpor FOREIGN KEY (cod_ultactualizacionpor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4118 (class 2606 OID 18206)
-- Name: tsgrtcorreo fk_cod_usuario; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtcorreo
    ADD CONSTRAINT fk_cod_usuario FOREIGN KEY (cod_usuario) REFERENCES sgco.tsgcousuarios(cod_usuario) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4109 (class 2606 OID 18211)
-- Name: tsgrtciudades fk_estadorep; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtciudades
    ADD CONSTRAINT fk_estadorep FOREIGN KEY (cod_estadorep) REFERENCES sgrt.tsgrtestados(cod_estadorep) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4330 (class 2606 OID 22212)
-- Name: tsisatcartaasignacion fk__cod_gpy; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcartaasignacion
    ADD CONSTRAINT fk__cod_gpy FOREIGN KEY (cod_gpy) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4331 (class 2606 OID 22217)
-- Name: tsisatcartaasignacion fk_cod_ape; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcartaasignacion
    ADD CONSTRAINT fk_cod_ape FOREIGN KEY (cod_ape) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4343 (class 2606 OID 22222)
-- Name: tsisatcotizaciones fk_cod_ciudad; Type: FK CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatcotizaciones
    ADD CONSTRAINT fk_cod_ciudad FOREIGN KEY (cod_ciudad) REFERENCES sgrt.tsgrtciudades(cod_ciudad);


--
-- TOC entry 4356 (class 2606 OID 22227)
-- Name: tsisatordenservicio fk_cod_ciudad; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatordenservicio
    ADD CONSTRAINT fk_cod_ciudad FOREIGN KEY (cod_ciudad) REFERENCES sgrt.tsgrtciudades(cod_ciudad);


--
-- TOC entry 4321 (class 2606 OID 22232)
-- Name: tsisatasignaciones fk_cod_cliente; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatasignaciones
    ADD CONSTRAINT fk_cod_cliente FOREIGN KEY (cod_cliente) REFERENCES sgrh.tsgrhclientes(cod_cliente);


--
-- TOC entry 4332 (class 2606 OID 22237)
-- Name: tsisatcartaasignacion fk_cod_cliente; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcartaasignacion
    ADD CONSTRAINT fk_cod_cliente FOREIGN KEY (cod_cliente) REFERENCES sgrh.tsgrhclientes(cod_cliente);


--
-- TOC entry 4357 (class 2606 OID 22242)
-- Name: tsisatordenservicio fk_cod_cliente; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatordenservicio
    ADD CONSTRAINT fk_cod_cliente FOREIGN KEY (cod_cliente) REFERENCES sgrh.tsgrhclientes(cod_cliente);


--
-- TOC entry 4364 (class 2606 OID 22247)
-- Name: tsisatvacantes fk_cod_cliente; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatvacantes
    ADD CONSTRAINT fk_cod_cliente FOREIGN KEY (cod_cliente) REFERENCES sgrh.tsgrhclientes(cod_cliente);


--
-- TOC entry 4344 (class 2606 OID 22252)
-- Name: tsisatcotizaciones fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatcotizaciones
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4322 (class 2606 OID 22257)
-- Name: tsisatasignaciones fk_cod_empleado; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatasignaciones
    ADD CONSTRAINT fk_cod_empleado FOREIGN KEY (cod_empleado) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4361 (class 2606 OID 22262)
-- Name: tsisatprospectos fk_cod_entrevista; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatprospectos
    ADD CONSTRAINT fk_cod_entrevista FOREIGN KEY (cod_entrevista) REFERENCES sisat.tsisatentrevistas(cod_entrevista);


--
-- TOC entry 4345 (class 2606 OID 22267)
-- Name: tsisatcotizaciones fk_cod_estado; Type: FK CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatcotizaciones
    ADD CONSTRAINT fk_cod_estado FOREIGN KEY (cod_estado) REFERENCES sgrt.tsgrtestados(cod_estadorep);


--
-- TOC entry 4358 (class 2606 OID 22272)
-- Name: tsisatordenservicio fk_cod_estadorep; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatordenservicio
    ADD CONSTRAINT fk_cod_estadorep FOREIGN KEY (cod_estadorep) REFERENCES sgrt.tsgrtestados(cod_estadorep);


--
-- TOC entry 4359 (class 2606 OID 22277)
-- Name: tsisatordenservicio fk_cod_gpy; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatordenservicio
    ADD CONSTRAINT fk_cod_gpy FOREIGN KEY (cod_gpy) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4365 (class 2606 OID 22282)
-- Name: tsisatvacantes fk_cod_idioma; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatvacantes
    ADD CONSTRAINT fk_cod_idioma FOREIGN KEY (cod_idioma) REFERENCES sisat.tsisatidiomas(cod_idioma);


--
-- TOC entry 4325 (class 2606 OID 22287)
-- Name: tsisatcandidatos fk_cod_perfil; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcandidatos
    ADD CONSTRAINT fk_cod_perfil FOREIGN KEY (cod_perfil) REFERENCES sgrh.tsgrhperfiles(cod_perfil);


--
-- TOC entry 4362 (class 2606 OID 22292)
-- Name: tsisatproyectos fk_cod_perfil; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatproyectos
    ADD CONSTRAINT fk_cod_perfil FOREIGN KEY (cod_perfil) REFERENCES sgrh.tsgrhperfiles(cod_perfil);


--
-- TOC entry 4323 (class 2606 OID 22297)
-- Name: tsisatasignaciones fk_cod_perfil; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatasignaciones
    ADD CONSTRAINT fk_cod_perfil FOREIGN KEY (cod_perfil) REFERENCES sgrh.tsgrhperfiles(cod_perfil);


--
-- TOC entry 4333 (class 2606 OID 22302)
-- Name: tsisatcartaasignacion fk_cod_perfil; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcartaasignacion
    ADD CONSTRAINT fk_cod_perfil FOREIGN KEY (cod_perfil) REFERENCES sgrh.tsgrhperfiles(cod_perfil);


--
-- TOC entry 4334 (class 2606 OID 22307)
-- Name: tsisatcartaasignacion fk_cod_prospecto; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcartaasignacion
    ADD CONSTRAINT fk_cod_prospecto FOREIGN KEY (cod_prospecto) REFERENCES sisat.tsisatprospectos(cod_prospecto) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4324 (class 2606 OID 22312)
-- Name: tsisatasignaciones fk_cod_prospecto; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatasignaciones
    ADD CONSTRAINT fk_cod_prospecto FOREIGN KEY (cod_prospecto) REFERENCES sisat.tsisatprospectos(cod_prospecto) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4363 (class 2606 OID 22317)
-- Name: tsisatproyectos fk_cod_prospecto; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatproyectos
    ADD CONSTRAINT fk_cod_prospecto FOREIGN KEY (cod_prospecto) REFERENCES sisat.tsisatprospectos(cod_prospecto) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4353 (class 2606 OID 22322)
-- Name: tsisatexperienciaslaborales fk_cod_prospecto; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatexperienciaslaborales
    ADD CONSTRAINT fk_cod_prospecto FOREIGN KEY (cod_prospecto) REFERENCES sisat.tsisatprospectos(cod_prospecto) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4352 (class 2606 OID 22327)
-- Name: tsisatescolaridad fk_cod_prospecto; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatescolaridad
    ADD CONSTRAINT fk_cod_prospecto FOREIGN KEY (cod_prospecto) REFERENCES sisat.tsisatprospectos(cod_prospecto) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4347 (class 2606 OID 22332)
-- Name: tsisatcursosycertificados fk_cod_prospecto; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcursosycertificados
    ADD CONSTRAINT fk_cod_prospecto FOREIGN KEY (cod_prospecto) REFERENCES sisat.tsisatprospectos(cod_prospecto) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4355 (class 2606 OID 22337)
-- Name: tsisatidiomas fk_cod_prospecto; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatidiomas
    ADD CONSTRAINT fk_cod_prospecto FOREIGN KEY (cod_prospecto) REFERENCES sisat.tsisatprospectos(cod_prospecto) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4354 (class 2606 OID 22342)
-- Name: tsisathabilidades fk_cod_prospecto; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisathabilidades
    ADD CONSTRAINT fk_cod_prospecto FOREIGN KEY (cod_prospecto) REFERENCES sisat.tsisatprospectos(cod_prospecto);


--
-- TOC entry 4346 (class 2606 OID 22347)
-- Name: tsisatcotizaciones fk_cod_puesto; Type: FK CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatcotizaciones
    ADD CONSTRAINT fk_cod_puesto FOREIGN KEY (cod_puesto) REFERENCES sgrh.tsgrhpuestos(cod_puesto);


--
-- TOC entry 4360 (class 2606 OID 22352)
-- Name: tsisatordenservicio fk_cod_puesto; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatordenservicio
    ADD CONSTRAINT fk_cod_puesto FOREIGN KEY (cod_puesto) REFERENCES sgrh.tsgrhpuestos(cod_puesto);


--
-- TOC entry 4335 (class 2606 OID 22357)
-- Name: tsisatcartaasignacion fk_cod_rhta; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcartaasignacion
    ADD CONSTRAINT fk_cod_rhta FOREIGN KEY (cod_rhta) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4336 (class 2606 OID 22362)
-- Name: tsisatcartaasignacion fk_cod_rys; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcartaasignacion
    ADD CONSTRAINT fk_cod_rys FOREIGN KEY (cod_rys) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4337 (class 2606 OID 22367)
-- Name: tsisatcomentarios fk_comentarioasignacion; Type: FK CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatcomentarios
    ADD CONSTRAINT fk_comentarioasignacion FOREIGN KEY (cod_comentario) REFERENCES sisat.tsisatcartaasignacion(cod_asignacion);


--
-- TOC entry 4338 (class 2606 OID 22372)
-- Name: tsisatcomentarios fk_comentarioprospecto; Type: FK CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatcomentarios
    ADD CONSTRAINT fk_comentarioprospecto FOREIGN KEY (cod_comentario) REFERENCES sisat.tsisatentrevistas(cod_entrevista);


--
-- TOC entry 4339 (class 2606 OID 22377)
-- Name: tsisatcomentarios fk_comentariorequisicion; Type: FK CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatcomentarios
    ADD CONSTRAINT fk_comentariorequisicion FOREIGN KEY (cod_comentario) REFERENCES sisat.tsisatvacantes(cod_vacante);


--
-- TOC entry 4340 (class 2606 OID 22382)
-- Name: tsisatcomentarios fk_comentarioscosteos; Type: FK CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatcomentarios
    ADD CONSTRAINT fk_comentarioscosteos FOREIGN KEY (cod_comentario) REFERENCES sisat.tsisatcandidatos(cod_candidato);


--
-- TOC entry 4341 (class 2606 OID 22387)
-- Name: tsisatcomentarios fk_contrataciones; Type: FK CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatcomentarios
    ADD CONSTRAINT fk_contrataciones FOREIGN KEY (cod_contratacion) REFERENCES sgrh.tsgrhcontrataciones(cod_contratacion);


--
-- TOC entry 4326 (class 2606 OID 22392)
-- Name: tsisatcandidatos fk_creadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcandidatos
    ADD CONSTRAINT fk_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4328 (class 2606 OID 22397)
-- Name: tsisatcartaaceptacion fk_creadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcartaaceptacion
    ADD CONSTRAINT fk_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4348 (class 2606 OID 22402)
-- Name: tsisatentrevistas fk_creadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatentrevistas
    ADD CONSTRAINT fk_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4350 (class 2606 OID 22407)
-- Name: tsisatenviocorreos fk_creadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatenviocorreos
    ADD CONSTRAINT fk_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4342 (class 2606 OID 22412)
-- Name: tsisatcomentarios fk_firma; Type: FK CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatcomentarios
    ADD CONSTRAINT fk_firma FOREIGN KEY (cod_firma) REFERENCES sisat.tsisatfirmas(cod_firma);


--
-- TOC entry 4327 (class 2606 OID 22417)
-- Name: tsisatcandidatos fk_modificadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcandidatos
    ADD CONSTRAINT fk_modificadopor FOREIGN KEY (cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4329 (class 2606 OID 22422)
-- Name: tsisatcartaaceptacion fk_modificadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatcartaaceptacion
    ADD CONSTRAINT fk_modificadopor FOREIGN KEY (cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4349 (class 2606 OID 22427)
-- Name: tsisatentrevistas fk_modificadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatentrevistas
    ADD CONSTRAINT fk_modificadopor FOREIGN KEY (cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4351 (class 2606 OID 22432)
-- Name: tsisatenviocorreos fk_modificadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
--

ALTER TABLE ONLY sisat.tsisatenviocorreos
    ADD CONSTRAINT fk_modificadopor FOREIGN KEY (cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4720 (class 0 OID 0)
-- Dependencies: 13
-- Name: SCHEMA sgco; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA sgco FROM postgres;
GRANT ALL ON SCHEMA sgco TO postgres WITH GRANT OPTION;


--
-- TOC entry 4722 (class 0 OID 0)
-- Dependencies: 10
-- Name: SCHEMA sgnom; Type: ACL; Schema: -; Owner: postgres
--

GRANT ALL ON SCHEMA sgnom TO suite WITH GRANT OPTION;


--
-- TOC entry 4726 (class 0 OID 0)
-- Dependencies: 1119
-- Name: TYPE edo_encuesta; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT ALL ON TYPE sgrh.edo_encuesta TO suite WITH GRANT OPTION;


--
-- TOC entry 4727 (class 0 OID 0)
-- Dependencies: 470
-- Name: FUNCTION actualizar_comentarios_incidencia(incidenciaid integer, comentarios text, reporta integer); Type: ACL; Schema: sgnom; Owner: postgres
--

GRANT ALL ON FUNCTION sgnom.actualizar_comentarios_incidencia(incidenciaid integer, comentarios text, reporta integer) TO suite WITH GRANT OPTION;


--
-- TOC entry 4728 (class 0 OID 0)
-- Dependencies: 471
-- Name: FUNCTION actualizar_importe_incidencia(incidenciaid integer, importe numeric, reporta integer); Type: ACL; Schema: sgnom; Owner: postgres
--

GRANT ALL ON FUNCTION sgnom.actualizar_importe_incidencia(incidenciaid integer, importe numeric, reporta integer) TO suite WITH GRANT OPTION;


--
-- TOC entry 4729 (class 0 OID 0)
-- Dependencies: 472
-- Name: FUNCTION buscar_incidencias_por_empleado(idempleado integer); Type: ACL; Schema: sgnom; Owner: postgres
--

GRANT ALL ON FUNCTION sgnom.buscar_incidencias_por_empleado(idempleado integer) TO suite WITH GRANT OPTION;


--
-- TOC entry 4730 (class 0 OID 0)
-- Dependencies: 473
-- Name: FUNCTION eliminar_incidencia_por_empleado(incidenciaid integer, reporta integer); Type: ACL; Schema: sgnom; Owner: postgres
--

GRANT ALL ON FUNCTION sgnom.eliminar_incidencia_por_empleado(incidenciaid integer, reporta integer) TO suite WITH GRANT OPTION;


--
-- TOC entry 4731 (class 0 OID 0)
-- Dependencies: 478
-- Name: FUNCTION fn_calcula_importes_nomina(idnomina integer, vempleados character varying, todos numeric); Type: ACL; Schema: sgnom; Owner: postgres
--

GRANT ALL ON FUNCTION sgnom.fn_calcula_importes_nomina(idnomina integer, vempleados character varying, todos numeric) TO suite WITH GRANT OPTION;


--
-- TOC entry 4732 (class 0 OID 0)
-- Dependencies: 467
-- Name: FUNCTION fn_calcula_nomina(vidnomina integer); Type: ACL; Schema: sgnom; Owner: postgres
--

GRANT ALL ON FUNCTION sgnom.fn_calcula_nomina(vidnomina integer) TO suite WITH GRANT OPTION;


--
-- TOC entry 4733 (class 0 OID 0)
-- Dependencies: 468
-- Name: FUNCTION fn_calcula_nomina1(empid integer, vidnomina integer); Type: ACL; Schema: sgnom; Owner: postgres
--

GRANT ALL ON FUNCTION sgnom.fn_calcula_nomina1(empid integer, vidnomina integer) TO suite WITH GRANT OPTION;


--
-- TOC entry 4734 (class 0 OID 0)
-- Dependencies: 477
-- Name: FUNCTION fn_crearnomina(cabecera integer); Type: ACL; Schema: sgnom; Owner: postgres
--

GRANT ALL ON FUNCTION sgnom.fn_crearnomina(cabecera integer) TO suite WITH GRANT OPTION;


--
-- TOC entry 4735 (class 0 OID 0)
-- Dependencies: 476
-- Name: FUNCTION fn_dias_laborados(empid integer, prmnumquincenacalculo integer); Type: ACL; Schema: sgnom; Owner: postgres
--

GRANT ALL ON FUNCTION sgnom.fn_dias_laborados(empid integer, prmnumquincenacalculo integer) TO suite WITH GRANT OPTION;


--
-- TOC entry 4736 (class 0 OID 0)
-- Dependencies: 469
-- Name: FUNCTION fn_sueldo_base(empid integer); Type: ACL; Schema: sgnom; Owner: postgres
--

GRANT ALL ON FUNCTION sgnom.fn_sueldo_base(empid integer) TO suite WITH GRANT OPTION;


--
-- TOC entry 4737 (class 0 OID 0)
-- Dependencies: 474
-- Name: FUNCTION insertar_incidencia_por_empleado(incidenciaid integer, cantidad integer, actividad character varying, comentarios text, reporta integer); Type: ACL; Schema: sgnom; Owner: postgres
--

GRANT ALL ON FUNCTION sgnom.insertar_incidencia_por_empleado(incidenciaid integer, cantidad integer, actividad character varying, comentarios text, reporta integer) TO suite WITH GRANT OPTION;


--
-- TOC entry 4738 (class 0 OID 0)
-- Dependencies: 458
-- Name: FUNCTION totalimpcab(id integer); Type: ACL; Schema: sgnom; Owner: postgres
--

GRANT ALL ON FUNCTION sgnom.totalimpcab(id integer) TO suite WITH GRANT OPTION;


--
-- TOC entry 4739 (class 0 OID 0)
-- Dependencies: 460
-- Name: FUNCTION crosstab_report_encuesta(integer); Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT ALL ON FUNCTION sgrh.crosstab_report_encuesta(integer) TO suite WITH GRANT OPTION;


--
-- TOC entry 4740 (class 0 OID 0)
-- Dependencies: 461
-- Name: FUNCTION factualizarfecha(); Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT ALL ON FUNCTION sgrh.factualizarfecha() TO suite WITH GRANT OPTION;


--
-- TOC entry 4741 (class 0 OID 0)
-- Dependencies: 457
-- Name: FUNCTION buscar_asignaciones(fec_creacion date); Type: ACL; Schema: sisat; Owner: postgres
--

GRANT ALL ON FUNCTION sisat.buscar_asignaciones(fec_creacion date) TO suite WITH GRANT OPTION;


--
-- TOC entry 4742 (class 0 OID 0)
-- Dependencies: 466
-- Name: FUNCTION buscar_asignacionesporanio(anio integer); Type: ACL; Schema: sisat; Owner: postgres
--

GRANT ALL ON FUNCTION sisat.buscar_asignacionesporanio(anio integer) TO suite WITH GRANT OPTION;


--
-- TOC entry 4743 (class 0 OID 0)
-- Dependencies: 459
-- Name: FUNCTION buscar_asignacionesporaniomes(anio integer, mes integer); Type: ACL; Schema: sisat; Owner: postgres
--

GRANT ALL ON FUNCTION sisat.buscar_asignacionesporaniomes(anio integer, mes integer) TO suite WITH GRANT OPTION;


--
-- TOC entry 4744 (class 0 OID 0)
-- Dependencies: 475
-- Name: FUNCTION buscar_carta_detalle_asignacion(asignacion_cod integer); Type: ACL; Schema: sisat; Owner: postgres
--

GRANT ALL ON FUNCTION sisat.buscar_carta_detalle_asignacion(asignacion_cod integer) TO suite WITH GRANT OPTION;


--
-- TOC entry 4745 (class 0 OID 0)
-- Dependencies: 428
-- Name: TABLE rtsueldobase; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON TABLE public.rtsueldobase TO suite WITH GRANT OPTION;


--
-- TOC entry 4746 (class 0 OID 0)
-- Dependencies: 205
-- Name: SEQUENCE seq_sistema; Type: ACL; Schema: sgco; Owner: postgres
--

REVOKE ALL ON SEQUENCE sgco.seq_sistema FROM postgres;
GRANT UPDATE ON SEQUENCE sgco.seq_sistema TO postgres;
GRANT SELECT,USAGE ON SEQUENCE sgco.seq_sistema TO postgres WITH GRANT OPTION;


--
-- TOC entry 4747 (class 0 OID 0)
-- Dependencies: 206
-- Name: SEQUENCE seq_tipousuario; Type: ACL; Schema: sgco; Owner: postgres
--

REVOKE ALL ON SEQUENCE sgco.seq_tipousuario FROM postgres;
GRANT UPDATE ON SEQUENCE sgco.seq_tipousuario TO postgres;
GRANT SELECT,USAGE ON SEQUENCE sgco.seq_tipousuario TO postgres WITH GRANT OPTION;


--
-- TOC entry 4748 (class 0 OID 0)
-- Dependencies: 207
-- Name: SEQUENCE seq_usuarios; Type: ACL; Schema: sgco; Owner: postgres
--

REVOKE ALL ON SEQUENCE sgco.seq_usuarios FROM postgres;
GRANT UPDATE ON SEQUENCE sgco.seq_usuarios TO postgres;
GRANT SELECT,USAGE ON SEQUENCE sgco.seq_usuarios TO postgres WITH GRANT OPTION;


--
-- TOC entry 4749 (class 0 OID 0)
-- Dependencies: 208
-- Name: TABLE tsgcosistemas; Type: ACL; Schema: sgco; Owner: postgres
--

REVOKE ALL ON TABLE sgco.tsgcosistemas FROM postgres;
GRANT ALL ON TABLE sgco.tsgcosistemas TO postgres WITH GRANT OPTION;


--
-- TOC entry 4750 (class 0 OID 0)
-- Dependencies: 209
-- Name: TABLE tsgcotipousuario; Type: ACL; Schema: sgco; Owner: postgres
--

REVOKE ALL ON TABLE sgco.tsgcotipousuario FROM postgres;
GRANT ALL ON TABLE sgco.tsgcotipousuario TO postgres WITH GRANT OPTION;


--
-- TOC entry 4751 (class 0 OID 0)
-- Dependencies: 210
-- Name: TABLE tsgcousuarios; Type: ACL; Schema: sgco; Owner: postgres
--

REVOKE ALL ON TABLE sgco.tsgcousuarios FROM postgres;
GRANT ALL ON TABLE sgco.tsgcousuarios TO postgres WITH GRANT OPTION;


--
-- TOC entry 4752 (class 0 OID 0)
-- Dependencies: 426
-- Name: SEQUENCE seq_cabecera; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON SEQUENCE sgnom.seq_cabecera FROM suite;
GRANT UPDATE ON SEQUENCE sgnom.seq_cabecera TO suite;
GRANT SELECT,USAGE ON SEQUENCE sgnom.seq_cabecera TO suite WITH GRANT OPTION;


--
-- TOC entry 4753 (class 0 OID 0)
-- Dependencies: 427
-- Name: SEQUENCE seq_empquincena; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON SEQUENCE sgnom.seq_empquincena FROM suite;
GRANT UPDATE ON SEQUENCE sgnom.seq_empquincena TO suite;
GRANT SELECT,USAGE ON SEQUENCE sgnom.seq_empquincena TO suite WITH GRANT OPTION;


--
-- TOC entry 4754 (class 0 OID 0)
-- Dependencies: 429
-- Name: SEQUENCE seq_incidencia; Type: ACL; Schema: sgnom; Owner: postgres
--

GRANT SELECT,USAGE ON SEQUENCE sgnom.seq_incidencia TO suite WITH GRANT OPTION;


--
-- TOC entry 4756 (class 0 OID 0)
-- Dependencies: 348
-- Name: TABLE tsgnomalguinaldo; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnomalguinaldo FROM suite;
GRANT ALL ON TABLE sgnom.tsgnomalguinaldo TO suite WITH GRANT OPTION;


--
-- TOC entry 4757 (class 0 OID 0)
-- Dependencies: 349
-- Name: TABLE tsgnomargumento; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnomargumento FROM suite;
GRANT ALL ON TABLE sgnom.tsgnomargumento TO suite WITH GRANT OPTION;


--
-- TOC entry 4758 (class 0 OID 0)
-- Dependencies: 350
-- Name: TABLE tsgnombitacora; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnombitacora FROM suite;
GRANT ALL ON TABLE sgnom.tsgnombitacora TO suite WITH GRANT OPTION;


--
-- TOC entry 4759 (class 0 OID 0)
-- Dependencies: 351
-- Name: TABLE tsgnomcabecera; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnomcabecera FROM suite;
GRANT ALL ON TABLE sgnom.tsgnomcabecera TO suite WITH GRANT OPTION;


--
-- TOC entry 4760 (class 0 OID 0)
-- Dependencies: 352
-- Name: TABLE tsgnomcabeceraht; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnomcabeceraht FROM suite;
GRANT ALL ON TABLE sgnom.tsgnomcabeceraht TO suite WITH GRANT OPTION;


--
-- TOC entry 4762 (class 0 OID 0)
-- Dependencies: 353
-- Name: TABLE tsgnomcalculo; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnomcalculo FROM suite;
GRANT ALL ON TABLE sgnom.tsgnomcalculo TO suite WITH GRANT OPTION;


--
-- TOC entry 4764 (class 0 OID 0)
-- Dependencies: 354
-- Name: TABLE tsgnomcatincidencia; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnomcatincidencia FROM suite;
GRANT ALL ON TABLE sgnom.tsgnomcatincidencia TO suite WITH GRANT OPTION;


--
-- TOC entry 4765 (class 0 OID 0)
-- Dependencies: 355
-- Name: TABLE tsgnomclasificador; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnomclasificador FROM suite;
GRANT ALL ON TABLE sgnom.tsgnomclasificador TO suite WITH GRANT OPTION;


--
-- TOC entry 4766 (class 0 OID 0)
-- Dependencies: 356
-- Name: TABLE tsgnomcncptoquinc; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnomcncptoquinc FROM suite;
GRANT ALL ON TABLE sgnom.tsgnomcncptoquinc TO suite WITH GRANT OPTION;


--
-- TOC entry 4767 (class 0 OID 0)
-- Dependencies: 357
-- Name: TABLE tsgnomcncptoquincht; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnomcncptoquincht FROM suite;
GRANT ALL ON TABLE sgnom.tsgnomcncptoquincht TO suite WITH GRANT OPTION;


--
-- TOC entry 4769 (class 0 OID 0)
-- Dependencies: 358
-- Name: TABLE tsgnomconcepto; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnomconcepto FROM suite;
GRANT ALL ON TABLE sgnom.tsgnomconcepto TO suite WITH GRANT OPTION;


--
-- TOC entry 4771 (class 0 OID 0)
-- Dependencies: 359
-- Name: TABLE tsgnomconceptosat; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnomconceptosat FROM suite;
GRANT ALL ON TABLE sgnom.tsgnomconceptosat TO suite WITH GRANT OPTION;


--
-- TOC entry 4773 (class 0 OID 0)
-- Dependencies: 360
-- Name: TABLE tsgnomconfpago; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnomconfpago FROM suite;
GRANT ALL ON TABLE sgnom.tsgnomconfpago TO suite WITH GRANT OPTION;


--
-- TOC entry 4774 (class 0 OID 0)
-- Dependencies: 361
-- Name: TABLE tsgnomejercicio; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnomejercicio FROM suite;
GRANT ALL ON TABLE sgnom.tsgnomejercicio TO suite WITH GRANT OPTION;


--
-- TOC entry 4776 (class 0 OID 0)
-- Dependencies: 362
-- Name: TABLE tsgnomempleados; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnomempleados FROM suite;
GRANT ALL ON TABLE sgnom.tsgnomempleados TO suite WITH GRANT OPTION;


--
-- TOC entry 4778 (class 0 OID 0)
-- Dependencies: 363
-- Name: TABLE tsgnomempquincena; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnomempquincena FROM suite;
GRANT ALL ON TABLE sgnom.tsgnomempquincena TO suite WITH GRANT OPTION;


--
-- TOC entry 4779 (class 0 OID 0)
-- Dependencies: 364
-- Name: TABLE tsgnomempquincenaht; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnomempquincenaht FROM suite;
GRANT ALL ON TABLE sgnom.tsgnomempquincenaht TO suite WITH GRANT OPTION;


--
-- TOC entry 4781 (class 0 OID 0)
-- Dependencies: 365
-- Name: TABLE tsgnomestatusnom; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnomestatusnom FROM suite;
GRANT ALL ON TABLE sgnom.tsgnomestatusnom TO suite WITH GRANT OPTION;


--
-- TOC entry 4783 (class 0 OID 0)
-- Dependencies: 366
-- Name: TABLE tsgnomformula; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnomformula FROM suite;
GRANT ALL ON TABLE sgnom.tsgnomformula TO suite WITH GRANT OPTION;


--
-- TOC entry 4784 (class 0 OID 0)
-- Dependencies: 367
-- Name: TABLE tsgnomfuncion; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnomfuncion FROM suite;
GRANT ALL ON TABLE sgnom.tsgnomfuncion TO suite WITH GRANT OPTION;


--
-- TOC entry 4785 (class 0 OID 0)
-- Dependencies: 368
-- Name: TABLE tsgnomhisttabla; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnomhisttabla FROM suite;
GRANT ALL ON TABLE sgnom.tsgnomhisttabla TO suite WITH GRANT OPTION;


--
-- TOC entry 4787 (class 0 OID 0)
-- Dependencies: 369
-- Name: TABLE tsgnomincidencia; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnomincidencia FROM suite;
GRANT ALL ON TABLE sgnom.tsgnomincidencia TO suite WITH GRANT OPTION;


--
-- TOC entry 4788 (class 0 OID 0)
-- Dependencies: 370
-- Name: TABLE tsgnommanterceros; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnommanterceros FROM suite;
GRANT ALL ON TABLE sgnom.tsgnommanterceros TO suite WITH GRANT OPTION;


--
-- TOC entry 4789 (class 0 OID 0)
-- Dependencies: 371
-- Name: TABLE tsgnomquincena; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnomquincena FROM suite;
GRANT ALL ON TABLE sgnom.tsgnomquincena TO suite WITH GRANT OPTION;


--
-- TOC entry 4791 (class 0 OID 0)
-- Dependencies: 372
-- Name: TABLE tsgnomtipoconcepto; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnomtipoconcepto FROM suite;
GRANT ALL ON TABLE sgnom.tsgnomtipoconcepto TO suite WITH GRANT OPTION;


--
-- TOC entry 4793 (class 0 OID 0)
-- Dependencies: 373
-- Name: TABLE tsgnomtiponomina; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnomtiponomina FROM suite;
GRANT ALL ON TABLE sgnom.tsgnomtiponomina TO suite WITH GRANT OPTION;


--
-- TOC entry 4794 (class 0 OID 0)
-- Dependencies: 273
-- Name: SEQUENCE seq_area; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT SELECT,USAGE ON SEQUENCE sgrh.seq_area TO suite WITH GRANT OPTION;


--
-- TOC entry 4795 (class 0 OID 0)
-- Dependencies: 274
-- Name: SEQUENCE seq_capacitaciones; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT SELECT,USAGE ON SEQUENCE sgrh.seq_capacitaciones TO suite WITH GRANT OPTION;


--
-- TOC entry 4796 (class 0 OID 0)
-- Dependencies: 275
-- Name: SEQUENCE seq_cartaasignacion; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT SELECT,USAGE ON SEQUENCE sgrh.seq_cartaasignacion TO suite WITH GRANT OPTION;


--
-- TOC entry 4797 (class 0 OID 0)
-- Dependencies: 276
-- Name: SEQUENCE seq_cat_encuesta_participantes; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT SELECT,USAGE ON SEQUENCE sgrh.seq_cat_encuesta_participantes TO suite WITH GRANT OPTION;


--
-- TOC entry 4798 (class 0 OID 0)
-- Dependencies: 277
-- Name: SEQUENCE seq_catrespuestas; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT SELECT,USAGE ON SEQUENCE sgrh.seq_catrespuestas TO suite WITH GRANT OPTION;


--
-- TOC entry 4799 (class 0 OID 0)
-- Dependencies: 278
-- Name: SEQUENCE seq_clientes; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT SELECT,USAGE ON SEQUENCE sgrh.seq_clientes TO suite WITH GRANT OPTION;


--
-- TOC entry 4800 (class 0 OID 0)
-- Dependencies: 279
-- Name: SEQUENCE seq_contrataciones; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT SELECT,USAGE ON SEQUENCE sgrh.seq_contrataciones TO suite WITH GRANT OPTION;


--
-- TOC entry 4801 (class 0 OID 0)
-- Dependencies: 280
-- Name: SEQUENCE seq_contratos; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT SELECT,USAGE ON SEQUENCE sgrh.seq_contratos TO suite WITH GRANT OPTION;


--
-- TOC entry 4802 (class 0 OID 0)
-- Dependencies: 281
-- Name: SEQUENCE seq_empleado; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT SELECT,USAGE ON SEQUENCE sgrh.seq_empleado TO suite WITH GRANT OPTION;


--
-- TOC entry 4803 (class 0 OID 0)
-- Dependencies: 282
-- Name: SEQUENCE seq_encuestas; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT SELECT,USAGE ON SEQUENCE sgrh.seq_encuestas TO suite WITH GRANT OPTION;


--
-- TOC entry 4804 (class 0 OID 0)
-- Dependencies: 283
-- Name: SEQUENCE seq_escolaridad; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT SELECT,USAGE ON SEQUENCE sgrh.seq_escolaridad TO suite WITH GRANT OPTION;


--
-- TOC entry 4805 (class 0 OID 0)
-- Dependencies: 284
-- Name: SEQUENCE seq_estatus; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT SELECT,USAGE ON SEQUENCE sgrh.seq_estatus TO suite WITH GRANT OPTION;


--
-- TOC entry 4806 (class 0 OID 0)
-- Dependencies: 285
-- Name: SEQUENCE seq_evacapacitacion; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT SELECT,USAGE ON SEQUENCE sgrh.seq_evacapacitacion TO suite WITH GRANT OPTION;


--
-- TOC entry 4807 (class 0 OID 0)
-- Dependencies: 286
-- Name: SEQUENCE seq_evacontestadas; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT SELECT,USAGE ON SEQUENCE sgrh.seq_evacontestadas TO suite WITH GRANT OPTION;


--
-- TOC entry 4808 (class 0 OID 0)
-- Dependencies: 287
-- Name: SEQUENCE seq_evaluaciones; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT SELECT,USAGE ON SEQUENCE sgrh.seq_evaluaciones TO suite WITH GRANT OPTION;


--
-- TOC entry 4809 (class 0 OID 0)
-- Dependencies: 288
-- Name: SEQUENCE seq_experiencialab; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT SELECT,USAGE ON SEQUENCE sgrh.seq_experiencialab TO suite WITH GRANT OPTION;


--
-- TOC entry 4810 (class 0 OID 0)
-- Dependencies: 289
-- Name: SEQUENCE seq_factoreseva; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT SELECT,USAGE ON SEQUENCE sgrh.seq_factoreseva TO suite WITH GRANT OPTION;


--
-- TOC entry 4811 (class 0 OID 0)
-- Dependencies: 290
-- Name: SEQUENCE seq_idiomas; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT SELECT,USAGE ON SEQUENCE sgrh.seq_idiomas TO suite WITH GRANT OPTION;


--
-- TOC entry 4812 (class 0 OID 0)
-- Dependencies: 291
-- Name: SEQUENCE seq_logistica; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT SELECT,USAGE ON SEQUENCE sgrh.seq_logistica TO suite WITH GRANT OPTION;


--
-- TOC entry 4813 (class 0 OID 0)
-- Dependencies: 292
-- Name: SEQUENCE seq_lugar; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT SELECT,USAGE ON SEQUENCE sgrh.seq_lugar TO suite WITH GRANT OPTION;


--
-- TOC entry 4814 (class 0 OID 0)
-- Dependencies: 293
-- Name: SEQUENCE seq_modo; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT SELECT,USAGE ON SEQUENCE sgrh.seq_modo TO suite WITH GRANT OPTION;


--
-- TOC entry 4815 (class 0 OID 0)
-- Dependencies: 294
-- Name: SEQUENCE seq_perfiles; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT SELECT,USAGE ON SEQUENCE sgrh.seq_perfiles TO suite WITH GRANT OPTION;


--
-- TOC entry 4816 (class 0 OID 0)
-- Dependencies: 295
-- Name: SEQUENCE seq_plancapacitacion; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT SELECT,USAGE ON SEQUENCE sgrh.seq_plancapacitacion TO suite WITH GRANT OPTION;


--
-- TOC entry 4817 (class 0 OID 0)
-- Dependencies: 296
-- Name: SEQUENCE seq_planesoperativos; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT SELECT,USAGE ON SEQUENCE sgrh.seq_planesoperativos TO suite WITH GRANT OPTION;


--
-- TOC entry 4818 (class 0 OID 0)
-- Dependencies: 297
-- Name: SEQUENCE seq_preguntasenc; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT SELECT,USAGE ON SEQUENCE sgrh.seq_preguntasenc TO suite WITH GRANT OPTION;


--
-- TOC entry 4819 (class 0 OID 0)
-- Dependencies: 298
-- Name: SEQUENCE seq_preguntaseva; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT SELECT,USAGE ON SEQUENCE sgrh.seq_preguntaseva TO suite WITH GRANT OPTION;


--
-- TOC entry 4820 (class 0 OID 0)
-- Dependencies: 299
-- Name: SEQUENCE seq_proceso; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT SELECT,USAGE ON SEQUENCE sgrh.seq_proceso TO suite WITH GRANT OPTION;


--
-- TOC entry 4821 (class 0 OID 0)
-- Dependencies: 300
-- Name: SEQUENCE seq_proveedor; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT SELECT,USAGE ON SEQUENCE sgrh.seq_proveedor TO suite WITH GRANT OPTION;


--
-- TOC entry 4822 (class 0 OID 0)
-- Dependencies: 301
-- Name: SEQUENCE seq_puestos; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT SELECT,USAGE ON SEQUENCE sgrh.seq_puestos TO suite WITH GRANT OPTION;


--
-- TOC entry 4823 (class 0 OID 0)
-- Dependencies: 302
-- Name: SEQUENCE seq_respuestasenc; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT SELECT,USAGE ON SEQUENCE sgrh.seq_respuestasenc TO suite WITH GRANT OPTION;


--
-- TOC entry 4824 (class 0 OID 0)
-- Dependencies: 303
-- Name: SEQUENCE seq_respuestaseva; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT SELECT,USAGE ON SEQUENCE sgrh.seq_respuestaseva TO suite WITH GRANT OPTION;


--
-- TOC entry 4825 (class 0 OID 0)
-- Dependencies: 304
-- Name: SEQUENCE seq_revplanesoperativos; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT SELECT,USAGE ON SEQUENCE sgrh.seq_revplanesoperativos TO suite WITH GRANT OPTION;


--
-- TOC entry 4826 (class 0 OID 0)
-- Dependencies: 305
-- Name: SEQUENCE seq_rolempleado; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT SELECT,USAGE ON SEQUENCE sgrh.seq_rolempleado TO suite WITH GRANT OPTION;


--
-- TOC entry 4827 (class 0 OID 0)
-- Dependencies: 306
-- Name: SEQUENCE seq_subfactoreseva; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT SELECT,USAGE ON SEQUENCE sgrh.seq_subfactoreseva TO suite WITH GRANT OPTION;


--
-- TOC entry 4828 (class 0 OID 0)
-- Dependencies: 307
-- Name: SEQUENCE seq_tipocapacitacion; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT SELECT,USAGE ON SEQUENCE sgrh.seq_tipocapacitacion TO suite WITH GRANT OPTION;


--
-- TOC entry 4829 (class 0 OID 0)
-- Dependencies: 308
-- Name: SEQUENCE seq_validaevaluaciones; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT SELECT,USAGE ON SEQUENCE sgrh.seq_validaevaluaciones TO suite WITH GRANT OPTION;


--
-- TOC entry 4830 (class 0 OID 0)
-- Dependencies: 309
-- Name: TABLE tsgrhareas; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT ALL ON TABLE sgrh.tsgrhareas TO suite WITH GRANT OPTION;


--
-- TOC entry 4831 (class 0 OID 0)
-- Dependencies: 310
-- Name: TABLE tsgrhasignacionesemp; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT ALL ON TABLE sgrh.tsgrhasignacionesemp TO suite WITH GRANT OPTION;


--
-- TOC entry 4832 (class 0 OID 0)
-- Dependencies: 311
-- Name: TABLE tsgrhcapacitaciones; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT ALL ON TABLE sgrh.tsgrhcapacitaciones TO suite WITH GRANT OPTION;


--
-- TOC entry 4833 (class 0 OID 0)
-- Dependencies: 312
-- Name: TABLE tsgrhcartaasignacion; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT ALL ON TABLE sgrh.tsgrhcartaasignacion TO suite WITH GRANT OPTION;


--
-- TOC entry 4834 (class 0 OID 0)
-- Dependencies: 313
-- Name: TABLE tsgrhcatrespuestas; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT ALL ON TABLE sgrh.tsgrhcatrespuestas TO suite WITH GRANT OPTION;


--
-- TOC entry 4835 (class 0 OID 0)
-- Dependencies: 314
-- Name: TABLE tsgrhclientes; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT ALL ON TABLE sgrh.tsgrhclientes TO suite WITH GRANT OPTION;


--
-- TOC entry 4836 (class 0 OID 0)
-- Dependencies: 315
-- Name: TABLE tsgrhcontrataciones; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT ALL ON TABLE sgrh.tsgrhcontrataciones TO suite WITH GRANT OPTION;


--
-- TOC entry 4837 (class 0 OID 0)
-- Dependencies: 316
-- Name: TABLE tsgrhcontratos; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT ALL ON TABLE sgrh.tsgrhcontratos TO suite WITH GRANT OPTION;


--
-- TOC entry 4838 (class 0 OID 0)
-- Dependencies: 317
-- Name: TABLE tsgrhempleados; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT ALL ON TABLE sgrh.tsgrhempleados TO suite WITH GRANT OPTION;


--
-- TOC entry 4839 (class 0 OID 0)
-- Dependencies: 318
-- Name: TABLE tsgrhencuesta; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT ALL ON TABLE sgrh.tsgrhencuesta TO suite WITH GRANT OPTION;


--
-- TOC entry 4840 (class 0 OID 0)
-- Dependencies: 319
-- Name: TABLE tsgrhencuesta_participantes; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT ALL ON TABLE sgrh.tsgrhencuesta_participantes TO suite WITH GRANT OPTION;


--
-- TOC entry 4841 (class 0 OID 0)
-- Dependencies: 320
-- Name: TABLE tsgrhescolaridad; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT ALL ON TABLE sgrh.tsgrhescolaridad TO suite WITH GRANT OPTION;


--
-- TOC entry 4842 (class 0 OID 0)
-- Dependencies: 321
-- Name: TABLE tsgrhestatuscapacitacion; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT ALL ON TABLE sgrh.tsgrhestatuscapacitacion TO suite WITH GRANT OPTION;


--
-- TOC entry 4843 (class 0 OID 0)
-- Dependencies: 322
-- Name: TABLE tsgrhevacapacitacion; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT ALL ON TABLE sgrh.tsgrhevacapacitacion TO suite WITH GRANT OPTION;


--
-- TOC entry 4844 (class 0 OID 0)
-- Dependencies: 323
-- Name: TABLE tsgrhevacontestadas; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT ALL ON TABLE sgrh.tsgrhevacontestadas TO suite WITH GRANT OPTION;


--
-- TOC entry 4845 (class 0 OID 0)
-- Dependencies: 324
-- Name: TABLE tsgrhevaluaciones; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT ALL ON TABLE sgrh.tsgrhevaluaciones TO suite WITH GRANT OPTION;


--
-- TOC entry 4846 (class 0 OID 0)
-- Dependencies: 325
-- Name: TABLE tsgrhexperienciaslaborales; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT ALL ON TABLE sgrh.tsgrhexperienciaslaborales TO suite WITH GRANT OPTION;


--
-- TOC entry 4847 (class 0 OID 0)
-- Dependencies: 326
-- Name: TABLE tsgrhfactoreseva; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT ALL ON TABLE sgrh.tsgrhfactoreseva TO suite WITH GRANT OPTION;


--
-- TOC entry 4848 (class 0 OID 0)
-- Dependencies: 327
-- Name: TABLE tsgrhidiomas; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT ALL ON TABLE sgrh.tsgrhidiomas TO suite WITH GRANT OPTION;


--
-- TOC entry 4849 (class 0 OID 0)
-- Dependencies: 328
-- Name: TABLE tsgrhlogistica; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT ALL ON TABLE sgrh.tsgrhlogistica TO suite WITH GRANT OPTION;


--
-- TOC entry 4850 (class 0 OID 0)
-- Dependencies: 329
-- Name: TABLE tsgrhlugares; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT ALL ON TABLE sgrh.tsgrhlugares TO suite WITH GRANT OPTION;


--
-- TOC entry 4851 (class 0 OID 0)
-- Dependencies: 330
-- Name: TABLE tsgrhmodo; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT ALL ON TABLE sgrh.tsgrhmodo TO suite WITH GRANT OPTION;


--
-- TOC entry 4852 (class 0 OID 0)
-- Dependencies: 331
-- Name: TABLE tsgrhperfiles; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT ALL ON TABLE sgrh.tsgrhperfiles TO suite WITH GRANT OPTION;


--
-- TOC entry 4853 (class 0 OID 0)
-- Dependencies: 332
-- Name: TABLE tsgrhplancapacitacion; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT ALL ON TABLE sgrh.tsgrhplancapacitacion TO suite WITH GRANT OPTION;


--
-- TOC entry 4854 (class 0 OID 0)
-- Dependencies: 333
-- Name: TABLE tsgrhplanoperativo; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT ALL ON TABLE sgrh.tsgrhplanoperativo TO suite WITH GRANT OPTION;


--
-- TOC entry 4855 (class 0 OID 0)
-- Dependencies: 334
-- Name: TABLE tsgrhpreguntasenc; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT ALL ON TABLE sgrh.tsgrhpreguntasenc TO suite WITH GRANT OPTION;


--
-- TOC entry 4856 (class 0 OID 0)
-- Dependencies: 335
-- Name: TABLE tsgrhpreguntaseva; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT ALL ON TABLE sgrh.tsgrhpreguntaseva TO suite WITH GRANT OPTION;


--
-- TOC entry 4857 (class 0 OID 0)
-- Dependencies: 336
-- Name: TABLE tsgrhprocesos; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT ALL ON TABLE sgrh.tsgrhprocesos TO suite WITH GRANT OPTION;


--
-- TOC entry 4858 (class 0 OID 0)
-- Dependencies: 337
-- Name: TABLE tsgrhproveedores; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT ALL ON TABLE sgrh.tsgrhproveedores TO suite WITH GRANT OPTION;


--
-- TOC entry 4859 (class 0 OID 0)
-- Dependencies: 338
-- Name: TABLE tsgrhpuestos; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT ALL ON TABLE sgrh.tsgrhpuestos TO suite WITH GRANT OPTION;


--
-- TOC entry 4860 (class 0 OID 0)
-- Dependencies: 339
-- Name: TABLE tsgrhrelacionroles; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT ALL ON TABLE sgrh.tsgrhrelacionroles TO suite WITH GRANT OPTION;


--
-- TOC entry 4861 (class 0 OID 0)
-- Dependencies: 340
-- Name: TABLE tsgrhrespuestasenc; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT ALL ON TABLE sgrh.tsgrhrespuestasenc TO suite WITH GRANT OPTION;


--
-- TOC entry 4862 (class 0 OID 0)
-- Dependencies: 341
-- Name: TABLE tsgrhrespuestaseva; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT ALL ON TABLE sgrh.tsgrhrespuestaseva TO suite WITH GRANT OPTION;


--
-- TOC entry 4863 (class 0 OID 0)
-- Dependencies: 342
-- Name: TABLE tsgrhrevplanoperativo; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT ALL ON TABLE sgrh.tsgrhrevplanoperativo TO suite WITH GRANT OPTION;


--
-- TOC entry 4864 (class 0 OID 0)
-- Dependencies: 343
-- Name: TABLE tsgrhrolempleado; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT ALL ON TABLE sgrh.tsgrhrolempleado TO suite WITH GRANT OPTION;


--
-- TOC entry 4865 (class 0 OID 0)
-- Dependencies: 344
-- Name: TABLE tsgrhsubfactoreseva; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT ALL ON TABLE sgrh.tsgrhsubfactoreseva TO suite WITH GRANT OPTION;


--
-- TOC entry 4866 (class 0 OID 0)
-- Dependencies: 345
-- Name: TABLE tsgrhtipocapacitacion; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT ALL ON TABLE sgrh.tsgrhtipocapacitacion TO suite WITH GRANT OPTION;


--
-- TOC entry 4867 (class 0 OID 0)
-- Dependencies: 346
-- Name: TABLE tsgrhvalidaevaluaciondes; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT ALL ON TABLE sgrh.tsgrhvalidaevaluaciondes TO suite WITH GRANT OPTION;


--
-- TOC entry 4868 (class 0 OID 0)
-- Dependencies: 347
-- Name: SEQUENCE seq_respuestas_participantes; Type: ACL; Schema: sgrt; Owner: postgres
--

GRANT SELECT,USAGE ON SEQUENCE sgrt.seq_respuestas_participantes TO suite WITH GRANT OPTION;


--
-- TOC entry 4869 (class 0 OID 0)
-- Dependencies: 374
-- Name: SEQUENCE seq_aceptaciones; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON SEQUENCE sisat.seq_aceptaciones FROM suite;
GRANT UPDATE ON SEQUENCE sisat.seq_aceptaciones TO suite;
GRANT SELECT,USAGE ON SEQUENCE sisat.seq_aceptaciones TO suite WITH GRANT OPTION;


--
-- TOC entry 4870 (class 0 OID 0)
-- Dependencies: 375
-- Name: SEQUENCE seq_asignaciones; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON SEQUENCE sisat.seq_asignaciones FROM suite;
GRANT UPDATE ON SEQUENCE sisat.seq_asignaciones TO suite;
GRANT SELECT,USAGE ON SEQUENCE sisat.seq_asignaciones TO suite WITH GRANT OPTION;


--
-- TOC entry 4871 (class 0 OID 0)
-- Dependencies: 376
-- Name: SEQUENCE seq_candidatos; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON SEQUENCE sisat.seq_candidatos FROM suite;
GRANT UPDATE ON SEQUENCE sisat.seq_candidatos TO suite;
GRANT SELECT,USAGE ON SEQUENCE sisat.seq_candidatos TO suite WITH GRANT OPTION;


--
-- TOC entry 4872 (class 0 OID 0)
-- Dependencies: 377
-- Name: SEQUENCE seq_cartaasignaciones; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON SEQUENCE sisat.seq_cartaasignaciones FROM suite;
GRANT UPDATE ON SEQUENCE sisat.seq_cartaasignaciones TO suite;
GRANT SELECT,USAGE ON SEQUENCE sisat.seq_cartaasignaciones TO suite WITH GRANT OPTION;


--
-- TOC entry 4873 (class 0 OID 0)
-- Dependencies: 378
-- Name: SEQUENCE seq_cotizaciones; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON SEQUENCE sisat.seq_cotizaciones FROM suite;
GRANT UPDATE ON SEQUENCE sisat.seq_cotizaciones TO suite;
GRANT SELECT,USAGE ON SEQUENCE sisat.seq_cotizaciones TO suite WITH GRANT OPTION;


--
-- TOC entry 4874 (class 0 OID 0)
-- Dependencies: 379
-- Name: SEQUENCE seq_cursos; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON SEQUENCE sisat.seq_cursos FROM suite;
GRANT UPDATE ON SEQUENCE sisat.seq_cursos TO suite;
GRANT SELECT,USAGE ON SEQUENCE sisat.seq_cursos TO suite WITH GRANT OPTION;


--
-- TOC entry 4875 (class 0 OID 0)
-- Dependencies: 380
-- Name: SEQUENCE seq_entrevistas; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON SEQUENCE sisat.seq_entrevistas FROM suite;
GRANT UPDATE ON SEQUENCE sisat.seq_entrevistas TO suite;
GRANT SELECT,USAGE ON SEQUENCE sisat.seq_entrevistas TO suite WITH GRANT OPTION;


--
-- TOC entry 4876 (class 0 OID 0)
-- Dependencies: 381
-- Name: SEQUENCE seq_envios; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON SEQUENCE sisat.seq_envios FROM suite;
GRANT UPDATE ON SEQUENCE sisat.seq_envios TO suite;
GRANT SELECT,USAGE ON SEQUENCE sisat.seq_envios TO suite WITH GRANT OPTION;


--
-- TOC entry 4877 (class 0 OID 0)
-- Dependencies: 382
-- Name: SEQUENCE seq_escolaridad; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON SEQUENCE sisat.seq_escolaridad FROM suite;
GRANT UPDATE ON SEQUENCE sisat.seq_escolaridad TO suite;
GRANT SELECT,USAGE ON SEQUENCE sisat.seq_escolaridad TO suite WITH GRANT OPTION;


--
-- TOC entry 4878 (class 0 OID 0)
-- Dependencies: 383
-- Name: SEQUENCE seq_experiencias; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON SEQUENCE sisat.seq_experiencias FROM suite;
GRANT UPDATE ON SEQUENCE sisat.seq_experiencias TO suite;
GRANT SELECT,USAGE ON SEQUENCE sisat.seq_experiencias TO suite WITH GRANT OPTION;


--
-- TOC entry 4879 (class 0 OID 0)
-- Dependencies: 384
-- Name: SEQUENCE seq_firmas; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON SEQUENCE sisat.seq_firmas FROM suite;
GRANT UPDATE ON SEQUENCE sisat.seq_firmas TO suite;
GRANT SELECT,USAGE ON SEQUENCE sisat.seq_firmas TO suite WITH GRANT OPTION;


--
-- TOC entry 4880 (class 0 OID 0)
-- Dependencies: 385
-- Name: SEQUENCE seq_habilidades; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON SEQUENCE sisat.seq_habilidades FROM suite;
GRANT UPDATE ON SEQUENCE sisat.seq_habilidades TO suite;
GRANT SELECT,USAGE ON SEQUENCE sisat.seq_habilidades TO suite WITH GRANT OPTION;


--
-- TOC entry 4881 (class 0 OID 0)
-- Dependencies: 386
-- Name: SEQUENCE seq_idiomas; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON SEQUENCE sisat.seq_idiomas FROM suite;
GRANT UPDATE ON SEQUENCE sisat.seq_idiomas TO suite;
GRANT SELECT,USAGE ON SEQUENCE sisat.seq_idiomas TO suite WITH GRANT OPTION;


--
-- TOC entry 4882 (class 0 OID 0)
-- Dependencies: 387
-- Name: SEQUENCE seq_ordenservicios; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON SEQUENCE sisat.seq_ordenservicios FROM suite;
GRANT UPDATE ON SEQUENCE sisat.seq_ordenservicios TO suite;
GRANT SELECT,USAGE ON SEQUENCE sisat.seq_ordenservicios TO suite WITH GRANT OPTION;


--
-- TOC entry 4883 (class 0 OID 0)
-- Dependencies: 388
-- Name: SEQUENCE seq_prospectos; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON SEQUENCE sisat.seq_prospectos FROM suite;
GRANT UPDATE ON SEQUENCE sisat.seq_prospectos TO suite;
GRANT SELECT,USAGE ON SEQUENCE sisat.seq_prospectos TO suite WITH GRANT OPTION;


--
-- TOC entry 4884 (class 0 OID 0)
-- Dependencies: 389
-- Name: SEQUENCE seq_proyectos; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON SEQUENCE sisat.seq_proyectos FROM suite;
GRANT UPDATE ON SEQUENCE sisat.seq_proyectos TO suite;
GRANT SELECT,USAGE ON SEQUENCE sisat.seq_proyectos TO suite WITH GRANT OPTION;


--
-- TOC entry 4885 (class 0 OID 0)
-- Dependencies: 390
-- Name: SEQUENCE seq_vacantes; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON SEQUENCE sisat.seq_vacantes FROM suite;
GRANT UPDATE ON SEQUENCE sisat.seq_vacantes TO suite;
GRANT SELECT,USAGE ON SEQUENCE sisat.seq_vacantes TO suite WITH GRANT OPTION;


--
-- TOC entry 4886 (class 0 OID 0)
-- Dependencies: 391
-- Name: TABLE tsisatappservices; Type: ACL; Schema: sisat; Owner: postgres
--

GRANT ALL ON TABLE sisat.tsisatappservices TO suite WITH GRANT OPTION;


--
-- TOC entry 4887 (class 0 OID 0)
-- Dependencies: 392
-- Name: TABLE tsisatarquitecturas; Type: ACL; Schema: sisat; Owner: postgres
--

GRANT ALL ON TABLE sisat.tsisatarquitecturas TO suite WITH GRANT OPTION;


--
-- TOC entry 4888 (class 0 OID 0)
-- Dependencies: 393
-- Name: TABLE tsisatasignaciones; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisatasignaciones FROM suite;
GRANT ALL ON TABLE sisat.tsisatasignaciones TO suite WITH GRANT OPTION;


--
-- TOC entry 4889 (class 0 OID 0)
-- Dependencies: 394
-- Name: TABLE tsisatcandidatos; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisatcandidatos FROM suite;
GRANT ALL ON TABLE sisat.tsisatcandidatos TO suite WITH GRANT OPTION;


--
-- TOC entry 4890 (class 0 OID 0)
-- Dependencies: 395
-- Name: TABLE tsisatcartaaceptacion; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisatcartaaceptacion FROM suite;
GRANT ALL ON TABLE sisat.tsisatcartaaceptacion TO suite WITH GRANT OPTION;


--
-- TOC entry 4891 (class 0 OID 0)
-- Dependencies: 396
-- Name: TABLE tsisatcartaasignacion; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisatcartaasignacion FROM suite;
GRANT ALL ON TABLE sisat.tsisatcartaasignacion TO suite WITH GRANT OPTION;


--
-- TOC entry 4892 (class 0 OID 0)
-- Dependencies: 397
-- Name: TABLE tsisatcomentarios; Type: ACL; Schema: sisat; Owner: postgres
--

GRANT ALL ON TABLE sisat.tsisatcomentarios TO suite WITH GRANT OPTION;


--
-- TOC entry 4893 (class 0 OID 0)
-- Dependencies: 398
-- Name: TABLE tsisatcotizaciones; Type: ACL; Schema: sisat; Owner: postgres
--

GRANT ALL ON TABLE sisat.tsisatcotizaciones TO suite WITH GRANT OPTION;


--
-- TOC entry 4894 (class 0 OID 0)
-- Dependencies: 399
-- Name: TABLE tsisatcursosycertificados; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisatcursosycertificados FROM suite;
GRANT ALL ON TABLE sisat.tsisatcursosycertificados TO suite WITH GRANT OPTION;


--
-- TOC entry 4895 (class 0 OID 0)
-- Dependencies: 400
-- Name: TABLE tsisatentrevistas; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisatentrevistas FROM suite;
GRANT ALL ON TABLE sisat.tsisatentrevistas TO suite WITH GRANT OPTION;


--
-- TOC entry 4896 (class 0 OID 0)
-- Dependencies: 401
-- Name: TABLE tsisatenviocorreos; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisatenviocorreos FROM suite;
GRANT ALL ON TABLE sisat.tsisatenviocorreos TO suite WITH GRANT OPTION;


--
-- TOC entry 4897 (class 0 OID 0)
-- Dependencies: 402
-- Name: TABLE tsisatescolaridad; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisatescolaridad FROM suite;
GRANT ALL ON TABLE sisat.tsisatescolaridad TO suite WITH GRANT OPTION;


--
-- TOC entry 4898 (class 0 OID 0)
-- Dependencies: 403
-- Name: TABLE tsisatexperienciaslaborales; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisatexperienciaslaborales FROM suite;
GRANT ALL ON TABLE sisat.tsisatexperienciaslaborales TO suite WITH GRANT OPTION;


--
-- TOC entry 4899 (class 0 OID 0)
-- Dependencies: 404
-- Name: TABLE tsisatfirmas; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisatfirmas FROM suite;
GRANT ALL ON TABLE sisat.tsisatfirmas TO suite WITH GRANT OPTION;


--
-- TOC entry 4900 (class 0 OID 0)
-- Dependencies: 405
-- Name: TABLE tsisatframeworks; Type: ACL; Schema: sisat; Owner: postgres
--

GRANT ALL ON TABLE sisat.tsisatframeworks TO suite WITH GRANT OPTION;


--
-- TOC entry 4901 (class 0 OID 0)
-- Dependencies: 406
-- Name: TABLE tsisathabilidades; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisathabilidades FROM suite;
GRANT ALL ON TABLE sisat.tsisathabilidades TO suite WITH GRANT OPTION;


--
-- TOC entry 4902 (class 0 OID 0)
-- Dependencies: 407
-- Name: TABLE tsisatherramientas; Type: ACL; Schema: sisat; Owner: postgres
--

GRANT ALL ON TABLE sisat.tsisatherramientas TO suite WITH GRANT OPTION;


--
-- TOC entry 4904 (class 0 OID 0)
-- Dependencies: 408
-- Name: SEQUENCE tsisatherramientas_cod_herramientas_seq; Type: ACL; Schema: sisat; Owner: postgres
--

GRANT SELECT,USAGE ON SEQUENCE sisat.tsisatherramientas_cod_herramientas_seq TO suite WITH GRANT OPTION;


--
-- TOC entry 4905 (class 0 OID 0)
-- Dependencies: 409
-- Name: TABLE tsisatides; Type: ACL; Schema: sisat; Owner: postgres
--

GRANT ALL ON TABLE sisat.tsisatides TO suite WITH GRANT OPTION;


--
-- TOC entry 4906 (class 0 OID 0)
-- Dependencies: 410
-- Name: TABLE tsisatidiomas; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisatidiomas FROM suite;
GRANT ALL ON TABLE sisat.tsisatidiomas TO suite WITH GRANT OPTION;


--
-- TOC entry 4907 (class 0 OID 0)
-- Dependencies: 411
-- Name: TABLE tsisatlenguajes; Type: ACL; Schema: sisat; Owner: postgres
--

GRANT ALL ON TABLE sisat.tsisatlenguajes TO suite WITH GRANT OPTION;


--
-- TOC entry 4908 (class 0 OID 0)
-- Dependencies: 412
-- Name: TABLE tsisatmaquetados; Type: ACL; Schema: sisat; Owner: postgres
--

GRANT ALL ON TABLE sisat.tsisatmaquetados TO suite WITH GRANT OPTION;


--
-- TOC entry 4909 (class 0 OID 0)
-- Dependencies: 413
-- Name: TABLE tsisatmetodologias; Type: ACL; Schema: sisat; Owner: postgres
--

GRANT ALL ON TABLE sisat.tsisatmetodologias TO suite WITH GRANT OPTION;


--
-- TOC entry 4910 (class 0 OID 0)
-- Dependencies: 414
-- Name: TABLE tsisatmodelados; Type: ACL; Schema: sisat; Owner: postgres
--

GRANT ALL ON TABLE sisat.tsisatmodelados TO suite WITH GRANT OPTION;


--
-- TOC entry 4911 (class 0 OID 0)
-- Dependencies: 415
-- Name: TABLE tsisatordenservicio; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisatordenservicio FROM suite;
GRANT ALL ON TABLE sisat.tsisatordenservicio TO suite WITH GRANT OPTION;


--
-- TOC entry 4912 (class 0 OID 0)
-- Dependencies: 416
-- Name: TABLE tsisatpatrones; Type: ACL; Schema: sisat; Owner: postgres
--

GRANT ALL ON TABLE sisat.tsisatpatrones TO suite WITH GRANT OPTION;


--
-- TOC entry 4913 (class 0 OID 0)
-- Dependencies: 417
-- Name: TABLE tsisatprospectos; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisatprospectos FROM suite;
GRANT ALL ON TABLE sisat.tsisatprospectos TO suite WITH GRANT OPTION;


--
-- TOC entry 4914 (class 0 OID 0)
-- Dependencies: 418
-- Name: TABLE tsisatprotocolos; Type: ACL; Schema: sisat; Owner: postgres
--

GRANT ALL ON TABLE sisat.tsisatprotocolos TO suite WITH GRANT OPTION;


--
-- TOC entry 4915 (class 0 OID 0)
-- Dependencies: 419
-- Name: TABLE tsisatproyectos; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisatproyectos FROM suite;
GRANT ALL ON TABLE sisat.tsisatproyectos TO suite WITH GRANT OPTION;


--
-- TOC entry 4916 (class 0 OID 0)
-- Dependencies: 420
-- Name: TABLE tsisatqa; Type: ACL; Schema: sisat; Owner: postgres
--

GRANT ALL ON TABLE sisat.tsisatqa TO suite WITH GRANT OPTION;


--
-- TOC entry 4917 (class 0 OID 0)
-- Dependencies: 421
-- Name: TABLE tsisatrepositoriolibrerias; Type: ACL; Schema: sisat; Owner: postgres
--

GRANT ALL ON TABLE sisat.tsisatrepositoriolibrerias TO suite WITH GRANT OPTION;


--
-- TOC entry 4918 (class 0 OID 0)
-- Dependencies: 422
-- Name: TABLE tsisatrepositorios; Type: ACL; Schema: sisat; Owner: postgres
--

GRANT ALL ON TABLE sisat.tsisatrepositorios TO suite WITH GRANT OPTION;


--
-- TOC entry 4919 (class 0 OID 0)
-- Dependencies: 423
-- Name: TABLE tsisatsgbd; Type: ACL; Schema: sisat; Owner: postgres
--

GRANT ALL ON TABLE sisat.tsisatsgbd TO suite WITH GRANT OPTION;


--
-- TOC entry 4920 (class 0 OID 0)
-- Dependencies: 424
-- Name: TABLE tsisatso; Type: ACL; Schema: sisat; Owner: postgres
--

GRANT ALL ON TABLE sisat.tsisatso TO suite WITH GRANT OPTION;


--
-- TOC entry 4921 (class 0 OID 0)
-- Dependencies: 425
-- Name: TABLE tsisatvacantes; Type: ACL; Schema: sisat; Owner: suite
--

REVOKE ALL ON TABLE sisat.tsisatvacantes FROM suite;
GRANT ALL ON TABLE sisat.tsisatvacantes TO suite WITH GRANT OPTION;


--
-- TOC entry 2533 (class 826 OID 16451)
-- Name: DEFAULT PRIVILEGES FOR SEQUENCES; Type: DEFAULT ACL; Schema: sgco; Owner: suite
--

ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sgco REVOKE ALL ON SEQUENCES  FROM suite;
ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sgco GRANT SELECT,USAGE ON SEQUENCES  TO suite WITH GRANT OPTION;


--
-- TOC entry 2535 (class 826 OID 16453)
-- Name: DEFAULT PRIVILEGES FOR TYPES; Type: DEFAULT ACL; Schema: sgco; Owner: suite
--

ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sgco REVOKE ALL ON TYPES  FROM PUBLIC;
ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sgco REVOKE ALL ON TYPES  FROM suite;
ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sgco GRANT ALL ON TYPES  TO suite WITH GRANT OPTION;


--
-- TOC entry 2534 (class 826 OID 16452)
-- Name: DEFAULT PRIVILEGES FOR FUNCTIONS; Type: DEFAULT ACL; Schema: sgco; Owner: suite
--

ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sgco REVOKE ALL ON FUNCTIONS  FROM PUBLIC;
ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sgco REVOKE ALL ON FUNCTIONS  FROM suite;
ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sgco GRANT ALL ON FUNCTIONS  TO suite WITH GRANT OPTION;


--
-- TOC entry 2532 (class 826 OID 16450)
-- Name: DEFAULT PRIVILEGES FOR TABLES; Type: DEFAULT ACL; Schema: sgco; Owner: suite
--

ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sgco REVOKE ALL ON TABLES  FROM suite;
ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sgco GRANT ALL ON TABLES  TO suite WITH GRANT OPTION;


--
-- TOC entry 2536 (class 826 OID 21259)
-- Name: DEFAULT PRIVILEGES FOR SEQUENCES; Type: DEFAULT ACL; Schema: sgnom; Owner: postgres
--

ALTER DEFAULT PRIVILEGES FOR ROLE postgres IN SCHEMA sgnom REVOKE ALL ON SEQUENCES  FROM postgres;
ALTER DEFAULT PRIVILEGES FOR ROLE postgres IN SCHEMA sgnom GRANT SELECT,USAGE ON SEQUENCES  TO suite WITH GRANT OPTION;


--
-- TOC entry 2537 (class 826 OID 21260)
-- Name: DEFAULT PRIVILEGES FOR TYPES; Type: DEFAULT ACL; Schema: sgnom; Owner: postgres
--

ALTER DEFAULT PRIVILEGES FOR ROLE postgres IN SCHEMA sgnom REVOKE ALL ON TYPES  FROM PUBLIC;
ALTER DEFAULT PRIVILEGES FOR ROLE postgres IN SCHEMA sgnom REVOKE ALL ON TYPES  FROM postgres;
ALTER DEFAULT PRIVILEGES FOR ROLE postgres IN SCHEMA sgnom GRANT ALL ON TYPES  TO suite WITH GRANT OPTION;


--
-- TOC entry 2538 (class 826 OID 21261)
-- Name: DEFAULT PRIVILEGES FOR FUNCTIONS; Type: DEFAULT ACL; Schema: sgnom; Owner: postgres
--

ALTER DEFAULT PRIVILEGES FOR ROLE postgres IN SCHEMA sgnom REVOKE ALL ON FUNCTIONS  FROM PUBLIC;
ALTER DEFAULT PRIVILEGES FOR ROLE postgres IN SCHEMA sgnom REVOKE ALL ON FUNCTIONS  FROM postgres;
ALTER DEFAULT PRIVILEGES FOR ROLE postgres IN SCHEMA sgnom GRANT ALL ON FUNCTIONS  TO suite WITH GRANT OPTION;


--
-- TOC entry 2539 (class 826 OID 21262)
-- Name: DEFAULT PRIVILEGES FOR TABLES; Type: DEFAULT ACL; Schema: sgnom; Owner: postgres
--

ALTER DEFAULT PRIVILEGES FOR ROLE postgres IN SCHEMA sgnom REVOKE ALL ON TABLES  FROM postgres;
ALTER DEFAULT PRIVILEGES FOR ROLE postgres IN SCHEMA sgnom GRANT ALL ON TABLES  TO suite WITH GRANT OPTION;


--
-- TOC entry 2529 (class 826 OID 16425)
-- Name: DEFAULT PRIVILEGES FOR SEQUENCES; Type: DEFAULT ACL; Schema: sgrt; Owner: postgres
--

ALTER DEFAULT PRIVILEGES FOR ROLE postgres IN SCHEMA sgrt REVOKE ALL ON SEQUENCES  FROM postgres;
ALTER DEFAULT PRIVILEGES FOR ROLE postgres IN SCHEMA sgrt GRANT SELECT,USAGE ON SEQUENCES  TO suite WITH GRANT OPTION;


--
-- TOC entry 2531 (class 826 OID 16427)
-- Name: DEFAULT PRIVILEGES FOR TYPES; Type: DEFAULT ACL; Schema: sgrt; Owner: postgres
--

ALTER DEFAULT PRIVILEGES FOR ROLE postgres IN SCHEMA sgrt REVOKE ALL ON TYPES  FROM PUBLIC;
ALTER DEFAULT PRIVILEGES FOR ROLE postgres IN SCHEMA sgrt REVOKE ALL ON TYPES  FROM postgres;
ALTER DEFAULT PRIVILEGES FOR ROLE postgres IN SCHEMA sgrt GRANT ALL ON TYPES  TO suite WITH GRANT OPTION;


--
-- TOC entry 2530 (class 826 OID 16426)
-- Name: DEFAULT PRIVILEGES FOR FUNCTIONS; Type: DEFAULT ACL; Schema: sgrt; Owner: postgres
--

ALTER DEFAULT PRIVILEGES FOR ROLE postgres IN SCHEMA sgrt REVOKE ALL ON FUNCTIONS  FROM PUBLIC;
ALTER DEFAULT PRIVILEGES FOR ROLE postgres IN SCHEMA sgrt REVOKE ALL ON FUNCTIONS  FROM postgres;
ALTER DEFAULT PRIVILEGES FOR ROLE postgres IN SCHEMA sgrt GRANT ALL ON FUNCTIONS  TO suite WITH GRANT OPTION;


--
-- TOC entry 2528 (class 826 OID 16424)
-- Name: DEFAULT PRIVILEGES FOR TABLES; Type: DEFAULT ACL; Schema: sgrt; Owner: postgres
--

ALTER DEFAULT PRIVILEGES FOR ROLE postgres IN SCHEMA sgrt REVOKE ALL ON TABLES  FROM postgres;
ALTER DEFAULT PRIVILEGES FOR ROLE postgres IN SCHEMA sgrt GRANT ALL ON TABLES  TO suite WITH GRANT OPTION;


--
-- TOC entry 2540 (class 826 OID 22437)
-- Name: DEFAULT PRIVILEGES FOR SEQUENCES; Type: DEFAULT ACL; Schema: sisat; Owner: suite
--

ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sisat REVOKE ALL ON SEQUENCES  FROM suite;
ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sisat GRANT SELECT,USAGE ON SEQUENCES  TO suite WITH GRANT OPTION;


--
-- TOC entry 2541 (class 826 OID 22438)
-- Name: DEFAULT PRIVILEGES FOR TYPES; Type: DEFAULT ACL; Schema: sisat; Owner: suite
--

ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sisat REVOKE ALL ON TYPES  FROM PUBLIC;
ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sisat REVOKE ALL ON TYPES  FROM suite;
ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sisat GRANT ALL ON TYPES  TO suite WITH GRANT OPTION;


--
-- TOC entry 2542 (class 826 OID 22439)
-- Name: DEFAULT PRIVILEGES FOR FUNCTIONS; Type: DEFAULT ACL; Schema: sisat; Owner: suite
--

ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sisat REVOKE ALL ON FUNCTIONS  FROM PUBLIC;
ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sisat REVOKE ALL ON FUNCTIONS  FROM suite;
ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sisat GRANT ALL ON FUNCTIONS  TO suite WITH GRANT OPTION;


--
-- TOC entry 2543 (class 826 OID 22440)
-- Name: DEFAULT PRIVILEGES FOR TABLES; Type: DEFAULT ACL; Schema: sisat; Owner: suite
--

ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sisat REVOKE ALL ON TABLES  FROM suite;
ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sisat GRANT ALL ON TABLES  TO suite WITH GRANT OPTION;


--
-- TOC entry 2525 (class 826 OID 16420)
-- Name: DEFAULT PRIVILEGES FOR SEQUENCES; Type: DEFAULT ACL; Schema: -; Owner: postgres
--

ALTER DEFAULT PRIVILEGES FOR ROLE postgres GRANT SELECT,USAGE ON SEQUENCES  TO suite WITH GRANT OPTION;


--
-- TOC entry 2527 (class 826 OID 16422)
-- Name: DEFAULT PRIVILEGES FOR TYPES; Type: DEFAULT ACL; Schema: -; Owner: postgres
--

ALTER DEFAULT PRIVILEGES FOR ROLE postgres GRANT ALL ON TYPES  TO suite WITH GRANT OPTION;


--
-- TOC entry 2526 (class 826 OID 16421)
-- Name: DEFAULT PRIVILEGES FOR FUNCTIONS; Type: DEFAULT ACL; Schema: -; Owner: postgres
--

ALTER DEFAULT PRIVILEGES FOR ROLE postgres GRANT ALL ON FUNCTIONS  TO suite WITH GRANT OPTION;


--
-- TOC entry 2524 (class 826 OID 16419)
-- Name: DEFAULT PRIVILEGES FOR TABLES; Type: DEFAULT ACL; Schema: -; Owner: postgres
--

ALTER DEFAULT PRIVILEGES FOR ROLE postgres GRANT ALL ON TABLES  TO suite WITH GRANT OPTION;


-- Completed on 2019-08-25 22:58:37 CDT

--
-- PostgreSQL database dump complete
--

